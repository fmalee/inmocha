/*
    个人文件柜 
	2012-10-26 
*/
function FileCabinet(setting){
    this.setting = setting;
    this.init();
}
!function(FileCabinet){
    FileCabinet.prototype = {
        init: function(){
            var oSelf = this;
            oSelf.oView = new View(oSelf.setting);
        }
    }
    var oInitSetting = { //初始设置
        toolbar:{//工具栏按钮生成
            check: true,
            back: true,
            upload: true,
            add: true,
            more: true,
            refresh: true,
            sort: true,
            viewMode: true,
            search: true,
            restore: false,
            del: false,
            clear: false
        },
        type : 'myfile', //类型：myfile, myshare, fromshare, trash
        uid: '',
        swfconfig: {
			hash : "",
			file_size_limit : "100",
			file_types : "*.*",
			file_types_description : "All Support Formats"
		}
    }
    var oInitConfig = {     //基本配置
        aSort: ['filename'],  //排序依据['addtime'=>添加时间, 'name'=>文件名, 'size'=>文件大小]
        sOrder: 'ASC',      //排序正反['ASC' => 正序]
        fid: 0,                 //当前目录ID
        idpath: "|000|",           //当前目录
        aPathData: [],
        aFilter: ['all'],           //筛选类型
        viewmode: 'thumb', //视图模式 ['thumb'=>网格, 'list'=>列表]
        //add: 'cate',            //默认添加类型['cate'=>目录,'doc'=>word文档,'xls'=>excel文档,'ppt'=>ppt文档]
        nNum: 18,          //每页显示数目
        nPage: 0                //页码
    }
    var oLang = {   //语言包
		tips: '提示',
        allcheck: "全选",
        back: "返回",
        del: "删除",
        clear: "清空",
        restore: "还原",
        upload: "上传文件",
        add: "新建",
        more: "更多操作",
        open: "打开",
        download: "下载",
		mark: "标记",
        remark: "备注",
        share: "共享",
        sharer: "共享人",
        path: "路径",
        move: "移动",
        rename: "重命名",
		edit: '编辑',
		show: '查看',
        refresh: "刷新",
        sort: "排序",
        filename: "文件名",
        time: "时间",
        size: "大小",
        viewthumb: "网格视图",
        viewlist: "列表视图",
        search: "搜索",
        addtime: "创建时间",
        total: "包含文件",
        file: "文件",
        all: "全部 ",
        doc: "文档",
        text: "文本",
        image: "图片",
        'package' : "压缩包",
        video: "视频",
        audio: "音乐",
        program: "程序",
		excel: 'excel',
		word: 'word',
		ppt: 'ppt',
        directory: "文件夹",
        multifile: "多个文件",
        noInfo: "暂无信息",
        entry: " 个",
        myfile: "我的文件柜",
        myshare: "我的共享",
        fromshare: "我收到的共享",
        trash: "回收站",
        sift: "筛选",
        siftMyFile: "筛选我的文件",
        select: "选择",
        staff: "人员",
        department: "部门",
        position: "职位",
        toDel: "是否删除",
        multiFile: "多个文件",
        selected: "选中",
        trash: "回收站",
        errorMoveToSelf: "移动失败，目标文件夹被选中",
        errorNoMove: "移动失败，源文件夹与目标文件夹相同",
        errorIsTop: "已经到达顶级!",
		downloadStart: '下载文件',
		immediatelyTo: '是否立即对',
		proceedEdit: '进行编辑',
		loadOfficeError: '读取OFFICE文件错误',
		inputKey: '请输入文件名',
		dblclick: '双击',
		clicknew: '点击此处新建'
    };
    var ex = {     //扩展函数
        /**
            将对象解析为PHP表单参数
            @param obj o 要解析的对象
            @return string 解析后的URL
            @example var o = {a: 1, b: 2}  parseParam(o)//  a=1&b=2
            **/
        parseParam: function(o){
            var aParam = [], i;
            o.baseURL = o.baseURL||"";
            for(i in o){
                if(i == 'baseURL') continue;
                aParam.push(i + '=' + o[i]);
            } 
            url = o.baseURL + (o.baseURL.indexOf('?') == -1 ? '?' : '&') + aParam.join('&'); 
            //url = o.baseURL + (o.baseURL.indexOf('?') == -1 ? '?' : '&') + $.param(o); 
            return url;
        },
        /**
            格式化文件大小，为文件大小附上单位
            @param number size 大小，初始单位为B
            @return string  附上单位后的大小
            **/
        formatSize: function(size){//格式化文件大小
            var size = parseInt(size)
            var nSize = size < 1024 ? size.toFixed(2) + "B"  : size < 1024 * 1024 ? (size/1024).toFixed(2) + 'KB' : size < 1024 * 1024 *1024 ? (size/1024/1024).toFixed(2) + 'MB' : (size/1024/1024/1024).toFixed(2) + 'GB';
            return nSize;
        },
        showMsg: function(tip, type){//错误提示
            var sTip = tip||"",
                type = type||"warning";
            if(type == "set"){type = "success"}
            $("body").iTips({
                content:sTip,
                css: type
            })
        },
        post: function(oUrl, oData, fnCallBack, sType) {
            var oUrl = $.extend({
                'baseURL': 'desktop.php',
                'mod': 'file',
                //do是保留字，因此需要使用字符串的形式赋值和读取，为了统一形式其它属性也一样
                'do': 'action'
            }, oUrl);
            $.ajax({
                url: ex.parseParam(oUrl) + '&random=' + new Date().getTime(),
                type: 'POST',
                data: oData,
                dataType: sType || 'json',
                success: function(oResult){
                    fnCallBack(oResult);
                }
            })
        },
        /**
            从文件信息数组中，返回文件id序列
            @param array aFileData 文件信息
            @return array 文件 fid
            **/
        getFileIds: function(aFileData){
            var aIds = [], i = 0, l = aFileData.length;
            if(!l) return aIds;
            for(; i < l; i++){
                aIds.push(aFileData[i].fid)
            }
            return aIds;
        },
        /**
            @explain 判断对象是否为空
            @param obj obj 需进行判断的对象
            @return boolean true则为空
            **/
        isEmptyObject: function( obj ) {
            for ( var name in obj ) {
                return false;
            }
            return true;
        },
		/**
		 * 检查文件类型
		 * @param string sSuffix 文件后缀
		 * @param string sCkType 可选 指定类型 判定文件后缀是否属于这个类型
		 * @param string|boolean
		 */
		checkFileType: function(sSuffix, sCkType) {
			var fnCallee = arguments.callee,
				oTypeSet = fnCallee.oTypeSet;
			if(!oTypeSet) {
				oTypeSet = fnCallee.oTypeSet = {
					'excel' : ['xlsx', 'xlsm', 'xlsb', 'xltx', 'xltm', 'xlt', 'xls', 'xml', 'xlam', 'xla', 'xlw', 'csv'],
					'word' : ['doc', 'docm', 'docx' ,'dot', 'dotm', 'dotx'],
					'ppt' : ['pptx', 'pptm', 'ppt', 'potx', 'potm', 'pot', 'pps', 'ppsx', 'ppsm', 'ppam', 'ppa'],
					'text' : ['txt'],
					'image' : ['bmp', 'pcx' ,'tiff', 'gif', 'jpeg', 'jpg', 'tga', 'exif', 'fpx', 'svg', 'psd', 'cdr', 'pcd', 'dxf', 'ufo', 'eps', 'png', 'cdr', 'wmf', 'emf', 'hdri' ,'ai', 'raw', 'lic', 'fli', 'flc'],
					'package' : ['zip', 'rar', '7z', 'tar', 'gz', 'bz2'],
					'audio' : ['aac', 'ac3', 'acc', 'aiff', 'amr', 'ape', 'au', 'cda', 'dts', 'flac', 'm1a', 'm2a', 'm4a', 'mka', 'mp2', 'mp3', 'mpa', 'mpc', 'ra', 'tta', 'wav', 'wma', 'wv', 'mid', 'midi', 'ogg', 'oga'],
					'video' : ['asf', 'avi', 'wm', 'wmp', 'wmv', 'ram', 'rm', 'rmvb', 'rp', 'rpm', 'rt', 'smil', 'scm', 'dat', 'm1v', 'm2v', 'm2p', 'm2ts', 'mp2v', 'mpe', 'mpeg', 'mpeg1', 'mpeg2', 'mpg', 'mpv2', 'pss', 'pva', 'tp', 'tpr', 'ts', 'm4b', 'm4p', 'mp4', 'mpeg4', '3g2', '3gp', '3gp2', '3gpp', 'mov', 'qt', 'mov', 'qt', 'flv', 'f4v', 'swf', 'hlv', 'ifo', 'vob', 'amv', 'csf', 'divx', 'evo', 'mkv', 'mod', 'pmp', 'vp6', 'bik', 'mts', 'xv', 'xlmv', 'ogm', 'ogv', 'ogx', 'dvd'],
					'program' : ['exe', 'bat', 'dll', 'sh', 'rpm', 'srpm', 'deb']
				}
			}
			if(sCkType) {
				return ($.inArray(sSuffix, oTypeSet[sCkType]) > -1);
			} else {
				for(var sType in oTypeSet) {
					if($.inArray(sSuffix, oTypeSet[sType]) > -1) {
						return sType;
					}
				}
				return '';
			}
		}
    }

/*--------------------  视图生成 --------------------*/
    function View(setting){
        this.setting = {};
        $.extend(true, this.setting, oInitSetting, setting);
        window.f = this.oDatahandler = new Datahandler(this.setting.type);
        this.oOperate = new Operate(this, this.oDatahandler);
		this.aLocalData = [];
        this.init();
    }
    View.prototype = {
        init: function(){
            var oSelf = this,
                oContainer = $("#"+ oSelf.setting.containerId),
                oInfobar = $("#"+ oSelf.setting.infobarId),
                oToolbar = oSelf.createToolbar(),
                oHeader = oSelf.createHeader(),
                oListWrap;
			oSelf.bindHotKeys();
            oSelf.oDatahandler.getDirFile(oInitConfig.fid, function(data){
				oSelf.setLocalData(data);
                oListWrap= oSelf.createFileList(oSelf.oDatahandler.formatData(data, oInitConfig));
				oContainer.append(oToolbar,oHeader, oListWrap);
				oSelf.createInfobar();
            })
         //   $("body").noSelect();
        },
		
		/**
		 * 绑定快捷键操作
		 */
		bindHotKeys: function() {
			var oSelf = this;
			$(document).on('mousedown', '.file-list li', function(e) {
				if(e.which != 1) {
					return;
				}
				var oLi = $(this);
				//crtl + 鼠标 多选
				if(e.crtlKey) {
					oSelf.oOperate.select(oLi);
				//shift + 鼠标 多选	
				} else if(e.shiftKey) {
					var oFilelist = $('.file-list li'),
						oFirstSlLi = oFilelist.filter('.selected:eq(0)');
					if(!oFirstSlLi.get(0)) {
						return;
					}
					oSelf.oOperate.unSelectAll();
					var nIndex = oLi.index(),
						nFirstSlIndex = oFirstSlLi.index();
					//选择分两种情况，当前选择点可能大于或者小于已选择点，分别计算出应该选择的起始位置和结束位置
					if(nIndex > nFirstSlIndex) {
						var nFilterStart = nFirstSlIndex - 1 < 0 ? 0 : nFirstSlIndex - 1,
							nFilterEnd = nIndex;
					} else {
						var nFilterStart = nIndex -1 < 0 ? 0 : nIndex -1,
							nFilterEnd = nFirstSlIndex;
					}
					if(nIndex == 0 || nFirstSlIndex == 0) {
						oSelf.oOperate.select($(oFilelist.get(0)));
					}
					oFilelist
					//使用gt以及lt选择器进行先后两次筛选
					//第一次筛选返回的结果位置是从起始位置开始，所以第二次筛选结束位置要减去起始位置
					.filter(':gt(' + nFilterStart + '):lt(' + (nFilterEnd - nFilterStart) + ')')
					.each(function() {
						oSelf.oOperate.select($(this));
					});
				}
			});
			
			$(document).on('keydown', function(e) {
				//全选 ctrl + a
				if(e.ctrlKey && e.which == 65) {
					$("body").noSelect();
					oSelf.oOperate.selectAll();
				//删除 delete
				} else if(e.which == 46 && oSelf.setting.type != 'fromshare') {
					oSelf.oOperate[oSelf.setting.type == 'trash' ? 'destory' : 'del']();
				//后退 backspace
				} else if(e.which == 8 && document.activeElement) {
					//暂时取消backspace后退键，因为弹出dialog的时候也可以按，这样会导致一些问题，比如新建文件夹
					//所以dialog应该被再封装到当前类统一管理，同时也方便增加回车键事件等
//					if($.inArray(document.activeElement.nodeName, ['INPUT', 'TEXTAREA']) == -1) {
//						oSelf.oOperate.back();
//						return false;
//					}
				}
			});
		},
		
		/**
		 * 设置本地数据，函数会重置页码，否则新数据比旧数据少并且原页码比较大的时候，数据会无法显示，因为当前页没有数据
		 * 如果你不想改变页码（比如在刷新页面的时候），请先保存原来的页码，设置数据后再还原
		 * @param array aData
		 */
		setLocalData: function(aData) {
			oInitConfig.nPage = 1;
			this.aLocalData = aData;
		},
		
		/**
		 * 获取本地数据
		 */
		getLocalData: function() {
			return this.aLocalData;
		},
        /**
            创建工具栏，根据全局setting，决定工具栏功能
            **/
        createToolbar: function(){
            var oSelf = this, 
                oToolWrap = $("<div></div>").attr({'id': 'file_toolbar', 'class': 'file-toolbar cl'}),
                oRTool = $("<div></div>").attr("class","y"),
                oLTool = $("<div></div>").attr("class","z"),
                oToolbar = oSelf.setting.toolbar;
                oToolWrap.append(oRTool, oLTool)
                oSelf.btns = {};
            /* Left-Side Toolbar*/
            if(oToolbar.check){ //全选按钮
                var  oSelf = this;
                oSelf.btns.allcheck = oSelf.createOperateBtn("","btn mr10", "i-file-check").appendTo(oLTool);
                oSelf.btns.allcheck.on("click", function(){oSelf.oOperate.allCheck();})
            } 
            if(oToolbar.back){//返回按钮
                var oBackBtn = oSelf.createOperateBtn("","btn mr10", "i-file-return").appendTo(oLTool); 
                oBackBtn.on('click', function(){oSelf.oOperate.back()})
            }
            if(oToolbar.upload){ //上传按钮
                var oUploadBtn = oSelf.createOperateBtn(oLang.upload ,"btn btn-primary mr10", "i-file-upload").appendTo(oLTool);
                oUploadBtn.on('click', function(){oSelf.oOperate.upload()})
            } 
            if(oToolbar.restore){  //还原按钮
                var oRestoreBtn = oSelf.createOperateBtn("<span>" + oLang.restore + "</span>" ,"btn mr10", "").appendTo(oLTool);
                oRestoreBtn.on('click', function(){ 
                    if(oSelf.oOperate.aSelectedItem.length === 0) return false;
                    oSelf.oOperate.restore() 
                })
            }
            if(oToolbar.del){  //删除按钮
                var oDelBtn = oSelf.createOperateBtn("<span>" + oLang.del + "</span>","btn mr10", "").appendTo(oLTool);
                oDelBtn.on('click', function(){ 
                    if(oSelf.oOperate.aSelectedItem.length === 0) return false;
                    oSelf.oOperate.destory()
                })
            }
            if(oToolbar.clear){ //清除按钮
                var oClearBtn = oSelf.createOperateBtn("<span>" + oLang.clear + "</span>","btn mr10", "").appendTo(oLTool);
                oClearBtn.on('click', function(){oSelf.oOperate.clear();})
            } 
           if(oToolbar.add){        //新建按钮列表
                var oSelf = this,  
                    oNewWrap = $("<div></div>").attr({"class": "btn-group disib vat"}), 
                    oNewBtn = oSelf.createOperateBtn("" ,"btn ac", "i-file-directory").attr({"act":"directory", "title": oLang.clicknew}),
                    oCaret = oSelf.createOperateBtn("","btn dropdown-toggle","caret").attr({"href": "", "data-toggle": "dropdown"}),
                    oSetting = {
                        directory: {ico:"i-file-directory", txt: "&nbsp;" + oLang.directory},
                        doc: {ico:"i-file-doc", txt: "&nbsp;" + oLang.word},
                        xls: {ico:"i-file-xls", txt: "&nbsp;" + oLang.excel},
                        ppt: {ico:"i-file-ppt", txt: "&nbsp;" + oLang.ppt}
                    },
                    oNewList = oSelf.createOperateList(oSetting),
                    oNewItem = $("li", oNewList);
                oNewItem.each(function(){
                    if($(this).attr("act") == oNewBtn.attr("act")){
                        $(this).hide();
                    }
                })
                oNewWrap.append(oNewBtn, oCaret, oNewList)
                oNewWrap.addClass("mr10").appendTo(oLTool);
                oNewItem.on({
                    "click": function(){
                        var sAct = $(this).attr("act")
                        oNewItem.show();
                        $(this).hide();
                        oNewBtn.attr("act", sAct).find("i").attr("class", "i-file-" + sAct);
                        oSelf.oOperate.add(sAct);
                    }
                }).hover(function(){$(this).addClass("ac")},function(){$(this).removeClass()})
                oNewBtn.on('click', function(){oSelf.oOperate.add($(this).attr("act"))})
           }
           if(oToolbar.more){   //更多操作按钮列表
                var  oSelf = this,
                    oMoreWrap = oSelf.oMoreWrap = $("<div></div>").attr({"class": "btn-group vat"}).css("display", "inline-block").hide(), //For Ie9 inline-block
                    oMoreBtn = oSelf.createOperateBtn(oLang.more ,"", ""),
                    oCaret = oSelf.createOperateBtn("","btn dropdown-toggle","caret").attr({"href": "", "data-toggle": "dropdown"});
                oMoreWrap.append(oMoreBtn, oCaret);
                oMoreWrap.appendTo(oLTool);
           }
            /* Right-Side Toolbar*/
            if(oToolbar.refresh){ //刷新按钮
                var oRefreshBtn = oSelf.createOperateBtn("","", "i-file-refresh").appendTo(oRTool);
                oRefreshBtn.on("click", function(){
                    oSelf.oOperate.refresh();
                })
            }
            if(oToolbar.sort){ //排序按钮列表
                var oOrderWrap = $("<div></div>").attr("class", "btn-group disib vat ml10"),
                    oOrderBtn = oSelf.createOperateBtn(oLang.sort,"btn", "i-file-order").attr({"href": "", "data-toggle": "dropdown"}),
                    oSetting = {
                        filename: {ico: "i-file-asc", txt: oLang.filename},
                        addtime: {ico: "i-file-none", txt: oLang.time},
                        size: {ico: "i-file-none", txt: oLang.size}
                    },
                    oOrderList = oSelf.createOperateList(oSetting);
                oOrderWrap.append(oOrderBtn, oOrderList)
                oOrderWrap.appendTo(oRTool);
                var oItem = $("li", oOrderList);
                oItem.on("click", function(){
                    var oIco = $("i", $(this));
                    if(oIco.hasClass("i-file-desc")){
                        oIco.attr("class","i-file-asc");
                        oSelf.oOperate.sort($(this).attr("act"), 'ASC')
                    }else{
                        oIco.attr("class","i-file-desc");
                        oSelf.oOperate.sort($(this).attr("act"), 'DESC')
                    }
                    $(this).siblings().find("i").attr("class","i-file-none");
                })
            } 
            if(oToolbar.viewMode){ //视图切换按钮组
                var  oViewWrap = $("<div></div>").attr({"class": "btn-group disib vat", "data-toggle":"buttons-radio"}), 
                    oThumbBtn = oSelf.createOperateBtn("","btn active", "i-file-thumb"),
                    oListBtn = oSelf.createOperateBtn("","","i-file-list");
                oViewWrap.append(oThumbBtn, oListBtn);
                oThumbBtn.on("click", function(){ 
                    if($(this).hasClass("active"))return
                    oSelf.viewThumb()
                });
                oListBtn.on("click", function(){ 
                    if($(this).hasClass("active"))return
                    oSelf.viewList()
                });
                oViewWrap.addClass("ml10").appendTo(oRTool);
            }
            if(oToolbar.search){ //搜索框
                var  oSearchWrap = $("<div></div>").attr({"class": "input-append disib vat"}), 
                    oSearchInput = $("<input type='text' placeholder='" + oLang.inputKey + "' class=\"span4\" />"),
                    oSearchBtn = oSelf.createOperateBtn("","","i-file-search");
                oSearchWrap.append(oSearchInput, oSearchBtn);
                oSearchWrap.addClass("ml10").appendTo(oRTool);
				var fnSearch = function() {
					var sKeyword = oSearchInput.val();
					if(sKeyword == "") {
						oSearchInput.focus();
						return;
					}
					oSelf.oOperate.search(sKeyword);
				}
				oSearchInput.on('keydown', function(e) {
					//绑定回车事件
					if(e.which == 13) {
						fnSearch();
					}
				});
                oSearchBtn.on("click", function(){
                    fnSearch();
                })
            }   
            return oToolWrap;
        },
        /**
            创建工具栏按钮
            @param string txt 按钮文本
            @param string cls 按钮样式
            @param string ico 按钮小图标
            **/
        createOperateBtn: function(txt, cls, ico){ 
            var txt = txt||"",
                cls = cls || "btn",
                oBtn = $("<a></a>").attr({href: "javascript:;", "class": cls});
            if(ico){
                var oIco =$("<i></i>").attr("class",ico);
                oIco.after(" " + txt).appendTo(oBtn);
            }else{
                oBtn.html(txt);
            }
            return oBtn
        },
        /**
            创建操作列表
            @param obj setting 各列表配置
            @param listcls 列表样式
            **/
        createOperateList: function(setting,listcls){  //创建新建列表
            var sListCls = listcls||"", 
                oNewList = $("<ul></ul>").attr("class", sListCls + " dropdown-menu");
                for(var i in setting){
                    var oItem = $("<a></a>").attr("href", "javascript:;");
                    setting[i].txt = setting[i].txt||"";
                    setting[i].show = setting[i].show === false ? false : true;
                    if(!setting[i].show) continue;
                    if(setting[i].ico){
                        $("<i></i>").attr("class", setting[i].ico).after(" " + setting[i].txt).appendTo(oItem)
                    }else{
                        oItem.html(setting[i].txt)
                    }
                    var oItemWrap = $("<li></li>").attr("act", i);
					if(setting[i].title) {
						oItemWrap.attr("title", setting[i].title);
					}
                    oItemWrap.append(oItem).appendTo(oNewList);
                }
                return oNewList;
        },
        /**
            更多操作列表
            **/
        moreList: function(){
            var oSelf = this,
			l = oSelf.oOperate.aSelectedItem.length;
            if(l == 0){
                oSelf.oMoreWrap.hide();
                return false;
            }else{
                var oSetting = {
                    download: {ico: "i-fileop-download", txt: oLang.download},
                    remark: {ico: "i-fileop-remark", txt: oLang.remark},
                    share: {ico: "i-fileop-share", txt: oLang.share},
                    move: {ico: "i-fileop-move", txt: oLang.move},
                    rename: {ico: "i-fileop-rename", txt: oLang.rename},
                    del: {ico: "i-fileop-del", txt: oLang.del}
                };

                if(oSelf.setting.type == "fromshare"){ //当模式为“我收到的共享时”， 隐藏各种操作
                    oSetting.remark = oSetting.share = oSetting.move = oSetting.rename = oSetting.del = false
                }
                if(l > 1){
                    oSetting.remark.show = oSetting.share.show = oSetting.rename.show = false;
                }
/*                 for(var i = 0; i < l ; i++ ){
                    if(oSelf.oOperate.aSelectedItem[i].type != "directory")continue;
                    oSetting.download.show = false;
                } */
                var oMoreList = oSelf.createOperateList(oSetting);
                $("ul", oSelf.oMoreWrap).remove();
                oMoreList.appendTo(oSelf.oMoreWrap)
                $("li", oMoreList).on('click', function(){ //更多列表，列表项点击事件
                    var sAct = $(this).attr("act");
                    oSelf.oOperate.moreOperate(sAct);
                })
                oSelf.oMoreWrap.css('display', 'inline-block').show();
                if(l > 1){
					setTimeout(function() {
						oSelf.oMoreWrap.addClass("open");
					}, 0);
                }
            }
        },
        /**
            文件柜头部，包含面包屑及筛选栏
            **/
        createHeader: function(){
            var oSelf = this, oSiftBtn = {}, oSiftBar = {},
                oWrap = $("<div></div>").attr({"class": "posr"}),
                oPath = oSelf.createPath();
            if(oSelf.setting.sift){
                var oSiftBtn = $("<a></a>").attr({"href":"javascript:;", "class":"file-sift-btn"}),
                    oIco = $("<i></i>").attr("class","i-file-triangle"),
                    oSiftBar = oSelf.createSiftBar();
                oSiftBtn.html(oLang.sift)
                oIco.appendTo(oSiftBtn);
                oSiftBtn.on("click", function(){
                    $(this).toggleClass("active");
                    oSiftBar.toggle();
                }) 
            }
            oWrap.append(oPath, oSiftBtn, oSiftBar)
            return oWrap;
        },
        /**
            创建面包屑
            @param  string sMode[window, dialog]  默认为window，即文件柜路径，当为dialog时，创建对话框路径，主要作用于移动
            @explain 创建面包屑相关节点，当mode为window时，面包屑对应 this.oPath, 为dialog时对应 this.oDirPath;
                当mode为window，且具筛选功能（即setting.sift为true）时，创建筛选相关节点
            @return  obj    面包屑容器节点
           **/
        createPath: function(sMode){ 
            var oSelf = this,
                sMode = sMode||"window",
                oPath = $("<ul></ul>").attr("class","breadcrumb mtm");
            if(sMode == "dialog"){ 
                oSelf.oDirPath = oPath;
            }else{
                oSelf.oPath = oPath;
            };
            oSelf.refreshPath(sMode);
            return oPath;
        },
        /**
            刷新面包屑
            @param  string sMode[window, dialog]  默认为window，即文件柜路径，当为dialog时，刷新对话框路径，主要作用于移动
           **/
        refreshPath: function(sMode, aPathData){ 
            var oSelf = this, oData,
                sMode = sMode||"window",
                aPathData = aPathData||oInitConfig.aPathData,
                oPath = sMode == "dialog" ? oSelf.oDirPath : oSelf.oPath;
            if(aPathData.length == 0){//当路径数据为空时，默认为最上层路径
                aPathData = [{fid: '0', filename: oLang[oSelf.setting.type]}];
            }
            oPath.empty() //清空路径重新生成
            for(var i = 0, l = aPathData.length; i < l; i++){
                var oPathItem = $("<li></li>");
                var oPathItemInner = $("<a></a>").attr({"href":"javascript:;", "fid": aPathData[i].fid})
                if(i + 1< l){
                    oPathItemInner.html(aPathData[i].filename);
                    oPathItemInner.after("&nbsp;&gt;&nbsp;").appendTo(oPathItem);
                    oPathItemInner.on("click", function(){
                        if(sMode == "dialog"){
                            var nFid = $(this).attr("fid");
                            oSelf.oDatahandler.getDirFile(nFid, function(data){
                                var aFileData = oSelf.oDatahandler.filter(nFid, ['directory']),
                                    oDirList = oSelf.createDirList(aFileData),
                                    aPathData = oSelf.oOperate.splitPath(nFid);
                                $(".dir-list").replaceWith(oDirList);
                                oSelf.nDirId = nFid;
                                oSelf.refreshPath("dialog", aPathData);
                            })
                        }else{
                            oSelf.oOperate.openDir($(this).attr("fid"), sMode);
                        }
                    })
                }else{
                    oPathItem.html(aPathData[i].filename)
                    oPathItem.attr("class","active");
                }
                oPathItem.appendTo(oPath);
            }
        },
        /**
            当可筛选时，创建筛选栏
            @return obj 筛选栏节点
            **/
        createSiftBar: function(){
                var oSelf = this, 
                    oSiftBar = $("<div></div>").attr("class", "file-sift-bar"),
                    oStarIco = $("<a></a>").attr({"href":"javascript:;", "class":"i-file-star", "act": "mark"}),
                    oSiftList = $("<ul></ul>").attr("class", "file-sift-list cl"),
                    oSetting = {
                        all: oLang.all,
                        word : "doc" + oLang.doc,
                        excel : "xls" + oLang.doc,
                        ppt: "ppt" + oLang.doc,
                        text: oLang.text,
                        image: oLang.image,
                        'package': oLang['package'] ,
                        video: oLang.video,
                        audio: oLang.audio
                  //      program: oLang.program
                    }
                for(var i in oSetting){
                    var oSiftItem = $("<li></li>"),
                        oSiftItemInner = $("<a></a>").attr({"href":"javascript:;", "act": i}).html(oSetting[i]);
                    oSiftItemInner.appendTo(oSiftItem)
                    oSiftItem.appendTo(oSiftList);
                    oSiftItemInner.on("click", function(){
                        $("a", oSiftList).removeClass();
                        $(this).attr("class", "active")
                        oSelf.oOperate.sift($(this).attr("act"))
                    })
                }
                oSiftBar.append($("<span></span>").html(oLang.siftMyFile), oStarIco, oSiftList);
                oStarIco.on("click", function(){
                    oSelf.oOperate.sift($(this).attr("act"))
                })
                return oSiftBar;
        },
        /**
             创建文件柜文件列表
             @return obj 文件柜列表节点
			@denglh 修改去除参数，在调用本函数前请确认oSelf.aLocalData的数据完整（未经格式化），在创建列表时程序会自动根据oInitConfig格式化，这样能解决很多问题并且精简代码
           **/
        createFileList: function(){
            var oSelf = this,
				aFileData = oSelf.oDatahandler.formatData(oSelf.getLocalData(), oInitConfig),
                oList = $("<ul></ul>").attr("class", "file-list cl" + (oInitConfig.viewmode == "list" ? " view-list": "")),
                oListWrap = $("<div></div>").append(oList);
			if(!oSelf.oMulti) {
				oSelf.oMulti = $('<div class="file-multi pg" style="margin: 5px;"></div>').appendTo(oListWrap);
			}
            oSelf.oList = oList;
            if(!aFileData||aFileData.length === 0) { //若无数据则显示无相关信息
                oList.addClass("nodata-tip")
            } else {
                for(var i = 0, l = aFileData.length; i < l; i++){
                    oList.append(oSelf.createItem(aFileData[i],i));
                }
            }
            oSelf.addListEvent(oList);
			oSelf.createMulti(oSelf.getLocalData().length);
            return oListWrap;
        },
		
		/**
		 * 创建分页
		 * @paran nTotal 多少页
		 * @param nShowPage 暂支持奇数
		 */
		createMulti: function(nTotal, nShowPage) {
			var oSelf = this,
				nShowPage = nShowPage || 5,
				nNum = oInitConfig.nNum,
				nPage = oInitConfig.nPage || 1,
				sLeft = '', sCenter = '', sRight = '',
				nPageTotal = Math.ceil(nTotal / nNum),
				nHalfPageTotal = (nShowPage - 1) / 2;
			//判断是否超出最大分页
			if(nPageTotal > nShowPage) {
				if(nHalfPageTotal < nPage - 1) {
					sLeft += '<a class="first">1 ...</a>';
				}
				if(nHalfPageTotal < nPageTotal - nPage) {
					sRight += '<a class="last"> ...' + nPageTotal + '</a>';
				}
			}
			//计算当前页面的左右两边填充页数
			var nLeftFill = 0, nRightFill = 0;
			if(sLeft) {
				nLeftFill = nHalfPageTotal;
			} else {
				nLeftFill = nPage - 1;
			}
			if(sRight) {
				nRightFill = nHalfPageTotal;
			} else {
				nRightFill = (nPageTotal - nPage);
			}
			if(!sLeft && sRight) {
				nRightFill += nHalfPageTotal - nLeftFill;
			}
			if(sLeft && !sRight) {
				nLeftFill += nHalfPageTotal - nRightFill;
			}
			//生成html
			if(nPageTotal > 1) {
				for(var i = 0; i < nLeftFill; i++) {
					sCenter += '<a>' + (nPage - nLeftFill + i) + '</a>';
				}
				sCenter += '<strong>' + nPage + '</strong>';
				for(var i = 0; i < nRightFill; i++) {
					sCenter += '<a>' + (nPage + i + 1) + '</a>';
				}
				var sHtml = sLeft + sCenter + sRight;
				if(nPage == 1) {
					var sPrevClass = 'disabled';
				}
				sHtml += '<a class="prev ' + sPrevClass + '"><i class="icon-arrow-left"></i></a>';
				if(nPage == nPageTotal) {
					var sNextClass = 'disabled';
				}
				sHtml += '<a class="next ' + sNextClass + '"><i class="icon-arrow-right"></i></a>';
				oSelf.oMulti.html(sHtml);
				oSelf.oMulti.find('a').each(function() {
					var oLink = $(this);
					oLink.click(function() {
						if(oLink.hasClass('first')) {
							nPage = 1;
						} else if(oLink.hasClass('last')) {
							nPage = nPageTotal;
						} else if(oLink.hasClass('prev')) {
							if(oLink.hasClass('disabled')) {
								return;
							}
							oInitConfig.nPage = --nPage;
						} else if(oLink.hasClass('next')) {
							if(oLink.hasClass('disabled')) {
								return;
							}
							oInitConfig.nPage = ++nPage;
						} else {
							nPage = oLink.html();
						}
						oInitConfig.nPage = parseInt(nPage);
						oSelf.oOperate.refreshList();
					});
				});
			} else {
				oSelf.oMulti.html('');
			}
		},
		
        /**
            创建文件项
            @param obj  oFileData 文件项数据
            @param obj index    文件项下标
            @return obj 文件项节点
            **/
        createItem: function(oFileData, index){ //创建文件节点
            var oSelf = this, 
                oItem = $("<li></li>").attr("index", index),
                oCheckIco = $("<a></a>").attr({href:"javascript:;", "class": "i-file-check"}),
                oCheckbox = $("<input />").attr({type: 'checkbox', id: oFileData.fid}).hide(),
				oIco = $("<i></i>").attr("class","file-type "+ oSelf.getIcon(oFileData.type) + (oInitConfig.viewmode == "thumb" ? "":"-lt")),
                oMark = $("<a></a>").attr({"href":"javascript:;", "class":"i-file-mark", 'title': oLang.mark}),
                oShare = $("<i></i>").attr("class", "i-file-hand"),
                oFileName = $("<div></div>").attr( "class", "file-name").append($("<a></a>").attr({href: "javascript:;", "title": oFileData.filename}).html(oFileData.filename)),
                oFileDesc = $("<div></div>").attr("class","file-desc").html(oFileData.type=="directory" ? oFileData.addtime : ex.formatSize(oFileData.size)),
                oFileOption = $("<div></div>").attr("class","file-opt"),
				oDownload = $("<a></a>").attr({"href": "javascript:;", "class": "i-fileop-download"}),
				oRemark = $("<a></a>").attr({"href": "javascript:;", "class": "i-fileop-remark"}),
				oMore = $("<a></a>").attr({"href": "javascript:;", "class": "i-fileop-more"}),
				restore = $("<a></a>").attr({"href": "javascript:;", "class": "i-fileop-restore"}),
				del = $("<a></a>").attr({"href": "javascript:;", "class": "i-fileop-del"});
			//当类型为我收到的共享时或者回收站时， 隐藏 “备注按钮”, "星标"
            if($.inArray(oSelf.setting.type, ["fromshare", "trash"]) > -1){
                oRemark.css('visibility', 'hidden');
                oMark.hide();
            }
			//当类型为“回收站”， 隐藏 “操作栏”, "星标"
            if(oSelf.setting.type == "trash"){
				restore.on("click", function(e){
					oSelf.oOperate.selectOne(oItem);
					oSelf.oOperate.restore();
				})
				del.on("click", function(){
					oSelf.oOperate.selectOne(oItem);
					oSelf.oOperate.del();
				})
				oFileOption.append(restore, del);
            } else {
				oDownload.on("click", function(e){
					oSelf.oOperate.selectOne(oItem);
					oSelf.oOperate.download();
				})
				oRemark.on("click", function(){
				//	alert("your sister")
					oSelf.oOperate.selectOne(oItem);
					oSelf.oOperate.remark()
				})
				oMore.on("click", function(e){
					oSelf.oOperate.selectOne(oItem);
					oSelf.operateList(e)
				})
				oFileOption.append(oDownload, oRemark, oMore);
			}
            if(oFileData.type !="directory"){
                oMark.appendTo(oIco);
                if(oFileData.mark == 1){
                    oMark.addClass("marked");
                }
				if(oSelf.setting.type != 'trash') {
					var sTitle = oLang.dblclick;
					if($.inArray(ex.checkFileType(oFileData.type), ['word', 'excel', 'ppt']) > -1) {
						sTitle += oFileData.attachdata && +oFileData.attachdata.edit ? oLang.edit : oLang.show;
					} else if($.inArray(oFileData.type, ['png', 'jpg', 'jpeg', 'gif']) > -1 || ex.checkFileType(oFileData.type, 'text')) {
						sTitle += oLang.show;
					} else {
						sTitle += oLang.download;
					}
					oIco.attr('title', sTitle);
				}
            }else{
                oIco.attr('title','双击打开')
            }
            oShare.hide().appendTo(oIco);
            if(oFileData.touids||oFileData.todeptids||oFileData.toposids){
                oShare.show()
            }
            oItem.append(oCheckIco, oCheckbox, oIco, oFileName, oFileDesc, oFileOption);
            oMark.on({
                "click": function(){
                    oSelf.oOperate.mark($(this),oItem);
                },
                "mousedown": function(e){
                    e.stopPropagation();
                }
            })
            oFileData.self = {//数据的self属性指向本身对应节点
                wrap: oItem,
                name: oFileName
            }
            oItem.data('att',oFileData); //将每份数据绑定到对应节点
            return oItem;
        },
        /**
            创建单文件操作菜单
            @param  event e 事件对象
            @param  type    触发类型 0为下拉触发[默认], 1为右键触发
            **/
        operateList: function(e,type){
            var oSelf = this,
                type = type||0,
                oListWrap = $("#operate_list").length != 0 ? $("#operate_list"): $("<div></div>").attr({"id":"operate_list", "class": "open btn-group"}),
                oSetting = {
                    open: { ico: "i-fileop-open", txt: oLang.open },
                    download: {ico: "i-fileop-download", txt: oLang.download},
                    share: {ico: "i-fileop-share", txt: oLang.share},
                    move: {ico: "i-fileop-move", txt: oLang.move},
                    rename: {ico: "i-fileop-rename", txt: oLang.rename},
                    del: {ico: "i-fileop-del", txt: oLang.del}
                };
            if(oSelf.setting.type == "fromshare"){ //当模式为“我收到的共享时”， 隐藏各种操作，只显示“下载”
                oSetting.share = oSetting.move = oSetting.rename = oSetting.del = false;
            }
            if(oSelf.setting.type == "trash"){ //当模式为“回收”， 只显示“删除”， 此时功能等同“彻底删除”
                oSetting.download = oSetting.share = oSetting.move = oSetting.rename = oSetting.open = false;
            }
            if(oSelf.oOperate.aSelectedItem.length > 1){    //当选中多项时，触发失败
                return;
            }else{
                var oItem = oSelf.oOperate.aSelectedItem[0]
            };
            if(oItem.type != "directory" ) oSetting.open = false;
            if(type == 0){
                var oTar = e.target;
                var nTop = $(oTar).offset().top + $(oTar).outerHeight();
                var nLeft = $(oTar).offset().left ;
            }else{
                var nTop = e.clientY + $(window).scrollTop();
                var nLeft = e.clientX + $(window).scrollLeft();
            }
            oListWrap.empty();
            var oList = oSelf.createOperateList(oSetting);
            oList.appendTo(oListWrap).show();
            oListWrap.css({top: nTop, left: nLeft}).appendTo($("body")).show();
            $("li", oList).on('click', function(){ //更多列表，列表项点击事件
                var sAct = $(this).attr("act");
                oSelf.oOperate.moreOperate(sAct);
                oSelf.eDraw();
            })
        },
        /**
            关闭单文件操作菜单
            **/
        hideOperateList: function(){
            var oSelf = this,
                oList = $("#operate_list");
            oList.hide();
        },
        /**
            创建左侧信息栏
            **/
        createInfobar: function(){
            var oSelf = this, 
                so = oSelf.oInfobar = {}, oInfoList,
                oFileInfo = $("<div></div>").attr("class","hm"),
                oSetting = {
                    addtime: oLang.addtime,
                    total: oLang.total,
                    size: oLang.size,
                    filenum: oLang.file,
                    dirnum: oLang.directory
                },
                oRemarkBox = $("<div></div>").attr("class","file-remark");
            if(oSelf.setting.type == "fromshare"){ //当模式为“我收到的共享”时，增加共享人
                oSetting.sharer = oLang.sharer    
            }
            if(oSelf.setting.type == "trash"){ //当模式为“回收站”时，增加原路径
                oSetting.sourcepath = oLang.path    
            }
            oInfoList = oSelf.createInfoList(oSetting);
            /* with oSelf.oInfobar */
            so.wrap = $("#"+oSelf.setting.infobarId);
            so.ico = $("<i></i>").attr("class","file-type");
            so.name = $("<div></div>").html("name");
            so.remark = $("<p></p>").html("remark");
            /* info */
            so.ico.appendTo(oFileInfo);
            so.name.appendTo(oFileInfo);
            /* list */
            oInfoList.appendTo(oFileInfo);
            /* remark */
            var oRemarkBtn = $("<a></a>"), oRemarkTitle = $("<h6></h6>");
            oRemarkTitle.html(oLang.file+oLang.remark).appendTo(oRemarkBox);
            oRemarkBtn.attr({"href":"javascript:;", "class":"y i-fileop-remark"}).appendTo(oRemarkTitle);
            so.remark.appendTo(oRemarkBox);
            oRemarkBtn.on("click", function(){ 
                oSelf.oOperate.remark()
            });
            if(oSelf.setting.type == "fromshare"||oSelf.setting.type == "trash"){//当模式为“我收到的共享”时或“回收站”，隐藏侧栏“备注”按钮
                oRemarkBtn.hide()
            }
            oFileInfo.appendTo(so.wrap)
            oRemarkBox.appendTo(so.wrap);
        },
        /**
            创建左侧信息栏文件信息列表
            @return obj 信息列表节点
            **/
        createInfoList: function(setting){
            var oSelf = this, 
                oInfoList = $("<ul></ul>").attr("class","cl");
            for(var i in setting){
                var oAttName = $("<span></span>").html(setting[i]),
                    oAttVal = $("<em></em>");
                oSelf.oInfobar[i] = $("<li></li>").attr("class","cl");
                oSelf.oInfobar[i].append(oAttName, oAttVal)
                oSelf.oInfobar[i].appendTo(oInfoList);
            }
            return oInfoList;
        },
        /**
            显示左侧信息栏
            **/
        showInfobar: function(){
            var oSelf = this,
                aItems = oSelf.oOperate.aSelectedItem,
                l = aItems.length;
            if(l == 0){    //没选中项目时，隐藏信息栏
                oSelf.oInfobar.wrap.hide();
            }else{
                if(l > 1){  //选中多项
                    oSelf.oInfobar.addtime.hide();
                    oSelf.oInfobar.total.hide();
                    oSelf.oInfobar.size.hide();
                    var dnum = 0, fnum = 0, i = 0;
                    for(i; i < l; i++){
                        if(aItems[i].type == "directory"){
                            dnum++;
                        }else{
                            fnum++;
                        }
                    }
                    oSelf.oInfobar.name.html(oLang.multifile);
                    oSelf.oInfobar.ico.attr("class", "file-type o-multi");
                    $("em", oSelf.oInfobar.dirnum).html(dnum + oLang.entry).parent().show();
                    $("em", oSelf.oInfobar.filenum).html(fnum + oLang.entry).parent().show();
                    oSelf.oInfobar.remark.parent().hide(); //隐藏备注
                }else{
                    oSelf.oInfobar.dirnum.hide();
                    oSelf.oInfobar.filenum.hide();
                    $("em", oSelf.oInfobar.addtime).html(aItems[0].addtime).parent().show();
                    oSelf.oInfobar.name.html(aItems[0].filename);
                    oSelf.oInfobar.ico.attr("class", "file-type " + oSelf.getIcon(aItems[0].type));
                    oSelf.oInfobar.remark.html(aItems[0].remark == "" ? oLang.noInfo : aItems[0].remark).parent().show(); //备注为空时，显示“暂无信息”
                    if(aItems[0].type == "directory"){
                        oSelf.oInfobar.size.hide();
                        $("em", oSelf.oInfobar.total).html(aItems[0].total + oLang.entry).parent().show();
                    }else{
                        oSelf.oInfobar.total.hide();
                        $("em", oSelf.oInfobar.size).html(ex.formatSize(aItems[0].size)).parent().show();
                    }
                    if(oSelf.setting.type == "fromshare"){//当模式为“我收到的共享”时，显示共享人
                        $("em", oSelf.oInfobar.sharer).html(aItems[0].user).parent().show();
                    }
                    if(oSelf.setting.type == "trash"){ //当模式为“回收站”时，显示原路径
                        $("em", oSelf.oInfobar.sourcepath).html(aItems[0].sourcepath).parent().show();
                    }
                }
                oSelf.oInfobar.wrap.show();
            }
        },
        /**
            切换为网格视图
            **/
        viewThumb: function(){ 
            var oSelf = this;
            oInitConfig.viewmode = "thumb";
            oSelf.oOperate.refreshList();
            oSelf.oList.removeClass("view-list")
        },
        /**
            切换为列表视图
            **/
        viewList: function(){  //列表视图
            var oSelf = this;
            oInitConfig.viewmode = "list";
            oSelf.oOperate.refreshList();
            oSelf.oList.addClass("view-list");
        },
        /**
            文件选择框
            @param event e 事件对象
            **/
        draw: function(e){
            var oSelf = this,
                oDraw = $("#draw").get(0) ? $("#draw") : $("<div></div>").attr("id","draw").appendTo($("body")),
                sTop = e.clientY, sLeft = e.clientX,
                nScrollTop = $(window).scrollTop(),
                oL = {  //file-list的offset
                    t: oSelf.oList.position().top,
                    l: oSelf.oList.position().left,
                    b: oSelf.oList.position().top + oSelf.oList.height(),
                    r: oSelf.oList.position().left + oSelf.oList.width()
                };
            $(document).on("mousemove", function(ev){
                var iTop = ev.clientY, iLeft = ev.clientX;
                var oDPos  = {  //Draw层的POS
                    t: (iTop > sTop ? sTop : iTop + nScrollTop > oL.t ? iTop : oL.t -nScrollTop ) + nScrollTop, 
                    l:  iLeft > sLeft ? sLeft: iLeft > oL.l ? iLeft : oL.l,
                    h: iTop + nScrollTop < oL.t ?  sTop - oL.t + nScrollTop : iTop + nScrollTop > oL.b ? oL.b - ( sTop + nScrollTop)  : Math.abs(iTop - sTop),
                    w:  iLeft < oL.l ?  sLeft - oL.l : iLeft  > oL.r ? oL.r - sLeft : Math.abs(iLeft - sLeft)
                }
				if(oDPos.w > 2 && oDPos.h> 2){
					oDraw.css({top:oDPos.t,  left:oDPos.l,  height:oDPos.h,  width:oDPos.w}).show();
					$("li", oSelf.oList).each(function(){
						var oIPos = { //各Item的POS
							t: $(this).position().top,
							l: $(this).position().left,
							h: $(this).outerHeight(),
							w: $(this).outerWidth()
						}
						//选中的目标存入选中数组
						if(((oIPos.t <= oDPos.t && oDPos.t <= oIPos.t+oIPos.h)||(oIPos.t <= oDPos.t+oDPos.h && oDPos.t+oDPos.h <= oIPos.t+oIPos.h)||(oDPos.t <= oIPos.t && oIPos.t+oIPos.h <= oDPos.t+oDPos.h))
							&&
						((oIPos.l <= oDPos.l && oDPos.l <= oIPos.l+oIPos.w)||(oIPos.l <= oDPos.l+oDPos.w && oDPos.l+oDPos.w <= oIPos.l+oIPos.w)||(oDPos.l <= oIPos.l && oIPos.l+oIPos.w <= oDPos.l+oDPos.w)))
						{
							if(!$(this).hasClass("selected")){
								oSelf.oOperate.select($(this))
							}
						}else{
							oSelf.oOperate.unSelect($(this))
						}
					})
				}
            })
             $(document).on("mouseup", function(){
                oDraw.hide();
                $("body").noSelect(false)//恢复浏览器文本可选择
                $(document).off("mousemove").off("mouseup");
                oSelf.eDraw();
            })
        },
        /**
            选择结束，根据情况弹出操作栏
            **/
        eDraw: function(){
            var oSelf = this;
            if(oSelf.setting.type != "trash"){ //当模式不为“回收站”，触发操作列表更新
                oSelf.moreList();
            }
            oSelf.showInfobar();
            oSelf.hideOperateList();
        },
        /**
            文件列表中事件绑定
            @param obj list 文件列表节点
            **/
        addListEvent:function(list){    //添加事件
            var oSelf = this,
                nItemNum = oSelf.oOperate.aSelectedItem.length;
            list.on({
				'mousedown': function(e){ //文件列表事件
					$("body").noSelect(); //阻止选择文本
					if(e.target.nodeName == "UL"){ //防止由子节点冒泡触发事件
						oSelf.oOperate.unSelectAll();
					}
					if(e.which !=1)return
					oSelf.draw(e);
				}
			});
            list.on({   //文件项事件
                'mousedown': function(e){
					if(e.shiftKey) {
						return;
					}
                    if($(this).hasClass("selected") && (nItemNum > 1 || e.ctrlKey)){
                        oSelf.oOperate.unSelect($(this));
                    }else {
                        oSelf.oOperate[e.ctrlKey ? 'select' : 'selectOne'](this);
                    }
                },
                'dblclick':function(e){
					var oFile = $(this).data("att");
                    if(oSelf.setting.type == "trash"){
                        oSelf.oOperate.restore(oFile.fid);
                    } else {
                        if(oFile.type == "directory"){
                            oSelf.oOperate.openDir(oFile.fid);
                        } else if($.inArray(ex.checkFileType(oFile.type), ['word', 'excel', 'ppt']) > -1) {
							oSelf.oOperate.openOffice(oFile.fid);
						//只显示浏览器支持的特定格式
						} else if($.inArray(oFile.type, ['png', 'jpeg', 'jpg', 'gif']) > -1) {
							oSelf.oOperate.openImg(oFile.fid);
						}else if(ex.checkFileType(oFile.type, 'text')) {
							oSelf.oOperate.openTxt(oFile.fid);
						} else {
							oSelf.oOperate.download(oFile.fid);
						}
                    }
                },
                'contextmenu': function(e){
                    e.preventDefault();
                    oSelf.oOperate.selectOne($(this));
                    oSelf.operateList(e,1);
                    e.stopPropagation()
                }
            }, 'li');
            $('li', list).on({  //复选框事件
                'click': function(e){
                    if($(this).parents("li").hasClass("selected")){
                        oSelf.oOperate.unSelect($(this).parents("li"));
                    }else{
                        oSelf.oOperate.select($(this).parents("li"));
                    }
                    oSelf.eDraw();
                    e.stopPropagation();
                },
                'mousedown': function(e){
                    e.stopPropagation()
                }
                }, '.i-file-check')
        },
        /**
            读取中提示框
            @param [string "close"] 当无参数时，显示提示框；当参数为“close”时，隐藏提示框
            **/
        showLoading: function(){
            var oSelf = this,
                oWait = $("#wait_wp").length === 0 ? $("<div></div>").attr("id", "wait_wp") : $("#wait_wp"),
                oPos = {
                    left: oSelf.oList.offset().left + oSelf.oList.width()/2 - 50,
                    top: $(document).height()/2 - 15
                };
            if(arguments[0] == "close"){
                oWait.hide();
                return;
            }
            oWait.css(oPos);
            oWait.html("Loading...")
            oWait.appendTo($("body")).show();
        },
        /**
            创建文件夹列表，与文件列表相近，主要用于移动
            @param array aDirData 文件夹项数据
            @return obj 文件夹列表节点
            **/
        createDirList: function(aDirData){
            var oSelf = this,
                oList = oSelf.oDirList = $("<ul></ul>").attr("class", "dir-list cl");
            if(aDirData.length){
                for(var i = 0, l = aDirData.length; i < l; i++){
                    oList.append(oSelf.createDirItem(aDirData[i]));
                }
                $("li", oList).on("dblclick", function(){
                    var oFileData = $(this).data("att"), aPathData = [];
                    oSelf.oDatahandler.getDirFile(oFileData.fid, function(data){
                        var aDirData = oSelf.oDatahandler.filter(oFileData.fid, ['directory']);
                            aPathData = oSelf.oOperate.splitPath(oFileData.fid);
                        oSelf.nDirId = oFileData.fid;
                        oList.replaceWith(oSelf.createDirList(aDirData));
                        oSelf.refreshPath("dialog", aPathData)
                    });
                })
            }
            return oList;
        },
       /**
            创建文件夹列表项，主要用于移动
            @param array oDirData 文件夹项数据
            @return obj 文件夹项节点
            **/
        createDirItem: function(oDirData){
            var oSelf = this,
                oItem = $("<li></li>").data("att",oDirData),
                oIco = $("<i></i>").attr("class","file-type "+ oSelf.getIcon(oDirData.type) + "-lt"),
                oFileName = $("<div></div>").attr({"class": "file-name", "title": oDirData.filename}).html(oDirData.filename),
                oFileDesc = $("<div></div>").attr("class","file-desc").html(oDirData.addtime);
            oItem.append(oIco, oFileName,oFileDesc);
            return oItem;
        },
        /**
            **/
        sameNamePrompt: function(aIds, oData, i, oRenameData){
            var oSelf = this, oDialog = $("#prompt").length > 0 ? $("#prompt") : $("<div></div>").attr("id", "prompt"),
                asHtml = [], oRenameData =  oRenameData||{},
                source = oData.source,
                proposal = oData.proposal,
				target = oData.target,
                nPid = oData.target[0].pid,
                oWrap = $("<div></div>").attr("class", "file-prompt-box cl"),
                oRenameWrap = $("<div></div>").attr("class", "file-prompt-inner cl"),
                oSourceName = $("<p></p>"),
                oProposalName = $("<p></p>"),
                oRenameBtn = $("<button></button>").attr({"type": "button", "class": "btn btn-primary y"}).html("<span>确定</span>"),
                oMergeWrap = $("<div></div>").attr("class", "file-prompt-inner cl"),
                oMergeBtn = $("<button></button>").attr({"type": "button", "class": "btn btn-primary y"}).html("<span>确定</span>"),
				sSourceOldName = source[i].sourcepath + source[i].filename,
				sSourceNewName = source[i].sourcepath + proposal[i],
				sTargetName = target[i].sourcepath + target[i].filename;
            oSourceName.html("<strong>" + sSourceOldName + "</strong>与<strong>" + sTargetName + "</strong>名字相同");
            oProposalName.html("是否将<strong>" + sSourceOldName + "</strong>重新命名为<strong>" + sSourceNewName + "？</strong>");
            oRenameWrap.append(oSourceName, oProposalName, oRenameBtn.appendTo($("<div></div>")));
            if(source[i].type == "directory"){
                oMergeWrap.html("合并这两个文件夹");
            }else{
                oMergeWrap.html("覆盖文件");
            }
            oMergeWrap.append($("<div></div>").append(oMergeBtn));
            oRenameBtn.on("click", function(){
                if(ex.isEmptyObject(oRenameData)){
                    oRenameData[source[i].fid] = proposal[i];
                    for(var n in oRenameData){//重命名后，文件夹下的所有文件跳过检查
                        for(var m = 0; m < source.length; m++){
                            if((source[m].idpath).indexOf("/" + n +"/") > -1){ //当文件的路径中包含重命名文件时，跳过检查
                                source.splice(m, 1);
                                m--;
                               // if(i < source.length - 1){
                                    // oSelf.sameNamePrompt(oData ,i , oRenameData);
                                // }
                               // return
                            }
                        }
                    }
                }
                if(i < source.length - 1){
                    i++;
                    oSelf.sameNamePrompt(aIds, oData ,i , oRenameData);
                }else{
                    oSelf.oDatahandler.move(aIds, nPid, function(result){
                        if(+result.error === 0){
                            oDialog.dialog("close")
                            oSelf.oOperate.refresh();
                            ex.showMsg(result.message, "success")
                        }else{
                            ex.showMsg(result.message, "error")
                        }
                    }, oRenameData) 
                   // return oRenameData;
                }
            });
            oMergeBtn.on("click", function(){
                if(i < source.length - 1){
                    i++;
                    oSelf.sameNamePrompt(aIds, oData ,i , oRenameData);
                }else{
                    oSelf.oDatahandler.move(aIds, nPid, function(result){
                        if(+result.error === 0){
                            oDialog.dialog("close")
                            oSelf.oOperate.refresh();
                            ex.showMsg(result.message, "success")
                        }else{
                            ex.showMsg(result.message, "error")
                        }
                    })
                }
            })
            oWrap.append(oRenameWrap, oMergeWrap);
                oDialog.iDialog({
                    title: "命名冲突",
                    modal: true,
                    height: 'auto',
                    width: 600,
                    html: oWrap,
                    onClose: function(){
                        return false;
                    }
                })
         },
		 getIcon: function(sFileType) {
			var sType = ex.checkFileType(sFileType);
			//图标类型调整
			var oIcoSet = {
				'word': 'doc',
				'excel': 'xls',
				'ppt': 'ppt',
				'program': 'exe',
				'video': 'video'
			}
			var sIcon = oIcoSet[sType] ? oIcoSet[sType] : sFileType;
			return 'o-' + sIcon;
		 }
   }

/*--------------------  操作 --------------------*/
    function Operate(oView,oDatahandler){
        this.oView = oView;
        this.oDatahandler = oDatahandler;
        this.aSelectedItem = [];
    }
    Operate.prototype = {
        /**
            单项选择，不叠加选择，当选择一项时，会清空其他已选项
            @param obj o 选择项节点
            **/
        selectOne: function(o){
            var oSelf = this;
            oSelf.aSelectedItem = [];
            $(o).siblings().attr("class","");
            oSelf.select(o);
        },  
        /**
            单项选择，叠加选中,基本选择函数
            @param obj o 选择项节点
            **/
        select: function(o){ 
            var oSelf = this;
            $(o).attr("class", "selected").find("input[type=checkbox]").attr("checked", true);
			oSelf.oView.btns.allcheck.find('i').attr("class", "i-file-check");
            oSelf.aSelectedItem.push($(o).data('att'));
        },
        /**
                单项取消选中
                @param obj o 取消选择的节点
            **/
        unSelect: function(o){ 
            var oSelf = this;
            $(o).attr("class", "").find("input[type=checkbox]").attr("checked", false);
            $("i", oSelf.oView.btns.allcheck).attr("class","i-file-check")
            for(var i = 0, l = oSelf.aSelectedItem.length; i < l; i++){
                if(oSelf.aSelectedItem[i] == $(o).data('att')){
                    oSelf.aSelectedItem.splice(i,1)
                }
            }
        },
        /**
            全选，选择当前页所有文件
            **/
        selectAll: function(){
            var oSelf = this;
            $("li", oSelf.oView.oList).each(function(){
                oSelf.select($(this))
            });
            oSelf.oView.eDraw();
			$("i", oSelf.oView.btns.allcheck).attr("class", "i-file-checked");
        },
        /**
            全不选，取消所有选择文件
            **/
        unSelectAll: function(){
            var oSelf = this;
            $("li", oSelf.oView.oList).each(function(){
                oSelf.unSelect($(this))
            });
            oSelf.oView.eDraw();
			$("i", oSelf.oView.btns.allcheck).attr("class", "i-file-check");
        },
        /**
            更多操作跳转函数
            @param string act 操作类型[download, remark ,share, move, rename, del]
            **/
        moreOperate: function(act){
            var oSelf = this;
            switch(act){
                case "open":
                    oSelf.openDir(oSelf.aSelectedItem[0].fid);
                    break;
                case "download":
                    oSelf.download();
                    break;
                case "remark":
                    oSelf.remark();
                    break;
                case "share":
                    oSelf.share();
                    break;
                case "move":
                    oSelf.move();
                    break;
                case "rename":
                    oSelf.rename();
                    break;
                case "del":
                    if(oSelf.oView.setting.type == "trash"){
                        oSelf.destory();
                    }else{
                        oSelf.del();
                    }
                    break;
            }
        },
       /**
            文件下载
        **/
        download: function(){
            var oSelf = this, oDialog = $("#d_download").length == 0 ?  $("<div></div>").attr("id","d_download") : $("#d_download"),
                aItems = oSelf.aSelectedItem,
                aIds = ex.getFileIds(aItems),
                asHtml = [];
            asHtml.push("<div class='file-download-box'><div class='hm'>");
            if(aItems.length == 1){
                asHtml.push(aItems[0].filename);
            }else{
                asHtml.push(oLang.download + oLang.selected + oLang.file +"?");
            }
            asHtml.push("</div></div>");
            asHtml = asHtml.join("");
            oDialog.iDialog({
                title: oLang.downloadStart,
                modal: true,
                height: 'auto',
                html: asHtml,
                submit: function(){
					oSelf.oDatahandler.download(aIds);
                    oDialog.dialog("close");
				}
            })
        },
        /**
            文件备注
            **/
        remark: function(){
            var oSelf = this, oDialog = $("#d_remark").length == 0 ?  $("<div></div>").attr("id","d_remark") : $("#d_remark"),
                oItem = oSelf.aSelectedItem[0],
                asHtml = [];
            asHtml.push("<div class='file-remark-box'><textarea name='fileremak' id='file_remark'>" +  oItem.remark + "</textarea></div>");
            asHtml.join("");
			var fnSubmit = function() {
				oDialog.dialog('close');
				var sRemark = $("#file_remark").val();
				oSelf.oView.showLoading();
				oSelf.oDatahandler.remark(oItem.fid,sRemark, function(result){
					oSelf.oView.showLoading("close");
					if(result.error === 0){
						ex.showMsg(result.message, "success")
					}else{
						ex.showMsg(result.message, "error")
					}
				})
			}
            oDialog.iDialog({
                title: oLang.remark,
                modal: true,
                height: 'auto',
                html: asHtml,
                submit: function(){
					fnSubmit();
                },
				onOpen: function() {
					setTimeout(function() {
						$("#file_remark").focus();
						$("#file_remark").keydown(function(e) {
							if(e.which == 13) {
								fnSubmit();
							}
						});
					}, 150);
				}
            })
        },
        /**
            文件共享
            **/
        share: function(){
            var oSelf = this, oDialog = $("#d_share").length == 0 ?  $("<div></div>").attr("id","d_share") : $("#d_share"),
                oItem = oSelf.aSelectedItem[0],
                asHtml = [];
            asHtml.push("<div class='input-append input-prepend file-share-box' ><p>" + oLang.select + oLang.staff );
            asHtml.push("</p><input id='sharetouser' class='span9' type='hidden'  value='"+ (oItem.touids ? oItem.touids : "") +"'/><button type='button' href='misc.php?mod=getuser' class='btn add-on' onclick='getUser(this,\"sharetouser\",\"sharetouser_display\")'>");
            asHtml.push("<i class='i_g i-add-staff'></i></button><button type='button' class='btn add-on' onclick='ClearUser(\"sharetouser\")'><i class='i_g i-clear'></i></button></div>");
            asHtml.push("<div class='input-append input-prepend file-share-box'  ><p>" + oLang.select + oLang.department);
            asHtml.push("</p><input id='sharetodept' class='span9' type='hidden' value='"+ (oItem.todeptids ? oItem.todeptids: "") +"'/><button type='button' href='misc.php?mod=getdept' class='btn add-on' onclick='getDept(this,\"sharetodept\",\"sharetodept_display\")'>");
            asHtml.push("<i class='i_g i-add-department'></i></button><button type='button' class='btn add-on' onclick='ClearDept(\"sharetodept\")'><i class='i_g i-clear'></i></button></div>");
            asHtml.push("<div class='input-append input-prepend file-share-box'  ><p>" + oLang.select + oLang.position);
            asHtml.push("</p><input id='sharetopos' class='span9' type='hidden' value='"+ (oItem.toposids ? oItem.toposids : "") +"'/><button type='button' href='misc.php?mod=getpos' class='btn add-on' onclick='getPosition(this,\"sharetopos\",\"sharetopos_display\")'>");
            asHtml.push("<i class='i_g i-add-department'></i></button><button type='button' class='btn add-on' onclick='ClearPos(\"sharetopos\")'><i class='i_g i-clear'></i></button></div>");
            asHtml = asHtml.join("");
            oDialog.iDialog({
                title: oLang.share,
                modal: true,
                height: 'auto',
                html: asHtml,
                onOpen: function(){
                    setTimeout(function(){
                        $("#sharetouser").select2({
                            data: gStaffData,
                            multiple: true, 
                            iteration: true,
                            initSelection: function(element){
                                return  _initFormSelection(element);
                            }
                        });
                        $("#sharetodept").select2({
                            data: gDeptData,
                            multiple: true, 
                            iteration: true,
                            initSelection: function(element){
                                return  _initFormSelection(element, "dept");
                            }
                        });
                        $("#sharetopos").select2({
                            data: gPosData,
                            multiple: true, 
                            iteration: true,
                            initSelection: function(element){
                                return  _initFormSelection(element, "pos");
                            }
                        });
                    }, 150)
                },
                submit: function(){
                    var sUserValue = $("#sharetouser").val(),
                        sDeptValue = $("#sharetodept").val(),
                        sPosValue = $("#sharetopos").val(),
                        aItems = oSelf.aSelectedItem,
                        aIds = [],
                        oIdList = {
                            uids: sUserValue,
                            deptids: sDeptValue,
                            posids: sPosValue
                        };
                     for(var i = 0, l = aItems.length; i < l; i++){
                        aIds.push(aItems[i].fid)
                     }
                     if(!oIdList.uids && !oIdList.deptids && !oIdList.posids){
                        oSelf.oDatahandler.shareCancel(aIds, function(result){
                            if(+result.error === 0){
                                oDialog.dialog("close")
                                oSelf.refreshList();
                                ex.showMsg(result.message, "success")
                            }else{
                                ex.showMsg(result.message, "error")
                            }
                        })
                     }else{
                        oSelf.oDatahandler.share(aIds, oIdList, function(result){
                            if(+result.error === 0){
                                oDialog.dialog("close");
                                oSelf.refreshList();
                                ex.showMsg(result.message, "success")
                            }else{
                                ex.showMsg(result.message, "error")
                            }
                        })
                     }
                }  
            })
        },
        /**
            文件移动，可多文件操作
            **/
        move: function(){
            var oSelf = this, oDialog = oDialog = $("#d_move").length == 0 ?  $("<div></div>").attr("id","d_move") : $("#d_move"),
                 oDirList,  aDirData = oSelf.oDatahandler.filter(oInitConfig.fid, ['directory']),
                aItem = oSelf.aSelectedItem,
                aIds = ex.getFileIds(aItem),
                oWrap = $("<div></div>").attr("class", "file-move-box"),
                oPath = oSelf.oView.createPath("dialog"),
                oDirList= oSelf.oView.createDirList(aDirData);
            oSelf.oView.nDirId = oInitConfig.fid;
            oWrap.append(oPath, oDirList);
            oDialog.iDialog({
                title: oLang.move,
                modal: true,
                height: 'auto',
                width: 600,
                html: oWrap,
                submit: function(){
					oDialog.dialog("close");
					oSelf.oView.showLoading();
                    var nPid = oSelf.oView.nDirId;
                    if($.inArray(nPid, aIds) != -1){
						oSelf.oView.showLoading("close");
                        ex.showMsg(oLang.errorMoveToSelf, "error")
                        return false;
                    }
                    if(aItem[0].pid == nPid){
						oSelf.oView.showLoading("close");
                        ex.showMsg(oLang.errorNoMove, "error")
                        return false;
                    }
                    oSelf.oDatahandler.checkMoveSameNames(aIds, nPid, function(result){
                        if(+result.error === 0){
                            var oData = result.data;
                            if(oData.proposal.length > 0){
                                oSelf.oView.sameNamePrompt(aIds, oData, 0);
								oSelf.oView.showLoading("close");
								return;
                            }
							oSelf.oDatahandler.move(aIds, nPid, function(result){
								if(+result.error === 0){
									oSelf.oDatahandler.refreshDir(oSelf.oDatahandler.getFile([aIds[0]])[0].pid || 0, function() {
										oSelf.oDatahandler.refreshDir(nPid || 0, function() {
											oSelf.refresh();
											oSelf.oView.showLoading("close");
											ex.showMsg(result.message, "success")
										});
									});
								} else {
									oSelf.oView.showLoading("close");
									ex.showMsg(result.message, "error")
								}
							})
                        }else{
							oSelf.oView.showLoading("close");
                            ex.showMsg(result.message, "error")
                        }
                    })
                }
            })
        },
        /**
            文件重命名
            **/
        rename: function(){
            var oSelf = this, oDialog = oDialog = $("#d_rename").length == 0 ?  $("<div></div>").attr("id","d_rename") : $("#d_rename"),
                oItem = oSelf.aSelectedItem[0],
                asHtml = [];
            asHtml.push("<div class='file-rename-box'><input type='text' name='filerename' id='file_name' value='" + oItem.filename + "' class='span11' /></div>");
            asHtml = asHtml.join("");
			var fnSubmit = function(){
				oDialog.dialog("close");
				var oRename = $("#file_name").val();
				if(oRename === oItem.filename){
					oSelf.oView.showLoading("close");
					return false;
				}
				oSelf.oView.showLoading();
				oSelf.oDatahandler.rename(oItem.fid, oRename, function(result){
					oSelf.oView.showLoading("close");
					if(result.error === 0){
						oItem.self.name.html(oRename);
						oItem.self.name.attr('title', oRename);
						ex.showMsg(result.message, "success")
					}else{
						ex.showMsg(result.message, "error")
					}
				})
			}
            oDialog.iDialog({
                title: oLang.rename,
                modal: true,
                height: 'auto',
                html: asHtml,
                submit: function(){
                    fnSubmit();
                },
				onOpen: function() {
					setTimeout(function() {
						$("#file_name").focus();
						$("#file_name").keydown(function(e) {
							if(e.which == 13) {
								fnSubmit();
							}
						});
					}, 150);
				}
            })
        },
        /**
            文件删除，可多文件操作
            **/
        del: function(){
            var oSelf = this, oDialog = oDialog = $("#d_del").length == 0 ?  $("<div></div>").attr("id","d_del") : $("#d_del"),
                aItems = oSelf.aSelectedItem,
                asHtml = [];
			if(aItems.length == 0) {return;}
            var str = aItems.length > 1 ? oLang.toDel + oLang.multiFile : oLang.toDel + "&nbsp;" + aItems[0].filename;
            asHtml.push("<div class='file-del-box'><p class='hm'>"+ str +"?</p></div>");
            asHtml.join("0");
            oDialog.iDialog({
                title: oLang.del,
                modal: true,
                height: 'auto',
                html: asHtml,
                submit: function(){
					oDialog.dialog("close");
					oSelf.oView.showLoading();
                    var aIds = ex.getFileIds(aItems);
                    oSelf.oDatahandler.remove(aIds, function(result){
						oSelf.oView.showLoading("close");
                        if(+result.error === 0){
                            oSelf.refresh();
                            ex.showMsg(result.message, "success")
                        }else{
                            ex.showMsg(result.message, "error")
                        }
                    })
                }
            })
        },
        /**
            上传文件
            **/
        upload: function(){
            var oSelf = this, oDialog = $("#d_upload").length == 0 ?  $("<div></div>").attr("id","d_upload") : $("#d_upload");
            oDialog.iDialog({
                title: oLang.upload,
                height: 'auto',
                model: true,
                href: "template/default/desktop/file_upload.html",
                onLoad: function(){
                    var IMGDIR = 'static/image/common';
					var oSwfconfig = oSelf.oView.setting.swfconfig;
                    //注意,这个JS要在上传操作的所有控件之后,即所有涉及的ID等要先于这个控件出现
                    var upload = new SWFUpload({
                        // Backend Settings
                        upload_url: "misc.php?mod=swfupload&action=swfupload&operation=file&pid=" + oInitConfig.fid ,
                        post_params: {"uid" : oSelf.oView.setting.uid, "hash": oSwfconfig.hash},
                    
                        file_size_limit : oSwfconfig.max,
                        file_types : oSwfconfig.attachexts.ext,
                        file_types_description : oSwfconfig.attachexts.depict,
                        file_upload_limit : 0,
                        file_queue_limit : 0,
                    
                        // Event Handler Settings (all my handlers are in the Handler.js file)
                        swfupload_preload_handler : preLoad,
                        swfupload_load_failed_handler : loadFailed,
                        file_dialog_start_handler : fileDialogStart,
                        file_queued_handler : fileQueued,
                        file_queue_error_handler : fileQueueError,
                        file_dialog_complete_handler : fileDialogComplete,
                        upload_start_handler : uploadStart,
                        upload_progress_handler : uploadProgress,
                        upload_error_handler : uploadError,
                        upload_success_handler : uploadSuccess,
                        upload_complete_handler : uploadComplete,
                    
                        // Button Settings
                        button_image_url : IMGDIR+"/uploadbutton.png",
                        button_placeholder_id : "imgSpanButtonPlaceholder",
                        button_width: 100,
                        button_height: 25,
                        button_cursor:SWFUpload.CURSOR.HAND,
                        button_text_left_padding:16,
                        button_text_style: '.button { vertical-align: middle; overflow: hidden; }',
                        //button_text: '<span class="button">上传</span>',
                        button_text: '',
                        button_window_mode: "transparent",
                    
                        // Flash Settings
                        flash_url : IMGDIR+"/swfupload.swf",
                        flash9_url : IMGDIR+"/swfupload_fp9.swf",
                    
                        custom_settings : {
                            progressTarget : "imgUploadProgress",
                            uploadSource: 'file',
                            uploadType: 'file',
                            imgBoxObj: _id('attach_menu_ul'),
                            attachInput: _id('attachmentid')
                        },
                        // Debug Settings
                        debug: false
                    });
                },
                onClose: function(){
                    oSelf.refresh();
                }
            })
        },
        /**
            工具栏全选按钮联动
            **/
        allCheck: function(bCheck){
            var oSelf = this, 
                oIco = $("i", oSelf.oView.btns.allcheck);
            if(bCheck === false || (bCheck === undefined && oIco.hasClass("i-file-checked"))){
                oSelf.unSelectAll();
            }else{
                oSelf.unSelectAll();
                oSelf.selectAll();
            }
        },
        /**
            当不为顶层时返回上一层
            **/
        back: function(){
            var oSelf = this;
            if(oInitConfig.fid == 0){
                ex.showMsg(oLang.errorIsTop, 'notice');
                return
            }else{
                var aPathData = oInitConfig.aPathData, nPid;
                aPathData.pop()
                nPid = aPathData[aPathData.length-1].fid;
                oSelf.openDir(nPid)
            }
        },
        /**
            新建
            @param string act 新建类型 [directory, doc, xml, ppt]
            **/
        add: function(act){
            var oSelf = this, oDialog = $("#d_add").length == 0 ?  $("<div></div>").attr("id","d_add") : $("#d_add"),
                asHtml = [];
			var sTitle = ({'directory': oLang.directory, 'doc': 'word', 'xls': 'excel', 'ppt': 'ppt'})[act];
            asHtml.push("<div class='file-new-box cl'><i class='file-type z o-"+act+"'></i>");;
            asHtml.push("<input type='text' id='file_new' name='new"+act+"'/></div>");
            asHtml = asHtml.join("");
			var fnSubmit = function(){
				oDialog.dialog("close");
				var sNewValue = $("#file_new").val();
				oSelf.oView.showLoading();
				if(act == "directory"){
					oSelf.oDatahandler.mkDir(oInitConfig.fid, sNewValue, function(result){
						oSelf.oView.showLoading("close");
						if(+result.error === 0){
							ex.showMsg(result.message, "success")
						}else{
							ex.showMsg(result.message, "error")
						}
						oSelf.openDir(result.data.fid)
					})
				} else if($.inArray(act, ['doc', 'ppt', 'xls']) > -1) {
					oSelf.oDatahandler.mkOffice(oInitConfig.fid, act, sNewValue, function(result) {
						oSelf.oView.showLoading("close");
						if(+result.error === 0) {
							oSelf.refresh();
							oSelf.openOffice(result.data.fid, true);
							ex.showMsg(result.message, "success");
						} else {
							ex.showMsg(result.message, "error")
						}
					});
				}
				oDialog.dialog("close");
			}
            oDialog.iDialog({
                title: oLang.add + sTitle,
                modal: true,
                height: 'auto',
                html: asHtml,
                submit: function(){
					fnSubmit();
                },
				onOpen: function() {
					setTimeout(function() {
						$("#file_new").focus();
						$("#file_new").keydown(function(e) {
							if(e.which == 13) {
								fnSubmit();
							}
						});
					}, 150);
				}
            })
        },
		
        /** 
            排序， 初始排序为文件名正序，文件夹默认在前
            @param string sAct 排序依据[filename, addtime, size]
            @param string sOrder 正倒序 [ASC, DESC]
            **/
        sort: function(sAct, sOrder){
            var oSelf = this;
            oInitConfig.aSort = [sAct];
            oInitConfig.sOrder = sOrder;
            oSelf.refreshList();
        },
        /**
            刷新操作
            **/
        refresh: function(){
            var oSelf = this,
                oFileInfo,
                oFileList;
            oSelf.oView.showLoading()
            oSelf.oDatahandler.refreshDir(oInitConfig.fid, 
            function(){
                oSelf.oDatahandler.getDirFile(oInitConfig.fid, function(data){
					var nOldPage = oInitConfig.nPage;
					oSelf.oView.setLocalData(data);
					 oInitConfig.nPage = nOldPage;
                    oSelf.refreshList();
                });
                oSelf.oView.refreshPath();
                oSelf.oView.showLoading("close");
            })
        },
         /**
            刷新列表
            **/
        refreshList: function(data){
            var oSelf = this;
            oSelf.oView.createFileList(data);
            $(".file-list").replaceWith(oSelf.oView.oList);
            oSelf.initSelect();
        },
        /** 
            搜索文件
            @param string sKeyword 搜索关键字
            **/
        search: function(sKeyword){
            var oSelf = this,
                nPid = oInitConfig.fid;
			oSelf.oView.showLoading();
            oSelf.oDatahandler.search(nPid, sKeyword, function(result){
				oSelf.oView.showLoading("close");
                oSelf.oView.setLocalData(result.data);
                oSelf.refreshList();
                oSelf.oView.refreshPath('', [{fid: 0, filename: oLang[oSelf.oView.setting.type]},{filename: "搜索结果"}]);
            })
        },
        /**
            彻底删除文件，从回收站中删除
            **/
        destory: function(){
            var oSelf = this, oDialog = $("#d_destory").length == 0 ?  $("<div></div>").attr("id","d_destory") : $("#d_destory");
            oDialog.iDialog({
                title: "删除文件",
                model: true,
                height: 'auto',
                html: '<div class="file-destory-box hm">确定彻底删除文件所选文件?</div>',
                submit: function(){
					oDialog.dialog("close");
                    var aFileData = oSelf.aSelectedItem,
                        aFids = ex.getFileIds(aFileData);
					oSelf.oView.showLoading();
                    oSelf.oDatahandler.remove(aFids, function(result){
						oSelf.oView.showLoading("close");
                        if(+result.error === 0){
                            oSelf.refresh();
                            ex.showMsg(result.message,"success");
                        }else{
                            ex.showMsg(result.message,"error");
                        }
                    }, 'trash')
                }
            })
        },
        /**
            清空回收站
            **/
        clear: function(){
            var oSelf = this, oDialog = $("#d_clear").length == 0 ?  $("<div></div>").attr("id","d_clear") : $("#d_clear");
            oDialog.iDialog({
                title: oLang.clear,
                model: true,
                height: 'auto',
                html: '<div class="file-clear-box hm">' + oLang.clear + oLang.trash + '?</div>',
                submit: function(){
					oDialog.dialog("close");
                    if(oSelf.oView.getLocalData().length == 0){
						oSelf.oView.showLoading("close");
                        return;
                    }
					oSelf.oView.showLoading();
                    oSelf.oDatahandler.trashClearAll(function(result){
						oSelf.oView.showLoading("close");
                        if(+result.error === 0){
                            oSelf.refresh();
                            oDialog.dialog("close");
                            ex.showMsg(result.message,"success");
                        }else{
                            ex.showMsg(result.message,"error");
                        }
                    })
                }
            })
        },
        /**
            还原回收站的文件
            **/
        restore: function(){
            var oSelf = this,
                aItems = oSelf.aSelectedItem,
                aIds = ex.getFileIds(aItems),
                //oRestore = new Datahandler("myfile"),
                oDialog = $("#d_restore").length == 0 ?  $("<div></div>").attr("id","d_restore") : $("#d_restore"),
                oWrap = $("<div></div>").attr("class", "file-move-box"),
                oPath, aDirData, oDirList;
            oSelf.oView.setting.type = oSelf.oDatahandler.sType = "myfile";
            oSelf.oDatahandler.oFileItem[0].filename = oLang.myfile;
            oPath = oSelf.oView.createPath("dialog");
            oSelf.oDatahandler.clearLocalDir(0);
            oSelf.oDatahandler.getDirFile(0,function(data){
                aDirData =  oSelf.oDatahandler.filter(data, ['directory'])
                oDirList = oSelf.oView.createDirList(aDirData);
                oWrap.append(oPath, oDirList);
            })
            oDialog.iDialog({
                title: oLang.restore,
                model: true,
                height: 'auto',
                width: 600,
                html: oWrap,
               // html: '<div class="file-clear-box hm">' + oLang.restore + oLang.selected + oLang.file + '?</div>',
                submit: function(){
					oDialog.dialog("close");
                    var nPid = oSelf.oView.nDirId;
					oSelf.oView.showLoading();
                    oSelf.oDatahandler.restoreFromTrash(aIds, nPid, function(result){
						oSelf.oView.showLoading("close");
						oSelf.oDatahandler.getDirFile(0,function(data){});
						oSelf.refresh();
                        if(+result.error === 0){
                            ex.showMsg(result.message,"success");
                        }else{
                            ex.showMsg(result.message,"error");
                        }
                    })
                },
				onClose: function() {
					oSelf.oView.setting.type = oSelf.oDatahandler.sType = "trash";
					oSelf.oDatahandler.oFileItem[0].filename = oLang.trash;
					oSelf.oDatahandler.clearLocalDir(0);
				}
            })
        },
        /** 
            标记/取消标记 文件
            @param obj o 标记触发节点
            @param obj setting 需标记文件所在节点
            **/
        mark: function(o,setting){
            var oSelf = this,  nMark; //操作结果 boolean
            if(!o.hasClass("marked")){
                nMark = 1;
            }else{
                nMark = 0
            }
            oSelf.oDatahandler.mark(setting.data("att").fid, nMark, function(result){
                if(+result.error === 0){
                    if(nMark == 0){
                        o.removeClass("marked");
                    } else {
                        o.addClass("marked");
                    }
                }else{
                    ex.showMsg(result.message, "error")
                }
            }) 
        },
        /**
            筛选文件 
            @param string sAct 筛选类型[mark, all, word, excel, ppt, text, image,pickage,audio, video]
            **/
        sift: function(sAct){
            var oSelf = this, aAct = [];
            aAct.push(sAct);
       //     var aPathData = oSelf.oDatahandler.filter(oInitConfig.fid, aAct);
            oInitConfig.aFilter = aAct;
            oInitConfig.nPage = 1;
           // var aFileData = oSelf.oDatahandler.formatData(oSelf.oView.aLocalData, oInitConfig);
            oSelf.oDatahandler.getDirFile(oInitConfig.fid, function(data){
                oSelf.oView.setLocalData(data);
				oSelf.refreshList();
            });
        },
        /**
            打开文件夹
            @param number nFid 要打的文件夹的ID
            @param string sMode 对应模式[window, dialog], 
            **/
        openDir: function(nFid, sMode){
            var oSelf = this,
                sMode = sMode||"window",
                oFileData = oSelf.oDatahandler.getFile([nFid])[0];
			oSelf.oView.showLoading();
			var fnOpenDir = function(fnCallBack) {
				if(sMode !="dialog"){
					oInitConfig.aPathData = [];
					if(nFid != 0){
						oInitConfig.aPathData = oSelf.splitPath(oFileData.fid);
					}
					oInitConfig.fid = nFid;
					oSelf.oDatahandler.getDirFile(oInitConfig.fid, function(data){
						oSelf.oView.showLoading('close');
						var nOldPage = oInitConfig.nPage;
						oSelf.oView.setLocalData(data);
						oInitConfig.nPage = nOldPage;
						oSelf.refreshList();
					});
					oSelf.oView.refreshPath();
				}
			}
			if(!oFileData) {
				oSelf.oDatahandler.loadFile(nFid, function(oResult) {
					if(+oResult.error === 0) {
						oFileData = oSelf.oDatahandler.getFile([nFid])[0];
						fnOpenDir();
					} else {
						oSelf.oView.showLoading('close');
					}
				});
			} else {
				fnOpenDir();
			}
        },
		
		/**
		 * 打开办公文件
		 * @param int nFid
		 * @param boolean bShowDialog 是否显示窗口
		 */
		openOffice: function(nFid, bShowDialog) {
			var oSelf = this,
				oFile = oSelf.oDatahandler.getFile([nFid])[0],
				oAttachData = oFile.attachdata,
				sUrl = null;
			if(oAttachData) {
				//判断编辑还是查看
				if(+oAttachData.edit) {
					sUrl = oAttachData.officeediturl;
				} else if(oAttachData.down_office) {
					sUrl = oAttachData.officereadurl;
				}
			}
			if(sUrl) {
				sUrl += '&fromfile=1&fid=' + nFid;
				if(bShowDialog) {
					var oDialog = $("#d_showoffice").length == 0 ?  $("<div></div>").attr("id","d_showoffice") : $("#d_showoffice"),
						asHtml = []; 
					asHtml.push('<div class="file-openoffice-box"><div class="hm">');
					asHtml.push(oLang.immediatelyTo + ' ' + oFile.filename + ' ' + oLang.proceedEdit + '？');
					asHtml.push('</div></div>');
					oDialog.iDialog({
						title: oLang.tips,
						modal: true,
						height: 'auto',
						html: asHtml.join(''),
						submit: function(){
							window.open(sUrl);
							oDialog.dialog("close");
						}
					})
				} else {
					window.open(sUrl);
				}
			} else {
				ex.showMsg(oLang.loadOfficeError, "error")
			}
		},
		
		/**
		 * 打开图片
		 * @param int nFid 图片文件id
		 */
		openImg: function(nFid) {
			var oSelf = this,
				fnCallee = arguments.callee,
				oCache = fnCallee.oCache || {};
			var fnShowImg = function(sTitle, sUrl) {
				var oImg = new Image();
				oImg.src = sUrl;
				oImg.title = sTitle;
				oImg.alt = sTitle;
				zoom(oImg);
			}
			oSelf.oView.showLoading();
			var sFilename = oSelf.oDatahandler.getFile([nFid])[0].filename;
			if(oCache[nFid]) {
				fnShowImg(sFilename, oCache[nFid]);
				oSelf.oView.showLoading('close');
			} else {
				oSelf.oDatahandler.getAttachData(nFid, function(oResult) {
					if(+oResult.error == 0) {
						oCache[nFid] = oResult.data.attachment;
						fnShowImg(sFilename, oResult.data.attachment);
					} else {
						ex.showMsg(oResult.message, "error");
					}
					oSelf.oView.showLoading('close');
				});
			}
			fnCallee.oCache = oCache;
		},
		
		/**
		 * 打开文本
		 * @param int nFid 图片文件id
		 */
		openTxt: function(nFid) {
			var oSelf = this,
				fnCallee = arguments.callee,
				oCache = fnCallee.oCache || {};
			var fnShowTxt = function(sTitle, sContent) {
				var oDialog = $("#d_showcontent").length == 0 ?  $("<div></div>").attr("id","d_showcontent") : $("#d_showcontent");
				oDialog.iDialog({
					title: sTitle,
					modal: true,
					height: 'auto',
					width: 700,
					html: '<p class="file-show-txt">' + sContent + '</p>'
				});
			}
			oSelf.oView.showLoading();
			var sFilename = oSelf.oDatahandler.getFile([nFid])[0].filename;
			if(oCache[nFid]) {
				fnShowTxt(sFilename, oCache[nFid]);
				oSelf.oView.showLoading('close');
			} else {
				oSelf.oDatahandler.getAttachContent(nFid, function(oResult) {
					if(+oResult.error == 0) {
						oCache[nFid] = oResult.data;
						fnShowTxt(sFilename, oResult.data);
					} else {
						ex.showMsg(oResult.message, "error");
					}
					oSelf.oView.showLoading('close');
				});
			}
			fnCallee.oCache = oCache;
		},
		
        /**
            将文件所在路径转化为数组，格式如[{fid:0, filename:"我的文件柜"},{fid: 11, filename:"我的音乐"}]
            @param number fid 文件ID
            @explain 当fid为0时，即为最上层文件夹，此时会返回空数组
            @return array 文件路径数组
           **/
        splitPath: function(fid){
            var oSelf = this, 
                oFileData = oSelf.oDatahandler.getFile([fid])[0],
                aPathData = [];
            if(+fid !== 0){
				var aPathIds = oFileData.idpath.replace(/(^\/0\/|\/$)/, '').split('/'),
					aSourcePath = oFileData.sourcepath.replace(/(^\/|\/$)/, '').split('/'),
					oRoot = oSelf.oDatahandler.getFile([0])[0];
				aPathData.push({fid: oRoot.fid, filename: oRoot.filename});
                $(aPathIds).each(function(i) {
					if(this.valueOf()) {
						aPathData.push({fid: this.valueOf(), filename: aSourcePath[i]});
					}
                });
				aPathData.push({fid: oFileData.fid, filename: oFileData.filename});
            }
            return aPathData;
        },
        /**
            初始化选择数据
            **/
        initSelect: function(){
            var oSelf = this;
            oSelf.aSelectedItem = [];
            oSelf.oView.eDraw();
			oSelf.allCheck($('i', oSelf.oView.btns.allcheck).hasClass('i-file-checked'));
        }
   }

/*--------------------  数据控制 --------------------*/
    function Datahandler(sType){
		this.sType = sType || 'myfile';
        //储存文件数据的对象，该对象包含所有文件和文件夹的真实数据
		//数据格式是 { fid1 : {}, fid2 : {} ...}
        this.oFileItem = {
            //根目录是一个特殊的文件夹，它一直存在
			0: {fid: 0, type: "directory", filename: oLang[sType]}
        }
	}
	Datahandler.prototype = {
		/**
		* 刷新当前文件夹的数据
		* @param int nFid 文件夹id
		* @param function fnCallBack( oResult: 服务器返回的数据 )
		*/
		refreshDir: function(nFid, fnCallBack) {
			this.loadDirFile(nFid, fnCallBack);
		}

		/**
		* 加载数据
		* @param int nFid 文件夹id
		* @param function fnCallBack( oResult: 服务器返回的数据 )
		*/
		, loadDirFile: function(nFid, fnCallBack, sType) {
			var oSelf = this;
			ex.post({'act': 'loaddirfile'}, {
				type: sType||oSelf.sType,
				fid: nFid
			}, function(oResult) {
				if(+oResult.error === 0) {
					//删除文件数据
					var nSubFids = oSelf.dirCache(nFid);
					$.each(nSubFids, function(i) {
						delete oSelf.oFileItem[ nSubFids[i] ];
					});
					//记录缓存
					oSelf.dirCache(nFid, 'set', $.map(oResult.data, function(oFile) {
						oSelf.oFileItem[ oFile.fid ] = oFile;
						return oFile.fid;
					}));
					oSelf.oFileItem[nFid].isload = true;
				}
				if(typeof fnCallBack == 'function') {
					fnCallBack(oResult);
				}
			}, 'json');
		}
		
		
		/**
		 * 获取一个文件夹下的数据
		 * 取消快速格式化数据参数，因为这样做为视图类带来很多问题，必需调用formatData函数格式化
		 * @param int nFid
		 * @param function fnCallBack( aFilelist: 文件数据列表 | false )
		 */
		, getDirFile: function(nFid, fnCallBack) {
			var oSelf = this,
				oFileItem = oSelf.oFileItem,
				oDir = oFileItem[nFid],
				aFilelist = [];
			//已经加载的数据直接处理返回
			if(oDir && oDir.isload) {
				var aSubFids = oSelf.dirCache(nFid);
				$.each(aSubFids, function(i) {
					var nSubId = this.valueOf();
					aFilelist.push(oFileItem[ nSubId ]);
				});
				fnCallBack(aFilelist);
			} else {
				//否则发送服务器请求数据
				oSelf.loadDirFile(nFid, function(oResult) {
					aFilelist = +oResult.error !== 0 ? false : $.map(oResult.data, function(oFile) {return oFile;});
					//显然loadDirFile是要与服务器交互的，因此回调函数必需放在此处
					fnCallBack(aFilelist);
				});
			}
		}
		
		/**
		 * 添加文件
		 * @param object oFile
		 */
		, pushData: function(oFile) {
			this.oFileItem[oFile.fid] = oFile;
			this.dirCache(oFile.pid, 'update');
		}
		
		/**
		 * 格式化数据
		 * @param array aFilelist
		 * @param object oFormat 具体参数如下：
		 * 排序
		 *	aSort			排序字段，可以是文件信息中的任何字段、如filename、uid，支持多字段排序，格式: ['filename', 'uid' ...]
		 *	sOrder			排序方向  正向：ASC（默认）  反向：DESC
		 *	bDirPriority	文件夹优先排前，默认true
		 *
		 * 分页
		 *	nPage			第几页
		 *	nNum			每页显示多少条
		 *	
		 * 筛选
		 *	aFilter			支持类型有 all directory mark excel word ppt text image package audio video program，支持同时筛选多种类型，格式['mark', 'excel' ...]
		 * @return aFilelist
		 */
		, formatData: function(aFilelist, oFormat) {
			var oSelf = this,
				oFormat = $.extend({
					aSort: ['filename'],
					sOrder: 'ASC',
					bDirPriority: true
				}, oFormat || {});
			if(oFormat.aSort !== undefined) {
				aFilelist = oSelf.sort(aFilelist, oFormat.aSort, oFormat.sOrder, oFormat.bDirPriority);
			}
			if(oFormat.aFilter !== undefined) {
				aFilelist = oSelf.filter(aFilelist, oFormat.aFilter);
			}
			if(oFormat.nPage !== undefined) {
				aFilelist = oSelf.limit(aFilelist, oFormat.nPage, oFormat.nNum);
			}
			return aFilelist;
		}
		
		/**
		 * 目录缓存，用于记录当前目录下所有文件的id，提高读取速度
		 * 无特殊情况只限当前类的内部使用
		 * @param int nFid 目录id
		 * @param string sOp 操作：
		 *  get：获取（默认）
		 *  set：手动设置数据，需要提供aSetData参数，有时候我们已知文件夹下的数据时，可以手动的快速设置进而提高效率
		 *		 但一般情况下不推荐使用，函数会自动检查并获取
		 *  del: 删除
		 *  update： 更新缓存
		 * @param array aSetData [fid1, fid2 ...]
		 * @return array 目录下的文件id列表
		 */
		, dirCache: function(nFid, sOp, aSetData) {
			var oSelf = this,
				fnCallee = arguments.callee,
				oCacheData = fnCallee.oCacheData || {},
				bFileNoExists = false;
			var fnGetSubFids = function(_nFid) {
				var aSubFids = [],
					oFileItem = oSelf.oFileItem,
					fnHasOwn = Object.prototype.hasOwnProperty;
				for(var nFidKey in oFileItem) {
					if(!fnHasOwn.call(oFileItem, nFidKey)) {
						continue;
					}
					var oFile = oFileItem[nFidKey];
					if(!oFile) {
						bFileNoExists = true;
						return [];
					}
					if(oFile.pid == _nFid) {
						aSubFids.push(oFile.fid);
					}
				}
				return aSubFids;
			}
			switch(sOp) {
				case 'set':
					oCacheData[nFid] = aSetData;
					break;
				case 'update':
					oCacheData[nFid] = fnGetSubFids(nFid);
					break;
				case 'del':
					break;
				case 'get':
				default:
					if(!oCacheData[nFid]) {
						oCacheData[nFid] = fnGetSubFids(nFid);
					}
					break;
			}
			if(bFileNoExists || sOp == 'del') {
				//缓存中之前可能存在这个文件记录，但后来实际文件可能已经被删除
				//在这里无论如何都要尝试删除这些信息
				delete oCacheData[nFid];
				return [];
			}
			//重新记录数据
			fnCallee.oCacheData = oCacheData;
			return oCacheData[nFid];
		}
		
		/**
		 * 加载文件信息，暂时只支持单个文件信息
		 * @param array nFid 文件id列表
		 * @param function fnCallBack
		 * @return object file
		 */
		, loadFile: function(nFid, fnCallBack) {
			var oSelf = this;
			ex.post({'act': 'loadfile'}, { fid: nFid }, function(oResult) {
				if(+oResult.error === 0) {
					oSelf.pushData(oResult.data);
				}
				fnCallBack(oResult);
			});
		}
		
		/**
		 * 根据fid列表获取相应文件
		 * @param array nFid|aFids 文件id列表
		 * @return array aFilelist
		 */
		, getFile: function(aFids) {
			var oFileItem = this.oFileItem,
				aFilelist = [];
			var aFids = is_array(aFids) ? aFids : [aFids];
			$.each(aFids, function() {
				var nFid = this.valueOf(),
					oFile = oFileItem[nFid];
				if(oFile) {
					aFilelist.push(oFile);
				}
			});
			return aFilelist;
		}
		
		/**
		 * 搜索
		 * @param int nPid 在哪个文件夹下搜索
		 * @param string nKeyWrod 关键字
		 * @param false|function fnCallBack( oResult: 服务器返回的数据 )，如果不提供回调函数，程序将搜索当前本地数据
		 * @param boolean bFull 是否是只匹配完整的关键字，默认false
		 * @param boolean bCurrent 是否只在当前文件夹下搜索，默认false，否则从nPid开始一直往下搜索
		 * @return array aFilelist
		 */
		, search: function(nPid, nKeyWrod, fnCallback, bFull, bCurrent) {
			var oSelf = this,
				bFull = bFull ? 1 : 0,
				bCurrent = bCurrent ? 1 : 0;
			if(typeof fnCallback == 'function') {
				ex.post({'act': 'search'}, {
                    type: oSelf.sType,
					pid: nPid,
					keyword: nKeyWrod,
					full: bFull,
					current: bCurrent
				}, function(oResult) {
					if(+oResult.error === 0) {
						var fnHasOwn = Object.prototype.hasOwnProperty,
							oSearchlist = oResult.data;
						for(var nFidKey in oSearchlist) {
							if(!fnHasOwn.call(oSearchlist, nFidKey)) {
								continue;
							}
							oSelf.pushData(oSearchlist[nFidKey]);
						}
					}
					fnCallback(oResult);
				});
			} else {
				var fnMatch = bFull ? 
						function(oSelf) {return (oSelf == nKeyWrod)} :
						function(oSelf) {return (oSelf.indexOf(nKeyWrod) > -1)};
				var aFilelist = [],
					oSearchlist = bCurrent ? oSelf.getFile( oSelf.dirCache(nPid) ) : oSelf.oFileItem,
					fnHasOwn = Object.prototype.hasOwnProperty;
				for(var nFidKey in oSearchlist) {
					if(!fnHasOwn.call(oSearchlist, nFidKey) ||
						//根目录跳过
						nFidKey == 0) {
						continue;
					}
					var oFile = oSearchlist[nFidKey];
					if(oFile && fnMatch(oFile.filename) &&
						(bCurrent || oFile.idpath.indexOf('/' + nPid + '/') > -1)) {
						aFilelist.push(oFile);
					}
				}
				return aFilelist;
			}
		}
		
		/**
		 * 排序
		 * @param int|object|array mParams 可以是某个文件夹的id或者是一组文件列表的对象或数组，
		 		如果提供对象，程序将对这组文件进行排序并返回，否则返回这个文件夹下已排序的文件列表
		 * @param array aTypes 根据哪些字段进行排序
		 * @param string sOrder 分别有：'ASC'（正序，默认项） 、 'DESC'（倒序）
		 * @param boolean bDirPriority 文件夹优先靠前，默认true
		 * @return array oFilelist
		 */
		, sort: function(mParams, aTypes, sOrder, bDirPriority) {
			var oSelf = this,
				bDirPriority = (bDirPriority === undefined) ? true : !!bDirPriority,
				mFilelist = (+mParams == mParams) ? oSelf.getFile( oSelf.dirCache(mParams) ) : mParams;
			var aFilelist = $.map(mFilelist, function(oFile) {
				return oFile;
			});
			//设置排序优先级
			var nLPriority = (sOrder == 'DESC') ? 1 : -1,
				nRPriority = (sOrder == 'DESC') ? -1 : 1;
			$.each(aTypes, function() {
				var sType = this.toString();
				Array.prototype.sort.call(aFilelist, function(f1, f2) {
					if(bDirPriority) {
						if(f1.type == 'directory' && f2.type != 'directory') {
							return -1;
						} else if(f1.type != 'directory' && f2.type == 'directory') {
							return 1;
						}
					}
					var v1 = f1[sType], v2 = f2[sType];
					//某些字段需要转为数字才能比较
					if($.inArray(sType, ['pid', 'fid', 'uid', 'size']) > -1) {
						v1 = +v1, v2 = +v2;
					}
					if(v1 === v2) {return 0}
					return v1 < v2 ? nLPriority : nRPriority;
				});
			});
			return aFilelist;
		}
		
		/**
		 * 截取数据
		 * @param int|object|array mParams 可以是某个文件夹的id或者是一组文件列表的对象或数组，
		 		如果提供对象，程序将对这组文件进行截取并返回，否则返回这个文件夹下已截取的文件列表
		 * @param int nPage 第几页
		 * @param int nNum 显示多少条
		 * @return array aFilelist
		 */
		, limit: function(mParams, nPage, nNum) {
			var oSelf = this,
				bDirPriority = (bDirPriority === undefined) ? true : !!bDirPriority,
				mFilelist = (+mParams == mParams) ? oSelf.getFile( oSelf.dirCache(mParams) ) : mParams;
			var aFilelist = $.map(mFilelist, function(oFile) {
				return oFile;
			});
			nPage = parseInt(nPage);
			nPage = nPage > 0 ? nPage : 1;
			nNum = parseInt(nNum);
			nNum = nNum > 0 ? nNum : 18;
			var nStart = (nPage - 1) * nNum,
				nEnd = nStart + nNum;
			return aFilelist.slice(nStart, nEnd);
		}

		/**
		 * 筛选
		 * @param int|object|array mParams 可以是某个文件夹的id或者是一组文件列表的对象或数组，
				如果提供对象，程序将对这组文件进行筛选并返回，否则返回这个文件夹下已筛选的文件列表
		 * @param array aTypes all directory mark excel word ppt text image package audio video program
		 * @return array aFilelist
		 */
		, filter: function(mParams, aTypes) {
			var oSelf = this,
				mFilelist = (+mParams == mParams) ? oSelf.getFile( oSelf.dirCache(mParams) ) : mParams;
			var aFilelist = $.map(mFilelist, function(oFile) {
				return oFile;
			});
			if($.inArray('all', aTypes) > -1) {
				return aFilelist;
			}
			var aMatchFilelist = [],
				oRecord = {};
			var fnPushFile = function(oFile) {
				if(!oRecord[oFile.fid]) {
					aMatchFilelist.push(oFile);
					oRecord[oFile.fid] = true;
				}
			}
			$.each(aTypes, function() {
				var sType = this.toString();
				$.each(aFilelist, function() {
					var oFile = this;
					//目录跳过
					if(sType != 'directory' && oFile.type == 'directory') {
						//这是一个函数，要使用return
						return;
					}
					//像标记这样的特殊类型需要单独处理
					if(sType == 'mark') {
						if(oFile.mark == 1) {
							fnPushFile(oFile);
						}
					} else if(sType == 'directory' &&  oFile.type == 'directory') {
						fnPushFile(oFile);
					} else if(ex.checkFileType(oFile.type, sType)) {
						fnPushFile(oFile);
					}
				});
			});
			return aMatchFilelist;
		}
		
		/**
		 * 标记一个文件
		 * 当没有指定bMark的时候，如果文件原来是标记的，否则取消标记
		 * @param int nFid
		 * @param boolean bMark 明确指定是标记还是取消标记
		 * @param function fnCallBack( oResult: 服务器返回的数据 )
		 */
		, mark: function(nFid, bMark, fnCallBack) {
			var oSelf = this,
				oFile = oSelf.oFileItem[nFid];
			if(oFile && bMark === undefined) {
				bMark = oFile.mark == 1 ? 0 : 1;
			} else {
				bMark = bMark ? 1 : 0;
			}
			oSelf.update(nFid, {'mark': bMark}, fnCallBack);
		}
		
		/**
		 * 备注一个文件
		 * @param int nFid
		 * @param string sContent
		 * @param function fnCallBack( oResult: 服务器返回的数据 )
		 */
		, remark: function(nFid, sContent, fnCallBack) {
			this.update(nFid, {'remark': sContent}, fnCallBack);
		}
		
		/**
		 * 重命名
		 * @param int nFid
		 * @param string sName
		 * @param function fnCallBack( oResult: 服务器返回的数据 )
		 */
		, rename: function(nFid, sName, fnCallBack) {
			this.update(nFid, {'filename': sName}, fnCallBack);
		}
		
		/**
		 * 更新某个文件的某些数据
		 * @param int nFid
		 * @param object oUpdata 更新的字段以及值，格式{ key: value, key: value ... }
		 *		不可能所有字段都能更新，例如fid、addtime等等，具体数据由服务器端决定
		 * @param false|function fnCallBack( oResult: 服务器返回的数据 )，如果不提供回调函数，程序将只更新本地数据
		 */
		, update: function(nFid, oUpdata, fnCallBack) {
			var oSelf = this;
			//更新本地信息
			var fnUpdateLocal = function() {
				var oFile = oSelf.oFileItem[nFid];
				if(!oFile) {return;}
				var fnHasOwn = Object.prototype.hasOwnProperty;
				for(var sUpkey in oUpdata) {
					if(fnHasOwn.call(oFile, sUpkey)) {
						oFile[sUpkey] = oUpdata[sUpkey];
					}
				}
			}
			if(typeof fnCallBack == 'function') {
				var oPostdata = {fid: nFid};
				for(var sUpkey in oUpdata) {
					oPostdata[sUpkey] = oUpdata[sUpkey];
				}
				ex.post({'act': 'update'}, oPostdata, function(oResult) {
					if(+oResult.error === 0) {
						fnUpdateLocal();
					}
					fnCallBack(oResult);
				});
			} else {
				fnUpdateLocal();
			}
		}
		
		/**
		 * 共享文件
		 * @param array aFids 需要共享的文件id数组
		 * @param object oShareIdList 需要共享的人员、部门、岗位对象列表
		 *		格式：{ 'uids': [uid1, uid2 ...], 'deptids': [dept1, dept2 ...], 'posids': [posid1, posid2 ...] }
		 * @param function fnCallBack( oResult: 服务器返回的数据 )
		 */
		, share: function(aFids, oShareIdList, fnCallBack) {
			var oSelf = this,
				sUids = (oShareIdList.uids && (typeof oShareIdList.uids == 'string' ? oShareIdList.uids : oShareIdList.uids.join(','))) || '',
				sDeptids = (oShareIdList.deptids && (typeof oShareIdList.deptids == 'string' ? oShareIdList.deptids : oShareIdList.deptids.join(','))) || '',
				sPosids = (oShareIdList.posids &&(typeof oShareIdList.posids == 'string' ? oShareIdList.posids : oShareIdList.posids.join(','))) || '';
			ex.post({'act': 'share'}, {
				fids: aFids.join(','),
				uids: sUids,
				deptids: sDeptids,
				posids: sPosids
			}, function(oResult) {
				if(+oResult.error === 0) {
					//更新本地信息
					$.each(aFids, function() {
						oSelf.update(this.valueOf(), {
							touids: sUids,
							todeptids: sDeptids,
							toposids: sPosids
						});
					});
				}
				if(typeof fnCallBack == 'function') {
					fnCallBack(oResult);
				}
			});
		}
		
		/**
		 * 取消共享
		 * @param array aFids
		 * @param function fnCallBack( oResult: 服务器返回的数据 )
		 */
		, shareCancel: function(aFids, fnCallBack) {
			var oSelf = this;
			ex.post({'act': 'sharecancel'}, {fids: aFids.join(',')}, function(oResult) {
				if(+oResult.error === 0) {
					//更新本地信息
					$.each(aFids, function() {
						oSelf.update(this.valueOf(), {
							touids: '',
							todeptids: '',
							toposids: ''
						});
					});
				}
				if(typeof fnCallBack == 'function') {
					fnCallBack(oResult);
				}
			});
		}
		
		/**
		 * 检查某个文件夹下文件的名字是否已存在
		 * @param int nFid 在哪个文件夹下
		 * @param string sName
		 * @param false|function fnCallBack( oResult: 服务器返回的数据 )，如果不提供回调函数，程序将只检查本地数据
		 * @return boolean
		 */
		, checkSameName: function(nFid, sName, fnCallBack) {
			if(typeof fnCallBack == 'function') {
				ex.post({'act': 'checksamename'}, {fid: nFid, filename: sName}, function(oResult) {
					fnCallBack(oResult);
				});
			} else {
				return ( this.search(nFid, sName, false, true, true).length > 0 );
			}
		}
		
		/**
		 * 检查要移动的文件是否有相同的文件名
		 * @param array aSourceFids
		 * @param int nTargetFid
		 * @param function fnCallBack( oResult: 服务器返回的数据 )
		 */
		, checkMoveSameNames: function(aSourceFids, nTargetFid, fnCallBack) {
			ex.post({'act': 'check_move_same_name'}, {
				sourcefids: aSourceFids,
				targetfid: nTargetFid
			}, function(oResult) {
				fnCallBack(oResult);
			});
		}
		
		/**
		 * 移动文件
		 * @param array aSourceFids
		 * @param int nTargetFid
		 * @param function fnCallBack( oResult: 服务器返回的数据 )
		 * @param object oFromSettingList
		 */
		, move: function(aSourceFids, nTargetFid, fnCallBack, oNewNames) {
			var oSelf = this;
			var fnHasOwn = Object.prototype.hasOwnProperty;
			var oData = {
				sourcefids: aSourceFids.join(','),
				targetfid: nTargetFid
			};
			for(var nFid in oNewNames) {
				oData['newnames[' + nFid + ']'] = oNewNames[nFid];
			}
			ex.post({'act': 'move'}, oData , function(oResult) {
				if(+oResult.error === 0) {
					fnCallBack(oResult);
				}
			});
		}

		/**
		 * 添加文件夹
		 * @param int nPid 在哪个文件夹下新建
		 * @param string sName 文件夹名
		 * @param function fnCallBack( oResult )
		 */
		, mkDir: function(nPid, sName, fnCallBack) {
			var oSelf = this;
			ex.post({'act': 'mkdir'}, {pid: nPid, filename: sName}, function(oResult) {
				if(+oResult.error === 0) {
					oSelf.pushData(oResult.data);
				}
				fnCallBack(oResult);
			});
		}
		
		/*
		 * 添加文件夹
		 * @param int nPid 在哪个文件夹下新建
		 * @param string sType 类型 doc、xls、ppt
		 * @param string sName 文件夹名
		 * @param function fnCallBack( oResult )
		 */
		, mkOffice: function(nPid, sType, sName, fnCallBack) {
			var oSelf = this;
			ex.post({'act': 'mkoffice'}, {pid: nPid, filename: sName, type: sType}, function(oResult) {
				if(+oResult.error === 0) {
					oSelf.pushData(oResult.data);
				}
				fnCallBack(oResult);
			});
		}
		
		/**
		 * 清除本地数据
		 * @param int nFid 删除哪个文件夹下的文件
		 * @param boolean bRmSelf 是否删除自身
		 * @return boolean
		 */
		, clearLocalDir: function(nFid, bRmSelf) {
			var oSelf = this,
				fnHasOwn = Object.prototype.hasOwnProperty;
			for(var nFidKey in oSelf.oFileItem) {
				if(fnHasOwn.call(oSelf.oFileItem, nFidKey)) {
					var oFile = oSelf.oFileItem[nFidKey];
					if(oFile && oFile.idpath && oFile.idpath.indexOf('/' + nFid + '/') > -1) {
						delete oSelf.oFileItem[nFidKey];
						if(oFile.type == 'directory') {
							//清除缓存
							oSelf.dirCache(nFidKey, 'del');
						}
					}
				}
			}
			if(bRmSelf && nFid != 0) {
				delete oSelf.oFileItem[nFid];
				//清除缓存
				oSelf.dirCache(nFidKey, 'del');
			}
            if(nFid == 0) {
                oSelf.oFileItem[0].isload = false;
            }
			return true;
		}
		
		/**
		 * 删除文件
		 * @param array aFids
		 * @param function fnCallBack( oResult: 服务器返回的数据 )
		 * @param string sType 类型 myfile（默认） 、 trash
		 * @param boolean bFully 是否彻底删除（包含附件），当sType为trash时则总是彻底删除
		 */
		, remove: function(aFids, fnCallBack, sType, bFully) {
			var oSelf = this,
                sType = sType || 'myfile';
			ex.post({'act': 'remove'}, {
				fids: aFids.join(','),
				type: sType,
				fully: (bFully ? 1 : 0)
			}, function(oResult) {
				if(+oResult.error === 0) {
					var fnHasOwn = Object.prototype.hasOwnProperty;
					for(var nFidKey in aFids) {
                        var nFid = aFids[nFidKey];
						if(!fnHasOwn.call(oSelf.oFileItem, nFid) || nFid == 0) {
							continue;
						}
						//更新文件夹缓存
						if(oSelf.oFileItem[nFid].type == 'directory') {
							oSelf.dirCache(nFid, 'del');
						}
						delete oSelf.oFileItem[nFid];
					}
				}
				if(typeof fnCallBack == 'function') {
					fnCallBack(oResult);
				}
			});
		}
		
		
		/**
		 * 清空回收站数据
		 */
		, trashClearAll: function(fnCallBack) {
			ex.post({'act': 'trashclearall'}, {}, function(oResult) {
				fnCallBack(oResult);
			});
		}
		
		/**
		 * 从回收站还原文件
		 * @param array aSourceFids
		 * @param int nTargetFid
		 * @param function fnCallBack( oResult: 服务器返回的数据 )
		 */
		, restoreFromTrash: function(aSourceFids, nTargetFid, fnCallBack) {
			ex.post({'act': 'restorefromtrash'}, {
				sourcefids: aSourceFids.join(','),
				targetfid: nTargetFid
			}, function(oResult) {
				fnCallBack(oResult);
			});
		}
		
		/**
		 * 下载文件
		 * @param array aFids
		 */
		, download: function(aFids) {
			window.location = 'desktop.php?mod=file&do=action&act=download&fids=' + aFids.join(',');
		}
		
		/**
		 * 获得附件信息
		 * @param array nFid
		 * @param function fnCallBack
		 */
		, getAttachData: function(nFid, fnCallBack) {
			var oSelf = this;
			ex.post({'act': 'attachdata'}, {
				fid: nFid
			}, function(oResult) {
				if(+oResult.error === 0) {
					oSelf.oFileItem[nFid].attachdata = oResult.data;
				}
				fnCallBack(oResult);
			});
		}
		
		/**
		 * 获取附件内容
		 * @param array nFid
		 * @param function fnCallBack
		 */
		, getAttachContent: function(nFid, fnCallBack) {
			ex.post({'act': 'getcontent'}, {
				fid: nFid
			}, function(oResult) {
				fnCallBack(oResult);
			});
		}
    }
}(FileCabinet);