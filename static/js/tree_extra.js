 var ZTree = function(treeid, setting, data, isEdit,fnClick){
    this.treeid = treeid;   
    this.setting = setting;
    this.data = data;
    this.isEdit = isEdit;
    this.fnClick = fnClick;
    this.init();
} 
ZTree.prototype = { //基于jquery插件ztree扩展
    
    init: function(){
        var self = this;
        self.setting = $.extend({
            data: {
                simpleData: {enable: true }
            },
            callback: {
                onClick : function(e,treeid,treeNode){
                    self.fnClick(treeNode.id,treeNode.pId,treeNode,self);
                    if(self.isEdit){
                        self.node = treeNode;
                        self.menuObj = self.createMenu();
                        self.showMenuCtrl();
                    }
                }
                 /*onRightClick: function(e,treeid,treeNode){
                    self.node = treeNode;
                    self.menuObj =  self.createMenu();
                    self.showTreeMenu('',e); 
                }*/
            }
        },self.setting)
        $.fn.zTree.init($("#"+self.treeid), self.setting , self.data);//初始化树
        self.oTree = $.fn.zTree.getZTreeObj(self.treeid);//获取树对象
    },
    
    showMenuCtrl: function(){    //显示设置菜单
        var self = this;
         if(self.node.uneditable||self.node.id == 0){//属性为不可编辑或为根节点
            self.hideMenuCtrl();
            return;
        }
        if($('#tree_menu_ctrl').length == 0){//创建下拉触发层
            self.ctrlObj = $("<div></div>");
            self.ctrlObj.attr('id', 'tree_menu_ctrl').attr('class',"tree-ctrl");
            $("#"+self.node.tId).append(self.ctrlObj);
        }else{
            self.ctrlObj = $('#tree_menu_ctrl');
        }
        self.ctrlObj.show()
        var nTop =  $("#"+self.node.tId).offset().top - $("#"+ self.treeid).offset().top,
            timer;
        self.ctrlObj.css('top', nTop).hover(
            function(){ self.showTreeMenu(timer)},
            function(){ timer = setTimeout(function(){self.hideTreeMenu()}, 500) }
        );
        self.menuObj.hover(
            function(){ self.showTreeMenu(timer)},
            function(){ timer = setTimeout(function(){self.hideTreeMenu()}, 500)  }               
        )
    },
    
    showTreeMenu : function(timer){  //右键产生菜单时,param为事件参数,否则为定时器参数
        clearTimeout(timer);
        var self = this,
            nLeft,
            nTop,
            oCtrl = self.ctrlObj,
            oMenu = self.menuObj;
            if(oCtrl.offset().left + oMenu.width() > $('body').width()){
                nLeft = oCtrl.offset().left  - 10 - oMenu.width();
            }else{
                nLeft = oCtrl.offset().left + oCtrl.width() + 10
            }
            nTop = oCtrl.offset().top;
        /* else{
            if(!self.node)return;
            self.oTree.selectNode(self.node);
            self.hideMenuCtrl();
            nLeft = e.clientX;
            nTop = e.clientY + $("body").scrollTop();
        } */
        oMenu.css("left",nLeft).css("top", nTop).show()
    },
    
    hideTreeMenu: function(){
        this.menuObj.hide();
    },
    
    hideMenuCtrl: function(){
        if(this.ctrlObj)this.ctrlObj.hide();
        this.menuObj.hide();
    },
    
    createMenu: function(){ //创建操作菜单
        var self = this,
        m;
        if($("#tree_menu").length != 0 ){
            m = $("#tree_menu");
        }else{
            m = $("<ul></ul>").appendTo($("body")),
            aMenuHtml = [],
            jActAttr =[
                {act: 'add', ico: 'o_newdir', name: '新建'},
                {act: 'edit', ico: 'o_editdir', name: '编辑'},
                {act: 'moveup', ico: 'o_moveup', name: '上移'},
                {act: 'movedown', ico: 'o_movedown', name: '下移'},
                {act: 'del', ico: 'o_deldir', name: '删除'}
            ];
            for(var i = 0, l = jActAttr.length; i < l; i++){
                aMenuHtml.push("<li><i class='i_g "+ jActAttr[i].ico +"'></i><a href='javascript:;' act='"+ jActAttr[i].act +"'>"+ jActAttr[i].name +"</a></li>")
            }
            aMenuHtml = aMenuHtml.join("");
            m.attr("id", "tree_menu").attr("class", "tree-menu").html(aMenuHtml).hide();
            m.find("a").bind("click", function(){
                self.node.icon = "static/js/zTree/css/zTreeStyle/img/loading.gif"
                self.oTree.updateNode(self.node);
                var sAct = $(this).attr("act");
                if(!sAct)return false;
                switch(sAct){
                    case 'add':
                        self.dirAdd();
                        break;
                    case 'edit':
                        self.dirEdit();
                        break;
                    case 'moveup':
                        self.dirMoveUp();
                        break;
                    case 'movedown':
                        self.dirMoveDown();
                        break;
                    case 'del':
                        self.dirDel();
                        break;
                }
            })
        }
        return m;
    },
    
    //各种行为函数,根据情况可在实例化后进行重写
    dirAdd: function(){  //新增方法
        var self = this,
            msgbox = $("#d").length == 0 ? $("<div id='d'></div>"): $("#d");
        if(self.node.pId > 1){
            $("body").iTips({content:"新建失败", css:"error"})
            self.restoreIcon(self)
            return false;
        }
        var opt = {
            title: "新增部门",
            width: 550,
            height: 570,
            href: "http://ayumi.com/hr.php?mod=organization&do=dept&act=add",
            submit: function(e,url){
                var url = url||'', 
                    sDeptName = $("#deptname").val();
                $.post(url,{id:1000,name:sDeptName},function(){
                    self.oTree.addNodes(self.node, {id:1000,name:sDeptName});//testData
                    $("body").iTips({content:"添加部门成功", css:'success'});
                    msgbox.dialog("close");
                    self.restoreIcon(self)
                })
            },
            onClose: function(){
                self.restoreIcon(self)
            }
        }
        msgbox.iDialog(opt);
    },
    
    dirEdit: function(){ //编辑方法
        var self = this,
              msgbox = $("#d").length == 0 ? $("<div id='d'></div>"): $("#d");
        var opt = {
            title: "编辑部门",
            width: 550,
            height: 570,
            href: "http://ayumi.com/hr.php?mod=organization&do=dept&act=edit",
            submit: function(e,url){
                var url = url||'', 
                    sDeptName = $("#deptname").val();
                $.post(url,{id:1000,name:sDeptName},function(){
                    self.node.name = sDeptName;
                    self.oTree.updateNode(self.node);//testData
                    $("body").iTips({content:"编辑成功", css:'success'});
                    msgbox.dialog("close");
                    self.restoreIcon()
                })
            },
            onClose: function(){
                self.restoreIcon(self)
            }
        };
        msgbox.iDialog(opt);
    },
    
    dirMoveUp:function(url, data){  //向上移动
        alert(type+" = type");
        alert(url+" =url");
        alert(data+" =data");
        return ;
        var self = this,
            url = url || '',
            oNode = self.node,
            pNode = self.node.getPreNode(), //同目录的上一节点
            nNode = oNode.getNextNode();  //同目录的下一节点
        if(oNode.isFirstNode){//若是当前目录的第一节点则返回
            $("body").iTips({content:'移动失败',css:'error'})
            self.restoreIcon()
            return false;
        }
        pNode.index =self.oTree.getNodeIndex(oNode);
        oNode.index =  self.oTree.getNodeIndex(pNode);
        //{id: [oNode.id, pNode.id], index: [oNode.index, pNode.index]}
        $.post(url,data,
            function(data){
                self.oTree.moveNode(pNode,oNode,"prev") //上移一个节点
                $("body").iTips({content:'移动成功',css:'success'})
                self.restoreIcon()
            }
        )
        self.hideMenuCtrl();
    },
    
    dirMoveDown:function(type, url, data){  //向下移动
        var self = this,
            url = url || '',
            oNode = self.node,
            pNode = self.node.getPreNode(), //同目录的上一节点
            nNode = oNode.getNextNode();  //同目录的下一节点
        if(type == "movedown"){//若是最后节点则返回
            if(oNode.isLastNode){
                $("body").iTips({content:'移动失败', css:'error'})
                self.restoreIcon()
                return false;
            }
            nNode.index =self.oTree.getNodeIndex(oNode);
            oNode.index =  self.oTree.getNodeIndex(nNode);
            $.post(url,{id: [oNode.id, nNode.id], index: [oNode.index, nNode.index]},
                function(data){
                    self.oTree.moveNode(nNode,oNode,"next")
                    $("body").iTips({content:'移动成功',css:'success'})
                    self.restoreIcon()
                }
            )
        }else{
            return false;
        }
        self.hideMenuCtrl();
    },
    
    dirDel: function(url){  //删除方法
        var self = this,
            url = url||'';
        $.post(url,{id:self.node.id},function(){
            self.oTree.removeNode(self.node);
            $("body").iTips({content:'删除成功',css:'success'})
        })
        self.hideMenuCtrl();
    },
    
    restoreIcon: function(o){
        var self = o||this;
        self.node.icon = "";
        self.oTree.updateNode(self.node);
    },
    
    selectNode: function(id){
        var self = this;
        self.node = self.oTree.getNodeByParam("id", id, null);
        if(self.node){
            self.oTree.selectNode(self.node);
            self.menuObj =  self.createMenu();
            self.showMenuCtrl();
        }else{ return ;}
        
    }
};


/*    Test Data;
var jOrgData =  [
    {id: 1, pId:0, name: "yume", open:true},
    {id: 11, pId:1, name: "纯白交响曲", open:true},
    {id: 111, pId:11, name:"miu"}, 
    {id: 112, pId:11, name:"sagi"},
    {id: 12, pId:1, name:"架向星空之桥", open:true},
    {id: 121, pId:12, name:"wui"},
    {id: 122, pId:12, name:"maruka"},
    {id: 123, pId:12, name:"kohato"}
];
new ZTree('org_tree','',jOrgData);
*/
