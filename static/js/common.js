/*
	[IBOS] (C)2001-2099 ibos Inc.


	$Id: common.js 29624 2012-04-23 06:56:28Z monkey $
*/

var BROWSER = {};
var USERAGENT = navigator.userAgent.toLowerCase();
browserVersion({
    'ie':'msie',
    'firefox':'',
    'chrome':'',
    'opera':'',
    'safari':'',
    'mozilla':'',
    'webkit':'',
    'maxthon':'',
    'qq':'qqbrowser'
});
if(BROWSER.safari) {
    BROWSER.firefox = true;
}
BROWSER.opera = BROWSER.opera ? opera.version() : 0;

HTMLNODE = document.getElementsByTagName('head')[0].parentNode;
if(BROWSER.ie) {
    BROWSER.iemode = parseInt(typeof document.documentMode != 'undefined' ? document.documentMode : BROWSER.ie);
    HTMLNODE.className += ' ie_all ie' + BROWSER.iemode;
}
var CSSLOADED = [];
var JSLOADED = [];
var JSMENU = [];
JSMENU['active'] = [];
JSMENU['timer'] = [];
JSMENU['drag'] = [];
JSMENU['layer'] = 0;
JSMENU['zIndex'] = {
    'win':9020,
    'menu':9030,
    'dialog':9040,
    'prompt':9050
};
JSMENU['float'] = '';

var AJAX = [];
AJAX['url'] = [];
AJAX['stack'] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var CURRENTSTYPE = null;
var ibos_uid = isUndefined(ibos_uid) ? 0 : ibos_uid;
var creditnotice = isUndefined(creditnotice) ? '' : creditnotice;
var cookiedomain = isUndefined(cookiedomain) ? '' : cookiedomain;
var cookiepath = isUndefined(cookiepath) ? '' : cookiepath;
var EXTRAFUNC = [], EXTRASTR = '';
EXTRAFUNC['showmenu'] = [];

var IBOSCODE = [];
IBOSCODE['num'] = '-1';
IBOSCODE['html'] = [];

var USERABOUT_BOX = true;
var USERCARDST = null;
var CLIPBOARDSWFDATA = '';
var NOTICETITLE = [];

if(BROWSER.firefox && window.HTMLElement) {
    HTMLElement.prototype.__defineGetter__( "innerText", function(){
        var anyString = "";
        var childS = this.childNodes;
        for(var i=0; i <childS.length; i++) {
            if(childS[i].nodeType==1) {
                anyString += childS[i].tagName=="BR" ? '\n' : childS[i].innerText;
            } else if(childS[i].nodeType==3) {
                anyString += childS[i].nodeValue;
            }
        }
        return anyString;
    });
    HTMLElement.prototype.__defineSetter__( "innerText", function(sText){
        this.textContent=sText;
    });
    HTMLElement.prototype.__defineSetter__('outerHTML', function(sHTML) {
        var r = this.ownerDocument.createRange();
        r.setStartBefore(this);
        var df = r.createContextualFragment(sHTML);
        this.parentNode.replaceChild(df,this);
        return sHTML;
    });

    HTMLElement.prototype.__defineGetter__('outerHTML', function() {
        var attr;
        var attrs = this.attributes;
        var str = '<' + this.tagName.toLowerCase();
        for(var i = 0;i < attrs.length;i++){
            attr = attrs[i];
            if(attr.specified)
                str += ' ' + attr.name + '="' + attr.value + '"';
        }
        if(!this.canHaveChildren) {
            return str + '>';
        }
        return str + '>' + this.innerHTML + '</' + this.tagName.toLowerCase() + '>';
    });

    HTMLElement.prototype.__defineGetter__('canHaveChildren', function() {
        switch(this.tagName.toLowerCase()) {
            case 'area':case 'base':case 'basefont':case 'col':case 'frame':case 'hr':case 'img':case 'br':case 'input':case 'isindex':case 'link':case 'meta':case 'param':
                return false;
        }
        return true;
    });
}

function _id(id) {
    return !id ? null : document.getElementById(id);
}
function $C(classname, ele, tag) {
    var returns = [];
    ele = ele || document;
    tag = tag || '*';
    if(ele.getElementsByClassName) {
        var eles = ele.getElementsByClassName(classname);
        if(tag != '*') {
            for (var i = 0, L = eles.length; i < L; i++) {
                if (eles[i].tagName.toLowerCase() == tag.toLowerCase()) {
                    returns.push(eles[i]);
                }
            }
        } else {
            returns = eles;
        }
    }else {
        eles = ele.getElementsByTagName(tag);
        var pattern = new RegExp("(^|\\s)"+classname+"(\\s|$)");
        for (i = 0, L = eles.length; i < L; i++) {
            if (pattern.test(eles[i].className)) {
                returns.push(eles[i]);
            }
        }
    }
    return returns;
}
/**
* 添加事件
*
*
*/
function _attachEvent(obj, evt, func, eventobj) {
    eventobj = !eventobj ? obj : eventobj;
    if(obj.addEventListener) {
        obj.addEventListener(evt, func, false);
    } else if(eventobj.attachEvent) {
        obj.attachEvent('on' + evt, func);
    }
}
/**
* 移除事件
*
*
*/
function _detachEvent(obj, evt, func, eventobj) {
    eventobj = !eventobj ? obj : eventobj;
    if(obj.removeEventListener) {
        obj.removeEventListener(evt, func, false);
    } else if(eventobj.detachEvent) {
        obj.detachEvent('on' + evt, func);
    }
}
/**
* 浏览器版本
*
*
*/
function browserVersion(types) {
    var other = 1;
    for(i in types) {
        var v = types[i] ? types[i] : i;
        if(USERAGENT.indexOf(v) != -1) {
            var re = new RegExp(v + '(\\/|\\s)([\\d\\.]+)', 'ig');
            var matches = re.exec(USERAGENT);
            var ver = matches != null ? matches[2] : 0;
            other = ver !== 0 && v != 'mozilla' ? 0 : other;
        }else {
            var ver = 0;
        }
        eval('BROWSER.' + i + '= ver');
    }
    BROWSER.other = other;
}

/**
* 获取事件
*
*
*/
function getEvent() {
    if(document.all) return window.event;
    func = getEvent.caller;
    while(func != null) {
        var arg0 = func.arguments[0];
        if (arg0) {
            if((arg0.constructor  == Event || arg0.constructor == MouseEvent) || (typeof(arg0) == "object" && arg0.preventDefault && arg0.stopPropagation)) {
                return arg0;
            }
        }
        func=func.caller;
    }
    return null;
}
/**
* 检测undefined
*
*/
function isUndefined(variable) {
    return typeof variable == 'undefined' ? true : false;
}
/**
* 检测是否在数组内
*
*/
function in_array(needle, haystack) {
    if(typeof needle == 'string' || typeof needle == 'number') {
        for(var i in haystack) {
            if(haystack[i] == needle) {
                return true;
            }
        }
    }
    return false;
}

/**
 * 判断一个变量是否为数组
 * @param variable arr
 * @return boolean
 * @author denglh
 */
function is_array(variable) {
	return Object.prototype.toString.apply(variable) === '[object Array]';
}

/**
* 去除头尾空格
*
*/
function trim(str) {
    return (str + '').replace(/(\s+)$/g, '').replace(/^\s+/g, '');
}
/**
* 得到长度
*
*/
function strlen(str) {
    return (BROWSER.ie && str.indexOf('\n') != -1) ? str.replace(/\r?\n/g, '_').length : str.length;
}
/**
* 中文长度
*
*/
function mb_strlen(str) {
    var len = 0;
    for(var i = 0; i < str.length; i++) {
        len += str.charCodeAt(i) < 0 || str.charCodeAt(i) > 255 ? (charset == 'utf-8' ? 3 : 2) : 1;
    }
    return len;
}
/**
* 中文截取
*
*/
function mb_cutstr(str, maxlen, dot) {
    var len = 0;
    var ret = '';
    var dot = !dot ? '...' : dot;
    maxlen = maxlen - dot.length;
    for(var i = 0; i < str.length; i++) {
        len += str.charCodeAt(i) < 0 || str.charCodeAt(i) > 255 ? (charset == 'utf-8' ? 3 : 2) : 1;
        if(len > maxlen) {
            ret += dot;
            break;
        }
        ret += str.substr(i, 1);
    }
    return ret;
}
/**
* 正则替换
*
*/
function preg_replace(search, replace, str, regswitch) {
    var regswitch = !regswitch ? 'ig' : regswitch;
    var len = search.length;
    for(var i = 0; i < len; i++) {
        re = new RegExp(search[i], regswitch);
        str = str.replace(re, typeof replace == 'string' ? replace : (replace[i] ? replace[i] : replace[0]));
    }
    return str;
}
/**
* HTML转换
*
*/
function htmlspecialchars(str) {
    return preg_replace(['&', '<', '>', '"'], ['&amp;', '&lt;', '&gt;', '&quot;'], str);
}

/**
*显示和隐藏切换
*
*/
function display(id) {
    var obj = _id(id);
    if(obj.style.visibility) {
        obj.style.visibility = obj.style.visibility == 'visible' ? 'hidden' : 'visible';
    } else {
        obj.style.display = obj.style.display == '' ? 'none' : '';
    }
}

/**
*检查表单下被打勾选择框的数量
*@param <obj> form 表单对象
*@param <string> prefix 前缀
*@param <string> 不检查的选择框的name，默认是全部
*@return <int> 数量
*/
function checkall(form, prefix, checkall) {
    var checkall = checkall ? checkall : 'chkall';
    count = 0;
    for(var i = 0; i < form.elements.length; i++) {
        var e = form.elements[i];
        if(e.name && e.name != checkall && !e.disabled && (!prefix || (prefix && e.name.match(prefix)))) {
            e.checked = form.elements[checkall].checked;
            if(e.checked) {
                count++;
            }
        }
    }
    return count;
}
/**
* 存入COOKIE
*
*/
function setcookie(cookieName, cookieValue, seconds, path, domain, secure) {
    var expires = new Date();
    if(cookieValue == '' || seconds < 0) {
        cookieValue = '';
        seconds = -2592000;
    }
    expires.setTime(expires.getTime() + seconds * 1000);
    domain = !domain ? cookiedomain : domain;
    path = !path ? cookiepath : path;
    document.cookie = escape(cookiepre + cookieName) + '=' + escape(cookieValue)
    + (expires ? '; expires=' + expires.toGMTString() : '')
    + (path ? '; path=' + path : '/')
    + (domain ? '; domain=' + domain : '')
    + (secure ? '; secure' : '');
}
/**
* 读取COOKIE
*
*/
function getcookie(name, nounescape) {
    name = cookiepre + name;
    var cookie_start = document.cookie.indexOf(name);
    var cookie_end = document.cookie.indexOf(";", cookie_start);
    if(cookie_start == -1) {
        return '';
    } else {
        var v = document.cookie.substring(cookie_start + name.length + 1, (cookie_end > cookie_start ? cookie_end : document.cookie.length));
        return !nounescape ? unescape(v) : v;
    }
}

function Ajax(recvType, waitId) {

    var aj = new Object();

    aj.loading = '请稍候...';
    aj.recvType = recvType ? recvType : 'XML';
    aj.waitId = waitId ? _id(waitId) : null;

    aj.resultHandle = null;
    aj.sendString = '';
    aj.targetUrl = '';

    aj.setLoading = function(loading) {
        if(typeof loading !== 'undefined' && loading !== null) aj.loading = loading;
    };

    aj.setRecvType = function(recvtype) {
        aj.recvType = recvtype;
    };

    aj.setWaitId = function(waitid) {
        aj.waitId = typeof waitid == 'object' ? waitid : _id(waitid);
    };

    aj.createXMLHttpRequest = function() {
        var request = false;
        if(window.XMLHttpRequest) {
            request = new XMLHttpRequest();
            if(request.overrideMimeType) {
                request.overrideMimeType('text/xml');
            }
        } else if(window.ActiveXObject) {
            var versions = ['Microsoft.XMLHTTP', 'MSXML.XMLHTTP', 'Microsoft.XMLHTTP', 'Msxml2.XMLHTTP.7.0', 'Msxml2.XMLHTTP.6.0', 'Msxml2.XMLHTTP.5.0', 'Msxml2.XMLHTTP.4.0', 'MSXML2.XMLHTTP.3.0', 'MSXML2.XMLHTTP'];
            for(var i=0; i<versions.length; i++) {
                try {
                    request = new ActiveXObject(versions[i]);
                    if(request) {
                        return request;
                    }
                } catch(e) {}
            }
        }
        return request;
    };

    aj.XMLHttpRequest = aj.createXMLHttpRequest();
    aj.showLoading = function() {
        if(aj.waitId && (aj.XMLHttpRequest.readyState != 4 || aj.XMLHttpRequest.status != 200)) {
            aj.waitId.style.display = '';
            aj.waitId.innerHTML = '<span><img src="' + IMGDIR + '/loading.gif" class="vm"> ' + aj.loading + '</span>';
        }
    };

    aj.processHandle = function() {
        if(aj.XMLHttpRequest.readyState == 4 && aj.XMLHttpRequest.status == 200) {
            if(aj.waitId) {
                aj.waitId.style.display = 'none';
            }
            if(aj.recvType == 'HTML') {
                aj.resultHandle(aj.XMLHttpRequest.responseText, aj);
            } else if(aj.recvType == 'XML') {
                if(!aj.XMLHttpRequest.responseXML || !aj.XMLHttpRequest.responseXML.lastChild || aj.XMLHttpRequest.responseXML.lastChild.localName == 'parsererror') {
                    aj.resultHandle('<a href="' + aj.targetUrl + '" target="_blank" style="color:red">内部错误，无法显示此内容</a>' , aj);
                } else {
                    aj.resultHandle(aj.XMLHttpRequest.responseXML.lastChild.firstChild.nodeValue, aj);
                }
            }
        }
    };

    aj.get = function(targetUrl, resultHandle) {
        targetUrl = hostconvert(targetUrl);
        setTimeout(function(){
            aj.showLoading()
        }, 250);
        aj.targetUrl = targetUrl;
        aj.XMLHttpRequest.onreadystatechange = aj.processHandle;
        aj.resultHandle = resultHandle;
        var attackevasive = isUndefined(attackevasive) ? 0 : attackevasive;
        if(window.XMLHttpRequest) {
            aj.XMLHttpRequest.open('GET', aj.targetUrl);
            aj.XMLHttpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            aj.XMLHttpRequest.send(null);
        } else {
            aj.XMLHttpRequest.open("GET", targetUrl, true);
            aj.XMLHttpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            aj.XMLHttpRequest.send();
        }
    };
    aj.post = function(targetUrl, sendString, resultHandle) {
        targetUrl = hostconvert(targetUrl);
        setTimeout(function(){
            aj.showLoading()
        }, 250);
        aj.targetUrl = targetUrl;
        aj.sendString = sendString;
        aj.XMLHttpRequest.onreadystatechange = aj.processHandle;
        aj.resultHandle = resultHandle;
        aj.XMLHttpRequest.open('POST', targetUrl);
        aj.XMLHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        aj.XMLHttpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        aj.XMLHttpRequest.send(aj.sendString);
    };
    return aj;
}

/**
*获得主机名
*@param url 地址，如果为空则使用当前页面的地址
*@return <url> 主机名
*/
function getHost(url) {
    var host = "null";
    if(typeof url == "undefined"|| null == url) {
        url = window.location.href;
    }
    var regex = /^\w+\:\/\/([^\/]*).*/;
    var match = url.match(regex);
    if(typeof match != "undefined" && null != match) {
        host = match[1];
    }
    return host;
}
function hostconvert(url) {
    if(!url.match(/^https?:\/\//)) url = SITEURL + url;
    var url_host = getHost(url);
    var cur_host = getHost().toLowerCase();
    if(url_host && cur_host != url_host) {
        url = url.replace(url_host, cur_host);
    }
    return url;
}

function newfunction(func) {
    var args = [];
    for(var i=1; i<arguments.length; i++) args.push(arguments[i]);
    return function(event) {
        doane(event);
        window[func].apply(window, args);
        return false;
    }
}

function evalscript(s) {
    if(s.indexOf('<script') == -1) return s;
    var p = /<script[^\>]*?>([^\x00]*?)<\/script>/ig;
    var arr = [];
    while(arr = p.exec(s)) {
        var p1 = /<script[^\>]*?src=\"([^\>]*?)\"[^\>]*?(reload=\"1\")?(?:charset=\"([\w\-]+?)\")?><\/script>/i;
        var arr1 = [];
        arr1 = p1.exec(arr[0]);
        if(arr1) {
            appendscript(arr1[1], '', arr1[2], arr1[3]);
        } else {
            p1 = /<script(.*?)>([^\x00]+?)<\/script>/i;
            arr1 = p1.exec(arr[0]);
            appendscript('', arr1[2], arr1[1].indexOf('reload=') != -1);
        }
    }
    return s;
}

var safescripts = {}, evalscripts = [];
function safescript(id, call, seconds, times, timeoutcall, endcall, index) {
    seconds = seconds || 1000;
    times = times || 0;
    var checked = true;
    try {
        if(typeof call == 'function') {
            call();
        } else {
            eval(call);
        }
    } catch(e) {
        checked = false;
    }
    if(!checked) {
        if(!safescripts[id] || !index) {
            safescripts[id] = safescripts[id] || [];
            safescripts[id].push({
                'times':0,
                'si':setInterval(function () {
                    safescript(id, call, seconds, times, timeoutcall, endcall, safescripts[id].length);
                }, seconds)
            });
        } else {
            index = (index || 1) - 1;
            safescripts[id][index]['times']++;
            if(safescripts[id][index]['times'] >= times) {
                clearInterval(safescripts[id][index]['si']);
                if(typeof timeoutcall == 'function') {
                    timeoutcall();
                } else {
                    eval(timeoutcall);
                }
            }
        }
    } else {
        try {
            index = (index || 1) - 1;
            if(safescripts[id][index]['si']) {
                clearInterval(safescripts[id][index]['si']);
            }
            if(typeof endcall == 'function') {
                endcall();
            } else {
                eval(endcall);
            }
        } catch(e) {}
    }
}

function $F(func, args, script) {
    var run = function () {
        var argc = args.length, s = '';
        for(i = 0;i < argc;i++) {
            s += ',args[' + i + ']';
        }
        eval('var check = typeof ' + func + ' == \'function\'');
        if(check) {
            eval(func + '(' + s.substr(1) + ')');
        } else {
            setTimeout(function () {
                checkrun();
            }, 50);
        }
    };
    var checkrun = function () {
        if(JSLOADED[src]) {
            run();
        } else {
            setTimeout(function () {
                checkrun();
            }, 50);
        }
    };
    script = script || 'common';
    src = JSPATH + script + '.js?' + VERHASH;
    if(!JSLOADED[src]) {
        appendscript(src);
    }
    checkrun();
}

function appendscript(src, text, reload, charset) {
    var id = hash(src + text);
    if(!reload && in_array(id, evalscripts)) return;
    if(reload && _id(id)) {
        _id(id).parentNode.removeChild(_id(id));
    }

    evalscripts.push(id);
    var scriptNode = document.createElement("script");
    scriptNode.type = "text/javascript";
    scriptNode.id = id;
    scriptNode.charset = charset ? charset : (BROWSER.firefox ? document.characterSet : document.charset);
    try {
        if(src) {
            scriptNode.src = src;
            scriptNode.onloadDone = false;
            scriptNode.onload = function () {
                scriptNode.onloadDone = true;
                JSLOADED[src] = 1;
            };
            scriptNode.onreadystatechange = function () {
                if((scriptNode.readyState == 'loaded' || scriptNode.readyState == 'complete') && !scriptNode.onloadDone) {
                    scriptNode.onloadDone = true;
                    JSLOADED[src] = 1;
                }
            };
        } else if(text){
            scriptNode.text = text;
        }
        document.getElementsByTagName('head')[0].appendChild(scriptNode);
    } catch(e) {}
}
/**
* 清除SCRIPT
*
*/
function stripscript(s) {
    return s.replace(/<script.*?>.*?<\/script>/ig, '');
}

function ajaxupdateevents(obj, tagName) {
    tagName = tagName ? tagName : 'A';
    var objs = obj.getElementsByTagName(tagName);
    for(k in objs) {
        var o = objs[k];
        ajaxupdateevent(o);
    }
}

function ajaxupdateevent(o) {
    if(typeof o == 'object' && o.getAttribute) {
        if(o.getAttribute('ajaxtarget')) {
            if(!o.id) o.id = Math.random();
            var ajaxevent = o.getAttribute('ajaxevent') ? o.getAttribute('ajaxevent') : 'click';
            var ajaxurl = o.getAttribute('ajaxurl') ? o.getAttribute('ajaxurl') : o.href;
            _attachEvent(o, ajaxevent, newfunction('ajaxget', ajaxurl, o.getAttribute('ajaxtarget'), o.getAttribute('ajaxwaitid'), o.getAttribute('ajaxloading'), o.getAttribute('ajaxdisplay')));
            if(o.getAttribute('ajaxfunc')) {
                o.getAttribute('ajaxfunc').match(/(\w+)\((.+?)\)/);
                _attachEvent(o, ajaxevent, newfunction(RegExp.$1, RegExp.$2));
            }
        }
    }
}

function ajaxget(url, showid, waitid, loading, display, recall) {
    waitid = typeof waitid == 'undefined' || waitid === null ? showid : waitid;
    var x = new Ajax();
    x.setLoading(loading);
    x.setWaitId(waitid);
    x.display = typeof display == 'undefined' || display == null ? '' : display;
    x.showId = _id(showid);

    if(url.substr(strlen(url) - 1) == '#') {
        url = url.substr(0, strlen(url) - 1);
        x.autogoto = 1;
    }

    var url = url + '&inajax=1&ajaxtarget=' + showid;
    x.get(url, function(s, x) {
        var evaled = false;
        if(s.indexOf('ajaxerror') != -1) {
            evalscript(s);
            evaled = true;
        }
        if(!evaled && (typeof ajaxerror == 'undefined' || !ajaxerror)) {
            if(x.showId) {
                x.showId.style.display = x.display;
                ajaxinnerhtml(x.showId, s);
                ajaxupdateevents(x.showId);
                if(x.autogoto) scroll(0, x.showId.offsetTop);
            }
        }

        ajaxerror = null;
        if(recall && typeof recall == 'function') {
            recall();
        } else if(recall) {
            eval(recall);
        }
        if(!evaled) evalscript(s);
    });
}

function ajaxpost(formid, showid, waitid, showidclass, submitbtn, recall) {
    var waitid = typeof waitid == 'undefined' || waitid === null ? showid : (waitid !== '' ? waitid : '');
    var showidclass = !showidclass ? '' : showidclass;
    var ajaxframeid = 'ajaxframe';
    var ajaxframe = _id(ajaxframeid);
    var formtarget = _id(formid).target;

    var handleResult = function() {
        var s = '';
        var evaled = false;

        showloading('none');
        try {
            s = _id(ajaxframeid).contentWindow.document.XMLDocument.text;
        } catch(e) {
            try {
                s = _id(ajaxframeid).contentWindow.document.documentElement.firstChild.wholeText;
            } catch(e) {
                try {
                    s = _id(ajaxframeid).contentWindow.document.documentElement.firstChild.nodeValue;
                } catch(e) {
                    s = '内部错误，无法显示此内容';
                }
            }
        }
        //去除两边空格，模板用空格缩进返回纯JS代码时可能会出错 @denglh
        s = trim(s+'');
        if(s && s != '' && s.indexOf('ajaxerror') != -1) {
            evalscript(s);
            evaled = true;
        }
        if(showidclass) {
            if(showidclass != 'onerror') {
                _id(showid).className = showidclass;
            } else {
                showError(s);
                ajaxerror = true;
            }
        }
        if(submitbtn) {
            submitbtn.disabled = false;
        }
        if(!evaled && (typeof ajaxerror == 'undefined' || !ajaxerror)) {
            ajaxinnerhtml(_id(showid), s);
        }
        ajaxerror = null;
        if(_id(formid)) _id(formid).target = formtarget;
        if(typeof recall == 'function') {
            recall();
        } else {
            eval(recall);
        }
        if(!evaled) evalscript(s);
        ajaxframe.loading = 0;
        _id('append_parent').removeChild(ajaxframe.parentNode);
    };
    if(!ajaxframe) {
        var div = document.createElement('div');
        div.style.display = 'none';
        div.innerHTML = '<iframe name="' + ajaxframeid + '" id="' + ajaxframeid + '" loading="1"></iframe>';
        _id('append_parent').appendChild(div);
        ajaxframe = _id(ajaxframeid);
    } else if(ajaxframe.loading) {
        return false;
    }

    _attachEvent(ajaxframe, 'load', handleResult);

    showloading();
    _id(formid).target = ajaxframeid;
    var action = _id(formid).getAttribute('action');
    action = hostconvert(action);
    _id(formid).action = action.replace(/\&inajax\=1/g, '')+'&inajax=1';
    _id(formid).submit();
    if(submitbtn) {
        submitbtn.disabled = true;
    }
    doane();
    return false;
}

function ajaxmenu(ctrlObj, timeout, cache, duration, pos, recall, idclass, contentclass, menuid) {
    if(!ctrlObj.getAttribute('mid')) {
        var ctrlid = ctrlObj.id;
        if(!ctrlid) {
            ctrlObj.id = 'ajaxid_' + Math.random();
        }
    } else {
        var ctrlid = ctrlObj.getAttribute('mid');
        if(!ctrlObj.id) {
            ctrlObj.id = 'ajaxid_' + Math.random();
        }
    }
    if(isUndefined(menuid)) var menuid = ctrlid + '_menu';
    var menu = document.getElementById(menuid);
    if(isUndefined(timeout)) timeout = 3000;
    if(isUndefined(cache)) cache = 1;
    if(isUndefined(pos)) pos = '43';
    if(isUndefined(duration)) duration = timeout > 0 ? 0 : 3;
    if(isUndefined(idclass)) idclass = 'p_pop';
    if(isUndefined(contentclass)) contentclass = 'p_opt';
    var func = function() {
        showMenu({
            'ctrlid':ctrlObj.id,
            'menuid':menuid,
            'duration':duration,
            'timeout':timeout,
            'pos':pos,
            'cache':cache,
            'layer':2
        });
        if(typeof recall == 'function') {
            recall();
        } else {
            eval(recall);
        }
    };

    if(menu) {
        if(menu.style.display == '') {
            hideMenu(menuid);
        } else {
            func();
        }
    } else {
        menu = document.createElement('div');
        menu.id = menuid;
        menu.style.display = 'none';
        menu.className = idclass;
        menu.innerHTML = '<div class="' + contentclass + '" id="' + menuid + '_content"></div>';
        _id('append_parent').appendChild(menu);
        //IE9下object.href会首先获取src，如果一个对象同时存在src和href属性（事实上工作流表单就是这么做），会导致一些JS上的错误
        //所以要准确获取href的值要用js core标准的attributes属性或者getAttribute方法，下边的判断后半部分改为首先获取attributes的href值  @denglh
        var url = (!isUndefined(ctrlObj.attributes['shref']) ? ctrlObj.attributes['shref'].value : (ctrlObj.attributes['href'].value ? ctrlObj.attributes['href'].value : ctrlObj.href));
        url += (url.indexOf('?') != -1 ? '&' :'?') + 'ajaxmenu=1';
        ajaxget(url, menuid + '_content', 'ajaxwaitid', '', '', func);
    }
    doane();
}

function hash(string, length) {
    var length = length ? length : 32;
    var start = 0;
    var i = 0;
    var result = '';
    filllen = length - string.length % length;
    for(i = 0; i < filllen; i++){
        string += "0";
    }
    while(start < string.length) {
        result = stringxor(result, string.substr(start, length));
        start += length;
    }
    return result;
}

function stringxor(s1, s2) {
    var s = '';
    var hash = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var max = Math.max(s1.length, s2.length);
    for(var i=0; i<max; i++) {
        var k = s1.charCodeAt(i) ^ s2.charCodeAt(i);
        s += hash.charAt(k % 52);
    }
    return s;
}

function showPreview(val, id) {
    var showObj = _id(id);
    if(showObj) {
        showObj.innerHTML = val.replace(/\n/ig, "<bupdateseccoder />");
    }
}

function showloading(display, waiting) {
    var display = display ? display : 'block';
    var waiting = waiting ? waiting : '请稍候...';
    _id('ajaxwaitid').innerHTML = waiting;
    _id('ajaxwaitid').style.display = display;
}

function ajaxinnerhtml(showid, s) {
    if(showid.tagName != 'TBODY') {
        showid.innerHTML = s;
    } else {
        while(showid.firstChild) {
            showid.firstChild.parentNode.removeChild(showid.firstChild);
        }
        var div1 = document.createElement('DIV');
        div1.id = showid.id+'_div';
        div1.innerHTML = '<table><tbody id="'+showid.id+'_tbody">'+s+'</tbody></table>';
        _id('append_parent').appendChild(div1);
        var trs = div1.getElementsByTagName('TR');
        var l = trs.length;
        for(var i=0; i<l; i++) {
            showid.appendChild(trs[0]);
        }
        var inputs = div1.getElementsByTagName('INPUT');
        var l = inputs.length;
        for(var i=0; i<l; i++) {
            showid.appendChild(inputs[0]);
        }
        div1.parentNode.removeChild(div1);
    }
}

function doane(event, preventDefault, stopPropagation) {
    var preventDefault = isUndefined(preventDefault) ? 1 : preventDefault;
    var stopPropagation = isUndefined(stopPropagation) ? 1 : stopPropagation;
    e = event ? event : window.event;
    if(!e) {
        e = getEvent();
    }
    if(!e) {
        return null;
    }
    if(preventDefault) {
        if(e.preventDefault) {
            e.preventDefault();
        } else {
            e.returnValue = false;
        }
    }
    if(stopPropagation) {
        if(e.stopPropagation) {
            e.stopPropagation();
        } else {
            e.cancelBubble = true;
        }
    }
    return e;
}

/**
* 装载CSS
*
*/
function loadcss(cssname) {
    if(!CSSLOADED[cssname]) {
        if(!_id('css_' + cssname)) {
            css = document.createElement('link');
            css.id = 'css_' + cssname,
            css.type = 'text/css';
            css.rel = 'stylesheet';
            css.href = 'data/cache/style_' + STYLEID + '_' + cssname + '.css?' + VERHASH;
            var headNode = document.getElementsByTagName("head")[0];
            headNode.appendChild(css);
        } else {
            _id('css_' + cssname).href = 'data/cache/style_' + STYLEID + '_' + cssname + '.css?' + VERHASH;
        }
        CSSLOADED[cssname] = 1;
    }
}
/**
* 显示菜单

参数 v 格式：{'key1':'value1','key2':'value2','key3':'value3'}

目前，数组 v 支持传递的 key 有：

key 		默认值 					含义 				可选值及解释

ctrlid 		(必填) 				控制菜单的 id

showid 		ctrlid 				弹出菜单的 id

menuid 		showid + '_menu' 	显示菜单的 id

evt 		'mouseover' 		响应函数的事件 			click: ctrlObj 的 onclick 触发
														mouseover: ctrlObj 的 onmouseover 触发

pos 		'43' 				菜单位置 				详见 setMenuPosition() 中对 pos 的解释

layer 		1 				菜单层级 				大于 0 的任意整数

duration 	2 				菜单显示方式 			3:菜单一直显示，除非执行 hideMenu()，或者页面 unload
														2:鼠标移开 ctrlObj 及 menuObj 即开始计时 timeout 毫秒后消失
														1:鼠标移开 ctrlObj 即开始计时 timeout 毫秒后消失
														0:菜单显示即开始计时 timeout 毫秒后消失

timeout 	250 				菜单持续时间 			单位：毫秒

mtype 		'menu' 				菜单类型 				menu:普通菜单
														win:浮窗
														prompt:提示信息
														dialog:对话框

maxh 		600 				菜单最大高度，实际高度超过 maxh 时菜单将出现垂直滚动条

cache 		1 				是否缓存菜单 			1:是
														0:否

drag 		拖拽菜单对象的 id，如果希望整个菜单都可以拖拽 请设置 drag 等于1

fade 		0 				淡入淡出效果 			1:是
														0:否

cover 		0 				是否显示一个遮罩覆盖整个页面 	1:是
																0:否

zindex 		JSMENU['zIndex']['menu'] 	菜单层叠顺序

ctrlclass 	控制对象在菜单弹出后的 class 值，duration = 2 时有效
*
*/
function showMenu(v) {
    //{'ctrlid': this.id, 'menuid':'msg_menu', 'pos':'34',  'zindex': '9999'}
    var ctrlid = isUndefined(v['ctrlid']) ? v : v['ctrlid'];
    var showid = isUndefined(v['showid']) ? ctrlid : v['showid'];
    var menuid = isUndefined(v['menuid']) ? showid + '_menu' : v['menuid'];
    var ctrlObj = _id(ctrlid);
    var menuObj = _id(menuid);
    if(!menuObj) return;
    var mtype = isUndefined(v['mtype']) ? 'menu' : v['mtype'];
    var evt = isUndefined(v['evt']) ? 'mouseover' : v['evt'];
    var pos = isUndefined(v['pos']) ? '43' : v['pos'];
    var layer = isUndefined(v['layer']) ? 1 : v['layer'];
    var duration = isUndefined(v['duration']) ? 2 : v['duration'];
    var timeout = isUndefined(v['timeout']) ? 250 : v['timeout'];
    var maxh = isUndefined(v['maxh']) ? 600 : v['maxh'];
    var cache = isUndefined(v['cache']) ? 1 : v['cache'];
    var drag = isUndefined(v['drag']) ? '' : v['drag'];
    var dragobj = drag && _id(drag) ? _id(drag) : menuObj;
    var fade = isUndefined(v['fade']) ? 0 : v['fade'];
    var cover = isUndefined(v['cover']) ? 0 : v['cover'];
    var zindex = isUndefined(v['zindex']) ? JSMENU['zIndex']['menu'] : v['zindex'];
    var ctrlclass = isUndefined(v['ctrlclass']) ? '' : v['ctrlclass'];
    var winhandlekey = isUndefined(v['win']) ? '' : v['win'];
    var callback = (typeof v.callback == 'function') ? v.callback : false;
    zindex = cover ? zindex + 500 : zindex;
    if(typeof JSMENU['active'][layer] == 'undefined') {
        JSMENU['active'][layer] = [];
    }

    for(i in EXTRAFUNC['showmenu']) {
        try {
            eval(EXTRAFUNC['showmenu'][i] + '()');
        } catch(e) {}
    }

    if(evt == 'click' && in_array(menuid, JSMENU['active'][layer]) && mtype != 'win') {
        hideMenu(menuid, mtype);
        return;
    }
    if(mtype == 'menu') {
        hideMenu(layer, mtype);
    }

    if(ctrlObj) {
        if(!ctrlObj.getAttribute('initialized')) {
            ctrlObj.setAttribute('initialized', true);
            ctrlObj.unselectable = true;

            ctrlObj.outfunc = typeof ctrlObj.onmouseout == 'function' ? ctrlObj.onmouseout : null;
            ctrlObj.onmouseout = function() {
                if(this.outfunc) this.outfunc();
                if(duration < 3 && !JSMENU['timer'][menuid]) {
                    JSMENU['timer'][menuid] = setTimeout(function () {
                        hideMenu(menuid, mtype);
                    }, timeout);
                }
            };

            ctrlObj.overfunc = typeof ctrlObj.onmouseover == 'function' ? ctrlObj.onmouseover : null;
            ctrlObj.onmouseover = function(e) {
                doane(e);
                if(this.overfunc) this.overfunc();
                if(evt == 'click') {
                    clearTimeout(JSMENU['timer'][menuid]);
                    JSMENU['timer'][menuid] = null;
                } else {
                    for(var i in JSMENU['timer']) {
                        if(JSMENU['timer'][i]) {
                            clearTimeout(JSMENU['timer'][i]);
                            JSMENU['timer'][i] = null;
                        }
                    }
                }
            };
        }
    }

    if(!menuObj.getAttribute('initialized')) {
        menuObj.setAttribute('initialized', true);
        menuObj.ctrlkey = ctrlid;
        menuObj.mtype = mtype;
        menuObj.layer = layer;
        menuObj.cover = cover;
        if(ctrlObj && ctrlObj.getAttribute('fwin')) {
            menuObj.scrolly = true;
        }
        menuObj.style.position = 'absolute';
        menuObj.style.zIndex = zindex + layer;
        menuObj.onclick = function(e) {
            return doane(e, 0, 1);
        };
        if(duration < 3) {
            if(duration > 1) {
                menuObj.onmouseover = function() {
                    clearTimeout(JSMENU['timer'][menuid]);
                    JSMENU['timer'][menuid] = null;
                };
            }
            if(duration != 1) {
                menuObj.onmouseout = function() {
                    JSMENU['timer'][menuid] = setTimeout(function () {
                        hideMenu(menuid, mtype);
                    }, timeout);
                };
            }
        }
        if(cover) {
            var coverObj = document.createElement('div');
            coverObj.id = menuid + '_cover';
            coverObj.style.position = 'absolute';
            coverObj.style.zIndex = menuObj.style.zIndex - 1;
            coverObj.style.left = coverObj.style.top = '0px';
            coverObj.style.width = '100%';
            coverObj.style.height = Math.max(document.documentElement.clientHeight, document.body.offsetHeight) + 'px';
            coverObj.style.backgroundColor = '#000';
            coverObj.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=30)';
            coverObj.style.opacity = 0.3;
            coverObj.onclick = function () {
                hideMenu();
            };
            _id('append_parent').appendChild(coverObj);
            _attachEvent(window, 'load', function () {
                coverObj.style.height = Math.max(document.documentElement.clientHeight, document.body.offsetHeight) + 'px';
            }, document);
        }
    }
    if(drag) {
        dragobj.style.cursor = 'move';
        dragobj.onmousedown = function(event) {
            try{
                dragMenu(menuObj, event, 1);
            }catch(e){}
        };
    }

    if(cover) _id(menuid + '_cover').style.display = '';
    if(fade) {
        var O = 0;
        var fadeIn = function(O) {
            if(O > 100) {
                clearTimeout(fadeInTimer);
                return;
            }
            menuObj.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=' + O + ')';
            menuObj.style.opacity = O / 100;
            O += 20;
            var fadeInTimer = setTimeout(function () {
                fadeIn(O);
            }, 40);
        };
        fadeIn(O);
        menuObj.fade = true;
    } else {
        menuObj.fade = false;
    }
    menuObj.style.display = '';
    if(ctrlObj && ctrlclass) {
        ctrlObj.className += ' ' + ctrlclass;
        menuObj.setAttribute('ctrlid', ctrlid);
        menuObj.setAttribute('ctrlclass', ctrlclass);
    }
    if(pos != '*') {
        setMenuPosition(showid, menuid, pos);
    }
    if(callback){callback(menuObj)}
    if(BROWSER.ie && BROWSER.ie < 7 && winhandlekey && _id('fwin_' + winhandlekey)) {
        _id(menuid).style.left = (parseInt(_id(menuid).style.left) - parseInt(_id('fwin_' + winhandlekey).style.left)) + 'px';
        _id(menuid).style.top = (parseInt(_id(menuid).style.top) - parseInt(_id('fwin_' + winhandlekey).style.top)) + 'px';
    }
    if(maxh && menuObj.scrollHeight > maxh) {
        menuObj.style.height = maxh + 'px';
        if(BROWSER.opera) {
            menuObj.style.overflow = 'auto';
        } else {
            menuObj.style.overflowY = 'auto';
        }
    }

    if(!duration) {
        setTimeout('hideMenu(\'' + menuid + '\', \'' + mtype + '\')', timeout);
    }

    if(!in_array(menuid, JSMENU['active'][layer])) JSMENU['active'][layer].push(menuid);
    menuObj.cache = cache;
    if(layer > JSMENU['layer']) {
        JSMENU['layer'] = layer;
    }
}
/**
* 延迟显示
*
*/
var delayShowST = null;
function delayShow(ctrlObj, call, time) {
    if(typeof ctrlObj == 'object') {
        var ctrlid = ctrlObj.id;
        call = call || function () {
            showMenu(ctrlid);
        };
    }
    var time = isUndefined(time) ? 500 : time;
    delayShowST = setTimeout(function () {
        if(typeof call == 'function') {
            call();
        } else {
            eval(call);
        }
    }, time);
    if(!ctrlObj.delayinit) {
        _attachEvent(ctrlObj, 'mouseout', function() {
            clearTimeout(delayShowST);
        });
        ctrlObj.delayinit = 1;
    }
}
/**
 * 拖动函数
 *
 *
 *
 */
var dragMenuDisabled = false;
function dragMenu(menuObj, e, op) {
    e = e ? e : window.event;
    if(op == 1) {
        if(dragMenuDisabled || in_array(e.target ? e.target.tagName : e.srcElement.tagName, ['TEXTAREA', 'INPUT', 'BUTTON', 'SELECT'])) {
            return;
        }
        JSMENU['drag'] = [e.clientX, e.clientY];
        JSMENU['drag'][2] = parseInt(menuObj.style.left);
        JSMENU['drag'][3] = parseInt(menuObj.style.top);
        document.onmousemove = function(e) {
            try{
                dragMenu(menuObj, e, 2);
            }catch(err){}
        };
        document.onmouseup = function(e) {
            try{
                dragMenu(menuObj, e, 3);
            }catch(err){}
        };
        doane(e);
    }else if(op == 2 && JSMENU['drag'][0]) {
        var menudragnow = [e.clientX, e.clientY];
        menuObj.style.left = (JSMENU['drag'][2] + menudragnow[0] - JSMENU['drag'][0]) + 'px';
        menuObj.style.top = (JSMENU['drag'][3] + menudragnow[1] - JSMENU['drag'][1]) + 'px';
        menuObj.removeAttribute('top_');
        menuObj.removeAttribute('left_');
        doane(e);
    }else if(op == 3) {
        JSMENU['drag'] = [];
        document.onmousemove = null;
        document.onmouseup = null;
    }
}
/**
 *	设置菜单方向
 *  @param pos  方向 ， 例子 34! 两个数字组成
 *	第一个1,2,3,4 表示对齐点 左上，右上，右下，左下; 第二个1,2,3,4表示朝向，会自动适应， 加！表示强制按此显示方式不自动适应。
 *
 *
 */
function setMenuPosition(showid, menuid, pos) {
    var showObj = _id(showid);
    var menuObj = menuid ? _id(menuid) : _id(showid + '_menu');
    if(isUndefined(pos) || !pos) pos = '43';
    var basePoint = parseInt(pos.substr(0, 1));
    var direction = parseInt(pos.substr(1, 1));
    var important = pos.indexOf('!') != -1 ? 1 : 0;
    var sxy = 0, sx = 0, sy = 0, sw = 0, sh = 0, ml = 0, mt = 0, mw = 0, mcw = 0, mh = 0, mch = 0, bpl = 0, bpt = 0;

    if(!menuObj || (basePoint > 0 && !showObj)) return;
    if(showObj) {
        sxy = fetchOffset(showObj);
        sx = sxy['left'];
        sy = sxy['top'];
        sw = showObj.offsetWidth;
        sh = showObj.offsetHeight;
    }
    mw = menuObj.offsetWidth;
    mcw = menuObj.clientWidth;
    mh = menuObj.offsetHeight;
    mch = menuObj.clientHeight;

    switch(basePoint) {
        case 1:
            bpl = sx;
            bpt = sy;
            break;
        case 2:
            bpl = sx + sw;
            bpt = sy;
            break;
        case 3:
            bpl = sx + sw;
            bpt = sy + sh;
            break;
        case 4:
            bpl = sx;
            bpt = sy + sh;
            break;
    }
    switch(direction) {
        case 0:
            menuObj.style.left = (document.body.clientWidth - menuObj.clientWidth) / 2 + 'px';
            mt = (document.documentElement.clientHeight - menuObj.clientHeight) / 2;
            break;
        case 1:
            ml = bpl - mw;
            mt = bpt - mh;
            break;
        case 2:
            ml = bpl;
            mt = bpt - mh;
            break;
        case 3:
            ml = bpl;
            mt = bpt;
            break;
        case 4:
            ml = bpl - mw;
            mt = bpt;
            break;
    }
    var scrollTop = Math.max(document.documentElement.scrollTop, document.body.scrollTop);
    var scrollLeft = Math.max(document.documentElement.scrollLeft, document.body.scrollLeft);
    if(!important) {
        if(in_array(direction, [1, 4]) && ml < 0) {
            ml = bpl;
            if(in_array(basePoint, [1, 4])) ml += sw;
        } else if(ml + mw > scrollLeft + document.body.clientWidth && sx >= mw) {
            ml = bpl - mw;
            if(in_array(basePoint, [2, 3])) {
                ml -= sw;
            } else if(basePoint == 4) {
                ml += sw;
            }
        }
        if(in_array(direction, [1, 2]) && mt < 0) {
            mt = bpt;
            if(in_array(basePoint, [1, 2])) mt += sh;
        } else if(mt + mh > scrollTop + document.documentElement.clientHeight && sy >= mh) {
            mt = bpt - mh;
            if(in_array(basePoint, [3, 4])) mt -= sh;
        }
    }
    if(pos.substr(0, 3) == '210') {
        ml += 69 - sw / 2;
        mt -= 5;
        if(showObj.tagName == 'TEXTAREA') {
            ml -= sw / 2;
            mt += sh / 2;
        }
    }
    if(direction == 0 || menuObj.scrolly) {
        if(BROWSER.ie && BROWSER.ie < 7) {
            if(direction == 0) mt += scrollTop;
        } else {
            if(menuObj.scrolly) mt -= scrollTop;
            menuObj.style.position = 'fixed';
        }
    }
    if(ml) menuObj.style.left = ml + 'px';
    if(mt) menuObj.style.top = mt + 'px';
    if(direction == 0 && BROWSER.ie && !document.documentElement.clientHeight) {
        menuObj.style.position = 'absolute';
        menuObj.style.top = (document.body.clientHeight - menuObj.clientHeight) / 2 + 'px';
    }
    if(menuObj.style.clip && !BROWSER.opera) {
        menuObj.style.clip = 'rect(auto, auto, auto, auto)';
    }
}

function hideMenu(attr, mtype) {
    attr = isUndefined(attr) ? '' : attr;
    mtype = isUndefined(mtype) ? 'menu' : mtype;
    if(attr == '') {
        for(var i = 1; i <= JSMENU['layer']; i++) {
            hideMenu(i, mtype);
        }
        return;
    } else if(typeof attr == 'number') {
        for(var j in JSMENU['active'][attr]) {
            hideMenu(JSMENU['active'][attr][j], mtype);
        }
        return;
    }else if(typeof attr == 'string') {
        var menuObj = _id(attr);
        if(!menuObj || (mtype && menuObj.mtype != mtype)) return;
        var ctrlObj = '', ctrlclass = '';
        if((ctrlObj = _id(menuObj.getAttribute('ctrlid'))) && (ctrlclass = menuObj.getAttribute('ctrlclass'))) {
            var reg = new RegExp(' ' + ctrlclass);
            ctrlObj.className = ctrlObj.className.replace(reg, '');
        }
        clearTimeout(JSMENU['timer'][attr]);
        var hide = function() {
            if(menuObj.cache) {
                if(menuObj.style.visibility != 'hidden') {
                    menuObj.style.display = 'none';
                    if(menuObj.cover) _id(attr + '_cover').style.display = 'none';
                }
            }else {
                menuObj.parentNode.removeChild(menuObj);
                if(menuObj.cover) _id(attr + '_cover').parentNode.removeChild(_id(attr + '_cover'));
            }
            var tmp = [];
            for(var k in JSMENU['active'][menuObj.layer]) {
                if(attr != JSMENU['active'][menuObj.layer][k]) tmp.push(JSMENU['active'][menuObj.layer][k]);
            }
            JSMENU['active'][menuObj.layer] = tmp;
        };
        if(menuObj.fade) {
            var O = 100;
            var fadeOut = function(O) {
                if(O == 0) {
                    clearTimeout(fadeOutTimer);
                    hide();
                    return;
                }
                menuObj.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=' + O + ')';
                menuObj.style.opacity = O / 100;
                O -= 20;
                var fadeOutTimer = setTimeout(function () {
                    fadeOut(O);
                }, 40);
            };
            fadeOut(O);
        } else {
            hide();
        }
    }
	formNotAlert();
}

/*
* 获取当前风络样式
*
*/
function getCurrentStyle(obj, cssproperty, csspropertyNS) {
    if(obj.style[cssproperty]){
        return obj.style[cssproperty];
    }
    if (obj.currentStyle) {
        return obj.currentStyle[cssproperty];
    } else if (document.defaultView.getComputedStyle(obj, null)) {
        var currentStyle = document.defaultView.getComputedStyle(obj, null);
        var value = currentStyle.getPropertyValue(csspropertyNS);
        if(!value){
            value = currentStyle[cssproperty];
        }
        return value;
    } else if (window.getComputedStyle) {
        var currentStyle = window.getComputedStyle(obj, "");
        return currentStyle.getPropertyValue(csspropertyNS);
    }
}
/**
* 获取位置
*
*/
function fetchOffset(obj, mode) {
    var left_offset = 0, top_offset = 0, mode = !mode ? 0 : mode;

    if(obj.getBoundingClientRect && !mode) {
        var rect = obj.getBoundingClientRect();
        var scrollTop = Math.max(document.documentElement.scrollTop, document.body.scrollTop);
        var scrollLeft = Math.max(document.documentElement.scrollLeft, document.body.scrollLeft);
        if(document.documentElement.dir == 'rtl') {
            scrollLeft = scrollLeft + document.documentElement.clientWidth - document.documentElement.scrollWidth;
        }
        left_offset = rect.left + scrollLeft - document.documentElement.clientLeft;
        top_offset = rect.top + scrollTop - document.documentElement.clientTop;
    }
    if(left_offset <= 0 || top_offset <= 0) {
        left_offset = obj.offsetLeft;
        top_offset = obj.offsetTop;
        while((obj = obj.offsetParent) != null) {
            position = getCurrentStyle(obj, 'position', 'position');
            if(position == 'relative') {
                continue;
            }
            left_offset += obj.offsetLeft;
            top_offset += obj.offsetTop;
        }
    }
    return {
        'left' : left_offset,
        'top' : top_offset
    };
}

function showTip(ctrlobj) {
    $F('_showTip', arguments);
}

function showPrompt(ctrlid, evt, msg, timeout) {
    $F('_showPrompt', arguments);
}

function showCreditPrompt() {
    $F('_showCreditPrompt', []);
}

var showDialogST = null;
function showDialog(msg, mode, t, func, cover, funccancel, leftmsg, confirmtxt, canceltxt, closetime, locationtime) {
    clearTimeout(showDialogST);
    cover = isUndefined(cover) ? (mode == 'info' ? 0 : 1) : cover;
    leftmsg = isUndefined(leftmsg) ? '' : leftmsg;
    mode = in_array(mode, ['confirm', 'notice', 'info', 'right']) ? mode : 'alert';
    var menuid = 'fwin_dialog';
    var menuObj = _id(menuid);
    var showconfirm = 1;
    confirmtxtdefault = '确定';
    closetime = isUndefined(closetime) ? '' : closetime;
    closefunc = function () {
        if(typeof func == 'function') func();
        else eval(func);
        hideMenu(menuid, 'dialog');
    };
    if(closetime) {
        leftmsg = closetime + ' 秒后窗口关闭';
        showDialogST = setTimeout(closefunc, closetime * 1000);
        showconfirm = 0;
    }
    locationtime = isUndefined(locationtime) ? '' : locationtime;
    if(locationtime) {
        leftmsg = locationtime + ' 秒后页面跳转';
        showDialogST = setTimeout(closefunc, locationtime * 1000);
        showconfirm = 0;
    }
    confirmtxt = confirmtxt ? confirmtxt : confirmtxtdefault;
    canceltxt = canceltxt ? canceltxt : '取消';

    if(menuObj) hideMenu('fwin_dialog', 'dialog');
    menuObj = document.createElement('div');
    menuObj.style.display = 'none';
    menuObj.className = 'fwinmask';
    menuObj.id = menuid;
    _id('append_parent').appendChild(menuObj);
    var hidedom = '';
    if(!BROWSER.ie) {
        hidedom = '<style type="text/css">object{visibility:hidden;}</style>';
    }
    var s = hidedom + '<table cellpadding="0" cellspacing="0" class="fwin"><tr><td class="t_l"></td><td class="t_c"></td><td class="t_r"></td></tr><tr><td class="m_l">&nbsp;&nbsp;</td><td class="m_c"><h3 class="flb"><em>';
    s += t ? t : '提示信息';
    s += '</em><span><a href="javascript:;" id="fwin_dialog_close" class="flbc" onclick="hideMenu(\'' + menuid + '\', \'dialog\')" title="关闭">关闭</a></span></h3>';
    if(mode == 'info') {
        s += msg ? msg : '';
    } else {
        s += '<div class="c altw"><div class="' + (mode == 'alert' ? 'alert_error' : (mode == 'right' ? 'alert_right' : 'alert_info')) + '"><p>' + msg + '</p></div></div>';
        s += '<p class="o pns">' + (leftmsg ? '<span class="z xg1">' + leftmsg + '</span>' : '') + (showconfirm ? '<button id="fwin_dialog_submit" value="true" class="pn pnc"><strong>'+confirmtxt+'</strong></button>' : '');
        s += mode == 'confirm' ? '<button id="fwin_dialog_cancel" value="true" class="pn" onclick="hideMenu(\'' + menuid + '\', \'dialog\')"><strong>'+canceltxt+'</strong></button>' : '';
        s += '</p>';
    }
    s += '</td><td class="m_r"></td></tr><tr><td class="b_l"></td><td class="b_c"></td><td class="b_r"></td></tr></table>';
    menuObj.innerHTML = s;
    if(_id('fwin_dialog_submit')) _id('fwin_dialog_submit').onclick = function() {
        if(typeof func == 'function') func();
        else eval(func);
        hideMenu(menuid, 'dialog');
    };
    if(_id('fwin_dialog_cancel')) {
        _id('fwin_dialog_cancel').onclick = function() {
            if(typeof funccancel == 'function') funccancel();
            else eval(funccancel);
            hideMenu(menuid, 'dialog');
        };
        _id('fwin_dialog_close').onclick = _id('fwin_dialog_cancel').onclick;
    }
    showMenu({
        'mtype':'dialog',
        'menuid':menuid,
        'duration':3,
        'pos':'00',
        'zindex':JSMENU['zIndex']['dialog'],
        'cache':0,
        'cover':cover
    });
    try {
        if(_id('fwin_dialog_submit')) _id('fwin_dialog_submit').focus();
    } catch(e) {}
}

function showWindow(k, url, mode, cache, menuv) {
    mode = isUndefined(mode) ? 'get' : mode;
    cache = isUndefined(cache) ? 1 : cache;
    var menuid = 'fwin_' + k;
    var menuObj = _id(menuid);
    var drag = null;
    var loadingst = null;
    var hidedom = '';

    if(disallowfloat && disallowfloat.indexOf(k) != -1) {
        if(BROWSER.ie) url += (url.indexOf('?') != -1 ?  '&' : '?') + 'referer=' + escape(location.href);
        location.href = url;
        doane();
        return;
    }

    var fetchContent = function() {
        if(mode == 'get') {
            menuObj.url = url;
            url += (url.search(/\?/) > 0 ? '&' : '?') + 'infloat=yes&handlekey=' + k;
            url += cache == -1 ? '&t='+(+ new Date()) : '';
            ajaxget(url, 'fwin_content_' + k, null, '', '', function() {
                initMenu();
                show();
            });
        } else if(mode == 'post') {
            menuObj.act = _id(url).action;
            ajaxpost(url, 'fwin_content_' + k, '', '', '', function() {
                initMenu();
                show();
            });
        }
        if(parseInt(BROWSER.ie) != 6) {
            loadingst = setTimeout(function() {
                showDialog('', 'info', '<img src="' + IMGDIR + '/loading.gif"> 请稍候...')
            }, 500);
        }
    };
    var initMenu = function() {
        clearTimeout(loadingst);
        var objs = menuObj.getElementsByTagName('*');
        var fctrlidinit = false;
        for(var i = 0; i < objs.length; i++) {
            if(objs[i].id) {
                objs[i].setAttribute('fwin', k);
            }
            if(objs[i].className == 'flb' && !fctrlidinit) {
                if(!objs[i].id) objs[i].id = 'fctrl_' + k;
                drag = objs[i].id;
                fctrlidinit = true;
            }
        }
    };
    var show = function() {
        hideMenu('fwin_dialog', 'dialog');
        v = {
            'mtype':'win',
            'menuid':menuid,
            'duration':3,
            'pos':'00',
            'zindex':JSMENU['zIndex']['win'],
            'drag':typeof drag == null ? '' : drag,
            'cache':cache
        };
        for(k in menuv) {
            v[k] = menuv[k];
        }
        showMenu(v);
    };

    if(!menuObj) {
        menuObj = document.createElement('div');
        menuObj.id = menuid;
        menuObj.className = 'fwinmask';
        menuObj.style.display = 'none';
        _id('append_parent').appendChild(menuObj);
        evt = ' style="cursor:move" onmousedown="dragMenu(_id(\'' + menuid + '\'), event, 1)" ondblclick="hideWindow(\'' + k + '\')"';
        if(!BROWSER.ie) {
            hidedom = '<style type="text/css">object{visibility:hidden;}</style>';
        }
        menuObj.innerHTML = hidedom + '<table cellpadding="0" cellspacing="0" class="fwin"><tr><td class="t_l"></td><td class="t_c"' + evt + '></td><td class="t_r"></td></tr><tr><td class="m_l"' + evt + ')">&nbsp;&nbsp;</td><td class="m_c" id="fwin_content_' + k + '">'
        + '</td><td class="m_r"' + evt + '"></td></tr><tr><td class="b_l"></td><td class="b_c"' + evt + '></td><td class="b_r"></td></tr></table>';
        if(mode == 'html') {
            _id('fwin_content_' + k).innerHTML = url;
            initMenu();
            show();
        } else {
            fetchContent();
        }
    } else if((mode == 'get' && (url != menuObj.url || cache != 1)) || (mode == 'post' && _id(url).action != menuObj.act)) {
        fetchContent();
    } else {
        show();
    }
    doane();
}

function showError(msg) {
    var p = /<script[^\>]*?>([^\x00]*?)<\/script>/ig;
    msg = msg.replace(p, '');
    if(msg !== '') {
        showDialog(msg, 'alert', '错误信息', null, true, null, '', '', '', 3);
    }
}

function hideWindow(k, all, clear) {
    all = isUndefined(all) ? 1 : all;
    clear = isUndefined(clear) ? 1 : clear;
    hideMenu('fwin_' + k, 'win');
    if(clear && _id('fwin_' + k)) {
        _id('append_parent').removeChild(_id('fwin_' + k));
    }
    if(all) {
        hideMenu();
    }
}

function AC_FL_RunContent() {
    var str = '';
    var ret = AC_GetArgs(arguments, "clsid:d27cdb6e-ae6d-11cf-96b8-444553540000", "application/x-shockwave-flash");
    if(BROWSER.ie && !BROWSER.opera) {
        str += '<object ';
        for (var i in ret.objAttrs) {
            str += i + '="' + ret.objAttrs[i] + '" ';
        }
        str += '>';
        for (var i in ret.params) {
            str += '<param name="' + i + '" value="' + ret.params[i] + '" /> ';
        }
        str += '</object>';
    } else {
        str += '<embed ';
        for (var i in ret.embedAttrs) {
            str += i + '="' + ret.embedAttrs[i] + '" ';
        }
        str += '></embed>';
    }
    return str;
}

function AC_GetArgs(args, classid, mimeType) {
    var ret = new Object();
    ret.embedAttrs = new Object();
    ret.params = new Object();
    ret.objAttrs = new Object();
    for (var i = 0; i < args.length; i = i + 2){
        var currArg = args[i].toLowerCase();
        switch (currArg){
            case "classid":
                break;
            case "pluginspage":
                ret.embedAttrs[args[i]] = 'http://www.macromedia.com/go/getflashplayer';
                break;
            case "src":
                ret.embedAttrs[args[i]] = args[i+1];
                ret.params["movie"] = args[i+1];
                break;
            case "codebase":
                ret.objAttrs[args[i]] = 'http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0';
                break;
            case "onafterupdate":case "onbeforeupdate":case "onblur":case "oncellchange":case "onclick":case "ondblclick":case "ondrag":case "ondragend":
            case "ondragenter":case "ondragleave":case "ondragover":case "ondrop":case "onfinish":case "onfocus":case "onhelp":case "onmousedown":
            case "onmouseup":case "onmouseover":case "onmousemove":case "onmouseout":case "onkeypress":case "onkeydown":case "onkeyup":case "onload":
            case "onlosecapture":case "onpropertychange":case "onreadystatechange":case "onrowsdelete":case "onrowenter":case "onrowexit":case "onrowsinserted":case "onstart":
            case "onscroll":case "onbeforeeditfocus":case "onactivate":case "onbeforedeactivate":case "ondeactivate":case "type":
            case "id":
                ret.objAttrs[args[i]] = args[i+1];
                break;
            case "width":case "height":case "align":case "vspace": case "hspace":case "class":case "title":case "accesskey":case "name":
            case "tabindex":
                ret.embedAttrs[args[i]] = ret.objAttrs[args[i]] = args[i+1];
                break;
            default:
                ret.embedAttrs[args[i]] = ret.params[args[i]] = args[i+1];
        }
    }
    ret.objAttrs["classid"] = classid;
    if(mimeType) {
        ret.embedAttrs["type"] = mimeType;
    }
    return ret;
}

function simulateSelect(selectId, widthvalue) {
    var selectObj = _id(selectId);
    if(!selectObj) return;
    if(BROWSER.other) {
        if(selectObj.getAttribute('change')) {
            selectObj.onchange = function () {
                eval(selectObj.getAttribute('change'));
            }
        }
        return;
    }
    var widthvalue = widthvalue ? widthvalue : 70;
    var defaultopt = selectObj.options[0] ? selectObj.options[0].innerHTML : '';
    var defaultv = '';
    var menuObj = document.createElement('div');
    var ul = document.createElement('ul');
    var handleKeyDown = function(e) {
        e = BROWSER.ie ? event : e;
        if(e.keyCode == 40 || e.keyCode == 38) doane(e);
    };
    var selectwidth = (selectObj.getAttribute('width', i) ? selectObj.getAttribute('width', i) : widthvalue) + 'px';
    var tabindex = selectObj.getAttribute('tabindex', i) ? selectObj.getAttribute('tabindex', i) : 1;

    for(var i = 0; i < selectObj.options.length; i++) {
        var li = document.createElement('li');
        li.innerHTML = selectObj.options[i].innerHTML;
        li.k_id = i;
        li.k_value = selectObj.options[i].value;
        if(selectObj.options[i].selected) {
            defaultopt = selectObj.options[i].innerHTML;
            defaultv = selectObj.options[i].value;
            li.className = 'current';
            selectObj.setAttribute('selecti', i);
        }
        li.onclick = function() {
            if(_id(selectId + '_ctrl').innerHTML != this.innerHTML) {
                var lis = menuObj.getElementsByTagName('li');
                lis[_id(selectId).getAttribute('selecti')].className = '';
                this.className = 'current';
                _id(selectId + '_ctrl').innerHTML = this.innerHTML;
                _id(selectId).setAttribute('selecti', this.k_id);
                _id(selectId).options.length = 0;
                _id(selectId).options[0] = new Option('', this.k_value);
                eval(selectObj.getAttribute('change'));
            }
            hideMenu(menuObj.id);
            return false;
        };
        ul.appendChild(li);
    }

    selectObj.options.length = 0;
    selectObj.options[0]= new Option('', defaultv);
    selectObj.style.display = 'none';
    selectObj.outerHTML += '<a href="javascript:;" id="' + selectId + '_ctrl" style="width:' + selectwidth + '" tabindex="' + tabindex + '">' + defaultopt + '</a>';

    menuObj.id = selectId + '_ctrl_menu';
    menuObj.className = 'sltm';
    menuObj.style.display = 'none';
    menuObj.style.width = selectwidth;
    menuObj.appendChild(ul);
    _id('append_parent').appendChild(menuObj);

    _id(selectId + '_ctrl').onclick = function(e) {
        _id(selectId + '_ctrl_menu').style.width = selectwidth;
        showMenu({
            'ctrlid':(selectId == 'loginfield' ? 'account' : selectId + '_ctrl'),
            'menuid':selectId + '_ctrl_menu',
            'evt':'click',
            'pos':'43'
        });
        doane(e);
    };
    _id(selectId + '_ctrl').onfocus = menuObj.onfocus = function() {
        _attachEvent(document.body, 'keydown', handleKeyDown);
    };
    _id(selectId + '_ctrl').onblur = menuObj.onblur = function() {
        _detachEvent(document.body, 'keydown', handleKeyDown);
    };
    _id(selectId + '_ctrl').onkeyup = function(e) {
        e = e ? e : window.event;
        value = e.keyCode;
        if(value == 40 || value == 38) {
            if(menuObj.style.display == 'none') {
                _id(selectId + '_ctrl').onclick();
            } else {
                lis = menuObj.getElementsByTagName('li');
                selecti = selectObj.getAttribute('selecti');
                lis[selecti].className = '';
                if(value == 40) {
                    selecti = parseInt(selecti) + 1;
                } else if(value == 38) {
                    selecti = parseInt(selecti) - 1;
                }
                if(selecti < 0) {
                    selecti = lis.length - 1
                } else if(selecti > lis.length - 1) {
                    selecti = 0;
                }
                lis[selecti].className = 'current';
                selectObj.setAttribute('selecti', selecti);
                lis[selecti].parentNode.scrollTop = lis[selecti].offsetTop;
            }
        } else if(value == 13) {
            var lis = menuObj.getElementsByTagName('li');
            lis[selectObj.getAttribute('selecti')].onclick();
        }
        else if(value == 27) {
            hideMenu(menuObj.id);
        }
    };
}

/**
* 切换标签
*
*/
function switchTab(prefix, current, total, activeclass) {
    $F('_switchTab', arguments);
}

function imageRotate(imgid, direct) {
    $F('_imageRotate', arguments);
}

function thumbImg(obj, method) {
    if(!obj) {
        return;
    }
    obj.onload = null;
    file = obj.src;
    zw = obj.offsetWidth;
    zh = obj.offsetHeight;
    if(zw < 2) {
        if(!obj.id) {
            obj.id = 'img_' + Math.random();
        }
        setTimeout("thumbImg(_id('" + obj.id + "'), " + method + ")", 100);
        return;
    }
    zr = zw / zh;
    method = !method ? 0 : 1;
    if(method) {
        fixw = obj.getAttribute('_width');
        fixh = obj.getAttribute('_height');
        if(zw > fixw) {
            zw = fixw;
            zh = zw / zr;
        }
        if(zh > fixh) {
            zh = fixh;
            zw = zh * zr;
        }
    }
    else {
        fixw = typeof imagemaxwidth == 'undefined' ? '600' : imagemaxwidth;
        if(zw > fixw) {
            zw = fixw;
            zh = zw / zr;
            obj.style.cursor = 'pointer';
            if(!obj.onclick) {
                obj.onclick = function() {
                    zoom(obj, obj.src);
                };
            }
        }
    }
    obj.width = zw;
    obj.height = zh;
}

/**
 * 显示图片
 * obj 对象 一般用this
 * zimg  图片地址  this.src
 * nocover
 * pn
 *
 */
var zoomstatus = 1;
function zoom(obj, zimg, nocover, pn, showexif) {
    $F('_zoom', arguments);
}

function showselect(obj, inpid, t, rettype) {
    $F('_showselect', arguments);
}

function showColorBox(ctrlid, layer, k, bgcolor) {
    $F('_showColorBox', arguments);
}

function ctrlEnter(event, btnId, onlyEnter) {
    if(isUndefined(onlyEnter)) onlyEnter = 0;
    if((event.ctrlKey || onlyEnter) && event.keyCode == 13) {
        _id(btnId).click();
        return false;
    }
    return true;
}

function parseurl(str, mode, parsecode) {
    if(isUndefined(parsecode)) parsecode = true;
    if(parsecode) str= str.replace(/\s*\[code\]([\s\S]+?)\[\/code\]\s*/ig, function($1, $2) {
        return codetag($2);
    });
    str = str.replace(/([^>=\]"'\/@]|^)((((https?|ftp|gopher|news|telnet|rtsp|mms|callto|bctp|ed2k|thunder|qqdl|synacast):\/\/))([\w\-]+\.)*[:\.@\-\w\u4e00-\u9fa5]+\.([\.a-zA-Z0-9]+|\u4E2D\u56FD|\u7F51\u7EDC|\u516C\u53F8)((\?|\/|:)+[\w\.\/=\?%\-&;~`@':+!#]*)*)/ig, mode == 'html' ? '$1<a href="$2" target="_blank">$2</a>' : '$1[url]$2[/url]');
    str = str.replace(/([^\w>=\]"'\/@]|^)((www\.)([\w\-]+\.)*[:\.@\-\w\u4e00-\u9fa5]+\.([\.a-zA-Z0-9]+|\u4E2D\u56FD|\u7F51\u7EDC|\u516C\u53F8)((\?|\/|:)+[\w\.\/=\?%\-&;~`@':+!#]*)*)/ig, mode == 'html' ? '$1<a href="$2" target="_blank">$2</a>' : '$1[url]$2[/url]');
    str = str.replace(/([^\w->=\]:"'\.\/]|^)(([\-\.\w]+@[\.\-\w]+(\.\w+)+))/ig, mode == 'html' ? '$1<a href="mailto:$2">$2</a>' : '$1[email]$2[/email]');
    if(parsecode) {
        for(var i = 0; i <= IBOSCODE['num']; i++) {
            str = str.replace("[\tIBOS_CODE_" + i + "\t]", IBOSCODE['html'][i]);
        }
    }
    return str;
}

function codetag(text) {
    IBOSCODE['num']++;
    if(typeof wysiwyg != 'undefined' && wysiwyg) text = text.replace(/<br[^\>]*>/ig, '\n');
    IBOSCODE['html'][IBOSCODE['num']] = '[code]' + text + '[/code]';
    return '[\tIBOS_CODE_' + IBOSCODE['num'] + '\t]';
}

function saveUserdata(name, data) {
    try {
        if(window.localStorage){
            localStorage.setItem('iBOS_' + name, data);
        } else if(window.sessionStorage){
            sessionStorage.setItem('iBOS_' + name, data);
        }
    }
    catch(e) {
        if(BROWSER.ie){
            if(data.length < 54889) {
                with(document.documentElement) {
                    setAttribute("value", data);
                    save('iBOS_' + name);
                    }
            }
        }
    }
    setcookie('clearUserdata', '', -1);
}

function loadUserdata(name) {
    if(window.localStorage){
        return localStorage.getItem('iBOS_' + name);
    } else if(window.sessionStorage){
        return sessionStorage.getItem('iBOS_' + name);
    } else if(BROWSER.ie){
        with(document.documentElement) {
            load('iBOS_' + name);
            return getAttribute("value");
            }
    }
}

function initTab(frameId, type) {
    $F('_initTab', arguments);
}

function openDiy(){
    window.location.href = ((window.location.href + '').replace(/[\?\&]diy=yes/g, '').split('#')[0] + ( window.location.search && window.location.search.indexOf('?diy=yes') < 0 ? '&diy=yes' : '?diy=yes'));
}

function hasClass(elem, className) {
    return elem.className && (" " + elem.className + " ").indexOf(" " + className + " ") != -1;
}

function runslideshow() {
    $F('_runslideshow', []);
}

function toggle_collapse(objname, noimg, complex, lang) {
    $F('_toggle_collapse', arguments);
}

function updatestring(str1, str2, clear) {
    str2 = '_' + str2 + '_';
    return clear ? str1.replace(str2, '') : (str1.indexOf(str2) == -1 ? str1 + str2 : str1);
}

function getClipboardData() {
    window.document.clipboardswf.SetVariable('str', CLIPBOARDSWFDATA);
}

function setCopy(text, msg) {
    $F('_setCopy', arguments);
}

function copycode(obj) {
    $F('_copycode', arguments);
}

function showdistrict(container, elems, totallevel, changelevel, containertype) {
    $F('_showdistrict', arguments);
}

function setDoodle(fid, oid, url, tid, from) {
    $F('_setDoodle', arguments);
}


function initSearchmenu(searchform, cloudSearchUrl) {
    var defaultUrl = 'search.php?searchsubmit=yes';
    if(typeof cloudSearchUrl == "undefined" || cloudSearchUrl == null || cloudSearchUrl == '') {
        cloudSearchUrl = defaultUrl;
    }

    var searchtxt = _id(searchform + '_txt');
    if(!searchtxt) {
        searchtxt = _id(searchform);
    }
    var tclass = searchtxt.className;
    searchtxt.className = tclass + ' xg1';
    if (!!("placeholder" in document.createElement("input"))) {
        if(searchtxt.value == '请输入搜索内容') {
            searchtxt.value = '';
        }
        searchtxt.placeholder = '请输入搜索内容';
    } else {
        searchtxt.onfocus = function () {
            if(searchtxt.value == '请输入搜索内容') {
                searchtxt.value = '';
                searchtxt.className = tclass;
            }
        };
        searchtxt.onblur = function () {
            if(searchtxt.value == '' ) {
                searchtxt.value = '请输入搜索内容';
                searchtxt.className = tclass + ' xg1';
            }
        };
    }
    if(!_id(searchform + '_type_menu')) return false;
    var o = _id(searchform + '_type');
    var a = _id(searchform + '_type_menu').getElementsByTagName('a');
    var formobj = searchtxt.form;
    for(var i=0; i<a.length; i++){
        if(a[i].className == 'curtype'){
            o.innerHTML = a[i].innerHTML;
            _id(searchform + '_mod').value = a[i].rel;
            formobj.method = 'post';
            if((a[i].rel == 'forum' || a[i].rel == 'curforum') && defaultUrl != cloudSearchUrl) {
                formobj.action = cloudSearchUrl;
                formobj.method = 'get';
                if(_id('srchFId')) {
                    _id('srchFId').value = a[i].rel == 'forum' ? 0 : a[i].getAttribute('fid');
                }
            }
            else {
                formobj.action = defaultUrl;
            }
        }
        a[i].onclick = function(){
            o.innerHTML = this.innerHTML;
            _id(searchform + '_mod').value = this.rel;
            formobj.method = 'post';
            if((this.rel == 'forum' || this.rel == 'curforum') && defaultUrl != cloudSearchUrl) {
                formobj.action = cloudSearchUrl;
                formobj.method = 'get';
                if(_id('srchFId')) {
                    _id('srchFId').value = this.rel == 'forum' ? 0 : this.getAttribute('fid');
                }
            } else {
                formobj.action = defaultUrl;
            }
        };
    }
}

function searchFocus(obj) {
    if(obj.value == '请输入搜索内容') {
        obj.value = '';
    }
    if(_id('cloudsearchquery') != null) {
        _id('cloudsearchquery').value = obj.value;
    }
}

function extstyle(css) {
    $F('_extstyle', arguments);
}

function widthauto(obj) {
    $F('_widthauto', arguments);
}
var secST = new Array();
function updatesecqaa(idhash) {
    $F('_updatesecqaa', arguments);
}

function updateseccode(idhash, play) {
    $F('_updateseccode', arguments);
}

function checksec(type, idhash, showmsg, recall) {
    $F('_checksec', arguments);
}

function createPalette(colorid, id, func) {
    $F('_createPalette', arguments);
}

function showForummenu(fid) {
    $F('_showForummenu', arguments);
}

function cardInit() {
    var cardShow = function (obj) {
        if(BROWSER.ie && BROWSER.ie < 7 && obj.href.indexOf('username') != -1) {
            return;
        }
        pos = obj.getAttribute('c') == '1' ? '43' : obj.getAttribute('c');
        USERCARDST = setTimeout(function() {
            ajaxmenu(obj, 500, 1, 2, pos, null, 'p_pop card');
        }, 250);
    };
    var a = document.body.getElementsByTagName('a');
    for(var i = 0;i < a.length;i++){
        if(a[i].getAttribute('c')) {
            a[i].setAttribute('mid', hash(a[i].href));
            a[i].onmouseover = function() {
                cardShow(this)
            };
            a[i].onmouseout = function() {
                clearTimeout(USERCARDST);
            };
        }
    }
}

function navShow(id) {
	var ctrlobj = _id('mn_' + id),
        mnobj = _id('snav_mn_' + id),
        isLast = false;
	if(!mnobj) {
		return;
	}
	var uls = _id('mu').getElementsByTagName('ul');
	for(var i = 0;i < uls.length;i++) {
		if(uls[i].parentNode.className != 'cl current') {
			uls[i].parentNode.style.display = 'none';
		}
	}
    var oNext = mnobj.nextSibling;
    isLast = !oNext ? true : oNext.nodeType == 1 ? false : !oNext.nextSibling ? true : false;  /* 判断当前是否为最后一个导航 */
	if(mnobj.className != 'cl current') {
		showMenu({
			'ctrlid':'mn_' + id,
			'menuid':'snav_mn_' + id,
            'pos': isLast ? '34!': '43'
		});
		mnobj.className = 'cl';
        with(mnobj.style){
            position = "fixed";
            top = "80px";
            display = '';
            zIndex = '9000';
        }
	}
    if(isLast){ /* 最后一个导航，小三角定位在左侧 */
        var oFirst = mnobj.firstChild.nodeType == 1 ? mnobj.firstChild : mnobj.firstChild.nextSibling;
        with(oFirst.style){
            left ="";
            right = "20px";
        }
    }
}

function strLenCalc(obj, checklen, maxlen) {
    var v = obj.value, charlen = 0, maxlen = !maxlen ? 200 : maxlen, curlen = maxlen, len = strlen(v);
    for(var i = 0; i < v.length; i++) {
        if(v.charCodeAt(i) < 0 || v.charCodeAt(i) > 255) {
            curlen -= charset == 'utf-8' ? 2 : 1;
        }
    }
    if(curlen >= len) {
        _id(checklen).innerHTML = curlen - len;
    } else {
        obj.value = mb_cutstr(v, maxlen, 0);
    }
}

function patchNotice() {
    if(_id('patch_notice')) {
        ajaxget('misc.php?mod=patch&action=patchnotice', 'patch_notice', '');
    }
}

function pluginNotice() {
    if(_id('plugin_notice')) {
        ajaxget('misc.php?mod=patch&action=pluginnotice', 'plugin_notice', '');
    }
}

function noticeTitle() {
    NOTICETITLE = {
        'State':0,
        'oldTitle':document.title,
        flashNumber:0,
        sleep:15
    };
    if(!getcookie('noticeTitle')) {
        window.setInterval('noticeTitleFlash();', 500);
    } else {
        window.setTimeout('noticeTitleFlash();', 500);
    }
    setcookie('noticeTitle', 1, 600);
}

function noticeTitleFlash() {
    if(NOTICETITLE.flashNumber < 5 || NOTICETITLE.flashNumber > 4 && !NOTICETITLE['State']) {
        document.title = (NOTICETITLE['State'] ? '【　　　】' : '【新提醒】') + NOTICETITLE['oldTitle'];
        NOTICETITLE['State'] = !NOTICETITLE['State'];
    }
    NOTICETITLE.flashNumber = NOTICETITLE.flashNumber < NOTICETITLE.sleep ? ++NOTICETITLE.flashNumber : 0;
}

function relatedlinks(rlinkmsgid) {
    $F('_relatedlinks', arguments);
}

function con_handle_response(response) {
    return response;
}

function showTopLink() {
    if(_id('ft')){
        var viewPortHeight = parseInt(document.documentElement.clientHeight);
        var scrollHeight = parseInt(document.body.getBoundingClientRect().top);
        var basew = parseInt(_id('ft').clientWidth);
        var sw = _id('scrolltop').clientWidth;
        if (basew < 1000) {
            var left = parseInt(fetchOffset(_id('ft'))['left']);
            left = left < sw ? left * 2 - sw : left;
            _id('scrolltop').style.left = ( basew + left ) + 'px';
        } else {
            _id('scrolltop').style.left = 'auto';
            _id('scrolltop').style.right = 0;
        }

        if (BROWSER.ie && BROWSER.ie < 7) {
            _id('scrolltop').style.top = viewPortHeight - scrollHeight - 150 + 'px';
        }
        if (scrollHeight < -100) {
            _id('scrolltop').style.visibility = 'visible';
        } else {
            _id('scrolltop').style.visibility = 'hidden';
        }
    }
}

// 显示积分菜单
function showCreditmenu() {
    $F('_showCreditmenu', []);
}

function showUpgradeinfo() {
    showMenu({
        'ctrlid':'g_upmine',
        'pos':'21'
    });
}

function addFavorite(url, title) {
    try {
        window.external.addFavorite(url, title);
    }
    catch (e){
        try {
            window.sidebar.addPanel(title, url, '');
        }
        catch (e) {
            showDialog("请按 Ctrl+D 键添加到收藏夹", 'notice');
        }
    }
}

function setHomepage(sURL) {
    if(BROWSER.ie){
        document.body.style.behavior = 'url(#default#homepage)';
        document.body.setHomePage(sURL);
    } else {
        showDialog("非 IE 浏览器请手动将本站设为首页", 'notice');
        doane();
    }
}

function smilies_show(id, smcols, seditorkey) {
    $F('_smilies_show', arguments, 'smilies');
}

function showfocus(ftype, autoshow) {
    var id = parseInt(_id('focuscur').innerHTML);
    if(ftype == 'prev') {
        id = (id-1) < 1 ? focusnum : (id-1);
        if(!autoshow) {
            window.clearInterval(focusautoshow);
        }
    } else if(ftype == 'next') {
        id = (id+1) > focusnum ? 1 : (id+1);
        if(!autoshow) {
            window.clearInterval(focusautoshow);
        }
    }
    _id('focuscur').innerHTML = id;
    _id('focus_con').innerHTML = _id('focus_'+(id-1)).innerHTML;
}

function rateStarHover(target,level) {
    if(level ==  0) {
        _id(target).style.width = '';
    } else {
        _id(target).style.width = level * 16 + 'px';
    }
}
function rateStarSet(target,level,input) {
    _id(input).value = level;
    _id(target).className = 'star star' + level;
}

if(typeof IN_ADMINCP == 'undefined') {
    if(creditnotice != '' && getcookie('creditnotice')) {
        _attachEvent(window, 'load', showCreditPrompt, document);
    }
    if(typeof showusercard != 'undefined' && showusercard == 1) {
        _attachEvent(window, 'load', cardInit, document);
    }
}

if(BROWSER.ie) {
    document.documentElement.addBehavior("#default#userdata");
}

function img_onmouseoverfunc(obj) {
    if(typeof showsetcover == 'function') {
        showsetcover(obj);
    }
    return;
}
/**
* 复制工具栏
* 对于双重的工具栏,将工具栏复制到需要重显的位置
*
*/
/**
 * getUser() 获取用户选择框
 * getUser(ctrlObj, _uid, _realname, _username, duration, pos, recall)
 *
 *
 */
var getU_realname,getU_uid,getU_username,getU_num; /*debug:*/
function getUser(ctrlObj,  _uid, _realname, _username, num,  cache,duration, pos, recall){
    getU_uid = isUndefined(_uid) || _uid == '' ? '' : $('#' + _uid);
    getU_realname = isUndefined(_realname) || _realname == '' ? '' :$('#' + _realname);
    getU_username = isUndefined(_username) || _username == '' ? '' :$('#' + _username);
    getU_num = isUndefined(num) || num == '' ? 0 : num;

    var timeout = 0,idclass = 'getuser',contentclass = '';
    if(isUndefined(cache)) cache =  0; //debug:  //cache = 0;
    if(isUndefined(duration)) duration =  3;
    if(isUndefined(pos)) pos = '34!';
    if(isUndefined(recall)) recall = null;
    defaulthref="misc.php?mod=getuser";
    ajaxmenu(ctrlObj,timeout,cache,duration,pos,recall,idclass,contentclass,'getUser');
}


var getP_prcsname,getP_pid,getP_num;
function getPrcs(ctrlObj, _pid, _prcsname, num, cache,duration, pos, recall){

    if(isUndefined(_pid)||_pid==''){
        getP_pid = ''
    }else{
        getP_pid=$("#"+_pid)
    }
    if(isUndefined(_prcsname)||_prcsname==''){
        getP_prcsname=''
    }else{
        getP_prcsname=$("#"+_prcsname)
    }
    if(isUndefined(num)||num==''){
        getP_num=0
    }else{
        getP_num=num
    }
    var timeout = 0,idclass = 'getprcs',contentclass = '';
    if(isUndefined(cache)) cache =  1;
    if(isUndefined(duration)) duration =  3;
    if(isUndefined(pos)) pos = '34!';
    if(isUndefined(recall)) recall = null;
    defaulthref="misc.php?mod=getprcs";

    ajaxmenu(ctrlObj,timeout,cache,duration,pos,recall,idclass,contentclass,'getPrcs');
}
var getP_username,getP_uid,getP_unum;
function getPrcsUser(ctrlObj, _uid, _username, num, cache,duration, pos, recall){
    var random = Math.random();
    if(isUndefined(_uid)||_uid==''){
        getP_uid = ''
    }else{
        getP_uid=$("#"+_uid)
    }
    if(isUndefined(_username)||_username==''){
        getP_username=''
    }else{
        getP_username=$("#"+_username)
    }
    if(isUndefined(num)||num==''){
        getP_unum=0
    }else{
        getP_unum=num
    }
    var timeout = 0,idclass = 'getprcsuser',contentclass = '';
    if(isUndefined(cache)) cache =  1;
    if(isUndefined(duration)) duration =  3;
    if(isUndefined(pos)) pos = '34!';
    if(isUndefined(recall)) recall = null;
    defaulthref="misc.php?mod=getprcsuser&random="+random;
    ajaxmenu(ctrlObj,timeout,cache,duration,pos,recall,idclass,contentclass,'getPrcsUser');
}
/**
 * getDept() 获取部门选择框
 * ajaxmenu(ctrlObj, timeout, cache, duration, pos, recall, idclass, contentclass)
 *
 *
 */
var getD_deptid,getD_deptname,getD_num;
function getDept(ctrlObj, _deptid, _deptname, num, cache, duration, pos, recall){

    getD_deptid = isUndefined(_deptid) || _deptid == '' ? '' : $('#' + _deptid);
    getD_deptname = isUndefined(_deptname) || _deptname == '' ? '' :$('#' + _deptname);
    getD_num = isUndefined(num) || num == '' ? 0 : num;

    var timeout = 500, idclass = 'getdept',contentclass = '';
    if(isUndefined(cache)) cache =  0; //debug:  //cache = 0;
    if(isUndefined(duration)) duration =  2;
    if(isUndefined(pos)) pos = '34!';
    if(isUndefined(recall)) recall = null;
    defaulthref="misc.php?mod=getdept";

    ajaxmenu(ctrlObj,timeout,cache,duration,pos,recall,idclass,contentclass,'getDept');
}
/**
 * getRole() 获取角色选择框
 * ajaxmenu(ctrlObj, timeout, cache, duration, pos, recall, idclass, contentclass)
 *
 *
 */
var getR_roleid,getR_rolename,getR_num;
function getRole(ctrlObj, _roleid, _rolename, num, duration, pos, recall){

    if(isUndefined(_roleid)||_roleid==''){
        getR_roleid=''
    }else{
        getR_roleid=$("#"+_roleid)
    }
    if(isUndefined(_rolename)||_rolename==''){
        getR_rolename=''
    }else{
        getR_rolename=$("#"+_rolename)
    }
    if(isUndefined(num)||num==''){
        getR_num=0
    }else{
        getR_num=num
    }

    var timeout = 1000,cache = 1,idclass = 'getrole',contentclass = '';
    if(isUndefined(duration)) duration =  2;
    if(isUndefined(pos)) pos = '34!';
    if(isUndefined(recall)) recall = null;
    defaulthref="misc.php?mod=getrole";

    ajaxmenu(ctrlObj,timeout,cache,duration,pos,recall,idclass,contentclass,'getRole');
}
/**
 * getPos() 获取岗位选择框
 *
 *
 */
var getP_posid,getP_posname,getP_num;
function getPosition(ctrlObj, _posid, _posname, num, cache, duration, pos, recall){

    getP_posid = (isUndefined(_posid)||_posid=='') ? '' : $("#"+_posid) ;
    getP_posname = (isUndefined(_posname)||_posname=='') ? '' : $("#"+_posname) ;
    getP_num = (isUndefined(num)||num=='') ? 0 : num ;

    var timeout = 500, idclass = 'getpos',contentclass = '';
    if(isUndefined(cache)) cache =  0; //debug:  //cache = 0;
    if(isUndefined(duration)) duration =  2;
    if(isUndefined(pos)) pos = '34!';
    if(isUndefined(recall)) recall = null;
    defaulthref="misc.php?mod=getpos";

    ajaxmenu(ctrlObj,timeout,cache,duration,pos,recall,idclass,contentclass,'getPos');
}


/**
 * getUpload() 获取通用上传框
 * getUpload(ctrlObj, duration, pos, recall)
 *
 *
 */
var getUP_filename,getUP_fileid;
function getUpload(ctrlObj, _filename, _fileid, duration, pos, recall){

    if(isUndefined(_filename)){
        getUP_filename="attach_menu_ul";
    }else{
        getUP_filename=_filename
    }
    if(isUndefined(_fileid)){
        getUP_fileid="attachmentid";
    }else{
        getUP_fileid=_fileid
    }

    var timeout = 500,cache = 1;
    if(isUndefined(duration)) duration =  2;
    if(isUndefined(pos)) pos = '43';
    if(isUndefined(recall)) recall = null;

    ajaxmenu(ctrlObj,timeout,cache,duration,pos,recall);
}
/**
 * getFileFolder() 获取通用选择文件柜框
 * getFilefolder(ctrlObj, _filename, _fileid, duration, pos, recall)
 *
 *
 */
var getF_filename,getF_fileid;
function getFilefolder(ctrlObj,_filename,_fileid, duration, pos, recall){
    var sHref = $(ctrlObj).attr("href");
     if(isUndefined(_filename)){
        getF_filename=$("#attach_menu_ul");
    }else{
        getF_filename=$("#"+_filename)
    }
    if(isUndefined(_fileid)){
        getF_fileid=$("#attachmentid");
    }else{
        getF_fileid=$("#"+_fileid)
    }
    $("#append_parent").iDialog({
        title: '我的文件柜',
        href: sHref,
        width:  680,
        height: 475
    })

/*
    var timeout = 0,cache = 1,idclass = 'getuser',contentclass = '';
    if(isUndefined(duration)) duration =  3;
    if(isUndefined(pos)) pos = '43';
    if(isUndefined(recall)) recall = null;

    ajaxmenu(ctrlObj,timeout,cache,duration,pos,recall,idclass,contentclass,'getFile'); */
}
function addBlockLink(id, tag) {
    if(!document.getElementById(id)) return false;
    var a = document.getElementById(id).getElementsByTagName(tag);
    var taglist = {
        'A':1,
        'INPUT':1,
        'IMG':1
    };
    for(var i = 0, len = a.length; i < len; i++) {
        a[i].onmouseover = function () {
            if(this.className.indexOf(' hover') == -1) {
                this.className = this.className + ' hover';
            }
        };
        a[i].onmouseout = function () {
            this.className = this.className.replace(' hover', '');
        };
        a[i].onclick = function (e) {
            e = e ? e : window.event;
            var target = e.target || e.srcElement;
            if(!taglist[target.tagName]) {
                window.location.href = document.getElementById(this.id + '_a').href;
            }
        };
    }
}
/**
* 检查输入框的字数，超出则截断到maxcount
* @param node 检查的节点
* @param maxcount 最大可输入数值
* @param msg 超出后的提示信息
*/
function maxlength(node,maxcount,msg){
    if(node.value.length > maxcount) {
        node.value = node.value.substr(0,maxcount);
        $("body").iTips({
            content:msg,
            css:'notice'
        });
    }
}
/**
* getUserInfo() 用来显示用户的名片信息
*
*
*/
function getUserInfo(ctrlObj,duration, pos, recall) {
    var timeout = 0,cache = 0;
    if(isUndefined(duration)) duration =  2;
    if(isUndefined(pos)) pos = '34';
    if(isUndefined(recall)) recall = null;
    ajaxmenu(ctrlObj,timeout,cache,duration,pos,recall,'p_pop','userinfoTip');
}
/* zhangrong添加,select显示地级市*/
function showdistrict(container, elems, totallevel, changelevel) {
    var getdid = function(elem) {
        var op = elem.options[elem.selectedIndex];
        return op['did'] || op.getAttribute('did') || '0';
    };
    var pid = changelevel >= 1 && elems[0] && document.getElementById(elems[0]) ? getdid(document.getElementById(elems[0])) : 0;
    var cid = changelevel >= 2 && elems[1] && document.getElementById(elems[1]) ? getdid(document.getElementById(elems[1])) : 0;
    var did = changelevel >= 3 && elems[2] && document.getElementById(elems[2]) ? getdid(document.getElementById(elems[2])) : 0;
    var coid = changelevel >= 4 && elems[3] && document.getElementById(elems[3]) ? getdid(document.getElementById(elems[3])) : 0;
    var url = "home.php?mod=misc&ac=ajax&op=district&container="+container+"&province="+elems[0]+"&city="+elems[1]+"&district="+elems[2]+"&community="+elems[3]
    +"&pid="+pid + "&cid="+cid+"&did="+did+"&coid="+coid+'&level='+totallevel+'&handlekey='+container+'&inajax=1';
    ajaxget(url, container, '');
}

function getExt(path) {
    return path.lastIndexOf('.') == -1 ? '' : path.substr(path.lastIndexOf('.') + 1, path.length).toLowerCase();
}

function sendmobilesms(urlparameter,callback){
    if(isUndefined(callback)){
        callback = function(){
            smssubmit()
        }
    }
    if (!$('#msgwin').get(0)){
        _msgwin = $("<div id='msgwin'></div>");
    }
    if(typeof callback!=='function'){
        return false;
    }
    _msgwin.iDialog({
        title: '发送短信',
        href:'misc.php?mod=sendmobilesms'+urlparameter,
        width:400,
        height:'auto',
        submit:callback
    });
}

/* depend on the jquery.select2 */
function ClearUser(id) {
    $("#"+id).select2("delVal");
}
function ClearDept(id) {
    $("#"+id).select2("delVal");
}
function ClearPos(id) {
    $("#"+id).select2("delVal");
}

/*
* 取得当前网址根路径
*
*/
function getBaseUrl() {
    var baseURL = document.baseURI || document.URL;
    if (baseURL && baseURL.match(/(.*)\/([^\/]?)/)) {
        baseURL = RegExp.$1 + "/";
    }
    return baseURL;
}

/**

* 提示方法i
* 使用方式 $("body").iTips({content:'内容'，css:'warning'})
* 基于jquery插件 jGrowl
* Copyright Aeolus 2011-5-18
**/

$.fn.iTips=function(options){
    var defaults ={
			css : 'notice',			// 外观样式有 notice,warning,error,success
			content : 'loading...',	//内容
			timeout : 2000			//延时关闭时间
		},
		options = $.extend(defaults, options),
		themelist = {
			notice:'alert alert-info',
			success:'alert alert-success',
			warning:'alert alert-warning',
			error:'alert alert-error'
		},
		joptions = {
			theme : themelist[defaults.css],
			life : defaults.timeout
		}
	$.jGrowl(options.content, joptions);
}


/**
 * 封装jQuery EasyUI 1.2.2的 dialog方法, 使用此方法需调用jquery easyUI
 * 使用方法 $('<div id="d"></div>').iDialog({width: 300,height: 200,title: '标题',href:"main.htm"});
 *
 * Copyright Aeolus 2011-5-18
 *
 */

$.fn.iDialog = function (options) {
    var defaults = {
        width: 460,
        height: 320,
        title: 'Message',			//标题
        html: '',					//内容
        iconCls: '',				//标题前的图标
        collapsible: false,        //折叠
        minimizable: false,        //最小化
        maximizable: false,        //最大化
        resizable: false,        //改变窗口大小
        modal: false,            //模态
        shadow: false,          //阴影
        href: null,            //ajax请求内容
        nobtns: false,      //无按钮模式 @denglh 新增
        onOpen: function(){
            $(window).on('keydown', function(e){
                if(e.which == 13){ //enter键

                }else if(e.which == 8){ //escape键
                    this.dialog('close');
                }
            })
        }
     //submit: function () { alert('可执行代码.'); }   可选参数
    };
    var id = $(this).attr('id');
    options = $.extend(defaults, options);
    var self = this;
    if(!options.nobtns) {
        if(options.submit){
            var btns=[{
                text: '<span style="margin-left: -10px">确定</span>',
                iconCls: 'btn btn-primary', //o_cancel,o_confirm
                handler: options.submit
            }, {
                text: '<span style="margin-left: -10px">取消</span>',
                iconCls: 'btn',
                handler: function () {
                    $('#' + id).dialog('close');
                    return false;
                }
            }];
        }else{
            var btns=[{
                text: '<span style="margin-left: -10px">关闭</span>',
                iconCls: 'btn',
                handler: function () {
                    $('#' + id).dialog('close');
                    return false;
                }
            }];
        }
    }
    options.buttons = options.buttons||btns;
    $(self).show();
    $(self).dialog(options);
    function createContent() {
        $('.dialog-content',$(self)).empty().append('<div id="' + id + '_content" style="padding:5px;"></div>');
        $('#' + id + "_content").html(options.html);
    }
    if(options.href){
        if(self.attr("href") ){ // @Aeolus 此方法为了修复href不刷新的BUG
            $(self).dialog('refresh');
        }
        self.attr("href",options.href);
    }else if(options.html){
        createContent();
    }
}

/**
 * 检查页面表单是否有改变，如果有改变将弹出提示询问是否离开
 * 应该习惯把这个函数的调用放到页面最后面，防止其它脚本中途改变表单控件值导致函数产生错误判断
 *
 * @param <string> _range_elem   元素的ID，以此元素在节点树的位置往下搜索数据进行
 * @param <string> _not_checks   不需要检查的元素的ID(点击事件不检查)，可为多个，用','号隔
 * @param <string> _need_checks  额外要检查的元素ID，可选，可多个，用','号分隔
 * @param <json></json>   _data         扩展参数，格式为{'jq选择器表达式' : '属性名', '':''...}，函数会根据选择器匹配到的元素的属性值进行前后比较
 * @example html: <form id="formid">
 					<input type="text" />
 					<input id="submitid" type="submit" />
				  </form>
 			  js: $(document).ready(function(){
			  fromChangeAlert('submitid', 'formid'}));//当点击submit按钮的时候将不会检查，其它情况只要表单有改变都会弹出相应提示
			  其它情况如添加了附件，但是附件列表不属于表单控件，可以用函数的_data扩展更多元素的值进行对比检查
			  附件列表ID： attach_menu_acl_ul
			  _data参数： {'#attach_menu_acl_ul' : 'innerHTML'} //函数会对附件值列表的innerHTML执行比较
 * @Author dlh
 */
var CHECK_FORM_CHANGE = true; //函数根据这个变量判断是否需要检查
function formChangeAlert(_range_elem, _not_checks , _need_checks, _data){
    var range_elem  = $('#'+_range_elem).get(0) ? $('#'+_range_elem).get(0) : document.body,
    not_checks  = _not_checks,
    need_checks = _need_checks,
    data        = _data,
    data_arr    = new Array();

    //默认配置,格式：对象名是jq表达式，值是元素的属性名
    //很多:hidden被JS初始化，导致前后对比的值总是不相同，如果有需要的自己在_data扩展
    var def = {
        ':text':'value',
        ':radio':'checked',
        ':checkbox':'checked',
        'textarea':'value',
        'select':'value'
    };
    $.extend(def, data);
    var hasown = Object.prototype.hasOwnProperty;
    for(key in def) {
        if(hasown.call(def, key)) {
            //data_arr用于存放表单控件信息以及属性值，用于对页面载入和离开时两个状态的表单控件值进行比较
            //格式：第一个是查询元素的jq表达式，第二个是要元素的属性（也就是用来对比的值），第三个是数组（用来存放该属性的值）
            data_arr.push([key, def[key], []]);
        }
    }
    //先执行表单onsubmit,否则表单控件的相应值可能会被编辑器缓存
    var form_arr = $('form', range_elem).get();
    if(range_elem.nodeName == 'form') { //range_elem可能自己也是form
        form_arr.push(range_elem);
    }
    $.each(form_arr, function() {
        var onsubfn = this.onsubmit;
        if(typeof onsubfn == 'function') {
            onsubfn();
        }
    });
    $.each(data_arr, function(i) { //遍历并记录页面载入完成时的控件值
        $(data_arr[i][0], range_elem).each(function() {
            data_arr[i][2].push(this[data_arr[i][1]]);
        });
    });
    if(not_checks != '' && not_checks != undefined) {
        $.each(not_checks.split(','), function(i, o) {
            //用live函数，即使是后期添加到页面的元素都有效
            $('#'+o+',a', range_elem).live('mouseup',function(){ //鼠标键按下返回时
				formNotAlert();
            });
            $('#'+o+',a', range_elem).live('keydown',function(e){ //按键按下
                if(e.which == 13) { //如果是回车键
                    formNotAlert();
                }
            });
        });
    }
    if(need_checks != '' && need_checks != undefined) {
        $.each(need_checks.split(','), function(i, o) {
            $('#'+o).die('mouseup keydown'); //解除可能存在的live事件
            $('#'+o).live('mouseup keydown',function(){
                CHECK_FORM_CHANGE = true;
            });
        });
    }
	$("a[href='javascript:void(0);'],a[href='javascript:;']").live("click",function (){
		formNotAlert();
	});
    window.onbeforeunload = function() {
        if(CHECK_FORM_CHANGE){
            var do_alert = false; //是否弹窗
            //再次执行表单onsubmit事件
            $.each(form_arr, function() { //range_elem可能自己也是form
                var onsubfn = this.onsubmit;
                if(typeof onsubfn == 'function') {
                    onsubfn();
                }
            });
                FLAG:for(var i=0, d_len=data_arr.length; i<d_len; i++) { //用for循环中途可以退出，提高效率
                    //重新搜索元素进行对比
                    var old_v_arr = data_arr[i][2], //页面载入时表单控件的值
                    get_attr  = data_arr[i][1], //要获取的属性
                    new_arr   = $(data_arr[i][0], range_elem); //重新获取表单的控件数组
                    for(var n=0, o_len=old_v_arr.length; n<o_len; n++) { //遍历控件元素与初始值进行比较
                        if(old_v_arr[n] != new_arr[n][get_attr]) { //与初始值不相等
                            do_alert = true;
                            break FLAG;
                        }
                    }
                }
            if(do_alert) {
                return '关闭页面后将无法保存数据，您确定要离开吗？';
            }
        }
    }
}

function formNotAlert() {
	CHECK_FORM_CHANGE = false;
	setTimeout(function(){
		//如果点击后没有触发onbeforeunload，那么重置此参数
		//延迟执行是因为IE如果点击连接会马上触发onbeforeunload事件，这里可以在返回的一段时间CHECK_FORM_CHANGE状态都为false，这样就不会弹窗了
		CHECK_FORM_CHANGE = true;
	}, 200);
}

/**
 *开始导入
 */
var IMP_CONTAINER, IMP_UPLOAD, IMP_SETUP, IMP_FINISH;
function importStart(_imp_module){
    _imp_upload = 'imp_upload',
    _imp_setup  = 'imp_setup',
    _imp_finish = 'imp_finish';
    _imp_container = 'imp_container';
    $('#'+_imp_container).remove(); //debug easyui 连续加载内容再次，所以要先清空
    $('<div id="'+_imp_container+'"></div>').iDialog({
        title:'导入数据',
        width:753,
        height:'auto',
        modal:true,
        nobtns:true,
        href: 'misc.php?mod=import&act=upload&module='+_imp_module+'&rid='+Math.random(),
        onBeforeClose:function(){
            if(confirm('提示：您确定要离开吗？')){
                impDelAttach();
                return true;
            }else{
                return false;
            }
        },
        onLoad:function(){
            if($('#'+_imp_upload).get(0)) { //debug easyui 连续加载内容再次
                return;
            }
            IMP_CONTAINER = $('#'+_imp_container);
            IMP_CONTAINER.wrapInner('<div id="'+ _imp_upload +'"></div>');
            IMP_CONTAINER.append('<div id="'+ _imp_setup +'"></div><div id="'+ _imp_finish +'"></div>');
            IMP_UPLOAD = $('#'+_imp_upload);
            IMP_SETUP  = $('#'+_imp_setup);
            IMP_FINISH = $('#'+_imp_finish);
        }
    });
}

/**
 * 删除附件
 */
function impDelAttach(){
    var url='misc.php?mod=import&act=delattach';
    $.post(url,{aid:$('#attachmentid').val()},function(data){
        $('#attachmentid').val('');
    },'json');
}

/**
 * 开始导出
 */
function exportStart(option) {
    var def = {
        title:'导出设置',
        menuid:'exp_menu',
        formid:'exp_form',
        sendurl:'',
        params:{},
        replacespace:'',
        datasize:{
            'current':'当前数据',
            'all':'全部数据'
        },
        filetype:{
            'csv':'csv格式'
        }
    },
    option = $.extend(true, def, option),
    html = '<div id="'+option.menuid+'" style="display:none;">'+
    '<form method="post" id="'+option.formid+'" action="'+option.sendurl+'">'+
    '<table class="tbl" style="width:100%;">'+
    '<tr>'+
    '<th>格式</th>'+
    '<td><select style="width:120px;" name="exp_file_type">${ft_list}</select></td>'+
    '</tr>'+
    '<tr>'+
    '<th>数据</th>'+
    '<td><select style="width:120px;" name="exp_data_size">${ds_list}</select></td>'+
    '</tr>'+
    '</table>'+
    '${params}'+
    '</form>'+
    '</div>',
    temp = {};
    temp.ft_list = '';
    temp.ds_list = '';
    temp.params = '';
    var opt = Object.prototype;
    for(var key in option.filetype) {
        if(opt.hasOwnProperty.call(option.filetype, key) && option.filetype[key] != 'hide') {
            temp.ft_list += '<option value="'+key+'">'+option.filetype[key]+'</option>';
        }
    }
    for(var key in option.datasize) {
        if(opt.hasOwnProperty.call(option.datasize, key) && option.datasize[key] != 'hide') {
            temp.ds_list += '<option value="'+key+'">'+option.datasize[key]+'</option>';
        }
    }
    for(var key in option.params) {
        if(opt.hasOwnProperty.call(option.params, key) && option.params[key] != '') {
            if(option.replacespace != '' && option.replacespace) {
                option.params[key] = ''.replace.call(option.params[key] ,/\s/g, option.replacespace); //假替换
            }
            temp.params += '<input type="hidden" name="'+key+'" value='+option.params[key]+' />';
        }
    }
    html = html.replace(/\$\{(\w+)\}/g, function($1, $2){
        var tk = temp[$2];
        return typeof(tk) != 'undefined' ? tk : $1;
    });
    $('#'+option.menuid).remove();
    $(document.body).append(html);
    showExportMenu(option);
}

/**
 *显示导出菜单
 */
function showExportMenu(option) {
    var menu = $('#'+option.menuid);
    menu.iDialog({
        title:option.title,
        width:250,
        height:'auto',
        modal:true,
        submit:function(){
            $('#'+option.formid).submit();
            menu.dialog('close');
        }
    });
}

/**
        非IE 事件冒泡处理（mouseenter,mouseleave）
 **/
function enterOrLeave(e, handler) {
    if (e.type != 'mouseout' && e.type != 'mouseover') return false;
    var reltg = e.relatedTarget ? e.relatedTarget : e.type == 'mouseout' ? e.toElement : e.fromElement;
    while (reltg && reltg != handler)
        reltg = reltg.parentNode;
    return (reltg != handler);
}

/**
 * 显示帮助tips标签
 * @param json 显示文本
 * 名称		默认值		必需		说明
 * ctrlobj	无			1		控件ID
 * pk		无			1		全局维一标识
 * text		''			0		文本
 * apos		bottom		0		箭头出现方向 top|left|right|bottom
 * aleft				0		箭头左偏移量
 * atop					0		箭头上偏移量
 * abottom				0		箭头下偏移量
 * pos					0		出现位置 参考showMenu()
 * @author denglh
 */
function showTips(t) {
    //用到了几个JQUERY的方法，但尽量用原生的DOM方法
    if(t.ctrlobj && ckShowGuide(t.pk)) {
        var def = {
            text: '',
            width: 'auto',
            apos: 'bottom',
            aleft: '15px',
            atop: '',
            abottom: '-6px',
            pos: '12!',
            left: '-8px',
            top: '0px',
            rel: true
        }
        if(t.apos == 'left') {
            def.aleft = '-6px';
            def.abottom = '10px';
            def.pos = '23!';
        }
        if(t.apos == 'top') {
            def.aleft = '6px';
            def.atop = '-6px';
            def.pos = '34!';
        }
        var t = $.extend(def, t);
        var html = '';
        //如果该对像没有ID，则附一个ID值给它
        if(!t.ctrlobj.getAttribute('id')) {
            var stamptime = Date.parse(new Date()) + '';
            var tipid = 'showtips_' + stamptime.substr(-7, 4);
            t.ctrlobj.setAttribute('id', tipid);
        }
        var tipid = t.ctrlobj.getAttribute('id');
        var tipmenuid = tipid + '_menu';
        var astyle = t.apos == 'bottom' || t.apos == 'top' ? 'width: 11px;height: 6px;' : 'width: 6px;height: 11px;';
        var html = [];
        html.push("<div id='"+tipmenuid+"'style='position:absolute;z-index:1001;top:0;left:0;display:none;width:"+t.width+";padding:0px;border: 1px solid #B1B1B1;background: #FEFEE9;margin-top: "+t.top+";margin-left:"+t.left+"'>");
        html.push("<div style='position: absolute;left: "+t.aleft+";bottom: "+t.abottom+";top: "+t.atop+";background:url(../../../static/image/common/tip_"+t.apos+".png);"+astyle+"'></div>");
        html.push("<div style='padding: 8px 19px 8px 8px;'>");
        html.push("<div class='tip_c' style='max-width:200px;font-size:12px;color:#444;font-weight:normal;line-height:18px;'></div></div><a href='javascript:hideTips(\""+tipmenuid+"\",\""+t.pk+"\");' style='position: absolute;right: 6px;top: -2px;font-weight:bold;color: #BC9D3C;border:none;padding:0;background:none;' title='关闭'>x</a></div>");
        //父节点改为相对定位
        if(t.rel) {
            t.ctrlobj.style.position = 'relative';
            t.ctrlobj.style.zIndex = '1002';
        }
        $(t.ctrlobj).append(html.join(''));
        $('#'+tipmenuid+' .tip_c').html(t.text);
        _id(tipmenuid).style.display = '';
    }
}

/**
 *隐藏tips
 * @param string tipmenuid 不带tips前缀的tips id
 * @param string pk
 * @author denglh
 */
function hideTips(tipmenuid, pk) {
    _id(tipmenuid).style.display = 'none';
    //默认只显示一次，下次不会再显示
    setcookie('guide_'+pk, -1, 86400 * 30);
}

/**
 * 显示新手引导大图模块
 * @param json g
 * @param json n
 * @param json debug 开启调试，目前只有按钮调试
 * @author denglh
 */
function showGuide(g, n, debug) {
    if(ckShowGuide(g.pk)) {
        //声明一个周期函数变量
        var it;
        if(isUndefined(g)) {
            return;
        }
        if(isUndefined(n)){
            var n = {};
        }
        if(isUndefined(debug)) {
            var debug = false;
        }
        //判断是否宽屏
        if(HTMLNODE.className.indexOf('width960') < 0) {
            //设置为窄屏
            widthauto(_id('widthauto'));
        }
        //设置为窄屏需要一些时间，声明一个函数，用周期判断
        var acfn = function(g, n, debug) {
            //用HTMLNODE节点判断是否浏览器已经切换到窄屏，不能用COOKIE，因为渲染HTML比读取COOKIE要慢，根据HTML节点属性判断比较实时
            //否则有时会出现先弹出新手引导界面，然后网页才会切换的窄屏，那就穿帮了
            if(HTMLNODE.className.indexOf('width960')) {
                var def = {
                    ctrlobj: undefined,
                    img: '',
                    left: '',
                    top: '',
                    rel: true
                }
                var g = $.extend(def, g);
                //如果该对像没有ID，则附一个ID值给它
                if(!g.ctrlobj.getAttribute('id')) {
                    var stamptime = Date.parse(new Date()) + '';
                    var guidid = 'showguide_' + stamptime.substr(-7, 4);
                    g.ctrlobj.setAttribute('id', guidid);
                }
                var guidid = g.ctrlobj.getAttribute('id');
                var guidmenuid = guidid + '_menu';
                if(_id(guidmenuid)) {
                    $('#'+guidmenuid).remove();
                }
                var html = '<div id="'+guidmenuid+'" style="position:absolute;top:0;left:0;z-index:1003;display:none;overflow:hidden;margin-left:'+g.left+';margin-top:'+g.top+';overflow:hidden;"></div>';
                //父节点改为相对定位
                if(g.rel) {
                    g.ctrlobj.style.position = 'relative';
                    g.ctrlobj.style.zIndex = '1004';
                }
                $(g.ctrlobj).append(html);
                var gmenuobj = _id(guidmenuid);
                if(typeof g.img == 'object') {
                    gmenuobj.appendChild(g.img);
                    g.img.style.display = '';
                } else {
                    gmenuobj.innerHTML = '<img src="'+g.img+'" />';
                }
                var coverObj = document.createElement('div');
                coverObj.id = guidmenuid + '_cover';
                coverObj.style.position = 'absolute';
                coverObj.style.zIndex = gmenuobj.style.zIndex - 1;
                coverObj.style.left = coverObj.style.top = '0px';
                coverObj.style.width = '100%';
                coverObj.style.height = Math.max(document.documentElement.clientHeight, document.body.offsetHeight) + 'px';
                coverObj.style.backgroundColor = '#000';
                coverObj.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=30)';
                coverObj.style.opacity = 0.3;
                _attachEvent(window, 'load', function () {
                    coverObj.style.height = Math.max(document.documentElement.clientHeight, document.body.offsetHeight) + 'px';
                }, document);
                $(document.body).append(coverObj);
                gmenuobj.style.display = '';
                if(!isUndefined(n)) {
                    for(var i=0,len=n.length; i<len; i++) {
                        var def = {
                            width: '60px',
                            height: '30px',
                            left: '0px',
                            top: '0px'
                        }
                        var ni = $.extend(def, n[i]);
                        var btn = document.createElement('div');
                        btn.style.width = ni.width;
                        btn.style.height = ni.height;
                        btn.style.position = 'absolute';
                        btn.style.zIndex = '9600';
                        btn.style.left = ni.left;
                        btn.style.top = ni.top;
                        btn.style.cursor = 'pointer';
                        if(!isUndefined(ni.classname)) {
                            btn.className = ni.classname;
                        }
                        //IE的DIV必需有子元素或者背景等才能显示为块且能被点击（测试是这样，不知道是什么原因）
                        //所以应该先加上背景色，再设为完全透明，这样能保证IE下按钮能正常被点击
                        btn.style.backgroundColor = '#38bbc4';
                        if(!debug) {
                            btn.style.opacity = 0;
                            btn.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=0)';
                        }
                        _id(guidmenuid).appendChild(btn);
                        if(typeof ni.callback == 'function') {
                            btn.onclick = function() {
                                hideGuide(guidmenuid, g.pk);
                                ni.callback();
                            }
                        } else {
                            btn.onclick = function() {
                                hideGuide(guidmenuid, g.pk);
                            }
                        }
                    }
                }
                //每次切换新手引导大图的时候滚屏到顶部，这优化了新手引导体验，但同时也可能造成用户体验下降
                //或者可以改为跳到页面中某一个焦点，或者干脆取消
                window.scroll(0,0);
                clearInterval(it);
            }
        }
        it = setInterval(acfn(g, n, debug), 100);
    }
}

/**
 * 隐藏guide
 * @param string guidmenuid 窗口ID
 * @author denglh
 */
function hideGuide(guidmenuid, pk) {
    $('#'+guidmenuid).remove();
    $('#'+guidmenuid+'_cover').remove(); //删除阴影
    setcookie('guide_'+pk, -1, 86400 * 30);
}

/**
 * 设置新手引导tips的cookie
 * @param bool getparm 是否只获取参数
 * @param bool force 是否强制设置
 * @return array
 * @author denglh
 */
function handleTipsCookie(getparm, force) {
    if(getcookie('guide_on') < 0 && !getparm && !force) {
        return;
    } else {
        var TIPSLIST = ['index_setting', 'wiki_adddoc', 'email_addlist', 'diary_addrecord','diary_viewunderling', 'calendar_viewunderling', 'calendar_drag', 'staff_depttree'];
        if(getparm) {
            return TIPSLIST;
        }
        for(var i=0,len=TIPSLIST.length;i<len;i++) {
            var cktips = getcookie(TIPSLIST[i]);
            if(force || (cktips != -1 && cktips != 1)) {
                setcookie('guide_'+TIPSLIST[i], 1);
            }
        }
        setcookie('guide_on', -2);
    }
}

/**
 * 检查是否开启了新手模式
 * @param string pk 唯一键
 * @author denglh
 */
function ckShowGuide(pk) {
    //如果存在guide_pk，即开启了新手模式
    var guide_pk = getcookie('guide_' + pk);
    var guide_pk_php = getcookie('guide_pk_php');
    var guide_pk_js = handleTipsCookie(1);
    var guide_pks = guide_pk_php + ',' + guide_pk_js;
    if(getcookie('guide_on') != -1) {
        //唯一键guide_pk在核心类的ibos_application初始化用户时设置
        //唯一键将被保存在COOKIE中，用来判断用户是否点击关闭，以后不再显示
        //不能使用当前控件的ID作为记录，因为每个页面ID都可能重复，强制性添加唯一键(pk)在程序上可预防疏漏，与某一页的ID重复而导致tips可能无法被显示
        var pks = guide_pks.split(',');
        //如果是唯一键后缀为下划线加数字，则认为这是引导步骤的其中某一步
        if((in_array(pk, pks) && getcookie('guide_' + pk) != -1) || (/\w+_\d+$/).test(pk) && in_array(pk.replace(/_\d+$/, ''), pks)) {
            return true;
        }
    }
    return false
}

/**
 * 图片预加载，可多个
 * @param array list 图片属性列表，数组的每个元素为json对象，对象的属性是图片的属性
 * list数组每个元素的必需属性是有效的src，否则图片无法被加载
 * @param function callback 图片加载完成回调函数，callback参数：imgObj，图片的对象数组
 * @return json 图片的对象数组
 * @example loadImg([{src:'1.png',id="a",...}, {src:'2.png',id="b",...}, ...], callback)
 * @author denglh
 */
function loadImg(list, callback) {
    //思路：新建img节点，设置img节点的onload属性，去除并记录src属性
    //把img节点插入到body中，然后把记录的src重新插入触发onload
    //注：如果在img插入到body之前已存在src属性，IE下不能正常触发onload事件
    var imgObj = [];
    var src = [];
    if(!isUndefined(list)) {
        var hasOwn = Object.prototype.hasOwnProperty;
        for(var i = 0, len = list.length; i < len; i++) {
            var img = new Image();
            img.style.display = 'none';
            img.style.width = 'auto';
            img.style.height = 'auto';
            for(var key in list[i]) {
                if(hasOwn.call(list[i], key)) {
                    img[key] = list[i][key];
                }
            }
            src[i] = img.src;
            //去除src属性
            img.src = undefined;
            imgObj.push(img);
        }
    }
    var setonLoad = function(i) {
        if(imgObj[i]) {
            //先插入节点
            document.body.appendChild(imgObj[i]);
            //如果有下一张图片，则在当前图片加载完成时再加载下一张图片
            if(imgObj[i+1]) {
                imgObj[i].onload = function(){
                    setonLoad(i+1);
                }
				//遇到错误继续加载下一张图片
				imgObj[i].onerror = function(){
                                        var current = this;
					current.loaderror = true;
					//取消错误事件，防止死循环,IE7以下会有问题，要使用setTimeout
					setTimeout(function() {
                                            delete current.onerror;
					}, 0);
					setonLoad(i+1);
				}
            //否则执行回调函数
            } else if(callback && typeof callback == 'function') {
                imgObj[i].onload = function() {
                    callback(imgObj);
                }
				//遇到错误也保证回调函数正常执行
				imgObj[i].onerror = function(){
                                        var current = this;
					current.loaderror = true;
					//取消错误事件，防止死循环,IE7以下会有问题，要使用setTimeout
					setTimeout(function() {
                                            delete current.onerror;
					}, 0);
					callback(imgObj);
				}
            }
			//改变连接属性触发加载图片事件
			imgObj[i].src = src[i];
        }
    }
	var timesetload = function() {
		var callee = arguments.callee;
		setTimeout(function() {
			//在页面加载完成时才执行
			if(document.body.appendChild) {
				setonLoad(0);
			} else {
				callee();
			}
		}, 50);
	}
	timesetload();
	return imgObj;
}


/**
 * TODO::应该注明作者、参数等信息，没有用的代码应该删除，而不是注释掉
 * Depend: JQuery, Select2(jq plugin)
 * 此函数为基于Select2的表单初始化私有方法，请不要在通常环境下调用
 * @param element
 * @param isDept 如果为空或者未填写表示是选人，如果不为空并且等于dept说明是选部门，否则为选职位
**/
function _initFormSelection(element, isDept){
    var aSrcData = ( isUndefined(isDept) || isDept == '' )  ?  gStaffData  : ( isDept=='dept' ? gDeptData : gPosData ) ;
    var data = [], sUid = $(element).val(), aUid = [], sFormatUid='';
    sFormatUid=sUid.replace(/,$/gi,"");   //去掉初始value后面的逗号
    $(element).val(sFormatUid);
    aUid = sFormatUid.split(',');
    for(var i=0; i<aUid.length; i++){
        var sid = aUid[i], sName = '' ;
        //if( !sid || isNaN(sid) ) continue;
        if( isUndefined(aSrcData[sid]) ) {
            continue;
        }else {
            sName = aSrcData[sid].text ;
        }
         data.push({
            id:sid,
            text: sName,
            available:1
        });
    }
    return data;
}

/**
    @param:     boolean  g  true为阻止选择文本，false为允许选择文本，默认为true;
    @example:  $("body").noSelect();
    @explain:    此函数用于阻止浏览器默认的文本选择状态，调用的对象将不能划选文字
**/
$.fn.noSelect = function(g) {   //阻止选择html文本
    return (prevent = g == null ? true : g) ? this.each(function() {
        if ($.browser.msie || $.browser.safari) $(this).bind("selectstart",  //for IE及safari，支持onselectstart方法
            function() {return false});
        else if ($.browser.mozilla) {  //for FF，支持user-select样式
            $(this).css("MozUserSelect", "none");
            $("body").trigger("focus")
        }else if($.browser.chrome){ //for chrome
            $(this).css("WebkitUserSelect", "none");
            $("body").trigger("focus")
        }
        else $.browser.opera ? $(this).bind("mousedown",
        function() {
            return false
        }) : $(this).attr("unselectable", "on")
    }) : this.each(function() {
        if ($.browser.msie || $.browser.safari) $(this).unbind("selectstart");
        else if ($.browser.mozilla) $(this).css("MozUserSelect", "inherit");
        else if ($.browser.chrome) $(this).css("WebkitUserSelect", "inherit");
        else $.browser.opera ? $(this).unbind("mousedown") : $(this).removeAttr("unselectable", "on")
    })
};


/**
 * 设置系统代码，调用本函数将显示一个可设置和显示某一系统代码的列表窗口
 * 如果想在关闭窗口时及时更新这个系统代码信息到某个下拉列表的选项，则需要oSelect参数
 * oSelect是一个select元素的对象，关闭窗口时函数会尝试更新这个select对象下的选项
 *
 * @param string sTitle 标题
 * @param string sPnum 系统代码父类代码编号
 * @param object oSelect 可选 select元素对象
 * @param string sExtra 可选 select对象下的option元素的属性，可设置任何合法的HTML属性，例 sExtra = 'class="disable" title="呵呵"'
 * @author denglh
 */
 var goSysCodeBox = $('<div id="syscodebox"></div>');
function fnSetSysCode(sTitle, sPnum, oSelect, sExtra) {
	//iDalog基本配置
	var oDialogSetting = {
		title : sTitle,
		href : 'misc.php?mod=syscode&act=list&pnumber='+sPnum,
		height : 'auto',
		width : 500
	}
	//如果存在下拉列表元素对象（oSelect)，关闭窗口时尝试更新这个下拉列表的选项
	if(Object.prototype.toString.call(oSelect) == '[object HTMLSelectElement]') {
		//iDialog在关闭时动作
		oDialogSetting.onClose = function() {
			fnSetSysCodeOption(sTitle, sPnum, oSelect, sExtra);
		}
	}
	//加载设置窗口
	goSysCodeBox.iDialog(oDialogSetting);
}

/**
 * 设置系统代码下拉选项
 *
 * @param string sTitle 提示类型标题
 * @param string sPnum 系统代码父类代码编号
 * @param object oSelect select元素对象
 * @param string sExtra select对象下的option元素的属性，可设置任何合法的HTML属性，例 sExtra = 'class="disable" title="呵呵"'
 * @author denglh
 */
function fnSetSysCodeOption(sTitle, sPnum, oSelect, sExtra) {
	if(typeof oSelect != 'object') {return false;}
	//保存此选项的HTML，在更新失败时可还原这些选项
	var sOldOptionHtml = oSelect.innerHTML;
	//保存选中的值，更新成功时下可还原之前选择的那一项（如果还存在的话）
	var sOldSelectValue = oSelect.value;
	/**
		* 设置option的HTML
		* @param object oData 选项的数据
		* @param string sExtra 每个选项的属性
		*/
	var fnSetOptionHtml = function(oData, sExtra) {
		var sOptionHtml = '',
			sSetExtra = sExtra || '',
			fnHasOwn = Object.prototype.hasOwnProperty;
		for(var nCodeId in oData) {
			if(fnHasOwn.call(oData, nCodeId)) {
				//尝试还原之前选择的那一项
				var selected = (oData[nCodeId].number == sOldSelectValue) ? ' selected="selected"' : '';
				sOptionHtml += '<option value="'+oData[nCodeId].number+'" '+sSetExtra+selected+'>'+oData[nCodeId].name+'</option>';
			}
		}
		return sOptionHtml;
	}
	var _fnSetSysCodeOption = function() {
		var _fnSetSysCodeOptionCallee = arguments.callee;
		//选项在更新时不可用，并显示一个更新中的提示信息
		oSelect.innerHTML = '<option>正在更新...</option>';
		$.get(
			'misc.php?mod=syscode&act=getinfo&pnumber='+sPnum,
			function(result) {
				if(result && !result.error) {
					//jquery会自动将json对象的元素顺序根据键值由小至大排列
					//但实际上我们想要的是根据json对象的sort属性排列输出，所以有必要对其进行多一次排序
					//将json转为类数组对象
					var aResultData = $.map(result.data, function(row) {
						return row;
					});
					//用数组的sort方法根据sort属性由小至大重新排列
					Array.prototype.sort.call(aResultData, function(s, e) {
						return s.sort > e.sort;
					});
					var sOptionHtml = fnSetOptionHtml(aResultData, sExtra);
					oSelect.innerHTML = sOptionHtml;
				} else {
					var sMsg = '<div style="float:left;">更新'+sTitle+'失败，信息：<br />&nbsp;&nbsp;&nbsp;&nbsp;' +
							result.message + '<br />是否尝试再次更新？</div>';
					$.messager.confirm('错误提示', sMsg, function(r) {
						if(r) {
							//尝试再次执行更新动作
							_fnSetSysCodeOptionCallee();
						} else {
							//还原选项到初始状态
							oSelect.innerHTML = sOldOptionHtml;
						}
					});
				}
			},
		'json');
	}
	_fnSetSysCodeOption();
}

/**
 * 以某一个对象作为原型创建对象的函数
 * @param object: oProto 以这个对象为原型
 * @return object 返回类对象
 * @author denglh
 */
function fnObjectCreate(oProto) {
	//ES5中以null为原型创建的类含有某些特性，为了保持兼容将禁止使用null作为原型
	oProto = (typeof oProto == 'object' && oProto !== null) ? oProto : {};
	//create是ES5方法，如果存在直接使用
	if(Object.create) {
		return Object.create(oProto);
	} else {
		var fn = function() {}
		fn.prototype = oProto;
		return new fn();
	}
}

/**
 * @param   ctrl(Object)搜索输入框, func(Function)按enter后触发的函数
 * @context  列表页右上方的搜索栏，只以focus事件触发
 * @author Ink
 */
function fnSearchOperate(ctrl,func){
    //var  oSearchBtn = ctrl.nextSibling;
    //var  oIco = oSearchBtn.childNodes[0];
    var oSearchBtn = $(ctrl).next();
    var oIco = oSearchBtn.children();
    //oIco.className = "i_g i-sc-enter";
    oIco.attr("class","i_g i-sc-enter");
    ctrl.onkeydown = function(event){
        var myEvent = window.event || event;
        if(myEvent.keyCode == 13){
            if(trim(ctrl.value)==""){
                $("body").iTips({content:"请输入搜索关键词", css:'error'});return false;
            }
            if(oSearchBtn.attr("href")){
                var sLocation = oSearchBtn.attr("href");
                sLocation = sLocation + (sLocation.indexOf("?") == -1 ? "?k=" : "&k=") + ctrl.value;
            }else{
                func(ctrl);
            }
        }
    }
    ctrl.onblur = function(){
        //oIco.className = "i_g i-sc-config";
        oIco.attr("class","i_g i-sc-enter");
    }
}

/* Fixed页面固定层，未完成 */
/* function fnFixedDiv(id, evt){
    var obj = _id(id), objTemp, nScrollTop;
    if(!evt){
        objTemp = _id(id + "_temp")||null;
        _attachEvent(window, 'scroll',  function(){
           // obj = _id(id);
            nScrollTop =  document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop;
            if(nScrollTop > obj.offsetTop){
                if(!objTemp){
                    objTemp = document.createElement("div");
                    objTemp.id = obj.id + "_temp";
                    objTemp.style.height = obj.offsetHeight + "px";
                    objTemp.style.visibility = "hidden";
                    obj.parentNode.insertBefore(objTemp, obj)
                }else{
               //     obj.style.display = "";
                }
                obj.style.position = "fixed";
                obj.style.top = 0;
            }else{
               // obj.style.position = objTemp.style.position;
              //  objTemp.style.display = "none";
            }
        })
    }
} */

function showHeaderMenu(o, px){ //显示菜单水平移动
    var oMenu = _id(o.menuid), px = px||15;
    showMenu(o);
    with(oMenu.style){
        position = 'fixed';
        top = '30px';
        left = (oMenu.offsetLeft + px) + 'px';
    }
}


function CNDate(time){  //农历类，农历时间各种方法
    this.time = time||new Date();
}
CNDate.prototype = {
    getDay: function(){ //星期
        var dt = this.time;
        return "星期" + ("日一二三四五六".charAt(dt.getDay()))
    },
    getDateNumOfYear: function(){   //一年中的第几天
        var y = this.time.getFullYear();
        return parseInt((Date.parse(this.time)-Date.parse(y+"/1/1"))/86400000)+1;
    },
    getTime: function(){
        var dt = this.time,
        CnData = [
            0x16,0x2a,0xda,0x00,0x83,0x49,0xb6,0x05,0x0e,0x64,0xbb,0x00,0x19,0xb2,0x5b,0x00,
            0x87,0x6a,0x57,0x04,0x12,0x75,0x2b,0x00,0x1d,0xb6,0x95,0x00,0x8a,0xad,0x55,0x02,
            0x15,0x55,0xaa,0x00,0x82,0x55,0x6c,0x07,0x0d,0xc9,0x76,0x00,0x17,0x64,0xb7,0x00,
            0x86,0xe4,0xae,0x05,0x11,0xea,0x56,0x00,0x1b,0x6d,0x2a,0x00,0x88,0x5a,0xaa,0x04,
            0x14,0xad,0x55,0x00,0x81,0xaa,0xd5,0x09,0x0b,0x52,0xea,0x00,0x16,0xa9,0x6d,0x00,
            0x84,0xa9,0x5d,0x06,0x0f,0xd4,0xae,0x00,0x1a,0xea,0x4d,0x00,0x87,0xba,0x55,0x04
        ],
        CnMonth = [], CnMonthDays = [], CnBeginDay, LeapMonth, Bytes = [], i,  CnMonthData,
        DaysCount, CnDaysCount, ResultMonth, ResultDay, yyyy=dt.getFullYear(),
        mm=dt.getMonth()+1, dd=dt.getDate();

        if(yyyy<100) yyyy+=1900;
        if ((yyyy < 1997) || (yyyy > 2020)){
            return 0;
        }
        Bytes[0] = CnData[(yyyy - 1997) * 4];
        Bytes[1] = CnData[(yyyy - 1997) * 4 + 1];
        Bytes[2] = CnData[(yyyy - 1997) * 4 + 2];
        Bytes[3] = CnData[(yyyy - 1997) * 4 + 3];
        if ((Bytes[0] & 0x80) != 0) {CnMonth[0] = 12;}
        else {CnMonth[0] = 11;}
        CnBeginDay = (Bytes[0] & 0x7f);
        CnMonthData = Bytes[1];
        CnMonthData = CnMonthData << 8;
        CnMonthData = CnMonthData | Bytes[2];
        LeapMonth = Bytes[3];
        for (i=15;i>=0;i--){
            CnMonthDays[15 - i] = 29;
            if (((1 << i) & CnMonthData) != 0 ){
                CnMonthDays[15 - i]++;
            }
            if (CnMonth[15 - i] == LeapMonth ){
                CnMonth[15 - i + 1] = - LeapMonth;
            }else{
                if (CnMonth[15 - i] < 0 ){CnMonth[15 - i + 1] = - CnMonth[15 - i] + 1;}
                else {CnMonth[15 - i + 1] = CnMonth[15 - i] + 1;}
                if (CnMonth[15 - i + 1] > 12 ){CnMonth[15 - i + 1] = 1;}
            }
        }
        DaysCount = this.getDateNumOfYear(dt) - 1;
        if (DaysCount <= (CnMonthDays[0] - CnBeginDay)){
            if ((yyyy > 1901) && (CnDateofDate(new Date((yyyy - 1)+"/12/31")) < 0)){
                ResultMonth = - CnMonth[0];
            }else{
                ResultMonth = CnMonth[0];
            }
            ResultDay = CnBeginDay + DaysCount;
        }else{
            CnDaysCount = CnMonthDays[0] - CnBeginDay;
            i = 1;
            while ((CnDaysCount < DaysCount) && (CnDaysCount + CnMonthDays[i] < DaysCount)){
                CnDaysCount+= CnMonthDays[i];
                i++;
            }
            ResultMonth = CnMonth[i];
            ResultDay = DaysCount - CnDaysCount;
        }
        if (ResultMonth > 0){
            return ResultMonth * 100 + ResultDay;
        }else{
            return ResultMonth * 100 - ResultDay;
        }
    },
    getYear: function(){ //农历年份
        var dt = this.time,
            YYYY= dt.getFullYear(),
            MM=dt.getMonth()+1,
            CnMM=parseInt(Math.abs(this.getDate(dt))/100);
        if(YYYY<100) YYYY+=1900;
        if(CnMM>MM) YYYY--;
        YYYY-=1864;
        return CnEra(YYYY)+"年";
    },
    getMonth: function(){ //农历月份
        var dt = this.time,
            sMonthStr = ["零","正","二","三","四","五","六","七","八","九","十","冬","腊"],
            nMonth;
        nMonth = parseInt(this.getTime(dt)/100);
        if (nMonth < 0){
            return "闰" + sMonthStr[-nMonth] + "月";
        }else{
            return sMonthStr[nMonth] + "月";
        }
    },
    getDate: function(){    //农历日期
        var dt = this.time,
        sDayStr=["零",
            "初一", "初二", "初三", "初四", "初五",
            "初六", "初七", "初八", "初九", "初十",
            "十一", "十二", "十三", "十四", "十五",
            "十六", "十七", "十八", "十九", "二十",
            "廿一", "廿二", "廿三", "廿四", "廿五",
            "廿六", "廿七", "廿八", "廿九", "三十"];
        var nDay =  (Math.abs(this.getTime(dt)))%100;
        return sDayStr[nDay];
    },
    getDateCountOfMonth: function(){ //月份日数
        var dt = this.time,
        nCMonth = nNmonth= dt.getFullYear();
        nCMonth+="/"+(dt.getMonth()+1);
        nNmonth+="/"+(dt.getMonth()+2);
        nCMonth+="/1";
        nNmonth+="/1";
        return parseInt((Date.parse(nNmonth)-Date.parse(nCMonth))/86400000);
    },
    getEra: function(YYYY){   //天干地支年 -_-*
        var aTiangan = ["甲","乙","丙","丁","戊","己","庚","辛","壬","癸"],
            aDizhi= ["子","丑","寅","卯","辰","巳","午","未","申","酉","戌","亥"];
        return Tiangan[YYYY%10]+Dizhi[YYYY%12];
    },
    getCurrentTime: function(){ //当前时间
        var dt = new Date(),
            hh = dt.getHours(),
            mm = dt.getMinutes(),
            ss = dt.getTime() % 60000,
            clock;
        ss = (ss - (ss % 1000)) / 1000;
        clock = hh+':';
        if (mm < 10) clock += '0';
        clock += mm+':';
        if (ss < 10) clock += '0';
        clock += ss;
        return(clock);
    }
}

function fnInitTime(id){ //id: 容器ID
    var oWrap = _id(id), oEle, aEleHtml = [],
    dt = new Date(), cndt = new CNDate();
    if(!oWrap)return false;
    oEle = _id("time_panel");
    if(!oEle){
        oEle = document.createElement("div");
        oEle.id = "time_panel";
        oEle.className = "y time-panel";
        oWrap.appendChild(oEle);
    }
    aEleHtml.push("<span>" + (dt.getMonth() + 1) +"月");
    aEleHtml.push(dt.getDate() + "日" + cndt.getDay() + "</span>");
    aEleHtml.push("<span>农历" + cndt.getMonth() + cndt.getDate() + "</span>");
    aEleHtml.push("<span><em id='clock'></em></span>");
    oEle.innerHTML = aEleHtml.join("");
    setInterval(function(){
        var dt = new Date();
        _id('clock').innerHTML = cndt.getCurrentTime();
        if(dt.getHours()  == 0 && dt.getMinutes() == 0 && dt.getSeconds() == 0){fnInitTime()};
    },1000);
}

/**
 * @explain 弹出页面显示详细内容;
 * @example 信息中心查看详细页
 * @param popUp.view(obj) 为触发元素
    view方法为ajax加载弹出页，显示内容
    close方法关闭弹出页
    maximize方法最大化页面
**/
var popUp = {
    isMax: 0,
    maxWrap: "",
    view: function(obj){
        var oWrap = $("#popup_wrap"),
            oContent = $("#loadcontent"),
            aHtmlStr = [];
        if(oWrap.length == 0){
            oWrap = $("<div></div>");
            oWrap.attr("id", "popup_wrap").attr("class", "mainer cl posr");
            aHtmlStr.push("<div class='popup-operate'><a href='"+ obj.href +"' target='_blank' id='totrueurl'><i class='layerbtnt alllayerbtn'></i></a>");
            aHtmlStr.push("<a href='javascript:;' onclick='popUp.maximize()'><i class='layerbtnm alllayerbtn'></i></a>");
            aHtmlStr.push("<a href='javascript:;' onclick='popUp.close()'><i class='layerbtnc alllayerbtn'></i></a></div>");
            oWrap.html(aHtmlStr.join(""));
            oContent = $("<div id='loadcontent' class='popup-inner'></div>").appendTo(oWrap);
            oWrap.appendTo($("#container")).hide();
        }
        $.get(obj.href + "&isload=1&ajax=1&randid="+Math.random(),function(data){
            oWrap.fadeIn();
            $("#mainer").hide();
            oContent.html(data);
        });
    },
    close : function(){
        if(popUp.isMax==1){ //关闭动作分为先缩小窗口,再关闭
            popUp.maximize();
        }
        $("#mainer").fadeIn().siblings("#popup_wrap").hide(); //先显示后隐藏可防止页面跳动
    },
    maximize : function(){
        if(popUp.isMax==0){
            $("#popup_wrap").attr("class","wp posr").appendTo($("body"))
            $("#toper,#header,#wrap,#footer").hide();
            popUp.isMax=1;
        }else{
            $("#popup_wrap").attr("class","mainer cl posr").appendTo($("#container"));
            $("#toper,#header,#wrap,#footer").show();
            popUp.isMax=0;
        }
    }

}

/**
 * @explain 解决IE下不支持placeholder属性的问题;
 * @example <input id="a" placeholder="aaa" /><script>fnPlaceholder(_id("a"))</script>
 * @param el: (obj) 使用对象
**/
function fnPlaceholder(el) {
    var placeholder = '';
    if (el && !("placeholder" in document.createElement("input")) && (placeholder = el.getAttribute("placeholder"))) {
        _attachEvent(el, 'focus', function() {
            if (el.value === placeholder) {
                el.value = "";
            }
            el.style.color = '';
        })
        _attachEvent(el, 'blur', function() {
            if (el.value === "") {
                el.value = placeholder;
                el.style.color = 'graytext';
            }
        })
        //样式初始化
        if (el.value === "") {
            el.value = placeholder;
            el.style.color = 'graytext';
        }
    }
};

function fnShowSelectAd(){
    //var curDate = new Date();
    setTimeout(function(){
    $("body").fadeOut(3000,function(){
        var objBody = $(this);
        objBody.html("").slideDown("normal").css("background-color","#F5F5F5");
        var s = "%u5BF9%u4E8E%u6211%uFF0CIBOS%u662F%u6211%u4EBA%u751F%u7684%u4E00%"+
        "u6BB5%u4E0D%u53EF%u78E8%u706D%u7684%u804C%u4E1A%u751F%u6DAF%u3002";
        var tMlast = unescape(s);
        var tF = ["&nbsp;For me","&nbsp;IBOS","&nbsp;is my life for some indelible career.","&nbsp;"+tMlast];
        var tM = ["margin: 100px 0px 0px 395px; width: 60px; height: 20px;","margin: 100px 0px 0px 0px; width: 36px; height: 20px;",
            "margin: 100px 0px 0px 0px; width: 300px; height: 20px;","margin: 120px 0px 0px 392px; width: 315px; height: 20px;"];
        var tempStyle = "display:none; "+tM[0]+" background-color: white; box-shadow: 2px 4px 2px #999999;float : left;";
        var sDian = ".";
        var myDiv = $("<div style='"+tempStyle+"' id='myDiv'></div>");var fn_tF1 = null;myDiv.appendTo(objBody).html(tF[0]).slideDown("slow",function(){
            var tempLen = tF[0].length;fn_tF1 = setInterval(function(){if(myDiv.html().length<tempLen+3){myDiv.append(sDian);}else{myDiv.html(tF[0]);}},
            1000);});var tempStyle = "display:none; "+tM[1]+"text-shadow: 2px 4px 2px #999999; float : left;";var myDiv2 = $("<div style='"+tempStyle+"' id='myDiv2'></div>");
        myDiv2.html(tF[1]).appendTo(objBody);setTimeout(function(){clearInterval(fn_tF1);myDiv2.slideDown("slow",function(){myDiv.hide("normal",function(){$(myDiv).html(tF[0]+",")
            .css("width","52px").css("background-color","").css("box-shadow","").css("text-shadow","2px 4px 2px #999999");$(this).show("normal",function(){setTimeout(function(){
            var tempStyle = "display:none; "+tM[2]+ "text-shadow: 2px 4px 2px #999999; float : left;";var myDiv3 = $("<div style='"+tempStyle+"' id='myDiv3'></div>");
            myDiv3.html(tF[2]).appendTo(objBody);setTimeout(function(){myDiv3.slideDown("slow",function(){setTimeout(function(){
            var tempStyle = "display:none; "+tM[3]+" background-color: white; "+"box-shadow: 2px 4px 2px #999999; position: absolute;";
            var myDiv4 = $("<div style='"+tempStyle+"' id='myDiv4'></div>");myDiv4.html(tF[3]).appendTo(objBody);$(myDiv4).fadeIn(3000,function(){setTimeout(function(){
            $(myDiv3).slideUp("slow",function(){$(myDiv2).slideUp("slow",function(){$(myDiv).slideUp("slow",function(){setTimeout(function(){
            $(myDiv4).hide("slow",function(){setTimeout(function(){window.location.href = window.location.href.replace(/(\?shad=)[\d\D]+/g,"");},1000);});},2000);});});});},3000);});},4000);});},3000);},3000);});});});}
        ,6000);});}
    ,2000);
}
/**
 * Select元素选择视图
 * @param object oSetting
 * @example oSetting.oData参数格式：
 * 单个: {name: '名字', id: 'id', url: '图片地址'}
 * 多个: {
 *	'id': {name: '名字', id: 'id', url: '图片地址'},
 *	'id': {name: '名字', id :'id', url: '图片地址'}
 *	 ...
 * }
 * 如果相鼠标停留在图片时显示相关信息，则oData中的每条信息需要detail参数，参数个数不限
 * {name: '名字', id: 'id', url: '图片地址', detail: {
 *		'标题': '内容',
 *		'标题': '内容',
 *		...
 *
 * }}
 * @version: 0.9
 * @author denglh
 * TODO::底部不一定是预约，底部信息可作为外部扩展接口
 */
function fnSelectView(oSetting) {
	var oDefSetting = {
		oMain: null,									//容器对象
		nWidth: '200px',								//显示宽度
		oData: {},										//信息的对象列表，键名是id
		bShowHeader: false,								//显示头部
		bShowAppointment: false,						//显示预约数
		fnAppointmentBtnCall: function(nId) {},		//预约按钮点击回调函数
		sSelectName: '',								//下拉框的name值（需要多个信息才有效）
		sDefSelect: '',									//默认选中值（需要多个信息才有效）
		sDefImgUrl: 'static/image/common/default.png'	//默认图片
	}
	var oSet = $.extend(oDefSetting, oSetting);
	var bShowOne = true;
	for(var nId in oSet.oData) {
		if(parseInt(nId) == nId) {
			bShowOne = false;
		}
		//马上注销并退出
		nId = undefined;
		break;
	}
	//创建主容器
	var oMain = $('<div></div>').width(oSet.nWidth).appendTo(oSet.oMain);

	if(oSet.bShowHeader) {
		if(bShowOne) {
			oMain.append( $('<div class="sl-view-header">' + oSet.oData.name + '</div>').width(oMain.width() - 2) );
		} else {
			//（头部）创建选择列表，并绑定加载信息事件
			var oSelect = $('<select class="sl-view-input" name="'+oSet.sSelectName+'"></select>');
			//创建选择列表的选项
			var aSelsetOpts = [];
			var fnHasOwn = Object.prototype.hasOwnProperty;
			for(var nId in oSet.oData) {
				if(fnHasOwn.call(oSet.oData, nId)) {
					aSelsetOpts.push('<option value="'+nId+'">'+oSet.oData[nId].name+'</option>');
				}
			}
			oMain.append( oSelect.append( aSelsetOpts.join('') ) );
		}
	}
	oMain.append( $('<div class="sl-view-imgmain"><div class="sl-view-img-submain"><img class="sl-view-img" src="'+oSet.sDefImgUrl+'" /><div class="sl-view-msg-cover" style="display: none;"><div class="sl-view-msg">加载中...</div></div></div></div>') );

	oSet.bShowAppointment && oMain.append($('<div class="sl-view-bottom"><span class="sl-view-appointment-total"><em>0</em><span>预约</span></span><button type="button" class="sl-view-appointment-btn y w_40 btn btn-primary btn-mini">查看</button></div>'));

	if(bShowOne) {
		//初始化图片
		fnShowImg(oSet.oData.id);
	} else {
		//绑定和初始化选择器事件
		//程序在加载图片时会显示一个提示加载的遮罩，当图片加载完成时遮罩消失，显示图片并带有淡出效果

		//记录程序初始化，如果程序在页面加载完成后初始化
		var bInitSelectChange = false;
		var oSelect = oMain.find('.sl-view-input').change(function() {
			var nId = this.value;
			var oImgMain = oMain.find('.sl-view-imgmain');
			fnShowImg(nId);
			oImgMain.mouseout();
			if((!oSetting.sDefSelect && nId) || (oSetting.sDefSelect && bInitSelectChange)) {
				oImgMain.mouseover();
			}
			bInitSelectChange = true;
		}); //初始化选项并触发change事件
		if(oSet.sDefSelect) {
			oSelect.val(oSet.sDefSelect);
		}
		oSelect.change();
	}

	//绑定鼠标经过事件，显示详细信息
	//TODO::每次鼠标经过时元素字节都是实时生成的，有时间可以优化一下，使用缓存
	var oShowDetailEvent;
	oMain.find('.sl-view-imgmain').hover(
		function() {
			var oImgMain = $(this);
			//清空延迟事件
			clearTimeout(oShowDetailEvent);
			var sSlVal = oMain.find('.sl-view-input').val();
			if(bShowOne || sSlVal) {
				var oData = (!bShowOne && oSet.oData[sSlVal]) || oSet.oData;
				var oDetail = oData.detail;
				if(oDetail) {
					//处理文本内容
					var sDetailHtml = '';
					var fnHasOwn = Object.prototype.hasOwnProperty;
					for(var sKey in oDetail) {
						if(fnHasOwn.call(oDetail, sKey)) {
							sDetailHtml += '<tr><th>' + sKey + '</th><td>' + oDetail[sKey] + '</td></tr>';
						}
					}
					sDetailHtml = '<div class="sl-view-detail"><table>' + sDetailHtml + '</table><span class="sl-view-arrows"></span></div>';
					oShowDetailEvent = setTimeout(function() {
						$(sDetailHtml).css({'right': oMain.width() + 12 + 'px'}).appendTo(oImgMain);
					}, 500);
				}
			}
		}, function() {
			var oImgMain = $(this);
			//清空延迟事件
			clearTimeout(oShowDetailEvent);
			setTimeout(function() {
				oImgMain.find('.sl-view-detail').remove();
			}, 500);
		}
	);

	if(typeof oSet.fnAppointmentBtnCall == 'function') {
		oMain.find('.sl-view-appointment-btn').click(function() {
			oSet.fnAppointmentBtnCall(oMain.find('.sl-view-input').val());
		});
	}

	/**
	 * 显示图片
	 * @param int nId ID
	 */
	function fnShowImg(nId) {
		//声明一个变量记录图片是否已加载
		var oIsLoad = arguments.callee.oIsload;
		if(!oIsLoad) {
			oIsLoad = arguments.callee.oIsload = {};
		}
		var oData = (!bShowOne && oSet.oData[nId]) || oSet.oData;
		var bHasImg = !!(nId && oData.img && oData.img.url);
		//开始加载图片
		//设置图片URL
		var sUrl = (nId && oData.img && oData.img.url) || oSet.sDefImgUrl;
		var oImg = oMain.find('.sl-view-img').removeClass('defimg');
		if(!oIsLoad[nId] && bHasImg) {
			oMain.find('.sl-view-msg-cover').show();
			loadImg([{'src': sUrl}], function() {
				oMain.find('.sl-view-msg-cover').fadeOut(200);
				oImg.hide().attr('src', sUrl).fadeIn(200);
				oIsLoad[nId] = true;
			});
		} else {
			if(bHasImg) {
				oImg.hide().attr('src', sUrl);
			} else {
				//没有图片只能删除再添加新的了，因为默认图片用的是CSS，不能直接删除src属性，浏览器会显示一个叉
				oImg.hide().remove();
				oImg = oMain.find('.sl-view-img-submain').prepend('<img class="sl-view-img defimg" />');
			}
			oImg.fadeIn(200);
		}
		//绑定查看大图事件
		//点击事件与命名空间
		var sClickBySpace = 'click.info.meeting';
		if(bHasImg) {
			oMain.find('.sl-view-img-submain').css({'cursor': 'pointer'})
				.unbind(sClickBySpace)
				.bind(sClickBySpace, function() {
					zoom(oMain.find('.sl-view-img').get(0));
			});
		} else {
			oMain.find('.sl-view-img-submain').css({'cursor': ''}).unbind(sClickBySpace);
		}
		if(oSet.bShowAppointment) {
			oMain.find('.sl-view-appointment-total em').html(oData.appointment || 0);
		}
	}
}

/**
 * 获取HTML元素以data-为前缀的属性数据
 * 这是HTML5的新特性，以data-为键值的属性为合法属性，并且该元素含有dataset对象属性，其它不再赘述
 * 这个函数兼容HTML5以下
 *
 * @param object 元素对象
 * @param mixed aKeyList 可以是一个元素：'timeType' 或者是数据['timeType', 'vid']
 * @return mixed 当aKeyList为单元素时返回单个值，否则返回一个JSON对象，键值对应aKeyList每个元素
 *
 * @example
 *	html: <i data-table-type="foo" id="i"></i>
 *	js: console.log( fnGetEleData(_id('i'), 'tableType') ) //foo
 * @author denglh
 */
function fnGetEleData(oElement, aKeyList) {
	var bKeyListIsArray = is_array(aKeyList) === '[object Array]';
	aKeyList = bKeyListIsArray ? aKeyList : [aKeyList];
	var oData = {};
	for(var i = 0, len = aKeyList.length; i < len; i++) {
		var sKeyVal = aKeyList[i];
		//dataset是HTML5的特性，如果存在则使用
		if(oElement.dataset) {
			oData[sKeyVal] = oElement.dataset[sKeyVal];
		//否则使用普通方法
		} else {
			var sAfKey = sKeyVal.replace(/[A-Z]/g, function($1) {
				return '-'+($1.toLowerCase());
			});
			oData[sKeyVal] = oElement.getAttribute('data-'+sAfKey);
		}
	}
	return bKeyListIsArray ? oData : oData[aKeyList];
}
function fnTriggerRadio(obj){
	$(obj).find("input").attr("checked", true);
}