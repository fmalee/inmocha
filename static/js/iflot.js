/**
 * iFlot 1.0
 * 
 * 扩展基于jquery.flot
 * 由于flot本身设置比较麻烦，实际上我们使用的图表并不复杂，因此需要在flot的基础上封装一些重复的处理简化开发流程
 * iFlot封装了基本的柱形图表和折线图表
 *
 * 作者 denglh
 * 版本 1.0
 * 依赖 jquery jquery.flot
 * 修改日期 2012/09/21
 */

/** 
  iFlot与flot.js的使用方法不相同，例：
  柱形图
  $('selector').iFlot( 'bar', {
  		oData: { '公园': [10, 120, 20], '游乐场': [60, 240, 78], '网吧': [100, 120, 97] },
		//aLabelData中的数据与oData每一列的数据元素相对应
		aLabelData: ['活跃度', '活动总时间', '参与人数']
  });
  折线图
  $('selector').iFlot( 'line', {
		
  });
  或者可以使用$.iFlot($('selector'), type, setting);
 */
!function( $ ) {
	//默认配置
	var oDefSetting = {
		//柱形图
		bar: {
			nTickScale: 1,			//页面显示像素与刻度比例，默认1:1，这个比例用于下面一些参数的数值参照，例如nColumnSpace、nColumnWidth等
			nColumnSpace: 5,		//每个相邻的条形柱之间的间隔
			nColumnWidth: 15,		//柱形宽度
			nClassMargin: 15,		//每个类别之间的间隔
			bAutoWeight: false,		//窗口高度自适应或者固定，更多说明请参考fnSetBarWeight()函数
			nAutoScaleMargin: 0.3,	//柱的顶部预留空间比例（防止右上角标题档住数据）
			bHorizontal: false,		//水平（垂直于Y轴）显示数据
			nTicks: 10,				//显示多少刻度
			bHoveShowDetail: true,	//鼠标停留到数据上显示详请
			oData: {},				//数据
			aLabelData: []			//图表右上角的标题，如['标题1', '标题2']
		},
		//折线图
		line: {
			nYTicks: 5,		//Y轴显示多少刻度
			nXTicks: 5,		//X轴显示多少刻度
			oData: {},		//数据
			oXaxis: {		//X坐标的设置，目前只允许设置时间模式，详情参考$.flot的说明文档
				sMode: 'normal',			// or time
				sTimeFormat: '%y/%m/%d',	//sMode等于time时生效
				aTickSize: [1, 'day']		//同上
			},		
			bHoveShowDetail: true //鼠标停留到数据上显示详请
		}
	}
	//内部全局对象
	var oInputs = {},		//控件对象
		aPlotData = [],		//图表数据
		oPlotSetting = {},	//图表设置参数（由客户端参数计算出来的最终图表参数）
		oSetting = {};		//客户端参数
	
	$.iFlot = function(oInputs, sType, oSet) {
		return $.fn.iFlot.call(oInputs, sType, oSet);
	}
	
	$.fn.iFlot = function(sType, oSet) {
		oInputs = this;
		//注销全局缓存数据
		aPlotData = [];
		oPlotSetting = {};
		//返回$.plot创建的对象数组，每个元素包含对应的控件对象的图表信息
		return $.each(oInputs, function() {
			return fnDraw(oSet, sType, $(this));
		});
	}
	
	/**
	 * 绘制图表
	 * @param object oSet 客户端设置参数
	 * @param string sType 图标类型
	 * @param object oInput 控件对象
	 * @return object 由flot控件生成的对象
	 */
	function fnDraw(oSet, sType, oInput) {
		oSetting = $.extend(oDefSetting[sType], oSet);
		//设置宽度
		if(sType == 'bar' && oSetting.bAutoWeight) {
			fnSetBarWeight();
		}
		var oSetFns = {
			'bar': fnSetBar,
			'line': fnSetLine
		}
		//绘制图表
		var oPlotData = $.plot(oInput, aPlotData,
			$.extend(true, oPlotSetting, oSetFns[sType]())
		);
		//添加鼠标停留事件
		if(oSetting.bHoveShowDetail) {
			var oDetailbox = $('<div style="position: absolute;top: 0px;left: 0px;background-color: white;display: none;"></div>').appendTo(oInput.get(0));
			//缓存控件位置，提高效率，但是如果控件顶部以上的页面高度被改变，则会影响信息显示位置
			var oInputOffset = {
				nLeft: oInput.offset().left,
				nTop: oInput.offset().top
			}
			var nKeyIndex = 1;
			if(sType == 'bar' && oSetting.bHorizontal) {
				nKeyIndex = 0;
			}
			//绑定鼠标停留事件
			oInput.bind("plothover", function(event, pos, item) {
				if(item) {
					oDetailbox.css({
						'left': (pos.pageX - oInputOffset.nLeft - 5) + 'px',
						'top': (pos.pageY - oInputOffset.nTop - 25) + 'px'
					}).html(item.series.data[item.dataIndex][nKeyIndex]).show();
				} else {
					oDetailbox.hide();
				}
			}).mouseout(function() {
				oDetailbox.hide();
			});
		}
		return oPlotData;
	}
	
	/**
	 * 柱形图参数设置函数
	 * @param object op 自定义设置
	 * @return object 柱形图参数对象
	 */
	function fnSetBar() {
		var nColSpace = oSetting.nColumnSpace,
			nColWidth = oSetting.nColumnWidth,
			nClassMargin = oSetting.nClassMargin,
			nSubColTotal = oSetting.aLabelData.length,
			aColors = [],
			aTicks = [],
			nClassNum = 0,
			fnHasOwn = Object.prototype.hasOwnProperty;
		//计算柱形坐标位置，以下说的'位置'具体是x轴还是y轴取决于bHorizontal参数，当水平显示时，位置指y轴
		for(var sDataName in oSetting.oData) {
			if(fnHasOwn.call(oSetting.oData, sDataName)) {
					//获得当前类别第一条柱形的位置，以此为基准坐标
				var nBasePos = nClassMargin + (((nColWidth + nColSpace) * nSubColTotal + nClassMargin * 2) * nClassNum),
					//设置第一条柱形的位置，这个位置会随着迭代计算第N条柱形而改变
					nColPos = nBasePos,
					aSubData = oSetting.oData[sDataName];
				$.each(aSubData, function(i) {
					//设置当前数据坐标
					var aSubLabelData = oSetting.bHorizontal ? [aSubData[i], nColPos] : [nColPos, aSubData[i]];
					//下一条柱形的坐标
					nColPos += (nColWidth + nColSpace);
					if(!aPlotData[i]) {
						//初始化plot插件的data数据
						aPlotData[i] = {
							label: fnIsArray(oSetting.aLabelData[i]) ? oSetting.aLabelData[i][0] : oSetting.aLabelData[i],
							data: []
						}
//						aColors.push(oSetting.aLabelData[i][1]);
					}
					aPlotData[i].data.push(aSubLabelData);
				});
				//计算类别名的刻度位置，应该处于当前类别下所有柱形占轴范围的总和的一半，也就是在中间
				//因为每个类别边缘不含有列之间的间隔，所以要减去这个间隔的刻度
				aTicks.push([(nColPos - nColSpace - nBasePos) / 2 + nBasePos, sDataName]);
				nClassNum++;
			}
		}
		var oXaxis = { autoscaleMargin: oSetting.nAutoScaleMargin };
		var oYaxis = {};
		//垂直显示时
		if(!oSetting.bHorizontal) {
			//隐藏水平刻度轴
			oXaxis.tickLength = 0;
			//设置刻度
			oXaxis.ticks = aTicks;
			oYaxis.ticks = oSetting.nTicks;
		//水平显示时
		} else {
			//隐藏垂直刻度轴
			oYaxis.tickLength = 0;
			//设置刻度
			oXaxis.ticks = oSetting.nTicks;
			oYaxis.ticks = aTicks;
		}
		return {
			series: { bars: { show: true, horizontal: oSetting.bHorizontal, barWidth: nColWidth } },
			xaxis: oXaxis,
			yaxis: oYaxis,
			grid: { hoverable: true, clickable: true, autoHighlight: true, borderWidth: 0, borderColor: '#ccc' }
		}
	}
	
	/**
	 * 设置柱形图'宽度'
	 * 这个'宽度'不一定指css中的width，由设置中的bHorizontal参数决定，当为真时，这个'宽度'指height
	 */
	function fnSetBarWeight() {
		var nSubColTotal = oSetting.aLabelData.length,
			nColSpace = oSetting.nColumnSpace,
			nColWidth = oSetting.nColumnWidth,
			nClassMargin = oSetting.nClassMargin,
			//有多少类别
			nClassTotal = fnGetObjLen(oSetting.oData),
			//先计算最基本的宽度
			nBaseWeight = nClassTotal * nSubColTotal * nColWidth,
			//考虑每个类别下的条形柱之间的间隔，每个类别下的条形柱数量要减1，如：3列只有2条间隔，总是少1
			nSpaceWeight = nClassTotal * (nSubColTotal - 1) * nColSpace,
			//考虑每个类别两边的宽度
			nMarginWeight = nClassTotal * nClassMargin * 2,
			//计算比例
			nTickScale = +oSetting.nTickScale ? 1 / (+oSetting.nTickScale) : 1,
			//容器高度 = 总刻度 * 刻度与像素比例
			nMainWeight = (nBaseWeight + nSpaceWeight + nMarginWeight) * nTickScale,
			sWeightType = oSetting.bHorizontal ? 'height' : 'width';
		$.each(oInputs, function() {
			$(this).css(sWeightType, nMainWeight + 'px' );
		});
	}
	
	/**
	 * 折线图参数设置函数
	 * @param object op 自定义设置
	 * @return object 折线图参数对象
	 */
	function fnSetLine() {
		var fnHasOwn = Object.prototype.hasOwnProperty;
		for(var sDataName in oSetting.oData) {
			if(fnHasOwn.call(oSetting.oData, sDataName)) {
				var oData = oSetting.oData[sDataName];
				aPlotData.push({label: sDataName, data: oData});
			}
		}
		var oXaxis = { ticks: oSetting.nXTicks, tickLength: 0, alignTicksWithAxis: 5 };
		if(oSetting.oXaxis.sMode == 'time') {
			oXaxis.mode = "time";
			oXaxis.timeformat = oSetting.oXaxis.sTimeFormat || "%y/%m/%d";
			oXaxis.tickSize = oSetting.oXaxis.aTickSize ||  [1, 'day'];
		}
		var oYaxis = { ticks: oSetting.nYTicks }
		return {
			series: {
				lines: {
					show: true,
					fill: true,
					fillColor: {
						colors: [{ opacity: 0 }, { opacity: 1}]
					}
				},
				points: { show: true }
			},
			grid: { hoverable: true, clickable: true, autoHighlight: true, borderWidth: 0 },
			yaxis: oYaxis,
			xaxis: oXaxis
		}
	}
	
	/**
	 * 统计对象长度
	 */
	function fnGetObjLen(obj) {
		var nLen = 0;
		$.map(obj, function() {
			nLen++;
		});
		return nLen;
	}
	
	/**
	 * 判断一个元素是否为数组
	 */
	function fnIsArray(arr) {
		return Object.prototype.toString.call(arr) === '[object Array]';
	}
}(jQuery);