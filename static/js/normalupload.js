/*
	[Ibos!]
		normal upload javascript
	$Id$
*/

var attachexts = new Array();
var attachwh = new Array();

var insertType = 1;

var extensions = 'jpg,jpeg,gif,png,xls,xlsx,doc,docx,ppt,pptx,zip,rar,txt,chm,htm,html,pdf';
var forms;
var nowUid = 0;
var uploadStat = 0;
var picid = 0;
var nowid = 0;
var attachid;
var mainForm;
var successState = false;
var uploadErrFile = new Array();
var maxsize;

function delAttach(id) {
    document.getElementById('attachbody').removeChild(document.getElementById('attach_' + id).parentNode.parentNode.parentNode);
    if(document.getElementById('attachbody').innerHTML == '') {
        addAttach();
    }
    $('#child_'+id).remove();
}

function edit_insert(html) {
    var obj = window.frames['htmleditor'];
    var status = document.getElementById('uchome-editstatus').value;
    if(status != 'html') {
        alert('本操作只在多媒体编辑模式下才有效');
        return;
    }
    obj.focus();
    if(BROWSER.ie){
        var f = obj.document.selection.createRange();
        f.pasteHTML(html);
        f.collapse(false);
        f.select();
    } else {
        obj.document.execCommand('insertHTML', false, html);
    }
}

function addAttach() {
    newnode = document.getElementById('attachbodyhidden').rows[0].cloneNode(true);
    var id = nowid;
    var tags;
    tags = newnode.getElementsByTagName('form');
    for(i in tags) {
        if(tags[i].id == 'upload') {
            tags[i].id = 'upload_' + id;
        }
    }
    tags = newnode.getElementsByTagName('input');
    for(i in tags) {
        if(tags[i].name == 'attach') {
            tags[i].id = 'attach_' + id;
            tags[i].name = 'attach';
            tags[i].onchange = function() {
                insertAttach(id)
                };
            tags[i].unselectable = 'on';
        }
    }
    tags = newnode.getElementsByTagName('span');
    for(i in tags) {
        if(tags[i].id == 'localfile') {
            tags[i].id = 'localfile_' + id;
        }
    }
    nowid++;

    document.getElementById('attachbody').appendChild(newnode);
}

addAttach();

function insertAttach(id) {
    var path = document.getElementById('attach_' + id).value;
    var ext = getExt(path);
    var re = new RegExp("(^|\\s|,)" + ext + "($|\\s|,)", "ig");
    var localfile = document.getElementById('attach_' + id).value.substr(document.getElementById('attach_' + id).value.replace(/\\/g, '/').lastIndexOf('/') + 1);

    if(path == '') {
        return;
    }
    if(extensions != '' && (re.exec(extensions) == null || ext == '')) {
        alert('对不起，不允许上传此类扩展名的附件');
        return;
    }
    attachexts[id] = in_array(ext, ['gif', 'jpg', 'jpeg', 'png','xls','xlsx','doc','docx','ppt','pptx','zip','rar','txt']) ? 2 : 1;

    var inhtml = '<table cellspacing="0" cellpadding="0" class="up_row"><tr>';
    if(typeof no_insert=='undefined') {
        localfile += '&nbsp;';
    }
    inhtml += '<td>图片</td>';
    inhtml += '<td><strong id="showname' + id + '">' + localfile +'</strong></td>';
    inhtml += '<td class="o"><span id="showmsg' + id + '"><a href="javascript:;" onclick="delAttach(' + id + ')">【删除】</a></span>';
    inhtml += '</td></tr></table>';

    var lihtml = '<li id="child_' + id + '">' + localfile + '</li>';

    document.getElementById(getUP_filename).innerHTML += lihtml;
    document.getElementById('localfile_' + id).innerHTML = inhtml;
    document.getElementById('attach_' + id).style.display = 'none';

    addAttach();
}
/*
function getExt(path) {
	return path.lastIndexOf('.') == -1 ? '' : path.substr(path.lastIndexOf('.') + 1, path.length).toLowerCase();
} 
*/

function getPath(obj){
    if (obj) {
        if (BROWSER.ie && BROWSER.ie < 7) {
            obj.select();
            return document.selection.createRange().text;

        } else if(BROWSER.firefox) {
            if (obj.files) {
                return obj.files.item(0).getAsDataURL();
            }
            return obj.value;
        } else {
            return '';
        }
        return obj.value;
    }
}

function insertAttachimgTag(id) {
    edit_insert('[imgid=' + id + ']');
}

function uploadSubmit(obj) {
    obj.disabled = true;
    mainForm = obj.form;
    forms = document.getElementById('attachbody').getElementsByTagName("FORM");
    upload();
}

function start_upload(formid,inputid) {
	uploadErrFile = new Array();
    mainForm = document.getElementById(formid);
    attachinput = document.getElementById(inputid);
    forms = document.getElementById('attachbody').getElementsByTagName("FORM");
    upload();
}

function upload() {
    if(typeof(forms[nowUid]) == 'undefined') return false;
    var nid = forms[nowUid].id.split('_');
    nid = nid[1];
    if(nowUid>0) {
        var upobj = document.getElementById('showmsg'+nowid);
        if(uploadStat==1) {
            upobj.innerHTML = '上传成功';
            successState = true;
            var InputNode;
            try {
                attachinput.value += attachid+',';
            } catch(e) {
                var InputNode = document.createElement("input");
                InputNode.setAttribute("name", "attachmentid");
                InputNode.setAttribute("type", "hidden");
                InputNode.setAttribute("id", "attachmentid");
                InputNode.setAttribute("value", attachid+',');
            }
        } else {
            upobj.style.color = "#f00";
            upobj.innerHTML = '上传失败 '+uploadStat;
			var filename = document.getElementById('showname'+nowid).innerHTML; //获得文件名添加进数组
			uploadErrFile.push(filename+'|'+uploadStat);
        }
    }

    if(document.getElementById('showmsg'+nid) != null && document.getElementById('showmsg'+nid) != setStatMsg(1)) {
        document.getElementById('showmsg'+nid).innerHTML = '上传中，请等待(<a href="javascript:;" onclick="forms[nowUid].submit();">重试</a>)';
        forms[nowUid].submit();
    } else if(nowUid+1 == forms.length) {
        if(typeof (no_insert) != 'undefined') {
            //if(!successState) return false;
			if(uploadErrFile.length > 0) {
				var errMsg = setStatMsg(0)+'\n';
				for(var i=0;i<uploadErrFile.length;i++){
					var errmsgarr = uploadErrFile[i].split('|');
					errMsg += (errmsgarr[0].split('&nbsp'))[0] + setStatMsg(errmsgarr[1]) + '\n';
				}
				alert(errMsg);
				return false;
			}
        }
        mainForm.submit();
    }
    nowid = nid;
    nowUid++;
    uploadStat = 0;
}

/**
 * 根据上传状态设置提示
 * 
 *
 */
function setStatMsg(Stat){
	var statvalue = parseInt(Stat);
	switch(statvalue){
		case 0:
			msg = '上传失败';
			break;
		case 1:
			msg = '上传成功！';
			break;
		case 2:
			var setsize = !maxsize ? '未知限制' : maxsize;
			msg = '可能超出大小限制：' + setsize;
			break;
		default:
			msg = '未知错误！';
			break;
	}
	return msg;
}