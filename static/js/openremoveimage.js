/*×
负责将图片由新页面打开转为在弹出的DIV窗口中打开
*/
$(document).ready(function(){
    var a,url;
    var imgs = document.getElementById('wrap').getElementsByTagName('img');
    //遍历所有图片
    for(i=0; i<imgs.length; i++){
        url = $(imgs[i]).attr('src');//得到图片的 src 属性
        if(!url) continue;
        a = $(imgs[i]).parent("a");

        a.click(function(){
            zoom(this,this.href);
            return false;
        });
    }
});