/*
	[IBOS] (C)2001-2099 ibos Inc.
	

	$Id$
*/

var nowid = 0;

var extensions = '';

function addAttach() {
	var newnode = _id('upload').cloneNode(true);
	var id = nowid;
	var tags;

	newnode.id = 'upload_' + id;
	tags = newnode.getElementsByTagName('input');
	for(i in tags) {
		if(tags[i].name == 'attach') {
			tags[i].id = 'attach_' + id;
			tags[i].name = 'attach';
			tags[i].onchange = function() {this.form.action = this.form.action.replace(/catid\=\d/, 'catid='+_id('catid').value);insertAttach(id)};
			tags[i].unselectable = 'on';
		}
	}
	tags = newnode.getElementsByTagName('span');
	for(i in tags) {
		if(tags[i].id == 'localfile') {
			tags[i].id = 'localfile_' + id;
		}
	}
	nowid++;

	_id('attachbody').appendChild(newnode);
}

function insertAttach(id) {

	var path = _id('attach_' + id).value;
	if(path == '') {
		return;
	}
	var ext = path.lastIndexOf('.') == -1 ? '' : path.substr(path.lastIndexOf('.') + 1, path.length).toLowerCase();
	var re = new RegExp("(^|\\s|,)" + ext + "($|\\s|,)", "ig");
	if(extensions != '' && (re.exec(extensions) == null || ext == '')) {
		alert('对不起，不支持上传此类文件');
		return;
	}
	var localfile = _id('attach_' + id).value.substr(_id('attach_' + id).value.replace(/\\/g, '/').lastIndexOf('/') + 1);
	_id('localfile_' + id).innerHTML = localfile + ' 上传中...';
	_id('attach_' + id).style.display = 'none';
	_id('upload_' + id).action += '&attach_target_id=' + id;
	_id('upload_' + id).submit();

	addAttach();
}

function deleteAttach(attachid, url) {
	ajaxget(url);
	_id('attach_list_' + attachid).style.display = 'none';
}

function setConver(attach) {
	_id('conver').value = attach;
}

addAttach();