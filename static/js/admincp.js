/*
	[IBOS] (C)2001-2099 IBOS Inc.
	This is NOT a freeware, use is subject to license terms

	$Id: admincp.js 27346 2012-01-17 05:56:24Z monkey $
*/

function redirect(url) {
	window.location.replace(url);
}

function scrollTopBody() {
	return Math.max(document.documentElement.scrollTop, document.body.scrollTop);
}

function checkAll(type, form, value, checkall, changestyle) {
	var checkall = checkall ? checkall : 'chkall';
	for(var i = 0; i < form.elements.length; i++) {
		var e = form.elements[i];
		if(type == 'option' && e.type == 'radio' && e.value == value && e.disabled != true) {
			e.checked = true;
		} else if(type == 'value' && e.type == 'checkbox' && e.getAttribute('chkvalue') == value) {
			e.checked = form.elements[checkall].checked;
			if(changestyle) {
				multiupdate(e);
			}
		} else if(type == 'prefix' && e.name && e.name != checkall && (!value || (value && e.name.match(value)))) {
			e.checked = form.elements[checkall].checked;
			if(changestyle) {
				if(e.parentNode && e.parentNode.tagName.toLowerCase() == 'li') {
					e.parentNode.className = e.checked ? 'checked' : '';
				}
				if(e.parentNode.parentNode && e.parentNode.parentNode.tagName.toLowerCase() == 'div') {
					e.parentNode.parentNode.className = e.checked ? 'item checked' : 'item';
				}
			}
		}
	}
}

function altStyle(obj, disabled) {
	function altStyleClear(obj) {
		var input, lis, i;
		lis = obj.parentNode.getElementsByTagName('li');
		for(i=0; i < lis.length; i++){
			lis[i].className = '';
		}
	}
	var disabled = !disabled ? 0 : disabled;
	if(disabled) {
		return;
	}

	var input, lis, i, cc, o;
	cc = 0;
	lis = obj.getElementsByTagName('li');
	for(i=0; i < lis.length; i++){
		lis[i].onclick = function(e) {
			o = BROWSER.ie ? event.srcElement.tagName : e.target.tagName;
			altKey = BROWSER.ie ? window.event.altKey : e.altKey;
			if(cc) {
				return;
			}
			cc = 1;
			input = this.getElementsByTagName('input')[0];
			if(input.getAttribute('type') == 'checkbox' || input.getAttribute('type') == 'radio') {
				if(input.getAttribute('type') == 'radio') {
					altStyleClear(this);
				}

				if(BROWSER.ie || o != 'INPUT' && input.onclick) {
					input.click();
				}
				if(this.className != 'checked') {
					this.className = 'checked';
					input.checked = true;
				} else {
					this.className = '';
					input.checked = false;
				}
				if(altKey && input.name.match(/^multinew\[\d+\]/)) {
					miid = input.id.split('|');
					mi = 0;
					while(_id(miid[0] + '|' + mi)) {
						_id(miid[0] + '|' + mi).checked = input.checked;
						if(input.getAttribute('type') == 'radio') {
							altStyleClear(_id(miid[0] + '|' + mi).parentNode);
						}
						_id(miid[0] + '|' + mi).parentNode.className = input.checked ? 'checked' : '';
						mi++;
					}
				}
			}
		};
		lis[i].onmouseup = function(e) {
			cc = 0;
		}
	}
}

var addrowdirect = 0;
var addrowkey = 0;
function addrow(obj, type) {
	var table = obj.parentNode.parentNode.parentNode.parentNode.parentNode;
	if(!addrowdirect) {
		var row = table.insertRow(obj.parentNode.parentNode.parentNode.rowIndex);
	} else {
		var row = table.insertRow(obj.parentNode.parentNode.parentNode.rowIndex + 1);
	}
	var typedata = rowtypedata[type];
	for(var i = 0; i <= typedata.length - 1; i++) {
		var cell = row.insertCell(i);
		cell.colSpan = typedata[i][0];
		var tmp = typedata[i][1];
		if(typedata[i][2]) {
			cell.className = typedata[i][2];
		}
		tmp = tmp.replace(/\{(n)\}/g, function($1) {return addrowkey;});
		tmp = tmp.replace(/\{(\d+)\}/g, function($1, $2) {return addrow.arguments[parseInt($2) + 1];});
		cell.innerHTML = tmp;
	}
	addrowkey ++;
	addrowdirect = 0;
}

var delrowdirect = 0;
function deleterow(obj) {
    if(!delrowdirect){
        var table = obj.parentNode.parentNode.parentNode.parentNode;
        var tr = obj.parentNode.parentNode;    
    }else{
        var table = obj.parentNode.parentNode.parentNode.parentNode.parentNode;
        var tr = obj.parentNode.parentNode.parentNode;    
    }
	table.deleteRow(tr.rowIndex);
    delrowdirect = 0;
}
function dropmenu(obj){
	showMenu({'ctrlid':obj.id, 'menuid':obj.id + 'child', 'evt':'mouseover'});
	_id(obj.id + 'child').style.top = (parseInt(_id(obj.id + 'child').style.top) - Math.max(document.body.scrollTop, document.documentElement.scrollTop)) + 'px';
	if(BROWSER.ie > 6 || !BROWSER.ie) {
		_id(obj.id + 'child').style.left = (parseInt(_id(obj.id + 'child').style.left) - Math.max(document.body.scrollLeft, document.documentElement.scrollLeft)) + 'px';
	}
}

var heightag = BROWSER.chrome ? 4 : 0;
function textareasize(obj, op) {
	if(!op) {
		if(obj.scrollHeight > 70) {
			obj.style.height = (obj.scrollHeight < 300 ? obj.scrollHeight - heightag: 300) + 'px';
			if(obj.style.position == 'absolute') {
				obj.parentNode.style.height = (parseInt(obj.style.height) + 20) + 'px';
			}
		}
	} else {
		if(obj.style.position == 'absolute') {
			obj.style.position = '';
			obj.style.width = '';
			obj.parentNode.style.height = '';
		} else {
			obj.parentNode.style.height = obj.parentNode.offsetHeight + 'px';
			obj.style.width = BROWSER.ie > 6 || !BROWSER.ie ? '90%' : '600px';
			obj.style.position = 'absolute';
		}
	}
}

function showanchor(obj) {
	var navs = _id('submenu').getElementsByTagName('li');
	for(var i = 0; i < navs.length; i++) {
		if(navs[i].id.substr(0, 4) == 'nav_' && navs[i].id != obj.id) {
			if(_id(navs[i].id.substr(4))) {
				navs[i].className = '';
				_id(navs[i].id.substr(4)).style.display = 'none';
				if(_id(navs[i].id.substr(4) + '_tips')) _id(navs[i].id.substr(4) + '_tips').style.display = 'none';
			}
		}
	}
	obj.className = 'current';
	currentAnchor = obj.id.substr(4);
	_id(currentAnchor).style.display = '';
	if(_id(currentAnchor + '_tips')) _id(currentAnchor + '_tips').style.display = '';
	if(_id(currentAnchor + 'form')) {
		_id(currentAnchor + 'form').anchor.value = currentAnchor;
	} else if(_id('cpform')) {
		_id('cpform').anchor.value = currentAnchor;
	}
}

function updatecolorpreview(obj) {
	_id(obj).style.background = _id(obj + '_v').value;
}

function entersubmit(e, name) {
	var e = e ? e : event;
	if(e.keyCode != 13) {
		return;
	}
	var tag = BROWSER.ie ? e.srcElement.tagName : e.target.tagName;
	if(tag != 'TEXTAREA') {
		doane(e);
		if(_id('submit_' + name).offsetWidth) {
			_id('formscrolltop').value = document.documentElement.scrollTop;
			_id('submit_' + name).click();
		}
	}
}

function parsetag(tag) {
	var parse = function (tds) {
		for(var i = 0; i < tds.length; i++) {
			if(tds[i].getAttribute('s') == '1') {
				var str = tds[i].innerHTML.replace(/(^|>)([^<]+)(?=<|$)/ig, function($1, $2, $3) {
					if(tag && $3.indexOf(tag) != -1) {
						re = new RegExp(tag, "g");
						$3 = $3.replace(re, '<h_>');
					}
					return $2 + $3;
					});
				tds[i].innerHTML = str.replace(/<h_>/ig, function($1, $2) {
					return '<font class="highlight">' + tag + '</font>';
					});
			}
		}
	};
	parse(document.body.getElementsByTagName('td'));
	parse(document.body.getElementsByTagName('span'));
}

function sdisplay(id, obj) {
	obj.innerHTML = _id(id).style.display == 'none' ? '<img src="static/image/admincp/desc.gif" style="vertical-align:middle" />' : '<img src="static/image/admincp/add.gif" style="vertical-align:middle" />';
	display(id);
}

if(ISFRAME) {
	try {
		_attachEvent(document.documentElement, 'keydown', parent.resetEscAndF5);
	} catch(e) {}
}

var multiids = new Array();
function multiupdate(obj) {
	v = obj.value;
	if(obj.checked) {
		multiids[v] = v;
	} else {
		multiids[v] = null;
	}
}

function getmultiids() {
	var ids = '', comma = '';
	for(i in multiids) {
		if(multiids[i] != null) {
			ids += comma + multiids[i];
			comma = ',';
		}
	}
	return ids;
}


function toggle_group(oid, obj, conf) {
	obj = obj ? obj : _id('a_'+oid);
	if(!conf) {
		var conf = {'show':'[-]','hide':'[+]'};
	}
	var obody = _id(oid);
	if(obody.style.display == 'none') {
		obody.style.display = '';
		obj.innerHTML = conf.show;
	} else {
		obody.style.display = 'none';
		obj.innerHTML = conf.hide;
	}
}
function show_all() {
	var tbodys = _id("cpform").getElementsByTagName('tbody');
	for(var i = 0; i < tbodys.length; i++) {
		var re = /^group_(\d+)$/;
		var matches = re.exec(tbodys[i].id);
		if(matches != null) {
			tbodys[i].style.display = '';
			_id('a_group_' + matches[1]).innerHTML = '[-]';
		}
	}
}
function hide_all() {
	var tbodys = _id("cpform").getElementsByTagName('tbody');
	for(var i = 0; i < tbodys.length; i++) {
		var re = /^group_(\d+)$/;
		var matches = re.exec(tbodys[i].id);
		if(matches != null) {
			tbodys[i].style.display = 'none';
			_id('a_group_' + matches[1]).innerHTML = '[+]';
		}
	}
}

function show_all_hook(prefix, tagname) {
	var tbodys = _id("cpform").getElementsByTagName(tagname);
	for(var i = 0; i < tbodys.length; i++) {
		var re = new RegExp('^' + prefix + '(.+)$');
		var matches = re.exec(tbodys[i].id);
		if(matches != null) {
			tbodys[i].style.display = '';
			_id('a_' + prefix + matches[1]).innerHTML = '[-]';
		}
	}
}

function hide_all_hook(prefix, tagname) {
	var tbodys = _id("cpform").getElementsByTagName(tagname);
	for(var i = 0; i < tbodys.length; i++) {
		var re = new RegExp('^' + prefix + '(.+)$');
		var matches = re.exec(tbodys[i].id);
		if(matches != null) {
			tbodys[i].style.display = 'none';
			_id('a_' + prefix + matches[1]).innerHTML = '[+]';
		}
	}
}

function srchforum() {
	var fname = _id('srchforumipt').value.toLowerCase();
	if(!fname) return false;
	var inputs = _id("cpform").getElementsByTagName('input');
	for(var i = 0; i < inputs.length; i++) {
		if(inputs[i].name.match(/^name\[\d+\]$/)) {
			if(inputs[i].value.toLowerCase().indexOf(fname) !== -1) {
				inputs[i].parentNode.parentNode.parentNode.parentNode.style.display = '';
				inputs[i].parentNode.parentNode.parentNode.style.background = '#eee';
				window.scrollTo(0, fetchOffset(inputs[i]).top - 100);
				return false;
			}
		}
	}
	return false;
}

function setfaq(obj, id) {
	if(!_id(id)) {
		return;
	}
	_id(id).style.display = '';
	if(!obj.onmouseout) {
		obj.onmouseout = function () {
			_id(id).style.display = 'none';
		}
	}
}

function floatbottom(id) {
	if(!_id(id)) {
		return;
	}
	_id(id).style.position = 'fixed';
	_id(id).style.bottom = '0';
	_id(id).parentNode.style.paddingBottom = '15px';
	if(!BROWSER.ie || BROWSER.ie && BROWSER.ie > 6) {
		window.onscroll = function() {
			var scrollLeft = Math.max(document.documentElement.scrollLeft, document.body.scrollLeft);
			_id(id).style.marginLeft = '-' + scrollLeft + 'px';
		};
		_id(id).style.display = '';
	}
}

var sethtml_id = null;
function sethtml(id) {
	_id(id).className = 'txt html';
	_id(id).contentEditable = true;
	_id(id).onkeyup = function () {
		_id(id + '_v').value = _id(id).innerHTML;
	};
	var curvalue = _id(id).innerHTML;

	var div = document.createElement('div');
	div.id = id + '_c_menu';
	div.style.display = 'none';
	div.innerHTML = '<iframe id="' + id + '_c_frame" src="" frameborder="0" width="210" height="148" scrolling="no"></iframe>';
	_id(id).parentNode.appendChild(div);

	var btn = document.createElement('input');
	btn.id = id + '_c';
	btn.type = 'button';
	btn.className = 'htmlbtn c';
	if(curvalue.search(/<font/ig) !== -1) {
		btn.className = 'htmlbtn c current';
	}
	btn.onclick = function() {
		_id(id + '_c_frame').src = 'static/image/admincp/getcolor.htm?||sethtml_color';
		showMenu({'ctrlid' : id + '_c'});
		sethtml_id = id;
	};
	_id(id).parentNode.appendChild(btn);

	var btn = document.createElement('input');
	btn.id = id + '_b';
	btn.type = 'button';
	btn.className = 'htmlbtn b';
	if(curvalue.search(/<b>/ig) !== -1) {
		btn.className = 'htmlbtn b current';
	}
	btn.onclick = function() {
		var oldvalue = _id(id).innerHTML;
		_id(id).innerHTML = preg_replace(['<b>', '</b>'], '', _id(id).innerHTML);
		if(oldvalue == _id(id).innerHTML) {
			_id(id + '_b').className = 'htmlbtn b current';
			_id(id).innerHTML = '<b>' + _id(id).innerHTML + '</b>';
		} else {
			_id(id + '_b').className = 'htmlbtn b';
		}
		_id(id + '_v').value = _id(id).innerHTML;
	};
	_id(id).parentNode.appendChild(btn);

	var btn = document.createElement('input');
	btn.id = id + '_i';
	btn.type = 'button';
	btn.className = 'htmlbtn i';
	if(curvalue.search(/<i>/ig) !== -1) {
		btn.className = 'htmlbtn i current';
	}
	btn.onclick = function() {
		var oldvalue = _id(id).innerHTML;
		_id(id).innerHTML = preg_replace(['<i>', '</i>'], '', _id(id).innerHTML);
		if(oldvalue == _id(id).innerHTML) {
			_id(id + '_i').className = 'htmlbtn i current';
			_id(id).innerHTML = '<i>' + _id(id).innerHTML + '</i>';
		} else {
			_id(id + '_i').className = 'htmlbtn i';
		}
		_id(id + '_v').value = _id(id).innerHTML;
	};
	_id(id).parentNode.appendChild(btn);

	var btn = document.createElement('input');
	btn.id = id + '_u';
	btn.type = 'button';
	btn.style.textDecoration = 'underline';
	btn.className = 'htmlbtn u';
	if(curvalue.search(/<u>/ig) !== -1) {
		btn.className = 'htmlbtn u current';
	}
	btn.onclick = function() {
		var oldvalue = _id(id).innerHTML;
		_id(id).innerHTML = preg_replace(['<u>', '</u>'], '', _id(id).innerHTML);
		if(oldvalue == _id(id).innerHTML) {
			_id(id + '_u').className = 'htmlbtn u current';
			_id(id).innerHTML = '<u>' + _id(id).innerHTML + '</u>';
		} else {
			_id(id + '_u').className = 'htmlbtn u';
		}
		_id(id + '_v').value = _id(id).innerHTML;
	};
	_id(id).parentNode.appendChild(btn);
}

function sethtml_color(color) {
	_id(sethtml_id).innerHTML = preg_replace(['<font[^>]+?>', '</font>'], '', _id(sethtml_id).innerHTML);
	if(color != 'transparent') {
		_id(sethtml_id + '_c').className = 'htmlbtn c current';
		_id(sethtml_id).innerHTML = '<font color=' + color + '>' + _id(sethtml_id).innerHTML + '</font>';
	} else {
		_id(sethtml_id + '_c').className = 'htmlbtn c';
	}
	_id(sethtml_id + '_v').value = _id(sethtml_id).innerHTML;
}

function uploadthreadtypexml(formobj, formaction) {
	formobj.action = formaction;
	formobj.submit();
}

