/*
	[IBOS] (C)2011-2012 ibos Inc.
	$Id$
 */
(function() {
    var ibim_lang = {
	online:'在线',
	busy:'忙碌',
	away:'离开',
	offline:'离线',
	InvalidUid:'必须提供uid参数!',
	newMsg:'你有新短消息，请查收',
	takingWith:'正与',
	ing:'聊天中',
	searchFriend:'查找好友',
	searchNoResult:'没有找到符合搜索条件的联系人',
	dontTalktoYourself:'你不能与自己聊天',
	cantSendMsgtoYourself:'不能发信息给自己',
	me:'我'
    };
    ibim = function(parameter) {
	this.refreshTime = isUndefined(parameter['refreshTime']) ? 120000 : parameter['refreshTime'];
	this.defaultSend = isUndefined(parameter['defaultSend']) ? 1 : parameter['defaultSend'];
	this.lang = ibim_lang;
	this.initUrl = isUndefined(parameter['initUrl']) ? 'misc.php?mod=ibim&act=init' : parameter['initUrl'];
	this.handleUrl = isUndefined(parameter['handleUrl']) ? 'misc.php?mod=ibim&act=handle' : parameter['handleUrl'];
	this.MsgUrl = isUndefined(parameter['MsgUrl']) ? 'misc.php?mod=ibim&act=initchatbox&to=' : parameter['MsgUrl'];
	this.timestamp = isUndefined(parameter['timestamp']) ? this.getTimeStamp() : parameter['timestamp'];
	this.uid = isUndefined(parameter['uid']) ? null : parameter['uid'];
	this.error = false;
	this.status = isUndefined(parameter['status']) ? new Array('ibim_status_online','ibim_status_busy','ibim_status_away','ibim_status_offline') : parameter['status'];
	this.statusDesc = isUndefined(parameter['statusDesc']) ?new Array(this.lang.online,this.lang.busy,this.lang.away,this.lang.offline) : parameter['status'];
	this.initialize();
	var _this = this;
	setTimeout(function(){
	    _this.comet();
	}, 5000);
	return this;
    };

    ibim.prototype = {
	getTimeStamp : function() {
	    var timestamp = (new Date()).valueOf().toString().substr(0,10);
	    return parseInt(timestamp);
	},
	initList : function (listObj,dom) {
	    var list_str = '';
	    var _this = this;
	    $.each(listObj,function(i,n){
		$.each(n,function(j,k){
		    list_str += "<li title=\""+k.frealname+"\" uid=\""+k.fuid+"\" id=\"ibim_u_"+k.fuid+"\" class=\""+ (k.status==3 ? 'ibim_offline' : 'ibim_online')+"\">\n";
		    list_str += "<div class=\"ibim_userhead\">\n";
		    list_str +=  "<img id=\"ibim_u_i_"+k.fuid+"\" src=\""+k.avatar+"\">\n";
		    list_str += "<span style=\"display:none;\" class=\"ibim_icon_msg_s\"></span>\n";
		    list_str += "<span id=\"ibim_u_s_"+k.fuid+"\" class=\""+_this.status[k.status]+"\"></span>\n"
		    list_str += "</div>\n";
		    list_str += "<div class=\"ibim_username\">"+k.frealname+"</div>\n";
		    if (k.rtxstatus == 1) {
			list_str += "<div class=\"ibim_userhead\">";
			list_str +=	"<img style=\"width:16px;height:16px;margin-left:10px;opacity: 1;\" src=\"static/image/rtx/rtxonline.png\">";
			list_str += "</div>";
		    } else if (k.rtxstatus == 2) {
			list_str += "<div class=\"ibim_userhead\">";
			list_str +=	"<img style=\"width:16px;height:16px;margin-left:10px;opacity: 1;\" src=\"static/image/rtx/rtxaway.png\">";
			list_str += "</div>";
		    }
		    list_str += "</li>\n";
		})
	    })
	    _id(dom).innerHTML = list_str;
	},
	bindMouseLeave : function (obj) {
	    if(_id(obj)){
		$('#'+obj).bind('mouseleave',function(){
		    $(this).hide();
		})
	    }
	},
	initChatBox : function (uid,cls,name,msg) {
	    if(!_id('ibim_msg_'+uid)) {
		var html = '<dl id="ibim_msg_'+uid+'">';
		$.each(msg,function(i,n){
		    $.each(n,function(j,k){
			if(k.msgfromid==uid){
			    html += "<dd class=\"ibim_msgl\">\n";
			} else {
			    html += "<dd class=\"ibim_msgr\">\n";
			}
			html += "<div class=\"ibim_msgpos\">\n";
			html += "<div class=\"msg_time\">"+k.dateline+"</div>\n";
			html += "<div class=\"msg_box\">\n";
			html += " <p class=\"txt\">"+k.message+"</p>\n";
			html += "</div>\n";
			html += "<div class=\"msg_arr\"></div>\n";
			html += "</div>\n";
			html += "</dd>\n";
		    })
		    html += "<dd class=\"line\"></dd>";
		})
		html += "</dl>";
		$('#ibim_msg').append(html);
	    }
	    this.initChatboxTab('ibim_u_'+uid); //初始化多人聊天tab
	},
	initChatboxTab : function (id) {
	    var _this = this;
	    var html = '';
	    var uid = $('#'+id).attr('uid');
	    var data = getcookie('ibim_'+_this.uid);
	    var tmp = data.split('-');
	    if(tmp.length>1){ //如果是多人聊天
		$('.ibim_chat_lf').attr('style','').attr('style','visibility: visible;'); //显示多人列表
		$('#ibim_chatbox').removeClass('ibim_chat_box_s').show();		      //作用同上
		for( var i = 0, len = tmp.length; i < len; i++ ) { //生成多人列表内容
		    var tmpuid = $('#ibim_u_'+tmp[i]).attr('uid');
		    if(isUndefined(tmpuid)) continue; //如果初始化时已经离线，但cookie还有该数据的，不生成
		    var name = $('#ibim_u_'+tmp[i]).attr('title');
		    var cls = $('#ibim_u_'+tmp[i]).attr('class');
		    var status = $('#ibim_u_s_'+tmp[i]).attr('class');
		    var avatar = $('#ibim_u_i_'+tmp[i]).attr('src');
		    var active = id == 'ibim_u_'+tmp[i] ? ' ibim_active' : ''; //当前选中
		    html += "<li title=\""+name+"\" uid=\""+tmpuid+"\" class=\""+cls+active+"\">\n";
		    html += "<div class=\"ibim_userhead\">\n";
		    html += "<img src=\""+avatar+"\">\n";
		    html += "<span class=\""+status+"\"></span></div>\n";
		    html += "<div class=\"ibim_username\">"+name+"</div>\n";
		    html += "<a class=\"ibim_icon_close_s\" onclick=\"im.tabDel(this);\" href=\"javascript:void(0);\" hidefocus=\"true\"></a>\n";
		    html += "</li>\n";
		}
		$('#ibim_chat_friend_list').html(html); //插入html
		$('#ibim_msg_'+uid).show().siblings().hide(); //显示对应的消息框
		//绑定多人聊天tab点击事件
		$('#ibim_chat_friend_list li').bind('click',function(){
		    var uid = $(this).attr('uid');
		    if(_id('ibim_msg_'+uid)){ //如果已经读取消息
			$('#ibim_msg_'+uid).show().siblings().hide(); //显示
			$(this).addClass('ibim_active').siblings().removeClass('ibim_active'); //高亮当前
			$('#ibim_msg').scrollTop($('#ibim_msg_'+uid).height()+500);//自动滚动到底部
		    } else { //否则读取消息，调用init_chatbox方法
			var cls = $('#ibim_u_s_'+uid).attr('class');
			var name = $('#ibim_u_'+uid).attr('title');
			var url = _this.MsgUrl+uid;
			$.post(url,function(msg){
			    _this.initChatBox(uid,cls,name,msg.list);
			},'json');
		    }
		    _this.initChatBoxStatus(uid,$(this).attr('class')=='ibim_offline' ? '0' : '1');
		})
	    } else { //如果不是多人聊天，直接显示单个聊天窗口
		$('#ibim_chatbox').show();
	    }
	    _this.initChatBoxStatus(uid,$(this).attr('class')=='ibim_offline' ? '0' : '1'); //无论哪一个条件执行，都初始化聊天窗口的状态
	},
	initChatBoxStatus : function (uid,isonline) {
	    var cls = $('#ibim_u_s_'+uid).attr('class');
	    var name = $('#ibim_u_'+uid).attr('title');
	    $('#ibim_msg').scrollTop($('#ibim_msg_'+uid).height());//自动滚动到底部
	    $('#ibim_history').attr('href','home.php?mod=space&do=pm&subop=view&touid='+uid); //更多聊天记录的链接指向
	    $('#sb_status').attr('class',cls);
	    $('#sb_link').attr({
		'title':name,
		'href':'home.php?mod=space&uid='+uid
	    }).html(name);
	    $('#send_to').val(uid);
	    if(isonline){
		$('.ibim_chat_tips').hide();
	    } else {
		$('.ibim_chat_tips').show();
	    }
	},
	showOptWindow : function () {
	    $('#ibim_choose_menu').toggle();
	},
	setOpt : function (type) {
	    setcookie('ibim_opt_'+this.uid,type);
	    this.initOpt();
	},
	switchStatus : function (status) {
	    var _this = this;
	    $.post('misc.php?mod=ibim&act=switchstatus',{
		status:status
	    },function(data){
		if(data.IsSuccess){
		    var cls = _this.status[status];
		    var html = "<span class=\""+cls+"\"></span>";
		    html += "<span class=\"txt\">"+_this.statusDesc[status]+"</span>";
		    html += "<span class=\"icon\"></span>";
		    $('#ibim_sb_status').html(html);
		    $('#ibim_self_status').attr('class',cls);
		}
	    },'json')
	},
	Send : function () {
	    var uid = $('#send_to').val();
	    var msg = $('#ibim_sendmessage').val();
	    var _this = this;
	    if(msg=='') {
		_this.blink(_id('ibim_sendmessage'));
		return;
	    }
	    $.post(this.handleUrl,{
		uid:uid,
		msg:msg
	    },function(data){
		if(data.IsSuccess){
		    var html = '';
		    html += "<dd class=\"ibim_msgr\">\n";
		    html += "<div class=\"ibim_msgpos\">\n";
		    html += "<div class=\"msg_time\">"+_this.lang.me+'  '+data.date+"</div>\n";
		    html += "<div class=\"msg_box\">\n";
		    html += " <p class=\"txt\">"+data.msg+"</p>\n";
		    html += "</div>\n";
		    html += "<div class=\"msg_arr\"></div>\n";
		    html += "</div>\n";
		    html += "</dd>\n";
		    $('#ibim_msg_'+uid).append(html);
		    $('#ibim_msg').scrollTop($('#ibim_msg_'+uid).height()+500);//自动滚动到底部
		    $("#ibim_sendmessage").val('');
		}
	    },'json')
	},
	GSend : function (){
	    var uid = $('#ibim_friend').val();
	    var msg = $('#ibim_g_sendmessage').val();
	    if(msg=='') {
		this.blink(_id('ibim_g_sendmessage'));
		return;
	    }
	    if(uid==''){
		this.blink(_id('friend_display'));
		return;
	    }
	    if(uid.indexOf(this.uid)!== -1){
		alert(this.lang.cantSendMsgtoYourself);
		return;
	    }
	    $('#ibim_sendbox').hide();
	    $('#ibim_sending').show();
	    $.post(this.handleUrl,{
		uid:uid,
		msg:msg
	    },function(data){
		if(data.IsSuccess){
		    $('#ibim_success').show().siblings().hide();
		    setTimeout(function(){
			//$('#friend_display').val('');
			//$('#friend').val('');
			$('#ibim_g_sendmessage').val('');
			$('#ibim_sendbox').show().siblings().hide();
		    },3000)
		}
	    },'json')
	},
	countNum : function(d,obj){
	    var len = strlen($(d).val());
	    $('#'+obj).html(parseFloat(200-len));
	},
	search : function () {
	    var key = $('#ibim_search_text').val();
	    if(key=='') {
		$('.ibim_list_tab').show();
		$('.ibim_list_tab ul li').first().click();
		return false;
	    }

	    var count = 0;
	    $('#ibim_sclist').html('');
	    $('.ibim_list_tab').hide();
	    $('#ibim_search_list').show().siblings().hide();
	    $('#ibim_flist li,.ibim_list_group_ul li').each(function(i){
		var name = $(this).find('.ibim_username').html();
		var tmp = name;
		if(name.indexOf(key)!==-1){
		    count++;
		    name = name.replace(key,'<span style="color:red;">'+key+'</span>');
		    $('#ibim_sclist').append($(this).clone(true));
		    $('#ibim_sclist li').last().find('.ibim_username').html(name);
		}
	    })
	    if(count==0){
		$('#ibim_sclist').html('<li>'+this.lang.searchNoResult+'</li>');
	    }
	},
	displaySendBox : function (show,type) {
	    if(show){
		$('#ibim_send_box').show();
	    } else {
		$('#ibim_send_box').hide();
	    }
	    type ? $('.ibim_send_con').find('a').addClass('ibim_gmsging') : $('.ibim_send_con').find('a').removeClass('ibim_gmsging');
	},
	displayChatBox : function (type) {
	    if(type==1){ //缩小
		var nick = $('#sb_link').html();
		$('.ibim_min_box_col2').attr('class','ibim_min_box_col3').show();
		$('.ibim_min_friend').show();
		$('#ibim_chatbox').hide();
		$($('#ibim_min_chat').find('span').get(1)).html(this.lang.takingWith).attr('style','display: inline;');
		$($('#ibim_min_chat').find('span').get(2)).html(nick);
		$($('#ibim_min_chat').find('span').get(4)).html(this.lang.ing);
		$('#ibim_min_chat').show();
	    } else if (type == 2){ //关闭
		$('#ibim_chat_friend_list').empty();
		$('#ibim_msg').empty();
		setcookie('ibim_'+this.uid,'',-1);
		$('.ibim_chat_lf').attr('style','').attr('style','visibility: hidden; display: none;'); //隐藏多人列表
		$('#ibim_chatbox').addClass('ibim_chat_box_s').hide();
		$('.ibim_min_box_col3').attr('class','ibim_min_box_col2');
	    }
	},
	scroll : function (flag) {
	    if(flag){
		$('.ibim_chat_friend_box').animate({
		    scrollTop: $('#ibim_chat_friend_list').scrollTop()-50
		},500);
	    } else {
		$('.ibim_chat_friend_box').animate({
		    scrollTop: $('#ibim_chat_friend_list').height()
		},500);
	    }
	},
	hideExpand : function () {
	    $('.ibim_list_expand').hide();
	},
	toggleTree : function (d) {
	    $(d).siblings("ul").toggle();
	    $(d).parent().toggleClass("closed");
	    var obj = $(d).next('div');
	    if(obj){
		if(obj.find('div').is(':visible') && $(d).parent().hasClass('closed')){
		    obj.find('div').attr('style','display:none;')
		    obj.attr('style','display:none;');
		} else {
		    obj.find('div').attr('style','display:block;')
		    obj.attr('style','display:block;');
		}
	    }
	},
	toggleDept : function (d) {
	    $(d).parent().toggleClass("closed");
	    if($(d).parent().find('div').is(':visible')){
		$(d).parent().find('div').attr('style','display:none;');
	    } else {
		$(d).parent().find('div').attr('style','display:block;');
	    }
	},
	tabDel : function (d) {
	    var uid = $(d).parent().attr('uid');
	    var obj = $(d).parent();
	    obj.unbind('click');
	    var data = getcookie('ibim_'+this.uid);
	    var tmp = data.split('-');
	    if(in_array(uid,tmp)){
		var index = '';
		for( var i = 0, len = tmp.length; i < len; i++ ) {
		    if(tmp[i]==uid){
			index = i;
			break;
		    }
		}
		tmp.splice(index,1);
		setcookie('ibim_'+this.uid,tmp.join('-'));
	    }
	    $('#ibim_msg_'+uid).remove();
	    if(obj.siblings()){
		obj.remove();
		if($('#ibim_chat_friend_list li').length==1){
		    $('#ibim_chat_friend_list').find('a').remove().addClass('ibim_active');

		}
		$('#ibim_chat_friend_list li').first().click();
	    }
	},
	bindHover : function () {
	    //收缩状态下的经过样式
	    $('.ibim_min_friend').hover(
		function () {
		    $(this).addClass("ibim_min_box_hover");
		}, function () {
		    $(this).removeClass("ibim_min_box_hover");
		});
	    $('#ibim_show_menu').hover(
		function () {
		    $(this).addClass("ibim_tit_lf_hover").find('ul').css({
			'display':'block',
			'visibility':'visible'
		    });
		}, function () {
		    $(this).removeClass("ibim_tit_lf_hover").find('ul').css({
			'display':'none',
			'visibility':'hidden'
		    });
		});
	    $('.ibim_tit').hover(function () {
		$(this).addClass("ibim_tit_hover");
	    }, function () {
		$(this).removeClass("ibim_tit_hover");
	    });
	    $('#ibim_min_chat').hover(function(){
		$(this).addClass("ibim_min_box_hover");
	    }, function(){
		$(this).removeClass("ibim_min_box_hover");
	    });
	},
	bindOther : function () {
	    //搜索框交互初始化
	    var _this = this;
	    $('#ibim_search_text').bind('focus',function(){
		$(this).select();
		if($(this).val() == _this.lang.searchFriend) {
		    $(this).val('');
		}
	    }).bind('blur',function(){
		if($(this).val()=='') {
		    $(this).val(_this.lang.searchFriend);
		}
	    })
	},
	bindClick : function () {
	    $('.ibim_list_tab ul li').bind('click',function(){
		var index = $(this).index(); //获取自己在当前ul的位置
		$(this).addClass(' curr').siblings().removeClass(' curr'); //为li添加当前样式
		$('.ibim_list_box').children().eq(index).show().siblings().hide(); //相应的div box显示和隐藏
	    });
	    $('.ibim_min_friend').click(function(){
		$('.ibim_list_expand').show();
	    });
	    //我的好友点击切换可见度
	    $('#ibim_onlineuser').click(function(){
		if($(this).hasClass('ibim_close')){
		    $(this).removeClass('ibim_close').addClass('ibim_open');
		    $('#ibim_flist').show();
		} else {
		    $(this).removeClass('ibim_open').addClass('ibim_close');
		    $('#ibim_flist').hide();
		}
	    });
	    $('#ibim_min_chat').bind('click',function(){
		$(this).hide();
		$('#ibim_chatbox').show();
	    });
	},
	bindListClick:function(obj) {
	    var _this = this;
	    $(obj).bind('click',function(){
		var uid = $(this).attr('uid');
		if(uid == _this.uid) {
		    alert(_this.lang.dontTalktoYourself);
		    return;
		}
		var data = getcookie('ibim_'+_this.uid); //取cookie
		if(!_id('ibim_msg_'+uid)){  //如果没有读取消息，读取
		    var cls = $('#ibim_u_s_'+uid).attr('class');
		    var name = $('#ibim_u_'+uid).attr('title');
		    var url = _this.MsgUrl+uid;
		    $.post(url,function(msg){
			_this.initChatBox(uid,cls,name,msg.list);
		    },'json')
		} else { //否则初始化tab
		    _this.initChatboxTab($(this).attr('id'));
		}
		if(!data){ //没有cookie，写入一个
		    setcookie('ibim_'+_this.uid,uid);
		} else { //否则，取出cookie字符串，拆分为数组加入当前uid，再生成字符串重写入cookie
		    var tmp = data.split('-');
		    if(!in_array(uid,tmp)){
			tmp.push(uid);
			var newStr = '';
			for( var i = 0, len = tmp.length; i < len; i++ ) {
			    newStr += tmp[i];
			    if(i <len-1){
				newStr+='-';
			    }
			}
			setcookie('ibim_'+_this.uid,newStr);
		    }
		}
		_this.updateRecentContact(uid);
	    });
	},
	updateRecentContact : function (uid) {
	    var data = getcookie('ibim_recent_contacts_'+this.uid);
	    var tmp = data.split('-');
	    if(!in_array(uid,tmp)){
		tmp.push(uid);
		var newStr = '';
		for( var i = 0, len = tmp.length; i < len; i++ ) {
		    newStr += tmp[i];
		    if(i <len-1){
			newStr+='-';
		    }
		}
		setcookie('ibim_recent_contacts_'+this.uid,newStr);
	    }
	    var copy = $('#ibim_u_'+uid).clone();
	    $('#ibim_rclist').append(copy);
	},
	initOpt : function () {
	    var opt = getcookie('ibim_opt_'+this.uid);
	    var _this = this;
	    $('#ibim_sendmessage').unbind('keydown');
	    if(opt==1){
		$('#ibim_sendmessage').bind('keydown',function(event){
		    if(event.keyCode==13){
			_this.Send();
		    }
		})
		$($('#ibim_choose_menu').find('li').get(0)).attr('class','curr');
		$($('#ibim_choose_menu').find('li').get(2)).attr('class','');
	    } else {
		$('#ibim_sendmessage').bind('keydown',function(event){
		    if (event.keyCode === 13 && event.ctrlKey) {
			_this.Send();
		    }
		})
		$($('#ibim_choose_menu').find('li').get(0)).attr('class','');
		$($('#ibim_choose_menu').find('li').get(2)).attr('class','curr');
	    }
	},
	blink : function (obj) {
	    setTimeout(function(){
		$(obj).attr('style','background-color:white;');
	    }, 80);
	    setTimeout(function(){
		$(obj).attr('style','background-color:#F7B326;');
	    }, 160);
	    setTimeout(function(){
		$(obj).attr('style','background-color:white;');
	    }, 240);
	    setTimeout(function(){
		$(obj).attr('style','background-color:#F7B326;');
	    }, 320);
	    setTimeout(function(){
		$(obj).attr('style','background-color:#F7B326;');
	    }, 400);
	    setTimeout(function(){
		$(obj).attr('style','background-color:#FEF3DE;');
	    }, 480);
	    setTimeout(function(){
		$(obj).attr('style','');
	    }, 560);
	},
	newMsgRemind : function (uid,msg) {
	    var _this = this;
	    if(!$('#ibim_msg_'+uid).is(':visible')){
		if($('.ibim_chat_lf').is(':visible')){
		    $('#ibim_chat_friend_list li').each(function(i){
			if($(this).attr('uid')==uid){
			    _this.blink(this);
			    return;
			}
		    })
		} else {
		    $('.ibim_min_box_col2').attr('class','ibim_min_box_col3').show();
		    $('#ibim_chatbox').hide();
		    $($('#ibim_chat_msg').find('span').get(1)).html('').attr('style','display: none;');
		    $($('#ibim_chat_msg').find('span').get(2)).html(_this.lang.newMsg);
		    $($('#ibim_chat_msg').find('span').get(4)).html('');
		    $('#ibim_chat_msg').addClass('ibim_min_chat_msg').show().siblings('#ibim_min_chat').hide();
		    ;
		    var title = document.title;
		    var new_title = "【"+_this.lang.newMsg+"】";
		    var secs = 1000;
		    for(var i = 1;i<=3;i++){
			secs+=1000;
			if(i%2!=0){
			    setTimeout(function(){
				document.title = new_title;
			    }, secs);
			} else {
			    setTimeout(function(){
				document.title = title;
			    }, secs);
			}
		    }
		    $('#ibim_chat_msg').click(function(){
			document.title = title;
			if(_id('ibim_u_'+uid)){
			    $('#ibim_u_'+uid).click();
			} else {
			    $('#ibim_r_'+uid).click();
			}
			$(this).removeClass('ibim_min_chat_msg').hide();
		    })
		}
	    }
	},
	newMsg : function (msg) {
	    var uid = msg.fromid;
	    var html = '';
	    html += "<dd class=\"ibim_msgl\">\n";
	    html += "<div class=\"ibim_msgpos\">\n";
	    html += "<div class=\"msg_time\">"+msg.username+'  '+msg.date+"</div>\n";
	    html += "<div class=\"msg_box\">\n";
	    html += " <p class=\"txt\">"+msg.msg+"</p>\n";
	    html += "</div>\n";
	    html += "<div class=\"msg_arr\"></div>\n";
	    html += "</div>\n";
	    html += "</dd>\n";
	    $('#ibim_msg_'+uid).append(html);
	    $('#ibim_msg').scrollTop($('#ibim_msg_'+uid).height()+500);//自动滚动到底部
	    this.newMsgRemind(uid,msg);
	},
	comet: function () {
	    var _this = this;
	    $.ajax({
		data : {
		    'timestamp' : _this.timestamp
		},
		url : _this.handleUrl,
		type : 'post',
		timeout : 30000,
		success : function(response){
		    var data = jQuery.parseJSON(response);
		    if(data){
			_this.error = false;
			_this.timestamp = data.timestamp;
			if(typeof data.msg == 'boolean'){
			    _this.error = true;
			    setTimeout(function(){
				_this.comet();
			    }, 5000);
			    return false;
			}
			_this.newMsg(data.msg);
		    }
		},
		error : function(){
		    _this.error = true;
		},
		complete : function(){
		    if (_this.error){
			setTimeout(function(){
			    _this.comet();
			}, 5000);
		    } else {
			_this.comet();
		    }
		}
	    })
	},
	initialize : function () {
	    if(!this.uid && this.uid == 0) {
		alert(this.lang.InvalidUid);
		return;
	    }
	    var _this = this;
	    $.post(this.initUrl,function(data){
		var html = "<span class=\""+_this.status[data.self_status]+"\"></span>";
		html += "<span class=\"txt\">"+_this.statusDesc[data.self_status]+"</span>";
		html += "<span class=\"icon\"></span>";
		_id('ibim_sb_status').innerHTML = html;

		var userOpt = getcookie('ibim_'+_this.uid+'_opt'); //检查用户发送消息习惯
		if(userOpt==''){
		    setcookie('ibim_opt_'+_this.uid,_this.defaultSend);
		    _this.initOpt();
		}

		var recentContact = getcookie('ibim_recent_contacts_'+_this.uid);
		if(recentContact==''){
		    setcookie('ibim_recent_contacts_'+_this.uid,'');
		}
		_id('ibibm_online_friends').innerHTML = data.onlineFriends;
		_id('ibim_online_ount').innerHTML = data.onlineCount;
		_id('ibim_group_list').innerHTML = data.dlist;
		_id('ibim_self_status').setAttribute('class',_this.status[data.self_status]);
		_this.bindHover();
		_this.initList(data.flist,'ibim_flist');
		_this.bindMouseLeave('ibim_choose_menu');
		_this.bindListClick('#ibim_flist li');
		_this.bindListClick('.ibim_list_friend li'); //绑定
		_this.bindClick();
		_this.bindHover();
		_this.bindOther();

		setTimeout(function(){
		    _this.initialize();
		},_this.refreshTime);

	    },'json')
	}
    };
})();