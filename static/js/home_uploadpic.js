/*
	[IBOS] (C)2001-2099 ibos Inc.
	

	$Id$
*/

var attachexts = new Array();
var attachwh = new Array();

var insertType = 1;
var thumbwidth = parseInt(60);
var thumbheight = parseInt(60);
var extensions = 'jpg,jpeg,gif,png';
var forms;
var nowUid = 0;
var albumid = 0;
var uploadStat = 0;
var picid = 0;
var nowid = 0;
var mainForm;
var successState = false;

function delAttach(id) {
	_id('attachbody').removeChild(_id('attach_' + id).parentNode.parentNode.parentNode);
	if(_id('attachbody').innerHTML == '') {
		addAttach();
	}
	_id('localimgpreview_' + id + '_menu') ? document.body.removeChild(_id('localimgpreview_' + id + '_menu')) : null;
}

function addAttach() {
	newnode = _id('attachbodyhidden').rows[0].cloneNode(true);
	var id = nowid;
	var tags;
	tags = newnode.getElementsByTagName('form');
	for(i in tags) {
		if(tags[i].id == 'upload') {
			tags[i].id = 'upload_' + id;
		}
	}
	tags = newnode.getElementsByTagName('input');
	for(i in tags) {
		if(tags[i].name == 'attach') {
			tags[i].id = 'attach_' + id;
			tags[i].name = 'attach';
			tags[i].onchange = function() {insertAttach(id)};
			tags[i].unselectable = 'on';
		}
		if(tags[i].id == 'albumid') {
			tags[i].id = 'albumid_' + id;
		}
	}
	tags = newnode.getElementsByTagName('span');
	for(i in tags) {
		if(tags[i].id == 'localfile') {
			tags[i].id = 'localfile_' + id;
		}
	}
	nowid++;

	_id('attachbody').appendChild(newnode);
}

addAttach();

function insertAttach(id) {
	var localimgpreview = '';
	var path = _id('attach_' + id).value;
	var ext = getExt(path);
	var re = new RegExp("(^|\\s|,)" + ext + "($|\\s|,)", "ig");
	var localfile = _id('attach_' + id).value.substr(_id('attach_' + id).value.replace(/\\/g, '/').lastIndexOf('/') + 1);

	if(path == '') {
		return;
	}
	if(extensions != '' && (re.exec(extensions) == null || ext == '')) {
		alert('对不起，不支持上传此类扩展名的图片');
		return;
	}
	attachexts[id] = inArray(ext, ['gif', 'jpg', 'jpeg', 'png']) ? 2 : 1;

	var inhtml = '<table cellspacing="0" cellpadding="0" class="up_row"><tr>';
	if(typeof no_insert=='undefined') {
		localfile += '&nbsp;<a href="javascript:;" class="xi2" title="点击这里插入内容中当前光标的位置" onclick="insertAttachimgTag(' + id + ');return false;">[插入]</a>';
	}
	inhtml += '<td><strong>' + localfile +'</strong>';
	inhtml += '</td><td class="d">图片描述<br/><textarea name="pic_title" cols="40" rows="2" class="pt"></textarea>';
	inhtml += '</td><td class="o"><span id="showmsg' + id + '"><a href="javascript:;" onclick="delAttach(' + id + ');return false;" class="xi2">[删除]</a></span>';
	inhtml += '</td></tr></table>';

	_id('localfile_' + id).innerHTML = inhtml;
	_id('attach_' + id).style.display = 'none';

	addAttach();
}

function getPath(obj){
	if (obj) {
		if (BROWSER.ie && BROWSER.ie < 7) {
			obj.select();
			return document.selection.createRange().text;

		} else if(BROWSER.firefox) {
			if (obj.files) {
				return obj.files.item(0).getAsDataURL();
			}
			return obj.value;
		} else {
			return '';
		}
		return obj.value;
	}
}
function inArray(needle, haystack) {
	if(typeof needle == 'string') {
		for(var i in haystack) {
			if(haystack[i] == needle) {
					return true;
			}
		}
	}
	return false;
}

function insertAttachimgTag(id) {
	edit_insert('[imgid=' + id + ']');
}

function uploadSubmit(obj) {
	obj.disabled = true;
	mainForm = obj.form;
	forms = _id('attachbody').getElementsByTagName("FORM");
	albumid = _id('uploadalbum').value;
	upload();
}

function start_upload() {
	_id('btnupload').disabled = true;
	mainForm = _id('albumresultform');
	forms = _id('attachbody').getElementsByTagName("FORM");
	upload();
}

function upload() {
	if(typeof(forms[nowUid]) == 'undefined') return false;
	var nid = forms[nowUid].id.split('_');
	nid = nid[1];
	if(nowUid>0) {
		var upobj = _id('showmsg'+nowid);
		if(uploadStat==1) {
			upobj.innerHTML = '上传成功';
			successState = true;
			var InputNode;
			try {
				var InputNode = document.createElement("<input type=\"hidden\" id=\"picid_" + picid + "\" value=\""+ nowid +"\" name=\"picids["+picid+"]\">");
			} catch(e) {
				var InputNode = document.createElement("input");
				InputNode.setAttribute("name", "picids["+picid+"]");
				InputNode.setAttribute("type", "hidden");
				InputNode.setAttribute("id", "picid_" + picid);
				InputNode.setAttribute("value", nowid);
			}
			mainForm.appendChild(InputNode);

		} else {
			upobj.style.color = "#f00";
			upobj.innerHTML = '上传失败 '+uploadStat;
		}
	}

	if(_id('showmsg'+nid) != null) {
		_id('showmsg'+nid).innerHTML = '上传中，请等待(<a href="javascript:;" onclick="forms[nowUid].submit();">重试</a>)';
		_id('albumid_'+nid).value = albumid;
		forms[nowUid].submit();
	} else if(nowUid+1 == forms.length) {
		if(typeof (no_insert) != 'undefined') {
			var albumidcheck = parseInt(parent.albumid);
			_id('opalbumid').value = isNaN(albumidcheck)? 0 : albumid;
			if(!successState) return false;
		}
		window.onbeforeunload = null;
		mainForm.submit();
	}
	nowid = nid;
	nowUid++;
	uploadStat = 0;
}