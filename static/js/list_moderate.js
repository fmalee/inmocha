/*
	[iBOS!] (C)2001-2009 IBOS Inc.
	This is NOT a freeware, use is subject to license terms

	$Id: list_moderate.js 21562 2011-03-31 08:40:33Z monkey $
*/

var modclickcount = 0;

function tmodclick(obj) {
	if(obj.checked) {
		modclickcount++;
	} else {
		modclickcount--;
	}
	_id('mdct').innerHTML = modclickcount;
	if(modclickcount > 0) {
		var top_offset = obj.offsetTop;
		while((obj = obj.offsetParent).id != 'listbody') {
			top_offset += obj.offsetTop;
		}
		_id('mdly').style.top = top_offset - 7 + 'px';
		_id('mdly').style.display = 'block';
	} else {
		_id('mdly').style.display = 'none';
	}
	if(!_id('isedit')){
		return false;
	}else if(modclickcount==1){
		_id('isedit').style.display = '';
		_id('noedit').style.display = 'none';
	}else{
		_id('isedit').style.display = 'none';
		_id('noedit').style.display = '';
	}
}

function modarticle(act) {
	var checked = 0;
	for(var i = 0; i < _id('moderate').elements.length; i++) {
		if(_id('moderate').elements[i].name.match('moderate') && _id('moderate').elements[i].checked) {
			checked = 1;
			value = _id('moderate').elements[i].value;
			break;
		}
	}
	if(!checked) {
		alert('请选择需要操作的记录');
	} else {
		switch(act){
			case 'edit':
				location.href = "desktop.php?mod=article&do=add&act=edit&articleid="+value;
				break;
			case 'del':
                                $.messager.confirm('提示信息', '确定删除么？',function(bFlag){
                                    if(bFlag){ //判断是否确定删除选择的数据
                                        _id('moderate').action = "desktop.php?mod=article&do=add&act=del";
					_id('moderate').submit();
                                    }
                                });
				break;
			case 'verify':
				_id('moderate').action = "desktop.php?mod=article&do=add&act=verify";
				_id('moderate').submit();
				break;
			case 'move': //@dlh
				$('#articlemove').iDialog({
					title:'移动到',
					width:350,
					height:'auto',
					submit:function(){
						$('#moderate').attr('action','desktop.php?mod=article&do=add&act=move&category='+$('#artcatmove').val());
						$('#moderate').submit();
					}
				});
				break;
		}
	}
}
	
function modwiki(act) {
	var checked = 0;
	var allvalue = '';
	for(var i = 0; i < _id('moderate').elements.length; i++) {
		if(_id('moderate').elements[i].name.match('moderate') && _id('moderate').elements[i].checked) {
			checked++;
			if(checked > 1){
				allvalue += ','+_id('moderate').elements[i].value;
			}else{
				allvalue =_id('moderate').elements[i].value;
			}
		}
	}
	if(!checked) {
		alert('请选择需要操作的记录');
	} else {
		switch(act){
			case 'edit':
				location.href = "km.php?mod=wiki&do=add&act=docedit&docid="+allvalue;
				break;
			case 'del':
//				if(window.confirm("确定删除么？")){
//					location.href = "km.php?mod=wiki&do=action&act=remove&docid="+allvalue;
//				}
                                $.messager.confirm('提示信息', '确定删除么？',function(bFlag){
                                    if(bFlag){ //判断是否确定删除选择的数据
                                        location.href = "km.php?mod=wiki&do=action&act=remove&docid="+allvalue;
                                    }
                                });
				break;
			case 'audit':
				$.post("./km.php?mod=wiki&do=action&act=audit",{
					docid:allvalue
				},function(result){
					alert_result(result); //显示操作结果
				});
				break;
			case 'move':
				$('#wikimove').iDialog({
					title:'移动到',
					width:300,
					height:'auto',
					submit:function(){
						$('#moderate').attr('action','./km.php?mod=wiki&do=action&act=move&category='+$('#wikicatmove').val());
						$('#moderate').submit();
					}
				});
				break;
			case 'lock':
				$.post("./km.php?mod=wiki&do=action&act=lock",{
					docid:allvalue
				},function(result){
					alert_result(result); //显示操作结果
				});
				break;
			case 'unlock':
				$.post("./km.php?mod=wiki&do=action&act=unlock",{
					docid:allvalue
				},function(result){
					alert_result(result); //显示操作结果
				});
				break;
			case 'recommend':
				$.post("./km.php?mod=wiki&do=action&act=setfocus",{
					doctype:1,
					visible:1,
					docid:allvalue
				},function(result){
					alert_result(result); //显示操作结果
				});
				break;
			case 'hot':
				$.post("./km.php?mod=wiki&do=action&act=setfocus",{
					doctype:2, 
					visible:1, 
					docid:allvalue
				},function(result){
					alert_result(result); //显示操作结果
				});
				break;
			case 'brilliant':
				$.post("./km.php?mod=wiki&do=action&act=setfocus",{
					doctype:3, 
					visible:1, 
					docid:allvalue
				},function(result){
					alert_result(result); //显示操作结果
				});
				break;
		}
	}
}

function modask(act) {
	var checked = 0;
	var allvalue = '';
	for(var i = 0; i < _id('moderate').elements.length; i++) {
		if(_id('moderate').elements[i].name.match('moderate') && _id('moderate').elements[i].checked) {
			checked++;
			if(checked > 1){
				allvalue += ','+_id('moderate').elements[i].value;
			}else{
				allvalue =_id('moderate').elements[i].value;
			}
		}
	}
	if(!checked) {
		alert('请选择需要操作的记录');
	} else {
		switch(act){
			case 'edit':
				location.href = "km.php?mod=wiki&do=add&act=docedit&docid="+allvalue;
				break;
			case 'del':
//				if(window.confirm("确定删除么？")){
//					location.href = "km.php?mod=ask&do=action&act=delete&docid="+allvalue;
//				}
                                $.messager.confirm('提示信息', '确定删除么？',function(bFlag){
                                    if(bFlag){ //判断是否确定删除选择的数据
                                        location.href = "km.php?mod=ask&do=action&act=delete&docid="+allvalue;
                                    }
                                });
				break;
			case 'move':
				$('#askmove').iDialog({
					title:'移动到',
					width:300,
					height:'auto',
					submit:function(){
						$('#moderate').attr('action','km.php?mod=ask&do=action&act=move&category='+$('#askcatmove').val());
						$('#moderate').submit();
					}
				});
				break;
		}
	}
}

function alert_result(result){
	result = jQuery.parseJSON(result);
	if(result.IsSuccess){
		$("body").iTips({content:'操作成功！',css:'success'});
	}else{
		$("body").iTips({content:'操作失败',css:'warning'});
	}
}