/*
	[IBOS] (C)2001-2099 ibos Inc.
	

	$Id: home_blog.js 23838 2011-08-11 06:51:58Z monkey $
*/

function validate_ajax(obj) {
	var subject = _id('subject');
	if (subject) {
		var slen = strlen(subject.value);
		if (slen < 1 || slen > 80) {
			alert("标题长度(1~80字符)不符合要求");
			subject.focus();
			return false;
		}
	}
	if(_id('seccode')) {
		var code = _id('seccode').value;
		var x = new Ajax();
		x.get('cp.php?ac=common&op=seccode&code=' + code, function(s){
			s = trim(s);
			if(s.indexOf('succeed') == -1) {
				alert(s);
				_id('seccode').focus();
		   		return false;
			} else {
				edit_save();
				obj.form.submit();
				return true;
			}
		});
	} else {
		edit_save();
		obj.form.submit();
		return true;
	}
}

function edit_album_show(id) {
	var obj = _id('uchome-edit-'+id);
	if(id == 'album') {
		_id('uchome-edit-pic').style.display = 'none';
	}
	if(id == 'pic') {
		_id('uchome-edit-album').style.display = 'none';
	}
	if(obj.style.display == '') {
		obj.style.display = 'none';
	} else {
		obj.style.display = '';
	}
}