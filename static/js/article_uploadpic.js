/*
	[iBOS!] (C)2001-2009 IBOS Inc.
	This is NOT a freeware, use is subject to license terms

	$Id: home_uploadpic.js 17964 2010-11-09 01:11:24Z monkey $
*/

var pic_attachexts = new Array();
var pic_attachwh = new Array();

var pic_insertType = 1;
var pic_thumbwidth = parseInt(60);
var pic_thumbheight = parseInt(60);
var pic_extensions = 'jpg,jpeg,gif,png';
var pic_forms;
var pic_nowUid = 0;
var pic_articleid = 0;
var pic_uploadStat = 0;
var pic_picid = 0;
var pic_nowid = 0;
var pic_mainForm;
var pic_successState = false;


function pic_moveup(id){
    up = $("#pic_upload_"+id).parent().parent().prev("tr");

    if(up[0]){
        my = $("#pic_upload_"+id).parent().parent();
        up.before(my[0])
    }
}
function pic_movedown(id){	
    down = $("#pic_upload_"+id).parent().parent().next("tr");
    if(down.next("tr")[0]){
        my = $("#pic_upload_"+id).parent().parent();
        down.after(my[0])
    }
}
function pic_delAttach(id) {
    _id('pic_attachbody').removeChild(_id('pic_attach_' + id).parentNode.parentNode.parentNode);
    if(_id('pic_attachbody').innerHTML == '') {
        pic_addAttach();
    }
    _id('localimgpreview_' + id + '_menu') ? document.body.removeChild(_id('localimgpreview_' + id + '_menu')) : null;
}
function pic_delAttachold(id) {
    _id('pic_attachbody').removeChild(_id('pic_upload_old_' + id).parentNode.parentNode);
    if(_id('pic_attachbody').innerHTML == '') {
        pic_addAttach();
    }
    _id('localimgpreview_' + id + '_menu') ? document.body.removeChild(_id('localimgpreview_' + id + '_menu')) : null;
	
    // 记录要删除的图片
    try {
        var InputNode = document.createElement("<input type=\"hidden\" id=\"pic_delpicid_" + id + "\" value=\""+ id +"\" name=\"delpicids[]\">");
    } catch(e) {
        var InputNode = document.createElement("input");
        InputNode.setAttribute("name", "delpicids[]");
        InputNode.setAttribute("type", "hidden");
        InputNode.setAttribute("id", "pic_delpicid_" + id);
        InputNode.setAttribute("value", id);
    }
    _id("articlepost").appendChild(InputNode);
}

function pic_addAttach() {
    newnode = _id('pic_attachbodyhidden').rows[0].cloneNode(true);
    var id = pic_nowid;
    var tags;
    tags = newnode.getElementsByTagName('form');
    for(i in tags) {
        if(tags[i].id == 'pic_upload') {
            tags[i].id = 'pic_upload_' + id;
        }
    }
    tags = newnode.getElementsByTagName('input');
    for(i in tags) {
        if(tags[i].name == 'pic_attach') {
            tags[i].id = 'pic_attach_' + id;
            tags[i].name = 'pic_attach';
            tags[i].onchange = function() {
                upic_insertAttach(id);
            	}
            tags[i].unselectable = 'on';	
        }
        if(tags[i].id == 'pic_articleid') {
            tags[i].id = 'pic_articleid_' + id;
        }
    }
    tags = newnode.getElementsByTagName('span');
    for(i in tags) {
        if(tags[i].id == 'pic_localfile') {
            tags[i].id = 'pic_localfile_' + id;
            continue;
        }
        if(tags[i].id == 'pic_previewfile'){
            tags[i].id = 'pic_previewfile_' + id;
            continue;
        }
    }
	
    pic_nowid++;

    _id('pic_attachbody').appendChild(newnode);
}

pic_addAttach();

function upic_insertAttach(id) {
    var localimgpreview = '';
    var path = _id('pic_attach_' + id).value;
    var ext = getExt(path);
    var re = new RegExp("(^|\\s|,)" + ext + "($|\\s|,)", "ig");
    var pic_localfile_ = _id('pic_attach_' + id).value.substr(_id('pic_attach_' + id).value.replace(/\\/g, '/').lastIndexOf('/') + 1);
    var pic_previewfile = _id('pic_attach_' + id).value.substr(_id('pic_attach_' + id).value.replace(/\\/g, '/').lastIndexOf('/') + 1);

    if(path == '') {
        return;
    }
    if(pic_extensions != '' && (re.exec(pic_extensions) == null || ext == '')) {
        alert('对不起，不支持上传此类扩展名的图片');
        return;
    }
    pic_attachexts[id] = inArray(ext, ['gif', 'jpg', 'jpeg', 'png']) ? 2 : 1;

    var inhtml = '<table cellspacing="0" cellpadding="0" class="up_row"><tr>';
    if(typeof no_insert=='undefined') {
        pic_localfile_ += '&nbsp;<a href="javascript:;" class="xi2" title="点击这里插入内容中当前光标的位置" onclick="pic_insertAttachimgTag(' + id + ');return false;">[插入]</a>';
    }
    inhtml += '<td><strong>' + pic_localfile_ +'</strong>';
    inhtml += '</td><td class="d">图片描述<br/><textarea name="pic_title" cols="40" rows="2" class="pt"></textarea>';
    inhtml += '</td><td class="o"><span id="pic_showmsg' + id + '">';
    inhtml += '<a href="javascript:;" onclick="pic_moveup('+id+');return false;" class="xi2">[上移]</a> ';
    inhtml += '<a href="javascript:;" onclick="pic_movedown('+id+');return false;" class="xi2">[下移]</a> ';
    inhtml += '<a href="javascript:;" onclick="pic_delAttach(' + id + ');return false;" class="xi2">[删除]</a>';
    inhtml += '</span>';
    inhtml += '</td></tr></table>';

    _id('pic_localfile_' + id).innerHTML = inhtml;
    _id('pic_attach_' + id).style.display = 'none';
    if(BROWSER.chrome){
        //读入文件 
        var reader = new FileReader(); 
        reader.onload = function(e){
            //e.target.result返回的即是图片的dataURI格式的内容
            var imgData = e.target.result,
            img = document.createElement('img');
            img.style.width="130px";
            img.src = imgData;
            _id('pic_previewfile_' + id).innerHTML="";
            _id('pic_previewfile_' + id).appendChild(img);
        }
        reader.readAsDataURL(_id('pic_attach_' + id).files[0]);
			
    }else if(BROWSER.ie && BROWSER.ie>=7){		
        var newPreview = _id('pic_previewfile_' + id);
        //alert(_id('pic_attach_' + id).value);
		
        newPreview.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale);";
        newPreview.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(src="+_id('pic_attach_' + id).value+");";
		
        //newPreview.filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = _id('pic_attach_' + id).value;
        newPreview.style.width = "120px";
        newPreview.style.height = "120px";
        img = document.createElement('img');
        newPreview.appendChild(img);
    //_id('pic_previewfile_' + id).appendChild(newPreview);
		
    }else{
        _id('pic_previewfile_' + id).innerHTML = '<img src="'+pic_insertAttach(_id('pic_attach_' + id))+'" width="130" />';
    }
	
    pic_addAttach();
}

function pic_insertAttach(obj){
    if (obj) {
        if (BROWSER.ie && BROWSER.ie < 7) {
            obj.select();
            return document.selection.createRange().text;

        } else if(BROWSER.chrome) {
            //读入文件 
            var reader = new FileReader(); 
            reader.onload = function(e){
                //e.target.result返回的即是图片的dataURI格式的内容
                var imgData = e.target.result;
                return imgData;
            }
            reader.readAsDataURL(obj.files[0]); 
			
        } else if(BROWSER.firefox) {
            if (obj.files) {
				return window.URL.createObjectURL(obj.files[0]);
                //return obj.files.item(0).getAsDataURL();
            }
            return obj.value;
        } else {
            return '';
        }
        return obj.value;
    }
}
function inArray(needle, haystack) {
    if(typeof needle == 'string') {
        for(var i in haystack) {
            if(haystack[i] == needle) {
                return true;
            }
        }
    }
    return false;
}

function pic_insertAttachimgTag(id) {
    edit_insert('[imgid=' + id + ']');
}

function pic_uploadSubmit(obj) {
    obj.disabled = true;
    pic_mainForm = obj.form;
    pic_forms = _id('pic_attachbody').getElementsByTagName("FORM");
    pic_articleid = _id('uploadalbum').value;
    pic_upload();
}

function pic_start_upload(form) {
    //_id('btnupload').disabled = true;
    pic_mainForm = _id(form);
    pic_forms = _id('pic_attachbody').getElementsByTagName("FORM");
    pic_upload();
}

function pic_upload(flag) {
    if(typeof(pic_forms[pic_nowUid]) == 'undefined') return false;
    var nid = pic_forms[pic_nowUid].id.split('_');
    if(pic_forms[pic_nowUid].id.match('old')){
		nid = nid[3];
	}else{
		nid = nid[2];
	}
    if(pic_nowUid>0) {
        var upobj = _id('pic_showmsg'+pic_nowid);
        if(pic_uploadStat==1) {
            if(flag){
                upobj.innerHTML = '检测完成';
            }else{
                upobj.innerHTML = '上传成功';
            }
            pic_successState = true;
            var InputNode;
            try {
                var InputNode = document.createElement("<input type=\"hidden\" id=\"picid_" + pic_picid + "\" value=\""+ pic_picid +"\" name=\"picids["+pic_nowid+"]\">");
            } catch(e) {
                var InputNode = document.createElement("input");
                InputNode.setAttribute("name", "picids["+pic_picid+"]");
                InputNode.setAttribute("type", "hidden");
                InputNode.setAttribute("id", "picid_" + pic_nowid);
                InputNode.setAttribute("value", pic_picid);
            }
            pic_mainForm.appendChild(InputNode);

        } else {
            upobj.style.color = "#f00";
            upobj.innerHTML = '上传失败 '+pic_uploadStat;
        }
    }

	
    if(_id('pic_showmsg'+nid) != null) {
			
        //加上一个判断，如果是编辑已经上传好的图片
        if(pic_forms[pic_nowUid].topicid.value>0){
				
            _id('pic_showmsg'+nid).innerHTML = '检测中，请等待';
				
            pic_picid = pic_forms[pic_nowUid].topicid.value;
            //pic_nowid = nid;
            //pic_nowUid++;
            //pic_uploadStat = 1;
            window.setTimeout("pic_skipupload()",300);
				
        }else{
			
            _id('pic_showmsg'+nid).innerHTML = '上传中，请等待(<a href="javascript:;" onclick="pic_forms[pic_nowUid].submit();">重试</a>)';
            _id('pic_articleid_'+nid).value = pic_articleid;
            pic_forms[pic_nowUid].submit();
			
        }
    } else if(pic_nowUid+1 == pic_forms.length) {
        if(typeof (no_insert) != 'undefined') {
            var articleidcheck = parseInt(parent.pic_articleid);
            _id('oparticleid').value = isNaN(articleidcheck)? 0 : pic_articleid;
            if(!pic_successState) return false;
        }
        window.onbeforeunload = null;
        pic_mainForm.submit();
    }

    pic_nowid = nid;
    pic_nowUid++;
    pic_uploadStat = 0;
}
function pic_skipupload(){
    pic_uploadStat = 1;
    pic_upload(true);
}