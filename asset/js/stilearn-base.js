$(function(){
    // control-mode. control for top and side mode (fixed or normal), you can remove it if you want..
    var control_mode = '<div id="modal-setup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modalSetupLabel" aria-hidden="true">'
    +'        <div class="modal-header">'
    +'            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
    +'            <h3 id="modalSetupLabel">Layout Mode & Themes</h3>'
    +'        </div>'
    +'        <div class="modal-body">'
    +'            <form>'
    +'                <div class="control-group">'
    +'                    <label class="control-label">Layout:</label>'
    +'                    <div class="controls">'
    +'                        <div class="btn-group">'
    +'                            <button id="normal-mode" type="button" class="btn btn-small btn-primary">Normal</button>'
    +'                            <button id="fixedtop-mode" type="button" class="btn btn-small btn-primary">Fixed top</button>'
    +'                            <button id="fixedside-mode" type="button" class="btn btn-small btn-primary">Fixed Side</button>'
    +'                            <button id="fixedsideonly-mode" type="button" class="btn btn-small btn-primary">Fixed Side Only</button>'
    +'                        </div>'
    +'                    </div>'
    +'                </div>'
    +'                <div class="control-group">'
    +'                    <label class="control-label">Themes:</label>'
    +'                    <div class="controls">'
    +'                        <a class="themes-changer grd-black" rel="tooltip" title="default" data-theme="" href="#"></a>'
    +'                        <a class="themes-changer grd-blue" rel="tooltip" title="blue" data-theme="blue" href="#"></a>'
    +'                        <a class="themes-changer grd-green" rel="tooltip" title="green" data-theme="green" href="#"></a>'
    +'                        <a class="themes-changer grd-orange" rel="tooltip" title="orange" data-theme="orange" href="#"></a>'
    +'                        <a class="themes-changer grd-purple" rel="tooltip" title="purple" data-theme="purple" href="#"></a>'
    +'                        <a class="themes-changer grd-purple-dark" rel="tooltip" title="purple-dark" data-theme="purple-dark" href="#"></a>'
    +'                        <a class="themes-changer grd-red" rel="tooltip" title="red" data-theme="red" href="#"></a>'
    +'                        <a class="themes-changer grd-sky" rel="tooltip" title="sky" data-theme="sky" href="#"></a>'
    +'                        <a class="themes-changer grd-win8" rel="tooltip" title="win8" data-theme="win8" href="#"></a>'
    +'                    </div>'
    +'                </div>'
    +'                <div class="control-group">'
    +'                    <label class="control-label">Header & Side:</label>'
    +'                    <div class="controls">'
    +'                        <div class="btn-group">'
    +'                            <button id="themes-mode-default" type="button" class="btn btn-small btn-primary">Default</button>'
    +'                            <button id="themes-mode-light" type="button" class="btn btn-small btn-primary">Light</button>'
    +'                            <button id="themes-mode-dark" type="button" class="btn btn-small btn-primary">Dark</button>'
    +'                        </div>'
    +'                    </div>'
    +'                </div>'
    +'            </form>'
    +'        </div>'
    +'        <div class="modal-footer">'
    +'            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>'
    +'        </div>'
    +'    </div>'
    +'<div class="control-mode" id="control-mode">'
    +'    <div class="navigate-mode"><a href="#modal-setup" role="button" data-toggle="modal" class="grd-black corner-bottom"><i class="icon-white icon-cog"></i></a></div>'
    +'</div>';
    
    
    $('body').append(control_mode)
    
    // theme mode
    $(window).load(function () {
        setTimeout(function () {
            if(sessionStorage.theme_mode == undefined){
                sessionStorage.theme_mode = '';
            }
            // theme setting
            if(sessionStorage.themes == undefined){
                sessionStorage.themes = '';
            }
            
            if(sessionStorage.themes == ''){
                $('#style-base').attr('href', '/asset/css/stilearn.css');
                $('#style-responsive').attr('href', '/asset/css/stilearn-responsive.css');
                $('#style-helper').attr('href', '/asset/css/stilearn-helper.css');
                $('#style-calendar').attr('href', '/asset/css/fullcalendar.css');
            }
            else{
                themes = sessionStorage.themes;
                theme_mode = sessionStorage.theme_mode;

                $('#style-base').attr('href', '/asset/css/themes/'+themes+'/stilearn.css');
                $('#style-responsive').attr('href', '/asset/css/themes/'+themes+'/stilearn-responsive.css');
                $('#style-helper').attr('href', '/asset/css/themes/'+themes+'/stilearn-helper.css');
                $('#style-calendar').attr('href', '/asset/css/themes/'+themes+'/fullcalendar.css');

                // add usage theme mode
                $('header.header, .side-left').addClass(theme_mode);
            }
        }, 100);
    })
    
    $('.themes-changer').click(function(e){
        
        $this = $(this);
        data_themes = $this.attr('data-theme');
        sessionStorage.themes = data_themes;
        
        if(data_themes == ''){
            $('#style-base').attr('href', '/asset/css/stilearn.css');
            $('#style-responsive').attr('href', '/asset/css/stilearn-responsive.css');
            $('#style-helper').attr('href', '/asset/css/stilearn-helper.css');
            
            // plugin theme, (custom your plugin)
            $('#style-calendar').attr('href', '/asset/css/fullcalendar.css');
        }
        else{
            $('#style-base').attr('href', '/asset/css/themes/'+data_themes+'/stilearn.css');
            $('#style-responsive').attr('href', '/asset/css/themes/'+data_themes+'/stilearn-responsive.css');
            $('#style-helper').attr('href', '/asset/css/themes/'+data_themes+'/stilearn-helper.css');
            $('#style-calendar').attr('href', '/asset/css/themes/'+data_themes+'/fullcalendar.css');
        }
        
        e.preventDefault();
    });
    
    $('#themes-mode-default').click(function(e){
        
        $('header.header, .side-left').removeClass('dark light');
        sessionStorage.theme_mode = '';
        
        e.preventDefault();
    });
    $('#themes-mode-light').click(function(e){
        
        $('header.header, .side-left').removeClass('dark light');
        $('header.header, .side-left').addClass('light');
        sessionStorage.theme_mode = 'light';
        
        e.preventDefault();
    });
    $('#themes-mode-dark').click(function(e){
        
        $('header.header, .side-left').removeClass('dark light');
        $('header.header, .side-left').addClass('dark');
        sessionStorage.theme_mode = 'dark';
        
        e.preventDefault();
    });
    // end theme setting
    
    // control mode
    if(sessionStorage.mode == undefined){
        sessionStorage.mode = 1;
    }
    
    $('html').on('click', function(){
        $('#control-mode .choice-mode').slideUp(); // toggle slide hide
    });
    $('#normal-mode').click(function(){
        $('.header, .side-left, .side-right').removeClass('fixed');
        
        // set position by default
        $('.header').css({
            'top' : '0px'
        });
        $('.side-left, .side-right').css({
            'top' : '60px'
        });
        
        $('#control-mode .choice-mode').slideToggle(); // toggle slide hide
        
        sessionStorage.mode = 1;
        return false;
    });
    $('#fixedtop-mode').click(function(){
        $('.header, .side-left, .side-right').removeClass('fixed'); // remove first to normalize class
        $('.header').addClass('fixed');
        
        // set position by default
        $('.header').css({
            'top' : '0px'
        });
        $('.side-left, .side-right').css({
            'top' : '60px'
        });
        
        $('#control-mode .choice-mode').slideToggle(); // toggle slide hide
        
        sessionStorage.mode = 2;
        return false;
    });
    $('#fixedside-mode').click(function(){
        $('.header, .side-left, .side-right').removeClass('fixed'); // remove first to normalize class
        $('.header, .side-left, .side-right').addClass('fixed');
        
        // set position by default
        $('.header').css({
            'top' : '0px'
        });
        
        $('.side-left, .side-right').css({
            'top' : '60px'
        });
        
        $('#control-mode .choice-mode').slideToggle(); // toggle slide hide
        
        sessionStorage.mode = 3;
        return false;
    });
    $('#fixedsideonly-mode').click(function(){
        $('.header, .side-left, .side-right').removeClass('fixed'); // remove first to normalize class
        $('.side-left, .side-right').addClass('fixed');
        
        // set position by default
        if($(window).scrollTop() > 60){
            $('.side-left, .side-right').css({
                'top' : '0px'
            });
        }
            
        $('#control-mode .choice-mode').slideToggle(); // toggle slide hide
        
        sessionStorage.mode = 4;
        return false;
    });
    
    if(sessionStorage.mode){
        if(sessionStorage.mode == '1'){ // normal mode
            $('.header, .side-left, .side-right').removeClass('fixed');
        }
        if(sessionStorage.mode == '2'){ // fixed header only
            $('.header, .side-left, .side-right').removeClass('fixed'); // remove first to normalize class
            $('.header').addClass('fixed');
        }
        if(sessionStorage.mode == '3'){ // fixed all
            $('.header, .side-left, .side-right').removeClass('fixed'); // remove first to normalize class
            $('.header, .side-left, .side-right').addClass('fixed')
        }
        if(sessionStorage.mode == '4'){ // fixed side only
            $('.header, .side-left, .side-right').removeClass('fixed'); // remove first to normalize class
            $('.side-left, .side-right').addClass('fixed');
        }
        
        // help for responsive
        if(sessionStorage.mode == 4){
            // control for responsive
            if($(window).width() > 767){
                data_scroll = 60 - parseInt($(this).scrollTop());
                $('.side-left, .side-right').css({
                    'top' : data_scroll+'px'
                });
                $('body, html').animate({
                    scrollTop : 0
                })
            }
            else{
                $('.side-left, .side-right').css({
                    'top' : '0px'
                });
            }
        }
        else{
            if($(window).width() <= 767){
                $('.side-left, .side-right').css({
                    'top' : '0px'
                });
            }
            else{
                $('.side-left, .side-right').css({
                    'top' : '60px'
                });
            }
        }
    }
    
    // end control-mode
    
    // control for responsive
    $(window).resize(function(){
        if(sessionStorage.mode == 4){
            // control for responsive
            if($(window).width() > 767){
                data_scroll = 60 - parseInt($(this).scrollTop());
                $('.side-left, .side-right').css({
                    'top' : data_scroll+'px'
                });
                $('body, html').animate({
                    scrollTop : 0
                })
            }
            else{
                $('.side-left, .side-right').css({
                    'top' : '0px'
                });
            }
        }
        else{
            if($(window).width() <= 767){
                $('.side-left, .side-right').css({
                    'top' : '0px'
                });
            }
            else{
                $('.side-left, .side-right').css({
                    'top' : '60px'
                });
            }
        }
    });
    
    
    // scrolling event
    $(window).scroll(function() {
        
        // this for hide/show button to-top
        if($(this).scrollTop() > 480) {
            $('a[rel=to-top]').fadeIn('slow');	
        } else {
            $('a[rel=to-top]').fadeOut('slow');
        }
        
        // this for sincronize active sidebar item
        if($(this).scrollTop() > 35){
            if(sessionStorage.mode == 3 || sessionStorage.mode == 4 ){
                $('.sidebar > li:first-child.active').removeClass('first');
            }
        }
        else{
            $('.sidebar > li:first-child.active').addClass('first');
        }
        
        if(sessionStorage.mode){
            if(sessionStorage.mode == 4){
                if($(this).scrollTop() > 60){
                    $('.side-left, .side-right').css({
                        'top' : '0px'
                    });
                }
                else{
                    // control for responsive
                    if($(window).width() > 767){
                        data_scroll = 60 - parseInt($(this).scrollTop());
                        $('.side-left, .side-right').css({
                            'top' : data_scroll+'px'
                        });
                    }
                    else{
                        $('.side-left, .side-right').css({
                            'top' : '0px'
                        });
                    }
                }
            }
            else{
                $('.header').css({
                    'top' : '0px'
                });
            }
        }
        
    });
    
    $('a[rel=to-top]').click(function(e) {
        e.preventDefault();
        $('body,html').animate({
            scrollTop:0
        }, 'slow');
    });
    // end scroll to top
    
    
    // tooltip helper
    $('[rel=tooltip]').tooltip();	
    $('[rel=tooltip-bottom]').tooltip({
        placement : 'bottom'
    });	
    $('[rel=tooltip-right]').tooltip({
        placement : 'right'
    });
    $('[rel=tooltip-left]').tooltip({
        placement : 'left'
    });	
    // end tooltip helper
    
    
    // animate scroll, define class scroll will be activate this
    $(".scroll").click(function(e){
        e.preventDefault();
        $("html,body").animate({scrollTop: $(this.hash).offset().top-60}, 'slow');
    });
    // end animate scroll
    
    
    // control box
    // collapse a box
    $('.header-control [data-box=collapse]').click(function(){
        var collapse = $(this),
        box = collapse.parent().parent().parent();

        collapse.find('i').toggleClass('icofont-caret-up icofont-caret-down'); // change icon
        box.find('.box-body').slideToggle(); // toggle body box
    });
    
    // collapse on load
    $('.box-body[data-collapse=true]').slideUp() // slide up onload
    .parent() // on .box
    .find('.header-control [data-box=collapse] i').toggleClass('icofont-caret-up icofont-caret-down'); // find the controller and change default icon
    
    // close a box
    $('.header-control [data-box=close]').click(function(){
        var close = $(this),
        box = close.parent().parent().parent(),
        data_anim = close.attr('data-hide'),
        animate = (data_anim == undefined || data_anim == '') ? 'fadeOut' : data_anim;

        box.addClass('animated '+animate);
        setTimeout(function(){
            box.hide()
        },1000);
    });
    // end control box
    
    // toggle sideright
    toggle_sideright = false;
    $('.side-right[data-toggle=on]').animate({
        right: "-="+216+"px"
    });
    $('.side-right.side-right-large[data-toggle=on]').animate({
        right: "-="+329+"px" // total = 545px (216+329)
    });
    $('.sideright-toggle-nav').click(function(){
        //$('.content').toggleClass('.content-large').parent().toggleClass('span11 span9');
        width_sideright_cur = $('.side-right[data-toggle=on]').width();
        $('.sideright-toggle-nav > i').toggleClass('icofont-arrow-left icofont-arrow-right');
        
        if(toggle_sideright == false){
            $('.side-right[data-toggle=on], .sideright-toggle-nav').animate({
                right: "+="+width_sideright_cur+"px"
            });
            toggle_sideright = true;
        }
        else{
            $('.side-right[data-toggle=on], .sideright-toggle-nav').animate({
                right: "-="+width_sideright_cur+"px"
            });
        
            toggle_sideright = false;
        }
        
        return false;
    });
    
    $(window).scroll(function() {
        if($(this).scrollTop() > 60){
            $('.side-right[data-toggle=on]').css({
                'top' : '0px'
            });
        }
        else{
            $('.side-right[data-toggle=on]').css({
                'top' : '60px'
            });
        }
    });
    // end toggle sideright
    
    // helper ie9
    var browser = $.browser;
    if ( browser.msie && browser.version == "9.0" ) {
        $('.input-icon-append .grd-white').css({
            'filter' : "none"
        })
    }
})

//语言项目
var lang = new Object();
lang.connecting_please_wait = "请稍后...";lang.prompt_msg = "请输入";lang.confirm_title = "操作提示";lang.please_select = "请选择";lang.dialog_title = "操作消息";lang.dialog_ok = "确定";lang.dialog_cancel = "取消";

;$(function($){
    $.fn.listTable = function(options) {
		var self = this,
			local_url = window.location.search,
			settings = {
				url: $(self).attr('data-acturi')
			}
		if(options) {
			$.extend(settings, options);
		}
        //表格操作轮换
        $("tbody tr", $(self)).live("mouseover",function(){
            $(this).find('.iShow').hide();
            $(this).find('.iHidden').show();
        });
        $("tbody tr", $(self)).live("mouseout",function(){
            $(this).find('.iHidden').hide();
            $(this).find('.iShow').show();
        });
        
        //AJAX修改
        $('span[data-tdtype="edit"]', $(self)).live('click', function() {
            var s_val   = $(this).text(),
            s_name  = $(this).attr('data-field'),
            s_id    = $(this).attr('data-id'),
            width   = $(this).width() + 20;
            $('<input type="text" class="input-small" value="'+s_val+'" />').width(width).focusout(function(){
                $(this).prev('span').show().text($(this).val());
                if($(this).val() != s_val) {
                    $.getJSON(settings.url, {id:s_id, field:s_name, val:$(this).val()}, function(result){
                        if(result.status == 0) {
                            $.iTips(result.msg,"error");
                            $('span[data-field="'+s_name+'"][data-id="'+s_id+'"]').text(s_val);
                            return;
                        }else{
                            
                        }
                    });
                }
                $(this).remove();
            }).insertAfter($(this)).focus().select();
            $(this).hide();
            return false;
        });
        
        //bool值切换
        $('img[data-tdtype="toggle"]', $(self)).live('click', function() {
            var img    = this,
                s_val  = ($(img).attr('data-value'))== 0 ? 1 : 0,
                s_name = $(img).attr('data-field'),
                s_id   = $(img).attr('data-id'),
                s_src  = $(img).attr('src');
            $.getJSON(settings.url, {id:s_id, field:s_name, val:s_val}, function(result){
                if(result.status == 1) {
                    if(s_src.indexOf('disabled')>-1) {
                        $(img).attr({'src':s_src.replace('disabled','enabled'),'data-value':s_val});
                    } else {
                        $(img).attr({'src':s_src.replace('enabled','disabled'),'data-value':s_val});
                    }
                }else{
                    $.iTips("动态更新数据失败","error");
                }
            });
            return false;
        });
    };
    
	//AJAX请求效果
	$('#iAjaxloading').ajaxStart(function(){
        $.iTips("<img src='/asset/img/loader_16.gif'> 提交请求中，请稍候...", 'success');
	}).ajaxSuccess(function(){
        $.iTips("close");
	});
    
    //全选反选
    $('.iCheckAll').live('click', function(){
        $('.iCheck').attr('checked', this.checked);
        $('.iCheckAll').attr('checked', this.checked);
    });
    
    //批量操作
    $('button[data-tdtype="batch"]').live('click', function() {
        var btn = this;
        if($('.iCheck:checked').length == 0){
            $.iTips('请先选择数据','warning');
            return false;
        }
        var ids = '';
        $('.iCheck:checked').each(function(){
            ids += $(this).val() + ',';
        });
        ids = ids.substr(0, (ids.length - 1));
        var uri = '';
        uri = ($(btn).attr('data-path') != undefined) ? window.location.pathname + '/' + $(btn).attr('data-path') : $(btn).attr('data-uri');
        uri += '/' + $(btn).attr('data-name') + '/' + ids;
        //$(btn).attr('data-path') = uri;
        //有data-path刚用data-path
        if($(btn).attr('data-msg') != undefined){
            iConfirm(btn, uri); //弹窗表单iDialog
        }else{
            iDialog(btn, uri); //操作确认iConfirm
        }
    });
    
    /**
    * 操作确认iConfirm 监听
    * 使用方式 <a href="javascript:;" class="iDialog" data-uri="" data-title="" ....>删除</a>
    * 基于jquery插件 artDialog 及 AJAX
    **/
	$('.iPrompt').live('click', function(){
        iPrompt(this); //调用iPrompt
	});
    
    /**
    * 操作确认iConfirm 监听
    * 使用方式 <a href="javascript:;" class="iDialog" data-uri="" data-title="" ....>删除</a>
    * 基于jquery插件 artDialog 及 AJAX
    **/
	$('.iConfirm').live('click', function(){
        iConfirm(this); //调用iConfirm
	});
    
    /**
    * 图片 监听
    * 使用方式 <img class='iImg' data-url='ssssss' />
    **/
    $('.iImg').each(function(){
        var uri = $(this).attr('data-uri')||"";
        $(this).attr('src', uri);
    });
    
    /**
    * 旺旺在线 监听
    * type: 1=全状态 2=小图标
    * 使用方式 <span class='iWangwang' data-nick='没有远方' data-type='2'>没有远方</span>
    **/
    $('.iWangwang').each(function(){
        var nick = ($(this).attr('data-nick') != undefined) ? $(this).attr('data-nick') : '英摩卡',
            type = ($(this).attr('data-type') != undefined) ? $(this).attr('data-type') : 1,
            msg = ($(this).attr('data-msg') != undefined) ? $(this).attr('data-msg') : '点这里给我发消息',
            html = '<a target="_blank" href="http://www.taobao.com/webww/ww.php?ver=3&touid='+nick+'&siteid=cntaobao&status='+type+'&charset=utf-8">';
        html += '<img border="0" src="http://amos.alicdn.com/online.aw?v=2&uid='+nick+'&site=cntaobao&s='+type+'&charset=utf-8" alt="'+msg+'" /></a>';
        $(this).html(html);
    });
    
    /**
    * 弹窗表单iDialog 监听
    * 使用方式 <a href="javascript:;" class="iDialog" data-uri="" data-title="" data-id=""....>编辑</a>
    * 基于jquery插件 artDialog 及 Form
    **/
	$('.iDialog').live('click', function(){
        iDialog(this); //调用iDialog
	});
    
	//附件预览
	$('.J_attachment_icon').live('mouseover', function(){
		var ftype = $(this).attr('file-type');
		var rel = $(this).attr('file-rel');
		switch(ftype){
			case 'image':
				if(!$(this).find('.attachment_tip')[0]){
					$('<div class="attachment_tip"><img src="'+rel+'" /></div>').prependTo($(this)).fadeIn();
				}else{
					$(this).find('.attachment_tip').fadeIn();
				}
				break;
		}
	}).live('mouseout', function(){
		$('.attachment_tip').hide();
	});
	
	$('.J_attachment_icons').live('mouseover', function(){
		var ftype = $(this).attr('file-type');
		var rel = $(this).attr('file-rel');
		switch(ftype){
			case 'image':
				if(!$(this).find('.attachment_tip')[0]){
					$('<div class="attachment_tip" style="width:160px; height:80px;"><img width="160" height="80" src="'+rel+'" /></div>').prependTo($(this)).fadeIn();
				}else{
					$(this).find('.attachment_tip').fadeIn();
				}
				break;
		}
	}).live('mouseout', function(){
		$('.attachment_tip').hide();
	});
});

//显示大图
;(function($){
	$.fn.preview = function(){
		var w = $(window).width();
		var h = $(window).height();
		
		$(this).each(function(){
			$(this).hover(function(e){
				if(/.png$|.gif$|.jpg$|.bmp$|.jpeg$/.test($(this).attr("data-bimg"))){
					$("body").append("<div id='preview'><img src='"+$(this).attr('data-bimg')+"' /></div>");
				}
				var show_x = $(this).offset().left + $(this).width();
				var show_y = $(this).offset().top;
				var scroll_y = $(window).scrollTop();
				$("#preview").css({
					position:"absolute",
					padding:"4px",
					border:"1px solid #f3f3f3",
					backgroundColor:"#eeeeee",
					top:show_y + "px",
					left:show_x + "px",
					zIndex:1000
				});
				$("#preview > div").css({
					padding:"5px",
					backgroundColor:"white",
					border:"1px solid #cccccc"
				});
				if (show_y + 230 > h + scroll_y) {
					$("#preview").css("bottom", h - show_y - $(this).height() + "px").css("top", "auto");
				} else {
					$("#preview").css("top", show_y + "px").css("bottom", "auto");
				}
				$("#preview").fadeIn("fast")
			},function(){
				$("#preview").remove();
			})					  
		});
	};
})(jQuery);

;(function($){
    //联动菜单
    $.fn.cate_select = function(options) {
        var settings = {
            field: 'iCatid',
            top_option: lang.please_select
        };
        if(options) {
            $.extend(settings, options);
        }

        var self = $(this),
            pid = self.attr('data-pid'),
            uri = self.attr('data-uri'),
            selected = self.attr('data-selected'),
            selected_arr = [];
        if(selected != undefined && selected != '0'){
        	if(selected.indexOf('|')){
        		selected_arr = selected.split('|');
        	}else{
        		selected_arr = [selected];
        	}
        }
        self.nextAll('.iSelect').remove();
        $('<option value="">--'+settings.top_option+'--</option>').appendTo(self);
        $.getJSON(uri, {id:pid}, function(result){
            if(result.status == '1'){
                for(var i=0; i<result.msg.length; i++){
                $('<option value="'+result.msg[i].id+'">'+result.msg[i].name+'</option>').appendTo(self);
                }
            }
            if(selected_arr.length > 0){
            	//IE6 BUG
            	setTimeout(function(){
            		self.find('option[value="'+selected_arr[0]+'"]').attr("selected", true);
	        		self.trigger('change');
            	}, 1);
            }
        });

        var j = 1;
        $('.iSelect').die('change').live('change', function(){
            var _this = $(this),
            _pid = _this.val();
            _this.nextAll('.iSelect').remove();
            if(_pid != ''){
                $.getJSON(uri, {id:_pid}, function(result){
                    if(result.status == '1'){
                        var _childs = $('<select class="iSelect input-small" data-pid="'+_pid+'"><option value="">--'+settings.top_option+'--</option></select>')
                        for(var i=0; i<result.msg.length; i++){
                            $('<option value="'+result.msg[i].id+'">'+result.msg[i].name+'</option>').appendTo(_childs);
                        }
                        _childs.insertAfter(_this);
                        if(selected_arr[j] != undefined){
                        	//IE6 BUG
                        	//setTimeout(function(){
			            		_childs.find('option[value="'+selected_arr[j]+'"]').attr("selected", true);
				        		_childs.trigger('change');
			            	//}, 1);
			            }
                        j++;
                    }
                });
                $('#'+settings.field).val(_pid);
            }else{
            	$('#'+settings.field).val(_this.attr('data-pid'));
            }
        });
    }
})(jQuery);

/**
* 提示方法i
* 使用方式 $.iTips('内容','warning',{sticky:true})
* 基于jquery插件 jGrowl
**/
$.iTips = function(message, theme, opts){
    var defaults = { //增加一些默认配置
        //sticky : true,
        life : 2000, //延时关闭时间
    };
    var themelist = { // 外观样式有 notice,warning,error,success
        notice:'alert alert-block',
        success:'alert alert-success',
        warning:'alert alert-info',
        error:'alert alert-error'
    };
    opts = $.extend({}, defaults, opts);
    theme = theme ? theme : 'notice';
    opts.theme = themelist[theme];
    message = message ? message : 'Loading';
    $.jGrowl(message, opts)
}

/**
* 填值操作iPrompt
* 使用方式 <a href="javascript:;" class="iPrompt" data-uri="" data-title="" ....>删除</a>
* data-id表示窗口ID及表单ID
* url主要是来自于批量操作里的预处理路径
* 基于jquery插件 artDialog 及 AJAX
**/
function iPrompt(obj, url){
    var self = $(obj),
        uri = (url != undefined) ? url : self.attr('data-uri'), //判断URI
        acttype = self.attr('data-acttype'),
        msg = (self.attr('data-msg') != undefined) ? self.attr('data-msg') : lang.prompt_msg,
        dfield = (self.attr('data-field') != undefined) ? self.attr('data-field') : 'id',
        dvalue = (self.attr('data-value') != undefined) ? self.attr('data-value') : '',
        dtitle = (self.attr('data-title') != undefined) ? self.attr('data-title') : lang.dialog_title,
        dokValue = (self.attr('data-ok') != undefined) ? self.attr('data-ok') : lang.dialog_ok,
        callback = self.attr('data-callback');
    $.dialog({
        title:dtitle,
        content:'<div class="control-group"><label class="control-label" for="focusedInput">'+msg+'</label><div class="controls"><input class="input-xlarge focused" id="focusedInput" type="text" value="'+dvalue+'"></div></div>',
        padding:'10px 20px',
        lock:false,
        okValue: dokValue,
        cancelValue: lang.dialog_cancel,
        ok:function(){
            var value = $("#focusedInput").val();
            iDialog(obj,uri+'/'+dfield+'/'+value);
        },
        cancel:function(){}
    });
};
/**
* 确认操作iConfirm
* 使用方式 <a href="javascript:;" class="iDialog" data-uri="" data-title="" ....>删除</a>
* data-id表示窗口ID及表单ID
* url主要是来自于批量操作里的预处理路径
* 基于jquery插件 artDialog 及 AJAX
**/
function iConfirm(obj, url){
    var self = $(obj),
        uri = (url != undefined) ? url : self.attr('data-uri'), //判断URI
        acttype = self.attr('data-acttype'),
        msg = self.attr('data-msg'),
        dtitle = (self.attr('data-title') != undefined) ? self.attr('data-title') : lang.dialog_title,
        dokValue = (self.attr('data-ok') != undefined) ? self.attr('data-ok') : lang.dialog_ok,
        callback = self.attr('data-callback');
    $.dialog({
        title:dtitle,
        content:msg,
        padding:'10px 20px',
        lock:true,
        okValue: dokValue,
        cancelValue: lang.dialog_cancel,
        ok:function(){
            if(acttype == 'ajax'){
                $.getJSON(uri, function(result){
                    if(result.status == 1){
                        $.iTips(result.msg);
                        if(callback != undefined){
                            eval(callback+'(self)');
                        }else{
                            window.location.reload();
                        }
                    }else{
                        $.iTips(result.msg, 'error');
                    }
                });
            }else{
                location.href = uri;
            }
        },
        cancel:function(){}
    });
};

/**
* 弹窗表单iDialog
* 使用方式 <a href="javascript:;" class="iDialog" data-uri="" data-title="" data-id=""....>编辑</a>
* data-id表示窗口ID及表单ID
* 基于jquery插件 artDialog 及 Form
**/
function iDialog(obj, url){
    var self = $(obj),
        uri = (url != undefined) ? url : self.attr('data-uri'), //判断URI
        did = self.attr('data-id'),
        dtitle = (self.attr('data-title') != undefined) ? self.attr('data-title') : lang.confirm_title,
        dokValue = (self.attr('data-ok') != undefined) ? self.attr('data-ok') : lang.dialog_ok,
        dwidth = (self.attr('data-width') != undefined) ? parseInt(self.attr('data-width')) : 'auto',
        dheight = (self.attr('data-height') != undefined) ? parseInt(self.attr('data-height')) : 'auto',
        dpadding = (self.attr('data-padding') != undefined) ? self.attr('data-padding') : '',
        dcallback = self.attr('data-callback');
    $.dialog({id:did}).close();
    $.dialog({
        id:did,
        title:dtitle,
        width:dwidth,
        height:dheight,
        padding:dpadding,
        lock:false,
        okValue: dokValue,
        cancelValue: lang.dialog_cancel,
        ok:function(){
            var ajax_form = this.dom.content.find('#'+did);
            if(ajax_form[0] != undefined){
                ajax_form.ajaxSubmit({
                    success: function (result) {
                        if(result.status == 1){
                            $.dialog.get(did).close();
                            $.iTips(result.msg);
                            if(dcallback != undefined){
                                eval(dcallback+'(self)');
                            }else{
                                window.location.reload();
                            }
                        } else {
                            $.iTips(result.msg, 'success');
                        }
                     }
                 });
            } else if(dcallback != undefined){
                eval(dcallback+'()');
            }
            
            return false;
        },
        cancel:function(){}
    });
    $.getJSON(uri, function(result){
        if(result.status == 1){
            $.dialog.get(did).content(result.msg);
        }
    });
    return false;
}

//用按钮形式显示Radio的选择器
function fnTriggerRadio(obj){
	$(obj).find("input").attr("checked", true);
}