<?php
class Listen {
	public $appkey = '12395060';
	public $secretKey = 'b33bee16fd5622daf8aacc7000f4f1f6';
	
	public $streamUrl = 'http://stream.api.taobao.com/stream';
	public $asynUrl = 'http://inmocha.duapp.com/Seller/Stream/asyn';
	public $diUrl = 'http://inmocha.duapp.com/Seller/Stream/discardinfo';
	
	private $server = array ();
	private $server_id = 0;
	private $link = null;
	private $crlf = "\r\n";
	private $listen_201 = 0;
	
	public function __construct($server_id = 0, $timeout = 5) {
		$this->server = parse_url($this->streamUrl);
		$this->server['timeout'] = $timeout;
		$this->server_id = $server_id;
	}
	
	public function connect() {
		$this->link = @fsockopen($this->server['host'], $this->server['port'] ? $this->server['host'] : 80, $iError, $strError, $this->server['timeout']);
		if (!$this->check()) {
			return false;
		}
		// var_dump($this->link);
		stream_set_blocking($this->link, true);
		stream_set_timeout($this->link, 3);
		$this->server['info'] = stream_get_meta_data($this->link);
	}
	
	public function set_server_id($id = 0) {
		$this->server_id = $id;
	}
	
	// 淘宝api认证
	public function auth() {
		$postdata = array('app_key' => $this->appkey, 'timestamp' => date('Y-m-d H:i:s'), 'id' => '0');
		$postdata['sign'] = $this->generateSign ($postdata, $this->secretKey);
		$postdata_str = '';
		foreach ($postdata as $k => $v) {
			$postdata_str .= urlencode($k) . '=' . urlencode($v) . '&';
		}
		$this->send("POST {$this->server['path']} HTTP/1.1");
		$this->send("Host: {$this->server['host']}" );
		$this->send("Content-Type: application/x-www-form-urlencoded");
		$this->send("Content-Length: " . strlen($postdata_str ) . "\r\n");
		$this->send($postdata_str );
		$this->send("Id: {$this->server_id}" );
		$this->send();
	}
	
	public function check() {
		return (bool) $this->link;
	}
	
	public function send($msg = '') {
		return fputs($this->link, $msg . $this->crlf);
	}
	
	public function is_eof() {
		if (!$this->check()) return true; // to exit
		return feof($this->link) || $this->server['info']['time_out'];
	}
	
	public function get_msg() {
		$line = trim(fgets($this->link));
		$this->server['info'] = stream_get_meta_data($this->link);
		// var_dump($line);
		if ($line && substr($line, 0, 10) == '{"packet":') { // {"packet":
			 $line = preg_replace("/\":(\d+)/i",'":"$1"', $line); //将科学计数方式转成整数
			$ret = json_decode($line,true); // json msg
			if (is_array($ret)) {
				$ret = $ret['packet'];
			} else {
				$ret = false;
			}
			// } elseif ($line) {
			// $ret = false;
		} else {
			$ret = null;
		}
		
		ob_flush();
		flush();
		return $ret;
	}
	
	public function close() {
		$ret = fclose($this->link);
		$this->link = null;
		return $ret;
	}
	
	//处理202正常业务
	function asyn($data) {
		$data = reset($data);;
		$this->httpPost($this->asynUrl, $data);
	}
	
	//处理203丢失业务包
	function discardinfo($data) {
		$this->httpPost($this->diUrl, $data);
	}
	
	/**
	 * 轮询丢失的消息包
	 * @param $start_time 服务重新开始时间
	 * @param $timeout 频率(秒)
	 */
	public function discard() {
		$_now = time();
		$timeout = 86400;
		$cache = $this->F('config'); //读取监听记录
		if (!$cache['last']) {
			$cache['last'] = date('Y-m-d H:i:s',$_now - $timeout);
		}
		$this->msg('轮询丢失的消息包' . $cache['last'] . '/' . date('Y-m-d H:i:s',$_now));
		
		$data['begin'] = strtotime($cache['last']) * 1000;
		$data['end'] = $_now * 1000;
		
		$this->httpPost($this->diUrl, $data);
	}
	
	//发包函数
	function httpPost($url, $data, $timeout = '3') {
		$url = $this->urlinfo($url); //获取URL信息
		$query = $url["request"];
		
		if(is_array($data)) { //数组转成拼接
			foreach($data as $k =>$v) {
				$data_arr .=(empty($data_arr) ? "" : "&").urlencode($k)."=".urlencode($v);
			}
		}
		
		$header = "POST ".$query." HTTP/1.1\r\n";
		$header .= "Host:".$url["host"]."\r\n";
		$header .= "Content-type: application/x-www-form-urlencoded\r\n";
		$header .= "Content-Length:".strlen($data_arr)."\r\n";
		//$header .= "Connection: Keep-Alive\r\n\r\n";
		$header .= "Connection: Close\r\n\r\n";
		if ($data_arr) $header.= "$data_arr\r\n\r\n";
		
		$fp = fsockopen($url["host"],$url["port"],$errno, $errstr, $timeout);
		if (!$fp) $this->msg("fsockopen打开失败：$errstr ($errno)");
		fwrite($fp,$header);
		/* while (!feof($conn)) {
		 $resp .= fgets($conn, 8192);
		} */
		
		fclose($fp);
		unset($header);
		return true;
	}
	
	/**
	 * 锁定监听进程
	 * 确保连接不会被随意覆盖
	 */
/* 	public function lock() {
		$cookie_time = date('Y-m-d H:i:s') + 86400 * 30;
		cookie('listen_time', date('Y-m-d H:i:s'), $cookie_time );  //进程开始时间
		cookie('listen_pid', getmypid(), $cookie_time );  //监听进程PID
	} */
	
	/**
	 * 验证监听的锁定
	 * 确保连接不会被随意覆盖
	 */
/* 	public function unlock() {
		$lock_time = cookie('listen_time');
		if ((SYS_TIME-$lock_time) < 85000) { //如果时间没有到23.6小时，禁止运行第二个实例
			$pid = cookie('listen_pid');
			echo "上次运行时间是：".date('Y-m-d H:i:s', $lock_time)."<br />";
			echo "还差".((date('Y-m-d H:i:s')-$lock_time)/3600)."小时。<br />";
			echo "正在监听的网页进程：$pid";
			exit();  //退出
		}
	} */
	
	public function listen_201($time = 0, $key = '') {
		if ($key) {
			$key = '-' . $key;
		}
		/* if ($time) {
			setConfig ( 'var_listen-201' . $key, $time );
		}
		$path = DIR . DS . 'config' . DS . formatName ( 'var_listen-201' . $key ) . '.php';
		if (is_file ( $path )) {
			$data = include ($path);
			return ( int ) $data;
		} else {
			return 0;
		} */
	}
	
	public function listen_pid($pid = '') {
		/* if ($pid) {
			setConfig ( 'var_listen-pid', $pid );
		}
		$path = DIR . DS . 'config' . DS . formatName ( 'var_listen-pid' ) . '.php';
		if (is_file ( $path )) {
			$data = include ($path);
			return ( int ) $data;
		} else {
			return 0;
		} */
	}
	
	/**
	 * 记录最后监听时间， 下次连接轮询
	 * @param $start_time 服务开始时间
	 * @param $timeout 频率(秒)
	 */
	public function living($start_time, $first = 0) {
		if ($first) {
			$cache = $this->F('config');
			if (!is_array($cache)) {
				$cache = array();
				$cache['break'] = 0; //是否退出
			}
			$cache['start'] = date('Y-m-d H:i:s', $start_time); //记录开始时间
			$this->F('config', $cache);
		}
		$timeout = 300;
		$_now = time();
		if (($_now - $start_time) > $timeout) {
			$cache = $this->F('config');
			$cache['last'] = date('Y-m-d H:i:s', $_now); //记录最后存活时间
			if ($cache['break']) {
				$break = 1;
				$cache['break'] = 0;
			}
			$this->F('config', $cache);
			if ($break) return false; //强行退出
			return $_now;
		}
		return $start_time;
	}
	
	/**
	 * URl相关信息
	 * 'scheme' => 'http',
	 * 'host' => '127.0.0.1',
	 * 'path' => '/index.php',
	 * 'query' => 'm=taobao&c=stream&a=asyn',
	 * 'port' => '80',
	 * 'ip' => '127.0.0.1',
	 * 'request' => '/index.php?m=taobao&c=stream&a=asyn',
	 */
	public function urlinfo($url) {
		if (!$url) return false;
		$url_arr  = parse_url($url);
		$url_arr["path"]    = empty($url_arr["path"]) ? "/"  : $url_arr["path"];
		$url_arr["port"]    = empty($url_arr["port"]) ? "80" : $url_arr["port"];
		$url_arr["ip"]      = gethostbyname($url_arr["host"]);
		$url_arr["request"] = $url_arr["path"] . (empty($url_arr["query"]) ? "" : "?" . $url_arr["query"]) . (empty($url_arr["fragment"]) ? "" : "#" . $url_arr["fragment"]);
		
		return $url_arr;
	}
	
	/**
	 * 快速文件数据读取和保存 针对简单类型数据 字符串、数组
	 * @param string $name 缓存名称
	 * @param mixed $value 缓存值
	 * @param string $path 缓存路径
	 * @return mixed
	 */
	function F($name, $value='', $path = '') {
		$filename       = $path . $name . '.php';
		if ('' !== $value) {
			if (is_null($value)) {
				// 删除缓存
				return false !== strpos($name,'*')?array_map("unlink", glob($filename)):unlink($filename);
			} else {
				// 缓存数据
				$dir            =   dirname($filename);
				// 目录不存在则创建
				if (!is_dir($dir))
					mkdir($dir,0755,true);
				return file_put_contents($filename, $this->strip_whitespace("<?php\treturn " . var_export($value, true) . ";?>"));
			}
		}
		// 获取缓存数据
		if (is_file($filename)) {
			$value          =   include $filename;
		} else {
			$value          =   false;
		}
		return $value;
	}
	
	/**
	 * 记录日志
	 * @param string $str 缓存名称
	 * @param $file 数据路径
	 */
	public function msg($str, $name='') {
		$path = 'Logs/';
		!$name && $name = date('y-m-d');
		$filename = $path . $name;
		$time = date('H-i-s');
		$content = $time . ' | ' . ListenPID . ' | ' . $str . "\r\n";
		error_log($content, 3, $filename.'.log');
	}
	
	/**
	 * 去除代码中的空白和注释
	 * @param string $content 代码内容
	 * @return string
	 */
	function strip_whitespace($content) {
		$stripStr   = '';
		//分析php源码
		$tokens     = token_get_all($content);
		$last_space = false;
		for ($i = 0, $j = count($tokens); $i < $j; $i++) {
			if (is_string($tokens[$i])) {
				$last_space = false;
				$stripStr  .= $tokens[$i];
			} else {
				switch ($tokens[$i][0]) {
					//过滤各种PHP注释
					case T_COMMENT:
					case T_DOC_COMMENT:
						break;
						//过滤空格
					case T_WHITESPACE:
						if (!$last_space) {
							$stripStr  .= ' ';
							$last_space = true;
						}
						break;
					case T_START_HEREDOC:
						$stripStr .= "<<<THINK\n";
						break;
					case T_END_HEREDOC:
						$stripStr .= "THINK;\n";
						for($k = $i+1; $k < $j; $k++) {
							if(is_string($tokens[$k]) && $tokens[$k] == ';') {
								$i = $k;
								break;
							} else if($tokens[$k][0] == T_CLOSE_TAG) {
								break;
							}
						}
						break;
					default:
						$last_space = false;
						$stripStr  .= $tokens[$i][1];
				}
			}
		}
		return $stripStr;
	}
	
	/**
	 * TOP签名
	 */
	function generateSign($params, $secretKey) {
		ksort ($params);
		
		$stringToBeSigned = $secretKey;
		foreach ( $params as $k => $v ) {
		if (is_null ( $v )) continue; //判断键和键值非空
		$stringToBeSigned .= "$k$v";
		}
		unset ( $k, $v );
		$stringToBeSigned .= $secretKey;
		
		return strtoupper ( md5 ( $stringToBeSigned ) );
	}
}

