<?php
//error_reporting(E_ALL & ~(E_STRICT | E_NOTICE));

//应用程序环境，可选：debug,release,
defined('APP_ENV') or define('APP_ENV','debug');

require_once('MoCha/common/common.php');
//require_once('MoCha/common/common_local.php');
//require_once('MoCha/common/yii.php');

if (APP_ENV == 'release') {
    error_reporting(0);
    $yii=dirname(__FILE__).'/_Core/yiilite.php';
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',1);
} else {
    $yii=dirname(__FILE__).'/_Core/yii.php';
    defined('YII_DEBUG') or define('YII_DEBUG',true);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
}
$config=dirname(__FILE__).'/MoCha/config/'.APP_ENV.'.php';

require_once($yii);
Yii::createWebApplication($config)->run();