<?php
/**
 * 淘宝API处理类-订单
 */
class TaoTrade extends Taoapi
{

    public function __construct() {
        parent::__construct();
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * 同步淘宝和本地交易数据
     * @param $tid 传入TID
     * @param $field 需要更新的字段
     * @param $nick 商家昵称 用于获取SESSIONKEY
     * 包含以下字段的返回会增加TOP的后台压力:commission_fee, buyer_alipay_no, buyer_email, timeout_action_time, item_memo, trade_memo, available_confirm_fee
     */
    public function synTrade($tid, $fields = "", $nick = "") {
        $this->method = 'trade.fullinfo';
        $this->session = $this->_session;
        if (!$fields) {
            $this->fields = "seller_nick, buyer_nick, type, created, tid, seller_rate, buyer_rate, status, payment, post_fee, total_fee, pay_time, end_time, modified, consign_time, buyer_obtain_point_fee, point_fee, real_point_fee, received_payment, commission_fee, cod_fee, cod_status, shipping_type, receiver_name, receiver_state, receiver_city, receiver_district, receiver_address, receiver_zip, receiver_mobile, receiver_phone, seller_memo, alipay_no, buyer_message, buyer_alipay_no, buyer_email, seller_flag, available_confirm_fee, has_post_fee, trade_memo, is_3D, invoice_name, promotion_details, orders, trade_from";
        } else {
            $this->fields = $fields . ",tid,modified";
        }
        $this->where = "tid=$tid";
        $trade = $this->select()->get();

        if (isset($trade['trade_fullinfo_get_response']['trade'])) {
            $this->saveTrade($trade['trade_fullinfo_get_response']['trade']); //入库

            return true;
        } else {
            return false;
        }
    }

    /**
     * 批量同步交易数据
     * @param $where 查询条件  start_created，end_created，status，page_no，page_size，buyer_nick，type，ext_type，rate_status，tag，use_has_next，is_acookie
     * @param $field 需要更新的字段
     * @param $nick 商家昵称 用于获取SESSIONKEY
     */
    public function synTrades($where, $fields = "", $nick = "") {
        $this->session = $this->_session;

        $this->method = 'trades.sold';
        $this->fields = 'tid';
        $this->select($where);

        $this->method = 'trade.fullinfo';
        if (!$fields) {
            $this->fields = "seller_nick, buyer_nick, type, created, tid, seller_rate, buyer_rate, status, payment, post_fee, total_fee, pay_time, end_time, modified, consign_time, buyer_obtain_point_fee, point_fee, real_point_fee, received_payment, commission_fee, cod_fee, cod_status, shipping_type, receiver_name, receiver_state, receiver_city, receiver_district, receiver_address, receiver_zip, receiver_mobile, receiver_phone, seller_memo, alipay_no, buyer_message, buyer_alipay_no, buyer_email, seller_flag, available_confirm_fee, has_post_fee, trade_memo, is_3D, invoice_name, promotion_details, orders, trade_from";
        } else {
            $this->fields = $fields . ",tid,modified";
        }
        $this->select(array('tid'=>'__SUBSELECT__'));

        $result = $this->sub()->get();

        foreach ($result as $trade) {
            if (isset($trade['trade_fullinfo_get_response']['trade']))
                $this->saveTrade($trade['trade_fullinfo_get_response']['trade']); //入库
        }

        return true;
    }

    /**
     * 批量同步交易数据
     * @param $where 查询条件  start_created，end_created，status，page_no，page_size，buyer_nick，type，ext_type，rate_status，tag，use_has_next，is_acookie
     * @param $field 需要更新的字段
     * @param $nick 商家昵称 用于获取SESSIONKEY
     */
    public function synSold($where, $fields = "") {
        $this->session = $this->_session;
        $this->method = 'trades.sold';
        if (!$fields) {
            $this->fields = "tid,num,num_iid,status,title,type,price,discount_fee,point_fee,total_fee,is_lgtype,is_brand_sale,is_force_wlb,lg_aging,lg_aging_type,created,pay_time,modified,end_time,alipay_id,alipay_no,seller_flag,buyer_nick,buyer_area,has_yfx,yfx_fee,yfx_id,yfx_type,has_buyer_message,credit_card_fee,nut_feature,step_trade_status,step_paid_fee,mark_desc,send_time,shipping_type,adjust_fee,buyer_obtain_point_fee,cod_fee,trade_from,cod_status,service_orders,commission_fee,buyer_rate,trade_source,seller_can_rate,seller_nick,pic_path,payment,seller_rate,real_point_fee,post_fee,receiver_name,receiver_state,receiver_city,receiver_district,receiver_address,receiver_zip,receiver_mobile,receiver_phone,consign_time,received_payment,orders";
        } else {
            $this->fields = $fields;
        }
        $this->where = $where;

        $result = $this->select()->get();

        return $result;
    }

    /**
     * 同步订单物流信息
     * @param $tid 传入TID
     * @param $nick 商家昵称  用于主动通知
     */
    public function TradeDelivery($tid) {
        $this->method = 'logistics.orders';
        $this->session = $this->_session;
        $this->fields = "delivery_start, delivery_end, out_sid, created, modified, status, freight_payer, company_name";
        $this->where = "tid=$tid";
        $shibping = $this->select()->get();

        $shipping = $shibping['logistics_orders_get_response']['shippings']['shipping'];
        $shipping = reset($shipping);
            
        $trade['delivery_start'] = strtotime($shipping['delivery_start'] );
        $trade['delivery_end'] = strtotime($shipping['delivery_end'] );
        $trade['out_sid'] = $shipping['out_sid'];
        $trade['delivery_company'] = $shipping['company_name'];
        $trade['delivery_created'] = strtotime($shipping['created'] );
        $trade['delivery_modified'] = strtotime($shipping['modified'] );
        $trade['delivery_status'] = $shipping['status'];

        unset($shipping);
        
        return $trade;
    }

    /**
     * 保存淘宝交易数据
     * @param trade $trade
     */
    public function saveTrade($trade)
    {
        $trade = $this->formatTrade($trade); //格式化
        
        $id = $trade['id'];
        if (isset($trade['orders'])) {
            $this->saveOrder($id, $trade['orders']['order']);  //更新订单数据
            unset($trade['orders']);
        }
        if (in_array($trade['status'], array(Trade::WAIT_BUYER_CONFIRM_GOODS, Trade::TRADE_BUYER_SIGNED, Trade::TRADE_FINISHED))) { // 物流信息
            $shipping = $this->TradeDelivery($id);
            $trade = array_merge($trade, $shipping);
        }
        
        $is_exists = Trade::model()->exists("id=:id",array(":id"=>$id));
        if(!$is_exists)
        {
            $model = new Trade;
            $model->attributes = $trade;
            $model->save();
        }
        else
            Trade::model()->updateByPk($id, $trade);

        return $id;
    }

    /**
     * 同步淘宝和本地订单数据
     * @param $trade_id 交易编号
     * @param $orders 订单数据
     */
    public function saveOrder($trade_id, $orders) {
        foreach ($orders as $order) {
            $order = $this->formatOrder($order);
            $order['trade_id'] = $trade_id;
            $id = $order['id'];

            $is_exists = Order::model()->exists("id=:id",array(":id"=>$id));
            if(!$is_exists)
            {
                $model = new Order;
                $model->attributes = $order;
                $model->save();
            }
            else
                Order::model()->updateByPk($id, $order);
        }
    }

    /**
     * 转换交易数据格式
     * @param $trade 传入交易数据
     */
    public function formatTrade($trade)
    {
        $trade['id'] = $trade['tid'];
        $trade['shop_id'] = Shop::model()->getIdByNick($trade['seller_nick']);
        unset($trade['tid'], $trade['seller_nick']);

        if(isset($trade['status'])) $trade['status'] = self::formatAlias('status', $trade['status']);
        if(isset($trade['trade_from'])) $trade['trade_from'] = self::formatAlias('tradeFrom', $trade['trade_from']);
        if(isset($trade['type'])) $trade['type'] = self::formatAlias('type', $trade['type']);
        if(isset($trade['shipping_type'])) $trade['shipping_type'] = self::formatAlias('shippingType', $trade['shipping_type']);
        if(isset($trade['cod_status'])) $trade['cod_status'] = self::formatAlias('codStatus', $trade['cod_status']);

        if (isset($trade['created'])) $trade['created'] = strtotime($trade['created']);
        if (isset($trade['modified'])) $trade['modified'] = strtotime($trade['modified']);
        if (isset($trade['end_time'])) $trade['end_time'] = strtotime($trade['end_time']);
        if (isset($trade['pay_time'])) $trade['pay_time'] = strtotime($trade['pay_time']);
        if (isset($trade['consign_time'])) $trade['consign_time'] = strtotime($trade['consign_time']);
        
        if (isset($trade['promotion_details'])) $trade['promotion_details'] = serialize($trade['promotion_detail']);
        
        if (isset($trade['has_post_fee'])) $trade['has_post_fee'] = Ya::str2bool($trade['has_post_fee']);
        if (isset($trade['is_lgtype'])) $trade['is_lgtype'] = Ya::str2bool($trade['is_lgtype']);
        if (isset($trade['is_brand_sale'])) $trade['is_brand_sale'] = Ya::str2bool($trade['is_brand_sale']);
        if (isset($trade['is_force_wlb'])) $trade['is_force_wlb'] = Ya::str2bool($trade['is_force_wlb']);
        if (isset($trade['has_yfx'])) $trade['has_yfx'] = Ya::str2bool($trade['has_yfx']);
        if (isset($trade['has_buyer_message'])) $trade['has_buyer_message'] = Ya::str2bool($trade['has_buyer_message']);
        if (isset($trade['can_rate'])) $trade['can_rate'] = Ya::str2bool($trade['can_rate']);
        if (isset($trade['buyer_rate'])) $trade['buyer_rate'] = Ya::str2bool($trade['buyer_rate']);
        if (isset($trade['seller_can_rate'])) $trade['seller_can_rate'] = Ya::str2bool($trade['seller_can_rate']);
        if (isset($trade['is_part_consign'])) $trade['is_part_consign'] = Ya::str2bool($trade['is_part_consign']);
        if (isset($trade['seller_rate'])) $trade['seller_rate'] = Ya::str2bool($trade['seller_rate']);
        if (isset($trade['is_3D'])) $trade['is_3D'] = Ya::str2bool($trade['is_3D']);
        
        return $trade;
    }

    /**
     * 转换交易数据格式
     * @param $trade 传入交易数据
     */
    public static function formatAlias($type,$code=NULL)
    {
        $_items = array(
            'status' => array(
                'WAIT_BUYER_PAY' => Trade::WAIT_BUYER_PAY,
                'WAIT_SELLER_SEND_GOODS' => Trade::WAIT_SELLER_SEND_GOODS,
                'WAIT_BUYER_CONFIRM_GOODS' => Trade::WAIT_BUYER_CONFIRM_GOODS,
                'TRADE_BUYER_SIGNED' => Trade::TRADE_BUYER_SIGNED,
                'TRADE_FINISHED' => Trade::TRADE_FINISHED,
                'TRADE_CLOSED' => Trade::TRADE_CLOSED,
                'TRADE_CLOSED_BY_TAOBAO' => Trade::TRADE_CLOSED_BY_TAOBAO,
                'TRADE_NO_CREATE_PAY' => Trade::TRADE_NO_CREATE_PAY,
            ),
            'codStatus' => array(
                'NEW_CREATED' => Trade::NEW_CREATED,
                'WAITING_TO_BE_SENT' => Trade::WAITING_TO_BE_SENT,
                'ACCEPTED_BY_COMPANY' => Trade::ACCEPTED_BY_COMPANY,
                'REJECTED_BY_COMPANY' => Trade::REJECTED_BY_COMPANY,
                'RECIEVE_TIMEOUT' => Trade::RECIEVE_TIMEOUT,
                'TAKEN_IN_SUCCESS' => Trade::TAKEN_IN_SUCCESS,
                'TAKEN_IN_FAILED' => Trade::TAKEN_IN_FAILED,
                'TAKEN_TIMEOUT' => Trade::TAKEN_TIMEOUT,
                'REJECTED_BY_OTHER_SIDE' => Trade::REJECTED_BY_OTHER_SIDE,
                'SIGN_IN' => Trade::SIGN_IN,
                'CANCELED' => Trade::CANCELED,
                'CLOSED' => Trade::CLOSED,
            ),
            'type' => array(
                'fixed' => Trade::TYPE_FIXED,
                'auction' => Trade::TYPE_AUCTION,
                'guarantee_trade' => Trade::TYPE_GUARANTEE_TRADE,
                'auto_delivery' => Trade::TYPE_AUTO_DELIVERY,
                'independent_simple_trade' => Trade::TYPE_INDEPENDENT_SIMPLE_TRADE,
                'independent_shop_trade' => Trade::TYPE_INDEPENDENT_SHOP_TRADE,
                'cod' => Trade::TYPE_COD,
                'fenxiao' => Trade::TYPE_FENXIAO,
                'game_equipment' => Trade::TYPE_GAME_EQUIPMENT,
                'shopex_trade' => Trade::TYPE_SHOPEX_TRADE,
                'netcn_trade' => Trade::TYPE_NETCN_TRADE,
                'external_trade' => Trade::TYPE_EXTERNAL_TRADE,
            ),
            'shippingType' => array(
                'free' => Trade::SHIP_FREE,
                'ems' => Trade::SHIP_EMS,
                'post' => Trade::SHIP_POST,
                'express' => Trade::SHIP_EXPRESS,
                'virtual' => Trade::SHIP_VIRTUAL,
            ),
            'tradeFrom' => array(
                'TAOBAO' => Trade::FROM_TAOBAO,
                'WAP' => Trade::FROM_WAP,
                'HITAO' => Trade::FROM_HITAO,
                'TOP' => Trade::FROM_TOP,
                'JHS' => Trade::FROM_JHS,
            )
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    /**
     * 转换订单数据格式
     * @param $order 传入订单数据
     */
    public function formatOrder($order) {
        $order['id'] = $order['oid'];
        $order['item_id'] = $order['num_iid'];
        unset($order['oid'], $order['num_iid']);
        if ($order['end_time']) $order['end_time'] = strtotime($order['end_time']);
        if (isset($order['buyer_rate'])) $order['buyer_rate'] = Ya::str2bool($order['buyer_rate']);
        if (isset($order['seller_rate'])) $order['seller_rate'] = Ya::str2bool($order['seller_rate']);
        if (isset($order['is_oversold'])) $order['is_oversold'] = Ya::str2bool($order['is_oversold']);
        
        return $order;
    }

    /**
     * 更改订单备注
     * @param $tid 传入TID
     * @param $memo 备注信息
     * @param $nick 商家昵称  用于主动通知
     */
    public function memo($tid, $memo) {
        $this->method = 'trade.memo';
        $this->session = $this->_session;
        $this->where = array('tid'=>$tid);

        $update = array('flag'=>$memo['flag']);
        if (array_key_exists('memo', $memo)) {
            $seller_memo = trim($memo['memo']);
            if ($seller_memo) {
                $update['memo'] = "#memo#";
                $this->memo = $seller_memo;
            } else {
                $update['reset'] = 'true';
            }
        }
        $result = $this->update($update)->get();
        
        return isset($result['trade_memo_update_response']) ? true : false;
    }
    
    /**
     * 延长收货时间
     * @param $tid 交易编号
     * @param $delay 延长天数 可选值 ：3, 5, 7, 10
     * @param $nick 商家昵称  用于主动通知
     */
    public function delay($tid, $delay, $nick = "") {
        $this->method = 'taobao.trade.receivetime.delay';
        $this->session = $this->_session;
        $this->where = "tid=$tid";
        $result = $this->update(array('days'=>$delay))->get();

        return isset($result['trade']) ? true : false;
    }
    
    /**
     * 关闭交易
     * @param $tid 交易编号
     * @param $close 延长天数 可选值 ：3, 5, 7, 10
     * @param $nick 商家昵称  用于主动通知
     */
    public function close($tid, $close) {
        $this->method = 'taobao.trade.close';
        $this->session = $this->_session;
        $this->where = "tid=$tid";
        $result = $this->update(array('close_reason'=>$close))->get();
    
        return isset($result['trade']) ? true : false;
    }
    
    /**
     * 更新销售属性
     * @param $oid 订单编号
     * @param $sku_id 销售属性编号
     * @param $sku_props 销售属性组合串 格式：p1:v1;p2:v2
     */
    public function sku($oid, $sku_id, $sku_props, $nick = "") {
        $this->method = 'trade.ordersku';
        $this->session = $this->_session;
        $this->where = "oid=$oid";
        $result = $this->update(array('sku_id'=>$sku_id,'sku_props'=>$sku_props))->get();
    
        return isset($result['trade_ordersku_update_response']) ? true : false;
    }
}