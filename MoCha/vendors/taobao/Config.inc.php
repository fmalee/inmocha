<?php
/*
警告: 修改该文件必须保存为无ROM头的文件,也就是去掉文件头签名
如果使用记事本改的话可能会出现获取数据乱码的问题
*/

//设置获取数据的编码. 支持UTF-8 GBK GB2312 
//需要 iconv或mb_convert_encoding 函数支持
//UTF-8 不可写成UTF8
$apiConfig['Charset'] = 'UTF-8';

//您的appKey和appSecret 支持多个appKey
$apiConfig['AppKey'] = array(
	'12395060'=>'b33bee16fd5622daf8aacc7000f4f1f6',
	'21026410'=>'a867837bce8df4bc6d4e6a11723bda10'
);

//当appKey不只一个时,API次数超限后自动启用下一个APPKEY
//false:关闭 true:开启
$apiConfig['AppKeyAuto'] = false;

//设置sign加密方式,支持 md5 和 hmac 
//版本2.0时才可以使用 hmac
$apiConfig['SignMode'] = 'md5';

//设置响应格式,目前支持格式为xml,json
$apiConfig['Format'] = 'json';

//授权入口
$apiConfig['Container'] = 'http://container.api.taobao.com/container';

//TQL入口
$apiConfig['TqlUrl'] = 'http://gw.api.taobao.com/tql/2.0/json';

//RSET入口
$apiConfig['RestUrl'] = 'http://gw.api.taobao.com/router/rest';

//设置RSET的版本
$apiConfig['Version'] = '2.0';

//显示或关闭错语提示,
//true:关闭 false:开启
$apiConfig['CloseError'] = false;

//开启或关闭API调用日志功能,开启后可以查看到每天APPKEY调用的次数以及调用的API
//false:关闭 true:开启
$apiConfig['ApiLog'] = false;

//开启或关闭错误日志功能
//false:关闭 true:开启
$apiConfig['Errorlog'] = true;

//设置API读取失败时重试的次数,可以提高API的稳定性,默认为3次
$apiConfig['RestNumberic'] = 0;

//每次调用API后自动清除原有传入参数
//false:关闭 true:开启
$apiConfig['AutoRestParam'] = false;

return $apiConfig;