<?php
/*
* 处理回调参数$top_parameters
* 将其转换成数组，并可用get型函数各参数的值
*/
/**
* Description of Parameter
*
* @author 张永军
*/
class Parameter {
	private $parametersArray;//top_parameters解析后存在此数组中
	
	public function __construct($top_parameters) {  
			//$paramArray[]=new ArrayObject;
			$param = explode('&', base64_decode($top_parameters));
			foreach ($param as $key => $value) {
					$s=explode('=', $value);
					$paramArray[$s[0]]=$s[1];
			}
			$this->parametersArray= $paramArray;
	}
	
	public function getParamArray()
	{
			return $this->parametersArray;
	}
	
	public function getVisitorId()
	{
			return $this->parametersArray['visitor_id'];
	}
	
	public function getVisitorNick()
	{
			return $this->parametersArray['visitor_nick'];
	}
	
	 public function getVisitorRole()
	{
			return $this->parametersArray['visitor_role'];
	}  
}
?>
<!--
<php 
$top_appkey = $_GET['top_appkey']; 
$top_parameters = $_GET['top_parameters']; 
$top_session = $_GET['top_session']; 
$top_sign = $_GET['top_sign']; 

$secret = 'xxxxxxx'; // 别忘了改成你自己的 

$md5 = md5( $top_appkey . $top_parameters . $top_session . $secret, true ); 
$sign = base64_encode( $md5 ); 

if ( $sign != $top_sign ) { 
echo "signature invalid."; 
exit(); 
} 

$parameters = array(); 
parse_str( base64_decode( $top_parameters ), $parameters ); 

$now = time(); 
$ts = $parameters['ts'] / 1000; 
if ( $ts > ( $now + 60 * 10 ) || $now > ( $ts + 60 * 30 ) ) { 
echo "request out of date."; 
exit(); 
} 

echo "welcome {$parameters['visitor_nick']}."; 
> 
-->