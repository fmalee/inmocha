<?php
/**
 * 淘宝API处理类-商品
 */
class TaoItem extends Taoapi
{

    public function __construct() {
        parent::__construct();
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * 同步淘宝和本地宝贝数据
     * @param $num_iid 传入商品编号
     * @param $fields 需要获取的字段
     * 全部字段：approve_status,auction_point,cid,created,delist_time,desc,ems_fee,express_fee,freight_payer,has_discount,has_invoice,has_showcase,has_warranty,increment,input_pids,input_str,is_3D,is_ex,is_lightning_consignment,is_taobao,is_timing,is_virtual,is_xinpin,item_img,list_time,modified,nick,num,one_station,outer_id,pic_url,post_fee,postage_id,price,product_id,property_alias,props,props_name,sell_promise,seller_cids,sku,stuff_status,sub_stock,template_id,title,type,valid_thru,violation,wap_desc,wap_detail_url,location,is_fenxiao
     */
    public function synItem($num_iid, $fields = "") {
        if (!$fields) {
            $this->fields = "num_iid,approve_status,auction_point,cid,created,delist_time,ems_fee,express_fee,freight_payer,has_discount,has_invoice,has_showcase,has_warranty,increment,input_pids,input_str,is_3D,is_ex,is_lightning_consignment,is_taobao,is_timing,is_virtual,is_xinpin,list_time,modified,nick,num,one_station,outer_id,pic_url,post_fee,postage_id,price,product_id,property_alias,props,sell_promise,seller_cids,sku,stuff_status,sub_stock,template_id,title,type,valid_thru,violation,item_img,prop_img,desc";
        } else {
            $this->fields = $fields . ",num_iid,modified";
        }
        $this->method = 'item';
        $this->session = $this->_session;
        $this->where = array('num_iid'=>$num_iid);
        $resp = $this->select()->get();
        $item = $resp['item_get_response']['item'];

        
        return $this->format($item);
    }

    /**
     * 同步淘宝和本地宝贝数据
     * @param sting $num_iids 传入商品编号列表
     * @param $update 是否更新数据表 0 不更新 1 item表 2 goods表
     * @param $fields 需要获取的字段
     * 全部字段：approve_status,auction_point,cid,created,delist_time,desc,ems_fee,express_fee,freight_payer,has_discount,has_invoice,has_showcase,has_warranty,increment,input_pids,input_str,is_3D,is_ex,is_lightning_consignment,is_taobao,is_timing,is_virtual,is_xinpin,item_img,list_time,modified,nick,num,one_station,outer_id,pic_url,post_fee,postage_id,price,product_id,property_alias,props,props_name,sell_promise,seller_cids,sku,stuff_status,sub_stock,template_id,title,type,valid_thru,violation,wap_desc,wap_detail_url,location,is_fenxiao
     */
    public function synItems($num_iids, $update = 1, $fields = "") {
        $this->method = 'items.list';
        $this->session = $this->setSession();
        $this->where = array('num_iids'=>$num_iids);
        if (!$fields) {
            $this->fields = "num_iid,approve_status,auction_point,cid,created,delist_time,ems_fee,express_fee,freight_payer,has_discount,has_invoice,has_showcase,has_warranty,increment,input_pids,input_str,is_3D,is_ex,is_lightning_consignment,is_taobao,is_timing,is_virtual,is_xinpin,list_time,modified,nick,num,one_station,outer_id,pic_url,post_fee,postage_id,price,product_id,property_alias,props,sell_promise,seller_cids,sku,stuff_status,sub_stock,template_id,title,type,valid_thru,violation";
        } else {
            $this->fields = $fields . ",num_iid,modified";
        }

        $resp = $this->select()->get();
        $items = $resp['items_list_get_response']['items']['item'];
        
        if ($update > 0) {
            foreach ($items as $item) {
                $this->saveItem($item, $update); //入库
            }
        }
    
        return $items;
    }
    /**
     * 同步出售中的宝贝
     * @param $where 筛选条件 q,cid,seller_cids,page_no,has_discount,has_showcase,order_by,is_taobao,is_ex,page_size,start_modified,end_modified
     * @param $fields 需要获取的字段
     * @param $batch 嵌套查询的字段
     */
    public function synOnsale($where, $fields = "", $batch = "") {
        $this->method = 'items.onsale';
        $this->session = $this->setSession();
        if ($batch) {
            $this->fields = 'num_iid';
        } elseif (!$fields) {
            $this->fields = "num_iid,title,nick,type,cid,seller_cids,props,pic_url,num,valid_thru,list_time,delist_time,price,has_discount,has_invoice,has_warranty,has_showcase,modified,approve_status,postage_id,outer_id,is_virtual,is_taobao,is_ex";
        } else {
            $this->fields = $fields;
        }
        $this->where = $where;
        $this->select();

        if ($batch) {
            $this->method = 'item';
            $this->fields = $batch;
            $this->select(array('num_iid'=>'__SUBSELECT__'));
            $this->sub();
        }

        $result = $this->get();

        if ($batch) {
            foreach ($result as $item) {
                $item = $result['item_get_response']['item'];
                $this->saveItem($item, $update); //入库
            }
            return true;
        } else {
            $item = $result['items_onsale_get_response']['items']['item'];
            $total = $result['items_onsale_get_response']['total_results'];
            $this->saveItem($item, $update); //入库

            return $total;
        }
    }
    
    /**
     * 保存商品信息到表
     * @param array $item 传入商品数据
     * @param $update 是否更新数据表  1：item表  2：goods表
     */
    public function saveItem($item, $update = 1) {
        $num_iid = $item['num_iid'];
        
        if ($update == 1) { //Item表
            if ($item['skus']) {
                D('Sku')->add_skus($num_iid, $item); //更新SKUS表
            }
            $Model = $this;
            $has = $this->getFieldByNum_iid($item['num_iid'], 'num_iid'); //是否存在记录
        } else { //Goods表
            $Model = M('Goods');
            unset($item['num_iid']);
            $item['id'] = $num_iid;
            $has = $Model->getFieldById($item['id'], 'id'); //是否存在记录
        }
        $item = $this->format($item);  //内容转换
        //$item['discount'] = $this->promotionPrice($num_iid);  //接口调用次数受限制
        if (!$has) {
            $Model->add($item);
        } else {
            $Model->save($item);
        }
    }

    /**
     * 搜索商品信息
     * @param $where 商品搜索条件
     * 
     * @param $fields 需返回的字段
     * @param $sub 嵌套查询的字段
     * 字段：num_iid,title,nick,pic_url,cid,price,type,delist_time,location,post_fee,score,volume,detail_url,second_kill
     * where参数：q,nicks,cid,props,product_id,ww_status,post_free,location.state,location.city,is_3D,start_score,end_score,start_volume,end_volume,one_station,is_cod,is_mall,is_prepay,genuine_security,stuff_status,start_price,end_price,promoted_service,is_xinpin
     * nicks不能超过5个，$page_size最大200，结果不超过1024个(page_no*page_size)
     */
    public function searchItems($where, $fields = "", $sub = "") {
        if (!is_array($where)) return false;
        $this->method = 'items';
        $this->session = $this->setSession();

        if ($where['nicks']) {
            $this->nicks = $where['nicks'];
            $where['nicks'] = "#nicks#";
        }
        if (!isset($where['page_no'])) $where['page_no'] = 1;
        if (!isset($where['page_size'])) $where['page_size'] =40;
        if (!isset($where['order_by'])) $where['order_by'] = "popularity:desc";
        $this->where = $where;

        if ($sub) {
            $this->fields = 'num_iid';
        } elseif (!$fields) {
            $this->fields = "num_iid,title,nick,pic_url,cid,price,type,volume";
        } else {
            $this->fields = $fields;
        }

        $this->select();

        if ($sub) {
            $this->method = 'item';
            $this->fields = $sub;
            $this->select(array('num_iid'=>'__SUBSELECT__'));
            $this->sub();
        }

        $result = $this->get();

/*      if ($sub) {
            foreach ($result as $item) {
                $item = $result['item_get_response']['item'];
                $this->saveItem($item, 2); //入库
            }
            return true;
        } else {
            $items = $result['items_onsale_get_response']['items']['item'];
            $total = $result['items_onsale_get_response']['total_results'];
            foreach ($items as $item) {
                $this->saveItem($item, 2); //入库
            }

            return $total;
        }*/
        return $result;
    }

    /**
     * 更新商品信息
     * @param $num_iid 传入商品编号
     */
    public function updateItem($num_iid, $item) {
        $this->checkItem($item);  //商品信息检查
        
        $this->method = 'item';
        $this->session = $this->setSession();
        $this->where = array('num_iid'=>$num_iid);
        $resp = $this->update()->get();
        $item = $resp['item_update_response']['item'];

        return $item ? true : false;
    }
    
    /**
     * 商品信息检查
     * @param array $item 商品数据
     */
    public function checkItem($item) {
        foreach ($item as $k=>$v)  //判断键和键值非空
            if (is_null($v)) unset($item[$k]);

        if(isset($item['desc']))
        {
            $this->desc = $item['desc'];
            $item['desc'] = "#desc#";
        }
        if(isset($item['input_pids']))
        {
            $this->input_pids = $item['input_pids'];
            $item['input_pids'] = "#input_pids#";
        }
        if(isset($item['input_str']))
        {
            $this->input_str = $item['input_str'];
            $item['input_str'] = "#input_str#";
        }
        if(isset($item['outer_id']))
        {
            $item['outer_id'] = str_replace(',', '，', $item['outer_id']);
        }
        if(isset($item['sku_outer_ids']))
        {
            $this->sku_outer_ids = $item['sku_outer_ids'];
            $item['sku_outer_ids'] = "#sku_outer_ids#";
        }
        if(isset($item['sku_prices']))
        {
            $this->sku_prices = $item['sku_prices'];
            $item['sku_prices'] = "#sku_prices#";
        }
        if(isset($item['sku_quantities']))
        {
            $this->sku_quantities = $item['sku_quantities'];
            $item['sku_quantities'] = "#sku_quantities#";
        }
        if(isset($item['sku_properties']))
        {
            $this->sku_properties = $item['sku_properties'];
            $item['sku_properties'] = "#sku_properties#";
        }
        if(isset($item['image']))
        {
            $this->image = '@'.$item['image'];
            $item['image'] = "#image#";
        }
        $this->data = $item;
    }

    /**
     * 上传商品
     * @param $item 商品数据
     */
    public function addItem($item) {
        $item['title'] = str_replace(",", "", $item['title']); //替换','
        
        //$image = remote_pic($item['pic_url']); //下载远程图片
        $replaceDesc = 1; //是否替换图片
        $desc = '系统正在搬家中，宝贝详情一会儿呈现，请稍候……';
        
        if ($item['tmall']) {
            $product_id = $this->addProduct($item);  //上传产品
            if (!$product_id) return false;
            $upload['auction_point'] = $item['auction_point'];
            $upload['product_id'] = $product_id;
            //$upload['product_id'] = 202636380;
        }
        $upload['input_pids'] = '#input_pids#';
        $upload['input_str'] = "#input_str#";
        $upload['sku_outer_ids'] = "#sku_outer_ids#";
        $upload['sku_prices'] = "#sku_prices#";
        $upload['sku_quantities'] = "#sku_quantities#";
        $upload['sku_properties'] = "#sku_properties#";
        $upload['image'] = "#image#";
        $upload['desc'] = "#desc#";
        if ($item['property_alias']) $upload['property_alias'] = "#property_alias#";
        
        $upload['props'] = $item['props'];
        $upload['outer_id'] = str_replace(',', "，", $item['outer_id']);
        $upload['approve_status'] = $item['approve_status'];
        //$upload['list_time'] = $item['list_time'];  //定时上架
        $upload['postage_id'] = $item['postage_id'];
        if ($item['seller_cids']) $upload['seller_cids'] = $item['seller_cids'];
        $upload['sell_promise'] = $item['sell_promise'];
        $upload['sub_stock'] = $item['sub_stock'];
        //$upload['valid_thru'] = $item['valid_thru'];
        $upload['cid'] = $item['cid'];
        $upload['title'] = $item['title'];
        $upload['price'] = $item['price'];
        $upload['num'] = $item['num'];
        $upload['type'] = $item['type'];
        $upload['stuff_status'] = $item['stuff_status'];
        $upload['freight_payer'] = $item['freight_payer'];
        $upload['has_discount'] = $item['has_discount'];
        $upload['has_showcase'] = $item['has_showcase'];
        $upload['has_warranty'] = $item['has_warranty'];
        $upload['has_invoice'] = $item['has_invoice'];
        $upload['is_3D'] = $item['is_3D'];
        $upload['is_ex'] = $item['is_ex'];
        $upload['is_lightning_consignment'] = $item['is_lightning_consignment'];
        $upload['is_taobao'] = $item['is_taobao'];
        $upload['is_xinpin'] = $item['is_xinpin'];
        $upload['lang'] = "zh_CN";
        $upload['location.state'] = "浙江";
        $upload['location.city'] = "杭州";
        //$upload['increment'] = "0.00";
        //$upload['auto_fill'] = $item['props'];
        //$upload['ems_fee'] = $item['ems_fee'];
        //$upload['express_fee'] = $item['express_fee'];
        //$upload['post_fee'] = $item['post_fee'];
        //$upload['weight'] = $item['weight'];
        //$upload['after_sale_id'] = $item['after_sale_id'];
        //$upload['cod_postage_id'] = $item['cod_postage_id'];
        //$upload['pic_path'] = $item['pic_path'];
        
        $this->input_pids = $item['input_pids'];
        $this->input_str = $item['input_str'];
        $this->sku_outer_ids = $item['sku_outer_ids'];
        $this->sku_prices = $item['sku_prices'];
        $this->sku_quantities = $item['sku_quantities'];
        $this->sku_properties = $item['sku_properties'];
        //$this->image = "@$image";
        $this->desc = $replaceDesc ? $desc : $item['desc'];
        if ($item['property_alias']) $this->property_alias = $item['property_alias'];

        $this->method = 'item';
        $this->session = $this->setSession();
        $items = $this->insert($upload)->get();

        if (isset($items['error_response'])) {
            $msg = $items['error_response']['sub_msg'] ? $items['error_response']['sub_msg'] : $items['error_response']['msg'];
            return $msg;
        }

        $num_iid = $items['item_add_response']['item']['num_iid'];  

        if($num_iid) {
            foreach ($item['item_imgs'] as $img) { //上传商品图片
                $is_major = $img['id'] ? 'false' : 'true';

                $image = $this->uploadPic($img['url'], $num_iid, 0, 'png'); //上传到图片空间
                $this->jointImg($num_iid, $image['picture_path'], $is_major, $img['position']); //关联主图
                if ($is_major == 'true') $this->deletePics($image['picture_id']); //删除图片空间上的主图
            }

            if ($replaceDesc) { //替换详情图片
                $data['num_iid'] = $num_iid;
                $data['desc'] = $item['desc'];
                $data['nick'] = NICK;
                httpPost(C('SITE_DOMAIN') . U('Seller/Goods/public_desc'), $data);
            }
        }
        
        return $num_iid;
    }

    /**
     * 更新商品SKU信息
     * @param string $num_iid 传入商品编号
     * @param string $properties 传入Sku属性串
     * @param array $skus SKU信息
     * 参数：quantity, price, outer_id, item_price, lang
     */
    public function updateSku($num_iid, $properties, $sku) {
        $this->method = 'item.sku';
        $this->session = $this->setSession();
        $this->where = array('num_iid'=>$num_iid, 'properties'=>$properties);       
        $resp = $this->update($sku)->get();
        $item = $resp['item_sku_update_response']['sku'];
        
        return $item ? true : false;
    }
    
    /**
     * 橱窗推荐一个商品
     * @param $num_iid 传入商品编号
     */
    public function recommend($num_iid, $mode = 'insert') {
        $this->method = 'item.recommend';
        $this->session = $this->setSession();
        $result = $this->$mode(array('num_iid'=>$num_iid))->get();
        
        return isset($result['error_response']) ? false : true;
    }
        
    /**
     * 一口价商品上架
     * @param $num_iid 传入商品编号
     * @param $num 需要上架的商品的数量
     */
    public function listingItem($num_iid, $num) {
        $this->method = 'taobao.item.update.listing';
        $this->session = $this->setSession();
        $this->where = array('num_iid'=>$num_iid);
        $result = $this->update(array('num'=>$num))->get();
        
        return isset($result['error_response']) ? false : true;
    }
    
    /**
     * 商品下架
     * @param $num_iid 传入商品编号
     */
    public function delistingItem($num_iid) {
        $this->method = 'taobao.item.update.delisting';
        $this->session = $this->setSession();
        $this->where = array('num_iid'=>$num_iid);
        $result = $this->update(array('status'=>1))->get();

        return isset($result['error_response']) ? false : true;
    }
    
    /**
     * 删除商品
     * @param $num_iid 传入商品编号
     */
    public function deleteItem($num_iid) {
        $this->method = 'item';
        $this->session = $this->setSession();
        $result = $this->delete(array('num_iid'=>$num_iid))->get();

        return isset($result['error_response']) ? false : true;
    }
    
    /**
     * 上传产品（商城）
     * @param $item 商品数据
     */
    public function addProduct($item) { //TODO
        $props = D('Props')->get_props($item, 3); // 获取非关键属性
        foreach ($props as $prop ) {
            foreach ($prop as $k=>$v) $bind[] = $k;
        }
        $binds = implode(";", $bind);
        
        $fields = array();
        $fields['name'] = $item['title'];
        $fields['cid'] = $item['cid'];
        $fields['price'] = $item['price'];
        $fields['binds'] = $binds;
        $fields['customer_props'] = "20000:".$item['brand'].";1632501:".$item['outer_id'];
        //$fields['props'] = $props;
        $fields['major'] = 'true';
        $fields['image'] = "#image#";
        
        $field = implode(",", array_keys($fields));  //拼装字段列表
        $value = implode(",", array_values($fields)); //拼装值列表
        
        $params['ql'] = "insert into product ($field) values ($value)";
        $params['image'] = $item['image'];
        $product = $this->top->tql($params, $this->setSession());
        $product_id = $product['product']['product_id'];
        
        return $product_id;
    }
    
    /**
     * 上传商品主图
     * @param $num_iid 图片ID
     * @param $image 图片地址
     * @param $is_major 设置为默认主图
     * @param $position 图片序号 
     * @param $id 商品图片id，(如果是更新图片，则需要传该参数) 
     * @param $code 需转换的图片格式
     */
    public function uploadImg($num_iid, $image, $is_major = 'false', $position = '', $id = '', $code = '') {
        $this->method = 'item.img';
        $this->session = $this->setSession();

        $data['num_iid'] = $num_iid;
        $data['is_major'] = $is_major;
        if (!empty($position)) $data['position'] = $position;
        if (!empty($id)) $data['id'] = $id;

        if (strstr($image, 'http://')) {
            $image = remote_pic($image, $code); //下载图片
        }
        $this->image = "@$image";
        $data['image'] = '#image#';
        
        $this->insert($data);
        $resp = $this->get();
        $image = $resp;

        return $image;
    }

    /**
     * 商品关联子图
     * @param $num_iid 图片ID
     * @param $image 图片地址，必须在自己的图片空间
     * @param $is_major 设置为默认主图
     * @param $position 图片序号 
     * @param $id 商品图片id，(如果是更新图片，则需要传该参数) 
     */
    public function jointImg($num_iid, $image, $is_major = 'false', $position = '', $id = '') {
        $this->method = 'taobao.item.joint.img';
        $this->session = $this->setSession();
        
        $data = array();
        $data['num_iid'] = $num_iid;
        $data['is_major'] = $is_major;
        if (!empty($position)) $data['position'] = $position;
        if (!empty($id)) $data['id'] = $id;

        if (strstr($image, '/imgextra/')) {
            $arr = explode('/imgextra/', $image);
            $image = $arr[1];
        }
        $data['pic_path'] = $image;
        
        $this->insert($data);
        $resp = $this->get();
        $picture = $resp['item_joint_img_response']['item_img'];

        return $picture;
    }

    /**
     * 商品关联子图
     * @param $num_iid 图片ID
     * @param $image 图片地址，必须在自己的图片空间
     * @param $is_major 设置为默认主图
     * @param $position 图片序号 
     * @param $id 商品图片id，(如果是更新图片，则需要传该参数) 
     */
    public function replaceDesc($num_iid, $desc, $code = '') {
        defined('USERID') or define('USERID', D('User')->nick2userid($this->setNick())); //定义USERID
        $reg = "/<img[^>]*src=\"(http:\/\/(.+)\/(.+)\.(jpg|gif|bmp|bnp))\"/isU"; //正则
        preg_match_all($reg, $desc, $img_array, PREG_PATTERN_ORDER); //把img地址存放到$img_array
        $img_array = array_unique($img_array[1]); //过滤重复的图片

        foreach ($img_array as $img){
            if (!strstr($img, '/'. USERID . '/')) { //排除自身空间图片
                $pic = $this->uploadPic($img, $num_iid); //上传图片到图片空间

                $desc = str_replace($img, $pic['picture_path'], $desc); //文本路径替换
            }
        }
        $update = array('desc'=>$desc);

        $resp = $this->updateItem($num_iid, $update);

        return $resp;
    }

    /**
     * 上传图片到图片空间
     * @param $image 图片地址
     * @param $title 图片标题
     * @param $cid 图片分类ID
     * @param $code 图片转换格式
     */
    public function uploadPic($image, $title, $cid = '0', $code = '') {
        $data = array();
        $this->method = 'picture';
        $this->session = $this->setSession();
        if (strstr($image, 'http://')) {
            $image = remote_pic($image, $code); //下载图片
        }
        $this->image = "@$image";
        $data['image'] = '#image#';
        $data['title'] = $title;
        $data['image_input_title'] = trim(strrchr($image, '/'), '/');
        $data['picture_category_id'] = $cid;
        $this->insert($data);
        $resp = $this->get();
        $picture = $resp['picture_upload_response']['picture'];

        return $picture;
    }

    /**
     * 替换图片空间的图片
     * @param $picture_id 图片地址
     * @param $image 图片地址
     * @param $code 图片转换格式
     */
    public function replacePic($picture_id, $image, $code = '') {
        $data = array();
        $this->method = 'taobao.picture.replace';
        $this->session = $this->setSession();
        if (strstr($image, 'http://')) {
            $image = remote_pic($image, $code); //下载图片
        }
        $this->image = "@$image";
        $data['image_data'] = '#image#';
        $this->where = array('picture_id'=>$picture_id);
        $this->update($data);
        $resp = $this->get();
        $picture = $resp['picture_upload_response']['picture'];

        return $picture;
    }

    /**
     * 修改图片名字
     * @param $picture_id 要更改名字的图片的id 
     * @param $title 新的图片名，最大长度50字符，不能为空 
     * @param $nick 用户
     */
    public function updateTitle($picture_id, $title) {
        $this->session = $this->setSession();
        $this->method = 'taobao.picture.update';
        $this->where = array('picture_id'=>$picture_id);
        $this->update(array('new_name'=>$title));
        $resp = $this->get();
        $info = $resp['picture_update_response']['done'];

        return $info;
    }

    /**
     * 删除图片空间图片
     * @param $picture_ids 要删除的图片ID字符串
     */
    public function deletePics($picture_ids) {
        $this->session = $this->setSession();
        $this->method = 'picture';

        $whereStr = '';
        if (is_object($picture_ids)) {
            $picture_ids = get_object_vars($picture_ids);
        }
        if (is_string($picture_ids)) {
            $whereStr = $picture_ids;
        }elseif (is_array($picture_ids)) { // 使用数组表达式
            $whereStr = implode(',', $picture_ids);
        }
        $where = array('picture_ids'=>$whereStr);

        $this->delete($where);
        $resp = $this->get();
        $info = $resp['picture_delete_response']['success'];

        return $info;
    }

    /**
     * 获取图片信息
     * @param array $where 搜索条件
     * picture_id,picture_category_id,deleted,title,order_by,start_date,end_date,page_no,page_size,modified_time
     * page_size <=100, order_by:time|size:desc|asc
     * @param $nick 用户昵称
     */
    public function getPics($where) {
        $this->session = $this->setSession();
        $this->method = 'picture';
        $this->fields = 'picture_id';
        $this->where = $where;

        $this->select();
        $resp = $this->get();
        $pictures = $resp['picture_get_response'];

        return $pictures;
    }

    /**
     * 获取图片的引用详情
     * @param $picture_id 要查询的图片
     * @param $nick 用户
     */
    public function getReferenced($picture_id) {
        $this->session = $this->setSession();
        $this->method = 'taobao.picture.referenced.get';
        $this->fields = 'picture_id';
        $this->where = array('picture_id'=>$picture_id);
        $resp = $this->select()->get();
        $referenced = $resp['picture_referenced_get_response']['references'];

        return $referenced;
    }

    /**
     * 图片是否被引用
     * @param $picture_id 要查询的图片
     * @param $nick 用户
     */
    public function isReferenced($picture_id) {
        $this->session = $this->setSession();
        $this->method = 'taobao.picture.isreferenced.get';
        $this->fields = 'picture_id';
        $this->where = array('picture_id'=>$picture_id);
        $resp = $this->select()->get();
        $isreferenced = $resp['picture_isreferenced_get_response']['is_referenced'];

        return $isreferenced;
    }

    /**
     * 查询图片空间用户的信息
     * @param $nick 用户
     */
    public function getInfo() {
        $this->session = $this->setSession();
        $this->method = 'taobao.picture.userinfo.get';
        $this->fields = 'used_space';
        $resp = $this->select()->get();
        $info = $resp['picture_userinfo_get_response']['user_info'];

        return $info;
    }
    
    /**
     * 检查porops，补充必要属性值
     * @param $item 商品数据
     */
    public function checkProps($item) {
        $cid = $item['cid'];
        
        $skus = array();
        $props_arr = explode(";", $item['props']);  //0=>pid:vid
        $skus_arr = explode(",", $item['sku_properties']);  //0=>pid:vid;pid;vid
        foreach ($skus_arr as $sku) {
            $sku_arr = explode(";", $sku);  //0=>pid:vid
            $skus = array_merge(array_flip($sku_arr), $skus); //pid:vid=>0
        }
        $props_arr = array_merge(array_flip($props_arr), $skus);  //将SKUS里的PROPS补充进来 pid:vid=>0
        
        foreach ($props_arr as $prop=>$v) {
            $prop_arr = explode(":", $prop);  //0=>pid 1=>vid
            $pid = $prop_arr[0];
            $vid = $prop_arr[1];
            if (!$pid) continue;
            $props[$pid] = $vid;   //组装成pid=>pv的形式
            
            $info = M('Props')->where(array('cid'=>$cid, 'pid'=>$pid, 'is_key_prop'=>0))->field('is_sale_prop,prop_values')->find();
            if (!$info) {  //pid不存在
                unset($props_arr[$prop_arr]); //删除不存在的props
                continue;
            }
            $prop_values = unserialize($info['prop_values']);
            if (!array_key_exists($vid, $prop_values)) { //vid不存在
                $values[$pid] = $prop_values;  //失效的vid的prop_values集合
                $none[$vid] = $pid;   //失效的vid集合
            } else {
                unset($values[$pid][$vid]);  //去除已经用掉的属性值
            }
        }
        
        $infos = M('Props')->where(array('cid'=>$cid, 'must'=>1))->field('pid,prop_values')->select();  //添加缺失的必要属性
        foreach ($infos as $info) {
            $pid = $info['pid'];
            if (!$props[$pid]) { //必要属性不在props中
                $prop_values = unserialize($info['prop_values']);
                $must = $pid.":".key($prop_values);  //取出缺失的必要属性
                $props_arr[$must] = 1;  //补充缺少的必要属性
            }
        }
        
        foreach ($none as $vid=>$pid) {  //给失效的vid重新赋值
            $new_vid = array_rand($values[$pid]);  //随机获取一个没有和过的属性值
            unset($values[$pid][$new_vid]);  //去除已经用掉的属性
            
            $prop = "$pid:$vid";
            $new_prop = "$pid:$new_vid";
            unset($props_arr[$prop]);  //删除失效的props
            $props_arr[$new_prop] = 1;  //添加新的props
            
            $item['sku_properties'] = str_replace($vid, $new_vid, $item['sku_properties']); //替换sku_properties
            $item['property_alias'] = str_replace($vid, $new_vid, $item['property_alias']); //替换property_alias
        }
        
        $item['props'] = implode(";", array_keys($props_arr));  //组装成新的props
        return $item;
    }
    
    /**
     * 获取商品的营销活动价格
     * @param $num_iid 传入宝贝编号
     */
    public function promotionPrice($num_iid) {
        $this->method = 'ump.promotion';
        $this->fields = 'item_id';
        $this->session = $this->setSession();
        $this->where = array('item_id'=>$num_iid);
        $promotions = $this->select()->get();
        $promotions = $promotions['ump_promotion_get_response']['promotions']['promotion_in_item']['promotion_in_item'];
        $promotions = reset($promotions);
        $price = $promotions['item_promo_price'];

        return $price;
    }
    
    /**
     * 获取用户宝贝详情页模板名称
     */
    public function synTemplates() {
        $this->method = 'item.templates';
        $this->fields = 'template_id';
        $this->session = $this->setSession();
        $resp = $this->select()->get();

        return $resp['item_templates_get_response']['item_template_list']['item_template'];
    }
    
    /**
     * 获取Item信息
     * @param $iid 传入字段值            
     * @param $api 字段接口。0：iid,1:outer_id            
     * @param $need 返回字段            
     */
    public function getItem($iid, $need = 'id,outer_id,title', $api = 0) {
        $field = $api ? 'outer_id' : 'id';
        $item = M('Item')->where(array($field=>$iid))->field($need)->find();
        return $item;
    }

    /**
     * 转换商品数据格式
     * @param $item 传入商品数据
     */
    public function format($item)
    {
        if (isset($item['has_good_return'])) $item['has_good_return'] = Ya::str2bool($item['has_good_return']);
        if (isset($item['has_invoice'])) $item['has_invoice'] = Ya::str2bool($item['has_invoice']);
        if (isset($item['has_showcase'])) $item['has_showcase'] = Ya::str2bool($item['has_showcase']);
        if (isset($item['has_warranty'])) $item['has_warranty'] = Ya::str2bool($item['has_warranty']);
        if (isset($item['is_3D'])) $item['is_3D'] = Ya::str2bool($item['is_3D']);
        if (isset($item['is_ex'])) $item['is_ex'] = Ya::str2bool($item['is_ex']);
        if (isset($item['is_lightning_consignment'])) $item['is_lightning_consignment'] = Ya::str2bool($item['is_lightning_consignment']);
        if (isset($item['is_taobao'])) $item['is_taobao'] = Ya::str2bool($item['is_taobao']);
        if (isset($item['is_timing'])) $item['is_timing'] = Ya::str2bool($item['is_timing']);
        if (isset($item['is_virtual'])) $item['is_virtual'] = Ya::str2bool($item['is_virtual']);
        if (isset($item['is_xinpin'])) $item['is_xinpin'] = Ya::str2bool($item['is_xinpin']);
        if (isset($item['one_station'])) $item['one_station'] = Ya::str2bool($item['one_station']);
        if (isset($item['sell_promise'])) $item['sell_promise'] = Ya::str2bool($item['sell_promise']);
        if (isset($item['violation'])) $item['violation'] = Ya::str2bool($item['violation']);
        
        if ($item['created']) $item['created'] = strtotime($item['created']);
        if ($item['modified']) $item['modified'] = strtotime($item['modified']);
        if ($item['delist_time']) $item['delist_time'] = strtotime($item['delist_time']);
        if ($item['list_time']) $item['list_time'] = strtotime($item['list_time']);
        
        return $item;
    }
}