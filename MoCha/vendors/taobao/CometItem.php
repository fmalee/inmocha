<?php
set_time_limit(0);
class CometItem {
	private $Item, $Sku, $items, $nick, $num_iid, $modified, $update, $where;
	
	/**
	 * 架构函数
	 * @access public
	 * @param array $items  宝贝数据
	 */
	public function __construct($items) {
		$this->Item = D('Item');
		$this->Sku = D('Sku');
	}
	
	/**
	 * 传入数据
	 * @access public
	 * @param array $items  宝贝数据
	 */
	public function Import($items) {
		//Log::write(json_encode($items), Log::ERR);
		if (!is_array($items)) return false;
		
		$this->items = $this->format($items);
		$status = $this->items['status'];
		$this->nick = $this->items['nick'];
		$this->num_iid = $this->items['num_iid'];
		$this->modified = $this->items['modified'];
	
		$this->update = array('modified' => $this->modified);  //更新数据
		$this->where = array('num_iid' => $this->num_iid);  //更新条件
		
		$this->$status();  //调用执行接口
	}
	
	/**
	 * 商品新增
	 */
	private function ItemAdd() {
		return $this->Item->synItem($this->num_iid);
	}
	
	/**
	 * 商品上架
	 */
	private function ItemUpshelf() {
		if ($this->syn("approve_status")) {
			$this->update['approve_status'] = 'onsale';
			$this->Item->where($this->where)->save($this->update);
		}
		return true;
	}
	
	/**
	 * 商品下架
	 */
	private function ItemDownshelf() {
		if ($this->syn("approve_status")) {
			$this->update['approve_status'] = 'instock';
			$this->Item->where($this->where)->save($this->update);
		}
		return true;
	}
	
	/**
	 * 商品删除
	 */
	private function ItemDelete() {
		$this->update['approve_status'] = 'recycle';
		$this->Item->where($this->where)->save($this->update);
		return true;
	}
	
	/**
	 * 商品更新
	 */
	private function ItemUpdate() {
		//TODO $this->changed_filds有sku才更新sku表
		return $this->Item->synItem($this->num_iid);
	}
	
	/**
	 * 取消橱窗推荐
	 */
	private function ItemRecommendDelete() {
		if ($this->syn("has_showcase")) {
			$this->update['has_showcase'] = 0;
			$this->Item->where($this->where)->save($this->update);
		}
		return true;
	}
	
	/**
	 * 橱窗推荐
	 */
	private function ItemRecommendAdd() {
		if ($this->syn("has_showcase")) {
			$this->update['has_showcase'] = 1;
			$this->Item->where($this->where)->save($this->update);
		}
		return true;
	}
	
	/**
	 * 小二删除商品
	 */
	private function ItemPunishDelete() {
		$this->update['approve_status'] = 'recycle';
		$this->Item->where($this->where)->save($this->update);
		//TODO 小二删除商品
		Log::write("小二删除商品:".$this->num_iid, Log::ERR);
		return true;
	}
	
	/**
	 * 小二下架商品
	 */
	private function ItemPunishDownshelf() {
		if ($this->syn("approve_status,violation")) {
			$this->update['approve_status'] = 'instock';
			$this->update['violation'] = 1;
			$this->Item->where($this->where)->save($this->update);
		}
		//TODO 小二下架商品
		Log::write("小二下架商品:".$this->num_iid, Log::ERR);
		return true;
	}
	
	/**
	 * 小二cc商品
	 */
	private function ItemPunishCc() {
		//TODO 小二cc商品
		$this->Item->synItem($this->num_iid);
		Log::write("小二cc商品:".$this->num_iid, Log::ERR);
		return true;
	}
	
	/**
	 * 商品卖空
	 */
	private function ItemZeroStock() {
		if ($this->syn("approve_status,num")) {
			$this->update['num'] = 0;
			$this->Item->where($this->where)->save($this->update);
		}
		//TODO 商品卖空,会不会下架
		Log::write("商品卖空:".$this->num_iid, Log::ERR);
		return true;
	}
	
	/**
	 * 商品sku卖空
	 */
	private function ItemSkuZeroStock() {
		if ($this->syn("num,sku")) {
			$this->update['quantity'] = 0;
			$this->where = array('sku_id'=>$this->items['sku_id'], 'num_iid'=>$this->num_iid);
			$this->Sku->where($this->where)->save($this->update);
		}
		//TODO 商品sku卖空
		Log::write("商品sku卖空:".$this->num_iid, Log::ERR);
		return true;
	}
	
	/**
	 * 更新商品库存
	 */
	private function ItemStockChanged() {
		if ($this->syn("num,sku")) {
			$update = $this->update;
			$this->update['num'] = $this->items['num'];
			$this->Item->where($this->where)->save($this->update);
			
			$update['quantity'] = $this->items['sku_num'];
			$this->where = array('sku_id'=>$this->items['sku_id'], 'num_iid'=>$this->num_iid);
			$this->Sku->where($this->where)->save($this->update);
		}
		return true;
	}
	
	/**
	 * 比较淘宝和本地商品最后更新时间
	 */
	private function syn($fields) {
		if (!$this->loc_time()) {  //记录在数据库中不存在
			$this->Item->synItem($this->num_iid);
			return false;
		} else if ($this->modified < $this->loc_time()) {  //新的记录变更时间比数据库的早
			$this->Item->synItem($this->num_iid, 1, $fields);
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * 获取本地商品最后更新时间
	 */
	private function loc_time() {
		$modified = $this->Item->where($this->where)->getField('modified');
		if (!$modified) return false;
		return $modified;
	}
	
	/**
	 * 数据检查
	 * @param $items 商品数据
	 */
	private function format($items) {
		//if ($items['num_iid']) $items['num_iid'] = number_format($items['num_iid'],0,',','');
		if ($items['modified']) $items['modified'] = strtotime($items['modified']);
		return $items;
	}
}
?>