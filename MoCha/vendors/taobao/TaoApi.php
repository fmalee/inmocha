<?php

/**
 * 淘宝API处理类
 *
 * @category Taoapi
 * @package Taoapi
 * @copyright Copyright (c) 2008-2009 Taoapi (http://www.Taoapi.com)
 * @license    http://www.Taoapi.com
 * @version    Id: Taoapi  2009-12-22  12:30:51 旺旺:浪子Arvin QQ:8769852
 */
abstract class Taoapi {
    //扩展类实例
    private static $_models=array();
	// 获取的数据
	protected $taobaoData;
    // 用户参数
    private $_userParam = array();
    // 系统参数
    private $_systemParam = array();
    // TQL串行化批量操作
    protected $_batchParam = array();
    // CURL方法
    protected $_modeParam = 'GET';
    // 错误信息
    private $_errorInfo;
    // 配置参数
    public $ApiConfig;
    // 配置类
    public $Config;
    // 串行化标准符号
    public $_seperator = false;
    
	private $_ArrayModeData;

    //初始当前用户SESSION
    public $_session;
    //初始当前用户NICK
    public $_nick;

    public function __construct() {
		$this->Config = TaoConfig::Init();
		$this->ApiConfig = $this->Config->getConfig();

        if (isset(Yii::app()->user->tbkey))
        {
            $this->_session = Yii::app()->user->tbkey;
            $this->_nick = Yii::app()->user->tbnick;
        }
    }
    
    public function __set($name, $value) {
        if ($this->taobaoData && $this->ApiConfig->AutoRestParam) {
            $this->_userParam = array();
			$this->taobaoData = null;
        }
        $this->_userParam[$name] = $value;
    }

    public function __get($name)
    {
        return isset($this->_userParam[$name]) ? $this->_userParam[$name] : null;
    }
    public function __unset($name)
    {
        unset($this->_userParam[$name]);
    }

    public function __isset($name)
    {
        return isset($this->_userParam[$name]);
    }

    public function __destruct()
    {
        $this->_userParam = array();
        $this->_systemParam = array();
    }

    public function __toString ()
    {
        return $this->createStrParam($this->_userParam);
    }

    public static function model($className=__CLASS__)
    {
        if(isset(self::$_models[$className]))
            return self::$_models[$className];
        else
        {
            $model=self::$_models[$className]=new $className();
            return $model;
        }
    }

    /**
     * 设置NICK
     * @access public
     * @return mixed
     */
    public function setSession($session = '')
    {
        if ('' === $session) {
            return $this->_session;
        } elseif (null == $session) {
            $this->_session = '';
        } elseif (is_string($session)) {
            $this->_session = $session;
        }

        return $this;
    }

    /**
     * 设置SESSION
     * @access public
     * @return mixed
     */
    public function setNick($nick = '')
    {
        if ('' === $nick) {
            return $this->_nick;
        } elseif (null == $nick) {
            $this->_nick = '';
        } elseif (is_string($nick)) {
            $this->_nick = $nick;

            $this->_session = Shop::mode()->nick2session($nick);
        }

        return $this;
    }

    // 用户参数设置与获取
    public function setUser($param = '') {
        if ('' === $param) {
            return $this->_userParam; //读取
        } elseif (null == $param) {
            $this->_userParam = array(); //清空
        } elseif(is_object($param)){
            $param = get_object_vars($param);
        } elseif(is_string($param)){
            parse_str($param,$param);
        } elseif(!is_array($param)){
            return $this->_userParam;
        }
        $this->_userParam = empty($this->_userParam) ? $param : array_merge($this->_userParam, $param);

        return $this;
    }

    // 系统参数设置与获取
    public function setSystem($param = '') {
        if ('' === $param) {
            return $this->_systemParam;
        } elseif (null == $param) {
            $this->_systemParam = array();
        } elseif(is_object($param)){
            $param = get_object_vars($param);
        } elseif(is_string($param)){
            parse_str($param,$param);
        } elseif(!is_array($param)){
            return $this->_systemParam;
        }
        $this->_systemParam = empty($this->_systemParam) ? $param : array_merge($this->_systemParam, $param);

        return $this;
    }

    /**
     * 数据提交方式
     * @access public
     * @return mixed
     */
    protected function setMode($mode = '') {
        $Modes = array('GET' => 1 , 'POST' => 2 , 'IMAGE' => 3);
        
        if ('' === $mode) {
            return $this->_modeParam;
        } elseif (null == $mode) {
            $this->_modeParam = 'GET';
        } elseif (array_key_exists(strtoupper($mode), $Modes)) {
            $mode = strtoupper($mode);
            if ($Modes[$mode] > $Modes[$this->_modeParam]) {
                $this->_modeParam = $mode;
            }
        }

        return $this;
    }

    /**
     * TQL串行化批量操作
     * @access public
     * @return mixed
     */
    public function setBatch($batch = '') {
        if ('' === $batch) {
            return $this->_batchParam;
        } elseif (null == $batch) {
            $this->_batchParam = array();
        } elseif (is_string($batch)) {
            $this->_batchParam[] = $batch;
        }

        return $this;
    }

    /**
     * 设置TQL串行化的操作符
     * @access public
     * @return mixed
     */
    public function setCut($cut = '') {
        if ('' === $cut) {
            return $this->_seperator;
        } elseif (null == $cut) {
            $this->_seperator = false;
	    unset($this->top_tql_seperator);
        } else {
            $this->_seperator = true;
	    $this->top_tql_seperator = 'true';
        }

        return $this;
    }

    /**
     * @return Taoapi
     */
    public function setRestNumberic ($rest)
    {
        $this->ApiConfig->RestNumberic = intval($rest);

		return $this;
    }
    
    /**
     * @return Taoapi
     */
    public function setVersion ($version, $signmode = 'md5')
    {
        $this->ApiConfig->Version  = intval($version);

        $this->ApiConfig->SignMode  = $signmode;

        return $this;
    }

    /**
     * @return Taoapi
     */
    public function setCloseError()
    {
        $this->ApiConfig->CloseError  = false;

        return $this;
    }

    /**
     * 查找记录
     * @access public
     * @return mixed
     */
    public function select($where = '') {
        if (!empty($where)) $this->where = $where;
    	$sql = 'select '
    		.$this->parseFields()
    		.' from '
    		.$this->parseMethod()
    		.$this->parseWhere();

        $this->setBatch($sql);
        $this->setMode('get');
        return $this;
    }

    /**
     * 插入记录
     * @access public
     * @param mixed $data 数据
     * @return false | integer
     */
    public function insert($data = '') {
        $data = $data ? $data : $this->data;
    	$values = $fields = array();

    	foreach ($data as $key=>$val){
    		$value = $this->parseValue($val);
    		if(is_scalar($value)) { // 过滤非标量数据
    			$values[] = $value;
    			$fields[] = $this->parseKey($key);
    		}
    	}
    	
    	$sql = 'insert into '
    		.$this->parseMethod()
    		.' ('.implode(',', $fields).')'
    		.' values '
    		.'('.implode(',', $values).')';

    	$this->setBatch($sql);
        $this->setMode('post');

        return $this;
    }
    
    /**
     * 更新记录
     * @access public
     * @param mixed $data 数据
     * @return false | integer
     */
    public function update($data = '') {
        $data = $data ? $data : $this->data;
    	$sql = 'update '
    		.$this->parseMethod()
    		.$this->parseSet($data)
    		.$this->parseWhere();

    	$this->setBatch($sql);
        $this->setMode('post');

        return $this;
    }
    
    /**
     * 删除记录
     * @access public
     * @param mixed $where 数据
     * @return false | integer
     */
    public function delete($where = '') {
    	$where && $this->where = $where;
    	$sql = 'delete from '
    		.$this->parseMethod()
    		.$this->parseWhere();

    	$this->setBatch($sql);
        $this->setMode('post');

        return $this;
    }

    /**
     * 嵌套查询、更新
     * 可以多层嵌套
     * 关联条件必须包含__SUBSELECT__，如$Taoapi->where = array('nick'=>'__SUBSELECT__');
     * select field1,field2,field3 from x where a in (select a from x1 where t=t1 and b=b1)
     * update x set field1=value1,field2=value2… where a in (select a from x1 where t=t1 and b=b1)
     * @access public
     * @return mixed
     */
    public function sub() {
        $batch = $this->setBatch();
        $count = count($batch);
        $sqls = '';
        if ($count > 1) {
            for ($i=0; $i < $count; $i++) { 
                $sql = current($batch);
                $sqls = str_replace('=__SUBSELECT__', " in ($sqls)", $sql);
                next($batch);
            }
        }
        $this->setBatch(null)->setBatch($sqls); //重新赋值
	$this->setCut(1); //设置间隔操作符号

        return $this;
    }

    /**
     * 复制新增、更新
     * 复制字段用$$封装，如$title$
     * insert into x (field1,field2) values($a$,$b$...) valuefrom select a,b… from x1 where t=t1 and b=b1
     * update x set field1=$a$,field2=$b$,field3=value3 where k=k1 valuefrom select a,bfrom x1 where t=t1 and b=b1
     * @access public
     * @return mixed
     */
    public function copy() {
        $batch = $this->setBatch();
        $select = current($batch);
        $update = next($batch);

        $sql = $update . ' valuefrom ' . $select;

        $this->setBatch(null)->setBatch($sql); //重新赋值

        return $this;
    }

    /**
     * 组装TQL数据
     * 支持串行化批量
     * @access public
     * @return mixed
     */
    public function get() {
        if (count($this->setBatch()) > 1) {
            foreach ($this->setBatch() as $sql) {
                $sqls .= '{'.$sql.'}';
            }
            $this->setMode('post');
            $this->setCut(1); //设置间隔操作符号
            //$this->_seperator = true; //串行化标准符号开启
            //$this->top_tql_seperator = 'true'; //串行化结果都由“\r\n”分割
        } else {
            $sqls = reset($this->setBatch());
            //$this->_seperator = false;
        }
        $this->ql = $sqls;
        

        //清空TQL封装
        unset($this->method);
        unset($this->where);
        unset($this->fields);
        unset($this->data);
        
        //trace($this->setUser(), 'user');
        //trace($this->setMode(), 'mode');
        //return 1;
        
        $resp = $this->Send()->getArrayData();
        $this->setCut(null); //清空间隔操作符号

        //trace($resp, 'resp');
        //trace($this->taobaoData, 'data');
        //F('b', $this->taobaoData);
        
        
        $this->setUser(null)->setBatch(null); //重置缓存数据

        return $resp;
    }

    /**
     * set分析
     * @access protected
     * @param array $data
     * @return string
    */
    protected function parseSet($data) {
    	foreach ($data as $key=>$val){
    		$value   =  $this->parseValue($val);
    		if(is_scalar($value)) // 过滤非标量数据
    			$set[]    = $this->parseKey($key).'='.$value;
    	}
    	return ' set '.implode(',',$set);
    }
    
    /**
     * 字段名分析
     * @access protected
     * @param string $key
     * @return string
     */
    protected function parseKey(&$key) {
    	return $key;
    }
    
    /**
     * value分析
     * @access protected
     * @param mixed $value
     * @return string
     */
    protected function parseValue($value) {
    	if(isset($value[0]) && is_string($value[0]) && strtolower($value[0]) == 'exp'){
    		$value =  $value[1];
    	}elseif(is_array($value)) {
    		$value =  array_map(array($this, 'parseValue'),$value);
    	}elseif(is_bool($value)){
    		$value =  $value ? 'true' : 'flase';
    	}elseif(is_null($value)){
    		$value =  'null';
    	}
    	return $value;
    }

    /**
     * 查询表组装
     * @access public
     * @param mixed _userParam['method']
     * @return string
     */
    protected function parseMethod(){
    	return $this->method;
    }
    
    /**
     * 查询字段组装
     * @access public
     * @param mixed _userParam['fields']
     * @return string
     */
    protected function parseFields(){
    	$fields = $this->fields;
    	 
    	if(is_string($fields) && strpos($fields,',')) {
    		$fields    = explode(',',$fields);
    	}
    	if(is_array($fields)) {
    		// 完善数组方式传字段名的支持
    		// 支持 'field1'=>'field2' 这样的字段别名定义
    		$array   =  array();
    		foreach ($fields as $key=>$field){
    			if(!is_numeric($key))
    				$array[] =  $this->parseKey($key).' AS '.$this->parseKey($field);
    			else
    				$array[] =  $this->parseKey($field);
    		}
    		$fieldsStr = implode(',', $array);
    	}elseif(is_string($fields) && !empty($fields)) {
    		$fieldsStr = $this->parseKey($fields);
    	}else{
    		$fieldsStr = '';
    	}

    	return $fieldsStr;
    }
    
    /**
     * 指定查询条件 支持安全过滤
     * @access public
     * @param mixed _userParam['where'] 条件表达式
     * @return string
     */
    protected function parseWhere() {
        $where = $this->where;
        if(!$where) return '';
        $whereStr = '';
        if (is_object($where)) {
            $where = get_object_vars($where);
        }
        if (is_string($where)) {
            $whereStr = $where;
        }elseif (is_array($where)) { // 使用数组表达式
            $operate = ' and ';
            foreach ($where as $key=>$val){
                $whereStr .= trim($key) . '=' . $this->parseValue($val) . $operate;
            }
            $whereStr = substr($whereStr,0,-strlen($operate));
        }
        return ' where ' . $whereStr;
    }
    
	private function FormatUserParam($param)
	{
		if(strtoupper($this->ApiConfig->Charset) != 'UTF-8')
		{
			if(function_exists('mb_convert_encoding'))
			{
			    if(is_array($param))
			    {
			        foreach($param as $key => $value)
			        {
				        $param[$key] = @mb_convert_encoding($value,'UTF-8',$this->ApiConfig->Charset);
			        }
			    }else{
				    $param = @mb_convert_encoding($param,'UTF-8',$this->ApiConfig->Charset);
			    }
			}elseif(function_exists('iconv'))
			{
			    if(is_array($param))
			    {
			        foreach($param as $key => $value)
			        {
				        $param[$key] = @iconv($this->ApiConfig->Charset,'UTF-8',$value);
			        }
			    }else{
				    $param = @iconv($this->ApiConfig->Charset,'UTF-8',$param);
			    }
			}
		}

		return $param;
	}

	private function FormatTaobaoData($data)
	{
		if(strtoupper($this->ApiConfig->Charset) != 'UTF-8')
		{
			if(function_exists('mb_convert_encoding'))
			{
				$data = str_replace('utf-8',$this->ApiConfig->Charset,$data);
				$data = @mb_convert_encoding($data,$this->ApiConfig->Charset,'UTF-8');
			}elseif(function_exists('iconv'))
			{
				$data = str_replace('utf-8',$this->ApiConfig->Charset,$data);
				$data = @iconv('UTF-8',$this->ApiConfig->Charset,$data);
			}
		}

		return $data;
	}

    /**
     * 发送数据
     * @return Taoapi
     */
    public function Send($mode = '', $format = '') {
        $imagesArray = $this->_ArrayModeData = array();
		$tempParam = $this->setUser();
        if (!empty($mode)) $this->setMode($mode);

        foreach ($tempParam as $key => $value) {
            if (trim($value) == '') {
                unset($tempParam[$key]);
            } else {
				if ("@" == substr($value, 0, 1)) $this->setMode('image'); //强制以postimg方式发送
                $tempParam[$key] = $this->FormatUserParam($value);
			}
        }
        if (!isset($tempParam['api_key'])) {
            $systemdefault['api_key'] = key($this->ApiConfig->AppKey);
            $systemdefault['sign_method'] = strtolower($this->ApiConfig->SignMode);
            $systemdefault['timestamp'] = date('Y-m-d H:i:s');
            $systemdefault['format'] = $format ? strtolower($format) : $this->ApiConfig->Format;
            if (isset($tempParam['method'])) { //Rest方式调用
            	$this->ApiConfig = $this->Config->setRest()->getConfig(); //重新加载配置
            	$systemdefault['v'] = $this->ApiConfig->Version;
            }
            
			$tempParam = array_merge($tempParam,$systemdefault);
			$this->_userParam = array_merge($this->_userParam, $systemdefault);
        }

        $this->submit($tempParam); //获取数据
        $this->ApiCallLog(); //记录API访问日志
        $this->ApiRetry(); //记录API错误日志

        return $this;
    }
    
    //记录重新及报错
    public function ApiRetry() {
        $error = $this->getArrayData();
        if (isset($error['error_response'])) {
            if(in_array($error['error_response']['code'],array(4,5,6,7,8,25))) {
                $this->_systemParam['apicount'] = empty($this->_systemParam['apicount']) ? 1 : $this->_systemParam['apicount'] + 1;
                if($this->_systemParam['apicount'] < count($this->ApiConfig->AppKey))
                {
                    next($this->ApiConfig->AppKey);
                    $this->api_key = key($this->ApiConfig->AppKey);
                    return $this->Send();
                }
            }
        
            if($this->ApiConfig->RestNumberic && empty($this->_systemParam['apicount'])) {
                $this->ApiConfig->RestNumberic = $this->ApiConfig->RestNumberic - 1;
                return $this->Send();
            } else {
                $tempParam['sign'] = $this->_systemParam['sign'];
                $this->_errorInfo = new TaoException($error, $this->_userParam, $this->ApiConfig->CloseError,$this->ApiConfig->Errorlog);
        
                if(!$this->ApiConfig->CloseError)
                {
                    echo $this->FormatTaobaoData($this->_errorInfo->getErrorInfo());
                }
            }
        }
    }

    //记录API访问日志
	public function ApiCallLog () {
		if($this->ApiConfig->ApiLog) {
			$apilogpath = dirname(__FILE__) . '/api_call_log';
			if (! is_dir($apilogpath)) {
				@mkdir($apilogpath);
			}
			if ($fp = @fopen($apilogpath . '/' .key($this->ApiConfig->AppKey).'_'. date('Y-m-d') . '.log', 'a')) {
				$logparam = $this->setUser();
				unset($logparam['fields']);
				@fwrite($fp, implode("\t", $logparam) . "\r\n");
				fclose($fp);
			}
		}
    }

    public function getXmlData ()
    {
        if (empty($this->taobaoData)) {
            return false;
        }		
        return $this->FormatTaobaoData($this->taobaoData);
    }

    public function getJsonData ()
    {
        if (empty($this->taobaoData)) {
            return false;
        }
        if (substr($this->taobaoData, 0, 1) != '{') {

            if ($this->format == 'xml') {
				$Charset = $this->ApiConfig->Charset;
				$this->ApiConfig->Charset = "UTF-8";
                $Data = $this->getArrayData($this->taobaoData);
				$this->ApiConfig->Charset = $Charset;
            }

            $Data = json_encode($Data);
            if (strpos($_SERVER['SERVER_SIGNATURE'], "Win32") > 0) {
                $Data = preg_replace("#\\\u([0-9a-f][0-9a-f])([0-9a-f][0-9a-f])#ie", "iconv('UCS-2','UTF-8',pack('H4', '\\1\\2'))", $Data);
            } else {
                $Data = preg_replace("#\\\u([0-9a-f][0-9a-f])([0-9a-f][0-9a-f])#ie", "iconv('UCS-2','UTF-8',pack('H4', '\\2\\1'))", $Data);
            }
			$Data = $this->FormatTaobaoData($Data);

        } else {
            $Data = $this->taobaoData;
        }
        return $Data;
    }

    public function getArrayData() {  //TODO XML的没有校验
        if (empty($this->taobaoData)) {
            return false;
        }
		if(!empty($this->_ArrayModeData[$this->ApiConfig->Charset])) {
			return $this->_ArrayModeData[$this->ApiConfig->Charset];
		}
        
        if ($this->format == 'json') {
            $this->taobaoData = preg_replace("/\":(\d+)/i",'":"$1"', $this->taobaoData); //将科学计数方式转成整数
            
            if ($this->setCut()) {
                $jsons = explode("\r\n", $this->taobaoData);
                foreach ($jsons as $key => $value) {
                    $json[] = json_decode($value, true);
                }
            } else {
                $json = json_decode($this->taobaoData, true);
            }
            
            return $json;
        } elseif ($this->format == 'xml') {
            $xmlCode = simplexml_load_string($this->taobaoData, 'SimpleXMLElement', LIBXML_NOCDATA);
			$taobaoData = $this->get_object_vars_final($xmlCode);

			if(strtoupper($this->ApiConfig->Charset) != 'UTF-8')
			{
				$taobaoData = $this->get_object_vars_final_coding($taobaoData);
			}

			$this->_ArrayModeData[$this->ApiConfig->Charset] = $taobaoData;

            return $taobaoData;

        } else {
            return false;
        }
    }

    /**
     * 返回错误提示信息
     * @return array
     */
    public function getErrorInfo ()
    {
        if ($this->_errorInfo) {
            if (is_object($this->_errorInfo)) {
                return $this->FormatTaobaoData($this->_errorInfo->getErrorInfo());
            } else {
                return $this->FormatTaobaoData($this->_errorInfo);
            }
        }
    }
    
    private function JoinSign($paramArr) {
       $sign = '';
       foreach ($paramArr as $key => $val) {
               if (is_array($val)) {
                   $sign .= $this->JoinSign($val);
               } elseif ($key != '' && $val != '' && "@" != substr($val, 0, 1)) { // 判断键和键值非空及文件上传
                    $sign .= $key . $val;
               }
        }
        
        return $sign;
    }
    
    public function createSign($paramArr)
    {
    	ksort($paramArr);
    	$sign = $this->JoinSign($paramArr);
    	if (strtolower($this->ApiConfig->SignMode) == 'hmac') {
    		$sign = strtoupper($this->hmac($sign,current($this->ApiConfig->AppKey)));
    	} else {
    		$sign  = strtoupper(md5(current($this->ApiConfig->AppKey) . $sign . current($this->ApiConfig->AppKey)));
    	}
    	$this->_systemParam['sign'] = $sign;
    	
        return $sign;
    }

    static public function createStrParam($paramArr) {
        $strParam = array();
        foreach ($paramArr as $key => $val) {
            if ($key != '' && $val != '') {
                $strParam []= $key . '=' . urlencode($val);
            }
        }
        return implode('&',$strParam);
    }
    
    /**
     * 用curl获取数据
     * @param string $url 远程地址
     * @param string $fields POST数据
     * @param string $timeout 超时时间
     * @return $result
     */
    public function curl($url, $fields = null, $timeout = 30) {
    	$ch = curl_init ();
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_FAILONERROR, false);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
    	// https 请求
    	if (strlen($url) > 5 && strtolower(substr($url, 0, 5)) == "https") {
    		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    	}
    	// POST方式上传数据
    	if ($fields) {
    		curl_setopt($ch, CURLOPT_POST, true);
    		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    	}
    	
    	$reponse = curl_exec($ch);
    	if (curl_errno($ch)) {
            throw new Exception(curl_error($ch), 0);
    	} else {
    		$httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    		if (200 !== $httpStatusCode) {
    			throw new Exception($reponse, $httpStatusCode);
    		}
    	}
    	curl_close($ch);
    	
    	return $reponse;
    }
    
    /**
     * 访问API服务
     * @param $paramArr：api参数数组
     * @return $result
     */
    public function submit ($paramArr) {
        $this->_systemParam['sign'] = $this->createSign($paramArr); //生成签名
        $paramArr['sign'] = $this->_systemParam['sign'];

        $url = $this->ApiConfig->Url;
        switch ($this->setMode()) {
            case 'POST':
                $paramArr = $this->createStrParam($paramArr); //urlencode编码
                $this->_systemParam['url'] = array($this->ApiConfig->Url , $paramArr);
                break;

            case 'IMAGE':
                $this->_systemParam['url'] = array($this->ApiConfig->Url , $paramArr);
                break;
            
            default:
                $strParam = $this->createStrParam($paramArr); //urlencode编码
                $url = $this->_systemParam['url'] = $this->ApiConfig->Url . '?' . $strParam;
                $paramArr = null;
        }
        $this->taobaoData = $this->curl($url, $paramArr);  //获取数据

        return $this;
    }

    private function get_object_vars_final ($obj)
    {
        if (is_object($obj)) {
            $obj = get_object_vars($obj);
        }

        if (is_array($obj)) {
            foreach ($obj as $key => $value) {
                $obj[$key] = $this->get_object_vars_final($value);
            }
        }
        return $obj;
    }

    private function get_object_vars_final_coding ($obj)
    {
		foreach($obj as $key => $value)
		{
			if(is_array($value))
			{
				$obj[$key] = $this->get_object_vars_final_coding($value);
			}else{
				$obj[$key] = $this->FormatTaobaoData($value);
			}
		}
        return $obj;
    }

	public function getUrl()
	{
		return !empty($this->_systemParam['url']) ? $this->_systemParam['url'] :'';
	}
	
	public function getSign()
	{
		return !empty($this->_systemParam['sign']) ? $this->_systemParam['sign'] :'';
	}
	
	/**
	 * HMAC加密
	 * @param String $data 预加密数据
	 * @param String $key  密钥
	 * @return String
	 */
	function hmac($data, $key){
		if (function_exists('hash_hmac')) {
			return hash_hmac('md5', $data, $key);
		}
		$key = (strlen($key) > 64) ? pack('H32', 'md5') : str_pad($key, 64, chr(0));
		$ipad = substr($key,0, 64) ^ str_repeat(chr(0x36), 64);
		$opad = substr($key,0, 64) ^ str_repeat(chr(0x5C), 64);
		return md5($opad.pack('H32', md5($ipad.$data)));
	}
	
}