<?
/**
 * 淘宝API处理类-通用
 */
class TaoComm extends Taoapi
{

    public function __construct() {
        parent::__construct();
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * 下载多个用户数据
     * @param $nicks 用户昵称。可传入数组或是字符串， 最多40个
     * 所有字段：user_id, nick, sex, buyer_credit, seller_credit, location, created, last_visit, type, consumer_protection
     */
    public function syn_users($nicks, $fields = "") {
        if (is_array($nicks)) $nicks = implode(',', $nicks);  //将数组转换成字符串
        if (!$fields) {
            $fields = "user_id, nick, sex, buyer_credit, seller_credit, location, created, last_visit, type, consumer_protection";
        } else {
            $fields .=", user_id, nick";
        }
        $params['ql'] = "select ".$fields." from users where nicks=$nicks";
        $users = $this->top->tql($params);
        $users = $users['users']['user'];
        return $users;
    }
    
    /**
     * 下载单个用户数据
     * @param $nick 用户昵称
     * 所有字段：user_id, uid, nick, sex, buyer_credit, seller_credit, location, created, last_visit, birthday, type, has_more_pic, item_img_num, item_img_size, prop_img_num, prop_img_size, auto_repost, promoted_type, status, alipay_bind, consumer_protection, alipay_account, alipay_no, avatar, liangpin, sign_food_seller_promise, has_shop, is_lightning_consignment, has_sub_stock, is_golden_seller, vip_info, email, magazine_subscribe, vertical_market, online_gaming
     */
    public function syn_user($nick, $fields = "", $session = SESSIONKEY) {
        if (!$fields) {
            $fields = "user_id, uid, nick, vip_info, promoted_type, location, birthday, sex, type, email, status, auto_repost, alipay_bind, alipay_account, alipay_no, avatar, has_shop, consumer_protection, is_lightning_consignment, liangpin, sign_food_seller_promise, has_sub_stock, is_golden_seller, magazine_subscribe, vertical_market, buyer_credit, seller_credit, last_visit, created";
        } else {
            $fields .=", user_id, nick";
        }
        $params['ql'] = "select ".$fields." from user where nick=$nick";
        $user = $this->top->tql($params, $session);
        
        $this->update_user($user['user']);  //更新用户表
        
        return $user;
    }
    
    /**
     * 下载单个店铺基本信息
     * @param $nick 用户昵称
     * @param $fields 需要获取的字段
     * @param $add 是否更新数据库
     */
    public function syn_Shop($nick, $fields = "", $add = true) {
        if (!$fields) {  //生成需要获取的字段
            $fields = "sid, cid, nick, title, desc, bulletin, pic_path, created, modified, shop_score";
        } else {
            $fields .= ", sid, nick, modified";
        }
        $params['ql'] = "select $fields from shop where nick=$nick";
        $shop = $this->top->tql($params, SESSIONKEY);
        $shop = $shop['shop'];
    
        if ($add) $this->save_shop($shop); //更新数据库
        
        return $shop;
    }
    
    /**
     * 更新店铺基本信息
     * @param $nick 用户昵称
     * @param $fields 需要获取的字段
     * @param $add 是否更新数据库
     */
    public function update_Shop($shop, $add = true) {
        $sql = $this->top->sqls($shop);
        $params['ql'] = "update shop set $sql";
        $shop = $this->top->tql($params, SESSIONKEY);
        $shop = $shop['shop'];
    
        if ($add) $this->save_shop($shop); //更新数据库
    
        return $shop;
    }
    
    /**
     * 橱窗数量
     * 获取卖家店铺剩余橱窗数量
     * 返回 remain_count，all_count，used_count
     */
    public function get_showcase() {
        $params['ql'] = "select shop from shop.remainshowcase";
        $result = $this->top->tql($params, SESSIONKEY);
        
        return $result['shop'];
    }
    
    /**
     * 授权登录
     * 获取授权登录信息
     */
    public function check_container($container) {
        $sign = $this->top->check_sign($container);  //检查授权签名是否合法
        if (!$sign) return false;
        $parameters = $this->top->Parameters($container['top_parameters']);
        if (!is_array($parameters)) return false;
        $nick = $parameters['visitor_nick'];
        $userid = cookie('userid');  //获取用户ID
        
        $User = D('User');
        $User->syn_user($nick, "", $container['top_session']); //同步淘宝用户信息
        $User->where(array('nick'=>$nick, 'userid'=>$userid))->save(array('sessionKey'=>$container['top_session'], "group"=>1));  //绑定淘宝用户到系统
        $this->syn_Shop($nick); //同步淘宝店铺信息
        $this->where(array("nick"=>$nick))->save(array("userid"=>$userid, "title"=>$nick, "status"=>77));  //更新店铺类型
        
        import('COM.Tb.Increment');
        Increment::permit_customer($nick);  //订阅主动通知
        
        return true;
    }
    
    /**
     * 下载退款数据
     * @param $start_modified 修改时间开始
     * @param $end_modified 修改时间结束
     * @param $page_no 页码
     * @param $page_size 每页条数
     */
    public function synRefunds($start_modified = "", $end_modified = "", $page_no = 1, $page_size=100) {
        $this->method = 'refunds.receive';
        $this->session = SESSIONKEY;
        $this->fields = "refund_id, tid, oid, seller_nick, status, good_status, refund_fee, payment, total_fee, desc, reason, company_name, sid, has_good_return, created, modified";
        $where = "page_no=$page_no and page_size=$page_size";
        if ($start_modified && $end_modified) {
            if ($end_modified <= $start_modified) return false;
            $where .= " and start_modified=$start_modified and end_modified=$end_modified"; 
        }
        $this->where = $where;
        $result = $this->select()->get();
        $total = $result['total_results'];

        foreach ($result['refunds_receive_get_response']['refunds']['refund']  as $refund) {
            $this->saveRefund($refund); //入库
        }
        
        return $total;
    }
    
    /**
     * 下载退款留言/凭证列表查询
     * @param $refund_id 退款ID
     * @param $page_no 页数
     * @param $page_size 页码
     */
    public function getMsg($refund_id, $page_no = 1, $page_size=100) {
        $this->session = SESSIONKEY;
        $this->method = 'refund.messages';
        $this->fields = "id,owner_id,owner_role,owner_nick,message_type,pic_urls,content,created";
        $this->where = "refund_id=$refund_id";

        $results = $this->select()->get();

        $messages['total'] = $results['refund_messages_get_response']['total_results'];
        $messages['messages'] = $results['refund_messages_get_response']['refund_messages']['refund_message'];
        
        return $messages;
    }
    
    /**
     * 创建退款留言/凭证
     * @param $refund_id 退款编号
     * @param $content 留言内容。最大长度: 400个字节
     * @param $image 图片（凭证）。类型: JPG,GIF,PNG;最大为: 500K
     */
    public function addMsg($refund_id, $content, $image="") {
        $this->session = SESSIONKEY;
        $this->method = 'refund.messages';
        $this->where = "refund_id=$refund_id";
        
        $results = $this->update($content)->get();

        $id = $results['refund_messages_update_response']['refund_message']['id'];
    
        return $id;
    }
    
    /**
     * 下载单笔退款数据
     * @param $refund_id 退款ID
     * @param $fields 需要获取的字段
     * @param $refund 退款数据
     */
    public function synRefund($refund_id, $fields = "", $nick = "") {
        $this->session = $nick ? D('User')->nick2session($nick) : SESSIONKEY;
        $this->method = 'refund';
        if (!$fields) {  //生成需要获取的字段
            $this->fields = "refund_id,tid,oid,seller_nick,status,good_status,cs_status,advance_status,split_taobao_fee,split_seller_fee,address,refund_fee,payment,total_fee,desc,reason,company_name,sid,has_good_return,good_return_time,created,modified,refund_remind_timeout";
        } else {
            $this->fields = $fields . ",refund_id,modified";
        }
        $this->where = "refund_id=$refund_id";

        $refund = $this->select()->get();

        $this->saveRefund($refund['refund_get_response']['refund']); //入库
        
        return $refund;
    }
    
    /**
    * 搜索评价信息
    * @param $where 评价搜索条件
    * @param $page_no 页码 翻页获取的条数（page_no*page_size）
    * @param $page_size 每页条数 最大值:150
    * @param $fields 需返回的字段
    * 参数：rate_type,role,result,page_no,page_size,start_date,end_date,tid,use_has_next,num_iid
    */
    public function get_Rates($where, $page_no = 1, $page_size = 40, $fields = "") {
        if (!is_array($where)) return false;
        
        $where = $this->top->sqls($where, ' and ');
        $where .=" and page_no=$page_no and page_size=$page_size";
        if (!$fields) {
            $fields = "num_iid, valid_score, tid, oid, role, nick, result, created, rated_nick, item_title, item_price, content, reply";
        } else {
            $fields .=", tid, oid, num_iid, created";
        }
        $params['ql'] = "select $fields from taobao.traderates.get where $where";
        $result = $this->top->tql($params, SESSIONKEY);
        $rates['rates'] = $result['trade_rates']['trade_rate'];
        $rates['total'] = $result['total_results'];
        
        return $rates;
    }

    /**
     * 更新单个系统类目的属性
     */
    public function syn_Prop($cid) {
        $fields = "is_input_prop, pid, parent_pid, parent_vid, name, is_key_prop, is_sale_prop, is_color_prop, is_enum_prop, is_item_prop, must, multi, prop_values, status, sort_order, child_template, is_allow_alias";
        $params['ql'] = "select $fields from itemprops where cid=$cid";
        $props = $this->top->tql($params);
        $props = $props['item_props']['item_prop'];
        
        if ($props) $this->where(array('cid'=>$cid))->delete(); //清空对应类目表数据
        $modified = NOW_TIME;
        foreach ($props as $prop ) {
            $prop = $this->format($prop);
            $prop['cid'] = $cid;
            $prop['modified'] = $modified;
            $this->add($prop);
        }
    }

    /**
     * 获取物流公司信息
     *
     * @param $api 物流查询接口.
     *  0:淘宝；1：金蝶kuaidi001
     */
    public function getCompanies($key = 'name') {
        $field = 'name';
        if ($key <> 'name') $field = $key.',name';
        
        $logistics = $this->field($field)->limit(0,200)->order('listorder DESC, id DESC')->select();
        foreach ($logistics as $v) {
            $logistic[$v[$key]] = $v['name'];
        }
        return $logistic;
    }
    
    /**
     * 查询卖家地址库
     * @param $where 默认地址库
     * no_def:查询非默认地址
     * get_def:查询默认取货地址
     * cancel_def:查询默认退货地址
     * @param $area_id 地址库ID
     */
    public function synAddress($where = "", $area_id = 0) {
        if ($where) $where = " where rdef=$where";
        $params['ql'] = "select area_id from taobao.logistics.address.search $where";
        $addresses = $this->top->tql($params, SESSIONKEY);
        $addresses = $addresses['addresses']['address_result'];
    
        if (!$area_id) return $addresses;
    
        foreach ($addresses as $address) {  //编辑卖家地址库
            if (in_array ($area_id, $address)) return $address;
        }
    }
    
    /**
     * 新增卖家地址库
     * @param $address 地址数据集合
     */
    public function addAddress($address) {
        if ($address['memo']) {
            $params['memo'] = $address['memo'];
            $address['memo'] = "#memo#";
        }
        $params['addr'] = $address['addr'];
        $address['addr'] = "#addr#";
        $field = implode(",", array_keys($address));  //拼装字段列表
        $value = implode(",", array_values($address)); //拼装值列表
    
        $params['ql'] = "insert into logistics.address ($field) values ($value)";
        $result = $this->top->tql($params, SESSIONKEY);
    
        if ($result['address_result']) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * 修改卖家地址库
     * @param $contact_id 地址库ID
     * @param $address 地址数据集合
     */
    public function modifyAddress($contact_id, $address) {
        if ($address['memo']) {
            $params['memo'] = $address['memo'];
            $address['memo'] = "#memo#";
        }
        $params['addr'] = $address['addr'];
        $address['addr'] = "#addr#";
    
        $update = $this->top->sqls($address, ',');
        $params['ql'] = "update taobao.logistics.address.modify set $update where contact_id=".$contact_id;
        $result = $this->top->tql($params, SESSIONKEY);
    
        if ($result['address_result']) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * 删除卖家地址库
     * @param $contact_id 地址库ID
     */
    public function removeAddress($contact_id) {
        $params['ql'] = "delete from taobao.logistics.address.remove where contact_id=".$contact_id;
        $result = $this->top->tql($params, SESSIONKEY);
    
        if ($result['address_result']) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * 获取用户下所有运费模板
     * template_name返回name fee_list
     */
    public function synTemplates($fields = "") {
        $this->method = 'delivery.templates';
        $this->session = SESSIONKEY;
        if (!$fields) {
            $this->fields = "template_id, template_name, assumer ,valuation, query_express, query_ems, query_cod, query_post, supports, created, modified";
        } else {
            $this->fields = $fields;
        }
        $resp = $this->select()->get();

        return $resp['delivery_templates_response']['delivery_templates']['delivery_template'];
    }
    
    /**
     * 获取物流公司信息
     * 表中有kuaidi100的信息，慎用
     */
    public function synCompany() {
        $fields = "id,code,name,reg_mail_no";
        $params['ql'] = "select $fields from logistics.companies";
        $companies = $this->top->tql($params);
    
        return $companies['logistics_companies']['logistics_company'];
    }
    
    /**
     * 物流流转信息查询
     * 表中有kuaidi100的信息，慎用
     */
    public function synTrace($tid, $seller_nick) {
        $where = "tid=$tid and seller_nick=$seller_nick";
        $params['ql'] = "select out_sid from taobao.logistics.trace.search where $where";
        $traces = $this->top->tql($params);
        $traces['trace_list'] = $traces['trace_list']['transit_step_info'];
    
        return $traces;
    }
    
    /**
     * 获取并更新地区表
     * 不需要经常更新
     */
    public function synArea() {
        $fields = "id,type,name,parent_id,zip";
        $params['ql'] = "select $fields from areas";
        $areas = $this->top->tql($params);
        if (!$areas['areas']) return false;
        
        $this->execute("truncate table __TABLE__ "); // 清空areas表
        foreach ($areas['areas']['area'] as $area) {
            $this->add($area);  //更新areas表
        }
        return true;
    }

    /**
     * 同步店铺自有类目
     * @param $nick 昵称
     */
    public function syn_sellercats($nick) {
        $fields = "type, cid, parent_cid, name, pic_url, sort_order";
        $params['ql'] = "select $fields from sellercats.list where nick=$nick";
        $cats= $this->top->tql($params);
        $cats = $cats['seller_cats']['seller_cat'];
        if ($cats)  $this->where(array('model'=>'seller', 'nick'=>$nick))->delete(); //清空对应类目表数据
        $modified = NOW_TIME;
        foreach ($cats as $cat) {
            $cat['modified'] = $modified;
            $cat['model'] = 'seller';
            $cat['nick'] = $nick;
            $this->add($cat);
        }
        
        return true;
    }
    
    /**
     * 更新宝贝类目
     * @param $cid 类目
     */
    public function syn_itemcats($cid) {
        $fields = "cid,parent_cid,name,is_parent,status,sort_order";
        $params['ql'] = "select $fields from itemcats where parent_cid=$cid";
        $cats= $this->top->tql($params);
        $cats = $cats['item_cats']['item_cat'];
        
        if ($cid == 0) $this->where(array('model'=>'item'))->delete(); //清空对应类目表数据
        $modified = NOW_TIME;
        foreach ($cats as $cat) {
            $cat['is_parent'] = str2bool($cat['is_parent']); // 进行布尔值转换
            $cat['modified'] = $modified;
            $cat['model'] = 'item';
            unset($cat['status']);
            $this->add($cat);
            if ($cat['is_parent'] && $cid > 0) $this->syn_itemcats($cat['cid']);
        }
        
        return true;
    }
}