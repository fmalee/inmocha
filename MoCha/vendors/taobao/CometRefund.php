<?php
set_time_limit(0);
class CometRefund {
	private $Refund, $refunds, $rid, $tid, $nick, $modified, $update, $where;
	
	/**
	 * 架构函数
	 * @access public
	 * @param array $items  宝贝数据
	 */
	public function __construct($items) {
		$this->Refund = D('Refund');
	}
	
	/**
	 * 传入数据
	 * @access public
	 * @param array $refunds  退款数据
	 */
	public function Import($refunds) {
		//Log::write(json_encode($refunds), Log::ERR);
		if (!is_array($refunds)) return false;
		
		$this->refunds = $this->format($refunds);
		$status = $this->refunds['status'];
		$this->rid = $this->refunds['rid'];
		$this->tid = $this->refunds['tid'];
		$this->nick = $this->refunds['seller_nick'];
		$this->modified = $this->refunds['modified'];
		/* $this->buyer_nick = $this->refunds['buyer_nick'];
		$this->refund_fee = $this->refunds['refund_fee']; */
		
		$this->update = array('modified' => $this->modified);  //更新数据
		$this->where = array('refund_id' => $this->rid);  //更新条件
		
		$this->$status();//调用执行接口
	}
	
	/**
	 * 退款创建
	 */
	private function RefundCreated() {
		$this->Refund->synRefund($this->rid, "", $this->nick);
	}
	
	/**
	 * 退款成功
	 */
	private function RefundSuccess() {
		if ($this->syn("status")) {
			$this->update['status'] = 'SUCCESS';
			$this->Refund->where($this->where)->save($this->update);
		}
	}
	
	/**
	 * 退款关闭
	 */
	private function RefundClosed() {
		if ($this->syn("status")) {
			$this->update['status'] = 'CLOSED';
			$this->Refund->where($this->where)->save($this->update);
		}
	}
	
	/**
	 * 卖家同意退款协议
	 */
	private function RefundSellerAgreeAgreement() {
		if ($this->syn("status")) {
			$this->update['status'] = 'WAIT_BUYER_RETURN_GOODS';
			$this->Refund->where($this->where)->save($this->update);
		}
	}
	
	/**
	 * 卖家拒绝退款协议
	 */
	private function RefundSellerRefuseAgreement() {
		if ($this->syn("status")) {
			$this->update['status'] = 'SELLER_REFUSE_BUYER';
			$this->Refund->where($this->where)->save($this->update);
		}
	}
	
	/**
	 * 买家修改退款协议
	 */
	private function RefundBuyerModifyAgreement() {
		return $this->Refund->synRefund($this->rid, "", $this->nick);
	}
	
	/**
	 * 买家退货给卖家
	 */
	private function RefundBuyerReturnGoods() {
		return $this->Refund->synRefund($this->rid, "", $this->nick);
	}
	
	/**
	 * 发表退款留言
	 */
	private function RefundCreateMessage() {
		//TODO 发表退款留言
		Log::write("发表退款留言:".$this->rid, Log::ERR);
		return true;
	}
	
	/**
	 * 屏蔽退款留言
	 */
	private function RefundBlockMessage() {
		//TODO 屏蔽退款留言
		Log::write("屏蔽退款留言:".$this->rid, Log::ERR);
		return true;
	}
	
	/**
	 * 退款超时提醒
	 */
	private function RefundTimeoutRemind() {
		//TODO 退款超时提醒
		Log::write("退款超时提醒:".$this->rid, Log::ERR);
		return true;
	}

	/**
	 * 比较淘宝和本地商品最后更新时间
	 */
	private function syn($fields) {
		if (!$this->loc_time()) {  //记录在数据库中不存在
			$this->Refund->synRefund($this->rid, "", $this->nick);
			return false;
		} else if ($this->modified < $this->loc_time()) {  //新的记录变更时间比数据库的早
			$this->Refund->synRefund($this->rid, $fields, $this->nick);
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * 获取本地商品最后更新时间
	 */
	private function loc_time() {
		$modified = $this->Refund->where($this->where)->getField('modified');
		if (!$modified) return false;
		return $modified;
	}
	
	/**
	 * 数据检查
	 * @param $items 商品数据
	 */
	private function format($refunds) {
		//if ($refunds['rid']) $refunds['rid'] = number_format($refunds['rid'],0,',','');
		//if ($refunds['tid']) $refunds['tid'] = number_format($refunds['tid'],0,',','');
		//if ($refunds['oid']) $refunds['oid'] = number_format($refunds['oid'],0,',','');
		if ($refunds['modified']) $refunds['modified'] = strtotime($refunds['modified']);
	
		return $refunds;
	}
}
?>