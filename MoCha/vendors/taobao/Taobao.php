<?php

Yii::import('application.vendors.taobao.*');
/**
 * 淘宝API处理类
 *
 * @category Taoapi
 * @package Taoapi
 * @copyright Copyright (c) 2008-2009 Taoapi (http://www.Taoapi.com)
 * @license    http://www.Taoapi.com
 * @version    Id: Taoapi  2009-12-22  12:30:51 旺旺:浪子Arvin QQ:8769852
 */
class Taobao extends Taoapi
{
    //初始当前用户SESSION
    public $_session;
    //初始当前用户NICK
    public $_nick;

    public function __construct() {
        parent::__construct();
        
        if (isset(Yii::app()->user->tbkey)) {
            $this->_session = Yii::app()->user->tbkey;
            $this->_nick = Yii::app()->user->tbnick;
        }
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * 设置NICK
     * @access public
     * @return mixed
     */
    public function setSession($session = '')
    {
        if ('' === $session) {
            return $this->_session;
        } elseif (null == $session) {
            $this->_session = '';
        } elseif (is_string($session)) {
            $this->_session = $session;
        }

        return $this;
    }

    /**
     * 设置SESSION
     * @access public
     * @return mixed
     */
    public function setNick($nick = '')
    {
        if ('' === $nick) {
            return $this->_nick;
        } elseif (null == $nick) {
            $this->_nick = '';
        } elseif (is_string($nick)) {
            $this->_nick = $nick;

            $this->_session = Shop::mode()->nick2session($nick);
        }

        return $this;
    }

    /**
     * 同步淘宝和本地交易数据
     * @param $tid 传入TID
     * @param $field 需要更新的字段
     * @param $nick 商家昵称 用于获取SESSIONKEY
     * 包含以下字段的返回会增加TOP的后台压力:commission_fee, buyer_alipay_no, buyer_email, timeout_action_time, item_memo, trade_memo, available_confirm_fee
     */
    public function synTrade($tid, $fields = "", $nick = "") {
        $this->method = 'trade.fullinfo';
        $this->session = $this->_session;
        if (!$fields) {
            $this->fields = "seller_nick, buyer_nick, type, created, tid, seller_rate, buyer_rate, status, payment, post_fee, total_fee, pay_time, end_time, modified, consign_time, buyer_obtain_point_fee, point_fee, real_point_fee, received_payment, commission_fee, cod_fee, cod_status, shipping_type, receiver_name, receiver_state, receiver_city, receiver_district, receiver_address, receiver_zip, receiver_mobile, receiver_phone, seller_memo, alipay_no, buyer_message, buyer_alipay_no, buyer_email, seller_flag, available_confirm_fee, has_post_fee, trade_memo, is_3D, invoice_name, promotion_details, orders, trade_from";
        } else {
            $this->fields = $fields . ",tid,modified";
        }
        $this->where = "tid=$tid";
        $trade = $this->select()->get();

        if (isset($trade['trade_fullinfo_get_response']['trade'])) {
            $this->saveTrade($trade['trade_fullinfo_get_response']['trade']); //入库

            return true;
        } else {
            return false;
        }
    }

    /**
     * 批量同步交易数据
     * @param $where 查询条件  start_created，end_created，status，page_no，page_size，buyer_nick，type，ext_type，rate_status，tag，use_has_next，is_acookie
     * @param $field 需要更新的字段
     * @param $nick 商家昵称 用于获取SESSIONKEY
     */
    public function synTrades($where, $fields = "", $nick = "") {
        $this->session = $this->_session;

        $this->method = 'trades.sold';
        $this->fields = 'tid';
        $this->select($where);

        $this->method = 'trade.fullinfo';
        if (!$fields) {
            $this->fields = "seller_nick, buyer_nick, type, created, tid, seller_rate, buyer_rate, status, payment, post_fee, total_fee, pay_time, end_time, modified, consign_time, buyer_obtain_point_fee, point_fee, real_point_fee, received_payment, commission_fee, cod_fee, cod_status, shipping_type, receiver_name, receiver_state, receiver_city, receiver_district, receiver_address, receiver_zip, receiver_mobile, receiver_phone, seller_memo, alipay_no, buyer_message, buyer_alipay_no, buyer_email, seller_flag, available_confirm_fee, has_post_fee, trade_memo, is_3D, invoice_name, promotion_details, orders, trade_from";
        } else {
            $this->fields = $fields . ",tid,modified";
        }
        $this->select(array('tid'=>'__SUBSELECT__'));

        $result = $this->sub()->get();

        foreach ($result as $trade) {
            if (isset($trade['trade_fullinfo_get_response']['trade']))
                $this->saveTrade($trade['trade_fullinfo_get_response']['trade']); //入库
        }

        return true;
    }

    /**
     * 批量同步交易数据
     * @param $where 查询条件  start_created，end_created，status，page_no，page_size，buyer_nick，type，ext_type，rate_status，tag，use_has_next，is_acookie
     * @param $field 需要更新的字段
     * @param $nick 商家昵称 用于获取SESSIONKEY
     */
    public function tradeSold($where, $fields = "") {
        $this->session = $this->_session;
        $this->method = 'trades.sold';
        if (!$fields) {
            $this->fields = "tid,num,num_iid,status,title,type,price,discount_fee,point_fee,total_fee,is_lgtype,is_brand_sale,is_force_wlb,lg_aging,lg_aging_type,created,pay_time,modified,end_time,alipay_id,alipay_no,seller_flag,buyer_nick,buyer_area,has_yfx,yfx_fee,yfx_id,yfx_type,has_buyer_message,credit_card_fee,nut_feature,step_trade_status,step_paid_fee,mark_desc,send_time,shipping_type,adjust_fee,buyer_obtain_point_fee,cod_fee,trade_from,cod_status,service_orders,commission_fee,buyer_rate,trade_source,seller_can_rate,seller_nick,pic_path,payment,seller_rate,real_point_fee,post_fee,receiver_name,receiver_state,receiver_city,receiver_district,receiver_address,receiver_zip,receiver_mobile,receiver_phone,consign_time,received_payment,orders";
        } else {
            $this->fields = $fields;
        }
        $this->where = $where;

        $result = $this->select()->get();

        return $result;
    }

    /**
     * 同步订单物流信息
     * @param $tid 传入TID
     * @param $nick 商家昵称  用于主动通知
     */
    public function TradeDelivery($tid) {
        $this->method = 'logistics.orders';
        $this->session = $this->_session;
        $this->fields = "delivery_start, delivery_end, out_sid, created, modified, status, freight_payer, company_name";
        $this->where = "tid=$tid";
        $shibping = $this->select()->get();

        $shipping = $shibping['logistics_orders_get_response']['shippings']['shipping'];
        $shipping = reset($shipping);
            
        $trade['delivery_start'] = strtotime($shipping['delivery_start'] );
        $trade['delivery_end'] = strtotime($shipping['delivery_end'] );
        $trade['out_sid'] = $shipping['out_sid'];
        $trade['delivery_company'] = $shipping['company_name'];
        $trade['delivery_created'] = strtotime($shipping['created'] );
        $trade['delivery_modified'] = strtotime($shipping['modified'] );
        $trade['delivery_status'] = $shipping['status'];

        unset($shipping);
        
        return $trade;
    }

    /**
     * 保存淘宝交易数据
     * @param trade $trade
     */
    public function saveTrade($trade)
    {
        $trade = $this->formatTrade($trade); //格式化
        
        $id = $trade['id'];
        if (isset($trade['orders'])) {
            $this->saveOrder($id, $trade['orders']['order']);  //更新订单数据
            unset($trade['orders']);
        }
        if (in_array($trade['status'], array(Trade::WAIT_BUYER_CONFIRM_GOODS, Trade::TRADE_BUYER_SIGNED, Trade::TRADE_FINISHED))) { // 物流信息
            $shipping = $this->TradeDelivery($id);
            $trade = array_merge($trade, $shipping);
        }
        
        $is_exists = Trade::model()->exists("id=:id",array(":id"=>$id));
        if(!$is_exists)
        {
            $model = new Trade;
            $model->attributes = $trade;
            $model->save();
        }
        else
            Trade::model()->updateByPk($id, $trade);

        return $id;
    }

    /**
     * 同步淘宝和本地订单数据
     * @param $trade_id 交易编号
     * @param $orders 订单数据
     */
    public function saveOrder($trade_id, $orders) {
        foreach ($orders as $order) {
            $order = $this->formatOrder($order);
            $order['trade_id'] = $trade_id;
            $id = $order['id'];

            $is_exists = Order::model()->exists("id=:id",array(":id"=>$id));
            if(!$is_exists)
            {
                $model = new Order;
                $model->attributes = $order;
                $model->save();
            }
            else
                Order::model()->updateByPk($id, $order);
        }
    }

    /**
     * 转换交易数据格式
     * @param $trade 传入交易数据
     */
    public function formatTrade($trade)
    {
        $trade['id'] = $trade['tid'];
        $trade['shop_id'] = Shop::model()->getIdByNick($trade['seller_nick']);
        unset($trade['tid'], $trade['seller_nick']);

        if(isset($trade['status'])) $trade['status'] = Trade::model()->formatAlias('status', $trade['status']);
        if(isset($trade['trade_from'])) $trade['trade_from'] = Trade::model()->formatAlias('tradeFrom', $trade['trade_from']);
        if(isset($trade['type'])) $trade['type'] = Trade::model()->formatAlias('type', $trade['type']);
        if(isset($trade['shipping_type'])) $trade['shipping_type'] = Trade::model()->formatAlias('shippingType', $trade['shipping_type']);
        if(isset($trade['cod_status'])) $trade['cod_status'] = Trade::model()->formatAlias('codStatus', $trade['cod_status']);

        if (isset($trade['created'])) $trade['created'] = strtotime($trade['created']);
        if (isset($trade['modified'])) $trade['modified'] = strtotime($trade['modified']);
        if (isset($trade['end_time'])) $trade['end_time'] = strtotime($trade['end_time']);
        if (isset($trade['pay_time'])) $trade['pay_time'] = strtotime($trade['pay_time']);
        if (isset($trade['consign_time'])) $trade['consign_time'] = strtotime($trade['consign_time']);
        
        if (isset($trade['promotion_details'])) $trade['promotion_details'] = serialize($trade['promotion_detail']);
        
        if (isset($trade['has_post_fee'])) $trade['has_post_fee'] = Ya::str2bool($trade['has_post_fee']);
        if (isset($trade['is_lgtype'])) $trade['is_lgtype'] = Ya::str2bool($trade['is_lgtype']);
        if (isset($trade['is_brand_sale'])) $trade['is_brand_sale'] = Ya::str2bool($trade['is_brand_sale']);
        if (isset($trade['is_force_wlb'])) $trade['is_force_wlb'] = Ya::str2bool($trade['is_force_wlb']);
        if (isset($trade['has_yfx'])) $trade['has_yfx'] = Ya::str2bool($trade['has_yfx']);
        if (isset($trade['has_buyer_message'])) $trade['has_buyer_message'] = Ya::str2bool($trade['has_buyer_message']);
        if (isset($trade['can_rate'])) $trade['can_rate'] = Ya::str2bool($trade['can_rate']);
        if (isset($trade['buyer_rate'])) $trade['buyer_rate'] = Ya::str2bool($trade['buyer_rate']);
        if (isset($trade['seller_can_rate'])) $trade['seller_can_rate'] = Ya::str2bool($trade['seller_can_rate']);
        if (isset($trade['is_part_consign'])) $trade['is_part_consign'] = Ya::str2bool($trade['is_part_consign']);
        if (isset($trade['seller_rate'])) $trade['seller_rate'] = Ya::str2bool($trade['seller_rate']);
        if (isset($trade['is_3D'])) $trade['is_3D'] = Ya::str2bool($trade['is_3D']);
        
        return $trade;
    }

    /**
     * 转换订单数据格式
     * @param $order 传入订单数据
     */
    public function formatOrder($order) {
        $order['id'] = $order['oid'];
        $order['item_id'] = $order['num_iid'];
        unset($order['oid'], $order['num_iid']);
        if ($order['end_time']) $order['end_time'] = strtotime($order['end_time']);
        if (isset($order['buyer_rate'])) $order['buyer_rate'] = Ya::str2bool($order['buyer_rate']);
        if (isset($order['seller_rate'])) $order['seller_rate'] = Ya::str2bool($order['seller_rate']);
        if (isset($order['is_oversold'])) $order['is_oversold'] = Ya::str2bool($order['is_oversold']);
        
        return $order;
    }

    /**
     * 更改订单备注
     * @param $tid 传入TID
     * @param $memo 备注信息
     * @param $nick 商家昵称  用于主动通知
     */
    public function tradeMemo($tid, $memo) {
        $this->method = 'trade.memo';
        $this->session = $this->_session;
        $this->where = array('tid'=>$tid);

        $update = array('flag'=>$memo['flag']);
        if (array_key_exists('memo', $memo)) {
            $seller_memo = trim($memo['memo']);
            if ($seller_memo) {
                $update['memo'] = "#memo#";
                $this->memo = $seller_memo;
            } else {
                $update['reset'] = 'true';
            }
        }
        $result = $this->update($update)->get();
        
        return isset($result['trade_memo_update_response']) ? true : false;
    }
    
    /**
     * 延长收货时间
     * @param $tid 交易编号
     * @param $delay 延长天数 可选值 ：3, 5, 7, 10
     * @param $nick 商家昵称  用于主动通知
     */
    public function tradeDelay($tid, $delay, $nick = "") {
        $this->method = 'taobao.trade.receivetime.delay';
        $this->session = $this->_session;
        $this->where = "tid=$tid";
        $result = $this->update(array('days'=>$delay))->get();

        return isset($result['trade']) ? true : false;
    }
    
    /**
     * 关闭交易
     * @param $tid 交易编号
     * @param $close 延长天数 可选值 ：3, 5, 7, 10
     * @param $nick 商家昵称  用于主动通知
     */
    public function tradeClose($tid, $close) {
        $this->method = 'taobao.trade.close';
        $this->session = $this->_session;
        $this->where = "tid=$tid";
        $result = $this->update(array('close_reason'=>$close))->get();
    
        return isset($result['trade']) ? true : false;
    }
    
    /**
     * 更新销售属性
     * @param $oid 订单编号
     * @param $sku_id 销售属性编号
     * @param $sku_props 销售属性组合串 格式：p1:v1;p2:v2
     */
    public function tradeSku($oid, $sku_id, $sku_props, $nick = "") {
        $this->method = 'trade.ordersku';
        $this->session = $this->_session;
        $this->where = "oid=$oid";
        $result = $this->update(array('sku_id'=>$sku_id,'sku_props'=>$sku_props))->get();
    
        return isset($result['trade_ordersku_update_response']) ? true : false;
    }
    
    /**
     * 同步淘宝和本地宝贝数据
     * @param $num_iid 传入商品编号
     * @param $fields 需要获取的字段
     * 全部字段：approve_status,auction_point,cid,created,delist_time,desc,ems_fee,express_fee,freight_payer,has_discount,has_invoice,has_showcase,has_warranty,increment,input_pids,input_str,is_3D,is_ex,is_lightning_consignment,is_taobao,is_timing,is_virtual,is_xinpin,item_img,list_time,modified,nick,num,one_station,outer_id,pic_url,post_fee,postage_id,price,product_id,property_alias,props,props_name,sell_promise,seller_cids,sku,stuff_status,sub_stock,template_id,title,type,valid_thru,violation,wap_desc,wap_detail_url,location,is_fenxiao
     */
    public function synItem($num_iid, $fields = "") {
        if (!$fields) {
            $this->fields = "num_iid,approve_status,auction_point,cid,created,delist_time,ems_fee,express_fee,freight_payer,has_discount,has_invoice,has_showcase,has_warranty,increment,input_pids,input_str,is_3D,is_ex,is_lightning_consignment,is_taobao,is_timing,is_virtual,is_xinpin,list_time,modified,nick,num,one_station,outer_id,pic_url,post_fee,postage_id,price,product_id,property_alias,props,sell_promise,seller_cids,sku,stuff_status,sub_stock,template_id,title,type,valid_thru,violation,item_img,prop_img,desc";
        } else {
            $this->fields = $fields . ",num_iid,modified";
        }
        $this->method = 'item';
        $this->session = $this->_session;
        $this->where = array('num_iid'=>$num_iid);
        $resp = $this->select()->get();
        $item = $resp['item_get_response']['item'];

        
        return $this->formatItem($item);
    }

    /**
     * 转换商品数据格式
     * @param $item 传入商品数据
     */
    public function formatItem($item)
    {
        if (isset($item['has_good_return'])) $item['has_good_return'] = Ya::str2bool($item['has_good_return']);
        if (isset($item['has_invoice'])) $item['has_invoice'] = Ya::str2bool($item['has_invoice']);
        if (isset($item['has_showcase'])) $item['has_showcase'] = Ya::str2bool($item['has_showcase']);
        if (isset($item['has_warranty'])) $item['has_warranty'] = Ya::str2bool($item['has_warranty']);
        if (isset($item['is_3D'])) $item['is_3D'] = Ya::str2bool($item['is_3D']);
        if (isset($item['is_ex'])) $item['is_ex'] = Ya::str2bool($item['is_ex']);
        if (isset($item['is_lightning_consignment'])) $item['is_lightning_consignment'] = Ya::str2bool($item['is_lightning_consignment']);
        if (isset($item['is_taobao'])) $item['is_taobao'] = Ya::str2bool($item['is_taobao']);
        if (isset($item['is_timing'])) $item['is_timing'] = Ya::str2bool($item['is_timing']);
        if (isset($item['is_virtual'])) $item['is_virtual'] = Ya::str2bool($item['is_virtual']);
        if (isset($item['is_xinpin'])) $item['is_xinpin'] = Ya::str2bool($item['is_xinpin']);
        if (isset($item['one_station'])) $item['one_station'] = Ya::str2bool($item['one_station']);
        if (isset($item['sell_promise'])) $item['sell_promise'] = Ya::str2bool($item['sell_promise']);
        if (isset($item['violation'])) $item['violation'] = Ya::str2bool($item['violation']);
        
        if ($item['created']) $item['created'] = strtotime($item['created']);
        if ($item['modified']) $item['modified'] = strtotime($item['modified']);
        if ($item['delist_time']) $item['delist_time'] = strtotime($item['delist_time']);
        if ($item['list_time']) $item['list_time'] = strtotime($item['list_time']);
        
        return $item;
    }

    /**
     * 下载多个用户数据
     * @param $nicks 用户昵称。可传入数组或是字符串， 最多40个
     * 所有字段：user_id, nick, sex, buyer_credit, seller_credit, location, created, last_visit, type, consumer_protection
     */
    public function syn_users($nicks, $fields = "") {
        if (is_array($nicks)) $nicks = implode(',', $nicks);  //将数组转换成字符串
        if (!$fields) {
            $fields = "user_id, nick, sex, buyer_credit, seller_credit, location, created, last_visit, type, consumer_protection";
        } else {
            $fields .=", user_id, nick";
        }
        $params['ql'] = "select ".$fields." from users where nicks=$nicks";
        $users = $this->top->tql($params);
        $users = $users['users']['user'];
        return $users;
    }
    
    /**
     * 下载单个用户数据
     * @param $nick 用户昵称
     * 所有字段：user_id, uid, nick, sex, buyer_credit, seller_credit, location, created, last_visit, birthday, type, has_more_pic, item_img_num, item_img_size, prop_img_num, prop_img_size, auto_repost, promoted_type, status, alipay_bind, consumer_protection, alipay_account, alipay_no, avatar, liangpin, sign_food_seller_promise, has_shop, is_lightning_consignment, has_sub_stock, is_golden_seller, vip_info, email, magazine_subscribe, vertical_market, online_gaming
     */
    public function syn_user($nick, $fields = "", $session = SESSIONKEY) {
        if (!$fields) {
            $fields = "user_id, uid, nick, vip_info, promoted_type, location, birthday, sex, type, email, status, auto_repost, alipay_bind, alipay_account, alipay_no, avatar, has_shop, consumer_protection, is_lightning_consignment, liangpin, sign_food_seller_promise, has_sub_stock, is_golden_seller, magazine_subscribe, vertical_market, buyer_credit, seller_credit, last_visit, created";
        } else {
            $fields .=", user_id, nick";
        }
        $params['ql'] = "select ".$fields." from user where nick=$nick";
        $user = $this->top->tql($params, $session);
        
        $this->update_user($user['user']);  //更新用户表
        
        return $user;
    }
    
    /**
     * 下载单个店铺基本信息
     * @param $nick 用户昵称
     * @param $fields 需要获取的字段
     * @param $add 是否更新数据库
     */
    public function syn_Shop($nick, $fields = "", $add = true) {
        if (!$fields) {  //生成需要获取的字段
            $fields = "sid, cid, nick, title, desc, bulletin, pic_path, created, modified, shop_score";
        } else {
            $fields .= ", sid, nick, modified";
        }
        $params['ql'] = "select $fields from shop where nick=$nick";
        $shop = $this->top->tql($params, SESSIONKEY);
        $shop = $shop['shop'];
    
        if ($add) $this->save_shop($shop); //更新数据库
        
        return $shop;
    }
    
    /**
     * 更新店铺基本信息
     * @param $nick 用户昵称
     * @param $fields 需要获取的字段
     * @param $add 是否更新数据库
     */
    public function update_Shop($shop, $add = true) {
        $sql = $this->top->sqls($shop);
        $params['ql'] = "update shop set $sql";
        $shop = $this->top->tql($params, SESSIONKEY);
        $shop = $shop['shop'];
    
        if ($add) $this->save_shop($shop); //更新数据库
    
        return $shop;
    }
    
    /**
     * 橱窗数量
     * 获取卖家店铺剩余橱窗数量
     * 返回 remain_count，all_count，used_count
     */
    public function get_showcase() {
        $params['ql'] = "select shop from shop.remainshowcase";
        $result = $this->top->tql($params, SESSIONKEY);
        
        return $result['shop'];
    }
    
    /**
     * 授权登录
     * 获取授权登录信息
     */
    public function check_container($container) {
        $sign = $this->top->check_sign($container);  //检查授权签名是否合法
        if (!$sign) return false;
        $parameters = $this->top->Parameters($container['top_parameters']);
        if (!is_array($parameters)) return false;
        $nick = $parameters['visitor_nick'];
        $userid = cookie('userid');  //获取用户ID
        
        $User = D('User');
        $User->syn_user($nick, "", $container['top_session']); //同步淘宝用户信息
        $User->where(array('nick'=>$nick, 'userid'=>$userid))->save(array('sessionKey'=>$container['top_session'], "group"=>1));  //绑定淘宝用户到系统
        $this->syn_Shop($nick); //同步淘宝店铺信息
        $this->where(array("nick"=>$nick))->save(array("userid"=>$userid, "title"=>$nick, "status"=>77));  //更新店铺类型
        
        import('COM.Tb.Increment');
        Increment::permit_customer($nick);  //订阅主动通知
        
        return true;
    }
    
    /**
     * 下载退款数据
     * @param $start_modified 修改时间开始
     * @param $end_modified 修改时间结束
     * @param $page_no 页码
     * @param $page_size 每页条数
     */
    public function synRefunds($start_modified = "", $end_modified = "", $page_no = 1, $page_size=100) {
        $this->method = 'refunds.receive';
        $this->session = SESSIONKEY;
        $this->fields = "refund_id, tid, oid, seller_nick, status, good_status, refund_fee, payment, total_fee, desc, reason, company_name, sid, has_good_return, created, modified";
        $where = "page_no=$page_no and page_size=$page_size";
        if ($start_modified && $end_modified) {
            if ($end_modified <= $start_modified) return false;
            $where .= " and start_modified=$start_modified and end_modified=$end_modified"; 
        }
        $this->where = $where;
        $result = $this->select()->get();
        $total = $result['total_results'];

        foreach ($result['refunds_receive_get_response']['refunds']['refund']  as $refund) {
            $this->saveRefund($refund); //入库
        }
        
        return $total;
    }
    
    /**
     * 下载退款留言/凭证列表查询
     * @param $refund_id 退款ID
     * @param $page_no 页数
     * @param $page_size 页码
     */
    public function getMsg($refund_id, $page_no = 1, $page_size=100) {
        $this->session = SESSIONKEY;
        $this->method = 'refund.messages';
        $this->fields = "id,owner_id,owner_role,owner_nick,message_type,pic_urls,content,created";
        $this->where = "refund_id=$refund_id";

        $results = $this->select()->get();

        $messages['total'] = $results['refund_messages_get_response']['total_results'];
        $messages['messages'] = $results['refund_messages_get_response']['refund_messages']['refund_message'];
        
        return $messages;
    }
    
    /**
     * 创建退款留言/凭证
     * @param $refund_id 退款编号
     * @param $content 留言内容。最大长度: 400个字节
     * @param $image 图片（凭证）。类型: JPG,GIF,PNG;最大为: 500K
     */
    public function addMsg($refund_id, $content, $image="") {
        $this->session = SESSIONKEY;
        $this->method = 'refund.messages';
        $this->where = "refund_id=$refund_id";
        
        $results = $this->update($content)->get();

        $id = $results['refund_messages_update_response']['refund_message']['id'];
    
        return $id;
    }
    
    /**
     * 下载单笔退款数据
     * @param $refund_id 退款ID
     * @param $fields 需要获取的字段
     * @param $refund 退款数据
     */
    public function synRefund($refund_id, $fields = "", $nick = "") {
        $this->session = $nick ? D('User')->nick2session($nick) : SESSIONKEY;
        $this->method = 'refund';
        if (!$fields) {  //生成需要获取的字段
            $this->fields = "refund_id,tid,oid,seller_nick,status,good_status,cs_status,advance_status,split_taobao_fee,split_seller_fee,address,refund_fee,payment,total_fee,desc,reason,company_name,sid,has_good_return,good_return_time,created,modified,refund_remind_timeout";
        } else {
            $this->fields = $fields . ",refund_id,modified";
        }
        $this->where = "refund_id=$refund_id";

        $refund = $this->select()->get();

        $this->saveRefund($refund['refund_get_response']['refund']); //入库
        
        return $refund;
    }
    
    /**
    * 搜索评价信息
    * @param $where 评价搜索条件
    * @param $page_no 页码 翻页获取的条数（page_no*page_size）
    * @param $page_size 每页条数 最大值:150
    * @param $fields 需返回的字段
    * 参数：rate_type,role,result,page_no,page_size,start_date,end_date,tid,use_has_next,num_iid
    */
    public function get_Rates($where, $page_no = 1, $page_size = 40, $fields = "") {
        if (!is_array($where)) return false;
        
        $where = $this->top->sqls($where, ' and ');
        $where .=" and page_no=$page_no and page_size=$page_size";
        if (!$fields) {
            $fields = "num_iid, valid_score, tid, oid, role, nick, result, created, rated_nick, item_title, item_price, content, reply";
        } else {
            $fields .=", tid, oid, num_iid, created";
        }
        $params['ql'] = "select $fields from taobao.traderates.get where $where";
        $result = $this->top->tql($params, SESSIONKEY);
        $rates['rates'] = $result['trade_rates']['trade_rate'];
        $rates['total'] = $result['total_results'];
        
        return $rates;
    }

    /**
     * 更新单个系统类目的属性
     */
    public function syn_Prop($cid) {
        $fields = "is_input_prop, pid, parent_pid, parent_vid, name, is_key_prop, is_sale_prop, is_color_prop, is_enum_prop, is_item_prop, must, multi, prop_values, status, sort_order, child_template, is_allow_alias";
        $params['ql'] = "select $fields from itemprops where cid=$cid";
        $props = $this->top->tql($params);
        $props = $props['item_props']['item_prop'];
        
        if ($props) $this->where(array('cid'=>$cid))->delete(); //清空对应类目表数据
        $modified = NOW_TIME;
        foreach ($props as $prop ) {
            $prop = $this->format($prop);
            $prop['cid'] = $cid;
            $prop['modified'] = $modified;
            $this->add($prop);
        }
    }

    /**
     * 获取物流公司信息
     *
     * @param $api 物流查询接口.
     *  0:淘宝；1：金蝶kuaidi001
     */
    public function getCompanies($key = 'name') {
        $field = 'name';
        if ($key <> 'name') $field = $key.',name';
        
        $logistics = $this->field($field)->limit(0,200)->order('listorder DESC, id DESC')->select();
        foreach ($logistics as $v) {
            $logistic[$v[$key]] = $v['name'];
        }
        return $logistic;
    }
    
    /**
     * 查询卖家地址库
     * @param $where 默认地址库
     * no_def:查询非默认地址
     * get_def:查询默认取货地址
     * cancel_def:查询默认退货地址
     * @param $area_id 地址库ID
     */
    public function synAddress($where = "", $area_id = 0) {
        if ($where) $where = " where rdef=$where";
        $params['ql'] = "select area_id from taobao.logistics.address.search $where";
        $addresses = $this->top->tql($params, SESSIONKEY);
        $addresses = $addresses['addresses']['address_result'];
    
        if (!$area_id) return $addresses;
    
        foreach ($addresses as $address) {  //编辑卖家地址库
            if (in_array ($area_id, $address)) return $address;
        }
    }
    
    /**
     * 新增卖家地址库
     * @param $address 地址数据集合
     */
    public function addAddress($address) {
        if ($address['memo']) {
            $params['memo'] = $address['memo'];
            $address['memo'] = "#memo#";
        }
        $params['addr'] = $address['addr'];
        $address['addr'] = "#addr#";
        $field = implode(",", array_keys($address));  //拼装字段列表
        $value = implode(",", array_values($address)); //拼装值列表
    
        $params['ql'] = "insert into logistics.address ($field) values ($value)";
        $result = $this->top->tql($params, SESSIONKEY);
    
        if ($result['address_result']) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * 修改卖家地址库
     * @param $contact_id 地址库ID
     * @param $address 地址数据集合
     */
    public function modifyAddress($contact_id, $address) {
        if ($address['memo']) {
            $params['memo'] = $address['memo'];
            $address['memo'] = "#memo#";
        }
        $params['addr'] = $address['addr'];
        $address['addr'] = "#addr#";
    
        $update = $this->top->sqls($address, ',');
        $params['ql'] = "update taobao.logistics.address.modify set $update where contact_id=".$contact_id;
        $result = $this->top->tql($params, SESSIONKEY);
    
        if ($result['address_result']) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * 删除卖家地址库
     * @param $contact_id 地址库ID
     */
    public function removeAddress($contact_id) {
        $params['ql'] = "delete from taobao.logistics.address.remove where contact_id=".$contact_id;
        $result = $this->top->tql($params, SESSIONKEY);
    
        if ($result['address_result']) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * 获取用户下所有运费模板
     * template_name返回name fee_list
     */
    public function synTemplates($fields = "") {
        $this->method = 'delivery.templates';
        $this->session = SESSIONKEY;
        if (!$fields) {
            $this->fields = "template_id, template_name, assumer ,valuation, query_express, query_ems, query_cod, query_post, supports, created, modified";
        } else {
            $this->fields = $fields;
        }
        $resp = $this->select()->get();

        return $resp['delivery_templates_response']['delivery_templates']['delivery_template'];
    }
    
    /**
     * 获取物流公司信息
     * 表中有kuaidi100的信息，慎用
     */
    public function synCompany() {
        $fields = "id,code,name,reg_mail_no";
        $params['ql'] = "select $fields from logistics.companies";
        $companies = $this->top->tql($params);
    
        return $companies['logistics_companies']['logistics_company'];
    }
    
    /**
     * 物流流转信息查询
     * 表中有kuaidi100的信息，慎用
     */
    public function synTrace($tid, $seller_nick) {
        $where = "tid=$tid and seller_nick=$seller_nick";
        $params['ql'] = "select out_sid from taobao.logistics.trace.search where $where";
        $traces = $this->top->tql($params);
        $traces['trace_list'] = $traces['trace_list']['transit_step_info'];
    
        return $traces;
    }
    
    /**
     * 获取并更新地区表
     * 不需要经常更新
     */
    public function synArea() {
        $fields = "id,type,name,parent_id,zip";
        $params['ql'] = "select $fields from areas";
        $areas = $this->top->tql($params);
        if (!$areas['areas']) return false;
        
        $this->execute("truncate table __TABLE__ "); // 清空areas表
        foreach ($areas['areas']['area'] as $area) {
            $this->add($area);  //更新areas表
        }
        return true;
    }

    /**
     * 同步店铺自有类目
     * @param $nick 昵称
     */
    public function syn_sellercats($nick) {
        $fields = "type, cid, parent_cid, name, pic_url, sort_order";
        $params['ql'] = "select $fields from sellercats.list where nick=$nick";
        $cats= $this->top->tql($params);
        $cats = $cats['seller_cats']['seller_cat'];
        if ($cats)  $this->where(array('model'=>'seller', 'nick'=>$nick))->delete(); //清空对应类目表数据
        $modified = NOW_TIME;
        foreach ($cats as $cat) {
            $cat['modified'] = $modified;
            $cat['model'] = 'seller';
            $cat['nick'] = $nick;
            $this->add($cat);
        }
        
        return true;
    }
    
    /**
     * 更新宝贝类目
     * @param $cid 类目
     */
    public function syn_itemcats($cid) {
        $fields = "cid,parent_cid,name,is_parent,status,sort_order";
        $params['ql'] = "select $fields from itemcats where parent_cid=$cid";
        $cats= $this->top->tql($params);
        $cats = $cats['item_cats']['item_cat'];
        
        if ($cid == 0) $this->where(array('model'=>'item'))->delete(); //清空对应类目表数据
        $modified = NOW_TIME;
        foreach ($cats as $cat) {
            $cat['is_parent'] = str2bool($cat['is_parent']); // 进行布尔值转换
            $cat['modified'] = $modified;
            $cat['model'] = 'item';
            unset($cat['status']);
            $this->add($cat);
            if ($cat['is_parent'] && $cid > 0) $this->syn_itemcats($cat['cid']);
        }
        
        return true;
    }
    
}