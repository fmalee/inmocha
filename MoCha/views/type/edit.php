<?php $form=$this->beginWidget('ActiveForm', array('id'=>'TypeForm')); ?>
<table class="table table-hover table-bordered">
    <?php if(isset($_GET['module']) && !in_array($_GET['module'], array('fake','fund'))): ?>
    <tr>
        <th><?php echo $form->labelEx($model,'parent_id',array('class'=>'control-label')); ?></th>
        <td>
            <?php echo $form->dropDownList($model,'parent_id', array(), array(
                'class'=>'input-small iSelect',
                'data-uri'=>$this->createUrl('childs',array('module'=>$_GET['module'])),
                'data-pid'=>'0',
                'data-selected'=>Type::getArrParent($model->id,1)
            )); ?>
            <?php echo $form->hiddenField($model,'parent_id', array('id'=>'iCatid'));?>
        </td>
    </tr>
    <?php endif;?>
    <tr>
        <th><?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'name', array('class'=>'input-small')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'status',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->radioButtonGroup($model,'status',Type::itemAlias('status')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'list_order',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'list_order', array('class'=>'input-small')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'memo',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textArea($model,'memo', array('row'=>'3','class'=>'input-small')); ?></td>
    </tr>
</table>
<?php if(isset($_GET['module'])) echo $form->hiddenField($model,'module', array('value'=>$_GET['module'])); ?>
<?php $this->endWidget(); ?>
<script>
$(function(){
    //分类联动
    $('.iSelect').cate_select();
});
</script>