<?php
$this->breadcrumbs=array(
    '英摩卡'=>array('...'),
    '卖家中心'=>array('..'),
    '分类管理'=>array('.'),
    Type::itemAlias('module', $module),
);
$this->menu=array(
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 订单管理',
        'url'=>array('/seller/trade'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 退款管理',
        'url'=>array('/seller/refund'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
);
$this->pageTitle= Type::itemAlias('module', $module).' 分类管理';
Ya::registerScript('js','type.js');
?>
                        <!-- content-body -->
                        <div class="content-body">
                            <!-- tab stat -->
                            <div class="box corner-all">
                                <div class="box-header grd-white color-silver-dark corner-top">
                                    <div class="header-control">
                                        <span>
                                            <button class="iDialog btn btn-mini btn-danger" data-uri="<?php echo $this->createUrl('create', array('module'=>$module));?>" data-title="新增分类" data-id="TypeForm" data-callback="oRefresh" data-ok="添加"><i class="icon-plus"></i> 新增分类</button>
                                        </span>
                                    </div>
                                    <span>
                                        <span>分类管理 > <?php echo Type::itemAlias('module', $module);?></span>
                                    </span>
                                </div>
                                <div class="box-body">
                                     <table id="ServiceList" class="iTablelist table table-striped responsive" data-acturi="<?php echo $this->createUrl("aedit");?>">
                                        <thead>
                                            <tr>
                                                <th><input class="iCheckAll" type="checkbox" name="checkall"></th>
                                                <th>分类ID</th>
                                                <th>分类名称</th>
                                                <th>状态</th>
                                                <th>排序</th>
                                                <th>备注</th>
                                                <th>创建时间</th>
                                                <th>修改时间</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                            <?php $this->widget('YListView', array(
                                                'dataProvider'=>$dataProvider,
                                                'id'=>'service',
                                                'itemView'=>'_list',
                                                'itemsTagName'=>'',
                                                'tagName'=>'tbody',
                                                'loadingCssClass'=>'',
                                                'summaryCssClass'=>'pagination pull-left',
                                                'pagerCssClass'=>'pagination pagination-right',
                                                'template'=>"{items}\n<tr><td colspan=\"9\">{summary}{pager}</td></tr>",
                                            )); ?>
                                     </table>
                                </div>
                            </div><!-- /tab stat -->
                        </div><!--/content-body -->
                    </div><!-- /content -->
                </div><!-- /span content -->
