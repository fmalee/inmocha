<!DOCTYPE html>
<html lang="zh-cn">
	<head>
        <meta charset="utf-8">
        <title><?php echo CHtml::encode($this->pageTitle); ?> - <?php echo CHtml::encode(Yii::app()->name); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo CHtml::encode(Yii::app()->params->description); ?>">
        <meta name="author" content="<?php echo CHtml::encode(Yii::app()->params->author); ?>">
        <!-- google font -->
        <link href="http://fonts.googleapis.com/css?family=Aclonica:regular" rel="stylesheet" type="text/css" />
        <!-- styles -->
        <link href="/asset/css/bootstrap.css" rel="stylesheet">
        <link href="/asset/css/bootstrap-responsive.css" rel="stylesheet">
        <!-- default theme -->
        <link id="style-base" href="/asset/css/stilearn.css" rel="stylesheet">
        <link id="style-responsive" href="/asset/css/stilearn-responsive.css" rel="stylesheet">
        <link id="style-helper" href="/asset/css/stilearn-helper.css" rel="stylesheet">
        <!-- usage -->
        <link href="/asset/css/stilearn-icon.css" rel="stylesheet">
        <link href="/asset/css/font-awesome.css" rel="stylesheet">
        <link href="/asset/css/animate.css" rel="stylesheet">
        <link href="/asset/css/uniform.default.css" rel="stylesheet">
        <link href="/asset/css/jquery.jgrowl.css" rel="stylesheet">
        <link href="/asset/css/artDialog/default.css" rel="stylesheet">
        <%=$this->clips['css']%> <!--CSS文件片断-->
        <%=$this->clips['style']%> <!--CSS片断-->
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="/asset/js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- section header -->
        <header class="header">
            <!--nav bar helper-->
            <div class="navbar-helper">
                <div class="row-fluid">
                    <!--panel site-name-->
                    <div class="span2">
                        <div class="panel-sitename">
                            <h2><a href="<?php echo Yii::app()->request->hostInfo; ?>"><span class="color-teal">英</span>摩卡</a></h2>
                        </div>
                    </div>
                    <!--/panel name-->

                    <div class="span6">
                        <!--panel search-->
                        <div class="panel-search">
                            <form class="form-search">
                                <div class="input-icon-append">
                                    <button type="submit" rel="tooltip-bottom" title="search" class="icon"><i class="icofont-search"></i></button>
                                    <input class="input-large search-query grd-white" maxlength="23" placeholder="Search here..." type="text">
                                </div>
                            </form>
                        </div><!--/panel search-->
                    </div>
                    <div class="span4">
                        <!--panel button ext-->
                        <div class="panel-ext">
                            <div class="btn-group">
                                <!--notification-->
                                <a class="btn btn-danger btn-small" data-toggle="dropdown" href="#" title="3 notification">3</a>
                                <ul class="dropdown-menu dropdown-notification">
                                    <li class="dropdown-header grd-white"><a href="#">View All Notifications</a></li>
                                    <li class="new">
                                        <a href="#">
                                            <div class="notification">John Doe commented on a post</div>
                                            <div class="media">
                                                <img class="media-object pull-left" data-src="/asset/js/holder.js/64x64" />
                                                <div class="media-body">
                                                    <h4 class="media-heading">Lorem ipsum <small class="helper-font-small"> john doe</small></h4>
                                                    <p>Raw denim you probably haven't heard of them jean shorts Austin.</p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="new">
                                        <a href="#">
                                            <div class="notification">Request new order</div>
                                            <div class="media">
                                                <img class="media-object pull-left" data-src="/asset/js/holder.js/64x64" />
                                                <div class="media-body">
                                                    <h4 class="media-heading">Tortor dapibus</h4>
                                                    <p>Vegan fanny pack odio cillum wes anderson 8-bit.</p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="new">
                                        <a href="#">
                                            <div class="notification">Request new order</div>
                                            <div class="media">
                                                <img class="media-object pull-left" data-src="/asset/js/holder.js/64x64" />
                                                <div class="media-body">
                                                    <h4 class="media-heading">Lacinia non</h4>
                                                    <p>Messenger bag gentrify pitchfork tattooed craft beer.</p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="notification">John Doe commented on a post</div>
                                            <div class="media">
                                                <img class="media-object pull-left" data-src="/asset/js/holder.js/64x64" />
                                                <div class="media-body">
                                                    <h4 class="media-heading">Lorem ipsum <small class="helper-font-small"> john doe</small></h4>
                                                    <p>Raw denim you probably haven't heard of them jean shorts Austin.</p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="notification">Request new order</div>
                                            <div class="media">
                                                <img class="media-object pull-left" data-src="/asset/js/holder.js/64x64" />
                                                <div class="media-body">
                                                    <h4 class="media-heading">Tortor dapibus</h4>
                                                    <p>Vegan fanny pack odio cillum wes anderson 8-bit.</p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="notification">Request new order</div>
                                            <div class="media">
                                                <img class="media-object pull-left" data-src="/asset/js/holder.js/64x64" />
                                                <div class="media-body">
                                                    <h4 class="media-heading">Lacinia non</h4>
                                                    <p>Messenger bag gentrify pitchfork tattooed craft beer.</p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- <li class="dropdown-footer"><a href=""></a></li> -->
                                </ul><!--notification-->
                            </div>
                            <div class="btn-group">
                                <a class="btn btn-inverse btn-small dropdown-toggle" data-toggle="dropdown" href="#">
                                    Shortcut
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                    <li><a tabindex="-1" href="calendar.html">Calendar</a></li>
                                    <li><a tabindex="-1" href="invoice.html">Invoice</a></li>
                                    <li><a tabindex="-1" href="message.html">Message</a></li>
                                    <li class="divider"></li>
                                    <li class="dropdown-submenu">
                                        <a tabindex="-1" href="#">Sample Page</a>
                                        <ul class="dropdown-menu">
                                            <li><a tabindex="-1" href="pricing.html">Pricing</a></li>
                                            <li><a tabindex="-1" href="bonus-page/resume/index.html">Resume</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu">
                                        <a tabindex="-1" href="#">Error Page</a>
                                        <ul class="dropdown-menu">
                                            <li><a tabindex="-1" href="403.html">Error 403</a></li>
                                            <li><a tabindex="-1" href="404.html">Error 404</a></li>
                                            <li><a tabindex="-1" href="405.html">Error 405</a></li>
                                            <li><a tabindex="-1" href="500.html">Error 500</a></li>
                                            <li><a tabindex="-1" href="503.html">Error 503</a></li>
                                            <li><a tabindex="-1" href="under-construction.html">Under Construction</a></li>
                                            <li><a tabindex="-1" href="coming-son.html">Coming Son</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider"></li>
                                    <li><a tabindex="-1" href="#">Something else here</a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <a class="btn btn-inverse btn-small dropdown-toggle" data-toggle="dropdown" href="#">店铺列表</a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                    <li><a tabindex="-1" href="layout_2columns.html">英摩卡</a></li>
                                    <li><a tabindex="-1" href="layout_grid.html">coromachi</a></li>
                                    <li class="divider"></li>
                                    <li><a tabindex="-1" href="layout_toggleright.html">店铺设置</a></li>
                                    <li><a tabindex="-1" href="layout_toggleright2.html">添加店铺</a></li>
                                </ul>
                            </div>
                            <div class="btn-group user-group">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <img class="corner-all" align="middle" src="/asset/img/user-thumb.jpg" title="John Doe" alt="john doe" /> <!--this for display on PC device-->
                                    <button class="btn btn-small btn-inverse">John Doe</button> <!--this for display on tablet and phone device-->
                                </a>
                                <ul class="dropdown-menu dropdown-user" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <div class="media">
                                            <a class="pull-left" href="#">
                                                <img class="img-circle" src="/asset/img/user.jpg" title="profile" alt="profile" />
                                            </a>
                                            <div class="media-body description">
                                                <p><strong>John Doe</strong></p>
                                                <p class="muted">johndoe@mail.com</p>
                                                <p class="action"><a class="link" href="#">Activity</a> - <a class="link" href="#">Setting</a></p>
                                                <a href="bonus-page/resume/index.html" class="btn btn-primary btn-small btn-block">View Profile</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="dropdown-footer">
                                        <div>
                                            <a class="btn btn-small pull-right" href="<?php echo $this->createUrl('/user/logout');?>">Logout</a>
                                            <a class="btn btn-small" href="#">Add Account</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div><!--panel button ext-->
                    </div>

                </div><!--/row-fluid-->
            </div><!--/nav bar helper-->
        </header>

        <!-- section content -->
        <section class="section">
            <div class="row-fluid">
                <!-- span side-left -->
                <div class="span1">
                    <!--side bar-->
                    <aside class="side-left">
                    <?php $this->widget('zii.widgets.CMenu',array(
                        'items'=>array(
                            array(
                                'label'=>'<span class="helper-font-16"><i class="icofont-dashboard"></i></span> <span class="sidebar-text">桌面</span>',
                                'url'=>array('/'),
                                'linkOptions'=>array('title'=>'桌面'),
                            ),
                            array(
                                'label'=>'<span class="helper-font-16"><i class="icofont-magnet"></i></span> <span class="sidebar-text">订单管理</span>',
                                'url'=>array('/seller/trade'),
                                'visible'=>!Yii::app()->user->isGuest,
                                'linkOptions'=>array('title'=>'订单管理'),
                            ),
                            array(
                                'label'=>'<span class="helper-font-16"><i class="icofont-magnet"></i></span> <span class="sidebar-text">宝贝管理</span>',
                                'url'=>array('/seller/item'),
                                'visible'=>!Yii::app()->user->isGuest,
                                'linkOptions'=>array('title'=>'宝贝管理'),
                            ),
                            array(
                                'label'=>'<span class="helper-font-16"><i class="icofont-magnet"></i></span> <span class="sidebar-text">产品中心</span>',
                                'url'=>array('/seller/product'),
                                'active'=>(Yii::app()->controller->route == 'seller/product/index') ? true : false,
                                'linkOptions'=>array('title'=>'产品中心'),
                            ),
                            array(
                                'label'=>'<span class="helper-font-16"><i class="icofont-bar-chart"></i></span> <span class="sidebar-text">供应商</span>',
                                'url'=>array('/seller/supplier'),
                                'active'=>(Yii::app()->controller->route == 'seller/supplier/index') ? true : false,
                                'linkOptions'=>array('title'=>'供应商管理'),
                            ),
                            array(
                                'label'=>'<span class="helper-font-16"><i class="icofont-bar-chart"></i></span> <span class="sidebar-text">供货管理</span>',
                                'url'=>array('/seller/supply'),
                                'active'=>(Yii::app()->controller->route == 'seller/supply/index') ? true : false,
                                'linkOptions'=>array('title'=>'供货管理'),
                            ),
                            array(
                                'label'=>'<span class="helper-font-16"><i class="icofont-bar-chart"></i></span> <span class="sidebar-text">配货管理</span>',
                                'url'=>array('/seller/invoice'),
                                'active'=>(Yii::app()->controller->route == 'seller/invoice/index') ? true : false,
                                'linkOptions'=>array('title'=>'配货管理'),
                            ),
                            array(
                                'label'=>'<span class="helper-font-16"><i class="icofont-bar-chart"></i></span> <span class="sidebar-text">刷单管理</span>',
                                'url'=>array('/seller/fake'),
                                'active'=>(Yii::app()->controller->route == 'seller/fake/index') ? true : false,
                                'linkOptions'=>array('title'=>'刷单管理'),
                            ),
                            array(
                                'label'=>'<span class="helper-font-16"><i class="icofont-table"></i></span> <span class="sidebar-text">员工管理</span>',
                                'url'=>array('/user/employee'),
                                'active'=>in_array(Yii::app()->controller->route, array('user/employee/index', 'user/department/index')) ? true : false,
                                'linkOptions'=>array('title'=>'员工管理'),
                            ),
                            array(
                                'label'=>'<div class="badge badge-important">5</div><span class="helper-font-16"><i class="icofont-edit"></i></span><span class="sidebar-text">售后服务</span>',
                                'url'=>array('/seller/service'),
                                'active'=>((Yii::app()->controller->id=='service') && in_array(Yii::app()->controller->action->id, array('index', 'fund', 'talk'))) ? true : false,
                                'linkOptions'=>array('title'=>'售后服务'),
                                'items'=>array(
                                    array('label'=>'<i class="icofont-lock"></i><span class="sidebar-text">换货记录</span>', 'url'=>array('/seller/service/niffer'), 'linkOptions'=>array('class'=>'corner-all')),
                                    array('label'=>'<i class="icofont-lock"></i><span class="sidebar-text">补发记录</span>', 'url'=>array('/seller/service/reissue'), 'linkOptions'=>array('class'=>'corner-all')),
                                    array('label'=>'<i class="icofont-lock"></i><span class="sidebar-text">退货记录</span>', 'url'=>array('/seller/service/retrun'), 'linkOptions'=>array('class'=>'corner-all', 'title'=>'退货记录')),
                                    array('label'=>'<i class="icofont-comments"></i><span class="sidebar-text">沟通记录</span>', 'url'=>array('/seller/service/talk'), 'linkOptions'=>array('class'=>'corner-all')),
                                    array('label'=>'<i class="icofont-bookmark"></i><span class="sidebar-text">财务记录</span>', 'url'=>array('/seller/service/fund'), 'linkOptions'=>array('class'=>'corner-all')),
                                )
                            ),
                        ),
                        'firstItemCssClass'=>'first',
                        'htmlOptions'=>array('class'=>'sidebar'),
                        'submenuHtmlOptions'=>array('class'=>'sub-sidebar-form corner-top shadow-white'),
                        'encodeLabel'=>false
                    )); ?>
                    </aside><!--/side bar -->
                </div><!-- span side-left -->
                <%=$content%>
				<?php //$this->renderPartial('//side/sideright',array('model'=>$list,)); ?>

            </div><!--/row-->
        </section><!--/section content-->

        <!-- section footer -->
        <footer>
            <a rel="to-top" href="#top"><i class="icofont-circle-arrow-up"></i></a>
        </footer>
        <div id="iAjaxloading"></div>

        <!-- javascript
        ================================================== -->
        <script src="/asset/js/widgets.js" type="text/javascript" ></script>
        <script src="/asset/js/jquery.js"></script>
        <script src="/asset/js/jquery-migrate-1.1.1.js"></script>
        <script src="/asset/js/jquery-ui.min.js"></script>
        <script src="/asset/js/bootstrap.js"></script>
        <script src="/asset/js/jquery.uniform.js"></script>
        <script src="/asset/js/jquery.form.js"></script>
        <script src="/asset/js/jquery.artDialog.min.js"></script>
        <script src="/asset/js/jquery.jgrowl.min.js"></script>
        <!-- required stilearn template js, for full feature-->
        <script src="/asset/js/holder.js"></script>
        <script src="/asset/js/stilearn-base.js"></script>
        <%=$this->clips['js']%><!--js文件片断-->
        <%=$this->clips['script']%><!--js片断-->
    </body>
</html>