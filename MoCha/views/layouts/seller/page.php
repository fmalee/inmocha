<!DOCTYPE html>
<html lang="zh-cn">
	<head>
        <meta charset="utf-8">
        <title><?php echo CHtml::encode($this->pageTitle); ?> - <?php echo CHtml::encode(Yii::app()->name); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo CHtml::encode(Yii::app()->params->description); ?>">
        <meta name="author" content="<?php echo CHtml::encode(Yii::app()->params->author); ?>">
        <!-- google font -->
        <link href="http://fonts.googleapis.com/css?family=Aclonica:regular" rel="stylesheet" type="text/css" />        
        <!-- styles -->
        <link href="/asset/css/bootstrap.css" rel="stylesheet">
        <link href="/asset/css/bootstrap-responsive.css" rel="stylesheet">
        <!-- default theme -->
        <link id="style-base" href="/asset/css/stilearn.css" rel="stylesheet">
        <link id="style-responsive" href="/asset/css/stilearn-responsive.css" rel="stylesheet">
        <link id="style-helper" href="/asset/css/stilearn-helper.css" rel="stylesheet">
        <!-- usage -->
        <link href="/asset/css/stilearn-icon.css" rel="stylesheet">
        <link href="/asset/css/font-awesome.css" rel="stylesheet">
        <link href="/asset/css/animate.css" rel="stylesheet">
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="/asset/js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- section header -->
        <header class="header" data-spy="affix" data-offset-top="0">
            <!--nav bar helper-->
            <div class="navbar-helper">
                <div class="row-fluid">
                    <!--panel site-name-->
                    <div class="span2">
                        <div class="panel-sitename">
                            <h2><a href="<?php echo Yii::app()->request->hostInfo; ?>"><span class="color-teal">英</span>摩卡</a></h2>
                        </div>
                    </div>
                    <!--/panel name-->
                </div>
            </div><!--/nav bar helper-->
        </header>
        <?php echo $content; ?>
        <!-- javascript
        ================================================== -->
        <script src="/asset/js/widgets.js" type="text/javascript"></script>
        <script src="/asset/js/jquery.js"></script>
        <script src="/asset/js/jquery-migrate-1.1.1.js"></script>
        <script src="/asset/js/bootstrap.js"></script>
    </body>
</html>