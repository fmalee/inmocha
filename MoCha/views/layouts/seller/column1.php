<%$this->beginContent('//layouts/seller/main')%>
                <!-- span content -->
                <div class="span11">
                    <!-- content -->
                    <div class="content content-large">
                        <!-- content-breadcrumb -->
                        <div class="content-breadcrumb">
                            <!--breadcrumb-nav-->
                            <?php
                                $menu = array(
                                    array('itemOptions'=>array('class'=>'divider'),'label'=>''),
                                    array(
                                        'label'=>'<i class="icofont-cogs"></i> 快捷菜单 <i class="icofont-caret-down"></i>',
                                        'url'=>array('#'),
                                        'itemOptions'=>array('class'=>'btn-group'),
                                        'linkOptions'=>array('class'=>'btn btn-small btn-link dropdown-toggle', 'data-toggle'=>'dropdown'),
                                        'items'=>array(
                                            array('label'=>'New Arrivals', 'url'=>array('product/new', 'tag'=>'new'), 'linkOptions'=>array('class'=>'scroll')),
                                            array('itemOptions'=>array('class'=>'divider'),'label'=>''),
                                            array('label'=>'Most Popular', 'url'=>array('product/index', 'tag'=>'popular'), 'linkOptions'=>array('class'=>'scroll')),
                                    )),
                                    array('itemOptions'=>array('class'=>'divider'),'label'=>''),
                                    array(
                                        'label'=>'<i class="icofont-user"></i> 待发货 <span class="color-red">(+34)</span>',
                                        'url'=>array('#'),
                                        'itemOptions'=>array('class'=>'btn-group'),
                                        'linkOptions'=>array('class'=>'btn btn-small btn-link'
                                    )),
                                );
                                $menu = count($this->menu) ? array_merge($this->menu, $menu) : $menu;
                                $this->widget('zii.widgets.CMenu',array(
                                    'items'=>$menu,
                                    'htmlOptions'=>array('class'=>'breadcrumb-nav pull-right'),
                                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
                                    'encodeLabel'=>false,
                                    'activateParents'=>true
                            )); ?>
                            <!--/breadcrumb-nav-->
                            <!--breadcrumb.由于中文字体的关系，导航右边会错位-->
                            <?php if(isset($this->breadcrumbs)):
                                $this->widget('zii.widgets.CBreadcrumbs', array(
                                    'links'=>$this->breadcrumbs,
                                    'homeLink'=>CHtml::link('<i class="icofont-home"></i>',Yii::app()->homeUrl),
                                    'tagName'=>'ul',
                                    'separator'=>'',
                                    'activeLinkTemplate'=>'<li><a href="{url}">{label}</a> <span class="divider">&rsaquo;</span></li>',
                                    'inactiveLinkTemplate'=>'<li class="active">{label}</li>',
                                    'htmlOptions'=>array ('class'=>'breadcrumb')
                                )); ?>
                            <?php endif; ?>
                            <!--/breadcrumb-->
                        </div><!-- /content-breadcrumb -->
                        <%=$content%>
<%$this->endContent()%>