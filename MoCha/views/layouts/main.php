<!DOCTYPE html>
<html dir="ltr" class="ltr" lang="zh-cn">
<head>
    <!-- Mobile viewport optimized: h5bp.com/viewport -->
    <meta name="viewport" content="width=device-width">
    <meta charset="UTF-8" />
    <title><%=CHtml::encode($this->pageTitle)%> - <%=CHtml::encode(Yii::app()->name)%></title>
    <meta name="description" content="<%=CHtml::encode(Yii::app()->params->description)%>" />
    <meta name="keywords" content="<%=CHtml::encode(Yii::app()->params->keywords)%>" />
    <meta name="author" content="<%=CHtml::encode(Yii::app()->params->author)%>">
    <link href="<%=Ya::C('imgPath')%>/cart.png" rel="icon" />
    <link href="<%=Ya::C('cssPath')%>/bootstrap.css" rel="stylesheet" />
    <link href="<%=Ya::C('cssPath')%>/stylesheet.css" rel="stylesheet" />
    <link href="<%=Ya::C('jsPath')%>/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
    <link href="<%=Ya::C('cssPath')%>/animation.css" rel="stylesheet" />
    <link href="<%=Ya::C('cssPath')%>/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=Ya::C('cssPath')%>/font.css" rel="stylesheet" />
    <link href="<%=Ya::C('cssPath')%>/pavcarousel.css" rel="stylesheet" />
    <link href="<%=Ya::C('cssPath')%>/pavnewsletter.css" rel="stylesheet" />
    <link href="<%=Ya::C('cssPath')%>/pavmegamenu/style.css" rel="stylesheet" />
    <%=$this->clips['css']%> <!--CSS文件片断-->
    <script type="text/javascript" src="<%=Ya::C('jsPath')%>/jquery/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="<%=Ya::C('jsPath')%>/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="<%=Ya::C('jsPath')%>/jquery/ui/external/jquery.cookie.js"></script>
    <script type="text/javascript" src="<%=Ya::C('jsPath')%>/common.js"></script>
    <script type="text/javascript" src="<%=Ya::C('jsPath')%>/sidebareffects.js"></script>
    <script type="text/javascript" src="<%=Ya::C('jsPath')%>/jquery/bootstrap/bootstrap.min.js"></script>
    <%=$this->clips['js']%> <!--js片断-->
    <%=$this->clips['style']%> <!--CSS片断-->
    <!--[if lt IE 9]>
    <script src="<%=Ya::C('jsPath')%>/html5.js"></script>
    <script src="<%=Ya::C('jsPath')%>/respond.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<%=Ya::C('cssPath')%>/ie8.css" />
    <![endif]-->
</head>
<body id="offcanvas-container" class="offcanvas-container layout-fullwidth fs12 page-category">
<!--<body id="offcanvas-container" class="offcanvas-container layout-<$layoutMode> fs<$themeConfig['fontsize']> <$helper->getPageClass()> <$helper->getParam('body_pattern','')>">-->
<section id="page" class="offcanvas-pusher" role="main">
<section id="topbar">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-4 hidden-sm hidden-xs">
                <div class="welcome pull-left">
                    享受更多服务，请 <a href="">登录</a> 或是 <a href="/register">注册</a>.
                </div>
                <div id="currency" class="currency pull-left">
                    <div class="currency-wrapper">
                        <div class="btn-group">
                            <button type="button" class="dropdown-toggle hidden-sm hidden-xs" data-toggle="dropdown"><span>$</span><span class="fa fa-sort-asc"></span></button>
                            <button type="button" class="dropdown-toggle hidden-lg hidden-md button" data-toggle="dropdown"><span>$US Dollar</span><span class="fa fa-sort-asc"></span></button>
                            <div class="dropdown-menu dropdown">
                                <form action="http://demopavothemes.com/pav_digital_store/index.php?route=module/currency" method="post" enctype="multipart/form-data">
                                    <div class="currency-item">
                                        <a class="list-item" title="Euro" onclick="$('input[name=\'currency_code\']').attr('value', 'EUR'); $(this).parent().parent().submit();">€ Euro</a>
                                        <a class="list-item" title="Pound Sterling" onclick="$('input[name=\'currency_code\']').attr('value', 'GBP'); $(this).parent().parent().submit();">£ Pound Sterling</a>
                                        <a class="list-item" title="US Dollar"><b>$US Dollar</b></a>
                                        <input type="hidden" name="currency_code" value="" />
                                        <input type="hidden" name="redirect" value="http://demopavothemes.com/pav_digital_store/index.php?route=account/login" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div> 
                <div id="language" class="language pull-left">
                    <div class="language-wrapper">  
                        <div class="btn-group">
                            <button type="button" class="dropdown-toggle hidden-sm hidden-xs" data-toggle="dropdown"><span>中文</span><span class="fa fa-sort-asc"></span></button>
                            <button type="button" class="dropdown-toggle hidden-lg hidden-md button" data-toggle="dropdown">
                                <span><img src="image/flags/gb.png" alt="English" title="English" />English</span><span class="fa fa-sort-asc"></span></button>
                            <div class="dropdown-menu dropdown">
                                <form action="http://demopavothemes.com/pav_digital_store/index.php?route=module/language" method="post" enctype="multipart/form-data">
                                    <div class="language-item">
                                        <span class="list-item" onclick="$('input[name=\'language_code\']').attr('value', 'en'); $(this).parent().parent().submit();">
                                            <img src="image/flags/gb.png" alt="English" title="English" onclick="$('input[name=\'language_code\']').attr('value', 'en'); $(this).parent().parent().submit();" class="item-symbol" />
                                            <span class="item-name">English</span>
                                        </span>
                                        <span class="list-item" onclick="$('input[name=\'language_code\']').attr('value', 'fr'); $(this).parent().parent().submit();">
                                            <img src="image/flags/fr.png" alt="French" title="French" onclick="$('input[name=\'language_code\']').attr('value', 'fr'); $(this).parent().parent().submit();" class="item-symbol" />
                                            <span class="item-name">French</span>
                                        </span>
                                        <span class="list-item" onclick="$('input[name=\'language_code\']').attr('value', 'fa'); $(this).parent().parent().submit();">
                                            <img src="image/flags/gq.png" alt="Persian" title="Persian" onclick="$('input[name=\'language_code\']').attr('value', 'fa'); $(this).parent().parent().submit();" class="item-symbol" />
                                            <span class="item-name">Persian</span>
                                        </span>
                                        <input type="hidden" name="language_code" value="" />
                                        <input type="hidden" name="redirect" value="http://demopavothemes.com/pav_digital_store/index.php?route=account/login" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-8 hidden-sm hidden-xs">
                <ul class="links pull-left">
                    <!-- <li><a class="first" href="http://demopavothemes.com/pav_digital_store/index.php?route=common/home">Home</a></li> -->
                    <li><a class="wishlist" href="http://demopavothemes.com/pav_digital_store/index.php?route=account/wishlist" id="wishlist-total"><span class="fa fa-heart"></span>收藏夹(0)</a></li>
                    <li><a class="account" href="http://demopavothemes.com/pav_digital_store/index.php?route=account/account"><span class="fa fa-user"></span>会员中心</a></li>
                    <li><a class="shoppingcart" href="http://demopavothemes.com/pav_digital_store/index.php?route=checkout/cart"><span class="fa fa-shopping-cart"></span>购物车</a></li>
                    <li><a class="last checkout" href="http://demopavothemes.com/pav_digital_store/index.php?route=checkout/checkout"><span class="fa fa-file"></span>结算</a></li>
                </ul>
                <div id="cart" class="clearfix pull-right">
                    <div class="heading">
                        <span class="fa fa-shopping-cart"></span>
                        <!-- <h4></h4> -->
                        <a><span id="cart-total">0 件 - $0.00</span></a>
                    </div>
                    <div class="content">
                        <div class="empty">您还没有购买任何物品!</div>
                    </div>
                </div>
            </div>
            <div class="show-mobile hidden-lg hidden-md pull-right">
                <div class="quick-user pull-left">
                    <div class="quickaccess-toggle"><i class="fa fa-user"></i></div>
                    <div class="inner-toggle">
                        <div class="login links">Welcome visitor you can <a href="">login</a> or <a href="">create an account</a>.</div>
                    </div>
                </div>
                <div class="quick-access pull-left">
                    <div class="quickaccess-toggle"><i class="fa fa-list"></i></div>
                    <div class="inner-toggle">
                        <ul class="links pull-left">
                            <!-- <li><a class="first" href="http://demopavothemes.com/pav_digital_store/index.php?route=common/home">Home</a></li> -->
                            <li><a class="wishlist" href="http://demopavothemes.com/pav_digital_store/index.php?route=account/wishlist" id="mobile-wishlist-total"><span class="fa fa-heart"></span>收藏夹 (0)</a></li>
                            <li><a class="account" href="http://demopavothemes.com/pav_digital_store/index.php?route=account/account"><span class="fa fa-user"></span>会员中心</a></li>
                            <li><a class="shoppingcart" href="http://demopavothemes.com/pav_digital_store/index.php?route=checkout/cart"><span class="fa fa-shopping-cart"></span>购物车</a></li>
                            <li><a class="last checkout" href="http://demopavothemes.com/pav_digital_store/index.php?route=checkout/checkout"><span class="fa fa-file"></span>结算</a></li>
                        </ul>
                    </div>
                </div>
                <div id="search_mobile" class="search pull-left">
                    <div class="quickaccess-toggle"><i class="fa fa-search"></i></div>
                    <div class="inner-toggle">
                        <div id="search-mobile"><input type="text" name="search1" placeholder="搜索" value="" /><span class="button-search"></span></div>
                    </div>
                </div>
                <div class="support pull-left">
                    <div class="quickaccess-toggle"><i class="fa fa-sun-o"></i></div>
                    <div class="inner-toggle">
                        <div class="currency pull-left">
                            <div class="currency-wrapper"> 
                                <div class="btn-group">
                                    <button type="button" class="dropdown-toggle hidden-sm hidden-xs" data-toggle="dropdown"><span>$</span><span class="fa fa-sort-asc"></span></button>
                                    <button type="button" class="dropdown-toggle hidden-lg hidden-md button" data-toggle="dropdown"><span>$ US Dollar</span><span class="fa fa-sort-asc"></span></button>
                                    <div class="dropdown-menu dropdown">
                                        <form action="http://demopavothemes.com/pav_digital_store/index.php?route=module/currency" method="post" enctype="multipart/form-data">
                                            <div class="currency-item">
                                                <a class="list-item" title="Euro" onclick="$('input[name=\'currency_code\']').attr('value', 'EUR'); $(this).parent().parent().submit();">€ Euro</a>
                                                <a class="list-item" title="Pound Sterling" onclick="$('input[name=\'currency_code\']').attr('value', 'GBP'); $(this).parent().parent().submit();">£ Pound Sterling</a>
                                                <a class="list-item" title="US Dollar"><b>$US Dollar</b></a>
                                                <input type="hidden" name="currency_code" value="" />
                                                <input type="hidden" name="redirect" value="http://demopavothemes.com/pav_digital_store/index.php?route=account/login" />
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="language pull-left">
                            <div class="language-wrapper">
                                <div class="btn-group">
                                    <button type="button" class="dropdown-toggle hidden-sm hidden-xs" data-toggle="dropdown"><span>中文</span><span class="fa fa-sort-asc"></span></button>
                                    <button type="button" class="dropdown-toggle hidden-lg hidden-md button" data-toggle="dropdown"><span><img src="image/flags/gb.png" alt="English" title="English" />English</span><span class="fa fa-sort-asc"></span></button>
                                    <div class="dropdown-menu dropdown">
                                        <form action="http://demopavothemes.com/pav_digital_store/index.php?route=module/language" method="post" enctype="multipart/form-data">
                                            <div class="language-item">
                                                <span class="list-item" onclick="$('input[name=\'language_code\']').attr('value', 'en'); $(this).parent().parent().submit();">
                                                    <img src="image/flags/gb.png" alt="English" title="English" onclick="$('input[name=\'language_code\']').attr('value', 'en'); $(this).parent().parent().submit();" class="item-symbol" />

                                                    <span class="item-name">English</span>
                                                </span>
                                                <span class="list-item" onclick="$('input[name=\'language_code\']').attr('value', 'fr'); $(this).parent().parent().submit();">
                                                    <img src="image/flags/fr.png" alt="French" title="French" onclick="$('input[name=\'language_code\']').attr('value', 'fr'); $(this).parent().parent().submit();" class="item-symbol" />
                                                    <span class="item-name">French</span>
                                                </span>
                                                <span class="list-item" onclick="$('input[name=\'language_code\']').attr('value', 'fa'); $(this).parent().parent().submit();">
                                                    <img src="image/flags/gq.png" alt="Persian" title="Persian" onclick="$('input[name=\'language_code\']').attr('value', 'fa'); $(this).parent().parent().submit();" class="item-symbol" />
                                                    <span class="item-name">Persian</span>
                                                </span>
                                                <input type="hidden" name="language_code" value="" />
                                                <input type="hidden" name="redirect" value="http://demopavothemes.com/pav_digital_store/index.php?route=account/login" />
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="header">
    <div class="container">
        <div class="header-wrap">
            <div class="pull-left inner">
                <div id="logo"><a href=""><img src="/Statics/images/logo.png" title="Pav DigitalStore - Responsive Opencart Theme" alt="Pav DigitalStore - Responsive Opencart Theme" /></a></div>
            </div>
            <div id="pav-mainnav" class="pull-right inner">
                <div class="mainnav-wrap">
                    <div class="navbar navbar-inverse ">
                        <nav class="pav-megamenu" role="navigation"> 
                            <div class="pav-megamenu">
                                <div class="navbar">
                                    <div id="mainmenutop" class="megamenu" role="navigation">
                                        <div class="navbar-header">
                                            <a href="javascript:;" data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a>
                                            <div class="collapse navbar-collapse navbar-ex1-collapse">
                                                <ul class="nav navbar-nav megamenu">
                                                    <li class="home" ><a href="?route=common/home"><span class="menu-title">首页</span></a></li>
                                                    <li class="parent dropdown pav-parrent" >
                                                        <a class="dropdown-toggle" data-toggle="dropdown" href=""><span class="menu-title">上装</span><b class="caret"></b></a>
                                                        <div class="dropdown-menu level1"  style="width:720px" >
                                                            <div class="dropdown-menu-inner">
                                                                <div class="row">
                                                                    <div class="mega-col col-xs-12 col-sm-12 col-md-3" data-type="menu" >
                                                                        <div class="mega-col-inner">
                                                                            <ul>
                                                                                <li class="parent dropdown-submenu " >
                                                                                    <a class="dropdown-toggle" data-toggle="dropdown" href=""><span class="menu-title">Watches</span><b class="caret"></b></a>
                                                                                    <div class="dropdown-menu level2"  >
                                                                                        <div class="dropdown-menu-inner">
                                                                                            <div class="row">
                                                                                                <div class="mega-col col-xs-12 col-sm-12 col-md-12" data-type="menu" >
                                                                                                    <div class="mega-col-inner">
                                                                                                        <ul>
                                                                                                            <li class=" " >
                                                                                                                <div class="menu-content">
                                                                                                                    <ul>
                                                                                                                        <li><a href="index.php?route=product/category&amp;path=20">裙装</a></li>
                                                                                                                        <li><a href="index.php?route=product/category&amp;path=18">Chicken </a></li>
                                                                                                                        <li><a href="index.php?route=product/category&amp;path=25">Salads</a></li>
                                                                                                                        <li><a href="index.php?route=product/category&amp;path=17">Drinks</a></li>
                                                                                                                        <li><a href="index.php?route=product/category&amp;path=24">Cake</a></li>
                                                                                                                    </ul>
                                                                                                                </div>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                                <li class=" " ><a href=""><span class="menu-title">Aliquam</span></a></li>
                                                                                <li class=" " ><a href=""><span class="menu-title">Claritas</span></a></li>
                                                                                <li class=" " ><a href=""><span class="menu-title">Consectetuer</span></a></li>
                                                                                <li class=" " ><a href=""><span class="menu-title">Hendrerit</span></a></li>
                                                                                <li class=" " ><a href=""><span class="menu-title">Litterarum</span></a></li>
                                                                                <li class=" " ><a href=""><span class="menu-title">Macs</span></a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mega-col col-xs-12 col-sm-12 col-md-5"  >
                                                                        <div class="mega-col-inner">
                                                                            <div class="pavo-widget" id="wid-3">
                                                                                <div class="menu-title">特别推荐</div>
                                                                                <div class="widget-product-list">
                                                                                    <div class="widget-inner">
                                                                                        <div class="w-product clearfix">
                                                                                            <div class="image"><a href=""><img src="/Statics/images/demo/img-produce04-60x65.jpg" alt="Fusce a congue" /></a></div>
                                                                                            <div class="name"><a href="">Fusce a congue</a></div>
                                                                                            <div class="price"><span class="price-old">$119.50</span> <span class="price-new">$96.00</span></div>
                                                                                            <div class="rating"><img src="/Statics/images/stars-4.png" alt="text_reviews" /></div>
                                                                                            <!--<div class="cart"><input type="button" value="Add to Cart" onclick="addToCart('30');" class="button" /></div> -->
                                                                                        </div>
                                                                                        <div class="w-product clearfix">
                                                                                            <div class="image"><a href="d=42"><img src="/Statics/images/demo/img-produce09-60x65.jpg" alt="Lorem ipsum dolor" /></a></div>
                                                                                            <div class="name"><a href="d=42">Lorem ipsum dolor</a></div>
                                                                                            <div class="price"><span class="price-old">$119.50</span> <span class="price-new">$107.75</span></div>
                                                                                            <div class="rating"><img src="/Statics/images/stars-4.png" alt="text_reviews" /></div>
                                                                                            <!--<div class="cart"><input type="button" value="Add to Cart" onclick="addToCart('42');" class="button" /></div> -->
                                                                                        </div>
                                                                                        <div class="w-product clearfix">
                                                                                            <div class="image"><a href="d=36"><img src="/Statics/images/demo/img-produce01-60x65.jpg" alt="Mollicitudin lobortis" /></a></div>
                                                                                            <div class="name"><a href="d=36">Mollicitudin lobortis</a></div>
                                                                                            <div class="price"><span class="price-old">$119.50</span> <span class="price-new">$113.63</span></div>
                                                                                            <div class="rating"><img src="/Statics/images/stars-2.png" alt="text_reviews" /></div>
                                                                                            <!--<div class="cart"><input type="button" value="Add to Cart" onclick="addToCart('36');" class="button" /></div> -->
                                                                                        </div>
                                                                                        <div class="w-product clearfix">
                                                                                            <div class="image"><a href="d=45"><img src="/Statics/images/demo/img-produce08-60x65.jpg" alt="Mollis eleifend" /></a></div>
                                                                                            <div class="name"><a href="d=45">Mollis eleifend</a></div>
                                                                                            <div class="price"><span class="price-old">$2,000.00</span> <span class="price-new">$70.00</span></div>
                                                                                            <div class="rating"><img src="/Statics/images/stars-3.png" alt="text_reviews" /></div>
                                                                                            <!--<div class="cart"><input type="button" value="Add to Cart" onclick="addToCart('45');" class="button" /></div> -->
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mega-col col-xs-12 col-sm-12 col-md-4"  >
                                                                        <div class="mega-col-inner">
                                                                            <div class="pavo-widget" id="wid-10">
                                                                                <div class="menu-title">最新上架</div>
                                                                                <div class="widget-product-list">
                                                                                    <div class="widget-inner">
                                                                                        <div class="w-product clearfix">
                                                                                            <div class="image"><a href="d=48"><img src="/Statics/images/demo/img-produce14-200x236.jpg" alt="Posuere lacus" /></a></div>
                                                                                            <div class="name"><a href="d=48">Posuere lacus</a></div>
                                                                                            <div class="price"><span class="price-old">$119.50</span> <span class="price-new">$101.88</span></div>
                                                                                            <div class="rating"><img src="/Statics/images/stars-4.png" alt="text_reviews" /></div>
                                                                                            <!--<div class="cart"><input type="button" value="Add to Cart" onclick="addToCart('48');" class="button" /></div> -->
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class=" parent dropdown ">
                                                        <a href="category&amp;path=18" class="dropdown-toggle" data-toggle="dropdown"><span class="menu-title">下装</span><b class="caret"></b></a>
                                                        <div class="dropdown-menu" style="width:550px">
                                                            <div class="dropdown-menu-inner">
                                                                <div class="row">
                                                                    <div class="mega-col col-xs-12 col-sm-12 col-md-4 ">
                                                                        <div class="mega-col-inner">
                                                                            <div class="pavo-widget" id="wid-11">
                                                                                <div class="menu-title">Category</div>
                                                                                <div class="widget-html">
                                                                                    <div class="widget-inner">
                                                                                        <ul>
                                                                                            <li><a href="index.php?route=product/category&amp;path=20">Breakfast</a></li>
                                                                                            <li><a href="index.php?route=product/category&amp;path=18">Chicken</a></li>
                                                                                            <li><a href="index.php?route=product/category&amp;path=25">Salads</a></li>
                                                                                            <li><a href="index.php?route=product/category&amp;path=17">Drinks</a></li>
                                                                                            <li><a href="index.php?route=product/category&amp;path=24">Cake</a></li>
                                                                                            <li><a href="index.php?route=product/category&amp;path=34">Burgers</a></li>
                                                                                            <li><a href="index.php?route=product/category&amp;path=20_26">PC</a></li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mega-col col-xs-12 col-sm-12 col-md-6 ">
                                                                        <div class="mega-col-inner"></div>
                                                                    </div>
                                                                    <div class="mega-col col-xs-12 col-sm-12 col-md-8 ">
                                                                        <div class="mega-col-inner">
                                                                            <div class="pavo-widget" id="wid-1">
                                                                                <div class="menu-title">Video Opencart Installation</div>
                                                                                <div class="widget-html">
                                                                                    <div class="widget-inner">
                                                                                        <iframe width="320" height="220" src="/embed/fNEepYl3LAk"></iframe>
                                                                                        <p>The Quickstart Package consists on a complete Opencart! + Template + Various Extensions + Sample...</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class=""><a href="category&amp;path=20"><span class="menu-title">客服中心</span></a></li>
                                                    <li class=""><a href="category&amp;path=34"><span class="menu-title">资讯</span></a></li>
                                                    <li class=""><a href="?route=pavblog/blogs"><span class="menu-title">公告</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>

                <div id="search" class="hidden-sm hidden-xs">
                    <input class="pull-right" type="text" name="search" placeholder="Search" value="" />
                    <span class="button-search"></span>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="sys-notification">
    <div class="container">
        <!--<div class="warning">这是一个错误提示<img src="/Statics/images/close.png" alt="" class="close" /></div>-->
        <div id="notification"></div>
    </div>
</section>
<section id="columns" class="offcanvas-siderbars">
    <div class="container">
        <div class="row visible-xs">
            <div class="container">
                <div class="offcanvas-sidebars-buttons">
                    <button type="button" data-for="column-left" class="pull-left btn btn-danger"><i class="glyphicon glyphicon-indent-left"></i>左侧内容</button>
                    <button type="button" data-for="column-right" class="pull-right btn btn-danger">右侧内容<i class="glyphicon glyphicon-indent-right"></i></button>
                </div>
            </div>
        </div>
        <div class="row"><!--header-->
            <div id="breadcrumb">
                <ol class="breadcrumb">
                    <li><a href="http://demopavothemes.com/pav_digital_store/index.php?route=common/home">首页</a></li>
                    <li><a href="http://demopavothemes.com/pav_digital_store/index.php?route=account/account">会员中心</a></li>
                    <li><a href="http://demopavothemes.com/pav_digital_store/index.php?route=account/login">登录</a></li>
                </ol>
            </div>
            <%=$content%>
        </div><!--footer-->
    </div>
</section>
<section id="pav-mass-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box pav-custom  ">
                    <div class="box-content">
                        <p class="hidden-xs"><img alt="" src="/Statics/images/banner-mass-bottom.jpg" style="border-radius: 5px;" /></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id="pavcarousel6" class="box carousel slide pavcarousel hidden-xs">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div style="width:14.2857142857%; float: left">
                                <div class="item-inner"><a href="1=7"><img src="/Statics/images/demo/blackberry-140x58.png" alt="logo" /></a></div>
                            </div>
                            <div style="width:14.2857142857%; float: left">
                                <div class="item-inner"><a href="1=5"><img src="/Statics/images/demo/samsung-140x58.png" alt="logo" /></a></div>
                            </div>
                            <div style="width:14.2857142857%; float: left">
                                <div class="item-inner"><a href="1=8"><img src="/Statics/images/demo/sony-140x58.png" alt="logo" /></a></div>
                            </div>
                            <div style="width:14.2857142857%; float: left">
                                <div class="item-inner"><a href="1=9"><img src="/Statics/images/demo/lenovo-140x58.png" alt="logo" /></a></div>
                            </div>
                            <div style="width:14.2857142857%; float: left">
                                <div class="item-inner"><a href="1=10"><img src="/Statics/images/demo/htc-140x58.png" alt="logo" /></a></div>
                            </div>
                            <div style="width:14.2857142857%; float: left">
                                <div class="item-inner"><a href="1=6"><img src="/Statics/images/demo/apple-140x58.png" alt="logo" /></a></div>
                            </div>
                            <div style="width:14.2857142857%; float: left">
                                <div class="item-inner"><a href="1=7"><img src="/Statics/images/demo/nokia-140x58.png" alt="logo" /></a></div>
                            </div>
                        </div>
                        <div class="item ">
                            <div style="width:14.2857142857%; float: left">
                                <div class="item-inner"><a href="1=7"><img src="/Statics/images/demo/lenovo-140x58.png" alt="logo" /></a></div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control left" href="#pavcarousel6" data-slide="prev">&lsaquo;</a>
                    <a class="carousel-control right" href="#pavcarousel6" data-slide="next">&rsaquo;</a>
                </div>
                <script type="text/javascript">
                    <!--
                     $('#pavcarousel6').carousel({interval:false});
                    -->
                </script>
            </div>
        </div>
    </div>
</section>

<section id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="container-inner">
                <div class="row">
                    <div class="col-lg-12 col-md-12  col-md-6">
                        <div class="box pav-custom  social">
                            <div class="box-content">
                                <p>
                                    <a href="#" title=""><i class="fa fa-pinterest">&nbsp;</i>Pinterest</a><a href="#" title=""><i class="fa fa-facebook">&nbsp;</i>Facebook</a>
                                    <a href="#" title=""><i class="fa fa-google-plus">&nbsp;</i>Google+</a><a href="#" title=""><i class="fa fa-twitter">&nbsp;</i>Twitter</a>
                                    <a href="#" title=""><i class="fa fa-youtube">&nbsp;</i>Youtube</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-center">
        <div class="container">
            <div class="container-inner">
                <div class="row">
                    <div class="col-lg-3 col-md-3 column">
                        <div class="box pav-custom  ">
                            <div class="box-heading"><span>服务中心</span></div>
                            <div class="box-content">
                                <ul class="list">
                                    <li><a href="index.php?route=information/contact">Contact Us</a></li>
                                    <li><a href="index.php?route=account/return/insert">Returns</a></li>
                                    <li><a href="index.php?route=information/sitemap">Site Map</a></li>
                                    <li><a href="index.php?route=product/manufacturer">Brands</a></li>
                                    <li><a href="index.php?route=account/voucher">Gift Vouchers</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 column">
                        <div class="box pav-custom  ">
                            <div class="box-heading"><span>我的账户</span></div>
                            <div class="box-content">
                                <ul class="list">
                                    <li><a href="index.php?route=account/account">My Account</a></li>
                                    <li><a href="index.php?route=account/order">Order History</a></li>
                                    <li><a href="index.php?route=account/wishlist">Wish List</a></li>
                                    <li><a href="index.php?route=account/newsletter">Newsletter</a></li>
                                    <li><a href="index.php?route=product/special">Specials</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 column">
                        <div class="box pav-custom  contact-us">
                            <div class="box-heading"><span>联系我们</span></div>
                            <div class="box-content">
                                <p>If your question is not answered there, please use one of the contact methods below.</p>
                                <ul>
                                    <li><span class="fa fa-map-marker">&nbsp;</span>Proin gravida, velit auctor aliquet</li>
                                    <li><span class="fa fa-mobile-phone">&nbsp;</span>Phone: +01 888 (000) 1234</li>
                                    <li><span class="fa fa-envelope">&nbsp;</span>Email: pavdigitaltore@gmail.com</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 column">
                        <div class=" box newsletter_block" id="newsletter_footer_center0">
                            <div class="box-heading"><span>Newsletter</span></div>
                            <div class="description"><p>Get the word out. Share this page with your frien...</div>
                            <div class="block_content">
                                <form id="formNewLestter" method="post" action="http://demopavothemes.com/pav_digital_store/index.php?route=module/pavnewsletter/subscribe">
                                    <p>
                                        <input type="text" class="inputNew" onblur="javascript:if(this.value=='')this.value='Your email address';" onfocus="javascript:if(this.value=='Your email address')this.value='';" value="Your email address" size="18" name="email">
                                        <input type="submit" name="submitNewsletter" class="button_mini btn btn-theme-primary" value="Ok">
                                        <input type="hidden" value="1" name="action">
                                    </p>
                                </form>
                            </div>
                        </div>
                        <script type="text/javascript">
                            $('#formNewLestter').on('submit', function() {
                                var email = $('.inputNew').val();
                                    $(".success_inline, .warning_inline, .error").remove();
                                if(!isValidEmailAddress(email)) {
                                        $('.inputNew').before("<div class=\"error alert alert-danger\">Email is not valid!</div>");
                                    $('.inputNew').focus();
                                    return false;
                                }
                                    var url = "http://demopavothemes.com/pav_digital_store/index.php?route=module/pavnewsletter/subscribe";
                                    $.ajax({
                                        type: "post",
                                        url: url,
                                        data: $("#formNewLestter").serialize(),
                                        dataType: 'json',
                                        success: function(json)
                                        {
                                            $(".success_inline, .warning_inline, .error").remove();
                                            if (json['error']) {
                                                $('.inputNew').before("<div class=\"warning_inline alert alert-danger\">"+json['error']+"</div>");
                                            }
                                            if (json['success']) {
                                                $('.inputNew').before("<div class=\"success_inline alert alert-success\">"+json['success']+"</div>");
                                            }
                                        }
                                    });
                                    return false;
                                });

                            function isValidEmailAddress(emailAddress) {
                                var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                                return pattern.test(emailAddress);
                            }
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="powered">
        <div class="container">
            <div class="container-inner">
                <div class="copyright pull-left">
                    Powered By <a href="http://www.opencart.com">OpenCart</a><br />
                    Pav DigitalStore - Responsive Opencart Theme &copy; 2014. Design By <a href="http://www.pavothemes.com" title="pavothemes - opencart themes clubs">PavoThemes</a>
                </div>
                <div class="paypal pull-right"><p><img alt="" src="/Statics/images/paypal.png" /></p></div>
            </div>
        </div>
    </div>
</section>
</section>
<%=$this->clips['script']%> <!--js片断-->
</body>
</html>