<?php
/* @var $this ArticleController */
/* @var $model Article */
?>

<?php
$this->breadcrumbs=array(
	'Articles'=>array('index'),
	$model->name,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Article', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Article', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Update Article', 'url'=>array('update', 'id'=>$model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Delete Article', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Article', 'url'=>array('admin')),
);
?>
<h1><%=$model->title%></h1>
<div class="category-info clearfix">
                        <div class="image">
                            <img src="/Statics/images/demo/img-blog5-900x350w.jpg" alt="Mac" />
                        </div>
                        <div class="description">
                            <p><%=$model->description%></p>
                            <ul class="list-inline">
                                <li><span class="fa fa-clock-o"><%=$model->getAttributeLabel('modified')%>:</span> <%=CHtml::encode($model->modified)%></li>
                                <li><span class="fa fa-pencil"><%=$model->getAttributeLabel('user_id')%>:</span> <%=CHtml::encode($model->user_id)%></li>
                                <li><span class="fa fa-eye"><%=$model->getAttributeLabel('view')%>:</span> <%=CHtml::encode($model->view)%></li>
                                <li><span class="fa fa-comments"><%=$model->getAttributeLabel('comment')%>:</span> <%=CHtml::encode($model->comment)%></li>
                            </ul>
                            
                        </div>
                    </div>
<div class="category-list clearfix">

                        <%=$model->content%>

                    </div>

<clip:sidebar>

<div class="box category">
  <div class="box-heading"><span>Categories</span></div>
  <div class="box-content">
    <ul class="box-category list">
            <li class="haschild">
                <a href="http://demopavothemes.com/pav_digital_store/index.php?route=product/category&amp;path=20" class="active">Mac <span class="badge pull-right">11</span></a>
                        <ul>
                              <li>
                        <a href="http://demopavothemes.com/pav_digital_store/index.php?route=product/category&amp;path=20_26"> PC <span class="badge pull-right">0</span></a>
                      </li>
                              <li>
                        <a href="http://demopavothemes.com/pav_digital_store/index.php?route=product/category&amp;path=20_27"> Mac <span class="badge pull-right">1</span></a>
                      </li>
                  </ul>
              </li>
            <li class="haschild">
                <a href="http://demopavothemes.com/pav_digital_store/index.php?route=product/category&amp;path=18">iPhone <span class="badge pull-right">8</span></a>
                        <ul>
                              <li>
                        <a href="http://demopavothemes.com/pav_digital_store/index.php?route=product/category&amp;path=18_46"> Macs <span class="badge pull-right">2</span></a>
                      </li>
                              <li>
                        <a href="http://demopavothemes.com/pav_digital_store/index.php?route=product/category&amp;path=18_45"> Windows <span class="badge pull-right">0</span></a>
                      </li>
                  </ul>
              </li>
            <li class="haschild">
                <a href="http://demopavothemes.com/pav_digital_store/index.php?route=product/category&amp;path=25">iPad <span class="badge pull-right">15</span></a>
                        <ul>
                              <li>
                        <a href="http://demopavothemes.com/pav_digital_store/index.php?route=product/category&amp;path=25_29"> Mice and Trackballs <span class="badge pull-right">1</span></a>
                      </li>
                              <li>
                        <a href="http://demopavothemes.com/pav_digital_store/index.php?route=product/category&amp;path=25_28"> Monitors <span class="badge pull-right">2</span></a>
                      </li>
                              <li>
                        <a href="http://demopavothemes.com/pav_digital_store/index.php?route=product/category&amp;path=25_30"> Printers <span class="badge pull-right">0</span></a>
                      </li>
                              <li>
                        <a href="http://demopavothemes.com/pav_digital_store/index.php?route=product/category&amp;path=25_31"> Scanners <span class="badge pull-right">2</span></a>
                      </li>
                              <li>
                        <a href="http://demopavothemes.com/pav_digital_store/index.php?route=product/category&amp;path=25_32"> Web Cameras <span class="badge pull-right">0</span></a>
                      </li>
                  </ul>
              </li>
            <li class="">
                <a href="http://demopavothemes.com/pav_digital_store/index.php?route=product/category&amp;path=17">Drinks <span class="badge pull-right">3</span></a>
                      </li>
            <li class="">
                <a href="http://demopavothemes.com/pav_digital_store/index.php?route=product/category&amp;path=24">Cake <span class="badge pull-right">4</span></a>
                      </li>
            <li class="">
                <a href="http://demopavothemes.com/pav_digital_store/index.php?route=product/category&amp;path=34">iPod <span class="badge pull-right">8</span></a>
                      </li>
          </ul>
  </div>
</div>
</clip:sidebar>
<clip:css>
    <link href="<%=Ya::C('cssPath')%>/pavblog.css" rel="stylesheet" />
</clip:css>
<clip:js>
    <script type="text/javascript" src="<%=Ya::C('jsPath')%>/jquery/pavblog_script.js"></script>
</clip:js>