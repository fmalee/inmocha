<?php
/* @var $this ArticleController */
/* @var $model Article */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'user_id',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>40)); ?>
    <?php echo $form->textFieldControlGroup($model,'title',array('maxlength'=>80)); ?>
    <?php echo $form->textFieldControlGroup($model,'category_id',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'description',array('maxlength'=>140)); ?>
    <?php echo $form->textAreaControlGroup($model,'content',array('rows'=>6)); ?>
    <?php echo $form->textFieldControlGroup($model,'position'); ?>
    <?php echo $form->textFieldControlGroup($model,'link_id',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'cover_id',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'display'); ?>
    <?php echo $form->textFieldControlGroup($model,'attach'); ?>
    <?php echo $form->textFieldControlGroup($model,'view',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'comment',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'level'); ?>
    <?php echo $form->textFieldControlGroup($model,'status'); ?>
    <?php echo $form->textFieldControlGroup($model,'template',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'created',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'modified',array('maxlength'=>10)); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Search',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
