<?php
/* @var $this ArticleController */
/* @var $model Article */
?>

<?php
$this->breadcrumbs=array(
	'Articles'=>array('index'),
	'Create',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Article', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Article', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Create','Article') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>