<?php
/* @var $this ArticleController */
/* @var $model Article */
?>

<?php
$this->breadcrumbs=array(
	'Articles'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Article', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Article', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View Article', 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Article', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Update','Article '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>