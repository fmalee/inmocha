<?php
$this->pageTitle='Error';

$this->layout='//layouts/seller/page';
?>
<?php $this->beginContent('//layouts/page'); ?>
<!-- section content -->
        <section class="section">
            <div class="container">
                <div class="error-page">
                    <h1 class="error-code color-red">Error <?php echo $code; ?></h1>
                    <p class="error-description muted"><?php echo CHtml::encode($message); ?></p>
                    <form>
                        <div class="control-group">
                            <div class="input-append input-icon-prepend">
                                <div class="add-on">
                                    <a title="search" style="" class="icon"><i class="icofont-search"></i></a>
                                    <input class="input-xxlarge animated grd-white" type="text" placeholder="试试搜索一下其他内容吧" />
                                </div>
                                <input type="submit" class="btn" value="搜索" />
                            </div>
                        </div>
                    </form>
                    <a href="/" class="btn btn-small btn-primary"><i class="icofont-arrow-left"></i> 回到首页</a>
                    <a href="" target="_blank" class="btn btn-small btn-success">重新试一下 <i class="icofont-arrow-right"></i></a>
                </div>
            </div>
        </section>

        <!-- section footer -->
<?php $this->endContent(); ?>