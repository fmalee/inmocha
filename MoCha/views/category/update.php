<?php
/* @var $this CategoryController */
/* @var $model Category */
?>

<?php
$this->breadcrumbs=array(
	'Categories'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Category', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Category', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View Category', 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Category', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Update','Category '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>