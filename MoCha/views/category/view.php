<?php
/* @var $this CategoryController */
/* @var $model Category */
?>

<?php
$this->breadcrumbs=array(
	'Categories'=>array('index'),
	$model->name,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Category', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Category', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Update Category', 'url'=>array('update', 'id'=>$model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Delete Category', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Category', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('View','Category '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'title',
		'parent_id',
		'arr_parent',
		'icon',
		'status',
		'module',
		'page_size',
		'meta_title',
		'keywords',
		'description',
		'template_index',
		'template_lists',
		'template_detail',
		'template_edit',
		'allow_publish',
		'display',
		'reply',
		'check',
		'sort',
		'created',
		'modified',
	),
)); ?>