<?php
/* @var $this CategoryController */
/* @var $model Category */
?>

<?php
$this->breadcrumbs=array(
	'Categories'=>array('index'),
	'Create',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Category', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Category', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Create','Category') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>