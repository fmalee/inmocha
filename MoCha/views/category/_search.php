<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>30)); ?>
    <?php echo $form->textFieldControlGroup($model,'title',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'parent_id',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'arr_parent',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'icon',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'status'); ?>
    <?php echo $form->textFieldControlGroup($model,'module'); ?>
    <?php echo $form->textFieldControlGroup($model,'page_size'); ?>
    <?php echo $form->textFieldControlGroup($model,'meta_title',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'keywords',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'description',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'template_index',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'template_lists',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'template_detail',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'template_edit',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'allow_publish'); ?>
    <?php echo $form->textFieldControlGroup($model,'display'); ?>
    <?php echo $form->textFieldControlGroup($model,'reply'); ?>
    <?php echo $form->textFieldControlGroup($model,'check'); ?>
    <?php echo $form->textFieldControlGroup($model,'sort',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'created',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'modified',array('maxlength'=>10)); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Search',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
