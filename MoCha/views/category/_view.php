                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="blog-item">
                                            <img src="http://demopavothemes.com/pav_digital_store/image/cache/data/pavblog/img-blog5-900x350w.jpg" title="Donec tellus lorem elit tristique " alt="Donec tellus lorem elit tristique ">
                                            <div class="row">
                                                <div class="blog-meta col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                                    <ul>
                                                        <li class="created"><span class="fa fa-clock-o"><?php echo CHtml::encode($data->getAttributeLabel('name')); ?> :<?php echo CHtml::encode($data->name); ?></li>
                                                        <li class="author"><span class="fa fa-pencil">Write By:</span>admin</li>
                                                        <li class="publishin"><span class="fa fa-user">Published In:</span><a href="category&amp;id=21" title="Vestibulum massa">Vestibulum massa</a></li>

                                                        <li class="hits">
                                                            <span class="fa fa-eye">Hits:</span>79</li>
                                                        <li class="comment_count">
                                                            <span class="fa fa-comments">Comment:</span>0</li>
                                                    </ul>
                                                </div>
                                                <div class="blog-body col-lg-9 col-md-9 col-sm-8 col-xs-12">

                                                    <div class="blog-header clearfix">
                                                        <h3 class="blog-title"><%=CHtml::link(CHtml::encode($data->name),array('view','id'=>$data->id))%></h3>
                                                    </div>
                                                    <div class="description">
                                                        <p><?php echo CHtml::encode($data->title); ?></p>
                                                    </div>
                                                    <div class="blog-readmore">
                                                        <a href="blog&amp;id=11" class="button">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>