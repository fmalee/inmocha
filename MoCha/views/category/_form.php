<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'category-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>30)); ?>
    <?php echo $form->textFieldControlGroup($model,'title',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'parent_id',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'arr_parent',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'icon',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'status'); ?>
    <?php echo $form->textFieldControlGroup($model,'module'); ?>
    <?php echo $form->textFieldControlGroup($model,'page_size'); ?>
    <?php echo $form->textFieldControlGroup($model,'meta_title',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'keywords',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'description',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'template_index',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'template_lists',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'template_detail',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'template_edit',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'allow_publish'); ?>
    <?php echo $form->textFieldControlGroup($model,'display'); ?>
    <?php echo $form->textFieldControlGroup($model,'reply'); ?>
    <?php echo $form->textFieldControlGroup($model,'check'); ?>
    <?php echo $form->textFieldControlGroup($model,'sort',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'created',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'modified',array('maxlength'=>10)); ?>

    <?php echo BsHtml::submitButton('Submit', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>

<?php $this->endWidget(); ?>
