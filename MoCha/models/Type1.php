<?php

class Type extends CActiveRecord
{
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	
	private static $_types = array();
	/**
	 * Type表的字段:
	 * @var integer $id
	 * @var integer $user_id
	 * @var string $module
	 * @var string $name
	 * @var integer $status
	 * @var string $memo
	 * @var integer $list_order
	 * @var string $creater
     * @var integer $created
     * @var integer $modified
	 */

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{type}}';
	}

	public function rules()
	{
		return array(
			array('status, name', 'required'),
			array('module', 'moduleFilter'),
			array('name', 'length', 'max'=>20),
			array('memo', 'length', 'max'=>100),
			array('list_order', 'numerical', 'integerOnly'=>true),
			//array('created, modified', 'default', 'value' => time(), 'setOnEmpty' => true, 'on' => 'insert'),
			//array('tag, contact, phone, wangwang, qq, qq_group, www, data_packet, memo', 'safe'),
			//array('id, author_id, group_id, createtime, title, message', 'safe', 'on'=>'search'), //特定环境不检查规则
		);
	}

	public function moduleFilter($attribute,$params)
	{
        $modules = array('fake','supplier');
        if(!in_array($this->module, $modules))
        	$this->addError($attribute, 'Invalid request. Please do not repeat this request again.!');
	}

	public function relations()
	{
        return array(
			//'supplier' => array(self::BELONGS_TO, 'Supplier', '', 'on'=>'type.id=supplier.type'),
			'supplierCount' => array(self::STAT, 'Supplier', 'type_id'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'id' => Ya::t('Type ID'),
			'module'=>Ya::t('Module'),
			'status'=>Ya::t('Status'),
			'name'=>Ya::t('Name'),
			'memo'=>Ya::t('Memo'),
			'list_order' => Ya::t('List Order'),
			'created' => Ya::t('Created Time'),
			'modified' => Ya::t('Modified Time'),
		);
	}
	
	public function scopes()
    {
        return array(
        	'belong'=>array(
                'condition'=>'type.user_id=:user_id',
                'params'=>array(':user_id'=>Yii::app()->user->id),
            ),
            'active'=>array(
                'condition'=>'type.status='.self::STATUS_ACTIVE,
            ),
            'notactive'=>array(
                'condition'=>'type.status='.self::STATUS_NOACTIVE,
            ),
            'listorder'=>array(
                'order'=>'type.list_order ASC',
            ),
            'fake'=>array(
            	'condition'=>"type.module='fake'",
            ),
            'notsafe'=>array(
            	'alias'=>'type',
            	'select' => 'id, module, name, status, memo, list_order, created, modified',
            ),
            'forcheck'=>array(
            	'alias'=>'type',
            	'select' => 'id, name',
            ),
        );
    }

	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->created=time();
				$this->user_id=Yii::app()->user->id;
			}
			$this->modified=time();
			return true;
		}
		else
			return false;
	}

	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'status' => array(
				self::STATUS_ACTIVE => Ya::t('Enabled'),
				self::STATUS_NOACTIVE => Ya::t('Disabled'),
			),
			'module' => array(
				'fake' => Ya::t('Fake'),
				'supplier' => Ya::t('Supplier'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

	public static function Lists($module,$code=NULL)
	{
		if(!isset(self::$_types[$module]))
		{
			$items = self::model()->belong()->$module()->active()->listorder()->forcheck()->findAll();
			if($items)
			{
				foreach ($items as $item)
					self::$_types[$module][$item->id] = $item->name;
			}else
				return false;
		}
		if (isset($code))
			return isset(self::$_types[$module][$code]) ? self::$_types[$module][$code] : false;
		else
			return isset(self::$_types[$module]) ? self::$_types[$module] : false;
	}

    public function formatHtml($model)
    {
    	//if(isset($model['module'])) $model['module'] = self::itemAlias('module', $model['module']);
    	if(isset($model['created'])) $model['created'] = date('Y-m-d H:i:s', $model['created']);
    	if(isset($model['modified'])) $model['modified'] = date('Y-m-d H:i:s', $model['modified']);

    	return $model;
    }
}