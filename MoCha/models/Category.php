<?php

/**
 * This is the model class for table "{{category}}".
 *
 * The followings are the available columns in table '{{category}}':
 * @property string $id
 * @property string $name
 * @property string $title
 * @property string $parent_id
 * @property string $arr_parent
 * @property string $icon
 * @property integer $status
 * @property integer $module
 * @property integer $page_size
 * @property string $meta_title
 * @property string $keywords
 * @property string $description
 * @property string $template_index
 * @property string $template_lists
 * @property string $template_detail
 * @property string $template_edit
 * @property integer $allow_publish
 * @property integer $display
 * @property integer $reply
 * @property integer $check
 * @property string $sort
 * @property string $created
 * @property string $modified
 */
class Category extends CActiveRecord
{
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	//MODUEL表名称
	const MODULE_ARTICLE = 1;

	private static $_categorys = array();
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, title, template_index, template_lists, template_detail, template_edit', 'required'),
			array('status, module, page_size, allow_publish, display, reply, check', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>30),
			array('title, meta_title', 'length', 'max'=>50),
			array('parent_id, icon, sort, created, modified', 'length', 'max'=>10),
			array('arr_parent, keywords, description', 'length', 'max'=>255),
			array('template_index, template_lists, template_detail, template_edit', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, title, parent_id, arr_parent, icon, status, module, page_size, meta_title, keywords, description, template_index, template_lists, template_detail, template_edit, allow_publish, display, reply, check, sort, created, modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => '分类ID',
			'name' => '标志',
			'title' => '标题',
			'parent_id' => '上级分类',
			'arr_parent' => '上级分类集合',
			'icon' => '分类图标',
			'status' => '数据状态',
			'module' => '关联模块',
			'page_size' => '列表每页行数',
			'meta_title' => 'SEO的网页标题',
			'keywords' => '关键字',
			'description' => '描述',
			'template_index' => '频道页模板',
			'template_lists' => '列表页模板',
			'template_detail' => '详情页模板',
			'template_edit' => '编辑页模板',
			'allow_publish' => '是否允许发布内容',
			'display' => '可见性',
			'reply' => '是否允许回复',
			'check' => '发布的文章是否需要审核',
			'sort' => '排序（同级有效）',
			'created' => '创建时间',
			'modified' => '更新时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('parent_id',$this->parent_id,true);
		$criteria->compare('arr_parent',$this->arr_parent,true);
		$criteria->compare('icon',$this->icon,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('module',$this->module);
		$criteria->compare('page_size',$this->page_size);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('keywords',$this->keywords,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('template_index',$this->template_index,true);
		$criteria->compare('template_lists',$this->template_lists,true);
		$criteria->compare('template_detail',$this->template_detail,true);
		$criteria->compare('template_edit',$this->template_edit,true);
		$criteria->compare('allow_publish',$this->allow_publish);
		$criteria->compare('display',$this->display);
		$criteria->compare('reply',$this->reply);
		$criteria->compare('check',$this->check);
		$criteria->compare('sort',$this->sort,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function scopes()
    {
        return array(
            'active'=>array(
                'condition'=>'category.status='.self::STATUS_ACTIVE,
            ),
            'notactive'=>array(
                'condition'=>'category.status='.self::STATUS_NOACTIVE,
            ),
            'notsafe'=>array(
            	'alias'=>'category',
            	//'select' => 'id, module, name, parent_id, arr_parent, memo, status, list_order, created, modified',
            ),
            'listorder'=>array(
                'order'=>'category.sort ASC',
            ),
            'forcheck'=>array(
            	'alias'=>'category',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'category',
            	'select' => 'id, name, parent_id',
            ),
        );
    }

	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->created=time();
			}
			$this->modified=time();

			if(isset($this->parent_id))
				$this->generateArrParent(); //生成arr_parent
			return true;
		}
		else
			return false;
	}

	protected function beforeDelete()
	{
		if(parent::beforeDelete())
		{
			return $this->hasChild();
		}
		else
			return false;
	}

	/**
	 * 获取模块名称
	 */
	public function getModuleName()
    {
        return $this->itemAlias('module', $this->module);
    }

	/**
     * 是否有子类目
     */
    public function hasChild()
    {
		$criteria=new CDbCriteria(array(
			'select'=>'id,arr_parent',
			'condition'=>'parent_id=:id',
			'params'=>array(':id'=>$this->id),
		));
		$model = $this->find($criteria);

		if($model)
			return false;
		else
			return true;

    }

    /**
     * 生成arr_parent
     */
    public function generateArrParent()
    {
		if($this->parent_id)
		{
			$criteria=new CDbCriteria(array(
				'select'=>'id,arr_parent',
				'condition'=>'id=:parent_id',
				'params'=>array(':parent_id'=>$this->parent_id),
			));
			$model = $this->find($criteria);

			if($model->arr_parent)
				$this->arr_parent = $model->arr_parent . $model->id . '|';
			else
				$this->arr_parent = $model->id . '|';
		}
		else
			$this->arr_parent = 0;
    }

    /**
     * 获取arr_parent
     */
    public static function getArrParent($id, $select = 0)
    {
		$criteria=new CDbCriteria(array(
			'select'=>'id,arr_parent',
			'condition'=>'id=:id',
			'params'=>array(':id'=>$id),
		));
		$model = Type::model()->find($criteria);

		if($select)
			return $model->arr_parent;
		else
			return $model->arr_parent.$id.'|';
    }

	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'status' => array(
				self::STATUS_ACTIVE => Ya::t('Enabled'),
				self::STATUS_NOACTIVE => Ya::t('Disabled'),
			),
			'module' => array(
				self::MODULE_FAKE => Ya::t('Fake'),
				self::MODULE_DEPARTMENT => Ya::t('Department'),
				self::MODULE_SUPPLIER => Ya::t('Supplier'),
				self::MODULE_PRODUCT => Ya::t('Product'),
				self::MODULE_FUND => Ya::t('财务记录'),
				self::MODULE_TALK => Ya::t('沟通记录'),
				self::MODULE_RETRUN => Ya::t('退货记录'),
				self::MODULE_REISSUE => Ya::t('补发记录'),
				self::MODULE_NIFFER => Ya::t('换货记录'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

	public static function Lists($module,$code=NULL)
	{
		if(!isset(self::$_categorys[$module]))
		{
			$condition = "module='$module'";
			$items = self::model()->belong()->active()->listorder()->simple()->findAll($condition);
			if($items)
			{
				foreach ($items as $item)
					self::$_categorys[$module][$item->id] = $item->name;
			}else
				return false;
		}
		if (isset($code))
			return isset(self::$_categorys[$module][$code]) ? self::$_categorys[$module][$code] : false;
		else
			return isset(self::$_categorys[$module]) ? self::$_categorys[$module] : false;
	}

    public static function formatHtml($model)
    {
    	if(isset($model['created'])) $model['created'] = date('Y-m-d', $model['created']);
    	if(isset($model['modified'])) $model['modified'] = date('Y-m-d', $model['modified']);

    	return $model;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
