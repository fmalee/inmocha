<?php

class Type extends CActiveRecord
{
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;

	//MODUEL表名称
	const MODULE_FAKE = 'fake';
	const MODULE_DEPARTMENT = 'department';
	const MODULE_SUPPLIER = 'supplier';
	const MODULE_PRODUCT = 'product';
	const MODULE_FUND = 'fund';
	const MODULE_TALK = 'talk';
	const MODULE_REISSUE = 'reissue';
	const MODULE_NIFFER = 'niffer';
	const MODULE_RETRUN = 'return';

	private static $_types = array();
	/**
	 * Type表的字段:
	 * @var integer $id
	 * @var integer $user_id
	 * @var string $module
	 * @var string $name
	 * @var string $parent_id
	 * @var integer $arr_parent
	 * @var integer $status
	 * @var string $memo
	 * @var integer $list_order
     * @var integer $created
     * @var integer $modified
	 */

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{type}}';
	}

	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>20),
			array('parent_id, status, list_order', 'numerical', 'integerOnly'=>true),
			array('memo', 'length', 'max'=>100),
			array('module', 'moduleFilter'), //模块过滤
		);
	}

	public function moduleFilter($attribute,$params)
	{
        if(!self::itemAlias('module', $this->module))
        	$this->addError($attribute, 'Invalid request. Please do not repeat this request again.!');
	}

	public function relations()
	{
        return array(
			'employee' => array(self::HAS_MANY, 'Employee', 'department_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id'=>Ya::t("Type ID"),
			'name'=>Ya::t("Name"),
			'status'=>Ya::t("Status"),
			'parent_id'=>Ya::t("Type"),
			'list_order'=>Ya::t("List Order"),
			'memo'=>Ya::t("Memo"),
			'created' => Ya::t("Created Time"),
			'modified' => Ya::t("Modified Time"),
		);
	}

	public function scopes()
    {
        return array(
            'belong'=>array(
            	'condition'=>'type.user_id=:user_id',
            	'params'=>array(':user_id'=>Yii::app()->user->id),
            ),
            'active'=>array(
                'condition'=>'type.status='.self::STATUS_ACTIVE,
            ),
            'notactive'=>array(
                'condition'=>'type.status='.self::STATUS_NOACTIVE,
            ),
            'notsafe'=>array(
            	'alias'=>'type',
            	'select' => 'id, module, name, parent_id, arr_parent, memo, status, list_order, created, modified',
            ),
            'listorder'=>array(
                'order'=>'type.list_order ASC',
            ),
            'forcheck'=>array(
            	'alias'=>'type',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'type',
            	'select' => 'id, name, parent_id',
            ),
        );
    }

	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->user_id=Yii::app()->user->id;
				$this->created=time();
			}
			$this->modified=time();

			if(isset($this->parent_id))
				$this->generateArrParent(); //生成arr_parent
			return true;
		}
		else
			return false;
	}

	protected function beforeDelete()
	{
		if(parent::beforeDelete())
		{
			return $this->hasChild();
		}
		else
			return false;
	}
	/**
	 * 获取模块名称
	 */
	public function getModuleName()
    {
        return $this->itemAlias('module', $this->module);
    }

	/**
     * 是否有子类目
     */
    public function hasChild()
    {
		$criteria=new CDbCriteria(array(
			'select'=>'id,arr_parent',
			'condition'=>'parent_id=:id',
			'params'=>array(':id'=>$this->id),
		));
		$model = $this->find($criteria);

		if($model)
			return false;
		else
			return true;

    }

    /**
     * 生成arr_parent
     */
    public function generateArrParent()
    {
		if($this->parent_id)
		{
			$criteria=new CDbCriteria(array(
				'select'=>'id,arr_parent',
				'condition'=>'id=:parent_id',
				'params'=>array(':parent_id'=>$this->parent_id),
			));
			$model = $this->find($criteria);

			if($model->arr_parent)
				$this->arr_parent = $model->arr_parent . $model->id . '|';
			else
				$this->arr_parent = $model->id . '|';
		}
		else
			$this->arr_parent = 0;
    }

    /**
     * 获取arr_parent
     */
    public static function getArrParent($id, $select = 0)
    {
		$criteria=new CDbCriteria(array(
			'select'=>'id,arr_parent',
			'condition'=>'id=:id',
			'params'=>array(':id'=>$id),
		));
		$model = Type::model()->find($criteria);

		if($select)
			return $model->arr_parent;
		else
			return $model->arr_parent.$id.'|';
    }

	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'status' => array(
				self::STATUS_ACTIVE => Ya::t('Enabled'),
				self::STATUS_NOACTIVE => Ya::t('Disabled'),
			),
			'module' => array(
				self::MODULE_FAKE => Ya::t('Fake'),
				self::MODULE_DEPARTMENT => Ya::t('Department'),
				self::MODULE_SUPPLIER => Ya::t('Supplier'),
				self::MODULE_PRODUCT => Ya::t('Product'),
				self::MODULE_FUND => Ya::t('财务记录'),
				self::MODULE_TALK => Ya::t('沟通记录'),
				self::MODULE_RETRUN => Ya::t('退货记录'),
				self::MODULE_REISSUE => Ya::t('补发记录'),
				self::MODULE_NIFFER => Ya::t('换货记录'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

	public static function Lists($module,$code=NULL)
	{
		if(!isset(self::$_types[$module]))
		{
			$condition = "module='$module'";
			$items = self::model()->belong()->active()->listorder()->simple()->findAll($condition);
			if($items)
			{
				foreach ($items as $item)
					self::$_types[$module][$item->id] = $item->name;
			}else
				return false;
		}
		if (isset($code))
			return isset(self::$_types[$module][$code]) ? self::$_types[$module][$code] : false;
		else
			return isset(self::$_types[$module]) ? self::$_types[$module] : false;
	}

    public static function formatHtml($model)
    {
    	if(isset($model['created'])) $model['created'] = date('Y-m-d', $model['created']);
    	if(isset($model['modified'])) $model['modified'] = date('Y-m-d', $model['modified']);

    	return $model;
    }
}