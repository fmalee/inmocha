<?php

/**
 * This is the model class for table "{{article}}".
 *
 * The followings are the available columns in table '{{article}}':
 * @property string $id
 * @property string $user_id
 * @property string $name
 * @property string $title
 * @property string $category_id
 * @property string $description
 * @property string $content
 * @property integer $position
 * @property string $link_id
 * @property string $cover_id
 * @property integer $display
 * @property integer $attach
 * @property string $view
 * @property string $comment
 * @property integer $level
 * @property integer $status
 * @property string $template
 * @property string $created
 * @property string $modified
 */
class Article extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{article}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('content', 'required'),
			array('position, display, attach, level, status', 'numerical', 'integerOnly'=>true),
			array('user_id, category_id, link_id, cover_id, view, comment, created, modified', 'length', 'max'=>10),
			array('name', 'length', 'max'=>40),
			array('title', 'length', 'max'=>80),
			array('description', 'length', 'max'=>140),
			array('template', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, name, title, category_id, description, content, position, link_id, cover_id, display, attach, view, comment, level, status, template, created, modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'sku' => array(self::HAS_MANY, 'Sku', 'product_id'),
			'supply' => array(self::HAS_MANY, 'Supply', 'product_id'),
		);
	}

	public function scopes()
    {
        return array(
            'belong'=>array(
            	//'condition'=>'article.user_id=:user_id',
            	//'params'=>array(':user_id'=>Yii::app()->user->id),
            ),
            'category'=>array(
            	'condition'=>'article.category_id=:category_id',
            	'params'=>array(':category_id'=>$this->category_id),
            ),
            'notsafe'=>array(
            	'alias'=>'article',
            	//'select' => 'id, name, type_id, supply_id, status, title, is_elite, tag, selling_price, current_price, buiing_price, stock, inferior, picture, sku_1, sku_2, memo, created, modified',
            ),
            'forcheck'=>array(
            	'alias'=>'article',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'article',
            	'select' => 'id, name, title, category_id, description',
            ),
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => '文档ID',
			'user_id' => '用户ID',
			'name' => '标识',
			'title' => '标题',
			'category_id' => '所属分类',
			'description' => '描述',
			'content' => '文章内容',
			'position' => '推荐位',
			'link_id' => '外链',
			'cover_id' => '封面',
			'display' => '可见性',
			'attach' => '附件数量',
			'view' => '浏览量',
			'comment' => '评论数',
			'level' => '优先级',
			'status' => '数据状态',
			'template' => '详情页显示模板',
			'created' => '创建时间',
			'modified' => '更新时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('category_id',$this->category_id,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('link_id',$this->link_id,true);
		$criteria->compare('cover_id',$this->cover_id,true);
		$criteria->compare('display',$this->display);
		$criteria->compare('attach',$this->attach);
		$criteria->compare('view',$this->view,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('level',$this->level);
		$criteria->compare('status',$this->status);
		$criteria->compare('template',$this->template,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Article the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
