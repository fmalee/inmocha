<?php

class Area extends CActiveRecord
{
	private static $_areas=array();

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{area}}';
	}

	/**
	 * 获取并更新地区表
	 * 不需要经常更新
	 */
	public function synAreas() {
		$this->Taoapi->method = 'areas';
		$this->Taoapi->fields = 'id,type,name,parent_id,zip';
		$areas = $this->Taoapi->select()->get();
		if (!isset($areas['areas_get_response']) || !isset($areas['areas_get_response']['areas'])) return false;

		$this->execute("truncate table __TABLE__ "); // 清空area表
		foreach ($areas['areas_get_response']['areas']['area'] as $area) {
			$this->save($area);  //更新area表
		}
		return true;
	}
}