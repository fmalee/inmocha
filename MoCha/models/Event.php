<?php

class Event extends CActiveRecord
{
	const TYPE_CREATE = 1; //创建
	const TYPE_UPDATE = 2; //修改
	const TYPE_READ = 3; //阅读
	const TYPE_DELETE = 4; //删除
	const TYPE_INIT = 5; //初始化
	const TYPE_BUYING = 5; //买入
	const TYPE_SELLING = 6; //卖出
	const TYPE_SHIPPING = 7; //运输
	const TYPE_REPAIR = 8; //次品修复
	const TYPE_DESTROY = 9; //次品销毁
	
	private static $_types = array();
	/**
	 * Event表的字段:
	 * @var integer $id
	 * @var integer $type
	 * @var integer $owner_id
	 * @var string $content
	 * @var string $reason
	 * @var string $module
     * @var integer $user_id
     * @var integer $created
	 */
	//public $buyer_nick;
	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{event}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('type, content, reason, module', 'required'),
			array('module', 'moduleFilter'),
		);
	}

	/**
	 * @return array 验证模型.
	 */
	public function moduleFilter($attribute,$params)
	{
        $modules = array('fake','supplier');
        if(!in_array($this->module, $modules))
        	$this->addError($attribute, 'Invalid request. Please do not repeat this request again.!');
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
        return array(
			//'supplier' => array(self::BELONGS_TO, 'Supplier', '', 'on'=>'type.id=supplier.type'),
			'supplierCount' => array(self::STAT, 'Supplier', 'type_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('common', 'Event ID'),
			'type'=>Yii::t('common', 'Type'),
			'content'=>Yii::t('common', 'Content'),
			'reason'=>Yii::t('common', 'Reason'),
			'module'=>Yii::t('common', 'Module'),
			'created' => Yii::t('common', 'Created Time'),
		);
	}
	
	public function scopes()
    {
        return array(
        	'belong'=>array(
                'condition'=>'type.owner_id=:owner_id',
                'params'=>array(':owner_id'=>Yii::app()->user->id),
            ),
            'active'=>array(
                'condition'=>'type.status='.self::STATUS_ACTIVE,
            ),
            'notactive'=>array(
                'condition'=>'type.status='.self::STATUS_NOACTIVE,
            ),
            'supplier'=>array(
            	'condition'=>"type.module='supplier'",
            ),
            'fake'=>array(
            	'condition'=>"type.module='fake'",
            ),
            'notsafe'=>array(
            	'alias'=>'type',
            	'select' => 'id, type, content, reason, module, created',
            ),
            'forcheck'=>array(
            	'alias'=>'type',
            	'select' => 'id',
            ),
        );
    }

	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->created=time();
				$this->owner_id=Yii::app()->user->id;
				$this->user_id=Yii::app()->user->id;
			}
			return true;
		}
		else
			return false;
	}

	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'type' => array(
				self::TYPE_CREATE => Ya::t('Create'),
				self::TYPE_UPDATE => Ya::t('Update'),
				self::TYPE_READ => Ya::t('Read'),
				self::TYPE_DELETE => Ya::t('Delete'),
				self::TYPE_INIT => Ya::t('Initialize'),
				self::TYPE_BUY => Ya::t('Buy'),
				self::TYPE_SELL => Ya::t('Sell'),
				self::TYPE_SHIPP => Ya::t('Ship'),
				self::TYPE_REPAIR => Ya::t('Repair'),
				self::TYPE_DESTROY => Ya::t('Destroy'),
			),
			'module' => array(
				'fake' => Ya::t('Fake'),
				'supplier' => Ya::t('Supplier'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

	public static function Lists($module,$code=NULL)
	{
		if(!isset(self::$_types[$module]))
		{
			$items = self::model()->belong()->$module()->active()->listorder()->forcheck()->findAll();
			if($items)
			{
				foreach ($items as $item)
					self::$_types[$module][$item->id] = $item->name;
			}else
				return false;
		}
		if (isset($code))
			return isset(self::$_types[$module][$code]) ? self::$_types[$module][$code] : false;
		else
			return isset(self::$_types[$module]) ? self::$_types[$module] : false;
	}

    public function formatHtml($model)
    {
    	if(isset($model['type'])) $model['type'] = self::itemAlias('type', $model['type']);
    	if(isset($model['module'])) $model['module'] = self::itemAlias('module', $model['module']);
    	if(isset($model['created'])) $model['created'] = date('Y-m-d H:i:s', $model['created']);

    	return $model;
    }
}