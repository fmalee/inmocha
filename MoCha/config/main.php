<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'摩卡网商',
	'language' => 'zh_CN',
	'timeZone'=>'Asia/Shanghai',

	//'theme'=>'bootstrap',
	'defaultController'=>'site',
	//维护程序
    //'catchAllRequest' => array('site/all'),

	// preloading 'log' component
	'preload'=>array('log'),
	//路径别名
	'aliases'=>array(
		'bootstrap'=>dirname(__FILE__).'/../extensions/bootstrap',
	),
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.modules.user.models.*',
		'application.modules.user.components.*',
		'application.vendors.*',
		'bootstrap.helpers.*',
		'bootstrap.behaviors.BsWidget',
		/*'application.modules.rights.*',
		'application.modules.rights.components.*',
		'application.modules.srbac.controllers.SBaseController',*/
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123456',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
			'generatorPaths'=>array(
                'bootstrap.gii',
            ),
		),
		'user'=>array(
			# send activation email
			'sendActivationMail' => false,
			# allow access for non-activated users
			'loginNotActiv' => false,
			# activate user on registration (only sendActivationMail = false)
			'activeAfterRegister' => true,
			# automatically login from registration
			'autoLogin' => true,
        ),
        'seller'=>array(
			# 淘宝APPKEY
			'AppKey' => '12395060',
			# 淘宝SecretKey
			'SecretKey' => 'b33bee16fd5622daf8aacc7000f4f1f6',
			/*# 淘宝APPKEY
			'AppKey' => '21026410',
			# 淘宝SecretKey
			'SecretKey' => 'a867837bce8df4bc6d4e6a11723bda10',*/
        ),
		/*'srbac' => array(
			'userclass'=>'Memeber', //可选,默认是 User
			'userid'=>'userid', //可选,默认是 userid
			'username'=>'username', //可选，默认是 username
			'debug'=>true, //可选,默认是 false
			'pageSize'=>10, //可选，默认是 15
			'superUser' =>'Authority', //可选，默认是 Authorizer
			'css'=>'srbac.css', //可选，默认是 srbac.css
			'layout'=>
			'application.views.layouts.member', //可选,默认是
			//application.views.layouts.main, 必须是一个存在的路径别名
			'notAuthorizedView'=>
			'srbac.views.authitem.unauthorized', // 可选,默认是unauthorized.php
			//srbac.views.authitem.unauthorized, 必须是一个存在的路径别名
			'alwaysAllowed'=>array( //可选,默认是 gui
				'SiteLogin','SiteLogout','SiteIndex','SiteAdmin',
				'SiteError', 'SiteContact'
			),
			'userActions'=>array( //可选,默认是空数组
				'Show','View','List'
			),
			'listBoxNumberOfLines' => 15, //可选,默认是10
			'imagesPath' => 'srbac.images', //可选,默认是 srbac.images
			'imagesPack'=>'noia', //可选,默认是 noia
			'iconText'=>true, //可选,默认是 false
			'header'=>'srbac.views.authitem.header', //可选,默认是
			//srbac.views.authitem.header, 必须是一个存在的路径别名
			'footer'=>'srbac.views.authitem.footer', //可选,默认是
			//srbac.views.authitem.footer, 必须是一个存在的路径别名
			'showHeader'=>true, //可选,默认是false
			'showFooter'=>true, //可选,默认是false
			'alwaysAllowedPath'=>'srbac.components', //可选,默认是 srbac.components
		),
		'rights'=>array(
		    'superuserName'=>'Memeber',                            // Name of the role with super user privileges. 
		    'authenticatedName'=>'Authenticated',                // Name of the authenticated user role. 
		    'userIdColumn'=>'userid',                                // Name of the user id column in the database. 
		    'userNameColumn'=>'username',                        // Name of the user name column in the database. 
		    'enableBizRule'=>true,                               // Whether to enable authorization item business rules. 
		    'enableBizRuleData'=>false,                          // Whether to enable data for business rules. 
		    'displayDescription'=>true,                          // Whether to use item description instead of name. 
		    'flashSuccessKey'=>'RightsSuccess',                  // Key to use for setting success flash messages. 
		    'flashErrorKey'=>'RightsError',                      // Key to use for setting error flash messages. 
		    'install'=>false,                                     // Whether to install rights. 
		    'baseUrl'=>'/rights',                                // Base URL for Rights. Change if module is nested. 
		    'layout'=>'rights.views.layouts.main',               // Layout to use for displaying Rights. 
		    'appLayout'=>'application.views.layouts.main',       // Application layout. 
		    'cssFile'=>'rights.css',                             // Style sheet file to use for Rights. 
		    'debug'=>true,                                      // Whether to enable debug mode. 
		),*/
	),

	// application components
	'components'=>array(
		//bootstrap扩展
		'bootstrap'=>array(
			'class'=>'bootstrap.components.BsApi',
		),
		'viewRenderer'=>array(
            'class'=>'CPradoViewRenderer',
        ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'class'=>'WebUser', //自定义权限管理
			'loginUrl' => array('user/login'), //设置默认登录页面

		),

		'request'=>array(
            'enableCsrfValidation'=>true, //防表单跨站攻击
            'enableCookieValidation'=>true, //Cookie验证
        ),
		/*'authManager'=>array(
			'class'=>'RDbAuthManager', //认证类名称
			'connectionID'=>'db', //使用的数据库组件
			'defaultRoles'=>array('guest'), //默认角色
			'itemTable'=>'mc_auth_item', //认证项表名称
			'itemChildTable'=>'mc_auth_item_child', //认证项父子关系
			'assignmentTable'=>'mc_auth_assignment', //认证项赋权关系
		),*/
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(
				'' => 'site/index', // 一个标准的URL规则，将 '/' 对应到 'site/index'
				'<action:(login|logout|about)>' => 'site/<action>',

				'post/<id:\d+>/<title:.*?>'=>'post/view',
				'posts/<tag:.*?>'=>'post/index',

				'<_c:(post|comment)>/<id:\d+>/<_a:(create|update|delete)>' => '<_c>/<_a>',
				'<_c:(post|comment)>/<id:\d+>' => '<_c>/read',
				'<_c:(post|comment)>s' => '<_c>/list',

				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>require(dirname(__FILE__).'/params.php'),
);