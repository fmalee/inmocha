<?php
return array(
	'components'=>array(
		//SAE下固定mysql配置
		'db'=>array(
			'connectionString' => 'mysql:host='.SAE_MYSQL_HOST_M.','.SAE_MYSQL_HOST_S.':'.SAE_MYSQL_PORT.';dbname='.SAE_MYSQL_DB.',
			'emulatePrepare' => true,
			'username' => SAE_MYSQL_USER,
			'password' => SAE_MYSQL_PASS,
			'charset' => 'utf8',
			'tablePrefix' => 'tb_',
		),
	),
);