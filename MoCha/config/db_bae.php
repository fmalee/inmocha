<?php
return array(
	'components'=>array(
		//BAE下固定mysql配置
		'db'=>array(
			'connectionString' => 'mysql:host='.HTTP_BAE_ENV_ADDR_SQL_IP.':'.HTTP_BAE_ENV_ADDR_SQL_PORT.';dbname=LhYrXxlPtscyQvkKKajH',
			'emulatePrepare' => true,
			'username' => HTTP_BAE_ENV_AK,
			'password' => HTTP_BAE_ENV_SK,
			'charset' => 'utf8',
			'tablePrefix' => 'tb_',
		),
	),
);
