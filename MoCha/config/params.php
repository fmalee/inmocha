<?php

// this contains the application parameters that can be maintained via GUI
return array(
	// this is displayed in the header section
	'title'=>'摩卡网商',
	'description' => '摩卡网商,淘宝管理利器',
	'keywords' => '摩卡网商,淘宝管理利器',
	'author' => 'Inmocha,Joey',

	'domain1' => 'http://m.coromachi.com', //网站域名
	'status' => '1', //网站状态
	'ipBan' => '0', //启用黑名单
	'imgPath' => '/asset/img/', //图片路径
	'jsPath' => '/asset/js/', //JS路径
	'cssPath' => '/asset/css/', //CSS路径
	'authcode' => 'zfyIOGxD@#', //authcode加密函数密钥

    /* 淘宝开放平台设定 */
	//'appKey' => '21026410',
	//'secretKey' => 'a867837bce8df4bc6d4e6a11723bda10',
	'appKey' => '12395060',
	'secretKey' => 'b33bee16fd5622daf8aacc7000f4f1f6',

	// this is used in error pages
	'adminEmail'=>'webmaster@example.com',
	// number of posts displayed per page
	'perPage'=>20,
	//'varPage' => 'page', //分页字符
	//默认AJAX 数据返回格式,可选JSON XML ...
	'ajaxReturn' => 'JSON',
	//默认JSONP格式返回的处理方法
	'jsonHander' => 'callback',

	'uploadPath' => './Public/', //上传附件路径
	'uploadUrl' => 'http://t.coromachi.com/uploadfile/', //附件URL
	'uploadRule' => 'uniqid', //上传文件名命名规则 例如可以是 time uniqid com_create_guid 等 必须是一个无需任何参数的函数名 可以使用自定义函数
	'uploadMaxSize' => '20480000', //允许上传附件大小
	'uploadAllowExt' => 'jpg,gif,png,jpeg,swf', //允许上传附件类型
	'htmlPath' => '/Html/', //HTML静态文件目录

	"version" => "1.0", //版本号
	"release" => "20121217", //发行日期
);