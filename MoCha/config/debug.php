<?php
$config = array(
	// application components
	'components'=>array(
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=inmocha',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '15158899866',
			'charset' => 'utf8',
			'tablePrefix' => 'mc_',
			'enableProfiling' => true,
			'enableParamLogging' => true,
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning, profile, info, trace',
				),
				/*array(
					'class'=>'CProfileLogRoute',
					'levels'=>'profile',
					'filter'=>'CLogFilter',
				),*/
				array(
					'class'=>'CWebLogRoute',
					'levels'=>'error, warning, profile, info, trace',
					//'categories' => 'db.*',
					//'categories'=>'system.db.*',
					'showInFireBug' => true,
					//'filter'=>'CLogFilter',
				),
			),
		),
	),
);

$main = require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'main.php');
$config = CMap::mergeArray($main,$config);
return $config;