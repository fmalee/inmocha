<?php

class TypeController extends Controller
{
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index', 'list', 'type', 'create', 'edit', 'aedit', 'delete', 'single', 'childs'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function beforeAction($action)
    {
      if (parent::beforeAction($action)) {
        //check GET parameters
        if ($action->id == 'edit') {
          if (empty($_GET['module']))
            throw new CHttpException(404,'Not found');
        }
        return true;
      } else
        return false;
    }

	/**
	 * 分类管理
	 */
	public function actionType($module)
	{
		$criteria=new CDbCriteria(array(
			'condition'=>"type.module='$module'",
			'scopes'=>array('belong','notsafe'),
			'order'=>'list_order DESC, id asc',
		));

		$dataProvider=new CActiveDataProvider('Type', array(
			'pagination'=>array(
				'pageSize'=>20
			),
			'criteria'=>$criteria,
		));

		$this->render('type',array(
			'dataProvider'=>$dataProvider,
			'module'=>$module
		));
	}

	/**
	 * AJAX获取单分类类型列表
	 */
	public function actionAjaxSingle($module)
	{
		$searchColumns = array('type.name', 'type.memo');

		$criteria=new CDbCriteria(array(
			'scopes'=>array('belong', 'notsafe', $module),
			'limit'=>100,
		));

		$DataTable = new DataTable('Type', $criteria, $searchColumns);
		$output = $DataTable->output();
		$data = $DataTable->data();

		foreach ($data as $model) {
			$row = Type::formatHtml($model->attributes);
			$row['DT_RowId'] = $model['id'];
			$output['aaData'][] = $row;
		}

		$this->ajaxReturn($output,null);
	}

	/**
	 * 获取紧接着的下一级分类ID
	 */
	public function actionChilds($module, $id = 0)
	{
		$criteria=new CDbCriteria(array(
			'condition'=>'parent_id=:parent_id AND module=:module',
			'params'=>array(':parent_id'=>$id,':module'=>$module),
		));
		//$condition = "module='$module'";
        $types = Type::model()->belong()->active()->listorder()->simple()->findAll($criteria);
		//$types = Type::model()->findAll($criteria);

		if($types)
		{
			foreach ($types as $key=>$value) {
				$childs[$key]['id'] = $value->id;
				$childs[$key]['name'] = $value->name;
			}
			$this->ajaxReturn($childs);
		}
		else
			$this->ajaxReturn(Ya::t('Operation Success'),0);
	}
}