<?php
//自定义的基类
class BaseController extends Controller
{
	public $layout='main';

	//确保URL的唯一性，便利与SEO
	public function beforeAction($action)
    {
      
        if (Yii::app()->request->url != CHtml::normalizeUrl(array_merge(array($this->route), $_GET)))
            $this->redirect(CHtml::normalizeUrl(array_merge(array($this->route), $_GET)), true, 301);
      
        return parent::beforeAction($action);
    }
}
