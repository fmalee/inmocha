<?
/**
 * Base class for all active records
 * @author yii-user-management
 * @since 0.6
 * @package Yum.core
 *
 */
abstract class ActiveRecord extends CActiveRecord
{
	protected $_tableName;

	public function limit($limit = 10)
	{
		$this->getDbCriteria()->mergeWith(array(
					'limit' => $limit,
					));
		return $this;
	}

	public function order($order = 'id')
	{
		$this->getDbCriteria()->mergeWith(array(
					'order' => $order,
					));
		return $this;
	}

	/**
	 * @return CActiveRecordMetaData the meta for this AR class.
	 */	
	public function getMetaData( )
	{
		$md = parent::getMetaData( );
		if($this->getScenario()==='search')
		{
			$md->attributeDefaults  = array ();
		}

		return $md;
	}

}
?>
