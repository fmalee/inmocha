<?php
class DataTable
{
	private $alias; //模型类
	private $model; //模型类
	private $criteria; //模型类
	private $searchColumns; //允许搜索的列

	public $filter;
	public $output;
	public $aColumns; //表格列名
	public $iColumns; //表格列数
	
	/**
     * 架构函数
     * @access public
     * @param array $totalRows  总的记录数
     * @param object $criteria  查询参数
     * @param array $parameter  分页跳转的参数
     */
    public function __construct($model, $criteria, $searchColumns) {
    	$this->alias = $model;
    	$this->model = $model::model();
    	$this->criteria = $criteria;

    	$this->iColumns = intval($_GET['iColumns']);
    	$this->searchColumns = $searchColumns;
    	$this->aColumns = $this->getColumns();

    }

    //输出视图
    public function output() {
    	$this->output['sEcho'] = intval($_GET['sEcho']);

    	$iTotal = $this->model->count($this->criteria);
    	$this->output['iTotalRecords'] = $iTotal;

    	$this->mergeCriteria();
		$iFilteredTotal = $this->model->count($this->criteria);
		$this->output['iTotalDisplayRecords'] = $iFilteredTotal;

		$this->output['aaData'] = array();
		
		return $this->output;
    }

    //输出数据
    public function data() {
    	$data = $this->model->findAll($this->criteria);

		return $data;
    }

    //分页
    public function page() {
		if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1')
		{
			$this->filter->limit = intval($_GET['iDisplayLength']);
			$this->filter->offset = intval($_GET['iDisplayStart']);
		}
    }

	//排序
    public function order()
    {
        $sOrder = '';
		if (isset($_GET['iSortingCols']))
		{
			for ($i=0; $i<intval($_GET['iSortingCols']); $i++)
			{
				$iSortCol = intval($_GET['iSortCol_'.$i]);
				if ($_GET['bSortable_'.$iSortCol] == 'true')
					$sOrder .= $this->alias.'.'.$this->aColumns[$iSortCol]." ". ($_GET['sSortDir_'.$i]==='asc' ? 'ASC' : 'DESC').", ";
			}
			if ($sOrder) $this->filter->order = substr_replace( $sOrder, "", -2 );
		}
    }

    //搜索框筛选，按照允许搜索的列进行搜索
    public function searchFilter()
    {
        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "")
		{
			for ($i=0; $i<count($this->searchColumns); $i++)
			{
				$this->filter->compare($this->searchColumns[$i], mysql_real_escape_string($_GET['sSearch']), true, 'OR');
			}
		}
    }

    //原生的搜索框筛选，按照前台返回的列进行搜索
    public function org_searchFilter()
    {
        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "")
		{
			for ($i=0; $i<count($this->aColumns); $i++)
			{
				if (isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true")
				{
					 $this->filter->compare($this->aColumns[$i], mysql_real_escape_string($_GET['sSearch']), true, 'OR');
				}
			}
		}
    }

    //列筛选
    public function columnFilter()
    {
		for ($i=0; $i<count($this->aColumns); $i++)
		{
			if (isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '')
			{
				 $this->filter->compare($this->aColumns[$i], mysql_real_escape_string($_GET['sSearch_'.$i]));
			}
		}
    }

    //获取表格列名
    public function getColumns() {
    	$aColumns = array();
		for ($i=0; $i<$this->iColumns; $i++)
		{
			if (isset($_GET['mDataProp_'.$i]) && $_GET['mDataProp_'.$i] != '\\')
			{
				 $aColumns[] = $_GET['mDataProp_'.$i];
			}
		}

		return $aColumns;
    }

    //组成SQL条件
    public function mergeCriteria() {
    	$this->filter=new CDbCriteria();
		$this->page();
		$this->order();
    	$this->searchFilter();
    	$this->columnFilter();
    	
    	$this->criteria->mergeWith($this->filter);
    }
}