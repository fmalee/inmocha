<?php
/**
 * Base controller class
 */
class Controller extends CController
{
	public $breadcrumbs = array();
	public $menu = array();
	//public $layout='//layouts/right';
	public $pageTitle ='桌面';
	public $_model;

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	//AJAX验证
	protected function performAjaxValidation($model, $form)
	{
		if(isset($_POST['ajax']) && $_POST['ajax'] == $form)
		{
			$this->ajaxReturn(CActiveForm::validate($model),0);
			//echo CActiveForm::validate($model);
			//Yii::app()->end();
		}
	}

	/**
	 * 列表
	 */
	public function actionIndex()
	{
        $this->render('index');
	}

	/**
	 * 查看
	 */
	public function actionView()
	{
		$model = $this->loadModel();
		$this->render('view',array('model' => $model,));
	}

	/**
     * 新增
     */
    public function actionCreate()
	{
		$modelName = str_replace('Controller', '', get_class($this));
		$model = new $modelName('create');

		//$this->performAjaxValidation($model, $modelName.'Form');

		if(isset($_POST[$modelName]))
		{
			$model->attributes = $_POST[$modelName];
			if($model->save())
			{
				if(Yii::app()->request->isAjaxRequest)
					$this->ajaxReturn(Yii::t('common', 'Operation Success'));
				else
				{
					Yii::app()->user->setFlash('OperationMessage',Yii::t('common', "Changes is saved."));
					$this->redirect(Yii::app()->request->urlReferrer);
				}
			}
			else{
				if(Yii::app()->request->isAjaxRequest)
				{
					$validate = Ya::formatValidate(CActiveForm::validate($model));
					$this->ajaxReturn($validate,0);
				}
				else
				{
					$validate = CActiveForm::validate($model);
					throw new CHttpException(400,Yii::t('common', 'Invalid request. Please do not repeat this request again.'));
				}
			}
		}
		if(Yii::app()->request->isAjaxRequest)
		{
			$data = $this->renderPartial('create',array('model'=>$model,),true);
			$this->ajaxReturn($data);
		}
		else
			$this->render('create',array('model'=>$model));
	}

	/**
     * 修改
     */
    public function actionEdit()
    {
        $model = $this->loadModel();
        $modelName = get_class($model); //MODEL的CLASS名字，一般就是表单前缀

		if(isset($_POST[$modelName]))
		{
			$model->attributes=$_POST[$modelName];

			if($model->save())
			{
				if(Yii::app()->request->isAjaxRequest)
					$this->ajaxReturn(Yii::t('common', 'Operation Success'));
				else
				{
					Yii::app()->user->setFlash('OperationMessage',Yii::t('common', "Changes is saved."));
					$this->redirect(Yii::app()->request->urlReferrer);
				}
			}
			else
			{

				if(Yii::app()->request->isAjaxRequest)
				{
					$validate = Ya::formatValidate(CActiveForm::validate($model));
					$this->ajaxReturn($validate,0);
				}
				else
				{
					$validate = CActiveForm::validate($model);
					throw new CHttpException(400,Yii::t('common', 'Invalid request. Please do not repeat this request again.'));
				}
			}
		}
		if(Yii::app()->request->isAjaxRequest)
		{
			$data = $this->renderPartial('edit',array('model'=>$model,),true);
			$this->ajaxReturn($data);
		}
		else
			$this->render('edit',array('model'=>$model));

    }

    /**
     * ajax修改单个字段值
     */
    public function actionAedit($field, $val)
    {
		$model = $this->loadModel();
		$model->$field = $val;

		if($model->save())
		{
			$this->ajaxReturn(Yii::t('common', 'Operation Success'));
		} else
		{
			$validate = Ya::formatValidate(CActiveForm::validate($model));
			$this->ajaxReturn($validate,0);
		}
    }

	/**
	 * 删除
	 */
	public function actionDelete($id)
	{
		$modelName = str_replace('Controller', '', get_class($this));
		$model = CActiveRecord::model($modelName);
		$ids = explode(',', $id);
		foreach ($ids as $pk)
		{
			$result=$model->belong()->forcheck()->findbyPk($pk);
			if($result)
				$result->delete();
		}
		if(Yii::app()->request->isAjaxRequest)
			$this->ajaxReturn(Ya::t('Operation Success'));
		else
			$this->redirect(Yii::app()->request->urlReferrer);
	}

    /**
     * 加载模块
     */
    public function loadModel($model = false) {
    	if(!$model)
			$model = str_replace('Controller', '', get_class($this));

		if($this->_model === null) {
			if(isset($_GET['id']))
				$this->_model = CActiveRecord::model($model)->belong()->notsafe()->findByPk($_GET['id']);

			if($this->_model===null && !is_numeric($_GET['id']))
				@$this->_model = CActiveRecord::model($model)->find(
						'name = :name', array(':name' => $_GET['id']));

			if($this->_model===null)
			{
				if(Yii::app()->request->isAjaxRequest)
					$this->ajaxReturn(Ya::t('Invalid request. Please do not repeat this request again.'));
				else
					throw new CHttpException(404, Ya::t('The requested page does not exist.'));
			}
		}
		return $this->_model;
	}

    /**
     * 页面跳转
     * 如果想直接赋值，用$this->ajaxRetun(array(key=>value),null);
     * @access protected
     * @param String $status 要返回的状态
     * @param String $info 要返回的标题
     * @param mixed $data 要返回的数据
     * @param String $type AJAX返回数据格式
     * @return void
     */
    protected function jump($message, $jumpUrl = '', $waitSecond = '10')
    {
    	Yii::log($jumpUrl, 'info');
    	$this->renderPartial('//site/jump',array(
    		'message'=>$message,
    		'jumpUrl'=>$jumpUrl,
    		'waitSecond'=>$waitSecond
    	));
    	Yii::app()->end();
    }

    /**
     * Ajax方式返回数据到客户端
     * 如果想直接赋值，用$this->ajaxRetun(array(key=>value),null);
     * @access protected
     * @param String $status 要返回的状态
     * @param String $info 要返回的标题
     * @param mixed $data 要返回的数据
     * @param String $type AJAX返回数据格式
     * @return void
     */
    protected function ajaxReturn($msg, $status = 1, $data = '', $type='')
    {
    	if (is_null($status))
    	{
    		$ajax = $msg;
    	} else
    	{
    		$ajax = array();
	    	$ajax['status'] = $status;
	    	$ajax['msg'] = $msg;
	    	if(!empty($data)) $ajax['data'] = $data;
    	}

        if(empty($type))
        {
        	$default  = Yii::app()->params->ajaxReturn;
        	$type = $default ? $default : 'JSON';
        }
        switch (strtoupper($type))
        {
            case 'XML'  :
                // 返回xml格式数据
                header('Content-Type:text/xml; charset=utf-8');
                echo xml_encode($ajax);
            case 'JSONP':
                // 返回JSON数据格式到客户端 包含状态信息
                header('Content-Type:application/json; charset=utf-8');
                $handler  =   isset($_GET[Yii::app()->params->jsonHander]) ? $_GET[Yii::app()->params->jsonHander] : 'jsonpReturn';
                echo $handler.'('.json_encode($ajax).');';
            case 'EVAL' :
                // 返回可执行的js脚本
                header('Content-Type:text/html; charset=utf-8');
                echo $ajax;
            default     : // 返回JSON数据格式到客户端 包含状态信息
                header('Content-Type:application/json; charset=utf-8');
                echo json_encode($ajax);
        }

        Yii::app()->end();
    }
}