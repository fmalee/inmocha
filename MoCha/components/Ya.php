<?php
/**
 * Helper class
 * @author yii-user-management
 * @since 0.6
 * @package Yum.core
 *
 */
class Ya {
	public static function hint($message) {
		return '<div class="hint">' . Ya::t($message) . '</div>';
	}

	//获取、设置配置参数
	public static function C($params) {
		return Yii::app()->params[$params];
	}
	public static function getAvailableLanguages ()	{
		$cache_id = 'yum_available_languages';

		$languages = false;
		if(Yii::app()->cache)
			$languages = Yii::app()->cache->get($cache_id);

		if($languages===false) {
			$translationTable = Yum::module()->translationTable;
			$sql = "select language from {$translationTable} group by language";

			$command=Yii::app()->db->createCommand($sql);

			$languages=array();
			foreach($command->queryAll() as $row)
				$languages[$row['language']]=$row['language'];

			if(Yii::app()->cache)
				Yii::app()->cache->set($cache_id, $languages);
		}

		return $languages;
	}

	/* set a flash message to display after the request is done */
	public static function setFlash($message, $delay = 5000)
	{
		$_SESSION['yum_message'] = Yum::t($message);
		$_SESSION['yum_delay'] = $delay;
	}

	public static function hasFlash()
	{
		return isset($_SESSION['yum_message']);
	}

	/* retrieve the flash message again */
	public static function getFlash()
	{
		if(Yum::hasFlash())
		{
			$message = @$_SESSION['yum_message'];
			unset($_SESSION['yum_message']);
			return $message;
		}
	}

	/* A wrapper for the Yii::log function. If no category is given, we
	 * use the YumController as a fallback value.
	 * In addition to that, the message is being translated by Yum::t() */
	public static function log($message,
			$level = 'info',
			$category = 'application.modules.user.controllers.YumController')
	{
		if(Yum::module()->enableLogging)
			return Yii::log(Yum::t($message), $level, $category);
	}

	public static function renderFlash()
	{
		if(Yum::hasFlash()) {
			echo '<div class="info">';
			echo Yum::getFlash();
			echo '</div>';
			Yii::app()->clientScript->registerScript('fade',"
					setTimeout(function() { $('.info').fadeOut('slow'); },
						{$_SESSION['yum_delay']});
					");
		}
	}

	public static function p($string, $params = array())
	{
		return '<p>' . Ya::t($string, $params) . '</p>';
	}

	/** Fetch the translation string from db and cache when necessary */
	public static function t($str='', $params=array(), $dic='MoCha')
	{
		return Yii::t($dic, $str, $params);
	}

	// returns the Yii User Management module. Frequently used for accessing
	// options by calling Yum::module()->option
	public static function module($module = 'seller') {
		return Yii::app()->getModule($module);
	}

	public static function hasModule($module) {
		return array_key_exists($module, Yii::app()->modules);
	}

	/**
	 * Produces note: "Field with * are required"
	 * @since 0.6
	 * @return string
	 */
	public static function requiredFieldNote()
	{
		return CHtml::tag('p',array('class'=>'note'),Yum::t(
					'Fields with <span class="required">*</span> are required.'
					),true);
	}

	/**
	 * 记录操作日志
	 *
	 * @param string $type
	 * @param string $reason
	 * @param string $content
	 *
	 */
	public function event($module, $type, $reason, $content)
	{
		$event = new Event;
		$event->module = $module;
		$event->type = $type;
		$event->reason = $reason;
		$event->content = $content;
		$$event->save();

		return true;
	}

	/**
	 * 转换Validate格式
	 * @param string $validate
	 */
	public static function formatValidate($validate)
	{
		$result = '';
		$validates = json_decode($validate, true);
		foreach ($validates as $field) {
			foreach ($field as $item) {
				$result .='<p>'.$item.'</p>';
			}
		}

		return $result;
	}

	/**
	 * 注册JS.CSS资源
	 * @param array $files
	 */
	public static function register($files) {
		if(is_array($files))
		{
			foreach ($files as $file) {
				if(strpos($file, 'js') !== false)
					$url = Yii::app()->params->jsPath;
				else
					$url = Yii::app()->params->cssPath;

				if(strpos($file,'/') > 0 || strpos($file,'/') === false)
					$path = $url.$file;

				if(strpos($file, 'js') !== false)
					Yii::app()->clientScript->registerScriptFile($path, CClientScript::POS_END);
				else if(strpos($file, 'css') !== false)
					Yii::app()->clientScript->registerCssFile($path);
			}
		}
	}

	/**
	 * 注册CSS代码资源
	 * @param string $id
	 * @param string $css
	 * @param bool $iscode
	 */
	public static function registerCss($id, $css, $iscode = false)
	{
		if(!$iscode)
			$css = Ya::renderFile($css);
		Yii::app()->clientScript->registerCss($id, $css);
	}

	/**
	 * 注册JS代码资源
	 * @param string $id
	 * @param string $script
	 * @param bool $iscode
	 */
	public static function registerScript($id, $script, $iscode = false)
	{
		if(!$iscode)
			$script = Ya::renderFile($script);
		Yii::app()->clientScript->registerScript($id, $script, CClientScript::POS_END);
	}

	/**
	 * 返回渲染的视图
	 *
	 * @param string $view
	 * @param array $data
	 * @param bool $return
	 * @param bool $processOutput
	 *
	 */
	public static function renderFile($view, $data=null, $return=true, $processOutput=false)
	{
		$cs = Yii::app()->getController();
		$path = $cs->getViewPath().DIRECTORY_SEPARATOR;
		return $cs->renderFile($path.$view, $data, $return, $processOutput);
	}

	/**
	 * 字符转换成布尔值
	 */
	function str2bool($str) {
		$str = (string)$str;
		if(($str == '1') || ($str == 'true'))
			return 1;
		elseif(!$str || ($str == 'false'))
			return 0;
		else
			return $str;
	}

	/**
	 * 加密解密
	 * @param type $string 明文 或 密文
	 * @param type $operation DECODE表示解密,其它表示加密
	 * @param type $key 密匙
	 * @param type $expiry 密文有效期
	 * @return string
	 */
	public static function authcode($string, $operation = 'DECODE', $key = '', $expiry = 0) {
		// 动态密匙长度，相同的明文会生成不同密文就是依靠动态密匙
		$ckey_length = 4;
		// 密匙
		$key = md5(($key ? $key : C("AUTHCODE")));
		// 密匙a会参与加解密
		$keya = md5(substr($key, 0, 16));
		// 密匙b会用来做数据完整性验证
		$keyb = md5(substr($key, 16, 16));
		// 密匙c用于变化生成的密文
		$keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length) : substr(md5(microtime()), -$ckey_length)) : '';
		// 参与运算的密匙
		$cryptkey = $keya . md5($keya . $keyc);
		$key_length = strlen($cryptkey);
		// 明文，前10位用来保存时间戳，解密时验证数据有效性，10到26位用来保存$keyb(密匙b)，解密时会通过这个密匙验证数据完整性
		// 如果是解码的话，会从第$ckey_length位开始，因为密文前$ckey_length位保存 动态密匙，以保证解密正确
		$string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
		$string_length = strlen($string);
		$result = '';
		$box = range(0, 255);
		$rndkey = array();
		// 产生密匙簿
		for ($i = 0; $i <= 255; $i++) {
			$rndkey[$i] = ord($cryptkey[$i % $key_length]);
		}
		// 用固定的算法，打乱密匙簿，增加随机性，好像很复杂，实际上对并不会增加密文的强度
		for ($j = $i = 0; $i < 256; $i++) {
			$j = ($j + $box[$i] + $rndkey[$i]) % 256;
			$tmp = $box[$i];
			$box[$i] = $box[$j];
			$box[$j] = $tmp;
		}
		// 核心加解密部分
		for ($a = $j = $i = 0; $i < $string_length; $i++) {
			$a = ($a + 1) % 256;
			$j = ($j + $box[$a]) % 256;
			$tmp = $box[$a];
			$box[$a] = $box[$j];
			$box[$j] = $tmp;
			// 从密匙簿得出密匙进行异或，再转成字符
			$result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
		}
		if ($operation == 'DECODE') {
			// substr($result, 0, 10) == 0 验证数据有效性
			// substr($result, 0, 10) - time() > 0 验证数据有效性
			// substr($result, 10, 16) == substr(md5(substr($result, 26).$keyb), 0, 16) 验证数据完整性
			// 验证数据有效性，请看未加密明文的格式
			if ((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26) . $keyb), 0, 16)) {
				return substr($result, 26);
			} else {
				return '';
			}
		} else {
			// 把动态密匙保存在密文里，这也是为什么同样的明文，生产不同密文后能解密的原因
			// 因为加密后的密文可能是一些特殊字符，复制过程可能会丢失，所以用base64编码
			return $keyc . str_replace('=', '', base64_encode($result));
		}
	}
}
?>
