<?php
/**
 * 扩展CActiveForm
 */
class ActiveForm extends CActiveForm
{
	public function init()
	{
		parent::init();
	}

	public function run()
	{
		parent::run();
	}

	public function radioButtonGroup($model,$attribute,$data,$htmlOptions=array())
	{
		return YHtml::activeRadioButtonGroup($model,$attribute,$data,$htmlOptions);
	}
	public function checkBoxButton($model,$attribute,$data,$htmlOptions=array())
	{
		return YHtml::activeRadioButtonGroup($model,$attribute,$data,$htmlOptions);
	}
}
