<?php
/**
 * 扩展CHtml
 */
class YHtml extends CHtml
{
	/**
	 * AJAX编辑字段
	 * @param CModel $model the data model
	 * @param string $field the input value
	 * @param array $htmlOptions additional HTML attributes for the HTML tag (see {@link tag}
	 */
	public static function ajaxEdit($model, $field, $htmlOptions=array())
	{
		return '<span class="ajaxedit" data-id="'.$model->id.'" data-field="'.$field.'" data-tdtype="edit">'.$model->$field.'</span>';
	}

	/**
	 * AJAX切换字段
	 * @param CModel $model the data model
	 * @param string $field the input value
	 * @param array $htmlOptions additional HTML attributes for the HTML tag (see {@link tag}
	 */
	public static function ajaxToggle($model, $field, $htmlOptions=array())
	{
		$path = '/asset/img/';
		$img = $model->$field ? 'toggle_enabled.gif' : 'toggle_disabled.gif';
		return '<img src="'.$path.$img.'" data-value="'.$model->$field.'" data-field="'.$field.'" data-id="'.$model->id.'" data-tdtype="toggle">';
	}

	/**
	 * 以BUTTON形式形式Radio列表
	 * @param string $name the input name
	 * @param string $value the input value
	 * @param array $htmlOptions additional HTML attributes for the HTML tag (see {@link tag}).
	 * @return string the generated radio tag
	 */
	public static function radioButtonGroup($name,$select,$data,$htmlOptions=array())
	{
		if(!isset($htmlOptions['id']))
			$htmlOptions['id']=parent::getIdByName($name);
		elseif($htmlOptions['id']===false)
			unset($htmlOptions['id']);

        $string = '<div class="btn-group" '.parent::renderAttributes($htmlOptions).' data-toggle="buttons-radio">';
        foreach ($data as $key => $v) {
			$active = $checked ='';
			$act = 'off';
			if (trim($select) == trim($key)) {
				$active = ' active';
				$checked = ' checked="checked" ';
			}
			$string .= '<button class="btn' . $active . '" type="button" onclick="fnTriggerRadio(this)"><span>' . $v . '</span>';
			$string .= '<input type="radio" style="display:none;" value="' . htmlspecialchars($key) . '" name="' . $name .'"'. $checked . '>';
			$string .= '</button>';
		}
		$string .= '</div>';
        return $string;
	}

	/**
	 * 以BUTTON形式形式Radio列表
	 * @param CModel $model the data model
	 * @param string $attribute the attribute
	 * @param array $htmlOptions additional HTML attributes. Besides normal HTML attributes, a few special
	 * attributes are also recognized (see {@link clientChange} and {@link tag} for more details.)
	 * @return string the generated radio tag
	 */
	public static function activeRadioButtonGroup($model,$attribute,$data,$htmlOptions=array())
	{
		parent::resolveNameID($model,$attribute,$htmlOptions);
		if($model->hasErrors($attribute))
			parent::addErrorCss($htmlOptions);
		if(isset($htmlOptions['name']))
		{
			$name=$htmlOptions['name'];
			unset($htmlOptions['name']);
		}
		
		if(isset($htmlOptions['value']))
		{
			$value=$htmlOptions['value'];
			unset($htmlOptions['value']);
		}
		else
			$value=parent::resolveValue($model,$attribute);
		return self::radioButtonGroup($name,$value,$data,$htmlOptions);
	}

	/**
	 * 以BUTTON形式形式Radio列表
	 * @param string $name the input name
	 * @param string $value the input value
	 * @param array $htmlOptions additional HTML attributes for the HTML tag (see {@link tag}).
	 * @return string the generated radio tag
	 */
	public static function checkBoxButton($name,$select,$data,$htmlOptions=array())
	{
		if(!isset($htmlOptions['id']))
			$htmlOptions['id']=parent::getIdByName($name);
		elseif($htmlOptions['id']===false)
			unset($htmlOptions['id']);

		if ($select != '') $select = strpos($select, ',') ? explode(',', $select) : array($select);

        $string = '<div class="btn-group" '.parent::renderAttributes($htmlOptions).' data-toggle="buttons-checkbox">';
        foreach ($data as $key => $v) {
			$active = $checked ='';
			$act = 'off';
			if ($select && in_array($key, $select)) {
				$active = ' active';
				$checked = ' checked="checked" ';
			}
			$string .= '<button class="btn' . $active . '" type="button"><label for="'. $id .'_'. $key .'"><span>' . $v . '</span></label></button>';
			$string .= '<input type="checkbox" id="'.$htmlOptions['id'].'" style="display:none;" value="' . htmlspecialchars($key) . '" name="' . $name .'"'. $checked . '>';
		}
		$string .= '</div>';
        return $string;
	}
	/**
	 * 以BUTTON形式形式Radio列表
	 * @param CModel $model the data model
	 * @param string $attribute the attribute
	 * @param array $htmlOptions additional HTML attributes. Besides normal HTML attributes, a few special
	 * attributes are also recognized (see {@link clientChange} and {@link tag} for more details.)
	 * @return string the generated radio tag
	 */
	public static function activeCheckBoxButton($model,$attribute,$data,$htmlOptions=array())
	{
		parent::resolveNameID($model,$attribute,$htmlOptions);
		if($model->hasErrors($attribute))
			parent::addErrorCss($htmlOptions);
		if(isset($htmlOptions['name']))
		{
			$name=$htmlOptions['name'];
			unset($htmlOptions['name']);
		}
		
		if(isset($htmlOptions['value']))
		{
			$value=$htmlOptions['value'];
			unset($htmlOptions['value']);
		}
		else
			$value=parent::resolveValue($model,$attribute);
		return self::checkBoxButton($name,$value,$data,$htmlOptions);
	}
}
