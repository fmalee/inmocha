<?php
/**
 * Yii-User module
 *
 * @author Mikhail Mangushev <mishamx@gmail.com>
 * @link http://yii-user.2mx.org/
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @version $Id: SellerModule.php 132 2011-10-30 10:45:01Z mishamx $
 */

class SellerModule extends CWebModule
{
	public $layout='//layouts/seller/column1';

	/**
	 * @var int
	 * @desc 淘宝信息
	 */
	public $AppKey;
	public $SecretKey;

	/**
	 * @var int
	 * @desc items on page
	 */
	public $page_size = 20;
	public $fake_page_size = 20;

	/**
	 * @var array
	 * @desc Behaviors for models
	 */
	public $componentBehaviors=array();

	public function init()
	{
		// import the module-level models and components
		$this->setImport(array(
			'seller.models.*',
			'seller.components.*',
			'application.vendors.taobao.*',
		));

		self::initSeller(); //初始化uid,nick,seesionkey
	}

	//初始化uid,nick,seesionkey到user
	public function initSeller(){
        if (!Yii::app()->user->id)
        	return false;

        if (!isset(Yii::app()->user->tbid) || !isset(Yii::app()->user->tbnick)) {
        	if(Yii::app()->controller->route == 'seller/shop/manage')
        		return true;

			$user = User::model()->belong()->find();
			if($user->major_shop)
				$shop = Shop::model()->belong()->setSession()->findByPk($user->major_shop);
			else
				$shop = Shop::model()->belong()->setSession()->find();

			if(!$shop)
			{
				Yii::app()->user->setFlash('manageshop',UserModule::t("One more shop need for you."));
				$this->redirect('seller/shop/manage');
			}
			else
			{
				$user->major_shop = $shop->id;
				$user->save();
			}

			Yii::app()->user->setState('tbid', $shop->id);
			Yii::app()->user->setState('tbnick', $shop->nick);
			Yii::app()->user->setState('tbkey', $shop->session_key);
		}
	}

	public function getBehaviorsFor($componentName){
        if (isset($this->componentBehaviors[$componentName])) {
            return $this->componentBehaviors[$componentName];
        } else {
            return array();
        }
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}

	/**
	 * @param $str
	 * @param $params
	 * @param $dic
	 * @return string
	 */
	public static function t($str='',$params=array(),$dic='seller') {
		if (Yii::t("SellerModule", $str)==$str)
		    return Yii::t("SellerModule.".$dic, $str, $params);
        else
            return Yii::t("SellerModule", $str, $params);
	}

	/**
	 * Return admins.
	 * @return array syperusers names
	 */
	public static function getAdmins() {
		if (!self::$_admins) {
			$admins = User::model()->active()->superuser()->findAll();
			$return_name = array();
			foreach ($admins as $admin)
				array_push($return_name,$admin->username);
			self::$_admins = ($return_name)?$return_name:array('');
		}
		return self::$_admins;
	}

	/**
	 * Return safe user data.
	 * @param user id not required
	 * @return user object or false
	 */
	public static function user($id=0,$clearCache=false) {
        if (!$id&&!Yii::app()->user->isGuest)
            $id = Yii::app()->user->id;
		if ($id) {
            if (!isset(self::$_users[$id])||$clearCache)
                self::$_users[$id] = User::model()->with(array('profile'))->findbyPk($id);
			return self::$_users[$id];
        } else return false;
	}

	/**
	 * Return safe user data.
	 * @param user name
	 * @return user object or false
	 */
	public static function majorShop($shop_id) {
		if(Yii::app()->user->isGuest)
			return false;
		else {
			if (!isset(self::$_majorShop)) {
				if(self::user()->majorShop)
					self::$_majorShop = self::user()->majorShop;
				else
					self::$_majorShop = false;
			}
			return self::$_majorShop;
		}
	}
}
