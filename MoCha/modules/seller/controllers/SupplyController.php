<?php

class SupplyController extends Controller
{
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index', 'list', 'type', 'create', 'edit', 'aedit', 'delete', 'setMajor'),
				'users'=>array('*'),
			),
		);
	}

	/**
	 * AJAX获取列表
	 */
	public function actionList()
	{
		$searchColumns = array('name', 'memo');

		$criteria=new CDbCriteria(array(
			'scopes'=>array('belong','notsafe'),
			'with'=>array(
				'product'=>array('scopes'=>'simple'),
				'supplier'=>array('scopes'=>'simple')
			),

			'limit'=>100,
		));

		$DataTable = new DataTable('Supply', $criteria, $alias, $searchColumns);
		$output = $DataTable->output();
		$data = $DataTable->data();

		foreach ($data as $model) {
			$row= Supply::formatHtml($model->attributes);
			$row['product'] = $model->product->name;
			$row['supplier'] = $model->supplier->name;

			$row['DT_RowId'] = $model['id'];
			$output['aaData'][] = $row;
		}

		$this->ajaxReturn($output,null);
	}

	/**
	 * 设置首先供应商
	 */
	public function actionSetMajor($supply_id, $product_id)
	{
		$result = Supply::setMajor($supply_id, $product_id);

		if($result)
			$this->ajaxReturn(Ya::t('Operation Success'));
		else
			$this->ajaxReturn(Ya::t('Operation Failure'));
	}

}