<?php

class SupplierController extends Controller
{
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index', 'list', 'type', 'create', 'edit', 'aedit', 'delete', 'choose'),
				'users'=>array('*'),
			),
		);
	}

	/**
	 * 列表
	 */
	public function actionIndex()
	{
		$tree = new Tree();
        $tree->icon = array('│ ','├─ ','└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $array = array();

		$condition = "module='".Type::MODULE_SUPPLIER."'";
        $types = Type::model()->belong()->active()->listorder()->simple()->findAll($condition);
        foreach($types as $r) {
        	$type['id'] = $r->id;
        	$type['name'] = $r->name;
        	$type['parent_id'] = $r->parent_id;
            $array[] = $type;
        }
        $str  = "<li><a data-toggle='tab' href='#type-\$id' onclick='javascript:filtering(\$id);'>\$spacer\$name</a></li>";
        $tree->init($array);
        $list = $tree->get_tree(0, $str);


        $this->render('index',array('types'=>$list));
	}
	
	/**
	 * 类别
	 */
	public function actionType()
	{
		$tree = new Tree();
        $tree->icon = array('│ ','├─ ','└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $array = array();

		$criteria=new CDbCriteria(array(
			'condition' => 'module=:module',
			'params'=>array(':module'=>Type::MODULE_SUPPLIER),
			'scopes'=>array('belong','notsafe'),
			'limit'=>100,
			'order'=>'list_order ASC',
		));
		$suppliers = Type::model()->findAll($criteria);

        foreach($suppliers as $r) {
        	$supplier['id'] = $r->id;
        	$supplier['name'] = $r->name;
        	$supplier['parent_id'] = $r->parent_id;
        	$supplier['list_order'] = $r->list_order;
        	$supplier['memo'] = $r->memo;
        	$supplier['created'] = date('Y-m-d H:i:s', $r->created);
        	$supplier['modified'] = date('Y-m-d H:i:s', $r->modified);
        	$supplier['str_status'] = '<img data-tdtype="toggle" data-id="'.$r->id.'" data-field="status" data-value="'.$r->status.'" src="/asset/img/toggle_' . ($r->status == 0 ? 'disabled' : 'enabled') . '.gif" />';
        	$supplier['str_manage'] = '<div class="btn-group pull-right">
        			<a href="javascript:;" class="iDialog btn btn-mini btn-danger" data-uri="'.$this->createUrl("/type/edit",array("module"=>"supplier", "id"=>$r->id)).'" data-title="编辑'.$r->name.'" data-id="TypeForm" data-ok="更新">编辑</a>
        			<a href="javascript:;" class="iConfirm btn btn-mini" data-msg="确定要删除'.$r->name.'吗？" data-uri="'.$this->createUrl("/type/delete",array("module"=>"supplier", "id"=>$r->id)).'" data-ok="删除" data-acttype="ajax">删除</a>
        			</div>';
            $array[] = $supplier;
        }
        $str  = "
        	<tr id='\$id'>
        		<td><input class='iCheck' data-form='uniform' type='checkbox' name='icheck' value='\$id'></td>
        		<td>\$spacer<span class='ajaxedit' data-id='\$id' data-field='name' data-tdtype='edit'>\$name</span></td>
        		<td>\$str_status</td>
        		<td><span class='ajaxedit' data-id='\$id' data-field='list_order' data-tdtype='edit'>\$list_order</span></td>
        		<td align='center'>\$created</td>
        		<td align='center'>\$modified</td>
        		<td><span class='ajaxedit' data-id='\$id' data-field='memo' data-tdtype='edit'>\$memo</span></td>
        		<td>\$str_manage</td>
        	</tr>";
        $tree->init($array);
        $list = $tree->get_tree(0, $str);
        
        $this->render('type',array('list'=>$list));
	}

	/**
	 * AJAX获取列表
	 */
	public function actionList()
	{
		$searchColumns = array('name', 'phone', 'qq', 'wangwang');

		$criteria=new CDbCriteria(array(
			'scopes'=>array('belong','notsafe'),
			'limit'=>100,
		));

		$DataTable = new DataTable('Supplier', $criteria, $searchColumns);
		$output = $DataTable->output();
		$data = $DataTable->data();

		foreach ($data as $model) {
			$row= Supplier::formatHtml($model->attributes);

			$row['DT_RowId'] = $model['id'];
			$output['aaData'][] = $row;
		}

		$this->ajaxReturn($output,null);
	}

	/**
	 * AJAX获取下拉列表
	 */
	public function actionChoose($name)
	{
		$criteria=new CDbCriteria(array(
			'scopes'=>array('belong', 'simple'),
			'limit'=>50,
			'order'=>'name ASC',
		));
		$criteria->compare('name',$name,true);

		$suppliers = Supplier::model()->findAll($criteria);

		$childs = array();
		if($suppliers != array())
		{
			foreach ($suppliers as $supplier)
				$childs[] = array('id'=>$supplier->id, 'name'=>$supplier->name);

			$this->ajaxReturn($childs);
		}
		else
			$this->ajaxReturn(array(), 0);
	}
}