<?php

class SkuController extends Controller
{
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index', 'list', 'create', 'choose', 'chooseid', 'edit', 'aedit', 'delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
     * 批量创建SKU
     */
    public function actionCreate($product_id)
	{
		if(isset($_POST['Skus']))
		{
			$status = $_POST['Skus']['status'];
			$skus = $_POST['Skus']['sku'];
			if(count($skus))
			{
				foreach ($skus as $key=>$sku) {
					if(!$sku['name']) continue;

					$model = new Sku();
					$model->product_id = $product_id;
					$model->status = $status;
					$model->attributes = $sku;
					$model->setBuiingPrice(); //设置经常商价格

					$model->save(); //保存
				}
				if(Yii::app()->request->isAjaxRequest)
					$this->ajaxReturn(Ya::t('Operation Success'));
				else
				{
					Yii::app()->user->setFlash('OperationMessage',Yii::t('common', "Changes is saved."));
					$this->redirect(Yii::app()->request->urlReferrer);
				}
				
			}
			else
				$this->ajaxReturn('请先填写相关内容',0);
		}
		$model = new Sku();

		$criteria=new CDbCriteria(array(
			'condition'=>'product.id=:id',
			'params'=>array(':id'=>$product_id),
			'scopes'=>array('belong','notsafe'),
			'with'=>array(
				'supply'=>array(
					'scopes'=>'simple',
					'with'=>array(
						'supplier'=>array('scopes'=>'simple')
					)
				)
			)
		));
		$product = Product::model()->find($criteria);

		if(!count($product->supply))
			$this->ajaxReturn('请先给产品添加供应商');

		$supplies = array();
		foreach ($product->supply as $supply)
			$supplies[$supply->id] = $supply->supplier->name.'-'.$supply->name.'-'.$supply->price;

		if(Yii::app()->request->isAjaxRequest)
		{
			$data = $this->renderPartial('create',array('model'=>$model,'supplies'=>$supplies, 'product'=>$product),true);
			$this->ajaxReturn($data);
		}
		else
			$this->render('create',array('model'=>$model,'supplies'=>$supplies, 'product'=>$product));
	}

	/**
	 * AJAX获取下拉列表
	 */
	public function actionChoose($name)
	{
		$criteria=new CDbCriteria(array(
			'scopes'=>array('belong', 'simple'),
			'limit'=>50,
			'order'=>'sku_1 ASC',
		));
		$criteria->compare('name',$name,true);

		$Skus = Sku::model()->findAll($criteria);

		$childs = array();
		if($Skus != array())
		{
			foreach ($Skus as $sku)
				$childs[] = array('id'=>$sku->id, 'name'=>$sku->name. '-' .$sku->sku_1 . ' '. $sku->sku_2);

			$this->ajaxReturn($childs);
		}
		else
			$this->ajaxReturn(array(), 0);
	}

	/**
	 * AJAX获取下拉列表
	 */
	public function actionChooseId($id)
	{
		$Sku = Sku::model()->findByPk($id);
		if($Sku)
		{
			$name =$Sku->name. '-' .$Sku->sku_1 . ' '. $Sku->sku_2;
			$this->ajaxReturn(array('id'=>$Sku->id, 'name'=>$name));
		}
		else
			$this->ajaxReturn(array(), 0);
	}
}