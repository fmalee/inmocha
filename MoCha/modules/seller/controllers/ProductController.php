<?php

class ProductController extends Controller
{
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index', 'list', 'type', 'view', 'create', 'edit', 'aedit', 'delete', 'choose', 'importTaobao', 'randomSerial', 'checkSerial'),
				'users'=>array('*'),
			),
		);
	}

	/**
	 * 列表
	 */
	public function actionIndex()
	{
		$tree = new Tree();
        $tree->icon = array('│ ','├─ ','└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $array = array();

        $condition = "module='".Type::MODULE_PRODUCT."'";
        $types = Type::model()->belong()->active()->listorder()->simple()->findAll($condition);
        foreach($types as $r) {
        	$type['id'] = $r->id;
        	$type['name'] = $r->name;
        	$type['parent_id'] = $r->parent_id;
            $array[] = $type;
        }
        $str  = "<li><a data-toggle='tab' href='#type-\$id' onclick='javascript:filtering(\$id);'>\$spacer\$name</a></li>";
        $tree->init($array);
        $list = $tree->get_tree(0, $str);


        $this->render('index',array('types'=>$list));
	}

	/**
     * 淘宝商品导入
     */
    public function actionImportTaobao($id)
    {
		if(isset($_POST['Import']))
		{
			$type_id = $_POST['Import']['type_id'];
			$label = $_POST['Import']['label'];
			$stock = $_POST['Import']['stock'];

			$ids = explode(',', $id);
			foreach ($ids as $pk)
				Product::import($pk, $type_id, $stock, $label);

			if(Yii::app()->request->isAjaxRequest)
				$this->ajaxReturn(Yii::t('common', 'Operation Success'));
			else
			{
				Yii::app()->user->setFlash('OperationMessage',Yii::t('common', "Changes is saved."));
				$this->redirect(Yii::app()->request->urlReferrer);
			}
		}
		if(Yii::app()->request->isAjaxRequest)
		{
			$data = $this->renderPartial('import',array(),true);
			$this->ajaxReturn($data);
		}
		else
			$this->render('import');
    }

	/**
	 * 随机获取编码
	 */
	public function actionRandomSerial() {
		$serial = Product::randomSerial();
		$this->ajaxReturn($serial);
	}

	/**
	 * 检查编码是否存在
	 */
	public function actionCheckSerial($serial) {
		$criteria=new CDbCriteria(array(
			'condition'=>'name=:name',
			'params'=>array(':name'=>$serial),
		));
		$exist = self::model()->belong()->simple()->exists($criteria);
		if ($exist) {
			$this->ajaxReturn('编码已存在！', 0);
		} else {
			$this->ajaxReturn('有效编码');
		}
	}

	/**
	 * 类别
	 */
	public function actionType()
	{
		$tree = new Tree();
        $tree->icon = array('│ ','├─ ','└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $array = array();

		$criteria=new CDbCriteria(array(
			'condition' => 'module=:module',
			'params'=>array(':module'=>Type::MODULE_PRODUCT),
			'scopes'=>array('belong','notsafe'),
			'limit'=>100,
			'order'=>'list_order ASC',
		));
		$departments = Type::model()->findAll($criteria);

        foreach($departments as $r) {
        	$department['id'] = $r->id;
        	$department['name'] = $r->name;
        	$department['parent_id'] = $r->parent_id;
        	$department['list_order'] = $r->list_order;
        	$department['memo'] = $r->memo;
        	$department['created'] = date('Y-m-d H:i:s', $r->created);
        	$department['modified'] = date('Y-m-d H:i:s', $r->modified);
        	$department['str_status'] = '<img data-tdtype="toggle" data-id="'.$r->id.'" data-field="status" data-value="'.$r->status.'" src="/asset/img/toggle_' . ($r->status == 0 ? 'disabled' : 'enabled') . '.gif" />';
        	$department['str_manage'] = '<div class="btn-group pull-right">
        			<a href="javascript:;" class="iDialog btn btn-mini btn-danger" data-uri="'.$this->createUrl("/type/edit",array("module"=>"product", "id"=>$r->id)).'" data-title="编辑'.$r->name.'" data-id="TypeForm" data-ok="更新">编辑</a>
        			<a href="javascript:;" class="iConfirm btn btn-mini" data-msg="确定要删除'.$r->name.'吗？" data-uri="'.$this->createUrl("/type/delete",array("module"=>"product", "id"=>$r->id)).'" data-ok="删除" data-acttype="ajax">删除</a>
        			</div>';
            $array[] = $department;
        }
        $str  = "
        	<tr id='\$id'>
        		<td><input class='iCheck' data-form='uniform' type='checkbox' name='icheck' value='\$id'></td>
        		<td>\$spacer<span class='ajaxedit' data-id='\$id' data-field='name' data-tdtype='edit'>\$name</span></td>
        		<td>\$str_status</td>
        		<td><span class='ajaxedit' data-id='\$id' data-field='list_order' data-tdtype='edit'>\$list_order</span></td>
        		<td align='center'>\$created</td>
        		<td align='center'>\$modified</td>
        		<td><span class='ajaxedit' data-id='\$id' data-field='memo' data-tdtype='edit'>\$memo</span></td>
        		<td>\$str_manage</td>
        	</tr>";
        $tree->init($array);
        $list = $tree->get_tree(0, $str);


        $this->render('type',array('list'=>$list));
	}

	/**
	 * AJAX获取列表
	 */
	public function actionList()
	{
		$searchColumns = array('name', 'title', 'memo');

		$criteria=new CDbCriteria(array(
			'scopes'=>array('belong','notsafe'),
			'limit'=>100,
		));

		$DataTable = new DataTable('Product', $criteria, $searchColumns);
		$output = $DataTable->output();
		$data = $DataTable->data();

		foreach ($data as $model) {
			$row= Product::formatHtml($model->attributes);

			$row['DT_RowId'] = $model['id'];
			$output['aaData'][] = $row;
		}

		$this->ajaxReturn($output,null);
	}

	/**
	 * AJAX获取下拉列表
	 */
	public function actionChoose($name)
	{
		$criteria=new CDbCriteria(array(
			'scopes'=>array('belong', 'simple'),
			'limit'=>50,
			'order'=>'name ASC',
		));
		$criteria->compare('name',$name,true);

		$products = Product::model()->findAll($criteria);

		$childs = array();
		if($products != array())
		{
			foreach ($products as $product)
				$childs[] = array('id'=>$product->id, 'name'=>$product->name);

			$this->ajaxReturn($childs);
		}
		else
			$this->ajaxReturn(array(), 0);
	}
}