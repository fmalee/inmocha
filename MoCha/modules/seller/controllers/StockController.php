<?php

class StockController extends Controller
{
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index', 'list', 'create', 'inferior', 'bath', 'aedit', 'delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
     * 调整库存
     */
    public function actionCreate($sku_id, $type)
	{
		$model = new Stock();

		if(isset($_POST['Stock']))
		{
			$model->attributes = $_POST['Stock'];
			if($model->save())
			{
				$model->updateSkuStock(); //更新库存数量

				if(Yii::app()->request->isAjaxRequest)
					$this->ajaxReturn(Yii::t('common', 'Operation Success'));
				else
				{
					Yii::app()->user->setFlash('OperationMessage',Yii::t('common', "Changes is saved."));
					$this->redirect(Yii::app()->request->urlReferrer);
				}
			}
			else{
				if(Yii::app()->request->isAjaxRequest)
				{
					$validate = Ya::formatValidate(CActiveForm::validate($model));
					$this->ajaxReturn($validate,0);
				}
				else
				{
					$validate = CActiveForm::validate($model);
					throw new CHttpException(400,Yii::t('common', 'Invalid request. Please do not repeat this request again.'));
				}
			}
		}
		$model->sku_id = $sku_id;
		$model->type = $type;

		$criteria=new CDbCriteria(array(
			'condition'=>'sku.id=:id',
			'params'=>array(':id'=>$sku_id),
			'scopes'=>array('belong','simple'),
			'with'=>array(
				'product'=>array(
					'scopes'=>'simple',
					'with'=>array(
						'supply'=>array(
							'scopes'=>'simple',
							'with'=>array(
								'supplier'=>array('scopes'=>'simple')
							)
						)
					)
				),
				//'product.supply.supplier',
			),
		));
		$skus = Sku::model()->find($criteria);

		if(!count($skus->product->supply))
			$this->ajaxReturn('请先给产品添加供应商');

		$model->price = $skus->buiing_price;
		$model->supply_id = $skus->supply_id;

		$supplies = array('0'=>'暂无供应商');
		foreach ($skus->product->supply as $supply)
			$supplies[$supply->id] = $supply->supplier->name.'-'.$supply->name.'-'.$supply->price;

		if(Yii::app()->request->isAjaxRequest)
		{
			$data = $this->renderPartial('create',array('model'=>$model,'supplies'=>$supplies),true);
			$this->ajaxReturn($data);
		}
		else
			$this->render('create',array('model'=>$model,'supplies'=>$supplies));
	}

	/**
     * 调整次品库存
     */
    public function actionInferior($sku_id, $type)
	{
		$model = new Stock();

		if(isset($_POST['Stock']))
		{
			$model->attributes = $_POST['Stock'];
			if($model->save())
			{
				$model->updateSkuStock(); //更新库存数量

				if(Yii::app()->request->isAjaxRequest)
					$this->ajaxReturn(Yii::t('common', 'Operation Success'));
				else
				{
					Yii::app()->user->setFlash('OperationMessage',Yii::t('common', "Changes is saved."));
					$this->redirect(Yii::app()->request->urlReferrer);
				}
			}
			else{
				if(Yii::app()->request->isAjaxRequest)
				{
					$validate = Ya::formatValidate(CActiveForm::validate($model));
					$this->ajaxReturn($validate,0);
				}
				else
				{
					$validate = CActiveForm::validate($model);
					throw new CHttpException(400,Yii::t('common', 'Invalid request. Please do not repeat this request again.'));
				}
			}
		}
		$model->sku_id = $sku_id;
		$model->type = $type;

		$criteria=new CDbCriteria(array(
			'condition'=>'sku.id=:id',
			'params'=>array(':id'=>$sku_id),
			'scopes'=>array('belong','simple'),
		));
		$sku = Sku::model()->find($criteria);

		$model->price = $sku->buiing_price;
		$model->supply_id = $sku->supply_id;

		if(Yii::app()->request->isAjaxRequest)
		{
			$data = $this->renderPartial('inferior',array('model'=>$model),true);
			$this->ajaxReturn($data);
		}
		else
			$this->render('inferior',array('model'=>$model));
	}

	/**
     * 批量创建SKU
     */
    public function actionBath($product_id, $type)
	{
		if(isset($_POST['Stock']))
		{
			$type = $_POST['Stock']['type'];
			$skus = $_POST['Stock']['sku'];
			if(count($skus))
			{
				foreach ($skus as $sku) {
					if(!$sku['stock'] && !$sku['inferior']) continue;

					$model = new Stock();
					$model->type = $type;
					$model->attributes = $sku;

					$model->save(); //保存

					$model->updateSkuStock(); //更新库存数量
				}
				if(Yii::app()->request->isAjaxRequest)
					$this->ajaxReturn(Ya::t('Operation Success'));
				else
				{
					Yii::app()->user->setFlash('OperationMessage',Yii::t('common', "Changes is saved."));
					$this->redirect(Yii::app()->request->urlReferrer);
				}
				
			}
		}
		$model = new Stock();
		$model->type = $type;

		$criteria=new CDbCriteria(array(
			'condition'=>'product.id=:id',
			'params'=>array(':id'=>$product_id),
			'scopes'=>array('belong','simple'),
			'with'=>array(
				'supply'=>array(
					'scopes'=>'simple',
					'with'=>array(
						'supplier'=>array('scopes'=>'simple')
					)
				),
				'sku'=>array('scopes'=>'simple')
			)
		));
		$product = Product::model()->find($criteria);

		if(!count($product->supply))
			$this->ajaxReturn('请先给产品添加供应商');

		$supplies = array();
		foreach ($product->supply as $supply)
			$supplies[$supply->id] = $supply->supplier->name.'-'.$supply->name.'-'.$supply->price;

		if(Yii::app()->request->isAjaxRequest)
		{
			$data = $this->renderPartial('bath',array('model'=>$model,'supplies'=>$supplies, 'product'=>$product),true);
			$this->ajaxReturn($data);
		}
		else
			$this->render('bath',array('model'=>$model,'supplies'=>$supplies, 'product'=>$product));
	}
}