<?php

class ServiceController extends Controller
{
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index', 'fund', 'talk', 'return', 'reissue', 'niffer', 'new', 'type', 'edit', 'aedit', 'delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * 列表
	 */
	public function actionIndex()
	{
		$criteria=new CDbCriteria(array(
			'scopes'=>array('belong','notsafe'),
			'order'=>'service.created DESC',
			'with'=>array(
				'trade'=>array('scopes'=>'simple')
			)
		));

		$dataProvider=new CActiveDataProvider('Service', array(
			'pagination'=>array(
				'pageSize'=>5
			),
			'criteria'=>$criteria,
		));

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * 财务记录
	 */
	public function actionFund()
	{
		$criteria=new CDbCriteria(array(
			'condition'=>'service.module=:module',
			'params'=>array(':module'=>'fund'),
			'scopes'=>array('belong','notsafe'),
			'order'=>'service.id DESC',
			'with'=>array(
				'trade',
				'fund'
			)
		));

		$dataProvider=new CActiveDataProvider('Service', array(
			'pagination'=>array(
				'pageSize'=>5
			),
			'criteria'=>$criteria,
		));

		$this->render('fund',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * 补发记录
	 */
	public function actionReissue()
	{
		$criteria=new CDbCriteria(array(
			'condition'=>'service.module=:module',
			'params'=>array(':module'=>'reissue'),
			'scopes'=>array('belong','notsafe'),
			'order'=>'service.id DESC',
			'with'=>array(
				'trade',
				'reissue'
			)
		));

		$dataProvider=new CActiveDataProvider('Service', array(
			'pagination'=>array(
				'pageSize'=>5
			),
			'criteria'=>$criteria,
		));

		$this->render('reissue',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * 沟通记录
	 */
	public function actionTalk()
	{
		$criteria=new CDbCriteria(array(
			'condition'=>'service.module=:module',
			'params'=>array(':module'=>'talk'),
			'scopes'=>array('belong','notsafe'),
			'order'=>'service.id DESC',
			'with'=>array(
				'trade'
			)
		));

		$dataProvider=new CActiveDataProvider('Service', array(
			'pagination'=>array(
				'pageSize'=>5
			),
			'criteria'=>$criteria,
		));

		$this->render('talk',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
     * 修改
     */
    public function actionEdit($model)
    {
        $Service = $this->loadModel();
        if($model != 'talk')
        {
        	$Module = $Service->$model;
        	$modelName = get_class($Module);
        }

		if(isset($_POST['Service']))
		{
			$Service->attributes=$_POST['Service'];

			if($Service->save())
			{
				if($Module)
				{
					$Module->attributes=$_POST[$modelName];
					$Module->save();
				}

				if(Yii::app()->request->isAjaxRequest)
					$this->ajaxReturn(Yii::t('common', 'Operation Success'));
				else
				{
					Yii::app()->user->setFlash('OperationMessage',Yii::t('common', "Changes is saved."));
					$this->redirect(Yii::app()->request->urlReferrer);
				}
			}
			else
			{
				
				if(Yii::app()->request->isAjaxRequest)
				{
					$validate = Ya::formatValidate(CActiveForm::validate($Service));
					$this->ajaxReturn($validate,0);
				}
				else
				{
					$validate = CActiveForm::validate($Service);
					throw new CHttpException(400,Yii::t('common', 'Invalid request. Please do not repeat this request again.'));
				}
			}
		}
		if(Yii::app()->request->isAjaxRequest)
		{
			$data = $this->renderPartial($model.'_edit',array('module'=>$Module, 'service'=>$Service),true);
			$this->ajaxReturn($data);
		}
		else
			$this->render($model.'_edit',array('module'=>$Module, 'service'=>$Service));
		
    }

	/**
	 * 删除
	 */
	public function actionDelete($id)
	{
		$ids = explode(',', $id);
		foreach ($ids as $pk)
		{
			$result = Service::model()->belong()->simple()->findbyPk($pk);
			if($result)
			{
				$class = Service::itemAlias('model', $result->module);
				$result->delete(); //删除主表
				$class::model()->deleteByPk($id); //删除副表
			}
		}
		if(Yii::app()->request->isAjaxRequest)
			$this->ajaxReturn(Yii::t('common', 'Operation Success'));
		else
			$this->redirect(Yii::app()->request->urlReferrer);
	}

	/**
	 * 转换
	 */
	public function actionNew()
	{
		$criteria=new CDbCriteria(array(
			'condition'=>'product.id=:id',
			'params'=>array(':id'=>$product_id),
			'scopes'=>array('belong','notsafe'),
		));
		$status = array(
			'跟踪中'=> Service::STATUS_FOLLOW,
			'退货中'=> Service::STATUS_RETURN,
			'已收货'=> Service::STATUS_RECIEVE,
			'已发货'=> Service::STATUS_SEND,
			'失败'=> Service::STATUS_FAIL,
			'完成'=> Service::STATUS_FINISH,
		);
		$services = array(
			'fund' => array(
				'model' => 'ServiceFund',
				'type' => array(
					'质量' => '21',
					'物流' => '22',
					'缺货' => '23',
					'运费' => '24',
					'丢包' => '25',
					'恶意' => '26',
					'差价' => '27',
					'其他' => '28',
				),
			),
			'talk' => array(
				'model' => 'ServiceTalk',
				'type' => array(
					'质量' => '29',
					'物流' => '30',
					'缺货' => '31',
					'运费' => '32',
					'丢包' => '33',
					'改码' => '34',
					'恶意' => '35',
					'差价' => '36',
					'投票' => '37',
					'其他' => '38',
				),
			),
			'return' => array(
				'model' => 'ServiceReturn',
				'type' => array(
					'尺码' => '40',
					'质量' => '41',
					'色差' => '42',
					'款式' => '43',
					'发错' => '44',
					'价格' => '45',
					'缺货' => '46',
					'其他' => '47',
				),
			),
			'niffer' => array(
				'model' => 'ServiceNiffer',
				'type' => array(
					'尺码' => '48',
					'发错' => '49',
					'改码' => '50',
					'质量' => '51',
					'色差' => '52',
				),
			),
			'reissue' => array(
				'model' => 'ServiceReissue',
				'type' => array(
					'虚假发货' => '53',
					'漏发' => '54',
					'丢包' => '55',
					'改投' => '56',
					'缺货' => '57',
				),
			),
			
		);
		//先增加service_id,转好后删除ID，删除ServiceTalk表
		foreach ($services as $key => $service) {
			$module = $service['model'];
			$results = $module::model()->findAll();

			foreach ($results as $result)
			{
				$model = new Service;

				$model->trade_id = $result->tid;
				$model->status = $status[$result->status];
				$model->type_id = $service['type'][$result->reason] ? $service['type'][$result->reason] : 0;
				$model->memo = $result->memo;
				$model->created = $result->created;
				$model->modified = $result->modified;
				$model->module = $key;

				$model->save();

				$result->service_id = $model->id;
				$result->save();
			}
		}
	}
}