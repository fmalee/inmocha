<?php

class FakeController extends Controller
{
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index', 'list', 'type', 'create', 'edit', 'aedit', 'delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * AJAX获取刷单列表
	 */
	public function actionList()
	{
		$searchColumns = array('fake.id', 'fake.contact', 'fake.qq', 'fake.user');

		$criteria=new CDbCriteria(array(
			'scopes'=>array('belong','notsafe'),
			'with'=>array(
				'trade'=>array(
					'scopes'=>'forFake',
				)
			),
			'limit'=>100,
		));

		$DataTable = new DataTable('Fake', $criteria, $searchColumns);
		$output = $DataTable->output();
		$data = $DataTable->data();

		foreach ($data as $model) {
			$row['fake']= Fake::formatHtml($model->attributes);
			$row['trade'] = Trade::formatHtml($model->trade->attributes);

			$row['DT_RowId'] = $model['id'];
			$output['aaData'][] = $row;
		}

		$this->ajaxReturn($output,null);
	}

	/**
     * 新增
     */
    public function actionCreate($id)
	{
		$model=new Fake;
		if(isset($_POST['Fake']))
		{
			$model->attributes = $_POST['Fake'];
			if($model->validate())
			{
				$model->save();
				if(Yii::app()->request->isAjaxRequest)
					$this->ajaxReturn(Yii::t('common', 'Operation Success'));
				else
				{
					Yii::app()->user->setFlash('Operation Message',Yii::t('common', "Changes is saved."));
					$this->redirect(Yii::app()->request->urlReferrer);
				}
			}
			else{
				$validate = CActiveForm::validate($model);
				if(Yii::app()->request->isAjaxRequest)
					$this->ajaxReturn($validate,0);
				else
					throw new CHttpException(400,Yii::t('common', 'Invalid request. Please do not repeat this request again.'));
			}
		}
		$trade = Trade::model()->forFake()->belong()->findbyPk($id);
		if(!$trade)
			$this->ajaxReturn(Yii::t('common', 'Invalid Trade ID.'));

		$data = $this->renderPartial('create',array('model'=>$model, 'trade'=>$trade),true);
		$this->ajaxReturn($data);
	}
}