<?php

class TradeController extends Controller
{
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index', 'list', 'trade', 'memo', 'syn', 'aedit', 'delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * 列表
	 */
	public function actionIndex()
	{
		$criteria=new CDbCriteria(array(
			'scopes'=>array('belong','notsafe'),
			'order'=>'trade.created DESC',
			'with'=>array(
				/*'order'=>array(
					'scopes'=>'simple',
					'with'=>array(
						'refund'=>array('scopes'=>'simple')
					)
				),*/
				//'invoice'=>array('scopes'=>'forcheck')
			)
		));

		$dataProvider=new CActiveDataProvider('Trade', array(
			'pagination'=>array(
				'pageSize'=>5
			),
			'criteria'=>$criteria,
		));

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * 增加备注
	 */
	public function actionMemo($id) {
		$model = $this->loadModel();
		if(isset($_POST['Memo']))
		{
			$result = Taobao::model()->TradeMemo($id, $_POST['Memo']);
			Yii::log($result, 'info');
			if ($result)
				$this->ajaxReturn(Ya::t('Operation Success'));
			else
				$this->ajaxReturn($result, 0);
		}

		$data = $this->renderPartial('memo',array('model'=>$model,),true);
		$this->ajaxReturn($data);
		//$this->render('syn',array('model'=>$model));
	}

	/**
	 * 同步订单准备
	 */
	public function actionSyn($status = 0, $page = 1, $start = 0, $end = 0, $pagesize = 0, $pagenum = 0, $total = 0) {
		if(isset($_POST['Syn']))
		{
			$time = $_POST['Syn']['time'];
			$start = date('Y-m-d H:i:s',  time() - $time);
			$end = date('Y-m-d H:i:s',  time());
			$status = $_POST['Syn']['status'] ? $_POST['Syn']['status'] : 0;
			$pagesize = 49;
		}
		if ($pagesize)
		{
			$where = array();
			$where['start_created'] = $start;
			$where['end_created'] = $end;
			$where['page_no'] = $page;
			$where['page_size'] = $pagesize;
			if($status) $where['status'] = $status;

			if (!$pagenum) {
				$result = Taobao::model()->tradeSold($where, 'tid'); //更新订单信息
		        
		        if (isset($result['trades_sold_get_response']['total_results'])) {
		            $total = $result['trades_sold_get_response']['total_results'];
		        }

		        $pagenum = ceil($total / $pagesize); //计算总数
			}
			if ($total > 0) {
				Taobao::model()->synTrades($where); //更新订单信息

				if ($pagenum > $page)
				{
					++$page;
					$msg = "正在操作第 $page / $pagenum 批，共 $total 个记录。";
					$url = $this->createUrl('syn',array(
						'pagesize'=>$pagesize,
						'page'=>$page,
						'status'=>$status,
						'start'=>$start,
						'end'=>$end,
						'total'=>$total,
						'pagenum'=>$pagenum,
					));
					
					$this->jump($msg, $url, 1);
				}
			}
			
			$this->redirect('/seller/trade');
		}
		$data = $this->renderPartial('syn',array('model'=>$model),true);
		$this->ajaxReturn($data);
	}

	/**
	 * 同步订单开始
	 */
	public function syn1() {
		$page = $this->_get('page', 'intval', 1);
		$cache = unserialize(cookie('trade_syn'));
		!$cache && $this->error('参数错误，请重试');
		
		$Taoapi = new Taoapi();
		$Trade = D('Trade');

		$where = array();
		$where['start_created'] = $cache['start'];
		$where['end_created'] = $cache['end'];
		$where['page_no'] = $page;
		$where['page_size'] = $cache['pagesize'];
		if ($cache['status']) $where['status'] = $cache['status'];

		if (!$cache['pagenum']) {
			$result = $Trade->synSold($where, 'tid'); //更新订单信息
	        
	        if (isset($result['trades_sold_get_response']['total_results'])) {
	            $total = $result['trades_sold_get_response']['total_results'];
	        }

			$total_page = ceil($total / $cache['pagesize']); //计算总数
			$cache['pagenum'] = $total_page;
			$cache['total'] = $total;
			cookie('trade_syn', serialize($cache));
		}
		if ($cache['total'] > 0) {
			$Trade->synTrades($where); //更新订单信息

			if ($cache['pagenum'] > $page) {
				$this->assign('pagenum', $cache['pagenum']);
				$this->assign('total', $cache['total']);
				$this->assign('page', $page);
				$resp = $this->fetch('Public:syn');
				$this->ajaxReturn($resp, '同步订单记录', 1);
			}
		}
		cookie('trade_syn', null); //删除缓存
		$this->error('同步完成，将刷新页面');
	}

	/**
	 * 表格
	 */
	public function actionTrade()
	{
		$searchColumns = array('name', 'memo');

		$criteria=new CDbCriteria(array(
			'scopes'=>array('belong','notsafe'),
			'with'=>array(
				'product'=>array('scopes'=>'simple'),
				'supplier'=>array('scopes'=>'simple')
			),

			'limit'=>100,
		));

		$DataTable = new DataTable('Invoice', $criteria, $alias, $searchColumns);
		$output = $DataTable->output();
		$data = $DataTable->data();

		foreach ($data as $model) {
			$row= Supply::formatHtml($model->attributes);
			$row['product'] = $model->product->name;
			$row['supplier'] = $model->supplier->name;

			$row['DT_RowId'] = $model['id'];
			$output['aaData'][] = $row;
		}

		$this->ajaxReturn($output,null);
	}
}