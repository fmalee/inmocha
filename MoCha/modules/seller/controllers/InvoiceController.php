<?php

class InvoiceController extends Controller
{
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index', 'list', 'trade', 'test', 'create', 'edit', 'aedit', 'delete', 'read', 'new'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionTest($id)
	{
		//Invoice::autoIpt($id, array('trade,niffer,reissue'));
		Invoice::autoIpt($id, array('1'));
	}
	/**
	 * 列表
	 */
	public function actionIndex()
	{
		$criteria=new CDbCriteria(array(
			'scopes'=>array('belong','notsafe'),
			'order'=>'invoice.id DESC',
			'with'=>'tradeCount',
		));
		if(isset($_GET['tag']))
			$criteria->addSearchCondition('tags',$_GET['tag']);

		$dataProvider=new CActiveDataProvider('Invoice', array(
			'pagination'=>array(
				'pageSize'=>20
			),
			'criteria'=>$criteria,
		));

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
     * 新增
     */
    public function actionCreate()
	{
		$model = new Invoice;

		if(isset($_POST['Invoice']))
		{
			$model->attributes = $_POST['Invoice'];
			if($model->save())
			{
				if(isset($_POST['Invoice']['import']) && $_POST['Invoice']['import'])
					Invoice::autoIpt($model->id, $_POST['Invoice']['import']); //自动导入

				if(Yii::app()->request->isAjaxRequest)
					$this->ajaxReturn(Yii::t('common', 'Operation Success'));
				else
				{
					Yii::app()->user->setFlash('OperationMessage',Yii::t('common', "Changes is saved."));
					$this->redirect(Yii::app()->request->urlReferrer);
				}
			}
			else{
				if(Yii::app()->request->isAjaxRequest)
				{
					$validate = Ya::formatValidate(CActiveForm::validate($model));
					$this->ajaxReturn($validate,0);
				}
				else
				{
					$validate = CActiveForm::validate($model);
					throw new CHttpException(400,Yii::t('common', 'Invalid request. Please do not repeat this request again.'));
				}
			}
		}
		$model->name = "配货单" . date('Ymd');
		if(Yii::app()->request->isAjaxRequest)
		{
			$data = $this->renderPartial('create',array('model'=>$model,),true);
			$this->ajaxReturn($data);
		}
		else
			$this->render('create',array('model'=>$model));
	}

	/**
	 * 表格
	 */
	public function actionList($id)
	{
		$model = $this->loadModel();
		$criteria=new CDbCriteria(array(
			'condition'=>'invoiceTrade.invoice_id=:invoice_id',
			'params'=>array(':invoice_id'=>$id),
			'scopes'=>'notsafe',
			'order'=>'invoiceTrade.trade_id DESC',
			'with'=>array(
				'invoiceOrder'=>array('scopes'=>'notsafe'),
				'trade'=>array('scopes'=>'simple'),
			),

		));

		$dataProvider=new CActiveDataProvider('InvoiceTrade', array(
			'pagination'=>array(
				'pageSize'=>200
			),
			'criteria'=>$criteria,
		));

		$this->render('list',array(
			'dataProvider'=>$dataProvider,
			'model'=>$model,
		));
	}

	/**
	 * 配货单打印
	 * @param integer $id 配货单ID
	 * @param integer $trade
	 */
	public function actionEdit($id, $trade)
	{
		$invoice = $this->loadModel();
		if($invoice->archive)
			$this->ajaxReturn(Ya::t('Operation Failure'), 0);
		if(isset($_POST['Sku']))
		{
			Yii::log(serialize($_POST), 'info');
			//Yii::app()->end();
			$condition = 'invoice_id=:invoice_id and trade_id=:trade_id';
			$params = array(':invoice_id'=>$id,':trade_id'=>$trade);
			InvoiceOrder::model()->deleteAll($condition, $params);

			foreach($_POST['Sku'] as $key=>$sku)
			{
				if(!$sku && !isset($_POST['Order'][$key]) && !isset($_POST['quantity'][$key]))
					continue;
				$quantity = $_POST['Quantity'][$key] ? $_POST['Quantity'][$key] : 1;
				$criteria=new CDbCriteria(array(
					'condition'=>'sku.id=:id',
					'params'=>array(':id'=>$sku),
					'scopes'=>array('belong','notsafe'),
					'with'=>array(
						'supply'=>array('scopes'=>'simple'),
					),
				));
				$skus = Sku::model()->find($criteria);
				if(!$skus)
					continue;
				
				$is_exists = InvoiceOrder::model()->exists("sku_id=:sku and trade_id=:trade",array(":sku"=>$skus->id,":trade"=>$trade));
		        if(!$is_exists)
		        {
		            $model = new InvoiceOrder;
		            $model->invoice_id = $id;
					$model->trade_id = $trade;
					$model->product_id = $skus->product_id;
					$model->sku_id = $skus->id;
					$model->supply_id = $skus->supply_id;
					$model->product = $skus->name;
					$model->sku = Sku::property($skus->id);
					$model->quantity = $quantity;
					$model->supplier = $skus->supply->Suppliername;
					$model->price = $skus->buiing_price;

		            $model->save();
		        }
		        else
		            InvoiceOrder::model()->updateCounters(array('quantity'=>$quantity), "sku_id=:sku and trade_id=:trade",array(":sku"=>$skus->id,":trade"=>$trade));
			}
			InvoiceTrade::model()->updateAll(array('source'=>InvoiceTrade::SOURCE_EDIT), "invoice_id=:id and trade_id=:trade",array(":id"=>$id,":trade"=>$trade));
			$this->ajaxReturn(Ya::t('Operation Success'), 0);
		}
		$model = InvoiceTrade::model()->belong($id)->trade($trade)->forcheck()->with('invoiceOrder')->find();
		$trade = Trade::model()->belong()->simple()->with('order')->findByPk($trade);
		//var_dump($model);
		//var_dump($trade);
		//Yii::app()->end();
		if(Yii::app()->request->isAjaxRequest)
		{
			$data = $this->renderPartial('edit',array('model'=>$model, 'invoice'=>$invoice, 'trade'=>$trade),true);
			$this->ajaxReturn($data);
		}
		else
			$this->render('edit',array('invoice'=>$invoice, 'model'=>$model, 'trade'=>$trade));
	}
	/**
	 * 配货单打印
	 * @param integer $id 配货单ID
	 * @param integer $type 进货：0 配货：1
	 */
	public function actionRead($id, $type = 0)
	{
		$model = $this->loadModel();
		$criteria=new CDbCriteria(array(
			'condition'=>'invoice_id=:invoice_id',
			'params'=>array(':invoice_id'=>$id),
			'select'=>'id, sku_id, product, supplier, sku, price, sum(quantity) as quantity',
			'order'=>'id ASC',
			'group'=>'sku_id',
		));
		$count = InvoiceOrder::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 1000;
		$pager->applyLimit($criteria);
		$orders = InvoiceOrder::model()->findAll($criteria);
		
		$items = array();
		foreach ($orders as $order)
		{
			$criteria=new CDbCriteria(array(
				'condition'=>'sku.id=:id',
				'params'=>array(':id'=>$order->sku_id),
				'scopes'=>array('belong','simple'),
				'with'=>array(
					'supply'=>array(
						'scopes'=>'simple',
						'with'=>array(
							'supplier'=>array('scopes'=>'simple')
						)
					),
				)
			));
			$sku = Sku::model()->find($criteria);
			
			if(isset($sku->supply->name) && isset($sku->supply->supplier->name))
			{
				$supplier = $sku->supply->supplier->name;
				$supply = $sku->supply->name;
				$price = $sku->buiing_price;
				$stock = $sku->stock;
				$property = $sku->sku_1. ' , ' . $sku->sku_2;
			}
			else
			{
				$supplier = $order->supplier ? $order->supplier : '未知';
				$supply = $order->product;
				$price = $order->price;
				$stock = 0;
				$property = $order->sku;
			}
			
			//库存处理
			$hasStock = $order->quantity; - $stock;
			if($type == 1) //配货
			{
				if($stock == 0)
					continue;
				$quantity = ($hasStock >= 0) ? $stock : $order->quantity;
			}
			else //进货
			{
				if($hasStock > 0)
					$quantity = $hasStock;
				else
					continue;
			}

			$item = array();
			$item['property'] = $property;
			$item['stock'] = $stock;
			$item['price'] = $price;
			$item['quantity'] = $quantity;

			$items['price'] += $price;
			$items['quantity'] += $quantity;

			$items['suppliers'][$supplier]['price'] += $price;
			$items['suppliers'][$supplier]['quantity'] += $quantity;

			$items['suppliers'][$supplier]['label'][$supply]['price'] += $price;
			$items['suppliers'][$supplier]['label'][$supply]['quantity'] += $quantity;
			$items['suppliers'][$supplier]['label'][$supply]['sku'][$sku->id] = $item;
		}
		
		$this->renderPartial('read',array(
			'pages'=>$pager,
			'model'=>$model,
			'item'=>$items,
		));
	}

	/**
	 * 订单删除
	 */
	public function actionTrade($id, $trade)
	{
		if($this->loadModel()->archive)
			$this->ajaxReturn(Ya::t('Operation Failure'), 0);
		$ids = explode(',', $trade);
		foreach ($ids as $pk)
		{
			$condition = 'invoice_id=:invoice_id and trade_id=:trade_id';
			$params = array(':invoice_id'=>$id,':trade_id'=>$pk);
			InvoiceTrade::model()->deleteAll($condition, $params);
			InvoiceOrder::model()->deleteAll($condition, $params);
		}
		if(Yii::app()->request->isAjaxRequest)
			$this->ajaxReturn(Ya::t('Operation Success'));
		else
			$this->redirect(Yii::app()->request->urlReferrer);
	}

	/**
	 * 配货单删除
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel();
		if($model->archive)
			$this->ajaxReturn(Ya::t('Operation Failure'), 0);

		$condition = 'invoice_id=:invoice_id';
		$params = array(':invoice_id'=>$id);
		InvoiceTrade::model()->deleteAll($condition, $params);
		InvoiceOrder::model()->deleteAll($condition, $params);

		$model->delete();

		if(Yii::app()->request->isAjaxRequest)
			$this->ajaxReturn(Ya::t('Operation Success'));
		else
			$this->redirect(Yii::app()->request->urlReferrer);
	}

	/**
	 * 配货单打印
	 */
	public function actionNew($id, $type = 1)
	{
		$criteria=new CDbCriteria(array(
			'condition'=>'invoice_id=:invoice_id',
			'params'=>array(':invoice_id'=>$id),
		));
		$orders = InvoiceOrder::model()->findAll($criteria);

		foreach ($orders as $order)
		{
			$sku = Sku::model()->find('name=:name', array(':name'=>$order->product));

			$order->sku_id = $sku->id;
			$order->product_id = $sku->product_id;
			$order->supply_id = $sku->supply_id;
			$order->price = $sku->buiing_price;
			$order->save();
		}
	}

}