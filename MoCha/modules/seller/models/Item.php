<?php

class Item extends CActiveRecord
{
	# 商品状态
	const STATUS_RECYcLE = 0; //回收站
	const STATUS_INSTOCK = 1; //出售中
	const STATUS_ONSALE = 2; //仓库中

	# 商品类型
	const TYPE_FIXED= 1; //一口价
	const TYPE_AUCTION = 2; //拍卖

	# 商品新旧程度
	const STUFF_NEW= 1; //全新
	const STUFF_UNUSEED = 2; //闲置
	const STUFF_SECOND = 3; //二手

	/**
	 * Item表的字段:
	 * @var integer $id
	 * @var integer $shop_id
	 * @var string $title
	 * @var string $outer_id
	 * @var integer $approve_status
	 * @var integer $approve_type
	 * @var integer $type
	 * @var integer $volume
	 * @var integer $cid
     * @var string $seller_cids
     * @var string $pic_url
     * @var integer $list_time
     * @var integer $delist_time
     * @var integer $stuff_status
     * @var string $props
     * @var string $property_alias
     * @var string $input_pids
     * @var string $input_str
     * @var integer $auction_point
     * @var integer $template_id
     * @var integer $postage_id
     * @var integer $sub_stock
     * @var integer $num
     * @var integer $price
     * @var integer $valid_thru
     * @var integer $express_fee
     * @var integer $ems_fee
     * @var integer $post_fee
     * @var integer $product_id
     * @var integer $freight_payer
     * @var integer $increment
     * @var integer $has_discount
     * @var integer $has_invoice
     * @var integer $has_warranty
     * @var integer $has_showcase
     * @var integer $is_lightning_consignment
     * @var integer $is_xinpin
     * @var integer $is_virtual
     * @var integer $is_taobao
     * @var integer $is_ex
     * @var integer $is_timing
     * @var integer $is_3D
     * @var integer $one_station
     * @var integer $violation
     * @var integer $cod_postage_id
     * @var string $sell_promise
     * @var integer $discount
     * @var integer $elite
     * @var integer $status
     * @var integer $created
     * @var integer $modified
	 */

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{item}}';
	}

	public function rules()
	{
		return array(
			array('title, approve_status', 'required'),
			array('cid, is_3D', 'numerical', 'integerOnly'=>true),
			array('id,shop_id,title,outer_id,approve_status,approve_type,type,volume,cid,seller_cids,pic_url,list_time,delist_time,stuff_status,props,property_alias,input_pids,input_str,auction_point,template_id,postage_id,sub_stock,num,price,valid_thru,express_fee,ems_fee,post_fee,product_id,freight_payer,increment,has_discount,has_invoice,has_warranty,has_showcase,is_lightning_consignment,is_xinpin,is_virtual,is_taobao,is_ex,is_timing,is_3D,one_station,violation,cod_postage_id,sell_promise,discount,elite,status,created,modified', 'safe'),
		);
	}

	public function relations()
	{
        return array(
			'shop' => array(self::BELONGS_TO, 'Shop', 'shop_id'),
			'order' => array(self::HAS_MANY, 'Order', 'item_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => SellerModule::t("Item ID"),
			'seller_nick'=>SellerModule::t("Seller Nick"),
			'seller_memo'=>SellerModule::t("Memo"),
			'seller_flag'=>SellerModule::t("Flag"),
		);
	}
	
	public function scopes()
    {
        return array(
            'belong'=>array(
            	'alias'=>'item',
            	'condition'=>'item.shop_id=:shop_id',
            	'params'=>array(':shop_id'=>Yii::app()->user->tbid),
            ),
            'notsafe'=>array(
            	'alias'=>'item',
            	'select' => 'id,title,outer_id,approve_status,approve_type,type,volume,cid,seller_cids,pic_url,list_time,delist_time,stuff_status,props,property_alias,input_pids,input_str,auction_point,template_id,postage_id,sub_stock,num,price,valid_thru,express_fee,ems_fee,post_fee,product_id,freight_payer,increment,has_discount,has_invoice,has_warranty,has_showcase,is_lightning_consignment,is_xinpin,is_virtual,is_taobao,is_ex,is_timing,is_3D,one_station,violation,cod_postage_id,sell_promise,discount,elite,status,created,modified',
            ),
            'forcheck'=>array(
            	'alias'=>'item',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'item',
                'select' => 'id, title, outer_id, approve_status, approve_type, volume, seller_cids, pic_url',
            ),
        );
    }

	/**
	 * 橱窗推荐
	 */
	public function getShowCase()
    {
        if( $this->has_showcase)
        	return '<i class="icofont-ok-sign"></i>';
        else
        	return '<i class="icofont-remove-sign"></i>';
    }
    
	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'status' => array(
				self::STATUS_ONSALE => SellerModule::t('On sale'),
				self::WAIT_BUYER_PAY => SellerModule::t('In stock'),
			),
			'type' => array(
				self::TYPE_FIXED => SellerModule::t('一口价'),
				self::TYPE_AUCTION => SellerModule::t('拍卖'),
			),
			'stuff' => array(
				self::STUFF_NEW => SellerModule::t('全新'),
				self::STUFF_UNUSEED => SellerModule::t('闲置'),
				self::STUFF_SECOND => SellerModule::t('二手'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

	/**
	 * 转换交易数据格式
	 * @param $trade 传入交易数据
	 */
	public function formatAlias($type,$code=NULL)
	{
		$_items = array(
			'status' => array(
				'onsale' => self::WAIT_BUYER_PAY,
				'instock' => self::WAIT_BUYER_PAY
			),
			'type' => array(
				'fixed' => self::TYPE_FIXED,
				'auction' => self::TYPE_AUCTION
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

    public function search()
    {
        $criteria=new CDbCriteria;
        
        $criteria->compare('tid',$this->tid);
        $criteria->compare('nick',$this->nick,true);
        $criteria->compare('user',$this->user,true);
        $criteria->compare('contact',$this->contact,true);
        $criteria->compare('qq',$this->qq,true);
        $criteria->compare('memo',$this->memo,true);
        $criteria->compare('is_send',$this->is_send);
        $criteria->compare('type',$this->type);
        $criteria->compare('settled',$this->settled);
        $criteria->compare('created',$this->created);
        $criteria->compare('modified',$this->modified);

        return new CActiveDataProvider(get_class($this), array(
            'criteria'=>$criteria,
        	'pagination'=>array(
				'pageSize'=>Yii::app()->getModule('seller')->page_size,
			),
        ));
    }

    public function formatHtml($model) {
    	if(isset($model['status'])) $model['status'] = self::itemAlias('status', $model['status']);
    	if(isset($model['cod_status'])) $model['cod_status'] = self::itemAlias('codStatus', $model['cod_status']);
    	if(isset($model['type'])) $model['type'] = self::itemAlias('type', $model['type']);
    	if(isset($model['shipping_type'])) $model['shipping_type'] = self::itemAlias('shippingType', $model['shipping_type']);
    	if(isset($model['trade_from'])) $model['trade_from'] = self::itemAlias('tradeFrom', $model['trade_from']);

    	if(isset($model['buyer_rate'])) $model['buyer_rate'] = self::itemAlias('buyerRate', $model['buyer_rate']);
    	if(isset($model['seller_rate'])) $model['seller_rate'] = self::itemAlias('sellerRate', $model['seller_rate']);
    	
    	if(isset($model['created'])) $model['created'] = date('Y-m-d H:i:s', $model['created']);
    	if(isset($model['modified'])) $model['modified'] = date('Y-m-d H:i:s', $model['modified']);

    	return $model;
    }
}