<?php

class Refund extends CActiveRecord
{
	# 退款状态
	const WAIT_SELLER_AGREE = 1;
	const WAIT_BUYER_RETURN_GOODS = 2;
	const WAIT_SELLER_CONFIRM_GOODS = 3;
	const SELLER_REFUSE_BUYER = 4;
	const CLOSED = 5;
	const SUCCESS = 6;
	# 货物状态
	const BUYER_NOT_RECEIVED = 0;
	const BUYER_RECEIVED = 1;
	const BUYER_RETURNED_GOODS = 2;

	/**
	 * Refund表的字段:
	 * @var integer $id
	 * @var integer $shop_id
	 * @var integer $trade_id
	 * @var integer $order_id
	 * @var integer $status
	 * @var integer $good_status
	 * @var integer $total_fee
	 * @var integer $payment
	 * @var integer $refund_fee
     * @var integer $split_taobao_fee
     * @var integer $split_seller_fee
     * @var string $reason
     * @var string $desc
     * @var integer $advance_status
     * @var integer $cs_status
     * @var string $company_name
     * @var string $sid
     * @var string $address
     * @var integer $has_good_return
     * @var integer $good_return_time
     * @var integer $created
     * @var integer $modified
	 */

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{refund}}';
	}

	public function rules()
	{
		return array(
			array('trade_id, status', 'required'),
			array('trade_id, status', 'numerical', 'integerOnly'=>true),
			array('*', 'safe'),
		);
	}

	public function relations()
	{
        return array(
			'shop' => array(self::BELONGS_TO, 'Shop', 'shop_id'),
			'trade' => array(self::BELONGS_TO, 'Trade', 'trade_id'),
			'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => SellerModule::t("Refund ID"),
			'seller_nick'=>SellerModule::t("Seller Nick"),
		);
	}
	
	public function scopes()
    {
        return array(
            'belong'=>array(
            	'condition'=>'refund.shop_id=:shop_id',
            	'params'=>array(':shop_id'=>Yii::app()->user->tbid),
            ),
            'forFake'=>array(
            	'alias'=>'refund',
                'select' => 'id, buyer_nick, status, payment, buyer_rate, seller_rate, receiver_name, created',
            ),
            'notsafe'=>array(
            	'alias'=>'refund',
            	'select' => 'id, trade_id, order_id, status, good_status, total_fee, payment, refund_fee, reason, desc, advance_status, cs_status, company_name, sid, address, has_good_return, good_return_time, created, modified',
            ),
            'forcheck'=>array(
            	'alias'=>'refund',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'refund',
            	'select' => 'id, trade_id, order_id, status, good_status, payment, reason, has_good_return, good_return_time, created',
            ),
        );
    }
    
	public static function itemAlias($type,$code=NULL) {
		$_items = array(
			'status' => array(
				self::WAIT_BUYER_PAY => SellerModule::t('WAIT_BUYER_PAY'),
				self::WAIT_BUYER_PAY => SellerModule::t('WAIT_BUYER_PAY'),
				self::WAIT_SELLER_SEND_GOODS => SellerModule::t('WAIT_SELLER_SEND_GOODS'),
				self::WAIT_BUYER_CONFIRM_GOODS => SellerModule::t('WAIT_BUYER_CONFIRM_GOODS'),
				self::TRADE_BUYER_SIGNED => SellerModule::t('TRADE_BUYER_SIGNED'),
				self::TRADE_FINISHED => SellerModule::t('TRADE_FINISHED'),
				self::TRADE_CLOSED => SellerModule::t('TRADE_CLOSED'),
				self::TRADE_CLOSED_BY_TAOBAO => SellerModule::t('TRADE_CLOSED_BY_TAOBAO'),
				self::TRADE_NO_CREATE_PAY => SellerModule::t('TRADE_NO_CREATE_PAY'),
			),
			'goodStatus' => array(
				self::BUYER_NOT_RECEIVED => SellerModule::t('BUYER_NOT_RECEIVED'),
				self::BUYER_RECEIVED => SellerModule::t('BUYER_RECEIVED'),
				self::BUYER_RETURNED_GOODS => SellerModule::t('BUYER_RETURNED_GOODS'),
			),
			'buyerRate' => array(
				'0' => SellerModule::t('Wait buyer rating'),
				'1' => SellerModule::t('Buyer had rated'),
			),
			'sellerRate' => array(
				'0' => SellerModule::t('Wait seller rating'),
				'1' => SellerModule::t('Seller had rated'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}
	
    public function search()
    {
        $criteria=new CDbCriteria;
        
        $criteria->compare('tid',$this->tid);
        $criteria->compare('nick',$this->nick,true);
        $criteria->compare('user',$this->user,true);
        $criteria->compare('contact',$this->contact,true);
        $criteria->compare('qq',$this->qq,true);
        $criteria->compare('memo',$this->memo,true);
        $criteria->compare('is_send',$this->is_send);
        $criteria->compare('type',$this->type);
        $criteria->compare('settled',$this->settled);
        $criteria->compare('created',$this->created);
        $criteria->compare('modified',$this->modified);

        return new CActiveDataProvider(get_class($this), array(
            'criteria'=>$criteria,
        	'pagination'=>array(
				'pageSize'=>Yii::app()->getModule('seller')->page_size,
			),
        ));
    }

    public function formatHtml($model) {
    	if(isset($model['status'])) $model['status'] = self::itemAlias('status', $model['status']);
    	if(isset($model['good_status'])) $model['good_status'] = self::itemAlias('goodStatus', $model['good_status']);

    	if(isset($model['buyer_rate'])) $model['buyer_rate'] = self::itemAlias('buyerRate', $model['buyer_rate']);
    	if(isset($model['seller_rate'])) $model['seller_rate'] = self::itemAlias('sellerRate', $model['seller_rate']);
    	
    	if(isset($model['created'])) $model['created'] = date('Y-m-d H:i:s', $model['created']);
    	if(isset($model['modified'])) $model['modified'] = date('Y-m-d H:i:s', $model['modified']);

    	return $model;
    }
}