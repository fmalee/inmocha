<?php
/**
 * Product表的字段:
 * @var integer $id
 * @var integer $user_id
 * @var string $name
 * @var integer $type_id
 * @var integer $supply_id
 * @var integer $status
 * @var string $title
 * @var string $tag
 * @var integer $is_elite
 * @var integer $selling_price
 * @var integer $current_price
 * @var integer $buiing_price
 * @var integer $stock
 * @var integer $inferior
 * @var string $picture
 * @var string $sku_1
 * @var string $sku_2
 * @var string $barcode
 * @var string $memo
 * @var integer $created
 * @var integer $modified
 * @var integer $supplier_id
 * @var integer $item_id
 */
class Product extends CActiveRecord {

    const STATUS_NOACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const ELITE_NOACTIVE = 0;
    const ELITE_ACTIVE = 1;

    private static $_namebyid = array();

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return '{{product}}';
    }

    public function rules() {
        return array(
            array('name, type_id', 'required'),
            array('type_id, status, is_elite', 'numerical', 'integerOnly' => true),
            array('name, sku_1, sku_2, barcode', 'length', 'max' => 20),
            array('title', 'length', 'max' => 70),
            array('tag, picture', 'length', 'max' => 128),
            array('memo', 'length', 'max' => 255),
            array('selling_price, current_price', 'safe'),
        );
    }

    public function relations() {
        return array(
            'extend' => array(self::HAS_ONE, 'ProductExtend', 'id'),
            'sku' => array(self::HAS_MANY, 'Sku', 'product_id'),
            'supply' => array(self::HAS_MANY, 'Supply', 'product_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => SellerModule::t('Product ID'),
            'name' => SellerModule::t('Model Number'),
            'type_id' => SellerModule::t('Type'),
            'supply_id' => SellerModule::t('Supplier'),
            'status' => SellerModule::t('Status'),
            'title' => SellerModule::t('Title'),
            'is_elite' => SellerModule::t('Elite'),
            'tag' => SellerModule::t('Tag'),
            'selling_price' => SellerModule::t('Selling Price'),
            'current_price' => SellerModule::t('Current Price'),
            'buiing_price' => SellerModule::t('Buiing Price'),
            'stock' => SellerModule::t('Stock'),
            'inferior' => SellerModule::t('Inferior'),
            'picture' => SellerModule::t('Picture'),
            'sku_1' => SellerModule::t('Sku 1'),
            'sku_2' => SellerModule::t('Sku 2'),
            'barcode' => SellerModule::t('Barcode'),
            'memo' => SellerModule::t('Memo'),
            'created' => SellerModule::t('Created'),
            'modified' => SellerModule::t('Modified'),
        );
    }

    public function scopes() {
        return array(
            'belong' => array(
                'condition' => 'product.user_id=1',
            ),
            'type' => array(
                'condition' => 'product.type_id=:type_id',
                'params' => array(':type_id' => $this->type_id),
            ),
            'supply' => array(
                'condition' => 'product.supply=:supply',
                'params' => array(':supply' => $this->supply_id),
            ),
            'notsafe' => array(
                'alias' => 'product',
                'select' => 'id, name, type_id, supply_id, status, title, is_elite, tag, selling_price, current_price, buiing_price, stock, inferior, picture, sku_1, sku_2, memo, created, modified',
            ),
            'forcheck' => array(
                'alias' => 'product',
                'select' => 'id',
            ),
            'simple' => array(
                'alias' => 'product',
                'select' => 'id, name, sku_1, sku_2, type_id',
            ),
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->created = time();
                $this->user_id = Yii::app()->user->id;
            }
            $this->modified = time();
            return true;
        } else
            return false;
    }

    /**
     * 返回产品名称
     * @param 产品ID
     * @return 产品名称
     */
    public static function getNameById($id) {
        if (!isset(self::$_namebyid[$id]))
            $_namebyid[$id] = Product::model()->simple()->findByPk($id)->name;
        return $_namebyid[$id];
    }

    /**
     * 导入淘宝宝贝
     * @param $item_id 商品ID
     * @param $type_id 类别
     * @param $stock 商品ID
     * @param $label 商品ID
     */
    public static function import($item_id, $type_id, $stock = false, $makeLabel = false) {
        $item = TaobaO::model()->synItem($item_id);
        if (!$item)
            return false;

        if ($makeLabel || !$item['outer_id']) //没胡商家编码
            $label = self::RandomSerial(); //生成随机编码

        $product = new Product;
        $product->name = $label ? $label : str_replace(',', "，", $item['outer_id']);
        $product->title = str_replace(",", "", $item['title']); //替换','
        $product->picture = $item['pic_url'];
        $product->selling_price = $item['price'];
        $product->item_id = $item_id;
        $product->status = 1;
        $product->type_id = $type_id;
        if ($stock)
            $product->stock = $item['num'];

        $saleNames = ItemProp::saleNames($item['cid']);
        $product->sku_1 = $saleNames[0]->name;
        if (count($saleNames) > 1)
            $product->sku_2 = $saleNames[1]->name;

        if ($product->save()) {
            if ($label)
                TaoItem::model()->updateItem($item_id, array('outer_id' => $label)); //更新outer_id

            $extend = new ProductExtend;
            $product_id = $product->id;

            $extend->id = $product_id;
            if (isset($item['property_alias']))
                $extend->property_alias = $item['property_alias'];
            if (isset($item['product_id']))
                $extend->product_id = $item['product_id'];
            if (isset($item['item_imgs']))
                $extend->item_imgs = serialize($item['item_imgs']);
            if (isset($item['prop_imgs']))
                $extend->prop_imgs = serialize($item['prop_imgs']);
            $extend->input_pids = $item['input_pids'];
            $extend->is_virtual = $item['is_virtual'];
            $extend->input_str = $item['input_str'];
            $extend->props = serialize($item['props']);
            $extend->cid = $item['cid'];
            $extend->description = $item['desc'];

            $extend->save();

            $properties = ItemProp::propAlias($item['property_alias']);
            $i = 1;
            foreach ($item['skus']['sku'] as $skus) {
                if ($label)
                    $skuLable = $label . '-' . $i;
                elseif ($item['outer_id'] == $skus['outer_id'])
                    $skuLable = $skus['outer_id'] . '-' . $i;
                else
                    $skuLable = $skus['outer_id'];

                $sku = new Sku;
                $sku->product_id = $product_id;
                $sku->name = $skuLable;
                $sku->status = 1;
                $sku->selling_price = $skus['price'];
                $sku->tb_sku = $skus['sku_id'];
                if ($stock)
                    $sku->stock = $skus['quantity'];

                $property = explode(';', $skus['properties_name']);
                $pids = explode(':', $property[0]);
                $name = $pids[0] . ':' . $pids[1];
                if (array_key_exists($name, $properties))
                    $sku->sku_1 = $properties[$name];
                else
                    $sku->sku_1 = trim(strrchr($property[0], ':'), ':');
                if (count($property) > 1) {
                    $pids = explode(':', $property[1]);
                    $name = $pids[0] . ':' . $pids[1];
                    if (array_key_exists($name, $properties))
                        $sku->sku_2 = $properties[$name];
                    else
                        $sku->sku_2 = trim(strrchr($property[1], ':'), ':');
                }
                $sku->save();

                if ($skuLable != $skus['outer_id']) {
                    //TaoItem::model()->updateSku($item_id, $skus['properties'], array('outer_id'=>$skuLable)); //更新SKU的outer_id;
                    ItemSku::model()->updateByPk($skus['sku_id'], array('outer_id' => $skuLable));
                    Order::model()->updateAll(array('outer_sku_id' => $skuLable), 'sku_id=' . $skus['sku_id']);
                    InvoiceOrder::model()->updateAll(array('product' => $skuLable), 'tb_sku_id=' . $skus['sku_id']);
                }


                $i++;
            }
        }

        return $product_id;
    }

    /**
     * 随机生成编码
     * @param $str 已经存在的编码 如A01,Z99
     */
    public static function randomSerial($str = array()) {
        $numbers = rand(01, 99); // 生成随机数字
        $alphabets = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z');
        $arr = rand(0, 23); // 生成随机数组指标
        $serial = $alphabets[$arr] . sprintf("%02d", $numbers);
        if (!array_key_exists($serial, $str)) {
            $criteria = new CDbCriteria(array(
                'condition' => 'name=:name',
                'params' => array(':name' => $serial),
            ));
            $exist = self::model()->belong()->simple()->exists($criteria);
            if (!$exist) return $serial;
        }
        $str[$serial] = 1; // 记录存在编码
        self::randomSerial($str); // 进行递归
    }

    public static function itemAlias($type, $code = NULL) {
        $_items = array(
            'is_elite' => array(
                self::ELITE_ACTIVE => SellerModule::t('Had Elite'),
                self::ELITE_NOACTIVE => SellerModule::t('No Elite'),
            ),
            'status' => array(
                self::STATUS_ACTIVE => SellerModule::t('Enabled'),
                self::STATUS_NOACTIVE => SellerModule::t('Disabled'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public static function formatHtml($model) {
        if (isset($model['type_id'])) $model['type_id'] = Type::Lists('product', $model['type_id']);
        if (isset($model['created'])) $model['created'] = date('Y-m-d', $model['created']);
        if (isset($model['modified'])) $model['modified'] = date('Y-m-d', $model['modified']);

        return $model;
    }
}