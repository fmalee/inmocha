<?php

class LogisticsCompany extends CActiveRecord
{	
	/**
	 * LogisticsCompany:
	 * @var integer $id
	 * @var string $name
	 * @var string $code
	 * @var string $kingdee
	 * @var string $reg_mail_no
	 * @var integer $is_recommended
	 * @var integer $list_order
	 * @var integer $created
	 * @var integer $modified
	*/
	//类型
	const SEND = 0;
	const RECEIVER = 1;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{logistics_company}}';
	}

	public function rules()
	{
		return array(
			array('name, code', 'required', 'on'=>'create'),
			array('is_recommended, list_order', 'numerical', 'integerOnly'=>true),
			array('kingdee, reg_mail_no', 'length', 'max'=>255),
		);
	}

	public function relations()
	{
        return array(
			'service' => array(self::BELONGS_TO, 'Service', 'service_id'),
			'sku' => array(self::BELONGS_TO, 'Sku', 'sku_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'service_id' => SellerModule::t('Service'),
			'money' => SellerModule::t('Money'),
			'flow' => SellerModule::t('Flow'),
			'channel' => SellerModule::t('Channel'),
		);
	}

	public function scopes()
    {
        return array(
            'notsafe'=>array(
            	'alias'=>'company',
            	'select' => 'id, name, code, kingdee, reg_mail_no, list_order, created',
            ),
            'forcheck'=>array(
            	'alias'=>'company',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'company',
            	'select' => 'id, name, code',
            ),
        );
    }
}