<?php

class ItemSku extends CActiveRecord
{	
	/**
	 * ItemSku:
	 * @var integer $id
	 * @var integer $item_id
	 * @var string $outer_id
	 * @var string $properties
	 * @var string $properties_name
	 * @var integer $price
	 * @var integer $quantity
	 * @var integer $status
	 * @var integer $created
	 * @var integer $modified
	*/
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{item_sku}}';
	}

	public function rules()
	{
		return array(
			array('item_id, quantity,status', 'numerical', 'integerOnly'=>true),
			array('outer_id', 'length', 'max'=>20),
			array('properties', 'length', 'max'=>100),
			array('properties_name', 'length', 'max'=>255),
			array('price', 'safe'),
		);
	}

	public function relations()
	{
        return array(
			'item' => array(self::BELONGS_TO, 'Item', 'item_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'service_id' => SellerModule::t('Service'),
			'money' => SellerModule::t('Money'),
			'flow' => SellerModule::t('Flow'),
			'channel' => SellerModule::t('Channel'),
		);
	}

	public function scopes()
    {
        return array(
            'notsafe'=>array(
            	'alias'=>'itemSku',
            	'select' => 'id, item_id, outer_id, properties, properties_name, price, quantity, status, modified',
            ),
            'forcheck'=>array(
            	'alias'=>'itemSku',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'itemSku',
            	'select' => 'id, item_id, outer_id, properties, properties_name',
            ),
        );
    }

	public function item($item_id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'itemSku.item_id='.$item_id,
			//'params'=>array(':cid'=>$cid),
	    ));
	    return $this;
	}

    public function formatHtml($model)
    {
    	if(isset($model['flow'])) $model['flow'] = self::itemAlias('flow', $model['flow']);

    	return $model;
    }
}