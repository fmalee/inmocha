<?php

class Invoice extends CActiveRecord
{	
	/**
	 * Invoice表的字段:
	 * @var integer $id
	 * @var integer $shop_id
	 * @var string $name
	 * @var integer $trade
	 * @var integer $total
	 * @var integer $price
	 * @var integer $archive
	 * @var string $memo
     * @var integer $created
     * @var integer $modified
	*/
	const ARCHIVE_OFF=0;
	const ARCHIVE_ON=1;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{invoice}}';
	}

	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>20),
			array('memo', 'length', 'max'=>128),
		);
	}

	public function relations()
	{
        return array(
			'shop' => array(self::BELONGS_TO, 'Shop', 'shop_id'),
			'trade' => array(self::HAS_MANY, 'InvoiceTrade', 'invoice_id'),
			'order' => array(self::HAS_MANY, 'InvoiceOrder', 'invoice_id'),
			'tradeCount' => array(self::STAT, 'InvoiceTrade', 'invoice_id'),
			'orderCount' => array(self::STAT, 'InvoiceOrder', 'invoice_id', 'select'=>'count(quantity)'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => SellerModule::t('Invoice ID'),
			'name' => SellerModule::t('Name'),
			'trade' => SellerModule::t('Trade'),
			'total' => SellerModule::t('Total'),
			'price' => SellerModule::t('Price'),
			'archive' => SellerModule::t('Archive'),
			'memo' => SellerModule::t('Memo'),
			'created' => SellerModule::t('Created'),
			'modified' => SellerModule::t('Modified'),
		);
	}

	public function scopes()
    {
        return array(
            'belong'=>array(
            	'condition'=>'invoice.shop_id=:shop_id',
            	'params'=>array(':shop_id'=>Yii::app()->user->tbid),
            ),
            'notsafe'=>array(
            	'alias'=>'invoice',
            	'select' => 'id, name, trade, total, price, archive, memo, created, modified',
            ),
            'forcheck'=>array(
            	'alias'=>'invoice',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'invoice',
            	'select' => 'id, name, created',
            ),
        );
    }

	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->created = time();
				$this->shop_id=Yii::app()->user->tbid;
			}
			$this->modified=time();
			return true;
		}
		else
			return false;
	}

	/**
	 * 自动导入相关记录
	 * @param array $source 来源,数组或是用'逗号'隔开
	 */
	public static function autoIpt($id, $source = array('trade,niffer,reissue'))
	{
		if(!is_array($source))
			 explode(',', trim($source, ','));

		if (in_array(InvoiceTrade::SOURCE_TRADE, $source))  //订单
		{
			$trades = Trade::model()
				->belong()
				->forcheck()
				->with(array('fake'=>array('scopes'=>'forcheck')))
				->status(Trade::WAIT_SELLER_SEND_GOODS)
				->findAll();
			
			foreach ($trades as $trade)
			{
				if(!$trade->fake)
					self::iptTrade($id, $trade->id); //导入配货单
			}
		}
		if (in_array(InvoiceTrade::SOURCE_NIFFER, $source)) //换货记录
		{
			$services = Service::model()->belong()->forcheck()->status(Service::STATUS_RECIEVE)->module('niffer')->findAll();
			foreach ($services as $service)
				self::iptService($id, $service->id); //导入配货单
		}
		if (in_array(InvoiceTrade::SOURCE_REISSUE, $source)) //补发记录
		{
			$services = Service::model()->belong()->forcheck()->status(Service::STATUS_RECIEVE)->module('reissue')->findAll();
			foreach ($services as $service)
				self::iptService($id, $service->id); //导入配货单
		}
		
		return true;
	}

	/**
	 * 导入订单
	 */
	public static function iptTrade($id, $trade_id)
	{
		$orders = Order::model()->belong()->simple()->trade($trade_id)->findAll();
		if(!$orders)
			return false;

		$trade = InvoiceTrade::model()->forcheck()->belong($id)->trade($trade_id)->find();
		if(!$trade || !in_array($trade->source, array(InvoiceTrade::SOURCE_TRADE, InvoiceTrade::SOURCE_EDIT)))
		{
			if(!$trade)
			{
				$trade = new InvoiceTrade;
				$trade->invoice_id = $id;
				$trade->trade_id = $trade_id;
				$trade->source = InvoiceTrade::SOURCE_TRADE;
				$trade->save();
			}
			elseif(!in_array($trade->source, array(InvoiceTrade::SOURCE_TRADE, InvoiceTrade::SOURCE_EDIT)))
			{
				$trade->source = InvoiceTrade::SOURCE_EDIT;
				$trade->save();
			}
			foreach($orders as $order)
			{
				$has_order = InvoiceOrder::model()->simple()->belong($id)->trade($trade_id)->product($order->outer_sku_id)->find();
				if($has_order)
					$has_order->updateCounters(array('quantity'=>$order->num));
				else
				{
					$skus = Sku::model()->belong()->notsafe()->name($order->outer_sku_id)->find();

					$model = new InvoiceOrder;
					$model->invoice_id = $id;
					$model->trade_id = $trade_id;
					$model->quantity = $order->num;

					if($skus)
					{
						$model->product_id = $skus->product_id;
						$model->sku_id = $skus->id;
						$model->supply_id = $skus->supply_id;
						$model->product = $skus->name;
						$model->sku = Sku::property($skus->id);
						$model->supplier = $skus->supply->Suppliername;
						$model->price = $skus->buiing_price;
					}
					else
					{
						$model->product = $order->outer_sku_id ? $order->outer_sku_id : $order->outer_iid;
						$model->sku = $order->sku_properties_name;
					}

					$model->save();
				}
			}

			return true;
		}

		return false;
	}

	/**
	 * 导入售后
	 */
	public static function iptService($id, $service_id)
	{
		$service = Service::model()->belong()->forcheck()->findByPk($service_id);
		if(!$service)
			return false;

		$trade_id = $service->trade_id;
		if($service->module == 'niffer')
		{
			$niffer = ServiceNiffer::model()->findByPk($service_id);
			if(!$niffer)
				return false;
			$sku_id = $niffer->send_sku_id;
			$product = $reissue->send_product;
			$quantity = $niffer->send_quantity;
			$source = InvoiceTrade::SOURCE_NIFFER;
		}
		elseif($service->module == 'reissue')
		{
			$reissue = ServiceReissue::model()->findByPk($service_id);
			if(!$reissue)
				return false;
			$sku_id = $reissue->sku_id;
			$product = $reissue->product;
			$quantity = $reissue->quantity;
			$source = InvoiceTrade::SOURCE_REISSUE;
		}
		$trade = InvoiceTrade::model()->forcheck()->belong($id)->trade($trade_id)->find();
		if(!$trade || !in_array($trade->source, array($source, InvoiceTrade::SOURCE_EDIT)))
		{
			if(!$trade)
			{
				$trade = new InvoiceTrade;
				$trade->invoice_id = $id;
				$trade->trade_id = $trade_id;
				$trade->source = $source;
				$trade->save();
			}
			elseif(!in_array($trade->source, array($source, InvoiceTrade::SOURCE_EDIT)))
			{
				$trade->source = InvoiceTrade::SOURCE_EDIT;
				$trade->save();
			}

			$has_order = InvoiceOrder::model()->simple()->belong($id)->trade($trade_id)->findByPk($sku_id);
			if($has_order)
				$has_order->updateCounters(array('quantity'=>$order->num));
			else
			{
				$skus = Sku::model()->belong()->notsafe()->findByPk($sku_id);

				$model = new InvoiceOrder;
				$model->invoice_id = $id;
				$model->trade_id = $trade_id;
				$model->quantity = $quantity;

				if($skus)
				{
					$model->product_id = $skus->product_id;
					$model->sku_id = $skus->id;
					$model->supply_id = $skus->supply_id;
					$model->product = $skus->name;
					$model->sku = Sku::property($skus->id);
					$model->supplier = $skus->supply->Suppliername;
					$model->price = $skus->buiing_price;
				}
				else
				{
					$model->product = $product;
					$model->sku = $product;
				}

				$model->save();
			}

			return true;
		}

		return false;
	}

	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'archive' => array(
				self::ARCHIVE_ON => SellerModule::t('Had Archive'),
				self::ARCHIVE_OFF => SellerModule::t('No Archive'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

    public function formatHtml($model)
    {
    	if(isset($model['created'])) $model['created'] = date('Y-m-d', $model['created']);
    	if(isset($model['modified'])) $model['modified'] = date('Y-m-d', $model['modified']);

    	return $model;
    }
}