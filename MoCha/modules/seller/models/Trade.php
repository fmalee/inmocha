<?php

class Trade extends CActiveRecord
{
	# 交易状态
	const TRADE_NO_CREATE_PAY= 1;
	const WAIT_BUYER_PAY= 2;
	const WAIT_SELLER_SEND_GOODS= 3;
	const WAIT_BUYER_CONFIRM_GOODS= 4;
	const TRADE_BUYER_SIGNED= 5;
	const TRADE_FINISHED= 6;
	const TRADE_CLOSED= 7;
	const TRADE_CLOSED_BY_TAOBAO= 8;
	# 货到付款状态
	const NEW_CREATED= 1;
	const WAITING_TO_BE_SENT = 2;
	const ACCEPTED_BY_COMPANY = 3;
	const REJECTED_BY_COMPANY = 4;
	const RECIEVE_TIMEOUT = 5;
	const TAKEN_IN_SUCCESS = 6;
	const TAKEN_IN_FAILED = 7;
	const TAKEN_TIMEOUT = 8;
	const REJECTED_BY_OTHER_SIDE = 9;
	const SIGN_IN = 10;
	const CANCELED = 11;
	const CLOSED = 12;
	# 交易类型
	const TYPE_FIXED= 1;
	const TYPE_AUCTION = 2;
	const TYPE_GUARANTEE_TRADE = 3;
	const TYPE_AUTO_DELIVERY = 4;
	const TYPE_INDEPENDENT_SIMPLE_TRADE = 5;
	const TYPE_INDEPENDENT_SHOP_TRADE = 6;
	const TYPE_COD = 7;
	const TYPE_FENXIAO = 8;
	const TYPE_GAME_EQUIPMENT = 9;
	const TYPE_SHOPEX_TRADE = 10;
	const TYPE_NETCN_TRADE = 11;
	const TYPE_EXTERNAL_TRADE = 12;
	# 物流方式
	const SHIP_FREE= 1;
	const SHIP_EMS = 2;
	const SHIP_POST = 3;
	const SHIP_EXPRESS = 4;
	const SHIP_VIRTUAL = 5;
	# 交易内部来源
	const FROM_TAOBAO = 1;
	const FROM_WAP= 2;
	const FROM_HITAO = 3;
	const FROM_TOP = 4;
	const FROM_JHS = 5;

	/**
	 * Trade表的字段:
	 * @var integer $id
	 * @var integer $shop_id
	 * @var integer $status
	 * @var integer $cod_status
	 * @var string $buyer_message
	 * @var string $seller_memo
	 * @var string $trade_memo
	 * @var integer $created
	 * @var integer $modified
     * @var integer $end_time
     * @var integer $pay_time
     * @var integer $consign_time
     * @var integer $alipay_id
     * @var integer $alipay_no
     * @var integer $type
     * @var integer $shipping_type
     * @var integer $seller_flag
     * @var integer $commission_fee
     * @var integer $received_payment
     * @var integer $available_confirm_fee
     * @var integer $payment
     * @var integer $cod_fee
     * @var integer $point_fee
     * @var integer $total_fee
     * @var integer $post_fee
     * @var integer $express_agency_fee
     * @var integer $buyer_obtain_point_fee
     * @var integer $has_post_fee
     * @var string $buyer_nick
     * @var string $buyer_alipay_no
     * @var string $buyer_email
     * @var string $receiver_name
     * @var string $receiver_state
     * @var string $receiver_city
     * @var string $receiver_district
     * @var string $receiver_address
     * @var string $receiver_zip
     * @var integer $receiver_mobile
     * @var string $receiver_phone
     * @var string $invoice_name
     * @var integer $real_point_fee
     * @var integer $is_3D
     * @var integer $can_rate
     * @var integer $buyer_rate
     * @var integer $seller_rate
     * @var string $trade_from
     * @var string $delivery_company
     * @var string $out_sid
     * @var integer $delivery_status
     * @var integer $delivery_start
     * @var integer $delivery_end
     * @var integer $delivery_created
     * @var integer $delivery_modified
     * @var integer $invoice
     * @var integer $service_time
     * @var string $promotion_details
     * @var integer $is_fake
	 */

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{trade}}';
	}

	public function rules()
	{
		return array(
			array('status, cod_status', 'required'),
			array('status, cod_status', 'numerical', 'integerOnly'=>true),
			array('id,shop_id,status,cod_status,buyer_message,seller_memo,trade_memo,created,modified,end_time,pay_time,consign_time,alipay_id,alipay_no,type,shipping_type,seller_flag,commission_fee,received_payment,available_confirm_fee,payment,cod_fee,point_fee,total_fee,post_fee,express_agency_fee,buyer_obtain_point_fee,has_post_fee,buyer_nick,buyer_alipay_no,buyer_email,receiver_name,receiver_state,receiver_city,receiver_district,receiver_address,receiver_zip,receiver_mobile,receiver_phone,invoice_name,real_point_fee,is_3D,can_rate,buyer_rate,seller_rate,trade_from,delivery_company,out_sid,delivery_status,delivery_start,delivery_end,delivery_created,delivery_modified,invoice,service_time,promotion_details,is_fake', 'safe'),
		);
	}

	public function relations()
	{
        return array(
			'shop' => array(self::BELONGS_TO, 'Shop', 'shop_id'),
			'order' => array(self::HAS_MANY, 'Order', 'trade_id'),
			'fake' => array(self::HAS_ONE, 'Fake', 'id'),
			'refund' => array(self::HAS_MANY, 'Refund', 'trade_id'),
			'invoice' => array(self::HAS_MANY, 'InvoiceTrade', 'trade_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => SellerModule::t("Trade ID"),
			'buyer_nick'=>SellerModule::t("买家昵称"),
			'seller_memo'=>SellerModule::t("卖家备注"),
			'status'=>SellerModule::t("Status"),
			'buyer_message'=>SellerModule::t("买家留言"),
			'seller_flag'=>SellerModule::t("Flag"),
		);
	}
	
	public function scopes()
    {
        return array(
            'belong'=>array(
            	'alias'=>'trade',
            	'condition'=>'trade.shop_id=:shop_id',
            	'params'=>array(':shop_id'=>Yii::app()->user->tbid),
            ),
            'notsafe'=>array(
            	'alias'=>'trade',
            	'select' => 'id, status, cod_status, buyer_message, seller_memo, trade_memo, created, modified, type, shipping_type, seller_flag, payment, post_fee, buyer_nick, receiver_name, buyer_rate, seller_rate, trade_from, delivery_company, service_time, is_fake',
            ),
            'forcheck'=>array(
            	'alias'=>'trade',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'trade',
                'select' => 'id, buyer_nick, status, payment, buyer_rate, seller_rate, receiver_name, created',
            ),
        );
    }

	public function status($status)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'trade.status=:status',
			'params'=>array(':status'=>$status),
	    ));
	    return $this;
	}

	/**
	 * 订单状态
	 */
	public function getStatusName()
    {
        return $this->itemAlias('status', $this->status);
    }

	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'status' => array(
				self::WAIT_BUYER_PAY => SellerModule::t('WAIT_BUYER_PAY'),
				self::WAIT_SELLER_SEND_GOODS => SellerModule::t('WAIT_SELLER_SEND_GOODS'),
				self::WAIT_BUYER_CONFIRM_GOODS => SellerModule::t('WAIT_BUYER_CONFIRM_GOODS'),
				self::TRADE_BUYER_SIGNED => SellerModule::t('TRADE_BUYER_SIGNED'),
				self::TRADE_FINISHED => SellerModule::t('TRADE_FINISHED'),
				self::TRADE_CLOSED => SellerModule::t('TRADE_CLOSED'),
				self::TRADE_CLOSED_BY_TAOBAO => SellerModule::t('TRADE_CLOSED_BY_TAOBAO'),
				self::TRADE_NO_CREATE_PAY => SellerModule::t('TRADE_NO_CREATE_PAY'),
			),
			'codStatus' => array(
				self::NEW_CREATED => SellerModule::t('NEW_CREATED'),
				self::WAITING_TO_BE_SENT => SellerModule::t('WAITING_TO_BE_SENT'),
				self::ACCEPTED_BY_COMPANY => SellerModule::t('ACCEPTED_BY_COMPANY'),
				self::REJECTED_BY_COMPANY => SellerModule::t('REJECTED_BY_COMPANY'),
				self::RECIEVE_TIMEOUT => SellerModule::t('RECIEVE_TIMEOUT'),
				self::TAKEN_IN_SUCCESS => SellerModule::t('TAKEN_IN_SUCCESS'),
				self::TAKEN_IN_FAILED => SellerModule::t('TAKEN_IN_FAILED'),
				self::TAKEN_TIMEOUT => SellerModule::t('TAKEN_TIMEOUT'),
				self::REJECTED_BY_OTHER_SIDE => SellerModule::t('REJECTED_BY_OTHER_SIDE'),
				self::SIGN_IN => SellerModule::t('SIGN_IN'),
				self::CANCELED => SellerModule::t('CANCELED'),
				self::CLOSED => SellerModule::t('CLOSED'),
			),
			'type' => array(
				self::TYPE_FIXED => SellerModule::t('fixed'),
				self::TYPE_AUCTION => SellerModule::t('auction'),
				self::TYPE_GUARANTEE_TRADE => SellerModule::t('guarantee_trade'),
				self::TYPE_AUTO_DELIVERY => SellerModule::t('auto_delivery'),
				self::TYPE_INDEPENDENT_SIMPLE_TRADE => SellerModule::t('independent_simple_trade'),
				self::TYPE_INDEPENDENT_SHOP_TRADE => SellerModule::t('independent_shop_trade'),
				self::TYPE_COD => SellerModule::t('cod'),
				self::TYPE_FENXIAO => SellerModule::t('fenxiao'),
				self::TYPE_GAME_EQUIPMENT => SellerModule::t('game_equipment'),
				self::TYPE_SHOPEX_TRADE => SellerModule::t('shopex_trade'),
				self::TYPE_NETCN_TRADE => SellerModule::t('netcn_trade'),
				self::TYPE_EXTERNAL_TRADE => SellerModule::t('external_trade'),
			),
			'shippingType' => array(
				self::SHIP_FREE => SellerModule::t('free'),
				self::SHIP_EMS => SellerModule::t('ems'),
				self::SHIP_POST => SellerModule::t('post'),
				self::SHIP_EXPRESS => SellerModule::t('express'),
				self::SHIP_VIRTUAL => SellerModule::t('virtual'),
			),
			'tradeFrom' => array(
				self::FROM_TAOBAO => SellerModule::t('TAOBAO'),
				self::FROM_WAP => SellerModule::t('WAP'),
				self::FROM_HITAO => SellerModule::t('HITAO'),
				self::FROM_TOP => SellerModule::t('TOP'),
				self::FROM_JHS => SellerModule::t('JHS'),
			),
			'buyerRate' => array(
				'0' => SellerModule::t('Wait buyer rating'),
				'1' => SellerModule::t('Buyer had rated'),
			),
			'sellerRate' => array(
				'0' => SellerModule::t('Wait seller rating'),
				'1' => SellerModule::t('Seller had rated'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

	public static function flags($code=NULL)
	{
		$flags = array(
			'0' => '<img src="/asset/img/taobao/flag_0.png" title="正常">',
			'1' => '<img src="/asset/img/taobao/flag_1.png" title="问题">',
			'2' => '<img src="/asset/img/taobao/flag_2.png" title="退款">',
			'3' => '<img src="/asset/img/taobao/flag_3.png" title="刷单">',
			'4' => '<img src="/asset/img/taobao/flag_4.png" title="朋友">',
			'5' => '<img src="/asset/img/taobao/flag_5.png" title="其他">'
		);
		if (isset($code))
			return isset($flags[$code]) ? $flags[$code] : false;
		else
			return $flags;
	}

    public function search()
    {
        $criteria=new CDbCriteria;
        
        $criteria->compare('tid',$this->tid);
        $criteria->compare('nick',$this->nick,true);
        $criteria->compare('user',$this->user,true);
        $criteria->compare('contact',$this->contact,true);
        $criteria->compare('qq',$this->qq,true);
        $criteria->compare('memo',$this->memo,true);
        $criteria->compare('is_send',$this->is_send);
        $criteria->compare('type',$this->type);
        $criteria->compare('settled',$this->settled);
        $criteria->compare('created',$this->created);
        $criteria->compare('modified',$this->modified);

        return new CActiveDataProvider(get_class($this), array(
            'criteria'=>$criteria,
        	'pagination'=>array(
				'pageSize'=>Yii::app()->getModule('seller')->page_size,
			),
        ));
    }

    public function formatHtml($model) {
    	if(isset($model['status'])) $model['status'] = self::itemAlias('status', $model['status']);
    	if(isset($model['cod_status'])) $model['cod_status'] = self::itemAlias('codStatus', $model['cod_status']);
    	if(isset($model['type'])) $model['type'] = self::itemAlias('type', $model['type']);
    	if(isset($model['shipping_type'])) $model['shipping_type'] = self::itemAlias('shippingType', $model['shipping_type']);
    	if(isset($model['trade_from'])) $model['trade_from'] = self::itemAlias('tradeFrom', $model['trade_from']);

    	if(isset($model['buyer_rate'])) $model['buyer_rate'] = self::itemAlias('buyerRate', $model['buyer_rate']);
    	if(isset($model['seller_rate'])) $model['seller_rate'] = self::itemAlias('sellerRate', $model['seller_rate']);
    	
    	if(isset($model['created'])) $model['created'] = date('Y-m-d H:i:s', $model['created']);
    	if(isset($model['modified'])) $model['modified'] = date('Y-m-d H:i:s', $model['modified']);

    	return $model;
    }
}