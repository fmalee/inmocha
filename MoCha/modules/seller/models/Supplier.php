<?php

class Supplier extends CActiveRecord
{	
	/**
	 * Supplier表的字段:
	 * @var integer $id
	 * @var string $user_id
	 * @var string $name
	 * @var integer $type_id
	 * @var integer $status
	 * @var integer $is_elite
	 * @var string $tag
	 * @var string $contact
	 * @var string $phone
     * @var string $wangwang
	 * @var string $qq
	 * @var string $qq_group
	 * @var string $www
	 * @var string $data_packet
	 * @var string $memo
	 * @var string $creater
     * @var integer $created
     * @var integer $modified
	*/
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	const ELITE_NOACTIVE=0;
	const ELITE_ACTIVE=1;
	
	private static $_namebyid=array();

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{supplier}}';
	}

	public function rules()
	{
		return array(
			array('name, type_id, status', 'required'),
			array('type_id, status, is_elite', 'numerical', 'integerOnly'=>true),
			array('name, phone, wangwang, qq, qq_group', 'length', 'max'=>30),
			array('tag, contact', 'length', 'max'=>20),
			array('www, data_packet', 'length', 'max'=>128),
			array('memo', 'length', 'max'=>255),
		);
	}

	public function relations()
	{
        return array(
			'user' => array(self::BELONGS_TO, 'user', '', 'on'=>'supplier.user_id=user.id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => SellerModule::t('Supplier ID'),
			'name' => SellerModule::t('Model Number'),
			'type_id' => SellerModule::t('Type'),
			'status' => SellerModule::t('Status'),
			'is_elite' => SellerModule::t('Elite'),
			'tag' => SellerModule::t('Tag'),
			'contact' => SellerModule::t('Contact'),
			'phone' => SellerModule::t('Phone'),
			'wangwang' => SellerModule::t('Wangwang'),
			'qq' => SellerModule::t('QQ'),
			'qq_group' => SellerModule::t('QQ Group'),
			'www' => SellerModule::t('Website'),
			'data_packet' => SellerModule::t('Data Packet'),
			'memo' => SellerModule::t('Memo'),
			'created' => SellerModule::t('Created'),
			'modified' => SellerModule::t('Modified'),
		);
	}

	public function scopes()
    {
        return array(
            'belong'=>array(
            	'condition'=>'supplier.user_id=:user_id',
            	'params'=>array(':user_id'=>Yii::app()->user->id),
            ),
            'type'=>array(
            	'condition'=>'type_id=:type_id',
            	'params'=>array(':type_id'=>$this->type_id),
            ),
            'notsafe'=>array(
            	'alias'=>'supplier',
            	'select' => 'id, name, type_id, status, is_elite, tag, contact, phone, wangwang, qq, qq_group, www, data_packet, memo, modified',
            ),
            'forcheck'=>array(
            	'alias'=>'supplier',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'supplier',
            	'select' => 'id, name',
            ),
        );
    }
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->created = time();
				$this->user_id=Yii::app()->user->id;
			}
			$this->creater=Yii::app()->user->id;
			$this->modified=time();
			return true;
		}
		else
			return false;
	}

	/**
	 * 返回供应商名称
	 * @param 供应商ID
	 * @return 供应商名称
	 */
	public static function getNameById($id) {
		if(!isset(self::$_namebyid[$id]))
			$_namebyid[$id] = Supplier::model()->simple()->findByPk($id)->name;
		return $_namebyid[$id];
	}

	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'is_elite' => array(
				self::ELITE_ACTIVE => SellerModule::t('Had Elite'),
				self::ELITE_NOACTIVE => SellerModule::t('No Elite'),
			),
			'status' => array(
				self::STATUS_ACTIVE => SellerModule::t('Enabled'),
				self::STATUS_NOACTIVE => SellerModule::t('Disabled'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

    public static function formatHtml($model)
    {
    	if(isset($model['type_id'])) $model['type_id'] = Type::Lists('supplier', $model['type_id']);
    	if(isset($model['created'])) $model['created'] = date('Y-m-d', $model['created']);
    	if(isset($model['modified'])) $model['modified'] = date('Y-m-d', $model['modified']);

    	return $model;
    }
}