<?php

class ProductExtend extends CActiveRecord
{	
	/**
	 * ProductExtend表的字段:
	 * @var integer $id
	 * @var integer $product_id
	 * @var integer $cid
	 * @var string $props
	 * @var string $property_alias
	 * @var string $input_pids
	 * @var string $input_str
	 * @var string $prop_imgs
	 * @var string $item_imgs
	 * @var string $description
     * @var integer $is_virtual
     * @var integer $modified
	*/
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	const ELITE_NOACTIVE=0;
	const ELITE_ACTIVE=1;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{product_extend}}';
	}

	public function rules()
	{
		return array(
			array('id', 'required'),
			array('product_id, cid, is_virtual', 'numerical', 'integerOnly'=>true),
			array('description, props, property_alias, input_pids, input_str, prop_imgs, item_imgs', 'safe'),
		);
	}

	public function relations()
	{
        return array(
			'product' => array(self::BELONGS_TO, 'Product', 'id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'product_id' => SellerModule::t('Product'),
			'modified' => SellerModule::t('Modified'),
		);
	}

	public function scopes()
    {
        return array(
            'belong'=>array(
            	'condition'=>'supply.user_id=:user_id',
            	'params'=>array(':user_id'=>Yii::app()->user->id),
            ),
            'product'=>array(
            	'condition'=>'supply.product_id=:product_id',
            	'params'=>array(':product_id'=>$this->product_id),
            ),
            'supply'=>array(
            	'condition'=>'supply.supply=:supply',
            	'params'=>array(':supply'=>$this->supply_id),
            ),
            'notsafe'=>array(
            	'alias'=>'supply',
            	'select' => 'id, supplier_id, product_id, name, status, is_elite, price, stock, www, memo, created, modified',
            ),
            'forcheck'=>array(
            	'alias'=>'supply',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'supply',
            	'select' => 'id, name',
            ),
        );
    }
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			$this->modified=time();
			return true;
		}
		else
			return false;
	}

	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'status' => array(
				self::STATUS_ACTIVE => SellerModule::t('Enabled'),
				self::STATUS_NOACTIVE => SellerModule::t('Disabled'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

    public function formatHtml($model)
    {
    	if(isset($model['created'])) $model['created'] = date('Y-m-d', $model['created']);
    	if(isset($model['modified'])) $model['modified'] = date('Y-m-d', $model['modified']);

    	return $model;
    }
}