<?php

class Sku extends CActiveRecord
{	
	/**
	 * SKu表的字段:
	 * @var integer $id
	 * @var integer $product_id
	 * @var integer $supply_id
	 * @var integer $user_id
	 * @var string $name
	 * @var integer $status
	 * @var string $sku_1
	 * @var string $sku_2
	 * @var integer $stock
	 * @var integer $inferior
	 * @var integer $selling_price
	 * @var integer $current_price
	 * @var integer $buiing_price
	 * @var string $location
	 * @var integer $tb_sku
     * @var integer $created
     * @var integer $modified
	*/
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	const ELITE_NOACTIVE=0;
	const ELITE_ACTIVE=1;

	private static $_skus = array();
	private static $_properties = array();

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{sku}}';
	}

	public function rules()
	{
		return array(
			array('product_id, name, sku_1, selling_price', 'required'),
			array('supply_id, product_id, status, stock, inferior', 'numerical', 'integerOnly'=>true),
			array('name, sku_2', 'length', 'max'=>20),
			array('location', 'length', 'max'=>10),
			array('current_price, buiing_price', 'safe'),
		);
	}

	public function relations()
	{
        return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'supply' => array(self::BELONGS_TO, 'Supply', 'supply_id'),
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => SellerModule::t('SKu ID'),
			'supply_id' => SellerModule::t('Supplier'),
			'product_id' => SellerModule::t('Product'),
			'name' => SellerModule::t('Name'),
			'status' => SellerModule::t('Status'),
			'selling_price' => SellerModule::t('Selling Price'),
			'current_price' => SellerModule::t('Current Price'),
			'buiing_price' => SellerModule::t('Buiing Price'),
			'stock' => SellerModule::t('Stock'),
			'inferior' => SellerModule::t('Inferior'),
			'location' => SellerModule::t('Location'),
			'created' => SellerModule::t('Created'),
			'modified' => SellerModule::t('Modified'),
		);
	}

	public function setBuiingPrice()
	{
		if(isset($this->supply_id))
		{
			$supply = Supply::model()->belong()->simple()->findByPk($this->supply_id);
			if($supply)
				$this->buiing_price = $supply->price;
			else
				unset($this->supply_id);
		}
	}

	public function scopes()
    {
        return array(
            'belong'=>array(
            	'condition'=>'sku.user_id=:user_id',
            	'params'=>array(':user_id'=>Yii::app()->user->id),
            ),
            'product'=>array(
            	'condition'=>'sku.product_id=:product_id',
            	'params'=>array(':product_id'=>$this->product_id),
            ),
            'supply'=>array(
            	'condition'=>'sku.supply_id=:supply_id',
            	'params'=>array(':supply_id'=>$this->supply_id),
            ),
            'notsafe'=>array(
            	'alias'=>'sku',
            	'select' => 'id, supply_id, product_id, name, status, sku_1, sku_2, stock, inferior, selling_price, current_price, buiing_price, location, created, modified',
            ),
            'forcheck'=>array(
            	'alias'=>'sku',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'sku',
            	'select' => 'id, product_id, supply_id, name, sku_1, sku_2, selling_price, buiing_price',
            ),
        );
    }

	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->created = time();
				$this->user_id=Yii::app()->user->id;
			}
			$this->modified=time();
			return true;
		}
		else
			return false;
	}

	public function name($name)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'Sku.name=:name',
			'params'=>array(':name'=>$name),
	    ));
	    return $this;
	}

	public static function property($id)
	{
		if(!isset(self::$_properties[$id]))
		{
			$criteria=new CDbCriteria(array(
				'condition'=>'sku.id=:id',
				'params'=>array(':id'=>$id),
				'scopes'=>array('belong','simple'),
				'with'=>array(
					'product'=>array('scopes'=>'simple'),
				),
			));
			$sku = self::model()->find($criteria);
			if($sku)
			{
				$property = $sku->product->sku_1 .":". $sku->sku_1;
				if($sku->sku_2)
					$property .= ";". $sku->product->sku_2 .":". $sku->sku_2;
				self::$_properties[$id] = $property;
			}
			else
				return false;
		}

		return isset(self::$_properties[$id]) ? self::$_properties[$id] : false;
	}

	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'status' => array(
				self::STATUS_ACTIVE => SellerModule::t('Selling'),
				self::STATUS_NOACTIVE => SellerModule::t('Downtime'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

	public static function Lists($id)
	{
		if(!isset(self::$_skus[$id]))
		{
			$items = self::model()->belong()->notsafe()->findByPk($id);
			if($items)
				$_skus[$id] = $items;
			else
				return false;
		}
		
		return isset(self::$_skus[$id]) ? self::$_skus[$id] : false;
	}

    public function formatHtml($model)
    {
    	if(isset($model['created'])) $model['created'] = date('Y-m-d', $model['created']);
    	if(isset($model['modified'])) $model['modified'] = date('Y-m-d', $model['modified']);

    	return $model;
    }
}