<?php

class ServiceFund extends CActiveRecord
{	
	/**
	 * ServiceFund:
	 * @var string $service_id
	 * @var integer $money
	 * @var integer $type
	 * @var string $channel
	*/
	const FLOW_PAYOUT=0; //支出
	const FLOW_INCOME=1; //收入

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{service_fund}}';
	}

	public function rules()
	{
		return array(
			array('service_id', 'required'),
			array('service_id, flow', 'numerical', 'integerOnly'=>true),
			array('channel', 'length', 'max'=>30),
			array('money', 'safe'),
		);
	}

	public function relations()
	{
        return array(
			'service' => array(self::BELONGS_TO, 'Service', 'service_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'service_id' => SellerModule::t('Service'),
			'money' => SellerModule::t('Money'),
			'flow' => SellerModule::t('Flow'),
			'channel' => SellerModule::t('Channel'),
		);
	}

	public function scopes()
    {
        return array(
            'service'=>array(
            	'condition'=>'fund.service_id=:service_id',
            	'params'=>array(':service_id'=>$this->service_id),
            ),
            'fund'=>array(
            	'condition'=>'fund.fund=:fund',
            	'params'=>array(':fund'=>$this->fund_id),
            ),
            'notsafe'=>array(
            	'alias'=>'fund',
            	'select' => 'id, service_id, money, type, channel',
            ),
            'forcheck'=>array(
            	'alias'=>'fund',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'fund',
            	'select' => 'id, service_id',
            ),
        );
    }

	/**
	 * 获取模块名称
	 */
	public function getflowName()
    {
        return $this->itemAlias('flow', $this->flow);
    }

	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'flow' => array(
				self::FLOW_PAYOUT => SellerModule::t('打款'),
				self::FLOW_INCOME => SellerModule::t('收款'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

    public function formatHtml($model)
    {
    	if(isset($model['flow'])) $model['flow'] = self::itemAlias('flow', $model['flow']);

    	return $model;
    }
}