<?php

class Stock extends CActiveRecord
{	
	/**
	 * Stock表的字段:
	 * @var integer $id
	 * @var integer $sku_id
	 * @var integer $user_id
	 * @var string $type
	 * @var integer $price
	 * @var integer $stock
	 * @var integer $inferior
	 * @var string $memo
	 * @var integer $supply_id
     * @var integer $created
     * @var integer $modified
	*/
	const TYPE_BUY = 1; //买入 +
	const TYPE_SELL = 2; //售出 -
	const TYPE_RECYCLE = 3; //回收 + 买家退换货
	const TYPE_RETURN = 4; //退货 + 退回供应商
	const TYPE_LOST = 5; //遗失 -
	const TYPE_GAIN = 6; //盈余 +
	const TYPE_BROKEN = 7; //损坏 - +
	const TYPE_REPAIR = 8; //次品修复 + -
	const TYPE_DESTROY = 9; //次品销毁 -
	const TYPE_SHIPP = 10; //运输 - +

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{stock}}';
	}

	public function rules()
	{
		return array(
			array('sku_id, type', 'required'),
			array('stock, inferior', 'resize'),
			array('supply_id, sku_id, type, stock, inferior', 'numerical', 'integerOnly'=>true),
			array('memo', 'length', 'max'=>30),
			array('price', 'safe'),
		);
	}

	public function resize($attribute,$params)
	{
        if(!$this->stock && !$this->inferior)
        	$this->addError($attribute, '库存和次品必须填写一个');
	}

	public function relations()
	{
        return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'supply' => array(self::BELONGS_TO, 'Supply', 'supply_id'),
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
			'sku' => array(self::BELONGS_TO, 'Sku', 'sku_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => SellerModule::t('Stock ID'),
			'supply_id' => SellerModule::t('Supplier'),
			'product_id' => SellerModule::t('Product'),
			'sku_id' => SellerModule::t('Sku'),
			'type' => SellerModule::t('Type'),
			'price' => SellerModule::t('Buiing Price'),
			'stock' => SellerModule::t('Quantity'),
			'inferior' => SellerModule::t('Inferior'),
			'memo' => SellerModule::t('Memo'),
			'created' => SellerModule::t('Created'),
			'modified' => SellerModule::t('Modified'),
		);
	}

	public function scopes()
    {
        return array(
            'belong'=>array(
            	'condition'=>'stock.user_id=:user_id',
            	'params'=>array(':user_id'=>Yii::app()->user->id),
            ),
            'sku'=>array(
            	'condition'=>'stock.sku_id=:sku_id',
            	'params'=>array(':sku_id'=>$this->sku_id),
            ),
            'supply'=>array(
            	'condition'=>'stock.supply_id=:supply_id',
            	'params'=>array(':supply_id'=>$this->supply_id),
            ),
            'sku'=>array(
            	'condition'=>'stock.sku_id=:sku_id',
            	'params'=>array(':sku_id'=>$this->sku_id),
            ),
            'notsafe'=>array(
            	'alias'=>'stock',
            	'select' => 'id, sku_id, type, price, stock, integer, memo, supply_id, created, modified',
            ),
            'forcheck'=>array(
            	'alias'=>'stock',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'stock',
            	'select' => 'id, sku_id',
            ),
        );
    }
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->created = time();
				$this->user_id=Yii::app()->user->id;
			}
			$this->modified=time();
			return true;
		}
		else
			return false;
	}

	public function updateSkuStock()
	{
		switch ($this->type)
		{
			case self::TYPE_BUY :
			case self::TYPE_RECYCLE :
			case self::TYPE_GAIN :
				$counters = array('stock'=>$this->stock, 'inferior'=>$this->inferior);
				break;
			
			case self::TYPE_SELL :
			case self::TYPE_RETURN :
			case self::TYPE_LOST :
				$counters = array('stock'=>-$this->stock, 'inferior'=>-$this->inferior);
				break;

			case self::TYPE_BROKEN :
				$counters = array('stock'=>-$this->stock, 'inferior'=>$this->stock);
				break;

			case self::TYPE_REPAIR :
				$counters = array('stock'=>$this->stock, 'inferior'=>-$this->stock);
				break;

			case self::TYPE_DESTROY :
				$counters = array('inferior'=>-$this->stock);
				break;
		}
		Sku::model()->updateCounters($counters, 'id=:id', array(':id'=>$this->sku_id));
		$sku = Sku::model()->belong()->simple()->findByPk($this->sku_id);
		Product::model()->updateCounters($counters, 'id=:id', array(':id'=>$sku->product_id));
	}
	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'stock' => array(
				self::TYPE_BUY => SellerModule::t('Buy'),
				self::TYPE_SELL => SellerModule::t('Sell'),
				self::TYPE_RECYCLE => SellerModule::t('Recycle'),
				self::TYPE_RETURN => SellerModule::t('Return'),
				self::TYPE_LOST => SellerModule::t('Lost'),
				self::TYPE_GAIN => SellerModule::t('Gain'),
				self::TYPE_BROKEN => SellerModule::t('Broken'),
				self::TYPE_REPAIR => SellerModule::t('Repair'),
				self::TYPE_DESTROY => SellerModule::t('Destroy'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

    public function formatHtml($model)
    {
    	if(isset($model['created'])) $model['created'] = date('Y-m-d H:s:i', $model['created']);
    	if(isset($model['modified'])) $model['modified'] = date('Y-m-d H:s:i', $model['modified']);

    	return $model;
    }
}