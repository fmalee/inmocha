<?php

class Shop extends CActiveRecord
{
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;

	private static $_shops = array();
	/**
	 * Shop表的字段:
	 * @var integer $shop_id
	 * @var integer $owner_id
	 * @var string $nick
	 * @var integer $status
	 * @var string $uid
	 * @var string $type
	 * @var string $session_key
	 * @var integer $has_setting
	 * @var string $setting
     * @var integer $syn_trade
     * @var integer $syn_product
     * @var integer $created
     * @var integer $modified
	 */


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{shop}}';
	}

	public function rules()
	{
		return array(
			array('user, type, issend, ispay', 'required'),
			array('type, issend, ispay, created', 'numerical', 'integerOnly'=>true),
			//array('modified', 'default', 'value' => date('Y-m-d H:i:s'), 'setOnEmpty' => true, 'on' => 'insert'),
			array('tid, user, contact, qq, memo, type, issend, ispay, created', 'safe'),
			array('tid, user, contact, qq, memo, type, issend, ispay, created', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
        return array(
			'Member' => array(self::BELONGS_TO, 'Member', 'id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'tid' => SellerModule::t("tid"),
			'user'=>SellerModule::t("user"),
			'contact'=>SellerModule::t("contact"),
			'qq'=>SellerModule::t("qq"),
			'memo'=>SellerModule::t("memo"),
			'issend' => SellerModule::t("issend"),
			'type' => SellerModule::t("type"),
			'ispay' => SellerModule::t("ispay"),
			'created' => SellerModule::t("created"),
			'modified' => SellerModule::t("modified"),
		);
	}

	public function scopes()
    {
        return array(
            'belong'=>array(
                'condition'=>'owner_id='.Yii::app()->user->id,
            ),
            'active'=>array(
                'condition'=>'status='.self::STATUS_ACTIVE,
            ),
            'notactive'=>array(
                'condition'=>'status='.self::STATUS_NOACTIVE,
            ),
            'setSession'=>array(
                'select' => 'id, shop_id, nick, session_key',
            )
        );
    }

	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->created=$this->modified=time();
				$this->owner_id=Yii::app()->user->id;
			}
			else
				$this->modified=time();
			return true;
		}
		else
			return false;
	}

	/**
     * 根据NICK获取ID
     */
	public static function getIdByNick($nick)
	{
		if(!isset(self::$_shops[$nick]))
		{
			$shop = self::model()->find(array(
				'select'=>'id,nick',
				'condition'=>'nick=:nick',
				'params'=>array(':nick'=>$nick)));
			if($shop)
			{
				self::$_shops[$nick] = $shop->id;
				return $shop->id;
			}
			else
				return false;
		}
		else
			return self::$_shops[$nick];
	}

	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'status' => array(
				self::STATUS_ACTIVE => Yii::t('common', 'Enabled'),
				self::STATUS_NOACTIVE => Yii::t('common', 'Disabled'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}
	
    public function search()
    {
        $criteria=new CDbCriteria(array(
			'condition'=>'credit.nick=$nick',
			'params'=>array(':nick'=>'英摩卡'),
			'order'=>'credit.created DESC',
			'with'=>'trade',
			'limit'=>10,
		));
        
        $criteria->compare('tid',$this->tid,true);
        $criteria->compare('nick',$this->nick,true);
        $criteria->compare('user',$this->user,true);
        $criteria->compare('contact',$this->contact,true);
        $criteria->compare('qq',$this->qq,true);
        $criteria->compare('memo',$this->memo,true);
        $criteria->compare('issend',$this->issend);
        $criteria->compare('credit.type',$this->type);
        $criteria->compare('ispay',$this->ispay);
        $criteria->compare('created',$this->created);
        $criteria->compare('buyer_nick',$this->buyer_nick,true);
        $criteria->compare('modified',$this->modified);

        return $criteria;
    }

    public function formatHtml($model) {
    	if(isset($model['type'])) $model['type'] = self::itemAlias('type', $model['type']);
    	if(isset($model['issend'])) $model['issend'] = self::itemAlias('issend', $model['issend']);
    	//if(isset($model['ispay'])) $model['ispay'] = self::itemAlias('ispay', $model['ispay']);
    	if(isset($model['created'])) $model['created'] = date('Y-m-d', $model['created']);
    	if(isset($model['modified'])) $model['modified'] = date('Y-m-d', $model['modified']);

    	return $model;
    }
}