<?php

class ItemProp extends CActiveRecord
{	
	/**
	 * ItemProp:
	 * @var integer $cid
	 * @var string $pid
	 * @var integer $name
	 * @var integer $parent_pid
	 * @var string $parent_vid
	 * @var string $status
	 * @var string $is_color_prop
	 * @var string $is_enum_prop
	 * @var string $is_input_prop
	 * @var string $is_item_prop
	 * @var string $is_key_prop
	 * @var string $is_sale_prop
	 * @var string $is_allow_alias
	 * @var string $multi
	 * @var string $must
	 * @var string $sort_order
	 * @var string $prop_values
	 * @var string $child_template
	 * @var string $modified
	*/
	const STATUS_NORMAL=0; //正常
	const STATUS_DELETED=1; //删除

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{item_prop}}';
	}

	public function rules()
	{
		return array(
			array('cid,pid,name', 'required'),
			array('parent_pid, parent_vid,status,is_color_prop,is_enum_prop,is_input_prop,is_item_prop,is_key_prop,is_sale_prop,is_allow_alias,multi,must,sort_order', 'numerical', 'integerOnly'=>true),
			array('child_template', 'length', 'max'=>50),
			array('prop_values', 'safe'),
		);
	}

	public function relations()
	{
        return array(
			'cat' => array(self::BELONGS_TO, 'Cat', 'cid'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'service_id' => SellerModule::t('Service'),
			'money' => SellerModule::t('Money'),
			'flow' => SellerModule::t('Flow'),
			'channel' => SellerModule::t('Channel'),
		);
	}

	public function scopes()
    {
        return array(
            'normal'=>array(
            	'condition'=>'prop.status=:status',
            	'params'=>array(':status'=>self::STATUS_NORMAL),
            ),
            'deleted'=>array(
            	'condition'=>'prop.status=:status',
            	'params'=>array(':status'=>self::STATUS_DELETED),
            ),
            'order'=>array(
            	'order'=>'prop.sort_order ASC',
            ),
            'saleProp'=>array(
            	'condition'=>'prop.is_sale_prop=1',
            ),
            'KeyProp'=>array(
            	'condition'=>'prop.is_key_prop=1',
            ),
            'notsafe'=>array(
            	'alias'=>'prop',
            	'select' => 'cid,pid,name,parent_pid, parent_vid,status,is_color_prop,is_enum_prop,is_input_prop,is_item_prop,is_key_prop,is_sale_prop,is_allow_alias,multi,must,sort_order,prop_values',
            	'order'=>'prop.sort_order ASC',
            ),
            'forcheck'=>array(
            	'alias'=>'prop',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'prop',
            	'select' => 'cid, pid, name',
            	'order'=>'prop.sort_order ASC',
            ),
        );
    }

	public function cid($cid)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'prop.cid='.$cid,
			//'params'=>array(':cid'=>$cid),
	    ));
	    return $this;
	}

	public function pid($pid)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'prop.pid='.$pid,
			//'params'=>array(':pid'=>$pid),
	    ));
	    return $this;
	}

    /**
	 * 商品属性别称
	 */
	public static function propAlias($alias)
	{
		$properties = array();
		$alias = explode(';', $alias);
		foreach ($alias as $alia)
		{
			$pos = strrpos($alia, ':');
			$pids = substr($alia, 0, $pos);
			$properties[$pids] = substr($alia, $pos+1);
		}

		return $properties;
    }

	/**
	 * 返回产品销售属性
	 * @param 产品ID
	 * @return 产品名称
	 */
	public static function saleNames($cid)
	{
		$props = ItemProp::model()->simple()->saleProp()->cid($cid)->findALL();
		return $props;
	}

	/**
	 * 获取宝贝属性
	 * @param $data 宝贝数据
	 *        	接受num_iid 或是$data数据
	 * @param $type 输出类型0=全部属性
	 *        	1=销售属性 2=关键属性 3=非关键属性 4=属性名称 5=属性值
	 * @param $props 直接传入需要格式化的数值
	 *        	输出：array ('颜色' => array ('1627207:28332' =>
	 *        	'灰色','1627207:28320' => '白色','1627207:28341' => '黑色'))
	 *        	输出：name pivdname:vidnam;pivdname:vidname
	 *        	get_props($data); 以 array 输出全部的props
	 *        	get_props($data, 1); 以 array 输出带销售属性的props
	 *        	get_props($id, 0, $props) 以 array 输出传入的$props
	 *        	get_props($id, 3, $props) 以 name 输出传入的$props
	 */
	public function get_props($data, $type = 0, $props = '') {
		if (!array_key_exists('cid', $data)) { // 判断数组
			$data = M('Item')->where("num_iid=$data")->field('cid,input_pids,input_str,property_alias,props')->find();
			if (!$data) return false;
		}
		$cid = $data['cid'];
		$input_pids = explode(',', $data['input_pids']);
		$input_str = explode(',', $data['input_str']);
		foreach ( $input_pids as $k => $v ) {
			$input [$v] = $input_str [$k];
		}
		$property_alias = explode(';', $data['property_alias']);
		$alias = array();
		foreach ( $property_alias as $v ) {
			$v = explode ( ':', $v );
			$key = $v [0] . ":" . $v [1]; // 组成pid:vid为key的数组
			$alias[$key] = $v [2];
		}
		
		if (! $props) $props = $data ['props']; // 是否接受传入的props
		$props = explode ( ';', $props );
		$props_arr = array ();
		foreach ($props as $prop) {
			$v = explode(':', $prop);
			$pid = $v[0];
			$vid = $v[1];
			$r = $this->where(array('pid' => $pid, 'cid' => $cid))->field('name, is_sale_prop, is_key_prop, prop_values')->find();
			$pname = $r['name'];
			if (array_key_exists($pid, $input)) {
				$name = $input[$pid];
			} elseif (array_key_exists($prop, $alias)) {
				$name = $alias[$prop];
			} else {
				$prop_values = unserialize($r['prop_values']);
				$name = $prop_values[$vid];
			}
			
			if ($type == 1) {
				if ($r ['is_sale_prop']) $props_arr[$pname][$prop] = $name;
			} elseif ($type == 2) {
				if ($r ['is_key_prop']) $props_arr[$pname][$prop] = $name;
			} elseif ($type == 3) {
				if ($r ['is_sale_prop'] || $r ['is_key_prop']) continue;
				$props_arr[$pname][$prop] = $name;
			} elseif ($type == 4) {
				$props_arr[$pname] = $pname . ":" . $name;
			} elseif ($type == 5) {
				$props_arr[$pname] = $name;
			} else {
				$props_arr[$pname][$prop] = $name;
			}
		}
		if ($type == 4)	$props_arr = implode ( ';', $props_arr );
		if ($type == 5)	$props_arr = implode ( ':', $props_arr );
		return $props_arr;
	}


	

	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'flow' => array(
				self::STATUS_NORMAL => SellerModule::t('打款'),
				self::STATUS_DELETED => SellerModule::t('收款'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

    public function formatHtml($model)
    {
    	if(isset($model['flow'])) $model['flow'] = self::itemAlias('flow', $model['flow']);

    	return $model;
    }
}