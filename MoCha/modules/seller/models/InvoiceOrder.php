<?php

class InvoiceOrder extends CActiveRecord
{	
	/**
	 * InvoiceOrder表的字段:
	 * @var integer $id
	 * @var integer $invoice_id
	 * @var integer $trade_id
	 * @var integer $order_id
	 * @var integer $product_id
	 * @var integer $sku_id
	 * @var integer $supply_id
	 * @var integer $quantity
	 * @var string $product
	 * @var string $sku
	 * @var string $supplier
     * @var integer $price
	 * @var integer $item_id
	 * @var integer $tb_sku_id
     * @var integer $created
	*/

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{invoice_order}}';
	}

	public function rules()
	{
		return array(
			array('invoice_id, sku_id, trade_id, quantity', 'required', 'on'=>'create'),
			array('invoice_id, sku_id, trade_id, order_id, product_id, supply_id, item_id, tb_sku_id, quantity,', 'numerical', 'integerOnly'=>true),
			array('product, supplier', 'length', 'max'=>20),
			array('sku', 'length', 'max'=>255),
			array('price', 'safe'),
		);
	}

	public function relations()
	{
        return array(
			'invoice' => array(self::BELONGS_TO, 'Invoice', 'invoice_id'),
			'sku' => array(self::BELONGS_TO, 'Sku', 'sku_id'),
			'trade' => array(self::BELONGS_TO, 'Trade', 'trade_id'),
			'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
			'item' => array(self::BELONGS_TO, 'Item', 'item_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => SellerModule::t('ID'),
			'invoice_id' => SellerModule::t('Invoice'),
			'sku_id' => SellerModule::t('Sku ID'),
			'trade_id' => SellerModule::t('Trade'),
			'order_id' => SellerModule::t('Order'),
			'item_id' => SellerModule::t('Item'),
			'tb_sku_id' => SellerModule::t('Taobao Sku'),
			'quantity' => SellerModule::t('Quantity'),
			'product' => SellerModule::t('Product'),
			'supplier' => SellerModule::t('Supplier'),
			'sku' => SellerModule::t('Sku'),
			'price' => SellerModule::t('Price'),
			'created' => SellerModule::t('Created'),
		);
	}

	public function scopes()
    {
        return array(
            'notsafe'=>array(
            	'alias'=>'invoiceOrder',
            	'select' => 'id, invoice_id, sku_id, trade_id, order_id, item_id, tb_sku_id, quantity, product, supplier, sku, price',
            ),
            'forcheck'=>array(
            	'alias'=>'invoiceOrder',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'invoiceOrder',
            	'select' => 'id, invoice_id, trade_id, sku_id',
            ),
        );
    }

    public function belong($id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'invoiceOrder.invoice_id='.$id
	    ));
	    return $this;
	}

	public function trade($id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'invoiceOrder.trade_id='.$id
	    ));
	    return $this;
	}

	public function order($id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'invoiceOrder.order_id='.$id
	    ));
	    return $this;
	}

	public function sku($id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'invoiceOrder.sku='.$id
	    ));
	    return $this;
	}

	public function product($name)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'invoiceOrder.product=:name',
			'params'=>array(':name'=>$name),
	    ));
	    return $this;
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
				$this->created = time();
			return true;
		}
		else
			return false;
	}

    public function formatHtml($model)
    {
    	if(isset($model['created'])) $model['created'] = date('Y-m-d', $model['created']);

    	return $model;
    }
}