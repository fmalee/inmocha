<?php

class LogisticsOrder extends CActiveRecord
{	
	/**
	 * LogisticsOrder:
	 * @var integer $id
	 * @var integer $company_id
	 * @var string $number
	 * @var integer $status
	 * @var integer $is_send
	 * @var integer $start_time
	 * @var integer $end_time
	 * @var integer $module
	 * @var integer $table_id
	 * @var integer $created
	 * @var integer $modified
	*/
	//状态
	const STATUS_CREATED = 0;
	const STATUS_RECREATED = 1;
	const STATUS_CANCELLED = 2;
	const STATUS_CLOSED = 3;
	const STATUS_SENDING = 4;
	const STATUS_ACCEPTING = 5;
	const STATUS_ACCEPTED = 5;
	const STATUS_PICK_UP = 6;
	const STATUS_PICK_UP_FAILED = 6;
	const STATUS_LOST = 7;
	const STATUS_REJECTED_BY_RECEIVER = 8;
	const STATUS_ACCEPTED_BY_RECEIVER = 9;
	//模块
	const MODULE_TRADE = 1;
	const MODULE_NIFFER = 2;
	const MODULE_REISSUE = 3;
	const MODULE_RETURN = 4;
	//类型
	const SEND = 0;
	const RECEIVER = 1;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{logistics_order}}';
	}

	public function rules()
	{
		return array(
			array('company_id, number, module, table_id', 'required', 'on'=>'create'),
			array('company_id, status, is_send, start_time, end_time, module, table_id', 'numerical', 'integerOnly'=>true),
			array('number', 'length', 'max'=>20),
		);
	}

	public function relations()
	{
        return array(
			'company' => array(self::BELONGS_TO, 'LogisticsCompany', 'company_id'),
			'sku' => array(self::BELONGS_TO, 'Sku', 'sku_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => SellerModule::t('Service'),
			'money' => SellerModule::t('Money'),
			'flow' => SellerModule::t('Flow'),
			'channel' => SellerModule::t('Channel'),
		);
	}

	public function scopes()
    {
        return array(
            'notsafe'=>array(
            	'alias'=>'order',
            	'select' => 'id, company_id, number',
            ),
            'forcheck'=>array(
            	'alias'=>'order',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'order',
            	'select' => 'id, company_id, number',
            ),
        );
    }
}