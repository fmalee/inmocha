<?php

class ServiceReissue extends CActiveRecord
{	
	/**
	 * ServiceReissue:
	 * @var integer $id
	 * @var integer $service_id
	 * @var integer $sku_id
	 * @var integer $quantity
	 * @var string $product
	 * @var string $sku
	 * @var string $logistics_id
	*/

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{service_reissue}}';
	}

	public function rules()
	{
		return array(
			array('service_id', 'required'),
			array('service_id, sku_id, logistics_id', 'numerical', 'integerOnly'=>true),
			array('product', 'length', 'max'=>20),
			array('sku', 'length', 'max'=>255),
		);
	}

	public function relations()
	{
        return array(
			'service' => array(self::BELONGS_TO, 'Service', 'service_id'),
			'sku' => array(self::BELONGS_TO, 'Sku', 'sku_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'service_id' => SellerModule::t('Service'),
			'money' => SellerModule::t('Money'),
			'flow' => SellerModule::t('Flow'),
			'channel' => SellerModule::t('Channel'),
		);
	}

	public function scopes()
    {
        return array(
            'service'=>array(
            	'condition'=>'fund.service_id=:service_id',
            	'params'=>array(':service_id'=>$this->service_id),
            ),
            'fund'=>array(
            	'condition'=>'fund.fund=:fund',
            	'params'=>array(':fund'=>$this->fund_id),
            ),
            'notsafe'=>array(
            	'alias'=>'fund',
            	'select' => 'service_id, order_id, sku_id, product, quantity, logistics_company, logistics_number',
            ),
            'forcheck'=>array(
            	'alias'=>'fund',
            	'select' => 'service_id',
            ),
            'simple'=>array(
            	'alias'=>'fund',
            	'select' => 'service_id, sku_id, quantity',
            ),
        );
    }
}