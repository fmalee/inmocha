<?php

class Service extends CActiveRecord
{	
	/**
	 * Service表的字段:
	 * @var integer $id
	 * @var integer $shop_id
	 * @var integer $trade_id
	 * @var integer $type_id
	 * @var integer $status
	 * @var string $module
	 * @var string $memo
     * @var integer $created
     * @var integer $modified
	*/
	//状态
	const STATUS_FOLLOW = 0;
	const STATUS_RETURN = 1;
	const STATUS_RECIEVE = 2;
	const STATUS_SEND = 3;
	const STATUS_FAIL = 4;
	const STATUS_FINISH = 5;

	//模块
	const MODULE_FUND = 'fund';
	const MODULE_TALK = 'talk';
	const MODULE_RETURN = 'return';
	const MODULE_REISSUE = 'reissue';
	const MODULE_NIFFER = 'niffer';

	private static $_namebyid=array();

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{service}}';
	}

	public function rules()
	{
		return array(
			array('trade_id, type_id, status, module', 'required'),
			array('trade_id, type_id, status', 'numerical', 'integerOnly'=>true),
			array('memo', 'length', 'max'=>255),
			array('created,modified', 'safe'),
		);
	}

	public function relations()
	{
        return array(
			'trade' => array(self::BELONGS_TO, 'Trade', 'trade_id', 'scopes'=>'simple'),
			'type' => array(self::BELONGS_TO, 'Type', 'type_id'),
			'return' => array(self::HAS_MANY, 'ServiceReturn', 'service_id'),
			'reissue' => array(self::HAS_MANY, 'ServiceReissue', 'service_id'),
			'niffer' => array(self::HAS_MANY, 'ServiceNiffer', 'service_id'),
			'fund' => array(self::HAS_ONE, 'ServiceFund', 'service_id'),
			'talk' => array(self::HAS_ONE, 'ServiceTalk', 'service_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => SellerModule::t('Service ID'),
			'trade_id' => SellerModule::t('Trade'),
			'type_id' => SellerModule::t('Type'),
			'status' => SellerModule::t('Status'),
			'module' => SellerModule::t('Module'),
			'memo' => SellerModule::t('Memo'),
			'created' => SellerModule::t('Created'),
			'modified' => SellerModule::t('Modified'),
		);
	}

	public function scopes()
    {
        return array(
            'belong'=>array(
            	'condition'=>'service.shop_id=:shop_id',
            	'params'=>array(':shop_id'=>Yii::app()->user->tbid),
            ),
            'type'=>array(
            	'condition'=>'service.type_id=:type_id',
            	'params'=>array(':type_id'=>$this->type_id),
            ),
            'return'=>array(
            	'condition'=>'service.module=:module',
            	'params'=>array(':module'=>self::MODULE_RETURN),
            ),
            'notsafe'=>array(
            	'alias'=>'service',
            	'select' => 'id, trade_id, type_id, status, module, memo, created, modified',
            ),
            'forcheck'=>array(
            	'alias'=>'service',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'service',
            	'select' => 'id, status, module',
            ),
        );
    }
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->created = time();
				$this->shop_id=Yii::app()->user->tbid;
			}
			$this->modified=time();
			return true;
		}
		else
			return false;
	}

	public function module($name)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'Service.module=:module',
			'params'=>array(':module'=>$name),
	    ));
	    return $this;
	}

	public function status($status)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'Service.module='.$status
	    ));
	    return $this;
	}

	/**
	 * 获取类别名称
	 */
	public function getTypeName()
    {
        return Type::model()->simple()->findByPk($this->type_id)->name;
    }

    /**
	 * 获取状态名称
	 */
	public function getStatusName()
    {
        return $this->itemAlias('status', $this->status);
    }
    
    /**
	 * 获取模块名称
	 */
	public function getModuleName()
    {
        return $this->itemAlias('module', $this->module);
    }

	/**
	 * 返回产品名称
	 * @param 产品ID
	 * @return 产品名称
	 */
	public static function getNameById($id) {
		if(!isset(self::$_namebyid[$id]))
			$_namebyid[$id] = Service::model()->simple()->findByPk($id)->name;
		return $_namebyid[$id];
	}

	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'status' => array(
				self::STATUS_FOLLOW => SellerModule::t('跟踪中'),
				self::STATUS_RETURN => SellerModule::t('退货中'),
				self::STATUS_RECIEVE => SellerModule::t('已收货'),
				self::STATUS_SEND => SellerModule::t('已发货'),
				self::STATUS_FAIL => SellerModule::t('失败'),
				self::STATUS_FINISH => SellerModule::t('完成'),
			),
			'fundStatus' => array(
				self::STATUS_FOLLOW => SellerModule::t('跟踪中'),
				self::STATUS_FINISH => SellerModule::t('完成'),
			),
			'talkStatus' => array(
				self::STATUS_FOLLOW => SellerModule::t('跟踪中'),
				self::STATUS_FAIL => SellerModule::t('失败'),
				self::STATUS_FINISH => SellerModule::t('完成'),
			),
			'module' => array(
				self::MODULE_FUND => SellerModule::t('账务'),
				self::MODULE_TALK => SellerModule::t('沟通'),
			),
			'model' => array(
				'fund' => 'ServiceFund',
				'talk' => 'ServiceTalk',
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

    public function formatHtml($model)
    {
    	if(isset($model['status'])) $model['status'] = self::itemAlias('status', $model['status']);
    	if(isset($model['type_id'])) $model['type_id'] = Type::Lists('service', $model['type_id']);

    	if(isset($model['created'])) $model['created'] = date('Y-m-d', $model['created']);
    	if(isset($model['modified'])) $model['modified'] = date('Y-m-d', $model['modified']);

    	return $model;
    }
}