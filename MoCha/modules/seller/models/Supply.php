<?php

class Supply extends CActiveRecord
{	
	/**
	 * Supply表的字段:
	 * @var integer $id
	 * @var integer $supplier_id
	 * @var integer $product_id
	 * @var integer $user_id
	 * @var string $name
	 * @var integer $status
	 * @var integer $is_elite
	 * @var integer $price
	 * @var integer $stock
	 * @var string $www
	 * @var string $memo
     * @var integer $created
     * @var integer $modified
	*/
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	const ELITE_NOACTIVE=0;
	const ELITE_ACTIVE=1;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{supply}}';
	}

	public function rules()
	{
		return array(
			array('name, supplier_id, product_id, price', 'required'),
			array('supplier_id, product_id, status, is_elite, stock', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>20),
			array('www, memo', 'length', 'max'=>128),
		);
	}

	public function relations()
	{
        return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'supplier' => array(self::BELONGS_TO, 'Supplier', 'supplier_id'),
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => SellerModule::t('Supply ID'),
			'supplier_id' => SellerModule::t('Supplier'),
			'product_id' => SellerModule::t('Product'),
			'name' => SellerModule::t('Name'),
			'status' => SellerModule::t('Status'),
			'is_elite' => SellerModule::t('Elite'),
			'price' => SellerModule::t('Buiing Price'),
			'stock' => SellerModule::t('Stock'),
			'www' => SellerModule::t('WebSite'),
			'memo' => SellerModule::t('Memo'),
			'created' => SellerModule::t('Created'),
			'modified' => SellerModule::t('Modified'),
		);
	}

	public function scopes()
    {
        return array(
            'belong'=>array(
            	'condition'=>'supply.user_id=:user_id',
            	'params'=>array(':user_id'=>Yii::app()->user->id),
            ),
            'product'=>array(
            	'condition'=>'supply.product_id=:product_id',
            	'params'=>array(':product_id'=>$this->product_id),
            ),
            'supplier'=>array(
            	'condition'=>'supply.supplier_id=:supplier_id',
            	'params'=>array(':supplier_id'=>$this->supplier_id),
            ),
            'notsafe'=>array(
            	'alias'=>'supply',
            	'select' => 'id, supplier_id, product_id, name, status, is_elite, price, stock, www, memo, created, modified',
            ),
            'forcheck'=>array(
            	'alias'=>'supply',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'supply',
            	'select' => 'id, name, price, stock, supplier_id',
            ),
        );
    }
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->created = time();
				$this->user_id=Yii::app()->user->id;
			}
			$this->modified=time();
			return true;
		}
		else
			return false;
	}

	public function getSupplierName()
	{
		return Supplier::model()->getNameById($this->supplier_id);
	}

	/**
	 * 设置首先供应商
	 * @param $supply 供应商ID
	 * @param $product 产品ID
	 */
	public static function setMajor($supply_id, $product_id)
	{
		$supply = self::model()->belong()->simple()->findByPk($supply_id);
		if(!$supply)
			return false;
		$product = Product::model()->belong()->simple()->findByPk($product_id);
		if(!$product)
			return false;
		$product->supply_id = $supply_id;
		$product->buiing_price = $supply->price;
		$product->save();

		$attributes = array('supply_id'=>$supply_id, 'buiing_price'=>$supply->price);
		$condition = 'product_id=:product_id';
		$params = array(':product_id'=>$product_id);
		Sku::model()->updateAll($attributes, $condition, $params);

		return true;
	}

	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'is_elite' => array(
				self::ELITE_ACTIVE => SellerModule::t('Had Elite'),
				self::ELITE_NOACTIVE => SellerModule::t('No Elite'),
			),
			'status' => array(
				self::STATUS_ACTIVE => SellerModule::t('Enabled'),
				self::STATUS_NOACTIVE => SellerModule::t('Disabled'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

    public function formatHtml($model)
    {
    	if(isset($model['created'])) $model['created'] = date('Y-m-d', $model['created']);
    	if(isset($model['modified'])) $model['modified'] = date('Y-m-d', $model['modified']);

    	return $model;
    }
}