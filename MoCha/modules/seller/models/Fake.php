<?php

class Fake extends CActiveRecord
{	
	/**
	 * Fake表的字段:
	 * @var integer $id
	 * @var string $shop_id
	 * @var string $user
	 * @var string $contact
	 * @var string $qq
	 * @var string $memo
	 * @var integer $type_id
	 * @var integer $is_send
	 * @var integer $is_pay
	 * @var integer $user_id
     * @var integer $created
     * @var integer $modified
	 */
	//public $buyer_nick;
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{fake}}';
	}

	public function rules()
	{
		return array(
			array('user, type_id, is_send, is_pay', 'required'),
			array('id', 'numerical', 'integerOnly'=>true),
			array('type_id, is_send, is_pay', 'numerical', 'integerOnly'=>true),
			array('user, contact, qq', 'length', 'max'=>20),
			array('memo', 'length', 'max'=>255),
		);
	}

	public function relations()
	{
        return array(
			'shop' => array(self::BELONGS_TO, 'Shop', '', 'on'=>'fake.shop_id=shop.id'),
			'trade' => array(self::BELONGS_TO, 'Trade', 'id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id'=>SellerModule::t("Trade ID"),
			'user'=>SellerModule::t("User"),
			'contact'=>SellerModule::t("Contact"),
			'qq'=>SellerModule::t("qq"),
			'memo'=>SellerModule::t("Memo"),
			'type_id' => SellerModule::t("Type"),
			'is_send' => SellerModule::t("Issend"),
			'is_pay' => SellerModule::t("Ispay"),
			'created' => SellerModule::t("Created"),
			'modified' => SellerModule::t("Modified"),
		);
	}
	
	public function scopes()
    {
        return array(
            'belong'=>array(
            	'condition'=>'fake.shop_id=:shop_id',
            	'params'=>array(':shop_id'=>Yii::app()->user->tbid),
            ),
            'type'=>array(
            	'condition'=>'fake.type_id=:type_id',
            	'params'=>array(':type_id'=>$this->type_id),
            ),
            'notsafe'=>array(
            	'alias'=>'fake',
            	'select' => 'id, user, contact, qq, memo, type_id, is_send, is_pay, created, modified',
            ),
            'forcheck'=>array(
            	'alias'=>'fake',
            	'select' => 'id',
            ),
        );
    }
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
				$this->shop_id=Yii::app()->user->tbid;
			$this->create_user=Yii::app()->user->id;
			$this->modified=time();
				
			return true;
		}
		else
			return false;
	}

	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'is_send' => array(
				'0' => SellerModule::t('Change address'),
				'1' => SellerModule::t('Empty Bag'),
			),
			'is_pay' => array(
				'0' => SellerModule::t('No Pay'),
				'1' => SellerModule::t('Had Pay'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

    public function formatHtml($model)
    {
    	if(isset($model['user_id'])) $model['user_id'] = Type::Lists('fake', $model['type_id']);
    	if(isset($model['is_send'])) $model['is_send'] = self::itemAlias('is_send', $model['is_send']);
    	if(isset($model['created'])) $model['created'] = date('Y-m-d', $model['created']);
    	if(isset($model['modified'])) $model['modified'] = date('Y-m-d', $model['modified']);

    	return $model;
    }
}