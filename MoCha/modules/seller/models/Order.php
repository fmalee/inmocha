<?php

class Order extends CActiveRecord
{
	/**
	 * Order表的字段:
	 * @var integer $id
	 * @var integer $trade_id
	 * @var integer $item_id
	 * @var string $sku_id
	 * @var integer $title
	 * @var string $item_meal_id
	 * @var integer $seller_type
	 * @var integer $cid
     * @var integer $num
     * @var integer $price
     * @var integer $payment
     * @var integer $adjust_fee
     * @var integer $discount_fee
     * @var integer $total_fee
     * @var integer $pic_path
     * @var integer $snapshot_url
     * @var integer $outer_iid
     * @var integer $outer_sku_id
     * @var integer $sku_properties_name
     * @var integer $buyer_rate
     * @var integer $seller_rate
     * @var integer $is_oversold
     * @var integer $end_time
	 */

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{order}}';
	}

	public function rules()
	{
		return array(
			array('trade_id', 'required'),
			array('trade_id', 'numerical', 'integerOnly'=>true),
			array('id,trade_id,item_id,sku_id,title,item_meal_id,seller_type,cid,num,price,payment,adjust_fee,discount_fee,total_fee,pic_path,snapshot_url,outer_iid,outer_sku_id,sku_properties_name,buyer_rate,seller_rate,is_oversold,end_time', 'safe'),
		);
	}

	public function relations()
	{
        return array(
			'shop' => array(self::BELONGS_TO, 'Shop', 'shop_id'),
			'trade' => array(self::BELONGS_TO, 'Trade', 'trade_id'),
			'refund' => array(self::HAS_MANY, 'Refund', 'order_id'),
			'invoice' => array(self::HAS_MANY, 'InvoiceOrder', 'order_id'),
			'item' => array(self::BELONGS_TO, 'Item', 'item_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => SellerModule::t("Order ID"),
			'seller_nick'=>SellerModule::t("Seller Nick"),
		);
	}
	
	public function scopes()
    {
        return array(
            'belong'=>array(
            	'condition'=>'order.trade_id=:trade_id',
            	'params'=>array(':trade_id'=>$this->trade_id),
            ),
            'forFake'=>array(
            	'alias'=>'order',
                'select' => 'id, trade_id, payment, buyer_rate, seller_rate, receiver_name, created',
            ),
            'notsafe'=>array(
            	'alias'=>'order',
            	'select' => 'id, trade_id, title, item_id, sku_id, num, price, payment, adjust_fee, discount_fee, total_fee, pic_path, outer_iid, outer_sku_id, sku_properties_name, buyer_rate, seller_rate, is_oversold, end_time',
            ),
            'forcheck'=>array(
            	'alias'=>'order',
            	'select' => 'id',
            ),
            'simple'=>array(
            	'alias'=>'order',
            	'select' => 'id, trade_id, title, item_id, num, price, payment, pic_path, outer_sku_id, sku_properties_name',
            ),
        );
    }

	public function trade($trade_id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'Order.trade_id=:trade_id',
			'params'=>array(':trade_id'=>$trade_id),
	    ));
	    return $this;
	}
	
	public static function itemAlias($type,$code=NULL) {
		$_items = array(
			'buyerRate' => array(
				'0' => SellerModule::t('Wait buyer rating'),
				'1' => SellerModule::t('Buyer had rated'),
			),
			'sellerRate' => array(
				'0' => SellerModule::t('Wait seller rating'),
				'1' => SellerModule::t('Seller had rated'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

    public function formatHtml($model) {
    	if(isset($model['buyer_rate'])) $model['buyer_rate'] = self::itemAlias('buyerRate', $model['buyer_rate']);
    	if(isset($model['seller_rate'])) $model['seller_rate'] = self::itemAlias('sellerRate', $model['seller_rate']);
    	
    	if(isset($model['created'])) $model['created'] = date('Y-m-d H:i:s', $model['created']);

    	return $model;
    }
}