<?php

class InvoiceTrade extends CActiveRecord
{	
	/**
	 * InvoiceTrade表的字段:
	 * @var integer $id
	 * @var integer $invoice_id
	 * @var integer $trade_id
	 * @var string $source
	*/

	# 订单来源
	const SOURCE_TRADE = 1;
	const SOURCE_EDIT = 2;
	const SOURCE_NIFFER = 3;
	const SOURCE_REISSUE = 4;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{invoice_trade}}';
	}

	public function rules()
	{
		return array(
			array('invoice_id, trade_id, source', 'required'),
			array('invoice_id, trade_id', 'numerical', 'integerOnly'=>true),
			array('source', 'length', 'max'=>10),
		);
	}

	public function relations()
	{
        return array(
			'invoice' => array(self::BELONGS_TO, 'Invoice', 'invoice_id'),
			'invoiceOrder' => array(self::HAS_MANY, 'InvoiceOrder', '', 'on'=>'invoiceTrade.trade_id=invoiceOrder.trade_id AND invoiceTrade.invoice_id=invoiceOrder.invoice_id'),
			'trade' => array(self::BELONGS_TO, 'Trade', 'trade_id'),
			'order' => array(self::HAS_MANY, 'Order', 'trade_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => SellerModule::t('ID'),
			'invoice_id' => SellerModule::t('Invoice'),
			'trade_id' => SellerModule::t('Trade'),
			'source' => SellerModule::t('Source'),
		);
	}

	public function scopes()
    {
        return array(
            'notsafe'=>array(
            	'alias'=>'invoiceTrade',
            	'select' => 'id, invoice_id, trade_id, source',
            ),
            'forcheck'=>array(
            	'alias'=>'invoiceTrade',
            	'select' => 'id, trade_id',
            ),
            'simple'=>array(
            	'alias'=>'invoiceTrade',
            	'select' => 'id, source',
            ),
        );
    }

	public function belong($id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'invoiceTrade.invoice_id=:invoice_id',
			'params'=>array(':invoice_id'=>$id),
	    ));
	    return $this;
	}

	public function trade($id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'invoiceTrade.trade_id=:trade_id',
			'params'=>array(':trade_id'=>$id),
	    ));
	    return $this;
	}

    /**
	 * 订单状态
	 */
	public function getSourceName()
    {
        return $this->itemAlias('source', $this->source);
    }

    public static function itemAlias($type,$code=NULL) {
		$_items = array(
			'source' => array(
				self::SOURCE_TRADE => '订单',
				self::SOURCE_EDIT => '修改',
				self::SOURCE_NIFFER => '换货',
				self::SOURCE_REISSUE => '补发',
			)
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}
}