<?php
$this->breadcrumbs=array(
    '英摩卡'=>array('/'),
    '卖家中心'=>array('.'),
    '供应中心',
);
$this->menu=array(
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 产品中心',
        'url'=>array('/seller/product'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 供应商',
        'url'=>array('/seller/supplier'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
);
$this->pageTitle='供应中心';
$files = array(
    'datatables/jquery.dataTables.min.js',
    'datatables/extras/ZeroClipboard.js',
    'datatables/extras/TableTools.min.js',
    'datatables/DT_bootstrap.js',
    'responsive-tables.js',
    'DT_bootstrap.css',
    'responsive-tables.css',
    'select2.min.js',
    'select2.css'
);
Ya::register($files);
Ya::registerScript('js','index.js');
?>
                        <!-- content-body -->
                        <div class="content-body">
                            <!-- tab stat -->
                            <div class="box corner-all">
                                <div class="box-header grd-white color-silver-dark corner-top">
                                    <div class="header-control">
                                        <span>供应中心</span>
                                    </div>
                                    <span><button class="iDialog btn" data-uri="<?php echo $this->createUrl('create');?>" data-title="新增供应" data-id="SupplyForm" data-callback="oRefresh" data-ok="添加"><i class="icon-plus"></i> 新增供应</button></span>
                                </div>
                                <div class="box-body">
                                    <table id="supplyList" class="iTablelist table table-hover responsive" data-acturi="<?php echo $this->createUrl('aedit');?>">
                                        <thead>
                                            <tr>
                                                <th><input class="iCheckAll" type="checkbox" name="checkall"></th>
                                                <th>产品</th>
                                                <th>供应商</th>
                                                <th>货号</th>
                                                <th>价格</th>
                                                <th>库存</th>
                                                <th>状态</th>
                                                <th>推荐</th>
                                                <th>备注</th>
                                                <th>修改时间</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="5" class="dataTables_empty">数据正在加载中，请稍候……</td>
                                            </tr>
                                    </tbody>
                                </table>
                                </div>
                            </div><!-- /tab stat -->
                        </div><!--/content-body -->
                    </div><!-- /content -->
                </div><!-- /span content -->
