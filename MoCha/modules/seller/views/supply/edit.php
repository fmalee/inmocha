<?php $form=$this->beginWidget('ActiveForm', array('id'=>'SupplyForm')); ?>
<table class="table table-hover table-bordered">
    <tr>
        <th><?php echo $form->labelEx($model,'product_id',array('class'=>'control-label')); ?></th>
        <td colspan="3">
            <?php echo Product::model()->getNameById($model->product_id);?>
            <?php echo $form->hiddenField($model,'product_id');?>
        </td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'supplier_id',array('class'=>'control-label')); ?></th>
        <td colspan="3">
            <?php echo Supplier::model()->getNameById($model->supplier_id);?>
            <?php echo $form->hiddenField($model,'supplier_id');?>
        </td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textField($model,'name', array('class'=>'input-large')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'price',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'price', array('class'=>'input-small')); ?></td>
        <th><?php echo $form->labelEx($model,'stock',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'stock', array('class'=>'input-small')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'www',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textField($model,'www', array('class'=>'input-large')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'status',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->radioButtonGroup($model,'status',Supplier::itemAlias('status')); ?></td>
        <th><?php echo $form->labelEx($model,'is_elite',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->radioButtonGroup($model,'is_elite',Supplier::itemAlias('is_elite')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'memo',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textArea($model,'memo', array('row'=>'3','class'=>'input-large')); ?></td>
    </tr>
</table>
<?php $this->endWidget(); ?>