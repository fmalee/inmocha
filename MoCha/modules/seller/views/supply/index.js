$(document).ready(function() {
    // uniform
    $('[data-form=uniform]').uniform();
    $('.iTablelist').listTable();
    
    // datatables table tools
    $('#supplyList').dataTable({
        "sAjaxSource": "<?php echo $this->createUrl('list');?>",
        "aaSorting": [[6,'desc'], [8,'desc']],
        "aoColumnDefs": [
            {
                "mData": "id",
                "aTargets": [0],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<input class="iCheck" type="checkbox" name="icheck" value="'+data+'">';
                }
            },
            {
                "mData": "product",
                "aTargets": [1],
            },
            {
                "mData": "supplier",
                "aTargets": [2],
            },
            {
                "mData": "name",
                "aTargets": [3],
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="name" data-tdtype="edit">'+data+'</span><a href="'+full.www+'" target="_blank">网店</a>';
                }
            },
            {
                "mData": "price",
                "aTargets": [4],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="phone" data-tdtype="edit">'+data+'</span>';
                }
            },
            {
                "mData": "stock",
                "aTargets": [5],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="tag" data-tdtype="edit">'+data+'</span>';
                }
            },
            {
                "mData": "status",
                "aTargets": [6],
                "mRender": function(data, type, full) {
                    var img='';
                    if(data == 0){
                        img = 'disabled';
                    }else{
                        img = 'enabled';
                    }
                    return '<img src="/asset/img/toggle_'+img+'.gif" data-value="'+data+'" data-field="status" data-id="'+full.id+'" data-tdtype="toggle">';
                }
            },
            {
                "mData": "is_elite",
                "aTargets": [7],
                "mRender": function(data, type, full) {
                    var img='';
                    if(data == 0){
                        img = 'disabled';
                    }else{
                        img = 'enabled';
                    }
                    return '<img src="/asset/img/toggle_'+img+'.gif" data-value="'+data+'" data-field="is_elite" data-id="'+full.id+'" data-tdtype="toggle">';
                }
            },
            {
                "mData": "memo",
                "aTargets": [8],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="memo" data-tdtype="edit">'+data+'</span>';
                }
            },
            {
                "mData": "modified",
                "aTargets": [9],
            },
            {
                "mData": "id",
                "aTargets": [10],
                "mRender": function(data, type, full) {
                    var html ='<a href="javascript:;" class="iDialog btn btn-mini btn-danger" data-uri="<?php echo $this->createUrl("edit");?>/id/'+data+'" data-title="编辑 '+full.name+' " data-id="SupplyForm" data-callback="oRefresh" data-ok="更新">编辑</a>';
                    html +='<a href="javascript:;" class="iConfirm btn btn-mini" data-msg="确定要删除 '+full.name+' 吗？" data-uri="<?php echo $this->createUrl('delete');?>/id/'+data+'" data-ok="删除" data-acttype="ajax" data-callback="oRefresh">删除</a>';
                    return '<div class="btn-group pull-right">'+html+'</div>';
                }
            },
            
        ],
    });
});

/*
 *刷新
 */
function oRefresh(){
    $('#supplyList').dataTable().fnDraw();
}
            
/*
 *快速筛选
 */
function filtering(type) {
    var oTable = $('#supplyList').dataTable();
    if(type === '') {
        oTable.fnFilter('', 3);
    } else {
        oTable.fnFilter(type, 3);
    }
}