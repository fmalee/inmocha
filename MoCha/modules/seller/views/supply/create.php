<?php $form=$this->beginWidget('ActiveForm', array('id'=>'SupplyForm')); ?>
<table class="table table-hover table-bordered">
    <?php if($_GET['product_id']):?>
    <?php echo $form->hiddenField($model,'product_id', array('value'=>$_GET['product_id']));?>
    <?php else:?>
    <tr>
        <th><?php echo $form->labelEx($model,'product_id',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->hiddenField($model,'product_id', array('class'=>'bigdrop iProduct', 'value'=>''));?></td>
    </tr>
    <?php endif;?>
    <tr>
        <th><?php echo $form->labelEx($model,'supplier_id',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->hiddenField($model,'supplier_id', array('class'=>'bigdrop iSupplier', 'value'=>''));?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textField($model,'name', array('class'=>'input-large')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'price',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'price', array('class'=>'input-small')); ?></td>
        <th><?php echo $form->labelEx($model,'stock',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'stock', array('class'=>'input-small')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'www',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textField($model,'www', array('class'=>'input-large')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'status', array('class'=>'control-label'));?></th>
        <td><?php echo $form->radioButtonGroup($model,'status',Supplier::itemAlias('status'), array('value'=>1));?></td>
        <th><?php echo $form->labelEx($model,'is_elite',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->radioButtonGroup($model,'is_elite',Supplier::itemAlias('is_elite')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'memo',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textArea($model,'memo', array('row'=>'3','class'=>'input-large')); ?></td>
    </tr>
</table>
<?php $this->endWidget(); ?>
<script>
$(document).ready(function() {

    $(".iProduct").select2({
        placeholder: '<?php echo Ya::t("Search for a product");?>',
        minimumInputLength: 3,
        ajax: {
            url: "<?php echo $this->createUrl('/seller/Product/choose');?>",
            dataType: 'json',
            quietMillis: 100,
            data: function (term, page) { // page is the one-based page number tracked by Select2
                return {name: term };
            },
            results: function (data, page) {
                return {results: data.msg};
            }
        },
        formatResult: format, // omitted for brevity, see the source of this page
        formatSelection: format, // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });
    $(".iSupplier").select2({
        placeholder: '<?php echo Ya::t("Search for a supplier");?>',
        minimumInputLength: 3,
        ajax: {
            url: "<?php echo $this->createUrl('/seller/supplier/choose');?>",
            dataType: 'json',
            quietMillis: 100,
            data: function (term, page) { // page is the one-based page number tracked by Select2
                return {name: term };
            },
            results: function (data, page) {
                return {results: data.msg};
            }
        },
        formatResult: format, // omitted for brevity, see the source of this page
        formatSelection: format, // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });
    
    function format(item) { return item.name; };

});

</script>