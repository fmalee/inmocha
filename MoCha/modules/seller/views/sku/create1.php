<?php $form=$this->beginWidget('ActiveForm', array('id'=>'SkuForm')); ?>
<table class="table table-hover table-bordered table-condensed">
    <tr>
        <th><?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'name', array('class'=>'input-medium')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'sku_1',array('class'=>'control-label','label'=>$product->sku_1));?></th>
        <td><?php echo $form->textField($model,'sku_1', array('class'=>'input-small')); ?></td>
    </tr>
    <?php if($product->sku_2):?>
    <tr>
        <th><?php echo $form->labelEx($model, 'sku_2', array('class'=>'control-label','label'=>$product->sku_2));?></th>
        <td><?php echo $form->textField($model, 'sku_2', array('class'=>'input-small')); ?></td>
    </tr>
    <?php endif;?>
    <tr>
        <th><?php echo $form->labelEx($model,'selling_price',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'selling_price', array('class'=>'input-small')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'supply_id',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->DropDownList($model,'supply_id',$supplies, array('class'=>'input-medium')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'location',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'location', array('class'=>'input-medium')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'status',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->radioButtonGroup($model,'status',Sku::itemAlias('status')); ?></td>
    </tr>
</table>
<?php echo $form->hiddenField($model,'product_id');?>
<?php $this->endWidget();?>