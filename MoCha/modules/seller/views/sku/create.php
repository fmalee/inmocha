<?php $form=$this->beginWidget('ActiveForm', array('id'=>'SkuForm')); ?>
<table class="table table-hover table-bordered table-condensed">
    <thead>
        <tr>
            <th><?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?></th>
            <th><?php echo $form->labelEx($model,'sku_1',array('class'=>'control-label','label'=>$product->sku_1));?></th>
            <th><?php echo $form->labelEx($model, 'sku_2', array('class'=>'control-label','label'=>$product->sku_2));?></th>
            <th><?php echo $form->labelEx($model,'selling_price',array('class'=>'control-label')); ?></th>
            <th><?php echo $form->labelEx($model,'supply_id',array('class'=>'control-label')); ?></th>
            <th><?php echo $form->labelEx($model,'location',array('class'=>'control-label')); ?></th>
            <th>删除</th>
        </tr>
    </thead>
    <tbody  id="rec_hidden" class="hidden">
        <tr id='rec_@a'>
            <td><?php echo Chtml::textField('Skus[sku][@a][name]', '', array('class'=>'input-small')); ?></td>
            <td><?php echo Chtml::textField('Skus[sku][@a][sku_1]', '', array('class'=>'input-small')); ?></td>
            <td><?php echo Chtml::textField('Skus[sku][@a][sku_2]', '', array('class'=>'input-small')); ?></td>
            <td><?php echo Chtml::textField('Skus[sku][@a][selling_price]', $product->selling_price, array('class'=>'input-small')); ?></td>
            <td><?php echo Chtml::DropDownList('Skus[sku][@a][supply_id]', $product->supply_id, $supplies, array('class'=>'input-small')); ?></td>
            <td><?php echo Chtml::textField('Skus[sku][@a][location]', '', array('class'=>'input-small')); ?></td>
            <td><a title="关闭" class="icon-remove" href="javascript:;" onclick="recordaction.del('rec_@a')"></a></td>
        </tr>
    </tbody>
    <tbody id='rec'>
        <tr id='rec_1'>
            <td><?php echo Chtml::textField('Skus[sku][1][name]', '', array('class'=>'input-small')); ?></td>
            <td><?php echo Chtml::textField('Skus[sku][1][sku_1]', '', array('class'=>'input-small')); ?></td>
            <td><?php echo Chtml::textField('Skus[sku][1][sku_2]', '', array('class'=>'input-small')); ?></td>
            <td><?php echo Chtml::textField('Skus[sku][1][selling_price]', $product->selling_price, array('class'=>'input-small')); ?></td>
            <td><?php echo Chtml::DropDownList('Skus[sku][1][supply_id]', $product->supply_id, $supplies, array('class'=>'input-small')); ?></td>
            <td><?php echo Chtml::textField('Skus[sku][1][location]', '', array('class'=>'input-small')); ?></td>
            <td><a title="关闭" class="icon-remove" href="javascript:;" onclick="recordaction.del('rec_1')"></a></td>
        </tr>
    </tbody>
    <tbody><tr><td colspan="7"><span id="insert1_span"><a href="javascript:;" id="insert1" onClick="recordaction.insert();" class="btn btn-primary"><span>增加一条</span></a></span></td></tr></tbody>
</table>

<?php echo Chtml::hiddenField('Skus[product_id]',$product->id);?>
<?php echo Chtml::hiddenField('Skus[status]',1);?>
<?php $this->endWidget();?>
<script type="text/javascript">
    function _id(id) {
        return !id ? null : document.getElementById(id);
    }
    var recordaction = {
        index:0,
        l:$('#rec').children().length,
        //插入一行并将索引I清零（避免check函数失效)
        insert:function(){
            this.l++;
            var node = _id('rec_hidden').rows[0].innerHTML;
            var s = node.replace(/\@a/g,this.l);
            var domtr ="<tr id=rec_"+this.l+">"+s+"</tr>"; 
            $('#rec').append(domtr);
        },
        //删除dom并重置trID
        del:function(D){
            $('#'+D).remove();
        }
    }

</script>