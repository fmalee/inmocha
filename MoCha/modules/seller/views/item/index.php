<?php
$this->breadcrumbs=array(
    '英摩卡'=>array('..'),
    '卖家中心'=>array('.'),
    '出售中的宝贝',
);
$this->menu=array(
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-plus"></i> 新增配货',
        'linkOptions'=>array(
            'class'=>'iDialog btn btn-small btn-link',
            'data-uri'=>$this->createUrl("create"),
            'data-title'=>'新增配货',
            'data-id'=>'InvoiceForm',
            'data-ok'=>'创建',
            'data-callback'=>'oRefresh',
        )
    ),
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 分类管理',
        'url'=>array('type'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
);
$this->pageTitle='出售中的宝贝';
Ya::registerScript('js','index.js');
?>
                        <!-- content-body -->
                        <div class="content-body">
                            <!-- tables -->
                            <div class="box corner-all">
                                <div class="box-header grd-white color-silver-dark corner-top">
                                    <div class="header-control">
                                        <span><button class="iDialog btn btn-mini" data-uri="<?php echo $this->createUrl('create', array('product_id'=>$model->id));?>" data-title="新增配货" data-id="TypeForm" data-ok="添加"><i class="icon-plus"></i> 新增配货</button></span>
                                    </div>
                                    <span>
                                        <span>出售中宝贝</span>
                                    </span>
                                </div>
                                <div class="box-body">
                                    <table id="ItemList" class="iTablelist table table-striped responsive" data-acturi="<?php echo $this->createUrl("aedit");?>">
                                        <thead>
                                            <tr class="success">
                                                <th>选择</th>
                                                <th>推荐</th>
                                                <th>宝贝图片</th>
                                                <th>宝贝名称</th>
                                                <th>商家编码</th>
                                                <th>主类目</th>
                                                <th>价格</th>
                                                <th>销量</th>
                                                <th>库存</th>
                                                <th>上架/下架时间</th>
                                                <th>创建/编辑时间</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="info">
                                                <td colspan="11">
                                                    <input class="iCheckAll" type="checkbox" name="checkall">
                                                    <div class="btn-group">
                                                        <button class="btn btn-mini btn-primary" data-msg="您确定要执行删除吗？" data-ok="删除" data-name="id" data-path="delete" data-callbak="oRefresh" data-acttype="ajax" data-tdtype="batch"><i class="icon-remove"></i> 删除</button>
                                                        <button class="btn btn-mini" data-name="id" data-uri="<?php echo $this->createUrl('/seller/product/importTaobao');?>" data-callbak="oRefresh" data-acttype="ajax" data-tdtype="batch" data-title="导入到产品" data-id="ImportForm" data-ok="导入"><i class="icon-remove"></i> 导入到产品</button>
                                                        <button data-tdtype="batch" class="iDialog btn btn-mini" data-uri="<?php echo $this->createUrl('/seller/product/importTaobao', array('id'=>$data->id));?>" data-title="导入到产品" data-id="ImportForm" data-ok="导入"><i class="icon-plus"></i> 导入到产品</button>
                                                        <button href="javascript:void(0)" class="btn btn-mini" target="_blank">下架</button>
                                                        <button href="javascript:void(0)" class="btn btn-mini" target="_blank">推荐</button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                            <?php $this->widget('YListView', array(
                                                'dataProvider'=>$dataProvider,
                                                'id'=>'invoice',
                                                'itemView'=>'_list',
                                                'itemsTagName'=>'',
                                                'tagName'=>'tbody',
                                                'loadingCssClass'=>'',
                                                'summaryCssClass'=>'pagination pull-left',
                                                'pagerCssClass'=>'pagination pagination-right',
                                                'template'=>"{items}\n<tr><td colspan=\"11\">{summary}{pager}</td></tr>",
                                            )); ?>
                                     </table>
                                </div>
                            </div><!-- /tab stat -->
                            
                            <!--/dashboar-->
                        </div><!--/content-body -->
                    </div><!-- /content -->
                </div><!-- /span content -->
