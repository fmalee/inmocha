<?php $form=$this->beginWidget('ActiveForm', array('id'=>'InvoiceForm')); ?>
<table class="table table-hover table-bordered">
    <tr>
        <th><?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'name', array('class'=>'input-medium')); ?></td>
    </tr>
    <tr>
        <th>自动导入</th>
        <td>
            <?php echo Yhtml::checkBoxButton('Invoice[import][]','trade,niffer,reissue', array("trade"=>"订单记录","niffer"=>"换货记录","reissue"=>"补发记录")); ?>
            <br /><small>"自动导入"功能会自动导入相应选项中的待发货订单</small>
        </td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'memo',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textArea($model,'memo', array('row'=>'3','class'=>'input-large')); ?></td>
    </tr>
</table>
<?php $this->endWidget(); ?>