<?php
$this->breadcrumbs=array(
    '英摩卡'=>array('..'),
    '卖家中心'=>array('..'),
    '配货记录'=>array('.'),
    $model->name
);
$this->menu=array(
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-plus"></i> 新增配货',
        'linkOptions'=>array(
            'class'=>'iDialog btn btn-small btn-link',
            'data-uri'=>$this->createUrl("create"),
            'data-title'=>'新增配货',
            'data-id'=>'InvoiceForm',
            'data-ok'=>'创建',
            'data-callback'=>'oRefresh',
        )
    ),
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 分类管理',
        'url'=>array('type'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
);
$this->pageTitle= $model->name;
$files = array(
    'select2.min.js',
    'select2.css'
);
Ya::register($files);
Ya::registerScript('js','list.js');
?>
                        <!-- content-body -->
                        <div class="content-body">
                            <!-- tables -->
                            <div class="box corner-all">
                                <div class="box-header grd-white color-silver-dark corner-top">
                                    <div class="header-control">
                                        <span><button class="iDialog btn btn-mini" data-uri="<?php echo $this->createUrl('create');?>" data-title="新增配货" data-id="InvoiceForm" data-ok="添加"><i class="icon-plus"></i> 新增配货</button></span>
                                    </div>
                                    <span>
                                        <span><?php echo $model->name;?></span>
                                    </span>
                                </div>
                                <div class="box-body">
                                     <table id="invoiceList" class="iTablelist table table-striped responsive" data-acturi="<?php echo $this->createUrl("aedit");?>">
                                        <thead>
                                            <tr class="success">
                                                <th>选择</th>
                                                <th>订单编号</th>
                                                <th>订单状态</th>
                                                <th>买家旺旺</th>
                                                <th>订单内容</th>
                                                <th>备注</th>
                                                <th>来源</th>
                                                <th>付款时间</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="info">
                                                <td colspan="8">
                                                    <input class="iCheckAll" type="checkbox" name="checkall" <?php if($model->archive):?>disabled<?php endif;?>>
                                                    <div class="btn-group">
                                                    <?php if(!$model->archive):?>
                                                        <button class="btn btn-mini btn-primary" data-msg="您确定要执行删除吗？" data-ok="删除" data-name="trade" data-uri="<?php echo $this->createUrl('trade', array('id'=>$model->id));?>" data-callbak="oRefresh" data-acttype="ajax" data-tdtype="batch"><i class="icon-remove"></i> 删除</button>
                                                    <?php endif;?>
                                                        <button class="btn btn-mini" data-name="id" data-uri="<?php echo $this->createUrl('/seller/product/importTaobao');?>" data-callbak="oRefresh" data-acttype="ajax" data-tdtype="batch" data-title="导入到产品" data-id="ImportForm" data-ok="导入"><i class="icon-remove"></i> 导入到产品</button>
                                                        <button data-tdtype="batch" class="iDialog btn btn-mini" data-uri="<?php echo $this->createUrl('/seller/product/importTaobao', array('id'=>$data->id));?>" data-title="导入到产品" data-id="ImportForm" data-ok="导入"><i class="icon-plus"></i> 导入到产品</button>
                                                        <button href="javascript:void(0)" class="btn btn-mini" target="_blank">下架</button>
                                                        <button href="javascript:void(0)" class="btn btn-mini" target="_blank">推荐</button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                            <?php $this->widget('YListView', array(
                                                'dataProvider'=>$dataProvider,
                                                'viewData'=>array('model'=>$model),
                                                'id'=>'invoice',
                                                'itemView'=>'list_view',
                                                'itemsTagName'=>'',
                                                'tagName'=>'tbody',
                                                'loadingCssClass'=>'',
                                                'summaryCssClass'=>'pagination pull-left',
                                                'pagerCssClass'=>'pagination pagination-right',
                                                'template'=>"{items}\n<tr><td colspan=\"8\">{summary}{pager}</td></tr>",
                                            )); ?>
                                     </table>
                                </div>
                            </div><!-- /tab stat -->
                            
                            <!--/dashboar-->
                        </div><!--/content-body -->
                    </div><!-- /content -->
                </div><!-- /span content -->
