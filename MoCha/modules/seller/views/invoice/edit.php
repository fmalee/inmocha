<?php $form=$this->beginWidget('ActiveForm', array('id'=>'SkuForm')); ?>
<table class="table table-hover table-bordered">
    <thead>
        <tr>
            <th colspan="4" style="text-align: center;">订单信息</th>
        </tr>
        <tr>
            <th>产品</th>
            <th>属性</th>
            <th>数量</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($trade->order as $order):?>
        <tr>
            <td><?php echo $order->outer_sku_id;?></td>
            <td><?php echo $order->sku_properties_name;?></td>
            <td><span class="badge"><?php echo $order->num;?></span></td>
        </tr>
    <?php endforeach;?>
    </tbody>
</table>
<table class="table table-hover table-bordered">
    <thead>
        <tr>
            <th colspan="4" style="text-align: center;">配货信息</th>
        </tr>
        <tr>
            <th>产品</th>
            <th>数量</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody id="rec">
    <?php $i=1;?>
    <?php foreach($model->invoiceOrder as $order):?>
        <tr id="rec_id" class="relationship">
            <td><input class="input-large bigdrop iProduct" type="hidden" value="<?php echo $order->sku_id;?>" name="Sku[]" id="product_id_<?php echo $i;?>"></td>
            <td><?php echo Chtml::textField('Quantity[]', $order->quantity, array('class'=>'input-mini')); ?></td>
            <td><span class="badge"><a title="删除" class="icon-remove" href="javascript:;" onclick="del(this)"></a></span></td>
        </tr>
    <?php endforeach;?>
        <tr>
            <td colspan="7"><span id="insert1_span"><a href="javascript:;" id="insert1" onClick="insert();" class="btn btn-primary"><span>增加一条</span></a></span></td>
        </tr>
    </tbody>
</table>
<?php $this->endWidget(); ?>
<script type="text/javascript">
var setting = {
    placeholder: '请输入产品型号',
    minimumInputLength: 3,
    ajax: {
        url: "<?php echo $this->createUrl('/seller/Sku/choose');?>",
        dataType: 'json',
        quietMillis: 100,
        data: function (term, page) { // page is the one-based page number tracked by Select2
            return {name: term };
        },
        results: function (data, page) {
            return {results: data.msg};
        }
    },
    initSelection: function(element, callback) {
        var id=$(element).val();
        if (id!=="") {
            $.ajax("<?php echo $this->createUrl('/seller/Sku/chooseid');?>", {
                data: {
                    id: id
                },
                dataType: "json"
            }).done(function(data) { callback(data.msg); });
        }
    },
    formatResult: format, // omitted for brevity, see the source of this page
    formatSelection: format, // omitted for brevity, see the source of this page
    dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
    escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
};
$(".iProduct").select2(setting);

function format(item) { return item.name; };

function insert() {
    l = $('#rec').children().length;
    l++;
    var $relationship = $('.relationship');
    var $clone = $("#rec_id").clone();
    var id = 'id_' + l;
    $clone.find(".select2-container").remove();
    $clone.find(".bigdrop").attr("id", id);
    $clone.find(".bigdrop").removeClass("iProduct");
    $clone.show();
    $relationship.eq(-1).after($clone);
    $("#"+id).select2(setting);
}
function del(D) {
    $(D).parent("span").parent("td").parent("tr").remove();
}
</script>