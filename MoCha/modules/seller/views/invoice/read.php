<?php if(!$item):?>
没有需要的信息。
<?php else:?>
<mc:tpl file="Public/windows_header" />
<script language="javascript" type="text/javascript" src="static/js/jquery.masonry.min.js"></script>
<style type="text/css">
.pad_10 {
    width: 800px;
    margin: 10px;
    padding: 10px
}

.desc {
    margin: 10px 0px 10px 20px;
}

.desc td {
    height: 20px;
}

.shops {
    padding: 5px;
}

.shop {
    margin: 5px;
    padding: 5px;
    float: left;
    border: 1px solid #000;
    width: 215px;
}

.item {
    margin: 3px 0px 5px 0px;
}

.skus {
    margin: 2px 0px 0px 8px;
}

.sku {
    margin-bottom: 2px;
}

.prop,.num {
    margin-right: 10px;
}

.title,.model,.num {
    font-weight: bold;
}
</style>
<div class="pad_10">
    <table class="desc">
        <caption> <strong><?php echo $model->name;?></strong> </caption>
        <tr>
            <td>总量:</td>
            <td width="50"><strong><?php echo $item['quantity'];?></strong> 件</td>
            <td>总价:</td>
            <td width="80"><strong><?php echo $item['price'];?></strong> 元</td>
            <td>快递:</td>
            <td width="50">&nbsp;</td>
            <td>配货:</td>
            <td width="50">&nbsp;</td>
            <td>其他:</td>
            <td width="50">&nbsp;</td>
            <td>实付:</td>
            <td width="50">&nbsp;</td>
            <td>打印:</td>
            <td colspan="3"><?php echo date('Y-m-d H:i:s');?></td>
        </tr>
    </table>

    <div class="shops" id="shops">
    <?php foreach($item['suppliers'] as $supplier => $labels):?>
        <div class="shop">
            <span class="title"><?php echo $supplier;?></span>-<?php echo $labels['price'];?> - <?php echo $labels['quantity'];?>
            <ul class="items">
            <?php foreach($labels['label'] as $label => $skus):?>
                <li class="item"><span class="model"><?php echo $label;?></span>-<span class="discount"><?php echo $skus['price'];?></span>-<span class="outer_id"><?php echo $skus['quantity'];?></span>
                    <ul class="skus">
                    <?php foreach($skus['sku'] as $sku):?>
                        <li class="sku"><span class="prop"><?php echo $sku['property'];?></span><span class="num">* <?php echo $sku['quantity'];?> - <?php echo $sku['price'];?></span></li>
                    <?php endforeach;?>
                    </ul>
                </li>
            <?php endforeach;?>
            </ul>
        </div>
    <?php endforeach;?>
    </div>
</div>
<script type="text/javascript">
$(function(){
  $('#shops').masonry({
    // options
    itemSelector : '.shop',
    columnWidth : 250
  });
});
</script>
<? endif;?>
</body>
</html>