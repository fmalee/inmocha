<?php
$this->breadcrumbs=array(
    '英摩卡'=>array('..'),
    '卖家中心'=>array('.'),
    '配货记录',
);
$this->menu=array(
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-plus"></i> 新增配货',
        'linkOptions'=>array(
            'class'=>'iDialog btn btn-small btn-link',
            'data-uri'=>$this->createUrl("create"),
            'data-title'=>'新增配货',
            'data-id'=>'InvoiceForm',
            'data-ok'=>'创建',
            'data-callback'=>'oRefresh',
        )
    ),
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 分类管理',
        'url'=>array('type'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
);
$this->pageTitle='配货记录';
Ya::registerScript('js','index.js');
?>
                        <!-- content-body -->
                        <div class="content-body">
                            <!-- tables -->
                            <div class="box corner-all">
                                <div class="box-header grd-white color-silver-dark corner-top">
                                    <div class="header-control">
                                        <span><button class="iDialog btn btn-mini" data-uri="<?php echo $this->createUrl('create');?>" data-title="新增配货" data-id="InvoiceForm" data-ok="添加"><i class="icon-plus"></i> 新增配货</button></span>
                                    </div>
                                    <span>
                                        <span>配货记录</span>
                                    </span>
                                </div>
                                <div class="box-body">
                                     <table id="invoiceList" class="iTablelist table table-hover responsive" data-acturi="<?php echo $this->createUrl("aedit");?>">
                                        <thead>
                                            <tr class="success">
                                                <th>名称</th>
                                                <th>订单</th>
                                                <th>数量</th>
                                                <th>成本</th>
                                                <th>说明</th>
                                                <th>创建时间</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                            <?php $this->widget('YListView', array(
                                                'dataProvider'=>$dataProvider,
                                                'id'=>'invoice',
                                                'itemView'=>'_list',
                                                'itemsTagName'=>'',
                                                'tagName'=>'tbody',
                                                'loadingCssClass'=>'',
                                                'summaryCssClass'=>'pagination pull-left',
                                                'pagerCssClass'=>'pagination pagination-right',
                                               'template'=>"{items}\n<tr><td colspan=\"7\">{summary}{pager}</td></tr>",
                                            )); ?>
                                     </table>
                                </div>
                            </div><!-- /tab stat -->
                            
                            <!--/dashboar-->
                        </div><!--/content-body -->
                    </div><!-- /content -->
                </div><!-- /span content -->
