
                                            <tr id="trade_<?php echo $data->trade_id;?>">
                                                <td><input class="iCheck" type="checkbox" name="icheck" <?php if($model->archive):?> disabled<?php endif;?> value="<?php echo $data->trade_id;?>"></td>
                                                <td><?php echo $data->trade_id;?></td>
                                                <td><?php echo $data->trade->statusName;?></td>
                                                <td><?php echo $data->trade->buyer_nick;?></td>
                                                <td>
                                                <?php foreach ($data->invoiceOrder as $order):?>
                                                    <p>
                                                        <span class="label label-inverse"><?php echo $order->product;?></span> <span class="label"><?php echo $order->sku;?></span><span class="badge"> <?php echo $order->quantity;?></span>
                                                    </p>
                                                <?php endforeach;?>
                                                </td>
                                                <td>
                                                    <p><?php echo $data->trade->buyer_message;?></p>
                                                    <p><?php echo $data->trade->seller_memo;?></p>
                                                </td>
                                                <td><?php echo $data->sourceName;?></td>
                                                <td>
                                                <?php if(!$model->archive):?>
                                                    <span class="iShow"><button class="btn btn-mini"><?php echo date('Y-m-d H:i:s',$data->trade->created);?></button></span>
                                                    <div class="btn-group iHidden" style="display: none;">
                                                        <button class="iDialog btn btn-mini btn-danger" data-uri="<?php echo $this->createUrl('edit', array('id'=>$model->id,'trade'=>$data->trade_id));?>" data-title="编辑 <?php echo $supply->name;?>" data-id="SkuForm" data-callback="oRefresh" data-ok="更新">编辑</button>
                                                        <button class="iConfirm btn btn-mini" data-msg="确定要删除 <?php echo $data->trade_id;?> 吗？" data-uri="<?php echo $this->createUrl('trade',array('id'=>$model->id,'trade'=>$data->trade_id));?>" data-ok="删除" data-acttype="ajax" data-callback="oDelete('<?php echo $data->id;?>');">删除</button>                                                    
                                                    </div>
                                                <?php else:?>
                                                    <?php echo date('Y-m-d H:i:s',$data->trade->created);?>
                                                <?php endif;?>
                                                </td>
                                            </tr>
