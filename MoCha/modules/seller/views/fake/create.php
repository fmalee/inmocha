<?php $form=$this->beginWidget('ActiveForm', array('id'=>'FakeForm')); ?>
<table class="table table-hover table-bordered">
    <tr>
        <th><?php echo $form->labelEx($model,'type_id',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->radioButtonGroup($model,'type_id',Type::Lists('fake')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'is_send',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->radioButtonGroup($model,'is_send',Fake::itemAlias('is_send'), array('value'=>'1')); ?></td>
        <th><?php echo $form->labelEx($model,'is_pay',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->radioButtonGroup($model,'is_pay',Fake::itemAlias('is_pay'), array('value'=>'1')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'contact',array('class'=>'control-label')); ?></th>
        <td>
        <?php echo $form->textField($model,'contact', array('class'=>'input-small','value'=>$trade->receiver_name)); ?>
        </td>
        <th><?php echo $form->labelEx($model,'qq',array('class'=>'control-label')); ?></th>
        <td>
        <?php echo $form->textField($model,'qq', array('class'=>'input-small')); ?>
        </td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'user',array('class'=>'control-label')); ?></th>
        <td>
        <?php echo $form->textField($model,'user', array('class'=>'input-small')); ?>
        </td>
        <th><?php echo $form->labelEx($model,'memo',array('class'=>'control-label')); ?></th>
        <td>
            <?php echo $form->textArea($model,'memo', array('row'=>'3','class'=>'input-small')); ?>
            <?php echo $form->hiddenField($model,'id', array('value'=>$trade->id));?>
            <?php echo $form->hiddenField($model,'created', array('value'=>$trade->created));?>
        </td>
    </tr>
</table>
<?php $this->endWidget(); ?>