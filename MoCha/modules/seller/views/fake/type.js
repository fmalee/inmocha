$(document).ready(function() {
    // uniform
    $('[data-form=uniform]').uniform();
    $('.iTablelist').listTable();
    
    // datatables table tools
    $('#typeList').dataTable({
        "sAjaxSource": "<?php echo $this->createUrl('/type/single', array('module'=>'fake'));?>",
        "aaSorting": [[5,'asc'], [7,'desc']],
        "aoColumnDefs": [
            {
                "mData": "id",
                "aTargets": [0],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<input class="iCheck" type="checkbox" name="icheck" value="'+data+'">';
                }
            },
            {
                "mData": "id",
                "aTargets": [1],
                "bSortable": false,
            },
            {
                "mData": "module",
                "aTargets": [2],
                "bVisible": false,
            },
            {
                "mData": "name",
                "aTargets": [3],
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="name" data-tdtype="edit">'+data+'</span>';
                }
            },
            {
                "mData": "status",
                "aTargets": [4],
                "mRender": function(data, type, full) {
                    var img='';
                    if(data == 0){
                        img = 'disabled';
                    }else{
                        img = 'enabled';
                    }
                    return '<img src="/asset/img/toggle_'+img+'.gif" data-value="'+data+'" data-field="status" data-id="'+full.id+'" data-tdtype="toggle">';
                }
            },
            {
                "mData": "list_order",
                "aTargets": [5],
                "bSortable": true,
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="listorder" data-tdtype="edit">'+data+'</span>';
                }
            },
            {
                "mData": "memo",
                "bSortable": false,
                "aTargets": [6],
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="memo" data-tdtype="edit">'+data+'</span>';
                }
            },
            {
                "mData": "modified",
                "aTargets": [7],
            },
            {
                "mData": "id",
                "aTargets": [8],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    var html='<a href="javascript:;" class="iDialog btn btn-mini btn-danger" data-uri="<?php echo $this->createUrl("/type/edit");?>/module/fake/id/'+data+'" data-title="编辑 '+full.name+' " data-id="TypeForm" data-callback="oRefresh" data-ok="更新">编辑</a>';
                    html +='<a href="javascript:;" class="iConfirm btn btn-mini" data-msg="确定要删除 '+full.name+' 吗？" data-uri="<?php echo $this->createUrl('/type/delete');?>/id/'+data+'" data-ok="删除" data-acttype="ajax" data-callback="oRefresh">删除</a>';
                    return '<div class="btn-group pull-right">'+html+'</div>';
                }
            }
        ],
        "fnInitComplete": function(oSettings, json) {
            var html = '<div class="btn-group">';
            html += '<button class="btn btn-primary" data-msg="您确定要执行删除吗？" data-ok="删除" data-name="id" data-uri="<?php echo $this->createUrl("/type/delete");?>" data-callbak="oRefresh" data-acttype="ajax" data-tdtype="batch"><i class="icon-remove"></i> 删除</button>';
            html += '<button class="btn" onclick="javascript:oRefresh();"><i class="icon-refresh"></i> 刷新</button>';
            html += '</div>';
            $("div.toolbar").html(html);
        }
    });
});

/*
 *刷新表格
 */
function oRefresh(){
    $('#typeList').dataTable().fnDraw();
}