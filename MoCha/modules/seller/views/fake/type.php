<?php
$this->pageTitle='分类管理';
$this->breadcrumbs=array(
    '英摩卡'=>array('/shop'),
    '卖家中心'=>array('./'),
    '刷单管理'=>array('.'),
    '分类管理',
);
$this->menu=array(
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 刷单管理',
        'url'=>array('.'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
);
$files = array(
	'datatables/jquery.dataTables.min.js',
	'datatables/extras/ZeroClipboard.js',
	'datatables/extras/TableTools.min.js',
	'datatables/DT_bootstrap.js',
	'responsive-tables.js',
	'DT_bootstrap.css',
    'responsive-tables.css'
);
Ya::register($files);
Ya::registerScript('js','type.js');
?>
                        <!-- content-body -->
                        <div class="content-body">
                            <!-- tab stat -->
                            <div class="box corner-all">
                                <div class="box-header grd-white color-silver-dark corner-top">
                                    <div class="header-control">
                                        <span>分类管理</span>
                                    </div>
                                    <span><button class="iDialog btn" data-uri="<?php echo $this->createUrl('/type/create', array('module'=>'fake'));?>" data-title="新增刷单分类" data-id="TypeForm" data-callback="oRefresh" data-ok="添加"><i class="icon-plus"></i> 新增分类</button></span>
                                </div>
                                <div class="box-body">
                                    <table id="typeList" class="iTablelist table table-hover responsive" data-acturi="<?php echo $this->createUrl('/type/aedit', array('module'=>'fake'));?>">
                                        <thead>
                                            <tr>
                                                <th><input class="iCheckAll" type="checkbox" name="checkall"></th>
                                                <th>分类ID</th>
                                                <th>分类模块</th>
                                                <th>分类名称</th>
                                                <th>状态</th>
                                                <th>排序</th>
                                                <th>备注</th>
                                                <th>修改时间</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="5" class="dataTables_empty">数据正在加载中，请稍候……</td>
                                            </tr>
                                    </tbody>
                                </table>
                                </div>
                            </div><!-- /tab stat -->
                        </div><!--/content-body -->
                    </div><!-- /content -->
                </div><!-- /span content -->
