<?php $form=$this->beginWidget('ActiveForm', array('id'=>'SupplierForm')); ?>
<table class="table table-hover table-bordered">
    <tr>
        <th><?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textField($model,'name', array('class'=>'input-medium')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'type_id',array('class'=>'control-label')); ?></th>
        <td colspan="3">
            <?php echo $form->dropDownList($model,'type_id', array(), array(
                'class'=>'input-small iSelect',
                'data-uri'=>$this->createUrl('/type/childs',array('module'=>'supplier')),
                'data-pid'=>'0',
            )); ?>
            <?php echo $form->hiddenField($model,'type_id', array('id'=>'iCatid'));?>
        </td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'tag',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textField($model,'tag', array('class'=>'input-medium')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'status',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->radioButtonGroup($model,'status',Supplier::itemAlias('status'), array('value'=>'1')); ?></td>
        <th><?php echo $form->labelEx($model,'is_elite',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->radioButtonGroup($model,'is_elite',Supplier::itemAlias('is_elite'), array('value'=>'0')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'contact',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'contact', array('class'=>'input-small')); ?></td>
        <th><?php echo $form->labelEx($model,'phone',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'phone', array('class'=>'input-small')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'qq',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'qq', array('class'=>'input-small')); ?></td>
        <th><?php echo $form->labelEx($model,'wangwang',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'wangwang', array('class'=>'input-small')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'www',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textField($model,'www', array('class'=>'input-large')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'data_packet',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textField($model,'data_packet', array('class'=>'input-large')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'qq_group',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'qq_group', array('class'=>'input-small')); ?></td>
        <th><?php echo $form->labelEx($model,'memo',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textArea($model,'memo', array('row'=>'3','class'=>'input-small')); ?></td>
    </tr>
</table>
<?php $this->endWidget(); ?>
<script>
$(function(){
    //分类联动
    $('.iSelect').cate_select();
    // datepicker
    $('[data-form=datepicker]').datepicker();
});
</script>