<?php
$this->breadcrumbs=array(
    '英摩卡'=>array('/'),
    '卖家中心'=>array('.'),
    '供应商中心',
);
$this->menu=array(
    array('label'=>'','itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-plus"></i> 新增供应商',
        'linkOptions'=>array(
            'class'=>'iDialog btn btn-small btn-link',
            'data-uri'=>$this->createUrl("create"),
            'data-title'=>'新增供应商',
            'data-id'=>'SupplierForm',
            'data-ok'=>'创建',
            'data-callback'=>'oRefresh',
        )
    ),
    array('label'=>'','itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 分类管理',
        'url'=>array('type'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
);
$this->pageTitle='供应商中心';
?>
                        <!-- content-body -->
                        <div class="content-body">
                            <!--employee-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <!--department-->
                                    <div class="box-tab corner-all">
                                        <div class="tabbable tabs-left">
                                            <ul class="nav nav-tabs">
                                                <!--tab menus-->
                                                <li class="active"><a data-toggle="tab" href="#type-all" onclick="javascript:filtering('');">所有分类</a></li>
                                                <?php echo $types;?>
                                            </ul>
                                            <!-- widgets-tab-body -->
                                            <div class="tab-content">
                                                <table id="supplierList" class="iTablelist table table-hover table-bordered responsive" data-acturi="<?php echo $this->createUrl("aedit");?>">
                                                    <thead>
                                                        <tr>
                                                            <th><input class="iCheckAll" type="checkbox" name="checkall"></th>
                                                            <th>供应商名称</th>
                                                            <th>联系电话</th>
                                                            <th>分类</th>
                                                            <th>标签</th>
                                                            <th>状态</th>
                                                            <th>推荐</th>
                                                            <th>备注</th>
                                                            <th>更新时间</th>
                                                            <th>操作</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="5" class="dataTables_empty">数据正在加载中，请稍候……</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div><!--/widgets-tab-body-->
                                        </div>
                                    </div>
                                    <!--/department-->
                                </div>
                            </div>
                            <!--/employee-->
                            
                        </div><!--/content-body -->
                    </div><!-- /content -->
                </div><!-- /span content -->
<clip:js>
<script type="text/javascript" src="<%=Yii::app()->params->jsPath%>datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<%=Yii::app()->params->jsPath%>datatables/extras/ZeroClipboard.js"></script>
<script type="text/javascript" src="<%=Yii::app()->params->jsPath%>datatables/extras/TableTools.min.js"></script>
<script type="text/javascript" src="<%=Yii::app()->params->jsPath%>datatables/DT_bootstrap.js"></script>
<script type="text/javascript" src="<%=Yii::app()->params->jsPath%>responsive-tables.js"></script>
</clip:js>

<clip:script>
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function() {
    // uniform
    $('[data-form=uniform]').uniform();
    $('.iTablelist').listTable();
    
    // datatables table tools
    $('#supplierList').dataTable({
        "sAjaxSource": "<?php echo $this->createUrl('list');?>",
        "aaSorting": [[6,'desc'], [8,'desc']],
        "aoColumnDefs": [
            {
                "mData": "id",
                "aTargets": [0],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<input class="iCheck" type="checkbox" name="icheck" value="'+data+'">';
                }
            },
            {
                "mData": "name",
                "aTargets": [1],
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="name" data-tdtype="edit">'+data+'</span>';
                }
            },
            {
                "mData": "phone",
                "aTargets": [2],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="phone" data-tdtype="edit">'+data+'</span>';
                }
            },
            {
                "mData": "type_id",
                "aTargets": [3],
                "bSortable": false,
            },
            {
                "mData": "tag",
                "aTargets": [4],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="tag" data-tdtype="edit">'+data+'</span>';
                }
            },
            {
                "mData": "status",
                "aTargets": [5],
                "mRender": function(data, type, full) {
                    var img='';
                    if(data == 0){
                        img = 'disabled';
                    }else{
                        img = 'enabled';
                    }
                    return '<img src="/asset/img/toggle_'+img+'.gif" data-value="'+data+'" data-field="status" data-id="'+full.id+'" data-tdtype="toggle">';
                }
            },
            {
                "mData": "is_elite",
                "aTargets": [6],
                "mRender": function(data, type, full) {
                    var img='';
                    if(data == 0){
                        img = 'disabled';
                    }else{
                        img = 'enabled';
                    }
                    return '<img src="/asset/img/toggle_'+img+'.gif" data-value="'+data+'" data-field="is_elite" data-id="'+full.id+'" data-tdtype="toggle">';
                }
            },
            {
                "mData": "memo",
                "aTargets": [7],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="memo" data-tdtype="edit">'+data+'</span>';
                }
            },
            {
                "mData": "modified",
                "aTargets": [8],
            },
            {
                "mData": "id",
                "aTargets": [9],
                "mRender": function(data, type, full) {
                    var html='<a href="javascript:;" class="iDialog btn btn-mini btn-danger" data-uri="<?php echo $this->createUrl("edit");?>/id/'+data+'" data-title="编辑 '+full.name+' " data-id="SupplierForm" data-callback="oRefresh" data-ok="更新">编辑</a>';
                    html +='<a href="javascript:;" class="iConfirm btn btn-mini" data-msg="确定要删除 '+full.name+' 吗？" data-uri="<?php echo $this->createUrl('delete');?>/id/'+data+'" data-ok="删除" data-acttype="ajax" data-callback="oRefresh">删除</a>';
                    return '<div class="btn-group pull-right">'+html+'</div>';
                }
            },
            
        ],
    });
});

/*
 *刷单
 */
function oRefresh(){
    $('#supplierList').dataTable().fnDraw();
}
            
/*
 *快速筛选
 */
function filtering(type) {
    var oTable = $('#supplierList').dataTable();
    if(type === '') {
        oTable.fnFilter('', 3);
    } else {
        oTable.fnFilter(type, 3);
    }
}
/*]]>*/
</script>
</clip:script>