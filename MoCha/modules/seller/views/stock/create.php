<?php $form=$this->beginWidget('ActiveForm', array('id'=>'StockForm')); ?>
<table class="table table-hover table-bordered">
    <tr>
        <th><?php echo $form->labelEx($model,'price',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textField($model,'price', array('class'=>'input-medium')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'stock',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'stock', array('class'=>'input-small','value'=>'')); ?></td>
        <th><?php echo $form->labelEx($model, 'inferior', array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model, 'inferior', array('class'=>'input-small','value'=>'')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'supply_id',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->DropDownList($model,'supply_id',$supplies, array('class'=>'input-medium')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'memo',array('class'=>'control-label')); ?></th>
        <td colspan="3">
            <?php echo $form->textArea($model,'memo', array('row'=>'5','class'=>'input-large')); ?>
            <?php echo $form->hiddenField($model,'sku_id');?>
            <?php echo $form->hiddenField($model,'type');?>
        </td>
    </tr>
</table>
<?php $this->endWidget(); ?>