<?php $form=$this->beginWidget('ActiveForm', array('id'=>'StockForm')); ?>
<table class="table table-hover table-bordered table-condensed">
    <thead>
        <tr>
            <th><?php echo $form->labelEx($model,'sku_id',array('class'=>'control-label'));?></th>
            <th><?php echo $form->labelEx($model,'price',array('class'=>'control-label'));?></th>
            <th><?php echo $form->labelEx($model, 'stock', array('class'=>'control-label'));?></th>
            <th><?php echo $form->labelEx($model,'inferior',array('class'=>'control-label')); ?></th>
            <th><?php echo $form->labelEx($model,'supply_id',array('class'=>'control-label')); ?></th>
            <th><?php echo $form->labelEx($model,'memo',array('class'=>'control-label')); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($product->sku as $key=>$sku):?>
        <tr>
            <td>
                <?php echo $sku->name; ?>
                <?php echo Chtml::hiddenField('Stock[sku]['.$key.'][sku_id]', $sku->id);?>
            </td>
            <td><?php echo Chtml::textField('Stock[sku]['.$key.'1][price]', $sku->buiing_price, array('class'=>'input-small')); ?></td>
            <td><?php echo Chtml::textField('Stock[sku]['.$key.'][stock]', '', array('class'=>'input-small')); ?></td>
            <td><?php echo Chtml::textField('Stock[sku]['.$key.'][inferior]', '', array('class'=>'input-small')); ?></td>
            <td><?php echo Chtml::DropDownList('Stock[sku]['.$key.'][supply_id]', $sku->supply_id, $supplies, array('class'=>'input-small')); ?></td>
            <td><?php echo Chtml::textField('Stock[sku]['.$key.'][memo]', '', array('class'=>'input-small')); ?></td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
<?php echo Chtml::hiddenField('Stock[type]', $model->type);?>
<?php $this->endWidget();?>