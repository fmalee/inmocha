<?php $form=$this->beginWidget('ActiveForm', array('id'=>'StockForm')); ?>
<table class="table table-hover table-bordered">
    <tr>
        <th><?php echo $form->labelEx($model,'stock',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'stock', array('class'=>'input-small','value'=>'')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'memo',array('class'=>'control-label')); ?></th>
        <td>
            <?php echo $form->textArea($model,'memo', array('row'=>'5','class'=>'input-medium')); ?>
            <?php echo $form->hiddenField($model,'sku_id');?>
            <?php echo $form->hiddenField($model,'type');?>
            <?php echo $form->hiddenField($model,'price');?>
            <?php echo $form->hiddenField($model,'supply_id');?>
        </td>
    </tr>
</table>
<?php $this->endWidget(); ?>