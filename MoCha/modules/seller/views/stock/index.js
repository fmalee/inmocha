$(document).ready(function() {
    // uniform
    $('[data-form=uniform]').uniform();
    $('.iTablelist').listTable();
    
    // datatables table tools
    var oTable = $('#fakeList').dataTable({
        "sAjaxSource": "<?php echo $this->createUrl('list');?>",
        "aaSorting": [[5, "desc"]],
        "aoColumnDefs": [
            {
                "mData": "fake.id",
                "aTargets": [0],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<input class="iCheck" type="checkbox" name="icheck" value="'+data+'">';
                }
            },
            {
                "mData": "fake.id",
                "aTargets": [1],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<strong>'+data+'</strong><br /><small class="helper-font-small">'+full.trade.status+'</small>';
                }
            },
            {
                "mData": "trade.payment",
                "aTargets": [2],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<strong>'+data+'</strong><br /><small class="helper-font-small">'+full.fake.is_send+'</small>';
                }
            },
            {
                "mData": "trade.buyer_rate",
                "aTargets": [3],
                "mRender": function(data, type, full) {
                    return '<span>'+data+'</span><br /><span>'+full.trade.seller_rate+'</span>';
                }
            },
            {
                "mData": "fake.contact",
                "aTargets": [4],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<strong>'+data+'</strong><br /><small class="helper-font-small">'+full.fake.qq+'</small>';
                }
            },
            {
                "mData": "fake.created",
                "aTargets": [5],
                "mRender": function(data, type, full) {
                    return '<strong>'+data+'</strong><br /><small class="helper-font-small">'+full.fake.modified+'</small>';
                }
            },
            {
                "mData": "fake.type_id",
                "aTargets": [6],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<span>'+data+'</span><br /><span class="ajaxedit" style="color:" data-id="'+full.fake.id+'" data-field="user" data-tdtype="edit">'+full.fake.user+'</span>';
                }
            },
            {
                "mData": "fake.is_pay",
                "aTargets": [7],
                "mRender": function(data, type, full) {
                    var img='';
                    if(data == 0){
                        img = 'disabled';
                    }else{
                        img = 'enabled';
                    }
                    return '<img src="/asset/img/toggle_'+img+'.gif" data-value="'+data+'" data-field="is_pay" data-id="'+full.fake.id+'" data-tdtype="toggle">';
                }
            },
            {
                "mData": "fake.memo",
                "bSortable": false,
                "aTargets": [8],
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.fake.id+'" data-field="memo" data-tdtype="edit">'+data+'</span>';
                }
            },
            {
                "mData": "fake.id",
                "aTargets": [9],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    var html='<a href="javascript:;" class="iDialog btn btn-mini btn-danger" data-uri="<?php echo $this->createUrl("edit");?>/id/'+data+'" data-title="编辑'+data+'" data-id="FakeForm" data-callback="oRefresh" data-ok="更新">编辑</a>';
                    html +='<a href="javascript:;" class="iConfirm btn btn-mini" data-msg="确定要删除'+data+'吗？" data-uri="<?php echo $this->createUrl('delete');?>/id/'+data+'" data-ok="删除" data-acttype="ajax" data-callback="oRefresh">删除</a>';
                    return '<div class="btn-group pull-right">'+html+'</div>';
                }
            }
        ],
    });
});

/*
 *刷单表格
 */
function oRefresh(){
    $('#fakeList').dataTable().fnDraw();
}

/*
 *快速筛选
 */
function filtering(type) {
    var oTable = $('#fakeList').dataTable();
    if(type === '') {
        oTable.fnFilter('', 6);
    } else {
        oTable.fnFilter(type, 6);
    }
}