<?php
$this->breadcrumbs=array(
    '英摩卡'=>array('..'),
    '卖家中心'=>array('.'),
    '刷单记录',
);
$this->menu=array(
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-plus"></i> 新增刷单',
        'linkOptions'=>array(
            'class'=>'iPrompt btn btn-small btn-link',
            'data-uri'=>$this->createUrl("create"),
            'data-title'=>'新增刷单记录',
            'data-msg'=>'请输入订单编号',
            'data-id'=>'FakeForm',
            'data-ok'=>'创建',
            'data-callback'=>'oRefresh',
        )
    ),
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 分类管理',
        'url'=>array('type'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
);
$this->pageTitle='刷单记录';
$files = array(
    'datatables/jquery.dataTables.min.js',
    'datatables/extras/ZeroClipboard.js',
    'datatables/extras/TableTools.min.js',
    'datatables/DT_bootstrap.js',
    'responsive-tables.js',
    'DT_bootstrap.css',
    'responsive-tables.css'
);
Ya::register($files);
Ya::registerScript('js','index.js');
?>
                        <!-- content-body -->
                        <div class="content-body">
                            <!-- tables -->
                            <!-- dashboar -->
                            <!-- tab stat -->
                            <div class="box-tab corner-all">
                                <div class="box-header corner-top">
                                    <div class="header-control pull-right">
                                        <a data-box="collapse"><i class="icofont-caret-up"></i></a>
                                    </div>
                                    <ul class="nav nav-pills">
                                        <li class="active"><a data-toggle="tab" href="#fake_all" onclick="javascript:filtering('');">所有记录</a></li>
                                        <?php foreach(Type::Lists('fake') as $key => $type): ?>
                                        <li><a data-toggle="tab" href="#fake_<?php echo $key; ?>" onclick="javascript:filtering('<?php echo $key;?>');"><?php echo $type;?></a></li>
                                        <?php endforeach;?>
                                    </ul>
                                </div>
                                <div class="box-body">
                                    <table id="fakeList" class="iTablelist table table-hover responsive" data-acturi="<?php echo $this->createUrl("aedit");?>">
                                        <thead>
                                            <tr>
                                                <th><input class="iCheckAll" type="checkbox" name="checkall"></th>
                                                <th>订单状态</th>
                                                <th>价格</th>
                                                <th>评价状态</th>
                                                <th>联系人</th>
                                                <th>订单时间</th>
                                                <th>刷单类别</th>
                                                <th>结算</th>
                                                <th>备注</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="5" class="dataTables_empty">数据正在加载中，请稍候……</td>
                                            </tr>
                                    </tbody>
                                </table>
                                </div>
                            </div><!-- /tab stat -->
                            
                            <!--/dashboar-->
                        </div><!--/content-body -->
                    </div><!-- /content -->
                </div><!-- /span content -->
