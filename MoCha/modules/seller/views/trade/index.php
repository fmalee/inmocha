<?php
$this->breadcrumbs=array(
    '英摩卡'=>array('..'),
    '卖家中心'=>array('.'),
    '订单管理',
);
$this->menu=array(
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-exchange"></i> 同步订单',
        'linkOptions'=>array(
            'class'=>'btn btn-small btn-link',
            'onclick'=>'tradeSyn();',
        )
    ),
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 退款管理',
        'url'=>array('/seller/refund'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
);
$this->pageTitle='订单管理';
Ya::registerScript('js','index.js');
?>
                        <!-- content-body -->
                        <div class="content-body">
                            <!-- tables -->
                            <div class="box-tab corner-all">
                                <div class="box-header corner-top">
                                    <div class="header-control pull-right">
                                        <a data-box="collapse"><i class="icofont-caret-up"></i></a>
                                    </div>
                                    <?php $this->widget('zii.widgets.CMenu',array(
                                        'items'=>array(
                                            array(
                                                'label'=>'所有订单',
                                                'url'=>array('/seller/trade'),
                                                'active'=>(!isset($this->actionParams['status']) && !isset($this->actionParams['refund']) && !isset($this->actionParams['seller_rate'])) ? true : false,
                                            ),
                                            array(
                                                'label'=>Trade::itemAlias('status', Trade::WAIT_BUYER_PAY),
                                                'url'=>array('/seller/trade', 'status'=>Trade::WAIT_BUYER_PAY),
                                                'active'=>($this->actionParams['status'] == Trade::WAIT_BUYER_PAY) ? true : false,
                                            ),
                                            array(
                                                'label'=>Trade::itemAlias('status', Trade::WAIT_SELLER_SEND_GOODS),
                                                'url'=>array('/seller/trade', 'status'=>Trade::WAIT_SELLER_SEND_GOODS),
                                                'active'=>($this->actionParams['status'] == Trade::WAIT_SELLER_SEND_GOODS) ? true : false,
                                            ),
                                            array(
                                                'label'=>Trade::itemAlias('status', Trade::WAIT_BUYER_CONFIRM_GOODS),
                                                'url'=>array('/seller/trade', 'status'=>Trade::WAIT_BUYER_CONFIRM_GOODS),
                                                'active'=>($this->actionParams['status'] == Trade::WAIT_BUYER_CONFIRM_GOODS) ? true : false,
                                            ),
                                            array(
                                                'label'=>Trade::itemAlias('sellerRate', 0),
                                                'url'=>array('/seller/trade', 'seller_rate'=>0),
                                                'active'=>($this->actionParams['seller_rate'] === '0') ? true : false,
                                            ),
                                            array(
                                                'label'=>Trade::itemAlias('status', Trade::TRADE_FINISHED),
                                                'url'=>array('/seller/trade', 'status'=>Trade::TRADE_FINISHED),
                                                'active'=>($this->actionParams['status'] == Trade::TRADE_FINISHED) ? true : false,
                                            ),
                                        ),
                                        'htmlOptions'=>array('class'=>'nav nav-tabs'),
                                    )); ?>
                                </div>
                                <div class="box-body">
                                     <table id="TradeList" class="table table-condensed table-bordered">
                                        <thead>
                                            <tr>
                                                <th  class="text-center"><span class="text-center">宝贝</span></th>
                                                <th>单价(元)</th>
                                                <th>数量</th>
                                                <th>售后</th>
                                                <th>买家</th>
                                                <th>交易状态</th>
                                                <th>实收款(元)</th>
                                                <th>评价</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="info">
                                                <td colspan="8">
                                                    <input class="iCheckAll" type="checkbox" name="checkall">
                                                    <div class="btn-group">
                                                        <a href="javascript:void(0)" class="btn btn-mini" target="_blank">批量发货</a>
                                                        <a href="javascript:void(0)" class="btn btn-mini" target="_blank">批量备忘</a>
                                                        <a href="javascript:void(0)" class="btn btn-mini" target="_blank">批量免运费</a>
                                                        <a href="javascript:void(0)" class="btn btn-mini" target="_blank">不显示已关闭的订单</a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                            <?php $this->widget('YListView', array(
                                                'dataProvider'=>$dataProvider,
                                                'id'=>'trade',
                                                'itemView'=>'_list',
                                                'itemsTagName'=>'',
                                                'tagName'=>'tbody',
                                                'loadingCssClass'=>'',
                                                'summaryCssClass'=>'pagination pull-left',
                                                'pagerCssClass'=>'pagination pagination-right',
                                                'template'=>"{items}\n<tr><td colspan=\"8\">{summary}{pager}</td></tr>",
                                            )); ?>
                                     </table>
                                </div>
                            </div><!-- /tab stat -->
                            
                            <!--/dashboar-->
                        </div><!--/content-body -->
                    </div><!-- /content -->
                </div><!-- /span content -->
