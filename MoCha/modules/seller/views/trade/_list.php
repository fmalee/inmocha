
                                            <tr><td colspan="8"></td></tr>
                                            <tr class="success">
                                                <td colspan="8">
                                                    <input class="iCheck" type="checkbox" id="icheck_<?php echo $data->id;?>" value="<?php echo $data->id;?>">
                                                    <span class="help-inline checkbox">订单编号：<?php echo $data->id;?></span>
                                                    <span class="help-inline">成交时间：<?php echo date('Y-m-d H:i:s',$data->created);?></span>
                                                    <span class="pull-right">
                                                        <a href="javascript:void(0)" class="" onclick="asyn(<?php echo $data->id;?>)" title="刷新宝贝">&nbsp;&nbsp;</a>
                                                    <?php if($data->is_fake):?>
                                                        <a href="javascript:void(0)" onclick="edit('Credit',<?php echo $data->id;?>)" title="刷单记录"><i class="icofont-magnet"></i></a>
                                                    <?php else: ?>
                                                        刷单
                                                     <?php endif;?>
                                                        <a href="javascript:void(0)" onclick="service(<?php echo $data->id;?>)" title="售后记录"><i class="icofont-magnet"></i></a>
                                                    <?php if($data->buyer_message):;?>
                                                        <a href="javascript:void(0)" rel="popover" data-placement="left" title="买家留言" data-content="<?php echo $data->buyer_message;?>"><img src="/asset/img/taobao/new_msg.gif" /></a>
                                                    <?php endif;?>
                                                        <a href="javascript:void(0)" class="iDialog" data-uri="<?php echo $this->createUrl('memo', array('id'=>$data->id));?>" data-title="修改备注" data-id="MemoForm" data-ok="修改"><?php echo Trade::flags($data->seller_flag);?></a>
                                                    </span>
                                                </td>
                                            </tr>
                                            <!--start 子订单list  start-->
                                            <?php $count = count($data->order);?>
                                            <?php $i = 0;?>
                                            <?php foreach($data->order as $k => $order):?>
                                            <?php $i++;?>
                                            <tr id="item_<?php echo $order->id;?>">
                                                <td title="<?php echo $order->title;?>">
                                                    <div class="row-fluid">
                                                        <div class="span2">
                                                            <a target="_blank" title="查看宝贝详情" href=""><img alt="查看宝贝详情" class="img-polaroid" src="<?php echo $order->pic_path;?>_sum.jpg" width="50"></a>
                                                        </div>
                                                        <div class="span10">
                                                            <a href="<?php echo $this->createUrl('./Item/', array('id', $order->item_id));?>" title="<?php echo $order->title;?>" target="_blank"><?php echo $order->outer_iid;?></a><br />
                                                            商家编码: <?php echo $order->outer_iid;?><br />
                                                        <?php if($data->status == Trade::WAIT_SELLER_SEND_GOODS):?>
                                                            <a href="javascript:void(0)" onclick="edit(<?php echo $order->id;?>, '{$action['sku']['action']}', '{$action['sku']['title']}', '500', '300')" title="{$action['sku']['title']}"><strong><?php echo $order->sku_properties_name;?></strong></a>
                                                        <?php else:?>
                                                            <?php echo $order->sku_properties_name;?>
                                                        <?php endif;?>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td title="<?php echo $order->price;?>" style="border-left:0;text-align: center;">
                                                    <?php echo $order->price;?><br />
                                                    {$order['discount']}
                                                </td>
                                                <td title="<?php echo $order->num;?>" style="border-left:0;text-align: center;">
                                                    <?php echo $order->num;?>
                                                </td>
                                                <td style="border-left:0;text-align: center;">
                                                    <a href="javascript:void(0)" onclick="service(<?php echo $data->id;?>,<?php echo $order->id;?>)" title="售后记录"><i class="icofont-magnet"></i></a>
                                                    <?php if(isset($order->refund->order_id)):?><a href="{:U('Seller/Refund/read')}/?refund_id={$order['refund_id']}" id="{$order['refund_id']}" class="xi2" onclick="">退款</a><?php endif;?>
                                                </td>
                                                <?php if($i == 1):;?>
                                                <td rowspan="<?php echo $count;?>" sumrows="<?php echo $count;?>" style="text-align: center;">
                                                    <a href="http://member1.taobao.com/member/user_profile.jhtml?userID=d446dc61881b1a30df3d7c8cb8ac59e7" target="_blank"><?php echo $data->buyer_nick;?></a>
                                                    <a title="查询该买家订单" href="{:U('index', array('searchtype'=>0,'keyword'=>$info['buyer_nick']))}"><img src="/asset/img/taobao/buyer_trade.png" alt="查询该买家订单"></a><br />
                                                <?php if($data->status == Trade::WAIT_SELLER_SEND_GOODS):;?>
                                                    <a href="javascript:void(0)" onclick="edit(<?php echo $data->id;?>, '{$action['address']['action']}', '{$action['address']['title']}', '500', '300')" title="{$action['address']['title']}"><strong><?php echo $data->receiver_name;?></strong></a>
                                                <?php else:?>
                                                    <?php echo $data->receiver_name;?>
                                                <?php endif;?>
                                                    <span class="iWangwang" data-nick="<?php echo $data->buyer_nick;?>" data-type="2"><?php echo $data->buyer_nick;?></span><br />
                                                    <a href="javascript:void(0)" onclick="trace(<?php echo $data->id;?>)" title="查询物流信息"><?php echo $data->delivery_company;?></a>
                                                </td>
                                                <td rowspan="<?php echo $count;?>" sumrows="<?php echo $count;?>" style="text-align: center;">
                                                    <a href="http://buy.taobao.com/detail/orderDetail.htm?bizOrderId=<?php echo $data->id;?>" target="_blank"><strong class="J_TradeStatus status todo h"><?php echo Trade::itemAlias('status',$data->status);?></strong></a><br />
                                                    <a href="http://trade.taobao.com/trade/detail/trade_item_detail.htm?bizOrderId=<?php echo $data->id;?>" class="detail-link" target="_blank">详情</a><br />
                                                <?php if($data->status == Trade::WAIT_BUYER_PAY):;?>
                                                    <a href="javascript:void(0)" class="btn btn-mini">关闭交易</a>
                                                <?php elseif($data->status == Trade::WAIT_SELLER_SEND_GOODS):;?>
                                                    <a href="javascript:void(0)" class="btn btn-mini btn-danger">发货</a>
                                                <?php elseif($data->status == Trade::WAIT_BUYER_CONFIRM_GOODS):;?>
                                                    <a href="javascript:void(0)" class="btn btn-mini btn-danger">延长收货时间</a>
                                                <?php endif;?>
                                                </td>
                                                <td rowspan="<?php echo $count;?>" sumrows="<?php echo $count;?>" style="text-align: center;">
                                                <?php if($data->status == Trade::WAIT_BUYER_PAY):;?>
                                                    <a href="javascript:void(0)" onclick="edit(<?php echo $data->id;?>, '{$action['price']['action']}', '{$action['price']['title']}')" title="{$action['price']['title']}"><?php echo $order->payment;?></a>
                                                <?php else: ?>
                                                    <strong><?php echo $order->payment;?></strong>
                                                <?php endif;?>
                                                    <div class="post-info">
                                                    <?php if($data->shipping_type =='free'):?>
                                                        卖家包邮
                                                    <?php else:?>
                                                        (含<?php echo Trade::itemAlias('shippingType',$data->shipping_type);?>:<?php echo $data->post_fee;?>)
                                                    <?php endif;?>
                                                    <br />&yen;{$info['discount']}<br />
                                                    </div>
                                                    <?php if(strstr($data->trade_from, Trade::FROM_WAP)):?><img src="{:C('IMG_PATH')}taobao/m_trade.png"><?php endif;?>
                                                    <?php if($data->type == Trade::TYPE_COD):?><img src="/asset/img/taobao/cod-icon.png"> 货到付款<?php endif;?>
                                                    <?php if($data->type == Trade::TYPE_FENXIAO):?><img src="/asset/img/taobao/fenxiao.png"><?php endif;?>
                                                </td>
                                                <td rowspan="<?php echo $count;?>" sumrows="<?php echo $count;?>" style="text-align: center;">
                                                    <?php echo Trade::itemAlias('sellerRate', $data->seller_rate);?><br />
                                                    <?php echo Trade::itemAlias('buyerRate', $data->buyer_rate);?>
                                                </td>
                                                <? endif;?>
                                            </tr>
                                            <?php endforeach;?>
