$(document).ready(function() {

    $("a[rel=popover]").popover().live("mouseover",function(e) {
       $(this).popover('show')
    });
    $("a[rel=popover]").popover().live("mouseout",function(e) {
       $(this).popover('hide')
    });

});

function tradeSyn(){
    var did = 'SynForm',
        uri = '<?php echo $this->createUrl('syn');?>';
    $.dialog({id:did}).close();
    $.dialog({
        id:did,
        title:'同步订单',
        lock:false,
        okValue: '同步',
        ok:function(){
            var form = this.dom.content.find('#'+did);
            form.submit();
            return false;
        }
    });
    $.getJSON(uri, function(result){
        if(result.status == 1){
            $.dialog.get(did).content(result.msg);
        }
    });
    return false;
}
/*
 *删除
 */
function oDelete(id){
    $('#invoice_'+id).hide(500); //动态隐藏
    $('#invoice_'+id).empty();
}