<?php $form=$this->beginWidget('ActiveForm', array('id'=>'SynForm')); ?>
<table class="table table-hover table-bordered">
    <tr>
        <th>交易创建时间</th>
        <th>交易状态</th>
    </tr>
    <tr>
        <td>
        <select name="Syn[time]" size="2" style="height:130px;width:110px;">
            <option value='43200' selected>最近半天</option>
            <option value="86400" >最近一天</option>
            <option value="259200" >最近三天</option>
            <option value="604800" >最近一周</option>
            <option value="1296000" >最近半个月</option>
            <option value="2592000" >最近一个月</option>
            <option value="7776000" >最近三个月</option>
        </select>
        </td>
        <td>
        <select name="Syn[status]" size="2" style="height:130px;width:90px;">
            <option value="" selected>全部状态</option>
            <option value="ALL_WAIT_PAY">待付款</option>
            <option value="WAIT_SELLER_SEND_GOODS">待发货</option>
            <option value="WAIT_BUYER_CONFIRM_GOODS">已发货</option>
            <option value="TRADE_FINISHED">已成功</option>
            <option value="ALL_CLOSED">已关闭</option>
        </select>
        </td>
    </tr>
    <tr>
        <td colspan="2">
        1、您的交易数据已经和淘宝实现同步，不需要手动更新；<br />
        2、如果想更新同步之前的数据，请选择对应的时间；<br />
        3、如需更新单笔交易，可以返回执行'更新'
        </td>
    </tr>
</table>
<?php $this->endWidget(); ?>