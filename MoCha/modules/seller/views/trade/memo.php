<?php $form=$this->beginWidget('ActiveForm', array('id'=>'MemoForm')); ?>
<table class="table table-hover table-bordered">
    <tr>
        <th><?php echo $form->labelEx($model,'seller_flag',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo Yhtml::radioButtonGroup('Memo[flag]',$model->seller_flag, Trade::flags()); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'seller_memo',array('class'=>'control-label')); ?></th>
        <td><?php echo Yhtml::textArea('Memo[memo]', $model->seller_memo, array('row'=>'5','class'=>'input-large')); ?></td>
    </tr>
</table>
<?php echo Chtml::submitButton();?>
<?php $this->endWidget(); ?>