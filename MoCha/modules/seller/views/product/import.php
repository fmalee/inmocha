<?php $form=$this->beginWidget('ActiveForm', array('id'=>'ImportForm')); ?>
<table class="table table-hover table-bordered">
    <tr>
        <th><?php echo Chtml::label('类别', 'Import[type_id]',array('class'=>'control-label')); ?></th>
        <td>
            <?php echo Chtml::dropDownList('Import[type_id]', 0, array(), array(
                'class'=>'input-small iSelect',
                'data-uri'=>$this->createUrl('/type/childs',array('module'=>Type::MODULE_PRODUCT)),
                'data-pid'=>'0',
            )); ?>
            <?php echo Chtml::hiddenField('Import[type_id]', 0, array('id'=>'iCatid'));?>
        </td>
    </tr>
    <tr>
        <th><?php echo Chtml::label('编码处理', 'Import[label]',array('class'=>'control-label')); ?></th>
        <td><?php echo Yhtml::radioButtonGroup('Import[label]', 0, array('0'=>'保持商家编码','1'=>'重新生成商家编码'));?></td>
    </tr>
    <tr>
        <th><?php echo Chtml::label('库存处理', 'Import[stock]',array('class'=>'control-label')); ?></th>
        <td><?php echo Yhtml::radioButtonGroup('Import[stock]', 0, array('0'=>'不导入库存','1'=>'导入库存'));?></td>
    </tr>
</table>
<?php $this->endWidget(); ?>
<script>
$(function(){
    //分类联动
    $('.iSelect').cate_select();
    // datepicker
    $('[data-form=datepicker]').datepicker();
});
</script>