<?php
$this->pageTitle='分类管理';
$this->breadcrumbs=array(
    '英摩卡'=>array('/shop'),
    '卖家中心'=>array('./'),
    '产品中心'=>array('.'),
    '分类管理',
);
$this->menu=array(
    array('label'=>'','itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 产品中心',
        'url'=>array('.'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
);
Ya::registerScript(
    'init',
    "$(document).ready(function() {
        // uniform
        $('[data-form=uniform]').uniform();
        $('.iTablelist').listTable();
    });",
    true
);
?>
                        <!-- content-body -->
                        <div class="content-body">
                            <!-- tab stat -->
                            <div class="box corner-all">
                                <div class="box-header grd-white color-silver-dark corner-top">
                                    <div class="header-control">
                                        <span>分类管理</span>
                                    </div>
                                    <span>
                                        <button class="iDialog btn" data-uri="<?php echo $this->createUrl('/type/create', array('module'=>'product'));?>" data-title="新增产品分类" data-id="TypeForm" data-ok="添加"><i class="icon-plus"></i> 新增产品分类</button>
                                    </span>
                                </div>
                                <div class="box-body">
                                    <table id="typeList" class="iTablelist table table-hover responsive" data-acturi="<?php echo $this->createUrl("/type/aedit", array('module'=>'product'));?>">
                                        <thead>
                                            <tr>
                                                <th><input class="iCheckAll" data-form="uniform" type="checkbox" name="checkall"></th>
                                                <th>分类名称</th>
                                                <th>状态</th>
                                                <th>排序</th>
                                                <th>创建时间</th>
                                                <th>修改时间</th>
                                                <th>备注</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php echo $list;?>
                                    </tbody>
                                </table>
                                </div>
                            </div><!-- /tab stat -->
                        </div><!--/content-body -->
                    </div><!-- /content -->
                </div><!-- /span content -->
