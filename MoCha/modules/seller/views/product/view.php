<?php
$this->breadcrumbs=array(
    '英摩卡'=>array('/'),
    '卖家中心'=>array('..'),
    '产品中心'=>array('.'),
    '产品详情',
);
$this->menu=array(
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-plus"></i> 新增产品',
        'linkOptions'=>array(
            'class'=>'iDialog btn btn-small btn-link',
            'data-uri'=>$this->createUrl("create"),
            'data-title'=>'新增产品',
            'data-id'=>'ProductForm',
            'data-ok'=>'创建'
        )
    ),
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 产品中心',
        'url'=>array('.'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 分类管理',
        'url'=>array('type'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
);
$this->pageTitle= $model->name.' - 产品详情';
$files = array(
    'datatables/jquery.dataTables.min.js',
    'datatables/extras/ZeroClipboard.js',
    'datatables/extras/TableTools.min.js',
    'datatables/DT_bootstrap.js',
    'responsive-tables.js',
    'DT_bootstrap.css',
    'responsive-tables.css',
    'select2.min.js',
    'select2.css'
);
Ya::register($files);
Ya::registerScript('js','view.js');
?>
                        <!-- content-body -->
                        <div class="content-body">
                            <!--employee-->
                            <div id="invoice-container" class="invoice-container">
                                <div class="page-header">
                                    <div class="pull-right">
                                        <img data-src="holder.js/120x120" src="<?php echo $model->picture;?>_120x120.jpg" class="img-circle">
                                    </div>
                                    <h3><?php echo $model->name;?> <small><?php echo $model->title;?></small></h3>
                                </div>
                                <div class="row-fluid">
                                    <div class="span4">
                                        <p class="muted">标签</p>
                                        <p><?php echo $model->tag;?></p>
                                        <p>47 Address</p>
                                        <p>Example, NY.80091</p>
                                        <p>invoice@mail.com</p>
                                    </div>
                                    <div class="span4">
                                        <p class="muted">备注</p>
                                        <p><?php echo $model->memo;?></p>
                                        <p>432 Main Street</p>
                                        <p>San Francisco, CA 91234</p>
                                    </div>
                                    <div class="span4">
                                        <p>Invoice No. 247</p>
                                        <p>Invoice Date. December 21, 2012</p>
                                        <p>Payment Due. January 17, 2012</p>
                                    </div>
                                </div>
                                <div class="invoice-table">
                                    <table class="iTablelist table table-bordered table-hover invoice responsive" data-acturi="<?php echo $this->createUrl("/seller/sku/aedit");?>">
                                        <thead>
                                            <tr>
                                                <th>货号</th>
                                                <th><?php echo $model->sku_1;?></th>
                                                <th><?php echo $model->sku_2;?></th>
                                                <th>现价</th>
                                                <th>时价</th>
                                                <th>成本</th>
                                                <th>库存</th>
                                                <th>次品</th>
                                                <th>状态</th>
                                                <th>位置</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($model->sku as $sku):?>
                                            <tr>
                                                <td><?php echo Yhtml::AjaxEdit($sku, 'name');?></td>
                                                <td><?php echo Yhtml::AjaxEdit($sku, 'sku_1');?></td>
                                                <?php if($model->sku_2){?><td><?php echo Yhtml::AjaxEdit($sku, 'sku_2');?></td><?};?>
                                                <td class="right"><?php echo Yhtml::AjaxEdit($sku, 'selling_price');?></td>
                                                <td class="right"><?php echo Yhtml::AjaxEdit($sku, 'current_price');?></td>
                                                <td class="right"><?php echo Yhtml::AjaxEdit($sku, 'buiing_price');?></td>
                                                <td class="right"><?php echo $sku->stock;?></td>
                                                <td class="right"><?php echo $sku->inferior;?></td>
                                                <td class="right"><?php echo Yhtml::AjaxToggle($sku, 'status');?></td>
                                                <td class="right"><?php echo Yhtml::AjaxEdit($sku, 'location');?></td>
                                                <td class="right">
                                                    <div class="btn-group pull-right">
                                                        <a class="btn btn-danger btn-mini dropdown-toggle" data-toggle="dropdown" href="#">调整库存 <span class="caret"></span></a>
                                                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/create',array('sku_id'=>$sku->id, 'type'=>Stock::TYPE_BUY));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_BUY).$sku->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_BUY);?></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/create',array('sku_id'=>$sku->id, 'type'=>Stock::TYPE_SELL));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_SELL).$sku->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_SELL);?></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/create',array('sku_id'=>$sku->id, 'type'=>Stock::TYPE_RECYCLE));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_RECYCLE).$sku->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_RECYCLE);?></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/create',array('sku_id'=>$sku->id, 'type'=>Stock::TYPE_RETURN));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_RETURN).$sku->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_RETURN);?></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/create',array('sku_id'=>$sku->id, 'type'=>Stock::TYPE_LOST));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_LOST).$sku->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_LOST);?></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/create',array('sku_id'=>$sku->id, 'type'=>Stock::TYPE_GAIN));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_GAIN).$sku->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_GAIN);?></a>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/inferior',array('sku_id'=>$sku->id, 'type'=>Stock::TYPE_BROKEN));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_BROKEN).$sku->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_BROKEN);?></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/inferior',array('sku_id'=>$sku->id, 'type'=>Stock::TYPE_REPAIR));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_REPAIR).$sku->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_REPAIR);?></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/inferior',array('sku_id'=>$sku->id, 'type'=>Stock::TYPE_DESTROY));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_DESTROY).$sku->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_DESTROY);?></a>
                                                            </li>
                                                        </ul>
                                                        <!--<a href="javascript:;" class="iDialog btn btn-mini btn-danger" data-uri="<?php echo $this->createUrl('/seller/sku/edit',array('id'=>$sku->id));?>" data-title="编辑 <?php echo $sku->name;?>" data-id="SkuForm" data-callback="oRefresh" data-ok="更新">编辑</a>-->
                                                        <?php if(!$sku->stock && !$sku->inferior):?>
                                                        <a href="javascript:;" class="iConfirm btn btn-mini" data-msg="确定要删除 <?php echo $sku->name;?> 吗？" data-uri="<?php echo $this->createUrl('/seller/sku/delete',array('id'=>$sku->id));?>" data-ok="删除" data-acttype="ajax" data-callback="oRefresh">删除</a>
                                                        <?php endif;?>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="<?php echo $model->sku_2 ? '3' : '2';?>" class="right">总计</td>
                                                <td class="right"><?php echo $model->selling_price;?></td>
                                                <td class="right"><?php echo $model->current_price;?></td>
                                                <td class="right"><?php echo $model->buiing_price;?></td>
                                                <td class="right"><?php echo $model->stock;?></td>
                                                <td class="right"><?php echo $model->inferior;?></td>
                                                <td colspan="3" class="right">
                                                    
                                                    <div class="btn-group">
                                                        <a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="#">调整库存 <span class="caret"></span></a>
                                                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/bath',array('product_id'=>$model->id, 'type'=>Stock::TYPE_BUY));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_BUY).$model->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_BUY);?></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/bath',array('product_id'=>$model->id, 'type'=>Stock::TYPE_SELL));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_SELL).$model->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_SELL);?></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/bath',array('product_id'=>$model->id, 'type'=>Stock::TYPE_RECYCLE));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_RECYCLE).$model->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_RECYCLE);?></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/bath',array('product_id'=>$model->id, 'type'=>Stock::TYPE_RETURN));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_RETURN).$model->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_RETURN);?></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/bath',array('product_id'=>$model->id, 'type'=>Stock::TYPE_LOST));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_LOST).$model->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_LOST);?></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/bath',array('product_id'=>$model->id, 'type'=>Stock::TYPE_GAIN));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_GAIN).$model->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_GAIN);?></a>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/bath',array('product_id'=>$model->id, 'type'=>Stock::TYPE_BROKEN));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_BROKEN).$model->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_BROKEN);?></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/bath',array('product_id'=>$model->id, 'type'=>Stock::TYPE_REPAIR));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_REPAIR).$model->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_REPAIR);?></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="iDialog" data-uri="<?php echo $this->createUrl('/seller/stock/bath',array('product_id'=>$model->id, 'type'=>Stock::TYPE_DESTROY));?>" data-title="<?php echo Stock::itemAlias('stock',Stock::TYPE_DESTROY).$model->name;?>" data-id="StockForm" data-callback="oRefresh" data-ok="修改"><?php echo Stock::itemAlias('stock',Stock::TYPE_DESTROY);?></a>
                                                            </li>
                                                        </ul>
                                                        <a class="iDialog btn" data-uri="<?php echo $this->createUrl('/seller/sku/create', array('product_id'=>$model->id));?>" data-title="添加属性" data-id="SkuForm" data-ok="添加"><i class="icon-plus"></i> 添加属性</a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <!--/employee-->
                            
                            <!-- tab stat -->
                            <div class="box corner-all">
                                <div class="box-header grd-white color-silver-dark corner-top">
                                    <div class="header-control">
                                        <span><button class="iDialog btn btn-mini" data-uri="<?php echo $this->createUrl('/seller/supply/create', array('product_id'=>$model->id));?>" data-title="新增供应" data-id="SupplyForm" data-ok="添加"><i class="icon-plus"></i> 新增供应</button></span>
                                    </div>
                                    <span>
                                        <span>供应商</span>
                                    </span>
                                </div>
                                <div class="box-body">
                                    <table id="supply" class="iTablelist table table-hover responsive" data-acturi="<?php echo $this->createUrl("/seller/supply/aedit");?>">
                                        <thead>
                                            <tr>
                                                <th>供应商</th>
                                                <th>货号</th>
                                                <th>价格</th>
                                                <th>库存</th>
                                                <th>状态</th>
                                                <th>推荐</th>
                                                <th>备注</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($model->supply as $supply):?>
                                            <tr>
                                                <td class="right"><?php echo $supply->suppliername;?></td>
                                                <td class="right"><?php echo Yhtml::AjaxEdit($supply, 'name');?><span><a href="<?php echo $supply->www;?>" target="_blank"><i class="icon-share"></i></a></span></td>
                                                <td class="right"><?php echo Yhtml::AjaxEdit($supply, 'price');?></td>
                                                <td class="right"><?php echo Yhtml::AjaxEdit($supply, 'stock');?></td>
                                                <td class="right"><?php echo Yhtml::AjaxToggle($supply, 'status');?></td>
                                                <td class="right"><?php echo Yhtml::AjaxToggle($supply, 'is_elite');?></td>
                                                <td class="right"><?php echo Yhtml::AjaxEdit($supply, 'memo');?></td>
                                                <td class="right">
                                                    <div class="btn-group pull-right">
                                                        <a href="javascript:;" class="iDialog btn btn-mini btn-danger" data-uri="<?php echo $this->createUrl('/seller/supply/edit',array('id'=>$supply->id));?>" data-title="编辑 <?php echo $supply->name;?>" data-id="SupplyForm" data-callback="oRefresh" data-ok="更新">编辑</a>
                                                        <a href="javascript:;" class="iConfirm btn btn-mini" data-msg="确定要删除 <?php echo $supply->name;?> 吗？" data-uri="<?php echo $this->createUrl('/seller/supply/delete',array('id'=>$supply->id));?>" data-ok="删除" data-acttype="ajax" data-callback="oRefresh">删除</a>
                                                        <?php if($model->supply_id != $supply->id):?><a href="javascript:;" class="iConfirm btn btn-mini" data-msg="确定要设置 <?php echo $supply->name;?> 为首选供应商吗？" data-uri="<?php echo $this->createUrl('/seller/supply/setMajor',array('supply_id'=>$supply->id, 'product_id'=>$model->id));?>" data-ok="设置" data-acttype="ajax" data-callback="oRefresh">首先供应商</a><?php endif;?>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div><!-- /tab stat -->
                            
                        </div><!--/content-body -->
                    </div><!-- /content -->
                </div><!-- /span content -->
