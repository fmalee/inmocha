<?php $form=$this->beginWidget('ActiveForm', array('id'=>'ProductForm')); ?>
<table class="table table-hover table-bordered">
    <tr>
        <th><?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textField($model,'name', array('class'=>'input-medium')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'type_id',array('class'=>'control-label')); ?></th>
        <td colspan="3">
            <?php echo $form->dropDownList($model,'type_id', array(), array(
                'class'=>'input-small iSelect',
                'data-uri'=>$this->createUrl('/type/childs',array('module'=>'product')),
                'data-pid'=>'0',
            )); ?>
            <?php echo $form->hiddenField($model,'type_id', array('id'=>'iCatid'));?>
        </td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'title',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textField($model,'title', array('class'=>'input-large')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'picture',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textField($model,'picture', array('class'=>'input-large')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'tag',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textField($model,'tag', array('class'=>'input-large')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'status',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->radioButtonGroup($model,'status',Supplier::itemAlias('status')); ?></td>
        <th><?php echo $form->labelEx($model,'is_elite',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->radioButtonGroup($model,'is_elite',Supplier::itemAlias('is_elite')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'selling_price',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'selling_price', array('class'=>'input-small')); ?></td>
        <th><?php echo $form->labelEx($model,'current_price',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'current_price', array('class'=>'input-small')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'memo',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textArea($model,'memo', array('row'=>'3','class'=>'input-large')); ?></td>
    </tr>
</table>
<?php $this->endWidget(); ?>
<script>
$(function(){
    //分类联动
    $('.iSelect').cate_select();
    // datepicker
    $('[data-form=datepicker]').datepicker();
});
</script>