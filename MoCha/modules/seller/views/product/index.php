<?php
$this->breadcrumbs=array(
    '英摩卡'=>array('/'),
    '卖家中心'=>array('.'),
    '产品中心',
);
$this->menu=array(
    array('label'=>'','itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-plus"></i> 新增产品',
        'linkOptions'=>array(
            'class'=>'iDialog btn btn-small btn-link',
            'data-uri'=>$this->createUrl("create"),
            'data-title'=>'新增产品',
            'data-id'=>'ProductForm',
            'data-ok'=>'创建',
            'data-callback'=>'oRefresh',
        )
    ),
    array('label'=>'','itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-plus"></i> 从淘宝导入',
        'linkOptions'=>array(
            'class'=>'iPrompt btn btn-small btn-link',
            'data-uri'=>$this->createUrl("importTaobao"),
            'data-title'=>'从淘宝导入',
            'data-msg'=>'请输入宝贝编号，多个用标点‘,’隔开',
            'data-id'=>'ImportForm',
            'data-ok'=>'导入',
            'data-callback'=>'oRefresh',
        )
    ),
    array('label'=>'','itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 分类管理',
        'url'=>array('type'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
);
$this->pageTitle='产品中心';
$files = array(
    'datatables/jquery.dataTables.min.js',
    'datatables/extras/ZeroClipboard.js',
    'datatables/extras/TableTools.min.js',
    'datatables/DT_bootstrap.js',
    'responsive-tables.js',
    'DT_bootstrap.css',
    'responsive-tables.css'
);
Ya::register($files);
?>
                        <!-- content-body -->
                        <div class="content-body">
                            <!--employee-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <!--department-->
                                    <div class="box-tab corner-all">
                                        <div class="tabbable tabs-left">
                                            <ul class="nav nav-tabs">
                                                <!--tab menus-->
                                                <li class="active"><a data-toggle="tab" href="#type-all" onclick="javascript:filtering('');">所有分类</a></li>
                                                <?php echo $types;?>
                                            </ul>
                                            <!-- widgets-tab-body -->
                                            <div class="tab-content">
                                                <table id="productList" class="iTablelist table table-hover table-bordered responsive" data-acturi="<?php echo $this->createUrl("aedit");?>">
                                                    <thead>
                                                        <tr>
                                                            <th><input class="iCheckAll" type="checkbox" name="checkall"></th>
                                                            <th>图片</th>
                                                            <th>货号/标题</th>
                                                            <th>标签</th>
                                                            <th>状态/推荐</th>
                                                            <th>售价/时价</th>
                                                            <th>进价</th>
                                                            <th>库存/次品</th>
                                                            <th>备注</th>
                                                            <th>创建/更新</th>
                                                            <th>操作</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="5" class="dataTables_empty">数据正在加载中，请稍候……</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div><!--/widgets-tab-body-->
                                        </div>
                                    </div>
                                    <!--/department-->
                                </div>
                            </div>
                            <!--/employee-->
                            
                        </div><!--/content-body -->
                    </div><!-- /content -->
                </div><!-- /span content -->

<clip:script>
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function() {
    // uniform
    $('[data-form=uniform]').uniform();
    $('.iTablelist').listTable();
    
    // datatables table tools
    $('#productList').dataTable({
        "sAjaxSource": "<?php echo $this->createUrl('list');?>",
        "aaSorting": [[4,'desc'], [9,'desc']],
        "aoColumnDefs": [
            {
                "mData": "id",
                "aTargets": [0],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<input class="iCheck" type="checkbox" name="icheck" value="'+data+'">';
                }
            },
            {
                "mData": "type_id",
                "aTargets": [1],
                "mRender": function(data, type, full) {
                    return '<img src="'+full.picture+'_sum.jpg" width="50">';
                }
            },
            {
                "mData": "name",
                "aTargets": [2],
                "mRender": function(data, type, full) {
                    return '<strong>'+data+'</strong><br /><small class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="title" data-tdtype="edit">'+full.title+'</small>';
                }
            },
            {
                "mData": "tag",
                "aTargets": [3],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="tag" data-tdtype="edit">'+data+'</span>';
                }
            },
            {
                "mData": "status",
                "aTargets": [4],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    var status=elite='';
                    if(data == 0){
                        status = 'disabled';
                    }else{
                        status = 'enabled';
                    }
                    if(full.is_elite == 0){
                        elite = 'disabled';
                    }else{
                        elite = 'enabled';
                    }
                    var html = '<img src="/asset/img/toggle_'+status+'.gif" data-value="'+data+'" data-field="is_elite" data-id="'+full.id+'" data-tdtype="toggle"><br />';
                    html += '<img src="/asset/img/toggle_'+elite+'.gif" data-value="'+full.is_elite+'" data-field="is_elite" data-id="'+full.id+'" data-tdtype="toggle">';
                    return html;
                }
            },
            {
                "mData": "selling_price",
                "aTargets": [5],
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="selling_price" data-tdtype="edit">'+data+'</span><br /><span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="current_price" data-tdtype="edit">'+full.current_price+'</span>';
                }
            },
            {
                "mData": "buiing_price",
                "aTargets": [6],
            },
            {
                "mData": "stock",
                "aTargets": [7],
                "mRender": function(data, type, full) {
                    return '<strong>'+data+'</strong><br /><small class="helper-font-small">'+full.inferior+'</small>';
                }
            },
            {
                "mData": "memo",
                "aTargets": [8],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="memo" data-tdtype="edit">'+data+'</span>';
                }
            },
            {
                "mData": "modified",
                "aTargets": [9],
                "mRender": function(data, type, full) {
                    return '<strong>'+full.created+'</strong><br /><strong>'+data+'</strong>';
                }
            },
            {
                "mData": "id",
                "aTargets": [10],
                "mRender": function(data, type, full) {
                    var html='<a href="javascript:;" class="iDialog btn btn-mini btn-danger" data-uri="<?php echo $this->createUrl("edit");?>/id/'+data+'" data-title="编辑 '+full.name+' " data-id="SupplyForm" data-callback="oRefresh" data-ok="更新">产品规格</a>';
                    html +='<a href="<?php echo $this->createUrl("view");?>/id/'+data+'" class="btn btn-mini" target="_blank">查看</a>';
                    html +='<a href="javascript:;" class="iDialog btn btn-mini btn-danger" data-uri="<?php echo $this->createUrl("edit");?>/id/'+data+'" data-title="编辑 '+full.name+' " data-id="ProductForm" data-callback="oRefresh" data-ok="更新">编辑</a>';
                    html +='<a href="javascript:;" class="iConfirm btn btn-mini" data-msg="确定要删除 '+full.name+' 吗？" data-uri="<?php echo $this->createUrl('delete');?>/id/'+data+'" data-ok="删除" data-acttype="ajax" data-callback="oRefresh">删除</a>';
                    return '<div class="btn-group pull-right">'+html+'</div>';
                }
            },
        ],
    });
});

/*
 *刷单
 */
function oRefresh(){
    $('#productList').dataTable().fnDraw();
}
            
/*
 *快速筛选
 */
function filtering(type) {
    var oTable = $('#productList').dataTable();
    if(type === '') {
        oTable.fnFilter('', 1);
    } else {
        oTable.fnFilter(type, 1);
    }
}
/*]]>*/
</script>
</clip:script>