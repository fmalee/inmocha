<?php $form=$this->beginWidget('ActiveForm', array('id'=>'ServiceForm')); ?>
<table class="table table-hover table-bordered">
    <tr>
        <th><?php echo $form->labelEx($service,'type_id',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->radioButtonGroup($service,'type_id',Type::Lists(Type::MODULE_FUND)); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($service,'status',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->radioButtonGroup($service,'status',Service::itemAlias('fundStatus')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($module,'money',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($module,'money', array('class'=>'input-small')); ?></td>
        <th><?php echo $form->labelEx($module,'flow',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->radioButtonGroup($module,'flow',ServiceFund::itemAlias('flow')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($module,'channel',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textField($module,'channel', array('class'=>'input-large')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($service,'memo',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textArea($service,'memo', array('row'=>'4','class'=>'input-large')); ?></td>
    </tr>
</table>
<?php $this->endWidget(); ?>