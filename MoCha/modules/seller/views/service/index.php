<?php
$this->breadcrumbs=array(
    '英摩卡'=>array('..'),
    '卖家中心'=>array('.'),
    '售后服务',
);
$this->menu=array(
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-cogs"></i> 添加售后 <i class="icofont-caret-down"></i>',
        'url'=>array('#'),
        'itemOptions'=>array('class'=>'btn-group'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link dropdown-toggle', 'data-toggle'=>'dropdown'),
        'items'=>array(
            array('label'=>'New Arrivals', 'url'=>array('product/new', 'tag'=>'new'), 'linkOptions'=>array('class'=>'scroll')),
            array('itemOptions'=>array('class'=>'divider')),
            array('label'=>'Most Popular', 'url'=>array('product/index', 'tag'=>'popular'), 'linkOptions'=>array('class'=>'scroll')),
    )),
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 订单管理',
        'url'=>array('/seller/trade'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 退款管理',
        'url'=>array('/seller/refund'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
);
$this->pageTitle='售后服务';
Ya::registerScript('js','index.js');
?>
                        <!-- content-body -->
                        <div class="content-body">
                            <!-- tables -->
                            <div class="box-tab corner-all">
                                <div class="box-header corner-top">
                                    <div class="header-control pull-right">
                                        <a data-box="collapse"><i class="icofont-caret-up"></i></a>
                                    </div>
                                    <?php $this->widget('zii.widgets.CMenu',array(
                                        'items'=>array(
                                            array(
                                                'label'=>'所有记录',
                                                'url'=>array('/seller/service'),
                                                'active'=>(!isset($this->actionParams['status']) && !isset($this->actionParams['refund']) && !isset($this->actionParams['seller_rate'])) ? true : false,
                                            ),
                                            array(
                                                'label'=>Service::itemAlias('status', Service::STATUS_FOLLOW),
                                                'url'=>array('/seller/service', 'status'=>Service::STATUS_FOLLOW),
                                                'active'=>($this->actionParams['status'] === Service::STATUS_FOLLOW) ? true : false,
                                            ),
                                            array(
                                                'label'=>Service::itemAlias('status', Service::STATUS_RETURN),
                                                'url'=>array('/seller/service', 'status'=>Service::STATUS_RETURN),
                                                'active'=>($this->actionParams['status'] == Service::STATUS_RETURN) ? true : false,
                                            ),
                                            array(
                                                'label'=>Service::itemAlias('status', Service::STATUS_RECIEVE),
                                                'url'=>array('/seller/service', 'status'=>Service::STATUS_RECIEVE),
                                                'active'=>($this->actionParams['status'] == Service::STATUS_RECIEVE) ? true : false,
                                            ),
                                            array(
                                                'label'=>Service::itemAlias('status', Service::STATUS_SEND),
                                                'url'=>array('/seller/service', 'status'=>Service::STATUS_SEND),
                                                'active'=>($this->actionParams['status'] == Service::STATUS_SEND) ? true : false,
                                            ),
                                            array(
                                                'label'=>Service::itemAlias('status', Service::STATUS_FAIL),
                                                'url'=>array('/seller/service', 'status'=>Service::STATUS_FAIL),
                                                'active'=>($this->actionParams['status'] == Service::STATUS_FAIL) ? true : false,
                                            ),
                                            array(
                                                'label'=>Service::itemAlias('status', Service::STATUS_FINISH),
                                                'url'=>array('/seller/service', 'status'=>Service::STATUS_FINISH),
                                                'active'=>($this->actionParams['status'] == Service::STATUS_FINISH) ? true : false,
                                            )
                                        ),
                                        'htmlOptions'=>array('class'=>'nav nav-tabs'),
                                    )); ?>
                                </div>
                                <div class="box-body">
                                     <table id="ServiceList" class="iTablelist table table-striped responsive" data-acturi="<?php echo $this->createUrl("aedit");?>">
                                        <thead>
                                            <tr>
                                                <th><input class="iCheckAll" type="checkbox" name="checkall"></th>
                                                <th>订单编号</th>
                                                <th>买家</th>
                                                <th>收件人</th>
                                                <th>类别</th>
                                                <th>状态</th>
                                                <th>原因</th>
                                                <th>备注</th>
                                                <th>创建日期</th>
                                            </tr>
                                        </thead>
                                            <?php $this->widget('YListView', array(
                                                'dataProvider'=>$dataProvider,
                                                'id'=>'service',
                                                'itemView'=>'_list',
                                                'itemsTagName'=>'',
                                                'tagName'=>'tbody',
                                                'loadingCssClass'=>'',
                                                'summaryCssClass'=>'pagination pull-left',
                                                'pagerCssClass'=>'pagination pagination-right',
                                                'template'=>"{items}\n<tr><td colspan=\"9\">{summary}{pager}</td></tr>",
                                            )); ?>
                                     </table>
                                </div>
                            </div><!-- /tab stat -->
                            
                            <!--/dashboar-->
                        </div><!--/content-body -->
                    </div><!-- /content -->
                </div><!-- /span content -->
