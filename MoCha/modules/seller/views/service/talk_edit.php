<?php $form=$this->beginWidget('ActiveForm', array('id'=>'ServiceForm')); ?>
<table class="table table-hover table-bordered">
    <tr>
        <th><?php echo $form->labelEx($service,'type_id',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->radioButtonGroup($service,'type_id',Type::Lists(Type::MODULE_TALK)); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($service,'status',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->radioButtonGroup($service,'status',Service::itemAlias('talkStatus')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($service,'memo',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textArea($service,'memo', array('row'=>'4','class'=>'input-large')); ?></td>
    </tr>
</table>
<?php $this->endWidget(); ?>