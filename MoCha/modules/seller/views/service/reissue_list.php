
                                            <tr id="service_<?php echo $data->id;?>">
                                                <td><input class="iCheck" type="checkbox" name="icheck" value="<?php echo $data->id;?>"></td>
                                                <td><?php echo $data->trade_id;?></td>
                                                <td><?php echo $data->trade->buyer_nick;?><span class="iWangwang" data-nick="<?php echo $data->trade->buyer_nick;?>" data-type="2"><?php echo $data->trade->buyer_nick;?></span></td>
                                                <td><?php echo $data->trade->receiver_name;?></td>
                                                <td><?php echo $data->statusName;?></td>
                                                <td><?php echo $data->typeName;?></td>
                                                <td>
                                                <?php foreach ($data->reissue as $reissue):?>
                                                    <p>
                                                        <span class="label label-inverse"><?php echo $reissue->product;?></span> <span class="label"><?php echo $reissue->sku;?></span><span class="badge"> <?php echo $reissue->quantity;?></span>
                                                    </p>
                                                <?php endforeach;?>
                                                </td>
                                                <td>
                                                    <?php echo $data->reissue[0];?>
                                                </td>
                                                <td><?php echo Yhtml::AjaxEdit($data, 'memo');?></td>
                                                <td>
                                                    <div class="iShow"><span class="btn btn-mini"><?php echo date('Y-m-d H:i:s',$data->created);?></span></div>
                                                    <div class="btn-group iHidden" style="display: none;">
                                                        <a href="javascript:;" class="iDialog btn btn-mini btn-danger" data-uri="<?php echo $this->createUrl('edit',array('id'=>$data->id, 'model'=>'fund'));?>" data-title="编辑 <?php echo $data->trade_id;?>" data-id="ServiceForm" data-callback="oRefresh" data-ok="更新">编辑</a>
                                                        <?php if($data->status != Service::STATUS_FINISH):?><a href="javascript:;" class="iConfirm btn btn-mini" data-msg="确定要删除 <?php echo $data->trade_id;?> 吗？" data-uri="<?php echo $this->createUrl('delete',array('id'=>$data->id));?>" data-ok="删除" data-acttype="ajax" data-callback="oDelete('<?php echo $data->id;?>');">删除</a><?php endif;?>
                                                    </div>
                                                </td>
                                            </tr>
