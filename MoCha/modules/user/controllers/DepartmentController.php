<?php

class DepartmentController extends Controller
{
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index', 'list', 'type', 'create', 'edit', 'aedit', 'delete', 'childs'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * 列表
	 */
	public function actionIndex()
	{
		$tree = new Tree();
        $tree->icon = array('│ ','├─ ','└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $array = array();

		$criteria=new CDbCriteria(array(
			'scopes'=>array('belong','notsafe'),
			'limit'=>100,
			'order'=>'list_order ASC',
		));
		$departments = Department::model()->findAll($criteria);

		
        foreach($departments as $r) {
        	$department['id'] = $r->id;
        	$department['name'] = $r->name;
        	$department['parent_id'] = $r->parent_id;
        	$department['list_order'] = $r->list_order;
        	$department['memo'] = $r->memo;
        	$department['created'] = date('Y-m-d H:i:s', $r->created);
        	$department['modified'] = date('Y-m-d H:i:s', $r->modified);
        	$department['str_status'] = '<img data-tdtype="toggle" data-id="'.$r->id.'" data-field="status" data-value="'.$r->status.'" src="/asset/img/toggle_' . ($r->status == 0 ? 'disabled' : 'enabled') . '.gif" />';
        	$department['str_manage'] = '<div class="btn-group pull-right">
        			<a href="javascript:;" class="iDialog btn btn-mini btn-danger" data-uri="'.$this->createUrl("edit",array("id"=>$r->id)).'" data-title="编辑'.$r->name.'" data-id="DepartmentForm" data-ok="更新">编辑</a>
        			<a href="javascript:;" class="iConfirm btn btn-mini" data-msg="确定要删除'.$r->name.'吗？" data-uri="'.$this->createUrl("delete",array("id"=>$r->id)).'" data-ok="删除" data-acttype="ajax">删除</a>
        			</div>';
            $array[] = $department;
        }
        $str  = "
        	<tr id='\$id'>
        		<td><input class='iCheck' data-form='uniform' type='checkbox' name='icheck' value='\$id'></td>
        		<td>\$spacer<span class='ajaxedit' data-id='\$id' data-field='name' data-tdtype='edit'>\$name</span></td>
        		<td>\$str_status</td>
        		<td><span class='ajaxedit' data-id='\$id' data-field='list_order' data-tdtype='edit'>\$list_order</span></td>
        		<td align='center'>\$created</td>
        		<td align='center'>\$modified</td>
        		<td><span class='ajaxedit' data-id='\$id' data-field='memo' data-tdtype='edit'>\$memo</span></td>
        		<td>\$str_manage</td>
        	</tr>";
        $tree->init($array);
        $list = $tree->get_tree(0, $str);


        $this->render('index',array('list'=>$list));
	}

	
	/**
	 * 获取紧接着的下一级分类ID
	 */
	public function actionChilds($id = 0)
	{
		$criteria=new CDbCriteria(array(
			'scopes'=>array('belong','simple'),
			'limit'=>100,
			'order'=>'list_order ASC',
			'condition'=>'department.parent_id=:parent_id',
			'params'=>array(':parent_id'=>$id),
		));

		$departments = Department::model()->findAll($criteria);

		if($departments)
		{
			foreach ($departments as $key=>$value) {
				$childs[$key]['id'] = $value->id;
				$childs[$key]['name'] = $value->name;
			}
			$this->ajaxReturn($childs);
		}
		else
			$this->ajaxReturn(Ya::t('Operation Success'),0);
	}
}