<?php
$this->breadcrumbs=array(
    '会员中心'=>array('..'),
    '员工管理',
);
$this->menu=array(
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-plus"></i> 新增员工',
        'linkOptions'=>array(
            'class'=>'iDialog btn btn-small btn-link',
            'data-uri'=>$this->createUrl("create"),
            'data-title'=>'新增员工',
            'data-id'=>'EmployeeForm',
            'data-ok'=>'创建',
            'data-callback'=>'oRefresh',
        )
    ),
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 部门管理',
        'url'=>array('department'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
);
$this->pageTitle='员工管理';
$files = array(
    'datatables/jquery.dataTables.min.js',
    'datatables/extras/ZeroClipboard.js',
    'datatables/extras/TableTools.min.js',
    'datatables/DT_bootstrap.js',
    'responsive-tables.js',
    'DT_bootstrap.css',
    'responsive-tables.css'
);
Ya::register($files);
Ya::registerScript('js','index.js');
?>
                        <!-- content-body -->
                        <div class="content-body">
                            <!--employee-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <!--department-->
                                    <div class="box-tab corner-all">
                                        <div class="tabbable tabs-left">
                                            <ul class="nav nav-tabs">
                                                <!--tab menus-->
                                                <li class="active"><a data-toggle="tab" href="#depmt" onclick="javascript:filtering('');">所有部门</a></li>
                                                <?php echo $departments;?>
                                            </ul>
                                            <!-- widgets-tab-body -->
                                            <div class="tab-content">
                                                <table id="employeeList" class="iTablelist table table-hover table-bordered responsive" data-acturi="<?php echo $this->createUrl("aedit");?>">
                                                    <thead>
                                                        <tr>
                                                            <th><input class="iCheckAll" type="checkbox" name="checkall"></th>
                                                            <th>姓名</th>
                                                            <th>花名</th>
                                                            <th>部门</th>
                                                            <th>性别</th>
                                                            <th>工号</th>
                                                            <th>入职时间</th>
                                                            <th>状态</th>
                                                            <th>操作</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="5" class="dataTables_empty">数据正在加载中，请稍候……</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div><!--/widgets-tab-body-->
                                        </div>
                                    </div>
                                    <!--/department-->
                                </div>
                            </div>
                            <!--/employee-->
                            
                        </div><!--/content-body -->
                    </div><!-- /content -->
                </div><!-- /span content -->
