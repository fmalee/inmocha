<?php
$this->breadcrumbs=array(
    '会员中心'=>array('..'),
    '部门管理',
);
$this->menu=array(
    array('itemOptions'=>array('class'=>'divider')),
    array(
        'label'=>'<i class="icofont-tasks"></i> 员工管理',
        'url'=>array('./employee'),
        'linkOptions'=>array('class'=>'btn btn-small btn-link')
    ),
);
$this->pageTitle='部门管理';
Ya::registerScript(
    'init',
    "$(document).ready(function() {
        // uniform
        $('[data-form=uniform]').uniform();
        $('.iTablelist').listTable();
    });",
    true
);
?>
                        <!-- content-body -->
                        <div class="content-body">
                            <!-- tab stat -->
                            <div class="box corner-all">
                                <div class="box-header grd-white color-silver-dark corner-top">
                                    <div class="header-control">
                                        <span>部门管理</span>
                                    </div>
                                    <span>
                                        <button class="iDialog btn" data-uri="<?php echo $this->createUrl('/type/create', array('module'=>'department'));?>" data-title="新增部门" data-id="TypeForm" data-ok="添加"><i class="icon-plus"></i> 新增部门</button>
                                    </span>
                                </div>
                                <div class="box-body">
                                    <table id="departmentList" class="iTablelist table table-hover responsive" data-acturi="<?php echo $this->createUrl("/type/aedit", array('module'=>'department'));?>">
                                        <thead>
                                            <tr>
                                                <th><input class="iCheckAll" data-form="uniform" type="checkbox" name="checkall"></th>
                                                <th>部门</th>
                                                <th>状态</th>
                                                <th>排序</th>
                                                <th>创建时间</th>
                                                <th>修改时间</th>
                                                <th>备注</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php echo $list;?>
                                    </tbody>
                                </table>
                                </div>
                            </div><!-- /tab stat -->
                        </div><!--/content-body -->
                    </div><!-- /content -->
                </div><!-- /span content -->
