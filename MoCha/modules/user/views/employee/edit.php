<?php $form=$this->beginWidget('ActiveForm', array('id'=>'EmployeeForm')); ?>
<table class="table table-hover table-bordered">
    <tr>
        <th><?php echo $form->labelEx($model,'department_id',array('class'=>'control-label')); ?></th>
        <td colspan="3">
            <?php echo $form->dropDownList($model,'department_id', array(), array(
                'class'=>'input-medium iSelect',
                'data-uri'=>$this->createUrl('/type/childs',array('module'=>'department')),
                'data-pid'=>'0',
                'data-selected'=>Type::getArrParent($model->department_id)
            )); ?>
            <?php echo $form->hiddenField($model,'department_id', array('id'=>'iCatid'));?>
        </td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'name', array('class'=>'input-medium')); ?></td>
        <th><?php echo $form->labelEx($model,'nick',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'nick', array('class'=>'input-medium')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'sex',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->radioButtonGroup($model,'sex',Employee::itemAlias('sex')); ?></td>
        <th><?php echo $form->labelEx($model,'status',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->radioButtonGroup($model,'status',Employee::itemAlias('status')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'label',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'label', array('class'=>'input-medium')); ?></td>
        <th><?php echo $form->labelEx($model,'duty',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'duty', array('class'=>'input-medium')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'id_card',array('class'=>'control-label')); ?></th>
        <td colspan="3"><?php echo $form->textField($model,'id_card', array('class'=>'input-large')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'phone',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'phone', array('class'=>'input-medium')); ?></td>
        <th><?php echo $form->labelEx($model,'email',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'email', array('class'=>'input-medium')); ?></td>
    </tr>
    <tr>
        <th><?php echo $form->labelEx($model,'entry_date',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textField($model,'entry_date', array('class'=>'input-small','data-form'=>'datepicker')); ?></td>
        <th><?php echo $form->labelEx($model,'memo',array('class'=>'control-label')); ?></th>
        <td><?php echo $form->textArea($model,'memo', array('row'=>'3','class'=>'input-small')); ?></td>
    </tr>
</table>
<?php $this->endWidget(); ?>

<script>
$(function(){
    //分类联动
    $('.iSelect').cate_select();
    // datepicker
    $('[data-form=datepicker]').datepicker();
});
</script>