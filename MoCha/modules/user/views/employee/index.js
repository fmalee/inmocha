$(document).ready(function() {
    // uniform
    $('[data-form=uniform]').uniform();
    $('.iTablelist').listTable();
    
    // datatables table tools
    var oTable = $('#employeeList').dataTable({
        "sAjaxSource": "<?php echo $this->createUrl('list');?>",
        "aoColumnDefs": [
            {
                "mData": "id",
                "aTargets": [0],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<input class="iCheck" type="checkbox" name="icheck" value="'+data+'">';
                }
            },
            {
                "mData": "name",
                "aTargets": [1]
            },
            {
                "mData": "nick",
                "aTargets": [2],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="nick" data-tdtype="edit">'+data+'</span>';
                }
            },
            {
                "mData": "department_id",
                "aTargets": [3],
                "bSortable": false,
            },
            {
                "mData": "sex",
                "aTargets": [4],
                "bSortable": false
            },
            {
                "mData": "label",
                "aTargets": [5],
                "mRender": function(data, type, full) {
                    return '<span class="ajaxedit" style="color:" data-id="'+full.id+'" data-field="label" data-tdtype="edit">'+data+'</span>';
                }
            },
            {
                "mData": "entry_date",
                "aTargets": [6],
                "bSortable": false,
            },
            {
                "mData": "status",
                "aTargets": [7],
                "mRender": function(data, type, full) {
                    var img='';
                    if(data == 0){
                        img = 'disabled';
                    }else{
                        img = 'enabled';
                    }
                    return '<img src="/asset/img/toggle_'+img+'.gif" data-value="'+data+'" data-field="status" data-id="'+full.id+'" data-tdtype="toggle">';
                }
            },
            {
                "mData": "id",
                "aTargets": [8],
                "bSortable": false,
                "mRender": function(data, type, full) {
                    var html='<a href="javascript:;" class="iDialog btn btn-mini btn-danger" data-uri="<?php echo $this->createUrl("edit");?>/id/'+data+'" data-title="编辑'+full.name+'" data-id="EmployeeForm" data-callback="oRefresh" data-ok="更新">编辑</a>';
                    html +='<a href="javascript:;" class="iConfirm btn btn-mini" data-msg="确定要删除'+full.name+'吗？" data-uri="<?php echo $this->createUrl('delete');?>/id/'+data+'" data-ok="删除" data-acttype="ajax" data-callback="oRefresh">删除</a>';
                    return '<div class="btn-group pull-right">'+html+'</div>';
                }
            }
        ],
    });
});

/*
 *刷单表格
 */
function oRefresh(){
    $('#employeeList').dataTable().fnDraw();
}

/*
 *快速筛选
 */
function filtering(department) {
    var oTable = $('#employeeList').dataTable();
    if(department === '') {
        oTable.fnFilter('', 3);
    } else {
        oTable.fnFilter(department, 3);
    }
}