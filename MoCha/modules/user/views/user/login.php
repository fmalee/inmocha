<?php
$this->pageTitle='会员登录';
Ya::register(array('jquery.uniform.js','jquery.metadata.js','jquery.validate.js','uniform.default.css'));
Ya::registerScript(
	'validate',
	"$(document).ready(function() {
                // try your js
				// uniform
                $('[data-form=uniform]').uniform();
                // validate
                $('#login-form').validate();
                $('#form-recover').validate();
	})",
    true
);

?>
<?php echo Yii::app()->baseUrl;?>
        <!-- section content -->
        <section class="section">
            <div class="container">
                <div class="signin-form row-fluid">
                    <!--Sign In-->
                    <div class="span4 offset4">
                        <div class="box corner-all">
                            <div class="box-header grd-teal color-white corner-top">
                                <span><?php echo UserModule::t('Login');?></span>
                            </div>
                            <div class="box-body bg-white form-inline">
                            	<?php $form=$this->beginWidget('CActiveForm', array('id'=>'login-form')); ?>
                                    <div class="control-group">
                                        <?php echo $form->labelEx($model,'username',array('class'=>'control-label')); ?>
                                        <div class="controls">
                                        	<?php echo $form->textField($model,'username', array(
														'id'=>'login_username',
														'class'=>'input-block-level',
														'placeholder'=>UserModule::t('Please enter field username'),
														'autocomplete'=>'off',
														'data-validate'=>'{required: true, messages:{required:"'.UserModule::t('Please enter field username').'"}}'
														)); ?>
                                        </div>
                                        <?php echo $form->error($model,'username'); ?>
                                    </div>
                                    <div class="control-group">
                                        <?php echo $form->labelEx($model,'password',array('class'=>'control-label')); ?>
                                        <div class="controls">
                                            <?php echo $form->passwordField($model,'password', array(
														'id'=>'login_password',
														'class'=>'input-block-level',
														'autocomplete'=>'off',
														'placeholder'=>UserModule::t('Please enter field password'),
														'data-validate'=>'{required: true, messages:{required:"'.UserModule::t('Please enter field password').'"}}'
														)); ?>
                                        </div>
                                        <?php echo $form->error($model,'password'); ?>
                                    </div>
                                    <div class="control-group">
                                        <div class="pull-right helper-font-24">
                                            <a href="#" rel="tooltip-left" title="Sign in using twitter account"><i class="icofont-twitter-sign color-blue"></i></a>
                                            <a href="#" rel="tooltip-left" title="Sign in using facebook account"><i class="icofont-facebook-sign color-sky"></i></a>
                                        </div>
                                        <?php echo $form->checkBox($model,'rememberMe',array('class'=>'checkbox','data-form'=>'uniform')); ?>
										<?php echo $form->label($model,'rememberMe'); ?>
                                    </div>
                                    <div class="form-actions">
                                    	<?php echo CHtml::submitButton(UserModule::t('Login'),array(
														'class'=>'btn btn-block btn-large btn-primary',
														)); ?>
                                        <p class="recover-account"><a href="#modal-recover" class="link" data-toggle="modal"><?php echo UserModule::t('Lost Password?');?></a></p>
                                    </div>
                                <?php $this->endWidget(); ?>
                            </div>
                        </div>
                    </div><!--/Sign In-->
                </div><!-- /row -->
            </div><!-- /container -->
            
            <!-- modal recover -->
            <div id="modal-recover" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modal-recoverLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 id="modal-recoverLabel"><?php echo UserModule::t('Reset password');?> <small><?php echo UserModule::t('username or email');?></small></h3>
                </div>
                <div class="modal-body">
                    <form id="form-recover" method="post">
                        <div class="control-group">
                            <div class="controls">
                                <input type="text" data-validate="{required: true, email:true, messages:{required:'<?php echo UserModule::t('Please enter field email');?>', email:'<?php echo UserModule::t('Please enter a valid email address');?>'}}" name="recover" />
                                <p class="help-block helper-font-small"><?php echo UserModule::t('Enter your username or email address and we will send you a link to reset your password.');?></p>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo UserModule::t('Close');?></button>
                    <input type="submit" form="form-recover" class="btn btn-primary" value="<?php echo UserModule::t('Send reset link');?>" >
                </div>
            </div><!-- /modal recover-->
        </section>
        