<?php

class Employee extends CActiveRecord
{	
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;

	const SEX_MALE=0;
	const SEX_FEMALE=1;

	/**
	 * Employee表的字段:
	 * @var integer $id
	 * @var integer $department_id
	 * @var integer $user_id
	 * @var string $nick
	 * @var integer $status
	 * @var integer $sex
	 * @var string $label
	 * @var string $name 
	 * @var integer $duty
	 * @var string $id_card
     * @var string $email
     * @var integer $phone
     * @var integer $fixed_phone
     * @var integer $entry_date
     * @var string $memo
     * @var integer $created
     * @var integer $modified
	 */
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{employee}}';
	}

	public function rules()
	{
		return array(
			array('department_id, name, nick, status, phone', 'required'),
			array('department_id, status, sex, phone, fixed_phone', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>32),
			array('name', 'unique', 'message'=>'已经存在相同名字的记录'),
			array('email', 'email', 'message'=>'必须为电子邮箱', 'pattern'=>'/[a-z]/i'),
			array('nick, label, duty, id_card', 'length', 'max'=>20),
			array('memo', 'length', 'max'=>255),
		);
	}

	public function relations()
	{
        return array(
			'department' => array(self::BELONGS_TO, 'Type', '', 'on'=>'employee.department_id=Type.id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id'=>Ya::t("Employee ID"),
			'department_id'=>Ya::t("Department"),
			'status'=>Ya::t("Status"),
			'name'=>Ya::t("True Name"),
			'nick'=>Ya::t("Nick"),
			'label'=>Ya::t("Employee Label"),
			'duty'=>Ya::t("Duty"),
			'sex'=>Ya::t("Sex"),
			'memo'=>Ya::t("Memo"),
			'email'=>Ya::t("E-mail"),
			'phone'=>Ya::t("Phone"),
			'entry_date'=>Ya::t("Entry Date"),
			'id_card'=>Ya::t("ID Card"),
			'created' => Ya::t("Created"),
			'modified' => Ya::t("Modified"),
		);
	}
	
	public function scopes()
    {
        return array(
            'belong'=>array(
            	'condition'=>'employee.user_id=:user_id',
            	'params'=>array(':user_id'=>Yii::app()->user->id),
            ),
            'departemnt'=>array(
            	'condition'=>'departemnt.department_id=:department_id',
            	'params'=>array(':department_id'=>$this->department_id),
            ),
            'notsafe'=>array(
            	'alias'=>'employee',
            	'select' => 'id, department_id, name, status, sex, phone, label, nick, duty, created, modified',
            ),
            'forcheck'=>array(
            	'alias'=>'employee',
            	'select' => 'id',
            ),
        );
    }
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->created=time();
				$this->user_id=Yii::app()->user->id;
			}
			$this->modified=time();
				
			return true;
		}
		else
			return false;
	}

	public static function itemAlias($type,$code=NULL)
	{
		$_items = array(
			'status' => array(
				self::STATUS_ACTIVE => Ya::t('Enabled'),
				self::STATUS_NOACTIVE => Ya::t('Disabled'),
			),
			'sex' => array(
				self::SEX_MALE => Ya::t('Male'),
				self::SEX_FEMALE => Ya::t('Female'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

    public function formatHtml($model)
    {
    	if(isset($model['department_id'])) $model['department_id'] = Type::Lists('department', $model['department_id']);
    	if(isset($model['sex'])) $model['sex'] = self::itemAlias('sex', $model['sex']);
    	if(isset($model['entry_date'])) $model['entry_date'] = date('Y-m-d', $model['entry_date']);
    	if(isset($model['created'])) $model['created'] = date('Y-m-d', $model['created']);
    	if(isset($model['modified'])) $model['modified'] = date('Y-m-d', $model['modified']);

    	return $model;
    }
}