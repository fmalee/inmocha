<?php

class Times extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{times}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			//array('username', 'required'),
			array('username, ip', 'safe', 'on'=>'search'),
		);
	}

	public function scopes()
    {
        return array(
            'ip'=>array(
                'condition'=>'ip="'.Yii::app()->request->userHostAddress.'"',
                'select' => 'username, ip, logintime, isadmin, times',
            ),
            'notsafe'=>array(
            	'select' => 'username, ip, logintime, isadmin, times',
            ),
        );
    }
    
	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->times = 1;
				$this->logintime = time();
			}
			$this->ip=Yii::app()->request->userHostAddress;
			return true;
		}
		else
			return false;
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		parent::afterDelete();
		$this->deleteAll('logintime<:time',array(':time'=>time() - Yii::app()->getModule('member')->errorTime)); //TODO 删除已过锁定时间的
	}

	/**
	 * 验证输错次数
	 * @param string username
	 * @return 是否超出
	 */
	public function validateTimes($username)
	{
		$times = Times::model()->ip()->findByAttributes(array('username'=>$username));
		if($times)
		{
			if(($times->logintime + Yii::app()->getModule('member')->errorTime) < time())
			{
				$times->logintime = time();
				$times->times = 1;
			}
			else if($times->times < Yii::app()->getModule('member')->errorLock)
			{
				$times->times ++;
			}
			else
				return true; //超过次数
		}
		else
		{
			$times=new Times;
		}
		$times->username = $username;
		$times->save();

		return false;
	}
}