<?php
/*
 * 项目扩展函数库
 */

/**
 * 生成上传附件验证
 * @param $args   参数
 */
function upload_key($args) {
	$auth_key = md5(C("AUTHCODE") . $_SERVER['HTTP_USER_AGENT']);
	$authkey = md5($args . $auth_key);
	return $authkey;
}

/*
 * 产生随机字符串 
 * 产生一个指定长度的随机字符串,并返回给用户 
 * @access public 
 * @param int $len 产生字符串的位数 
 * @return string 
 */

function genRandomString($len = 6) {
	$chars = array(
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
		"l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
		"w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G",
		"H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
		"S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2",
		"3", "4", "5", "6", "7", "8", "9"
	);
	$charsLen = count($chars) - 1;
	shuffle($chars);    // 将数组打乱 
	$output = "";
	for ($i = 0; $i < $len; $i++) {
		$output .= $chars[mt_rand(0, $charsLen)];
	}
	return $output;
}

/**
 * 去一个二维数组中的每个数组的固定的键知道的值来形成一个新的一维数组
 * @param $pArray 一个二维数组
 * @param $pKey 数组的键的名称
 * @return 返回新的一维数组
 */
function getSubByKey($pArray, $pKey = "", $pCondition = "") {
	$result = array();
	foreach ($pArray as $temp_array) {
		if (is_object($temp_array)) {
			$temp_array = (array) $temp_array;
		}
		if (("" != $pCondition && $temp_array[$pCondition[0]] == $pCondition[1]) || "" == $pCondition) {
			$result[] = ("" == $pKey) ? $temp_array : isset($temp_array[$pKey]) ? $temp_array[$pKey] : "";
		}
	}
	return $result;
}

/**
 * 字符截取 支持UTF8/GBK
 * @param $string
 * @param $length
 * @param $dot
 */
function str_cut($string, $length, $dot = '...') {
	$strlen = strlen($string);
	if ($strlen <= $length)
		return $string;
	$string = str_replace(array(' ', '&nbsp;', '&amp;', '&quot;', '&#039;', '&ldquo;', '&rdquo;', '&mdash;', '&lt;', '&gt;', '&middot;', '&hellip;'), array('∵', ' ', '&', '"', "'", '“', '”', '—', '<', '>', '·', '…'), $string);
	$strcut = '';
	if (strtolower(C("DEFAULT_CHARSET")) == 'utf-8') {
		$length = intval($length - strlen($dot) - $length / 3);
		$n = $tn = $noc = 0;
		while ($n < strlen($string)) {
			$t = ord($string[$n]);
			if ($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
				$tn = 1;
				$n++;
				$noc++;
			} elseif (194 <= $t && $t <= 223) {
				$tn = 2;
				$n += 2;
				$noc += 2;
			} elseif (224 <= $t && $t <= 239) {
				$tn = 3;
				$n += 3;
				$noc += 2;
			} elseif (240 <= $t && $t <= 247) {
				$tn = 4;
				$n += 4;
				$noc += 2;
			} elseif (248 <= $t && $t <= 251) {
				$tn = 5;
				$n += 5;
				$noc += 2;
			} elseif ($t == 252 || $t == 253) {
				$tn = 6;
				$n += 6;
				$noc += 2;
			} else {
				$n++;
			}
			if ($noc >= $length) {
				break;
			}
		}
		if ($noc > $length) {
			$n -= $tn;
		}
		$strcut = substr($string, 0, $n);
		$strcut = str_replace(array('∵', '&', '"', "'", '“', '”', '—', '<', '>', '·', '…'), array(' ', '&amp;', '&quot;', '&#039;', '&ldquo;', '&rdquo;', '&mdash;', '&lt;', '&gt;', '&middot;', '&hellip;'), $strcut);
	} else {
		$dotlen = strlen($dot);
		$maxi = $length - $dotlen - 1;
		$current_str = '';
		$search_arr = array('&', ' ', '"', "'", '“', '”', '—', '<', '>', '·', '…', '∵');
		$replace_arr = array('&amp;', '&nbsp;', '&quot;', '&#039;', '&ldquo;', '&rdquo;', '&mdash;', '&lt;', '&gt;', '&middot;', '&hellip;', ' ');
		$search_flip = array_flip($search_arr);
		for ($i = 0; $i < $maxi; $i++) {
			$current_str = ord($string[$i]) > 127 ? $string[$i] . $string[++$i] : $string[$i];
			if (in_array($current_str, $search_arr)) {
				$key = $search_flip[$current_str];
				$current_str = str_replace($search_arr[$key], $replace_arr[$key], $current_str);
			}
			$strcut .= $current_str;
		}
	}
	return $strcut . $dot;
}




/**
 * 安全过滤函数
 *
 * @param $string
 * @return string
 */
function safe_replace($string) {
	$string = str_replace('%20', '', $string);
	$string = str_replace('%27', '', $string);
	$string = str_replace('%2527', '', $string);
	$string = str_replace('*', '', $string);
	$string = str_replace('"', '&quot;', $string);
	$string = str_replace("'", '', $string);
	$string = str_replace('"', '', $string);
	$string = str_replace(';', '', $string);
	$string = str_replace('<', '&lt;', $string);
	$string = str_replace('>', '&gt;', $string);
	$string = str_replace("{", '', $string);
	$string = str_replace('}', '', $string);
	return $string;
}

/**
 * 取得URL地址中域名部分
 * @param type $url 
 * @return \url 返回域名
 */
function urlDomain($url) {
	if (preg_match_all('#https?://(.*?)($|/)#m', $url, $Domain)) {
		return $Domain[0][0];
	}
	return false;
}

/**
 * 判断字段长度
 * @param str 输入字符
 * @param $len 基准长度
 */
function str_length($str, $len = "") {
	$i = 0;
	if (empty ( $str ))
	return 0;
	$detect = mb_detect_encoding ( $str );
	$iconv = iconv ( $detect, "GB2312//IGNORE", $str );
	$lenth = strlen ( $iconv );
	while ( $i < $lenth ) {
	if (preg_match ( "/^[" . chr ( 0xa1 ) . "-" . chr ( 0xff ) . "]+$/", $iconv [$i] )) {
		$i += 2;
	} else {
		$i += 1;
	}
	}
	if ($len)
	$i = intval ( ($len - $i) / 2 );
	return $i;
}

// 旺旺状态
function msg_path($nick, $type = 0) {
	$s = $type ? 2 : 1;
	$msg = '<a target="_blank" href="http://amos.im.alisoft.com/msg.aw?v=2&uid=' . $nick . '&site=cntaobao&s=1&charset=utf-8" ><img border="0" src="http://amos.im.alisoft.com/online.aw?v=2&uid=' . urlencode ( $nick ) . '&site=cntaobao&s=' . $s . '&charset=utf-8" alt="联系' . $nick . '" /></a>';
	return $msg;
}


// 简化PROPS
function split_prop($properties, $symbol = " ") {
	$properties = explode ( ";", $properties );
	$propertie = array ();
	foreach ( $properties as $v ) {
	$v = explode ( ":", $v );
	$propertie [] = $v [1];
	}
	return implode ( $symbol, $propertie );
}

/**
 * 字符转换成布尔值
 */
function str2bool($str) {
	$str = (string)$str;
	if (($str == '1') || ($str == 'true')) {
	return 1;
	} elseif (!$str || ($str == 'false')) {
	return 0;
	}
	return $str;
}


/**
 * 布尔值转换成字符
 */
function bool2str($bool) {
	$str = intval(trim($bool));
	if ($bool == 1) return 'true';
	return 'false';
}

/**
 * URl相关信息
 * 'scheme' => 'http',
 * 'host' => '127.0.0.1',
 * 'path' => '/index.php',
 * 'query' => 'm=taobao&c=stream&a=asyn',
 * 'port' => '80',
 * 'ip' => '127.0.0.1',
 * 'request' => '/index.php?m=taobao&c=stream&a=asyn',
 */
function urlinfo($url) {
	if (!url) return false;
	$url_arr  = parse_url($url);
	$url_arr["path"]    = empty($url_arr["path"]) ? "/"  : $url_arr["path"];
	$url_arr["port"]    = empty($url_arr["port"]) ? "80" : $url_arr["port"];
	$url_arr["ip"]      = gethostbyname($url_arr["host"]);
	$url_arr["request"] = $url_arr["path"]
	. (empty($url_arr["query"])    ? "" : "?" . $url_arr["query"])
	. (empty($url_arr["fragment"]) ? "" : "#" . $url_arr["fragment"]);
	
	return $url_arr;
}

//发包函数
function httpPost($url, $data, $timeout = '10') {
	$url = urlinfo($url); //获取URL信息
	$query = $url["request"];
	
	if(is_array($data)) { //数组转成拼接
		foreach($data as $k =>$v) {
			$data_arr .=(empty($data_arr) ? "" : "&").urlencode($k)."=".urlencode($v);
		}
	}
	
	$header = "POST ".$query." HTTP/1.1\r\n";
	$header .= "Host:".$url["host"]."\r\n";
	$header .= "Content-type: application/x-www-form-urlencoded\r\n";
	$header .= "Content-Length:".strlen($data_arr)."\r\n";
	$header .= "Connection: Close\r\n\r\n";
	if ($data_arr) $header.= "$data_arr\r\n\r\n";

	$fp = fsockopen($url["host"], $url["port"], $errno, $errstr, $timeout);
	if (!$fp) {
		Log::write("fsockopen打开失败：$errstr ($errno)", Log::ERR);
	}

	fwrite($fp,$header);
	
	fclose($fp);
	unset($header);

	return true;
}

/*
 * 用处 ：此函数用来逆转javascript的escape函数编码后的字符。
* 关键的正则查找我不知道有没有问题.
* 参数：javascript编码过的字符串。
* 如：unicodeToUtf8("%u5927")= 大
* 2005-12-10
*
*/
function unescape($escstr){
	preg_match_all("/%u[0-9A-Za-z]{4}|%.{2}|[0-9a-zA-Z.+-_]+/",$escstr,$matches); //prt($matches);
	$ar = &$matches[0];
	$c = "";
	foreach($ar as $val){
	if (substr($val,0,1)!="%") { //如果是字母数字+-_.的ascii码
		$c .=$val;
	}
	elseif (substr($val,1,1)!="u") { //如果是非字母数字+-_.的ascii码
		$x = hexdec(substr($val,1,2));
		$c .=chr($x);
	}
	else { //如果是大于0xFF的码
		$val = intval(substr($val,2),16);
		if($val < 0x7F){        // 0000-007F
		$c .= chr($val);
		}elseif($val < 0x800) { // 0080-0800
		$c .= chr(0xC0 | ($val / 64));
		$c .= chr(0x80 | ($val % 64));
		}else{                // 0800-FFFF
		$c .= chr(0xE0 | (($val / 64) / 64));
		$c .= chr(0x80 | (($val / 64) % 64));
		$c .= chr(0x80 | ($val % 64));
		}
	}
	}
	return $c;
}

/** 
 * js escape php 实现 
 * @param $string           the sting want to be escaped 
 * @param $in_encoding       
 * @param $out_encoding      
 */ 
function escape($string, $in_encoding = 'UTF-8',$out_encoding = 'UCS-2') { 
	$return = ''; 
	if (function_exists('mb_get_info')) { 
	for($x = 0; $x < mb_strlen ( $string, $in_encoding ); $x ++) { 
		$str = mb_substr ( $string, $x, 1, $in_encoding ); 
		if (strlen ( $str ) > 1) { // 多字节字符 
		$return .= '%u' . strtoupper ( bin2hex ( mb_convert_encoding ( $str, $out_encoding, $in_encoding ) ) ); 
		} else { 
		$return .= '%' . strtoupper ( bin2hex ( $str ) ); 
		} 
	} 
	} 
	return $return; 
}

function format_date($timestamp, $showtime = 1) {
	$times = intval($timestamp);
	if (!$times) return true;
	$str = $showtime ? date('Y-m-d H:i:s', $times) : date('Y-m-d', $times);
	return $str;
}

/**
 * 将字符串转换为数组
 *
 * @param string $data        	
 * @return array
 *
 */
function string2array($data) {
	if ($data == '') return array();
	eval("\$array = \"$data\";");
	return $array;
}

/**
 * 将数组转换为字符串
 *
 * @param array $data        	
 * @param bool $isformdata        	
 * @return string
 *
 */
function array2string($data, $isformdata = 1) {
	if ($data == '') return '';
	if ($isformdata) $data = new_stripslashes($data);
	return addslashes(var_export($data, TRUE));
}

/**
 * 调试，用于保存数组到txt文件 正式生产删除 array2file($info, 'post.txt');
 */
function array2file($array, $filename) {
	file_exists($filename) or touch($filename);
	file_put_contents($filename, var_export($array, TRUE));
}

//打印并高亮函数
function dump($target,$bool=true){
    static $i = 0;
    if($i==0){
        header('content-type:text/html;charset=utf-8');
    }
    CVarDumper::dump($target, 10, true);
    $i++;
    if($bool){
       exit;
    }else{
        echo '<br />';
    }
}
 
//打印并高亮函数
function p($target,$bool=true){
    static $i = 0;
    if($i==0){
        header('content-type:text/html;charset=utf-8');
    }
    echo '<pre>';
    print_r($target);
    $i++;
    if($bool){
       exit;
    }else{
        echo '<br />';
    }
}
?>