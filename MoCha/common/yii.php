<?php
/*
 * YII扩展函数库
 */
defined('DS') or define('DS',DIRECTORY_SEPARATOR);

defined('TIME') or define('TIME', time());

defined('MTIME') or define('MTIME', intval(microtime(true)));//返回当前unix时间戳

function app()
{
	return Yii::app();
}

function cs()
{
     return Yii::app()->getClientScript();
}

/**
  * This is the shortcut to Yii::app()->user.
  */
function user()
{
     return Yii::app()->user();
}

/**
  * This is the shortcut to Yii::app()->createUrl()
  */
function url( $route , $params = array (), $ampersand = '&' )
{
     return Yii::app()->createUrl( $route , $params , $ampersand );
}

/**
  * This is the shortcut to CHtml::encode
  */
/* function h( $text )
{
     return htmlspecialchars( $text ,ENT_QUOTES,Yii::app()->charset);
} */

/**
  * This is the shortcut to Yii::app()->request->baseUrl
  * If the parameter is given, it will be returned and prefixed with the app baseUrl.
  */
function baseDir( $url =null)
{
     //static $baseUrl = null;
     //if ( $baseUrl ===null)
     $baseUrl =Yii::app()->getRequest()->getBaseUrl();
     return $url ===null ?  $baseUrl :  $baseUrl . '/' .ltrim( $url , '/' );
}

/**
  * Returns the named application parameter.
  * This is the shortcut to Yii::app()->params[$name].
  */
function param( $name )
{
     return Yii::app()->params[ $name ];
}
/**
  * A useful one that I use in development is the following
  * which dumps the target with syntax highlighting on by default
  */
function dump( $target )
{
   return CVarDumper::dump( $target , 10, true) ;
}

function mk_dir($dir, $mode = 0777)
{
 if (is_dir($dir) || @mkdir($dir,$mode)) return true;
 if (!mk_dir(dirname($dir),$mode)) return false;
 return @mkdir($dir,$mode);
}

?>