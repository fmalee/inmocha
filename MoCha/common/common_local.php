<?php
/*
 * 项目扩展函数库_普通环境
 */

/**
 * 远程图片下载
 * @param $field 预留字段
 * @param $value 传入下载内容
 * @param $watermark 是否加入水印
 * @param $absurl 绝对路径
 * @param
 *        	$basehref
 */
function remote_pic($value, $code) {
	load("@.dir");
	$uploaddir = SITE_PATH . C('UPLOAD_PATH');
	$file = new_stripslashes($value);
	dir_create($uploaddir);
	if (strpos($file, '://') === false || strpos($file, $uploaddir) !== false) continue;
	$filename = date('Ymdhis') . rand(100, 999) . '.' . fileext($file);
	$newfile = $uploaddir . $filename;
	$upload_func = 'copy';
	if ($upload_func($file, $newfile)) {
		@chmod($newfile, 0777);
	}
	return $newfile;
}
?>