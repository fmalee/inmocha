<?php
/*
 * 项目扩展函数库_SAE专用
 */

/**
 * 远程图片下载
 * @param $field 预留字段
 * @param $value 传入下载内容
 * @param $watermark 是否加入水印
 * @param $absurl 绝对路径
 * @param
 *        	$basehref
 */
function remote_pic($value) {
	import("ORG.Net.Http");
	$file = new_stripslashes($value);
	if (strpos($file, '://') === false) continue;
	$filename = date('Ymdhis') . rand(100, 999) . '.' . fileext($file);
	$newfile = SAE_TMP_PATH . $filename;
	Http::curlDownload($file, $newfile);
	return $newfile;
}
?>